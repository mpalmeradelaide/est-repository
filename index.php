<?php
require_once('connection.php');

mysql_select_db($database,$conn);
// To get VP Data
$query="select profile_count from profile_analysed where id=1";   
$res=mysql_query($query,$conn);
$row=mysql_fetch_assoc($res);
$count=$row['profile_count'];

// To get Raw Data
$query2="select count(*) as count from client_info "; 
$res2=mysql_query($query2,$conn);
$row2=mysql_fetch_assoc($res2);
$count2=$row2['count'];

$total =13187+$count+$count2;
//
?>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Exercise Science Toolkit</title>
<link rel="shortcut icon" href="images/favicon.ico">
<link rel="stylesheet" href="css/master.css">
<link rel="stylesheet" href="css/responsive.css">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
 
</head>
<body>

<!--=== Loader Start ======-->
<div id="loader-overlay">
  <div class="loader-wrapper">
    <div class="arcon-pulse"></div>
  </div>
</div>
<!--=== Loader End ======-->

<!--=== Wrapper Start ===-->
<div class="wrapper">

  <!--=== Header Start ===-->
  <nav class="navbar navbar-default navbar-fixed navbar-transparent white bootsnav on no-full">
    
    <div class="container">
      
      <!--=== Start Header Navigation ===-->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-menu"> <i class="eicon ion-android-menu"></i> </button>
        <div class="logo"> <a href="index.html">
			<!--<img class="logo logo-display" src="images/logo-white.png" alt="">-->
			<h3 class="logo logo-display header_title" style="font-weight: bold; color: white; position: fixed; top: 15px;">Exercise Science Toolkit</h3>
			<!-- <img class="logo logo-scrolled" src="images/logo-black.png" alt=""> -->
			<h3 class="logo logo-scrolled header_title_scroll" style="font-weight: bold; color: black;">Exercise Science Toolkit</h3>
			</a>
		</div>
      </div>
      <!--=== End Header Navigation ===-->

	  </script>
      
      <!--=== Collect the nav links, forms, and other content for toggling ===-->
      <div class="collapse navbar-collapse" id="navbar-menu">
        <ul class="nav navbar-nav navbar-right" data-in="fadeIn" data-out="fadeOut">
          <li><a class="page-scroll" href="#home">Home</a></li>
          <li><a class="page-scroll" href="#whoweare">Who We Are</a></li>
          <li><a class="page-scroll" href="#portfolio">Portfolio</a></li>
          <li><a class="page-scroll" href="#contact">Contact</a></li>
            <li><a class="page-scroll" href="#">Manual</a></li>
        </ul>
      </div>
      <!--=== /.navbar-collapse ===-->
    </div>
  </nav>
  <!--=== Header End ===-->
  
  <!--=== Flex Slider Start ===-->
  <section class="pt-0 pb-0" id="home">   	
    <div class="slider-bg flexslider">
      <ul class="slides">
        <!--=== SLIDE 1 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-1.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text white-color">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Exercise Science  </h2>
                <h1 class="white-color text-uppercase font-700">Toolkit</h1>
                <h3 class="white-color font-400">An overview of some of the screens in the six modules is presented below. </h3> 
                 <p class="text-center mt-30"><a class="btn btn-color btn-circle page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-4.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Screening and health risk factor assessment 
</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Pre-exercise screening is part of the duty of care for those involved in exercise prescription.</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
				
		<!--=== SLIDE 2 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-2.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Fitness testing</h2>
                <h1 class="white-color text-uppercase font-700">module</h1>
                <h3 class="white-color font-400">Fitness testing and comparisons across the three energy systems</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
        <!--=== SLIDE 3 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-3.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Body composition</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Analysis and comparison of anthropometry data</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-vio page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		<!-- Added Amit -->
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-5.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Sport Match</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Matching body size, shape and composition to elite-level athletes</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-cyan page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-6.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Virtual Population </h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Generate virtual populations and explore relationships among behaviours, health variables, body composition and fitness parameters</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-black page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-7.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Blood Biomarkers</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Determine percentile ranks and threshold levels for blood biomarkers</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-blue page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
        
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-8.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text white-color">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Exercise Science  </h2>
                <h1 class="white-color text-uppercase font-700">Toolkit</h1>
                <h3 class="white-color font-400">An overview of some of the screens in the six modules is presented below. </h3> 
                 <p class="text-center mt-30"><a class="btn btn-color btn-circle page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-9.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Screening and health risk factor assessment 
</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Pre-exercise screening is part of the duty of care for those involved in exercise prescription.</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
		
		<!--=== SLIDE 2 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-10.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Fitness testing</h2>
                <h1 class="white-color text-uppercase font-700">module</h1>
                <h3 class="white-color font-400">Fitness testing and comparisons across the three energy systems</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
        <!--=== SLIDE 3 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-11.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Body composition</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Analysis and comparison of anthropometry data</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-vio page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		<!-- Added Amit -->
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-12.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Sport Match</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Matching body size, shape and composition to elite-level athletes</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-cyan page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-13.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Virtual Population </h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Generate virtual populations and explore relationships among behaviours, health variables, body composition and fitness parameters</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-black page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-14.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Blood Biomarkers</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Determine percentile ranks and threshold levels for blood biomarkers</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-blue page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-15.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text white-color">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Exercise Science  </h2>
                <h1 class="white-color text-uppercase font-700">Toolkit</h1>
                <h3 class="white-color font-400">An overview of some of the screens in the six modules is presented below. </h3> 
                 <p class="text-center mt-30"><a class="btn btn-color btn-circle page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-16.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Screening and health risk factor assessment 
</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Pre-exercise screening is part of the duty of care for those involved in exercise prescription.</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
		
		
		<!--=== SLIDE 2 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-17.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Fitness testing</h2>
                <h1 class="white-color text-uppercase font-700">module</h1>
                <h3 class="white-color font-400">Fitness testing and comparisons across the three energy systems</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
        <!--=== SLIDE 3 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-18.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Body composition</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Analysis and comparison of anthropometry data</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-vio page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		<!-- Added Amit -->
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-19.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Sport Match</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Matching body size, shape and composition to elite-level athletes</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-cyan page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-20.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Virtual Population </h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Generate virtual populations and explore relationships among behaviours, health variables, body composition and fitness parameters</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-black page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
		
		
		
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-21.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Blood Biomarkers</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Determine percentile ranks and threshold levels for blood biomarkers</h3>
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-blue page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-22.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap">
            <div class="hero-text white-color">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Exercise Science  </h2>
                <h1 class="white-color text-uppercase font-700">Toolkit</h1>
                <h3 class="white-color font-400">An overview of some of the screens in the six modules is presented below. </h3> 
                 <p class="text-center mt-30"><a class="btn btn-color btn-circle page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a> </p>
              </div>
            </div>
          </div>
        </li>
        
		<li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-23.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Screening and health risk factor assessment 
</h2>
                <h1 class="white-color text-uppercase font-700">Module</h1>
                <h3 class="white-color font-400">Pre-exercise screening is part of the duty of care for those involved in exercise prescription.</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>
		
		<!--=== SLIDE 2 ===-->
        <li>
          <div class="slide-img parallax-effect" style="background:url(images/slides/home-bg-24.jpg) center center / cover scroll no-repeat;"></div>
          <div class="hero-text-wrap gradient-overlay-bg">
            <div class="hero-text">
              <div class="container text-center">
                <h2 class="white-color text-uppercase font-400 letter-spacing-5">Fitness testing</h2>
                <h1 class="white-color text-uppercase font-700">module</h1>
                <h3 class="white-color font-400">Fitness testing and comparisons across the three energy systems</h3>       
                <p class="text-center mt-30"><a class="btn btn-color btn-circle btn-green page-scroll" href="https://est-demo.jamesanthony.consulting/VirtualEST/">Open Toolkit</a></p>
              </div>
            </div>
          </div>
        </li>		
      </ul>
    </div>
  </section>
  <!--=== Flex Slider End ===-->
  
  <!--=== Counter Start ===-->
  <section class="dark-bg pt-80 pb-80">
    <div class="container-fluid">
      <div class="row">
        
        <div class="col-md-12 counter text-center col-sm-6 wow fadeInRight" data-wow-delay="0.2s"> <i class="fa fa-smile-o"></i>
          <h2 class="count white-color font-700"><?php echo $total ;?></h2>
          <h3>Profiles analysed</h3>
        </div>
       
      </div>
    </div>
  </section>
  <!--=== Counter End ===-->

  <!--=== Who We Are Start ===-->
  <section style="background-color: #ffe7e6;" id="whoweare">
    <div class="container">
      <div class="row">
        <div class="col-sm-10 section-heading">
          <h2 class="text-uppercase wow fadeTop" data-wow-delay="0.1s">Who We Are</h2>
          <!--<h4 class="text-uppercase source-font wow fadeTop" data-wow-delay="0.2s">- The world at your fingertips -</h4>-->
          <div class="mt-30 wow fadeTop" data-wow-delay="0.3s">
              <div class="row">
                  <div class="col-sm-6">
              <img src="images/kevin.png" alt="" style="width:200px; height:200px; margin-top: 1rem;">
            <h4 class="text-uppercase" style="margin-top: 2.75rem;"><strong>Professor Kevin NORTON <br> School of Health Sciences <br> University of South Australia</strong></h4>
            <p>Kevin is a Professor of Exercise Science in the School of Health Sciences, University of South Australia. He has about 30 years experience as an academic and has published about 200 research articles. He has worked with professional sporting organisations in Australian Football, Rugby Union and Rugby League, and Olympic water sports and also served on numerous national government and professional committees. Kevin and Lynda have produced a range of educational, sports and health-related software for high school students through to products for professionals, including the <i>Science through Sport</i> series, and HealthScreen Pro and MovementScreen Pro Apps on the Apple store. <strong>[k.norton@unisa.edu.au]</strong></p>
                </div>
              
    
                  <div class="col-sm-6">
              <img src="images/lynda.png" alt="" style="width:200px; height:200px; margin-top: 1rem;">
            <h4 class="text-uppercase" style="margin-top: 2.75rem;"><strong>Dr Lynda NORTON <br> College of Nursing and Health Sciences <br> Flinders University</strong></h4>
            <p>Lynda is a Senior Lecturer in the School of Health Sciences, Flinders University and teaches in health promotion, exercise science and paramedic studies. Lynda is a registered intensive care nurse who has practiced in both the USA and in Australia. Lynda also has an MPH and PhD in physical activity intervention strategies. In addition to software development, her current interests include trends in adult health and fitness, relationships between exercise and health outcomes, and mindfulness and stress modification in emergency services personnel. <strong>[lynda.norton@flinders.edu.au]</strong></p>
                </div>
            </div>
        </div>
        </div>
      </div>
    </div>
  </section>
  <!--=== Who We Are End ===-->
  
  <!--=== Portfolio Start ===-->
  <section class="pb-0 pt-0 white-bg" id="portfolio">
      <div class="container pt-50 pb-50">
		  <div class="row">
			<div class="col-sm-10 section-heading">
			  <h2 class="text-uppercase wow fadeTop" data-wow-delay="0.1s">Exercise Science Toolkit content </h2>
			  <!--<h4 class="text-uppercase source-font wow fadeTop" data-wow-delay="0.2s">- The world at your fingertips -</h4>-->
			  <div class="mt-30 wow fadeTop" data-wow-delay="0.3s">
				<p>The EST has multiple analytical tools on approximately 40 screens. These are grouped within six modules covering:</p>
				<p>1. Screening and health risk factor assessment<br>
				2. Fitness testing across the energy systems<br>
				3. Body composition analysis<br>
				4. Finding sports that match body size, shape and composition<br>
				5. Generating virtual populations to investigate relationships among behaviours, health biomarkers, and fitness<br>
				6. Analyzing blood biomarkers</p>
				<p>The EST has two 'layers', or ways of using the program:<br>Using raw data collected on real people or creating a virtual person (VP) to further investigate.</p>
			  </div>
			</div>
		  </div>
		</div>
    <section class="white-bg" id="test1">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen8.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">Screening and health risk factor assessment module</h3>
			<p class="mt-20">Pre-exercise screening is part of the duty of care in exercise prescription. The screening module in the EST can be used for real clients to assist in managing medical conditions and in constructing appropriate programs for people beginning exercise. It also allows investigation of unlimited VP including cases, for example, the elderly and those in poorer health and fitness states, and elite-level athletes. This facilitates decision-making and cross-checking of these decisions in the safety of dealing with a VP rather than real clients.</p>
			<p>Comprehensive ‘what-if’ functionality is embedded in the module to allow exploration of health risk factors, absolute risk of cardiovascular disease and life expectancy as behaviours and biomarkers are altered.</p>
		  </div>
		</div>
	  </section>
	  
	  <section class="white-bg" id="test2">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen3.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">Fitness Testing Module</h3>
			<p class="mt-20">The Fitness testing module is structured to reflect a range of tests associated with each of the energy systems. The screens have both population-based norms (5-year age- and sex-specific groups) as well as a contemporary database of the best quality athletes reported in the scientific literature. There are printout options for each screen and numerous graphing and comparison functions.</p>
		  </div>
		</div>
	  </section>
	  
	  <section class="white-bg" id="test3">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen10.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">Body Composition module</h3>
			<p class="mt-20">The Body composition module allows the user to input anthropometry data and then explore outputs such as: comparisons against population norms, % body fat prediction, skinfold plots, somatotype, fractionation of body mass, and various types of reliability analyses to check skill competencies. </p>
		  </div>
		</div>
	  </section>
	  
	  <section class="white-bg" id="test4">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen2.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">SPORT MATCH MODULE</h3>
			<p class="mt-20">The Sport match module uses the inputs of a range of body measurements (size, shape and composition) and a probability function to determine the profile’s best fit among about 100 sports. The normative data are from published information of the highest quality athletes and the degree of ‘best fit’ is represented by an ‘overlap score’ out of 100. ‘What-if’ scenarios can be performed to modify inputs and explore the sports rankings thereby reinforcing body proportions and composition with sport functionality and athletic performance. </p>
		  </div>
		</div>
	  </section>
	  
	  <section class="white-bg" id="test5">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen11.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">VIRTUAL POPULATION MODULE</h3>
			<p class="mt-20">To facilitate investigation among the ~200 VP variables a ‘Virtual population’ function has been developed. This allows up to 5000 VP profiles to be generated per iteration and exported in excel files. These populations can be explored for patterns such as correlations or ageing trends, differences across sexes or, for example, when analysing different disease conditions, sedentariness and activity levels. </p>
		  </div>
		</div>
	  </section>
	  <section class="white-bg" id="test6">
		<div class="col-md-6 col-sm-4 bg-flex bg-flex-left">
		  <div class="bg-flex-holder bg-flex-cover" style="background-image:url(images/portfolio/screen1.jpg);"></div>
		</div>
		<div class="container">
		  <div class="col-md-5 col-sm-7 col-md-offset-7">
			<h3 class="text-uppercase font-700">BLOOD BIOMARKERS MODULE</h3>
			<p class="mt-20">Users can enter data to determine percentile ranks for up to nine blood biomarkers, and to help reinforce units of measurement and risk factor threshold levels for key health indicators. Databases were constructed from national health surveys and large epidemiological studies across six countries and cover ages 18-75 yr.</p>
		  </div>
		</div>
	  </section>
  </section>
  <!--=== Portfolio End ===-->
  
  <section class="contact-us white-bg" id="contact">
    <div class="container">
      <div class="row">
        <div class="col-sm-8 section-heading">
          <h2 class="text-uppercase wow fadeTop" data-wow-delay="0.1s">Contact Us</h2>
          <!--h4 class="text-uppercase source-font wow fadeTop" data-wow-delay="0.2s">- Get in Touch -</h4-->
        </div>
      </div>
      <div class="row mt-50">
        <div class="col-md-8">
          <form name="contact-form" id="contact-form" action="mail.php" method="POST">
            <div class="messages"></div>
            <div class="row">
            	<div class="col-md-6">
                	<div class="form-group wow fadeTop" data-wow-delay="0.1s">
                      <label class="sr-only" for="name">Name</label>
                      <input type="text" name="name" id="name" class="form-control" id="name-2" required placeholder="Your Name" data-error="Your Name is Required">
                      <div class="help-block with-errors mt-20"></div>
                    </div>
                </div>
				
                <div class="col-md-6">
                	<div class="form-group wow fadeTop" data-wow-delay="0.2s">
                      <label class="sr-only" for="email">Email</label>
                      <input type="email" name="email" id="email" class="form-control" id="email-2" placeholder="Your Email" required data-error="Please Enter Valid Email">
                      <div class="help-block with-errors mt-20"></div>
                    </div>
                </div>
            </div>
            <div class="form-group wow fadeTop" data-wow-delay="0.3s">
              <label class="sr-only" for="subject">Subject</label>
              <input type="text" name="subject" id="subject" class="form-control" id="subject-2" placeholder="Your Subject">
            </div>
            <div class="form-group wow fadeTop" data-wow-delay="0.4s">
              <label class="sr-only" for="message">Message</label>
              <textarea name="message" class="form-control" id="message" name="message" rows="7" placeholder="Your Message" required data-error="Please, Leave us a message"></textarea>
              <div class="help-block with-errors mt-20"></div>
            </div>
            <div class="wow fadeTop" data-wow-delay="0.5s">
            	<button type="submit" name="submit" class="btn btn-color btn-circle">Send Mail</button>
   		   </div>
          </form>
        </div>
        <div class="col-md-4">
        <div class="wow fadeTop" data-wow-delay="0.1s">
          <!--h3>Postal Location</h3-->
          <address>
          Professor Kevin Norton<br/>
          School of Health Sciences<br/>
          University of South Australia<br/>
          <a>Mail  :   k.norton@unisa.edu.au</a>
          </address>
         </div>
         <div class="wow fadeTop" data-wow-delay="0.2s"> 
          <address>
          +61 8 8302 1503  |  o<br>
          +61 417 817 026  |  m<br>
          </address>
          </div>
          <div class="wow fadeTop" data-wow-delay="0.3s">
          <h3>Time Zone</h3>
          <p> <span>Adelaide time:</span> <br>
            <span>GMT +10:30 hours</span> </p>
          </div>
		
		  <div class="wow fadeTop" data-wow-delay="0.3s">
          <h3>Suggested citation</h3>
          <p> <span>Norton, K. and L. Norton (2018). </span><br>
		      <span>Exercise Science Toolkit [Computer software].</span> <br>
            <span>Retrieved from https://est-demo.jamesanthony.consulting</span> </p>
           </div>		
			
        </div>
      </div>
    </div>
  </section>
  <!--=== Contact Us End ======-->
  
  <!--=== Footer Start ===-->
  <footer class="footer" id="footer-fixed">
   
    <div class="footer-copyright">
      <div class="container">
        <div class="row">
          <div class="col-xs-12">
            <div class="copy-right text-center">© 2017 Exercise Science Toolkit. All rights reserved</div>
          </div>
        </div>
      </div>
    <!--  <div class="head_counter">13187</div>-->   
    </div>
  </footer>
  <!--=== Footer End ===-->
  
  <!--=== GO TO TOP  ===-->
  <a href="#" id="back-to-top" title="Back to top">&uarr;</a>
  <!--=== GO TO TOP End ===-->
  
</div>
<!--=== Wrapper End ===-->

<!--=== Javascript Plugins ===-->
<script src="js/jquery.min.js"></script>
<script src="js/smoothscroll.js"></script>
<script src="js/validator.js"></script>
<script src="js/plugins.js"></script>
<script src="js/master.js"></script>
<script src="js/bootsnav.js"></script>
<!--=== Javascript Plugins End ===-->
<script type="text/javascript">
    var i=13187;
	setInterval(function(){	
//			var we =Math.floor(i);        
        $(".head_counter").html(i);
        i++;
		//console.log(we);
	},4000);
  </script>
</body>
</html>
