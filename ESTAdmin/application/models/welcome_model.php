<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class Welcome_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	/**
	 * create_user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $email
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	

        
        public function login($username, $password)
        {
                $this -> db -> select('id, user_name, pass_word');
                $this -> db -> from('membership');
                $this -> db -> where('user_name', $username);
                $this -> db -> where('pass_word', MD5($password));
                $this -> db -> limit(1);

                $query = $this -> db -> get();

                if($query -> num_rows() == 1)
                {
                  return $query->result();
                }
                else
                {
                  return false;
                }
 
        }
        /**
	 * resolve_user_login function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @param mixed $password
	 * @return bool true on success, false on failure
	 */
	public function user_list()
         {
		
		$this->db->select('*');
		$this->db->from('loginapp');
		$query = $this->db->get();
		return $query->result_array(); 
		
            }
	
	/**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
        public function web_user_list()
        {
                $this->db->select('*');
                $this->db->where('is_website',1);
		$this->db->from('loginapp');
		$query = $this->db->get();
		return $query->result_array();    
        }
        
        
        /**
	 * ipad_user_list function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
        public function ipad_user_list()
        {
               
               
		
		$this->db->select('login_id');
		$this->db->select('login_name');
		$this->db->select('login_company');
		$this->db->select('login_username');
		$this->db->select('login_password');
		$this->db->select('login_ipad');
                 $this->db->select('DATE_FORMAT(start_date,"%Y-%m-%d") AS start_date',false);
		$this->db->select('DATE_FORMAT(end_date,"%Y-%m-%d") AS end_date',false);
		$this->db->select('COALESCE(DATEDIFF(end_date,start_date),days) AS days',false);
		$this->db->select('DATE_FORMAT(login_date,"%Y-%m-%d") AS login_date',false);
		$this->db->select('login_count');
		$this->db->select('version_no');
		$this->db->select('skip_status');
		$this->db->select('used_version_no');
                $this->db->select('is_ipad');
                $this->db->where('is_ipad',1);
		$this->db->from('loginapp');
                $this->db->order_by('login_id','asc');
                $query = $this->db->get();
		//print_r($this->db->last_query());die;
		return $query->result_array(); 	
        }
        /**
	 * get_user_id_from_username function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
        //delete selected record 
        
         /**
	 * Add User function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
        public function addnew_user($data)
        {
          //print_r($data);
         //$exists =$this->db->select('login_name,login_username')->where('login_name',$data['login_name'])->where('login_username',$data['login_username'])->get('loginapp')->row_array();
         
            
             $this->db->where('login_username', $data['login_username']);
             $this->db->where('login_name', $data['login_name']);
             $query = $this->db->get('loginapp');
                $count_row = $query->num_rows();
            //print_r($this->db->last_query());die;
                                     if($count_row > 0)
                                     {
                                     return FALSE; // here I change TRUE to false.
                                     } 
                                     else 
                                     {
                                      $insert = $this->db->insert('loginapp', $data);
                                      return $insert;
                                     }
//         if($exists)
//            {
//            return false;
//            }
//            else
//            {
//              $insert = $this->db->insert('loginapp', $data);
//             return $insert;
//            }  
        }






        public function deleteselectedrecord($data)
        {
            $comma_separated = implode(",", $data);
            $query = "DELETE FROM loginapp WHERE login_id IN ($comma_separated)";
            $result = $this->db->query($query);
            //$rajeev = $this->db->last_query();
           //print_r($rajeev);die;
           return $this->db->affected_rows() > 0;
        }
        
        /**
	 * delete single user function.
	 * 
	 * @access public
	 * @param mixed $username
	 * @return int the user id
	 */
     
        function delete_user($id)
        {
		$this->db->where('login_id', $id);
		$this->db->delete('loginapp'); 
	}
        /**
	 * hash_password function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @return string|bool could be a string on success, or bool false on failure
	 */
	private function hash_password($password) {
		
		return password_hash($password, PASSWORD_BCRYPT);
		
	}
	
	/**
	 * verify_password_hash function.
	 * 
	 * @access private
	 * @param mixed $password
	 * @param mixed $hash
	 * @return bool
	 */
	private function verify_password_hash($password, $hash) {
		
		return password_verify($password, $hash);
		
	}
        
        
        //End of model 
	
}
