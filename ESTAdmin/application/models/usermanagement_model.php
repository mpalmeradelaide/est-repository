<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * User_model class.
 * 
 * @extends CI_Model
 */
class usermanagement_model extends CI_Model {

	/**
	 * __construct function.
	 * 
	 * @access public
	 * @return void
	 */
	public function __construct() {
		
		parent::__construct();
		$this->load->database();
		
	}
	
	
	
	public function add_sportnormlist()
	{
		//$sid=$this->input->post('sid');
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sport_name=$this->input->post('sport_name1');
		$sport_type=$this->input->post('sport_type1');
		$gend=$this->input->post('gen1');
		$bf_per=$this->input->post('body_fatper1');
		$bf_sd=$this->input->post('body_fatsd1');
		$endo_morph=$this->input->post('endomorph1');
		$meso_morph=$this->input->post('mesomorph1');
		$ecto_morph=$this->input->post('ectomorph1');
		$endo_sd=$this->input->post('endosd1');
		$meso_sd=$this->input->post('mesosd1');
		$ecto_sd=$this->input->post('ectosd1');
		$hght=$this->input->post('height1');
		$ht_sd=$this->input->post('heightsd1');
		$b_m=$this->input->post('bodymass1');
		$bm_sd=$this->input->post('bodymasssd1');
		$sid=$this->input->post('sid1');
	
		$insert_qry= "INSERT INTO sport_match(sport, sport_type, gender, body_fat_per, body_fat_sd, endomorph, mesomorph, ectomorph, endo_sd, meso_sd, ecto_sd, height, height_sd, body_mass, body_mass_sd) VALUES ( '$sport_name','$sport_type','$gend','$bf_per','$bf_sd','$endo_morph','$meso_morph','$ecto_morph','$endo_sd','$meso_sd','$ecto_sd','$hght','$ht_sd','$b_m','$bm_sd')" ;
		
		/* $updateqry="update sport_match set sport='".$sport_name."' , sport_type='".$sport_type."' , gender= '".$gend."' , body_fat_per='".$bf_per."'
            ,body_fat_sd='".$bf_sd."',
			endomorph='".$endo_morph."',mesomorph='".$meso_morph."',ectomorph='".$ecto_morph."',endo_sd='".$endo_sd."',meso_sd='".$meso_sd."',
			ecto_sd='".$ecto_sd."',height='".$hght."',height_sd='".$ht_sd."',
			body_mass='".$b_m."',body_mass_sd='".$bm_sd."' where id = '".$sid."' "; 
		 */
		$res=$this->db->query($insert_qry);	
        if($res)
		{
			return true;
		}			
		else{
			return false;
		} 
	}
	
	
	
	
	public function save_project($project)
	{
		$insert_qry= "INSERT INTO project(projectname) VALUES ('$project')" ;
		$res=$this->db->query($insert_qry);	
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
	}
	
	//Get users
	public function get_users()
	{
		$getusers=$this->db->query("SELECT id,Gender,CONCAT(firstname,' ',lastname) AS `Name`,project,access_code,Age,Mass,Height,bmi AS `BMI`,subpopulation AS `Subpopulation`,country AS `Country`,occupation AS `Occupation`,heart_condition,pain_in_chaist ,feel_faint,asthma_attack,diabetes,bone_muscle_prob,other_medical_cond,walking,minutes_walking,vigorous,minute_vigorous,moderate,minute_moderate,tv_hours_games,driving_hours,is_setting_long_period,family_history,family_gender,age_at_heart_attack,current_smoker,how_many,quit_smoke_recently,how_many_recently,self_report_BP,self_report_Chol,self_report_Glucose,Measured_SBP,Measured_DBP,Measured_fast_blood_glucose,Measured_blood_Cholesterol,HDL,LDL,triglycerides,medication_selected,medical_cond_selected,vj ,FTCT,waist_hip_ratio,triceps ,biceps,subscapular AS `Subscapular Skinfold`,Sum_of_3_skinfolds,self_report_height,self_report_weight,MBJSites ,MBJOption ,L_grip ,R_Grip,Bench_press_kg,Bench_press_reps,Arm_curl_kg,Arm_curl_reps,Lateral_pulldown_kg,Lateral_pulldown_reps,Leg_press_kg,Leg_press_reps,Leg_extension_kg,Leg_extension_reps,Leg_curl_kg,Leg_curl_reps,illiacrest,supraspinale,abdominal,frontthigh,Medialcalf,midaxilla,armrelaxed,armflexed,waistminimum,Gluteal_hips,calfmaximum ,humerus,femur,head,neck,forearmmax,waist_distal_styloids,chest_mesosternale,thigh_onecmdistal,thigh_mid,ankle_minimum,acromiale_radiale,radiale_styl,midstyl_dacty,illiospinale,trochanterion,trochant_tibial,tibiale_laterale,tib_med_sphytib,biacromial,bideltoid,biiliocristal,bitrochanteric,footlength,sitting_length,transverse_chest,A_PChest,arm_span,mean_Body_Fat ,fat_mass,muscle_mass ,bone_mass,residual_mass,ectomorph_score,mesomorph_score,endomorph_score,vo2max,Continous_vo2max_treadmill,Discontinous_vo2max_distreadmill,Ergometer_Continous_vo2max,total_work_done_in30s_kJ ,shuttle_level,PPW_val ,PPWKg ,shuttle_VO2max,Watts_HRmax ,Predicted_VO2_submax_test ,Max_Power_RPE,VO2max_RPE ,sd_Body_Fat from client_person_info order by id ");
		$row = $getusers->result();
        return $row;
	}
	
	public function get_sportlist()
	{
		$getsport=$this->db->query("SELECT * from sport_match");
		$row = $getsport->result();
        return $row;
	}
	 
	public function checkuser($usname,$passwrd)
	{
		// Get entered
		$username=$usname;
		$password=$passwrd;
		$getcredentials=$this->db->query("SELECT * from admin_login");
		return  $getcredentials->result();
	}
	
	
	
	public function update_password($old,$new,$uname)
	{
		$getcredentials=$this->db->query("SELECT * from admin_login where password = '".$old."' && username='".$uname."'" );
		$res = $getcredentials->result();
		
		$usname = $res[0]->username;
		$psswrd = $res[0]->password;
		
		if(count($res) > 0 )
		{
		$updateqry=$this->db->query("update admin_login set password='".$new."' where username = '".$uname."' "); 	
        return $updateqry;
		}
		else{
			return false;			
		}
	}  
	
	public function update_sportlist()
	{
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sport_name=$this->input->post('sport_name');
		$sport_type=$this->input->post('sport_type');
		$gend=$this->input->post('gend');
		$bf_per=$this->input->post('bf_per');
		$bf_sd=$this->input->post('bf_sd');
		$endo_morph=$this->input->post('endo_morph');
		$meso_morph=$this->input->post('meso_morph');
		$ecto_morph=$this->input->post('ecto_morph');
		$endo_sd=$this->input->post('endo_sd');
		$meso_sd=$this->input->post('meso_sd');
		$ecto_sd=$this->input->post('ecto_sd');
		$hght=$this->input->post('hght');
		$ht_sd=$this->input->post('ht_sd');
		$b_m=$this->input->post('b_m');
		$bm_sd=$this->input->post('bm_sd');
		$sid=$this->input->post('sid');
		
		$updateqry="update sport_match set sport='".$sport_name."' , sport_type='".$sport_type."' , gender= '".$gend."' , body_fat_per='".$bf_per."'
            ,body_fat_sd='".$bf_sd."',
			endomorph='".$endo_morph."',mesomorph='".$meso_morph."',ectomorph='".$ecto_morph."',endo_sd='".$endo_sd."',meso_sd='".$meso_sd."',
			ecto_sd='".$ecto_sd."',height='".$hght."',height_sd='".$ht_sd."',
			body_mass='".$b_m."',body_mass_sd='".$bm_sd."' where id = '".$sid."' "; 		
		$res=$this->db->query($updateqry);
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
			
	}
	
	//Update Sports Fitness Norms
	 public function update_fitness_sports_norm()
	{
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sport_name=$this->input->post('sports');
		$vj_mean=$this->input->post('vjmean');
		$gend=$this->input->post('gend');
		$vj_sd=$this->input->post('vjsd');
		$flight_mean=$this->input->post('flight_mean');
		$flight_sd=$this->input->post('flight_sd');
		$peak_power_mean=$this->input->post('peak_power_mean');
		$peak_power_sd=$this->input->post('peak_power_sd');
		$thirtys_total_work_mean=$this->input->post('thirtys_total_work_mean');
		$thirtys_total_work_sd=$this->input->post('thirtys_total_work_sd');
		$endurance_mean=$this->input->post('endurance_mean');
		$endurance_sd=$this->input->post('endurance_sd');
		$lactate_w_mean=$this->input->post('lactate_w_mean');
		$lactate_w_sd=$this->input->post('lactate_w_sd');
		$lactate_mm_mean=$this->input->post('lactate_mm_mean');
		$lactate_mm_sd=$this->input->post('lactate_mm_sd');
		$grip_strength_mean=$this->input->post('grip_strength_mean');
		$grip_strength_sd=$this->input->post('grip_strength_sd');
		$sid=$this->input->post('sid');
		
		$updateqry="update sports_fitness_norms set sports='".$sport_name."',vp_mean='".$vj_mean."',gender= '".$gend."' ,vp_sd='".$vj_sd."'
            ,flight_mean='".$flight_mean."',
			flight_sd='".$flight_sd."',peak_power_mean='".$peak_power_mean."',peak_power_sd='".$peak_power_sd."',thirtys_total_work_mean='".$thirtys_total_work_mean."',thirtys_total_work_sd='".$thirtys_total_work_sd."',
			endurance_mean='".$endurance_mean."',endurance_sd='".$endurance_sd."',lactate_w_mean='".$lactate_w_mean."',
			lactate_w_sd='".$lactate_w_sd."',lactate_km_mean='".$lactate_mm_mean."',lactate_km_sd='".$lactate_mm_sd."',grip_strength_mean='".$grip_strength_mean."',grip_strength_sd='".$grip_strength_sd."' where id = '".$sid."' "; 		
		
		$res=$this->db->query($updateqry);
        if($res)
		{
			return true;
		}			
		else{
			return false;
		} 
			
	}
	
	
	public function get_normlist()
	{
		$getnorm=$this->db->query("SELECT * from blood_norm");
		$row = $getnorm->result();
        return $row;
	}
	
	
	public function update_normlist()
	{
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sid=$this->input->post('sid');
		$meansbp=$this->input->post('meansbp');
		$sdsbp=$this->input->post('sdsbp');
		$gend=$this->input->post('gend');
		$meandbp=$this->input->post('meandbp');
		$sddbp=$this->input->post('sddbp');
		$meancols=$this->input->post('meancols');
		$sdcols=$this->input->post('sdcols');
		$meanlnhdl=$this->input->post('meanlnhdl');
		$sdlnhdl=$this->input->post('sdlnhdl');
		$meanlntrig=$this->input->post('meanlntrig');
		$sdlntrig=$this->input->post('sdlntrig');
		$meanldl=$this->input->post('meanldl');
		$sdldl=$this->input->post('sdldl');
		$meanlnglu=$this->input->post('meanlnglu');
		$sdlnglu=$this->input->post('sdlnglu');
	
		
		$updateqry="update blood_norm set mean_sbp='".$meansbp."' , sd_sbp='".$sdsbp."' , mean_dbp='".$meandbp."'
            ,sd_dbp='".$sddbp."',
			mean_cols='".$meancols."',sd_cols='".$sdcols."',mean_ln_hdl='".$meanlnhdl."',sd_ln_hdl='".$sdlnhdl."',	mean_ln_trig='".$meanlntrig."',
			sd_ln_trig='".$sdlntrig."',mean_ldl='".$meanldl."',sd_ldl='".$sdldl."',
			mean_ln_glu='".$meanlnglu."',sd_ln_glu='".$sdlnglu."' where id = '".$sid."' "; 	
				
		$res=$this->db->query($updateqry);		
			
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
			
	}
	
	
	
	
	
	public function add_bloodnormlist()
	{
		//$sid=$this->input->post('sid');
		$country=$this->input->post('country1');
		$age=$this->input->post('age1');
		$meansbp=$this->input->post('meansbp1');
		$sdsbp=$this->input->post('sd_sbp1');
		$gend=$this->input->post('gen1');
		$meandbp=$this->input->post('mean_dbp1');
		$sddbp=$this->input->post('sd_dbp1');
		$meancols=$this->input->post('mean_cols1');
		$sdcols=$this->input->post('sd_cols1');
		$meanlnhdl=$this->input->post('mean_ln_hdl1');
		$sdlnhdl=$this->input->post('sd_ln_hdl1');
		$meanlntrig=$this->input->post('mean_ln_trig1');
		$sdlntrig=$this->input->post('sd_ln_trig1');
		$meanldl=$this->input->post('mean_ldl1');
		$sdldl=$this->input->post('sd_ldl1');
		$meanlnglu=$this->input->post('mean_ln_glu1');
		$sdlnglu=$this->input->post('sd_ln_glu1');
	
		$insert_qry= "INSERT INTO blood_norms_all_ctry(age, gender, country, mean_sbp, sd_sbp, mean_dbp, sd_dbp, mean_cols, sd_cols, mean_ln_hdl, sd_ln_hdl, mean_ln_trig, sd_ln_trig, mean_ldl, sd_ldl, mean_ln_glu, sd_ln_glu) VALUES ( '$age','$gend','$country','$meansbp','$sdsbp','$meandbp','$sddbp','$meancols','$sdcols','$meanlnhdl','$sdlnhdl','$meanlntrig','$sdlntrig','$meanldl','$sdldl','$meanlnglu','$sdlnglu')" ;
	
		$res=$this->db->query($insert_qry);	

        //echo $this->db->last_query();		die;
			
        if($res)
		{
			return true;
		}			
		else{
			return false;
		} 
			 
	}
	
	//Add New Sports Fitness Norms
	public function add_sports_fitness_norms()
	{
	   $postData=$this->input->post();
		$sport_name=$this->input->post('sports1');
		$vj_mean=$this->input->post('vjmean1');
		$gend=$this->input->post('gen1');
		$vj_sd=$this->input->post('vjsd1');
		$flight_mean=$this->input->post('flight_mean1');
		$flight_sd=$this->input->post('flight_sd1');
		$peak_power_mean=$this->input->post('peak_power_mean1');
		$peak_power_sd=$this->input->post('peak_power_sd1');
		$thirtys_total_work_mean=$this->input->post('thirtys_total_w_Mean1');
		$thirtys_total_work_sd=$this->input->post('thirtys_total_w_sd1');
		$endurance_mean=$this->input->post('endurance_mean1');
		$endurance_sd=$this->input->post('endurance_sd1');
		$lactate_w_mean=$this->input->post('lactate_w_mean1');
		$lactate_w_sd=$this->input->post('lactate_w_sd1');
		$lactate_mm_mean=$this->input->post('lactate_km_mean1');
		$lactate_mm_sd=$this->input->post('lactate_km_sd1');
		$grip_strength_mean=$this->input->post('grip_strength_mean1');
		$grip_strength_sd=$this->input->post('grip_strength_sd1');
		
		$insert_qry= "INSERT INTO sports_fitness_norms(sports,gender,vp_mean, vp_sd, flight_mean, flight_sd, peak_power_mean, peak_power_sd, thirtys_total_work_mean, thirtys_total_work_sd, endurance_mean, endurance_sd, lactate_w_mean, lactate_w_sd, lactate_km_mean, lactate_km_sd, grip_strength_mean,grip_strength_sd) VALUES ('".$sport_name."','".$gend."','".$vj_mean."','".$vj_sd."','".$flight_mean."','".$flight_sd."','".$peak_power_mean."','".$peak_power_sd."','".$thirtys_total_work_mean."','".$thirtys_total_work_sd."','".$endurance_mean."','".$endurance_sd."','".$lactate_w_mean."','".$lactate_w_sd."','".$lactate_mm_mean."','".$lactate_mm_sd."','".$grip_strength_mean."','".$grip_strength_sd."')" ;
	
		$res=$this->db->query($insert_qry);	

        //echo $this->db->last_query();		die;
			
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
	}
	
	
	public function update_bloodnormlist()
	 {
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sid=$this->input->post('sid');
		$meansbp=$this->input->post('meansbp');
		$sdsbp=$this->input->post('sdsbp');
		$gend=$this->input->post('gend');
		$meandbp=$this->input->post('meandbp');
		$sddbp=$this->input->post('sddbp');
		$meancols=$this->input->post('meancols');
		$sdcols=$this->input->post('sdcols');
		$meanlnhdl=$this->input->post('meanlnhdl');
		$sdlnhdl=$this->input->post('sdlnhdl');
		$meanlntrig=$this->input->post('meanlntrig');
		$sdlntrig=$this->input->post('sdlntrig');
		$meanldl=$this->input->post('meanldl');
		$sdldl=$this->input->post('sdldl');
		$meanlnglu=$this->input->post('meanlnglu');
		$sdlnglu=$this->input->post('sdlnglu');
	
		
		$updateqry="update blood_norms_all_ctry set mean_sbp='".$meansbp."' , sd_sbp='".$sdsbp."' , mean_dbp='".$meandbp."'
            ,sd_dbp='".$sddbp."',
			mean_cols='".$meancols."',sd_cols='".$sdcols."',mean_ln_hdl='".$meanlnhdl."',sd_ln_hdl='".$sdlnhdl."',	mean_ln_trig='".$meanlntrig."',
			sd_ln_trig='".$sdlntrig."',mean_ldl='".$meanldl."',sd_ldl='".$sdldl."',
			mean_ln_glu='".$meanlnglu."',sd_ln_glu='".$sdlnglu."' where id = '".$sid."' "; 	
				
		$res=$this->db->query($updateqry);		
			
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
			
	}
	
	
	
	
	
	
	public function get_fitnessnormlist()
	{
		$getnorm=$this->db->query("SELECT * from fitness_norm");
		$row = $getnorm->result();
        return $row;
	}
	
	
	public function update_fitnessnormlist()
	{
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sid=$this->input->post('sid');
		$MeanVJ=$this->input->post('MeanVJ');
		$sdVJ=$this->input->post('sdVJ');
		$MeanFTCT=$this->input->post('MeanFTCT');
		$SDFTCT=$this->input->post('SDFTCT');
		$MeanPPWKg=$this->input->post('MeanPPWKg');
		$SDPPWKg=$this->input->post('SDPPWKg');
		$MeanTW=$this->input->post('MeanTW');
		$SDTW=$this->input->post('SDTW');
		$endurance_VO2max=$this->input->post('endurance_VO2max');
		$SD_endurance_VO2max=$this->input->post('SD_endurance_VO2max');
		$Lactate_threshold_w=$this->input->post('Lactate_threshold_w');
		$SD_Lactate_threshold_w=$this->input->post('SD_Lactate_threshold_w');
		$Lactate_threshold_km=$this->input->post('Lactate_threshold_km');
		$SD_Lactate_threshold_km=$this->input->post('SD_Lactate_threshold_km');
		$MeanGS=$this->input->post('MeanGS');
		$SDGS=$this->input->post('SDGS');
	
		
		$updateqry="update fitness_norm set MeanVJ='".$MeanVJ."' , sdVJ='".$sdVJ."' , MeanFTCT='".$MeanFTCT."'
            ,SDFTCT='".$SDFTCT."',
			MeanPPWKg='".$MeanPPWKg."',SDPPWKg='".$SDPPWKg."',MeanTW='".$MeanTW."',SDTW='".$SDTW."',	endurance_VO2max='".$endurance_VO2max."',
			SD_endurance_VO2max='".$SD_endurance_VO2max."',Lactate_threshold_w='".$Lactate_threshold_w."',SD_Lactate_threshold_w='".$SD_Lactate_threshold_w."',
			Lactate_threshold_km='".$Lactate_threshold_km."',SD_Lactate_threshold_km='".$SD_Lactate_threshold_km."' ,MeanGS='".$MeanGS."' ,SDGS='".$SDGS."'where id = '".$sid."' "; 		
		$res=$this->db->query($updateqry);		
		//echo $updateqry; die;		
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
			
	}
	
	
	public function get_bminormlist()
	{
		$getnorm=$this->db->query("SELECT * from bmi_norm");
		$row = $getnorm->result();
        return $row;
	}
	
	public function update_bminormlist()
	{
		$postData=$this->input->post();
		//print_r($postData); die;
		
		$sid=$this->input->post('sid');
		$mean_log_mass=$this->input->post('mean_log_mass');
		$sd_log_mass=$this->input->post('sd_log_mass');
		$m_BMI=$this->input->post('m_BMI');
		$b_BMI=$this->input->post('b_BMI');
		$s_BMI=$this->input->post('s_BMI');
			
		
		$updateqry="update bmi_norm set mean_log_mass='".$mean_log_mass."' , sd_log_mass='".$sd_log_mass."' , m_BMI='".$m_BMI."'
            ,b_BMI='".$b_BMI."',
			s_BMI='".$s_BMI."' where id = '".$sid."' "; 		
		$res=$this->db->query($updateqry);		
        if($res)
		{
			return true;
		}			
		else{
			return false;
		}
	}
	
	
	
	public function get_bloodnormlist()
	{
		$getnorm=$this->db->query("SELECT * from blood_norms_all_ctry order by country");
		$row = $getnorm->result();
        return $row;
	}
	
	
	//Get data from
	public function sports_fitness_norms()
	{
		$getsportnorm=$this->db->query("SELECT * from sports_fitness_norms order by sports");
		$row = $getsportnorm->result();
        return $row;
	}
		
}
