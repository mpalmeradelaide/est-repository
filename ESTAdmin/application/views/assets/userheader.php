<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>air4casts - Dashboard</title>
<script src="<?php echo $this->config->item('path'); ?>/js/jquery-1.12.1.min.js"></script>
 
<script src="<?php echo $this->config->item('path'); ?>/js/bootstrap.min.js"></script>

<!-----Checkbox and radio Style------>
<link rel="stylesheet" href="<?php echo $this->config->item('path'); ?>/css/build.css"/>
<link rel="stylesheet" href="<?php echo $this->config->item('path'); ?>/css/awesome-bootstrap-checkbox.css"/>
<link href="<?php echo $this->config->item('path'); ?>/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<!-----/Checkbox and radio Style------>

<!-----Bootstrap and font-awesome CSS------>

<link rel="stylesheet" href="<?php echo $this->config->item('path'); ?>/css/font-awesome.min.css"/>
<!-----/Bootstrap and font-awesome CSS------>

<!-----Cusotm Layout css------>
<link rel="stylesheet" href="<?php echo $this->config->item('path'); ?>/css/layout.css"/>
<!-----/Cusotm Layout css------>

<!---Datepicker--->
<script src="<?php echo $this->config->item('path'); ?>/js/bootstrap-datepicker.js"></script>
<link href="<?php echo $this->config->item('path'); ?>/css/datepicker.css" rel="stylesheet">
<script src="<?php echo $this->config->item('path'); ?>/js/jquery.dataTables.min.js"></script>
<script src="<?php echo $this->config->item('path'); ?>/js/bootstrap-datatables.js"></script>
<script src="<?php echo $this->config->item('path'); ?>/js/dataTables-custom.js"></script>

</head> 
<body>
<div class="main">
<header>
  <div class="container">
    <div class="bgblue">
      <div class="col-lg-12">
        <div class="logo-inner col-sm-3 col-xs-12 nopadding"> <img src="<?php echo $this->config->item('path');?>/images/logo-main.png"> </div>
        <div class="col-sm-9 col-xs-12">
          <h2 class="pull-right text-white head_company_name"><?php echo $this->session->userdata('login_company');?></h2>
          <div class="clearfix"></div>
          <div style="clear:both;height:20px;"></div>
          <div class="col-lg-12 nopadding">
            <div class="col-lg-3 col-sm-0 col-xs-12"> </div>
            <div class="col-lg-3 col-sm-4 col-xs-12"> <a href="#" class="btn btn-border btn-block text-right"><i class="fa fa-user pull-left"></i> Support Centre</a> </div>
            <div class="col-lg-3 col-sm-4 col-xs-12"> <a href="#" class="btn btn-border btn-block text-right"><i class="fa fa-gear pull-left"></i> Manage Users</a> </div>
            <div class="col-lg-3 col-sm-4 col-xs-12"> <a href="#" class="btn btn-border btn-block text-right"><i class="fa fa-cloud pull-left"></i> Air4casts Blog</a> </div>
          </div>
        </div>
      </div>
      <div style="clear:both;height:30px;"></div>
      <div class="col-sm-12">
        <nav class="navbar navbar-default nav-custom fontbold">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
          </div>
          <div class="collapse navbar-collapse nopadding" id="myNavbar">
            <ul class="nav navbar-nav nopadding">
              <li><a href="#">Home</a></li>
              <li><a href="#">Airports 1500</a></li>
              <li><a href="#">Nationalities 1000</a></li>
              <li><a href="#">Routes 1000</a></li>
              <li><a href="#">Retail Reach</a></li>
              <li><a href="#">BrandWeb</a></li>
              <li><a href="#">Tourism</a></li>
              <li><a href="#">Apps</a></li>
            </ul>
          </div>
        </nav>
      </div>
    </div>
  </div>
</header>
