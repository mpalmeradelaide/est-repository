<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); ?>

<footer>
  <div class="container">
    <div class="bggrey">
        <div class="col-lg-9">
          <ul class="foot-links">
            <li><a href="#">Contact</a></li>
            <li><a href="#">About Us</a></li>
            <li><a href="#">Terms & Conditions</a></li>
          </ul>
        </div>
        <div class="inner-footer col-lg-3">
          <p class="text-right">Copyright 2016 | <a href="#">Air4casts</a></p>
        </div>
    </div>
  </div>
</footer>
</div>
<!---main---->

</body></html>