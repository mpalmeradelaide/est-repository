

<?php
session_start();
//print_r($bminormdata); die; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="<?php echo $this->config->item('path');?>/images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">

	$(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportNorm");     
        $("#exportnorm")[0].submit();     
    }); 
	  
	     function showModal(id)
		 {
			  //alert(id);
			 $('#sprtid').val(id);

			 
			 var ageid=$('#age'+id).html();               			$('#age').val(ageid);	
			 var gender=$('#gender'+id).html();            			$('#gen').val(gender);	
			 var mean_log_mass=$('#mean_log_mass'+id).html();       $('#mean_log_mass').val(mean_log_mass);			
			 var sd_log_mass=$('#sd_log_mass'+id).html();           $('#sd_log_mass').val(sd_log_mass);
			 var m_BMI=$('#m_BMI'+id).html();        			    $('#m_BMI').val(m_BMI);
			 var b_BMI=$('#b_BMI'+id).html();            			$('#b_BMI').val(b_BMI);
			 var s_BMI=$('#s_BMI'+id).html();      			        $('#s_BMI').val(s_BMI);
			 $( '#myModal' ).show();
		 }		 
		
		
		
		function updatedata()
		{
			 var normid= $('#sprtid').val();//alert(sportid);
			 var gen=	$('#gen').val();            
			 var mean_log_mass= $('#mean_log_mass').val();
			 var sd_log_mass=   $('#sd_log_mass').val();
			 var m_BMI= $('#m_BMI').val();
			 var b_BMI=  $('#b_BMI').val();
			 var s_BMI=  $('#s_BMI').val();
			
			
			$.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_path');?>user_management/updatebminorm",
                data: { sid:normid,gend:gen,mean_log_mass:mean_log_mass,				
					    sd_log_mass:sd_log_mass,m_BMI:m_BMI,					   
					    b_BMI:b_BMI , s_BMI:s_BMI },
				success: function (data){
					        //  alert(data);
					 		window.location='<?php echo $this->config->item('base_path');?>user_management/showbminormlist';
				}
            });
		}
		
        
	 
	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                    <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						 <input type="hidden" id="tablename" name="tablename" value="bmi_norm"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="<?php echo $this->config->item('path');?>/images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
				<!-- For User Listing-->
				 <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
            </div>

            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#"> Norm List</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
                        </li>
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
						 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
						 <input type="hidden" id="tablename" name="tablename" value="bmi_norm"/>
					</form-->
					
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>BMI and MASS Norm</h4>
                                    <p>BMI & MASS Norm Listing </p>
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Age</th>
                                                    <th>Gender</th>
                                                    <th>Mean Log Mass</th>
                                                    <th>SD Log Mass</th>
													<th>M BMI</th>
                                                    <th>B BMI</th>
                                                    <th>S BMI</th> 
													<th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($bminormdata as $key=>$val)
											{
										     ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="age<?php echo $val->id; ?>"><?php echo $val->age; ?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_log_mass<?php echo $val->id; ?>"><?php echo $val->mean_log_mass;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_log_mass<?php echo $val->id; ?>"><?php echo $val->sd_log_mass;?></span></td>
												  <td><span class="txt-dark weight-500" id="m_BMI<?php echo $val->id; ?>"><?php echo $val->m_BMI;?></span></td>
												  <td><span class="txt-dark weight-500" id="b_BMI<?php echo $val->id; ?>"><?php echo $val->b_BMI;?></span></td>
												  <td><span class="txt-dark weight-500" id="s_BMI<?php echo $val->id; ?>"><?php echo $val->s_BMI;?></span></td>
												  <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="showModal('<?php echo $val->id;?>');"><i class="fa fa-pencil-square-o  modalLink" aria-hidden="true" ></i></a></td> 
												  <input type="hidden" value="<?php echo $val->id;?>" name="sportid"></input>
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit BMI</h4>
											  </div>
											  <div class="modal-body">
											  	<div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="age" name="age" type="text" class="validate" readonly>
															<label for="age" class="active">Age</label>
														</div>
														<div class="input-field col s6">
															<input id="gen" name="gen" type="text" class="validate" readonly>
															<label for="gen" class="active">Gender</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mean_log_mass" name="mean_log_mass" type="text" class="validate">
															<label for="mean_log_mass" class="active">Mean Log Mass</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_log_mass" name="sd_log_mass" type="text" class="validate">
															<label for="sd_log_mass" class="active">SD Log Mass</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="m_BMI" name="m_BMI" type="text" class="validate">
															<label for="m_BMI" class="active">M BMI</label>
														</div>
														<div class="input-field col s6">
															<input id="b_BMI" name="b_BMI" type="text" class="validate">
															<label for="b_BMI" class="active">B BMI</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="s_BMI" name="s_BMI" type="text" class="validate">
															<label for="s_BMI" class="active">S_BMI</label>
														</div>
														
													</div>
												
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button"  id="submitform"  onclick="updatedata()" class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</div>

										  </div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>