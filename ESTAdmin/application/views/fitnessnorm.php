

<?php
session_start();
//print_r($fitnessnormdata); die; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="<?php echo $this->config->item('path');?>/images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">

	$(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportNorm");     
        $("#exportnorm")[0].submit();     
    }); 
	
	
	     function showModal(id)
		 {
			  //alert(id);
			 $('#sprtid').val(id);

			 
			 var ageid=$('#age'+id).html();               			 $('#age').val(ageid);	
			 var gender=$('#gender'+id).html();            			$('#gen').val(gender);	
			 var MeanVJ=$('#MeanVJ'+id).html();            			$('#MeanVJ').val(MeanVJ);			
			 var sdVJ=$('#sdVJ'+id).html();                			$('#sdVJ').val(sdVJ);
			 var MeanFTCT=$('#MeanFTCT'+id).html();        			$('#MeanFTCT').val(MeanFTCT);
			 var SDFTCT=$('#SDFTCT'+id).html();            			$('#SDFTCT').val(SDFTCT);
			 var MeanPPWKg=$('#MeanPPWKg'+id).html();      			$('#MeanPPWKg').val(MeanPPWKg);
			 var SDPPWKg=$('#SDPPWKg'+id).html();         			 $('#SDPPWKg').val(SDPPWKg);
			 var MeanTW=$('#MeanTW'+id).html();  		  			 $('#MeanTW').val(MeanTW);
			 var SDTW=$('#SDTW'+id).html();                			 $('#SDTW').val(SDTW);
			 var endurance_VO2max=$('#endurance_VO2max'+id).html();           $('#endurance_VO2max').val(endurance_VO2max);
			 var SD_endurance_VO2max=$('#SD_endurance_VO2max'+id).html();	   $('#SD_endurance_VO2max').val(SD_endurance_VO2max);
			 var Lactate_threshold_w=$('#Lactate_threshold_w'+id).html();	       $('#Lactate_threshold_w').val(Lactate_threshold_w);
			 var SD_Lactate_threshold_w=$('#SD_Lactate_threshold_w'+id).html();	           $('#SD_Lactate_threshold_w').val(SD_Lactate_threshold_w);
			 var Lactate_threshold_km=$('#Lactate_threshold_km'+id).html();  $('#Lactate_threshold_km').val(Lactate_threshold_km);
			 var SD_Lactate_threshold_km=$('#SD_Lactate_threshold_km'+id).html();	   $('#SD_Lactate_threshold_km').val(SD_Lactate_threshold_km);
			 var MeanGS=$('#MeanGS'+id).html();	  												 $('#MeanGS').val(MeanGS);
			 var SDGS=$('#SDGS'+id).html();	  													 $('#SDGS').val(SDGS);
            
			 $( '#myModal' ).show();
			 
		 }		 
		
		
		
		function updatedata()
		{
			 var normid= $('#sprtid').val();//alert(sportid);
			 var gen=	$('#gen').val();            
			 var MeanVJ= $('#MeanVJ').val();
			 var sdVJ=   $('#sdVJ').val();
			 var MeanFTCT= $('#MeanFTCT').val();
			 var SDFTCT=  $('#SDFTCT').val();
			 var MeanPPWKg=  $('#MeanPPWKg').val();
			 var SDPPWKg=  $('#SDPPWKg').val();
			 var MeanTW=  $('#MeanTW').val();
			 var SDTW=$('#SDTW').val();
			 var endurance_VO2max=        $('#endurance_VO2max').val();
			 var SD_endurance_VO2max=     $('#SD_endurance_VO2max').val();
			 var Lactate_threshold_w=     $('#Lactate_threshold_w').val();
			 var SD_Lactate_threshold_w=  $('#SD_Lactate_threshold_w').val();
			 var Lactate_threshold_km=    $('#Lactate_threshold_km').val();
			 var SD_Lactate_threshold_km=  $('#SD_Lactate_threshold_km').val();
			 var MeanGS=  $('#MeanGS').val();
			 var SDGS=  $('#SDGS').val();
			
			
			
			$.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_path');?>user_management/updatefitnessnorm",
                data: { sid:normid,gend:gen,MeanVJ:MeanVJ,sdVJ:sdVJ,				
					    MeanFTCT:MeanFTCT,SDFTCT:SDFTCT,MeanPPWKg:MeanPPWKg,					   
					    SDPPWKg:SDPPWKg,MeanTW:MeanTW,SDTW:SDTW,						   
					    endurance_VO2max:endurance_VO2max,SD_endurance_VO2max:SD_endurance_VO2max,Lactate_threshold_w:Lactate_threshold_w,					   
					    SD_Lactate_threshold_w:SD_Lactate_threshold_w,Lactate_threshold_km:Lactate_threshold_km,SD_Lactate_threshold_km:SD_Lactate_threshold_km,MeanGS:MeanGS , SDGS:SDGS  },
				success: function (data){
					          //alert(data);
					 		window.location='<?php echo $this->config->item('base_path');?>user_management/showfitnessnormlist';
				}
            });
		}
		
        
	 
	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                     <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect lite_btn grey_btn f_left btn_red" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						 <input type="hidden" id="tablename" name="tablename" value="fitness_norm"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
				
				
				
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="<?php echo $this->config->item('path');?>/images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
            <!-- For User Listing-->
				 <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
			</div>

            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#">General Population Fitness Norms List</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
                        </li>
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
					 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
					 <input type="hidden" id="tablename" name="tablename" value="fitness_norm"/>
					</form-->
					
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>General Population Fitness Norms List</h4>
                                   
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Age</th>
                                                    <th>Gender</th>
                                                    <th>Mean VJ</th>
                                                    <th>SD VJ</th>
													<th>Mean FTCT</th>
                                                    <th>SD FTCT</th>
                                                    <th>Mean PPWKg</th>
                                                    <th>SD PPWKg</th>
                                                    <th>Mean TW</th>
													<th>SD TW</th>
                                                    <th>Endurance VO2max</th>
                                                    <th>SD Endurance VO2max</th>
                                                    <th>Lactate Threshold w</th>
                                                    <th>SD Lactate threshold w</th>
                                                    <th>Lactate threshold km</th>
                                                    <th>SD Lactate threshold km</th>
                                                    <th>MeanGS</th>
                                                    <th>SDGS</th>
													<th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($fitnessnormdata as $key=>$val)
											{
										     ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="age<?php echo $val->id; ?>"><?php echo $val->age; ?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="MeanVJ<?php echo $val->id; ?>"><?php echo $val->MeanVJ;?></span></td>
												  <td><span class="txt-dark weight-500" id="sdVJ<?php echo $val->id; ?>"><?php echo $val->sdVJ;?></span></td>
												  <td><span class="txt-dark weight-500" id="MeanFTCT<?php echo $val->id; ?>"><?php echo $val->MeanFTCT;?></span></td>
												  <td><span class="txt-dark weight-500" id="SDFTCT<?php echo $val->id; ?>"><?php echo $val->SDFTCT;?></span></td>
												  <td><span class="txt-dark weight-500" id="MeanPPWKg<?php echo $val->id; ?>"><?php echo $val->MeanPPWKg;?></span></td>
												  <td><span class="txt-dark weight-500" id="SDPPWKg<?php echo $val->id; ?>"><?php echo $val->SDPPWKg;?></span></td>
												  <td><span class="txt-dark weight-500" id="MeanTW<?php echo $val->id; ?>"><?php echo $val->MeanTW;?></span></td>
												  <td><span class="txt-dark weight-500" id="SDTW<?php echo $val->id; ?>"><?php echo $val->SDTW;?></span></td>
												  <td><span class="txt-dark weight-500" id="endurance_VO2max<?php echo $val->id; ?>"><?php echo $val->endurance_VO2max;?></span></td>
												  <td><span class="txt-dark weight-500" id="SD_endurance_VO2max<?php echo $val->id; ?>"><?php echo $val->SD_endurance_VO2max;?></span></td>
												  <td><span class="txt-dark weight-500" id="Lactate_threshold_w<?php echo $val->id; ?>"><?php echo $val->Lactate_threshold_w;?></span></td>
												  <td><span class="txt-dark weight-500" id="SD_Lactate_threshold_w<?php echo $val->id; ?>"><?php echo $val->SD_Lactate_threshold_w;?></span></td>
												  <td><span class="txt-dark weight-500" id="Lactate_threshold_km<?php echo $val->id; ?>"><?php echo $val->Lactate_threshold_km;?></span></td>
												  <td><span class="txt-dark weight-500" id="SD_Lactate_threshold_km<?php echo $val->id; ?>"><?php echo $val->SD_Lactate_threshold_km;?></span></td>
												  <td><span class="txt-dark weight-500" id="MeanGS<?php echo $val->id; ?>"><?php echo $val->MeanGS;?></span></td>
												  <td><span class="txt-dark weight-500" id="SDGS<?php echo $val->id; ?>"><?php echo $val->SDGS;?></span></td>
												  <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="showModal('<?php echo $val->id;?>');"><i class="fa fa-pencil-square-o  modalLink" aria-hidden="true" ></i></a></td> 
												  <input type="hidden" value="<?php echo $val->id;?>" name="sportid"></input>
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit Sport</h4>
											  </div>
											  <div class="modal-body">
											  	<div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="age" name="age" type="text" class="validate" readonly>
															<label for="age" class="active">Age</label>
														</div>
														<div class="input-field col s6">
															<input id="gen" name="gen" type="text" class="validate" readonly>
															<label for="gen" class="active">Gender</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="MeanVJ" name="MeanVJ" type="text" class="validate">
															<label for="MeanVJ" class="active">Mean VJ</label>
														</div>
														<div class="input-field col s6">
															<input id="sdVJ" name="sdVJ" type="text" class="validate">
															<label for="sdVJ" class="active">SD VJ</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="MeanFTCT" name="MeanFTCT" type="text" class="validate">
															<label for="MeanFTCT" class="active">Mean FTCT</label>
														</div>
														<div class="input-field col s6">
															<input id="SDFTCT" name="SDFTCT" type="text" class="validate">
															<label for="SDFTCT" class="active">SD FTCT</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="MeanPPWKg" name="MeanPPWKg" type="text" class="validate">
															<label for="MeanPPWKg" class="active">Mean PPWKg</label>
														</div>
														<div class="input-field col s6">
															<input id="SDPPWKg" name="SDPPWKg" type="text" class="validate">
															<label for="SDPPWKg" class="active">SD PPWKg</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="MeanTW" name="MeanTW" type="text" class="validate">
															<label for="MeanTW" class="active">Mean TW</label>
														</div>
														<div class="input-field col s6">
															<input id="SDTW" name="SDTW" type="text" class="validate">
															<label for="SDTW" class="active">SD TW</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="endurance_VO2max" name="endurance_VO2max" type="text" class="validate">
															<label for="endurance_VO2max" class="active">Endurance VO2max</label>
														</div>
														<div class="input-field col s6">
															<input id="SD_endurance_VO2max" name="SD_endurance_VO2max" type="text" class="validate">
															<label for="SD_endurance_VO2max" class="active">SD Endurance VO2max</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="Lactate_threshold_w" name="Lactate_threshold_w" type="text" class="validate">
															<label for="Lactate_threshold_w" class="active">Lactate Threshold W</label>
														</div>
														<div class="input-field col s6">
															<input id="SD_Lactate_threshold_w" name="SD_Lactate_threshold_w" type="text" class="validate">
															<label for="SD_Lactate_threshold_w" class="active">SD Lactate Threshold W</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="Lactate_threshold_km" name="Lactate_threshold_km" type="text" class="validate">
															<label for="Lactate_threshold_km" class="active">Lactate threshold km</label>
														</div>
													</div>													
													<div class="row">
														<div class="input-field col s6">
															<input id="SD_Lactate_threshold_km" name="SD_Lactate_threshold_km" type="text" class="validate">
															<label for="SD_Lactate_threshold_km" class="active">SD Lactate threshold km</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="MeanGS" name="MeanGS" type="text" class="validate">
															<label for="MeanGS" class="active">Mean GS</label>
														</div>
													</div>													
													<div class="row">
														<div class="input-field col s6">
															<input id="SDGS" name="SDGS" type="text" class="validate">
															<label for="SDGS" class="active">SD GS</label>
														</div>
													</div>
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button"  id="submitform"  onclick="updatedata()" class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</div>

										  </div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>