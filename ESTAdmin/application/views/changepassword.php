<!DOCTYPE html>

<?php

session_start();
/* include('connection.php');

$username="admin";
$password="admin";

if( isset($_POST['submit']) )
{
	$uname=$_POST['username'];
	$pwrd=$_POST['passwrd'];

	$_SESSION['uname']=$uname;
	$_SESSION['pswrd']=$pwrd;

	if( ($username == $uname) && ($password == $pwrd) )
	{
	  header('Location: panel.php');
	}
} */

?>






<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="<?php echo $this->config->item('path');?>/images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
	<script src="js/html5shiv.js"></script>
	<script src="js/respond.min.js"></script>
	<![endif]-->
</head>

<body>
    <div class="blog-login">
        <div class="blog-login-in">
            <form action="<?php echo $this->config->item('base_path');?>user_management/checkpassword" method="POST" name="form" id="form">
				<div class="logo">Exercise Science Toolkit</div>
				
				<div class="row">
                    <div class="input-field col s12">
                        <input id="uname" name="uname" type="text" class="validate">
                        <label for="uname">Enter Username</label>
                    </div>
                </div>
				
                <div class="row">
                    <div class="input-field col s12">
                        <input id="oldpassword" name="oldpassword" type="password" class="validate">
                        <label for="oldpassword">Old Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input id="newpasswrd" name="newpasswrd" type="password" class="validate">
                        <label for="newpasswrd">New Password</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s12">
                        <input type="submit" name="submit" id="submit" value="Submit" class="waves-effect waves-light btn-large btn-log-in">
                    </div>
                </div>
                <!--a href="forgot.html" class="for-pass">Forgot Password?</a-->
            </form>
        </div>
    </div>

    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
</body>
</html>