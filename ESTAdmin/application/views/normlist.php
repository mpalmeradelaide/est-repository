

<?php
session_start();
//print_r($normdata); die; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="<?php echo $this->config->item('path');?>/images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">

		
	 $(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportNorm");     
        $("#exportnorm")[0].submit();     
    }); 

	
	
	     function showModal(id)
		 {
			 // alert(id);
			 $('#sprtid').val(id);

			 
			 var ageid=$('#age'+id).html();                $('#age').val(ageid);	
			 var gender=$('#gender'+id).html();            $('#gen').val(gender);	
			 var meansbp=$('#meansbp'+id).html();          $('#meansbp').val(meansbp);			
			 var sd_sbp=$('#sd_sbp'+id).html();            $('#sd_sbp').val(sd_sbp);
			 var mean_dbp=$('#mean_dbp'+id).html();        $('#mean_dbp').val(mean_dbp);
			 var sd_dbp=$('#sd_dbp'+id).html();            $('#sd_dbp').val(sd_dbp);
			 var mean_cols=$('#mean_cols'+id).html();      $('#mean_cols').val(mean_cols);
			 var sd_cols=$('#sd_cols'+id).html();          $('#sd_cols').val(sd_cols);
			 var mean_ln_hdl=$('#mean_ln_hdl'+id).html();  $('#mean_ln_hdl').val(mean_ln_hdl);
			 var sd_ln_hdl=$('#sd_ln_hdl'+id).html();      $('#sd_ln_hdl').val(sd_ln_hdl);
			 var mean_ln_trig=$('#mean_ln_trig'+id).html();$('#mean_ln_trig').val(mean_ln_trig);
			 var sd_ln_trig=$('#sd_ln_trig'+id).html();	   $('#sd_ln_trig').val(sd_ln_trig);
			 var mean_ldl=$('#mean_ldl'+id).html();	       $('#mean_ldl').val(mean_ldl);
			 var sd_ldl=$('#sd_ldl'+id).html();	           $('#sd_ldl').val(sd_ldl);
			 var mean_ln_glu=$('#mean_ln_glu'+id).html();  $('#mean_ln_glu').val(mean_ln_glu);
			 var sd_ln_glu=$('#sd_ln_glu'+id).html();	   $('#sd_ln_glu').val(sd_ln_glu);
            
			 $( '#myModal' ).show();
			 
		 }		 
		
		
		
		function updatedata()
		{
			 var normid= $('#sprtid').val();//alert(sportid);
			 var gen=	$('#gen').val();            
			 var meansbp= $('#meansbp').val();
			 var sd_sbp=   $('#sd_sbp').val();
			 var mean_dbp= $('#mean_dbp').val();
			 var sd_dbp=  $('#sd_dbp').val();
			 var mean_cols=  $('#mean_cols').val();
			 var sd_cols=  $('#sd_cols').val();
			 var mean_ln_hdl=  $('#mean_ln_hdl').val();
			 var sd_ln_hdl=$('#sd_ln_hdl').val();
			 var mean_ln_trig=$('#mean_ln_trig').val();
			 var sd_ln_trig=$('#sd_ln_trig').val();
			 var mean_ldl=    $('#mean_ldl').val();
			 var sd_ldl=  $('#sd_ldl').val();
			 var mean_ln_glu=    $('#mean_ln_glu').val();
			 var sd_ln_glu=  $('#sd_ln_glu').val();
			
			
			
			$.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_path');?>user_management/updatenorm",
                data: { sid:normid,gend:gen,meansbp:meansbp,sdsbp:sd_sbp,				
					    meandbp:mean_dbp,sddbp:sd_dbp,meancols:mean_cols,					   
					    sdcols:sd_cols,meanlnhdl:mean_ln_hdl,sdlnhdl:sd_ln_hdl,						   
					    meanlntrig:mean_ln_trig,sdlntrig:sd_ln_trig,meanldl:mean_ldl,					   
					    sdldl:sd_ldl,meanlnglu:mean_ln_glu,sdlnglu:sd_ln_glu },
				success: function (data){
					          //alert(data);
					 		window.location='<?php echo $this->config->item('base_path');?>user_management/shownormlist';
				}
            });
		}
		
        
	 
	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                    <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect lite_btn grey_btn f_left btn_red" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						 <input type="hidden" id="tablename" name="tablename" value="blood_norm"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="<?php echo $this->config->item('path');?>/images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
				<!-- For User Listing-->
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
            </div>

            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#">Screening Blood Norms</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a>
                        </li>
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
					 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
					 <input type="hidden" id="tablename" name="tablename" value="blood_norm"/>
					</form-->
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>Screening Blood Norms</h4>
                                   
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Age</th>
                                                    <th>Gender</th>
                                                    <th>Mean SBP</th>
                                                    <th>SD SBP</th>
													<th>Mean DBP</th>
                                                    <th>SD DBP</th>
                                                    <th>Mean Colestrol</th>
                                                    <th>SD Colestrol </th>
                                                    <th>Meso LN HDL</th>
													<th>SD LN HDL</th>
                                                    <th>Mean LN  Trigricerities</th>
                                                    <th>SD LN Trigericides</th>
                                                    <th>Mean LDL</th>
                                                    <th>SD LDL</th>
                                                    <th>Mean LN Glucose</th>
                                                    <th>SD LN Glucose</th>
													<th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($normdata as $key=>$val)
											{
										     ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="age<?php echo $val->id; ?>"><?php echo $val->age; ?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="meansbp<?php echo $val->id; ?>"><?php echo $val->mean_sbp;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_sbp<?php echo $val->id; ?>"><?php echo $val->sd_sbp;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_dbp<?php echo $val->id; ?>"><?php echo $val->mean_dbp;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_dbp<?php echo $val->id; ?>"><?php echo $val->sd_dbp;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_cols<?php echo $val->id; ?>"><?php echo $val->mean_cols;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_cols<?php echo $val->id; ?>"><?php echo $val->sd_cols;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_ln_hdl<?php echo $val->id; ?>"><?php echo $val->mean_ln_hdl;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_ln_hdl<?php echo $val->id; ?>"><?php echo $val->sd_ln_hdl;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_ln_trig<?php echo $val->id; ?>"><?php echo $val->mean_ln_trig;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_ln_trig<?php echo $val->id; ?>"><?php echo $val->sd_ln_trig;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_ldl<?php echo $val->id; ?>"><?php echo $val->mean_ldl;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_ldl<?php echo $val->id; ?>"><?php echo $val->sd_ldl;?></span></td>
												  <td><span class="txt-dark weight-500" id="mean_ln_glu<?php echo $val->id; ?>"><?php echo $val->mean_ln_glu;?></span></td>
												  <td><span class="txt-dark weight-500" id="sd_ln_glu<?php echo $val->id; ?>"><?php echo $val->sd_ln_glu;?></span></td>
												  <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="showModal('<?php echo $val->id;?>');"><i class="fa fa-pencil-square-o  modalLink" aria-hidden="true" ></i></a></td> 
												  <input type="hidden" value="<?php echo $val->id;?>" name="sportid"></input>
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit Sport</h4>
											  </div>
											  <div class="modal-body">
											  	<div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="age" name="age" type="text" class="validate" readonly>
															<label for="age" class="active">Age</label>
														</div>
														<div class="input-field col s6">
															<input id="gen" name="gen" type="text" class="validate" readonly>
															<label for="gen" class="active">Gender</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="meansbp" name="meansbp" type="text" class="validate">
															<label for="meansbp" class="active">Mean SBP</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_sbp" name="sd_sbp" type="text" class="validate">
															<label for="sd_sbp" class="active">SD SBP</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mean_dbp" name="mean_dbp" type="text" class="validate">
															<label for="mean_dbp" class="active">Mean DBP</label>
														</div>
														
														<div class="input-field col s6">
															<input id="sd_dbp" name="sd_dbp" type="text" class="validate">
															<label for="sd_dbp" class="active">SD DBP</label>
														</div>
														
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mean_cols" name="mean_cols" type="text" class="validate">
															<label for="mean_cols" class="active">Mean Colestrol</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_cols" name="sd_cols" type="text" class="validate">
															<label for="sd_cols" class="active">SD Colestrol</label>
														</div>
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mean_ln_hdl" name="mean_ln_hdl" type="text" class="validate">
															<label for="mean_ln_hdl" class="active">Mean HDL</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_ln_hdl" name="sd_ln_hdl" type="text" class="validate">
															<label for="sd_ln_hdl" class="active">SD HDL</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="mean_ln_trig" name="mean_ln_trig" type="text" class="validate">
															<label for="mean_ln_trig" class="active">Mean Trigericides</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_ln_trig" name="sd_ln_trig" type="text" class="validate">
															<label for="sd_ln_trig" class="active">SD Trigericides</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="mean_ldl" name="mean_ldl" type="text" class="validate">
															<label for="mean_ldl" class="active">Mean LDL</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_ldl" name="sd_ldl" type="text" class="validate">
															<label for="sd_ldl" class="active">SD LDL</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="mean_ln_glu" name="mean_ln_glu" type="text" class="validate">
															<label for="mean_ln_glu" class="active">Mean Glucose</label>
														</div>
														<div class="input-field col s6">
															<input id="sd_ln_glu" name="sd_ln_glu" type="text" class="validate">
															<label for="sd_ln_glu" class="active">SD Glucose</label>
														</div>
													</div>
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button"  id="submitform"  onclick="updatedata()" class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</div>

										  </div>
										</div>
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>