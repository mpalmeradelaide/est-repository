

<?php
session_start();

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">
	
	 $(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportUsers");     
        $("#exportnorm")[0].submit();     
    }); 
	
	
	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                   <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect lite_btn grey_btn f_left btn_red" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						  <input type="hidden" id="tablename" name="tablename" value="client_person_info"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
			<!-- For User Listing-->
				 <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users" class="menu-active"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
		   </div>
			
            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#"> User List</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a></li>
						
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
					 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
					 <input type="hidden" id="tablename" name="tablename" value="sport_match"/>
					</form-->
					
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>User List</h4>
                                    <p>User Listing </p>
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
													
													<th>Name</th>
													<th>Project</th>
													<th>Access Code</th>
													<th>Gender</th>
													<th>Age</th>
													<th>Mass</th>
													<th>Height</th>
													<th>BMI</th>
													<th>Subpopulation</th>
													<th>Country</th>
													<th>Occupation</th>
													<th>heart_condition</th>
													<th>Pain in chest</th>
													<th>feel_faint</th>
													<th>asthma_attack</th>
													<th>diabetes</th>
													<th>bone_muscle_prob</th>
													<th>other_medical_cond</th>
													<th>walking</th>
													<th>minutes_walking</th>
													<th>vigorous</th>
													<th>minute_vigorous</th>
													<th>moderate</th>
													<th>minute_moderate</th>
													<th>tv_hours_games</th>
													<th>driving_hours</th>
													<th>is_setting_long_period</th>
													<th>family_history</th>
													<th>family_gender</th>
													<th>age_at_heart_attack</th>
													<th>current_smoker</th>
													<th>how_many</th>
													<th>quit_smoke_recently</th>
													<th>how_many_recently</th>
													<th>self_report_BP</th>
													<th>self_report_Chol</th>
													<th>self_report_Glucose</th>
													<th>Measured_SBP</th>
													<th>Measured_DBP</th>
													<th>Measured_fast_blood_glucose</th>
													<th>Measured_blood_Cholesterol</th>
													<th>HDL</th>
													<th>LDL</th>
													<th>triglycerides</th>
													<th>medication_selected</th>
													<th>medical_cond_selected</th>
													<th>Vertical jump (cm)</th>
													<th>FTCT</th>
													<th>waist_hip_ratio</th>
													<th>Triceps skinfold</th>
													<th>Biceps Skinfold</th>
													<th>Subscapular Skinfold</th>
													<th>Sum of 3 skinfolds</th>
													<th>self_report_height</th>
													<th>self_report_weight</th>
													<th>Muscle and joint sites</th>
													<th>Muscle and joint sites option</th>
													<th>Grip strength L</th>
													<th>R_Grip</th>
													<th>Bench_press_kg</th>
													<th>Bench_press_reps</th>
													<th>Arm_curl_kg</th>
													<th>Arm_curl_reps</th>
													<th>Lateral_pulldown_kg</th>
													<th>Lateral_pulldown_reps</th>
													<th>Leg_press_kg</th>
													<th>Leg_press_reps</th>
													<th>Leg_extension_kg</th>
													<th>Leg_extension_reps</th>
													<th>Leg_curl_kg</th>
													<th>Leg_curl_reps</th>
													<th>illiacrest</th>
													<th>supraspinale</th>
													<th>abdominal</th>
													<th>frontthigh</th>
													<th>Medialcalf</th>
													<th>midaxilla</th>
													<th>armrelaxed</th>
													<th>armflexed</th>
													<th>waistminimum</th>
													<th>Gluteal_hips</th>
													<th>Calf max (cm)</th>
													<th>humerus</th>
													<th>femur</th>
													<th>head</th>
													<th>neck</th>
													<th>forearmmax</th>
													<th>waist_distal_styloids</th>
													<th>chest_mesosternale</th>
													<th>thigh_onecmdistal</th>
													<th>thigh_mid</th>
													<th>ankle_minimum</th>
													<th>acromiale_radiale</th>
													<th>radiale_styl</th>
													<th>midstyl_dacty</th>
													<th>illiospinale</th>
													<th>trochanterion</th>
													<th>trochant_tibial</th>
													<th>tibiale_laterale</th>
													<th>tib_med_sphytib</th>
													<th>biacromial</th>
													<th>bideltoid</th>
													<th>biiliocristal</th>
													<th>bitrochanteric</th>
													<th>footlength</th>
													<th>sitting_length</th>
													<th>transverse_chest</th>
													<th>A_PChest</th>
													<th>arm_span</th>
													<th>Mean of %body fat</th>
													<th>Fat mass (kg)</th>
													<th>muscle mass (kg)</th>
													<th>bone mass (kg)</th>
													<th>residual mass (kg)</th>
													<th>ectomorph_score</th>
													<th>mesomorph_score</th>
													<th>endomorph_score</th>
													<th>vo2max</th>
													<th>Continuous treadmill VO2max</th>
													<th>Discontinous treadmill VO2max</th>
													<th>Ergometer_Continous_vo2max</th>
													<th>total work done in 30s (kJ)</th>
													<th>shuttle_level</th>
													<th>Peak power (W)</th>
													<th>Peak power (W/kg)</th>
													<th>Shuttle test VO2max</th>
													<th>Pred watts at HR max</th>
													<th>Submax test VO2max (HR)</th>
													<th>Pred watts at RPE max</th>
													<th>Submax test VO2max (RPE)</th>
													<th>sd_Body_Fat</th>
													 </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($userdata as $key=>$val)
											{
										      
											 ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="sport<?php echo $val->id; ?>"><?php echo $val->Name; ?></span></td>
												  <td><span class="txt-dark weight-500" id="sport<?php echo $val->id; ?>"><?php echo $val->project; ?></span></td>
												  <td><span class="txt-dark weight-500" id="sport<?php echo $val->id; ?>"><?php echo $val->access_code; ?></span></td>
												  <td><span class="txt-dark weight-500" id="sporttype<?php echo $val->id; ?>"><?php echo $val->Gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="sporttype<?php echo $val->id; ?>"><?php echo $val->Age;?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->Mass;?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->Height;?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->BMI;?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->Subpopulation;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodyfatper<?php echo $val->id; ?>"><?php echo $val->Country;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodyfatsd<?php echo $val->id; ?>"><?php echo $val->Occupation;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->heart_condition;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->pain_in_chaist ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->feel_faint;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->asthma_attack;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->diabetes ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->bone_muscle_prob ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->other_medical_cond;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->walking;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->minutes_walking;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->vigorous;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->minute_vigorous;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->moderate;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->minute_moderate;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->tv_hours_games;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->driving_hours;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->is_setting_long_period;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->family_history;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->family_gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->age_at_heart_attack;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->current_smoker;?></span></td>
												  
												  
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->how_many;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->quit_smoke_recently;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->how_many_recently;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->self_report_BP;?></span></td>
												  
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->self_report_Chol;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->self_report_Glucose;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Measured_SBP;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Measured_DBP;?></span></td>
												
												
												
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Measured_fast_blood_glucose;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Measured_blood_Cholesterol;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->HDL;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->LDL;?></span></td>
												  
												  
												  
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->triglycerides;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->medication_selected;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->medical_cond_selected;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->vj ;?></span></td>
												  
												  
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->FTCT;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->waist_hip_ratio;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->triceps ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->biceps ;?></span></td>
												
												
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->subscapular;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Sum_of_3_skinfolds ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->self_report_height ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->self_report_weight ;?></span></td>
												
												
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->MBJSites ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->MBJOption;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->L_grip;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->R_Grip ;?></span></td>
												  
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Bench_press_kg ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Bench_press_reps;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Arm_curl_kg;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Arm_curl_reps ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Lateral_pulldown_kg ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Lateral_pulldown_reps;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_press_kg;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_press_reps ;?></span></td>
												  
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_extension_kg ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_extension_reps;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_curl_kg;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Leg_curl_reps ;?></span></td>
												  
												    
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->illiacrest ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->supraspinale;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->abdominal;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->frontthigh ;?></span></td>
												
												
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Medialcalf ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->midaxilla;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->armrelaxed;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->armflexed ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->waistminimum ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Gluteal_hips;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->calfmaximum ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->humerus ;?></span></td>
												
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->femur ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->head;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->neck;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->forearmmax ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->waist_distal_styloids ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->chest_mesosternale;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->thigh_onecmdistal ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->thigh_mid ;?></span></td>
												
												
												
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->ankle_minimum ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->acromiale_radiale;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->radiale_styl;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->midstyl_dacty ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->illiospinale ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->trochanterion;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->trochant_tibial ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->tibiale_laterale ;?></span></td>
												  
												  
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->tib_med_sphytib ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->biacromial;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->bideltoid;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->biiliocristal ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->bitrochanteric ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->footlength;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->sitting_length ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->transverse_chest ;?></span></td>
												  
												  
												<td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->A_PChest ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->arm_span;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->mean_Body_Fat ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->fat_mass ;?></span></td>
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->muscle_mass  ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->bone_mass ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->residual_mass  ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->ectomorph_score ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->mesomorph_score ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->endomorph_score;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->vo2max ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Continous_vo2max_treadmill  ;?></span></td>
												   <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Discontinous_vo2max_distreadmill   ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Ergometer_Continous_vo2max ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->total_work_done_in30s_kJ   ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->shuttle_level ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->PPW_val;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->PPWKg;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->shuttle_VO2max ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Watts_HRmax ;?></span></td>
												  
												  
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Predicted_VO2_submax_test  ;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->Max_Power_RPE;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->VO2max_RPE;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->sd_Body_Fat   ;?></span></td>
												   
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
									
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>