

<?php
session_start();
//print_r($sports_fitnessnorms); die; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="<?php echo $this->config->item('path');?>/images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">

		
	 $(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportNorm");     
        $("#exportnorm")[0].submit();     
    }); 

	
	
	     function showModal(id)
		 {
			 // alert(id);
			 $('#sprtid').val(id);

			 
			 var sports=$('#sports'+id).html();          
			 $('#sports').val(sports);	
			 var gender=$('#gender'+id).html();           
			 $('#gen').val(gender);	
			 var vjmean=$('#vp_mean'+id).html();        
			 $('#vjmean').val(vjmean);	
			 var vjsd=$('#vp_sd'+id).html();               
			 $('#vjsd').val(vjsd);			
			 var flight_mean=$('#flight_mean'+id).html();  
			 $('#flight_mean').val(flight_mean);
			 var flight_sd=$('#flight_sd'+id).html();        
			 $('#flight_sd').val(flight_sd);
			 var peak_power_mean=$('#peak_power_mean'+id).html();
			 $('#peak_power_mean').val(peak_power_mean);
			 var peak_power_sd=$('#peak_power_sd'+id).html(); 
			 $('#peak_power_sd').val(peak_power_sd);
			 var thirtys_total_work_mean=$('#thirtys_total_work_mean'+id).html();
			 $('#thirtys_total_w_Mean').val(thirtys_total_work_mean);
			 var thirtys_total_work_sd=$('#thirtys_total_work_sd'+id).html();
			 $('#thirtys_total_w_sd').val(thirtys_total_work_sd);
			 var endurance_mean=$('#endurance_mean'+id).html(); 
			 $('#endurance_mean').val(endurance_mean);
			 var endurance_sd=$('#endurance_sd'+id).html();
			 $('#endurance_sd').val(endurance_sd);
			 var lactate_w_mean=$('#lactate_w_mean'+id).html();
			 $('#lactate_w_mean').val(lactate_w_mean);
			 var lactate_w_sd=$('#lactate_w_sd'+id).html();
			 $('#lactate_w_sd').val(lactate_w_sd);
			 var lactate_km_mean=$('#lactate_km_mean'+id).html();
			 $('#lactate_km_mean').val(lactate_km_mean);
			 var lactate_km_sd=$('#lactate_km_sd'+id).html();
			 $('#lactate_km_sd').val(lactate_km_sd);
			 var grip_strength_mean=$('#grip_strength_mean'+id).html();
			 $('#grip_strength_mean').val(grip_strength_mean);
             var grip_strength_sd=$('#grip_strength_sd'+id).html();
			 $('#grip_strength_sd').val(grip_strength_sd);
			 $( '#myModal' ).show();
			 
		 }		 
		
		
		//Update Data 
		function updatedata()
		{
			
		    var normid= $('#sprtid').val();
			 
			 var sports=$('#sports').val(); 
			 
			 var gen=	$('#gen').val();
			
			var vjmean=$('#vjmean').val();	
			 
			 var vjsd= $('#vjsd').val();			
			
			 var flight_mean=$('#flight_mean').val();
			 
			 var flight_sd=$('#flight_sd').val();
			 
			 var peak_power_mean=$('#peak_power_mean').val();
			 
			 var peak_power_sd= $('#peak_power_sd').val();
			
			 var thirtys_total_work_mean=$('#thirtys_total_w_Mean').val();
			 
			 var thirtys_total_work_sd=$('#thirtys_total_w_sd').val();
			 
			 var endurance_mean=$('#endurance_mean').val();
			 
			 var endurance_sd=$('#endurance_sd').val();
			 
			 var lactate_w_mean=$('#lactate_w_mean').val();
			 
			 var lactate_w_sd=$('#lactate_w_sd').val();
			 
			 var lactate_mm_mean=$('#lactate_km_mean').val();
			 
			 var lactate_mm_sd=$('#lactate_km_sd').val();
			 
			 var grip_strength_mean=$('#grip_strength_mean').val();
			 
             var grip_strength_sd=$('#grip_strength_sd').val();
			 
	
			$.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_path');?>user_management/update_sports_fitness_norm",
                data: { sid:normid,gend:gen,sports:sports,vjmean:vjmean,vjsd:vjsd,				
					    flight_mean:flight_mean,flight_sd:flight_sd,peak_power_mean:peak_power_mean,
						peak_power_sd:peak_power_sd,thirtys_total_work_mean:thirtys_total_work_mean,
						thirtys_total_work_sd:thirtys_total_work_sd,	
					    endurance_mean:endurance_mean,endurance_sd:endurance_sd,lactate_w_mean:lactate_w_mean,						   
					    lactate_w_sd:lactate_w_sd,lactate_mm_mean:lactate_mm_mean,lactate_mm_sd:lactate_mm_sd,					   
					    grip_strength_mean:grip_strength_mean,grip_strength_sd:grip_strength_sd},
				success: function (data){
					          console.log(data);
					 	window.location='<?php echo $this->config->item('base_path');?>user_management/fitness_norms';
				}
            });
		}

	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                    <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect lite_btn grey_btn f_left btn_red" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						 <input type="hidden" id="tablename" name="tablename" value="blood_norms_all_ctry"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="<?php echo $this->config->item('path');?>/images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
				<!-- For User Listing-->
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
				
            </div>

            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#">Fitness Norms</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a></li>
						<li class="page-back"><a class="" href="#" data-toggle="modal" data-target="#myModal1" >Add Fitness Norms</a></li>
                       
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
					 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
					 <input type="hidden" id="tablename" name="tablename" value="blood_norm"/>
					</form-->
					
					<!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Add Blood Norm" id="add" style="margin-top:32px;"   onclick="showModal('<?php //echo $val->id;?>');"/-->
				
					
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>Fitness Norms</h4>
                                    
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Sports</th>
                                                    <th>Gender</th>
                                                    <th>VJ Mean</th>
													<th>VJ SD</th>
                                                    <th>Flight Mean</th>
													<th>Flight SD</th>
                                                    <th>Peak power Mean</th>
                                                    <th>Peak power SD</th>
                                                    <th>30s total work Mean </th>
                                                    <th>30s total work SD</th>
													<th>Endurance Mean</th>
                                                    <th>Endurance SD</th>
                                                    <th>Lactate w Mean</th>
                                                    <th>Lactate w SD</th>
                                                    <th>Lactate mM Mean</th>
                                                    <th>Lactate mM SD</th>
                                                    <th>Grip strength Mean</th>
													<th>Grip strength SD</th>
													<th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($sports_fitnessnorms as $key=>$val)
											{
										     ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="sports<?php echo $val->id; ?>"><?php echo $val->sports; ?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="vp_mean<?php echo $val->id; ?>"><?php echo $val->vp_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="vp_sd<?php echo $val->id; ?>"><?php echo $val->vp_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="flight_mean<?php echo $val->id; ?>"><?php echo $val->flight_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="flight_sd<?php echo $val->id; ?>"><?php echo $val->flight_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="peak_power_mean<?php echo $val->id; ?>"><?php echo $val->peak_power_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="peak_power_sd<?php echo $val->id; ?>"><?php echo $val->peak_power_sd;?></span></td>
										<td><span class="txt-dark weight-500" id="thirtys_total_work_mean<?php echo $val->id; ?>"><?php echo $val->thirtys_total_work_mean;?></span></td>
											<td><span class="txt-dark weight-500" id="thirtys_total_work_sd<?php echo $val->id; ?>"><?php echo $val->thirtys_total_work_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="endurance_mean<?php echo $val->id; ?>"><?php echo $val->endurance_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="endurance_sd<?php echo $val->id; ?>"><?php echo $val->endurance_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="lactate_w_mean<?php echo $val->id; ?>"><?php echo $val->lactate_w_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="lactate_w_sd<?php echo $val->id; ?>"><?php echo $val->lactate_w_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="lactate_km_mean<?php echo $val->id; ?>"><?php echo $val->lactate_km_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="lactate_km_sd<?php echo $val->id; ?>"><?php echo $val->lactate_km_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="grip_strength_mean<?php echo $val->id; ?>"><?php echo $val->grip_strength_mean;?></span></td>
												  <td><span class="txt-dark weight-500" id="grip_strength_sd<?php echo $val->id; ?>"><?php echo $val->grip_strength_sd;?></span></td>
												  <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="showModal('<?php echo $val->id;?>');"><i class="fa fa-pencil-square-o  modalLink" aria-hidden="true" ></i></a></td> 
												  <input type="hidden" value="<?php echo $val->id;?>" name="sportid"></input>
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit Fitness Norms</h4>
											  </div>
											  <div class="modal-body">
											  	<div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="sports" name="sports" type="text" class="validate" readonly>
															<label for="sports" class="active">sports</label>
														</div>
														<div class="input-field col s6">
															<input id="gen" name="gen" type="text" class="validate" readonly>
															<label for="gen" class="active">Gender</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="vjmean" name="vjmean" type="text" class="validate">
															<label for="vjmean" class="active">VJ Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="vjsd" name="vjsd" type="text" class="validate">
															<label for="vjsd" class="active">VJ SD</label>
														</div>
														
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="flight_mean" name="flight_mean" type="text" class="validate">
															<label for="flight_mean" class="active">Flight Mean</label>
														</div>
														
														<div class="input-field col s6">
															<input id="flight_sd" name="flight_sd" type="text" class="validate">
															<label for="flight_sd" class="active">Flight SD</label>
														</div>
														
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="peak_power_mean" name="peak_power_mean" type="text" class="validate">
															<label for="peak_power_mean" class="active">Peak Power Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="peak_power_sd" name="peak_power_sd" type="text" class="validate">
															<label for="peak_power_sd" class="active">Peak Power SD</label>
														</div>
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="thirtys_total_w_Mean" name="thirtys_total_w_Mean" type="text" class="validate">
															<label for="thirtys_total_w_Mean" class="active">30S Total Work Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="thirtys_total_w_sd" name="thirtys_total_w_sd" type="text" class="validate">
															<label for="thirtys_total_w_sd" class="active">30S Total Work SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="endurance_mean" name="endurance_mean" type="text" class="validate">
															<label for="endurance_mean" class="active">Endurance Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="endurance_sd" name="endurance_sd" type="text" class="validate">
															<label for="endurance_sd" class="active">Endurance SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="lactate_w_mean" name="lactate_w_mean" type="text" class="validate">
															<label for="lactate_w_mean" class="active">Lactate w Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="lactate_w_sd" name="lactate_w_sd" type="text" class="validate">
															<label for="lactate_w_sd" class="active">Lactate w SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="lactate_km_mean" name="lactate_km_mean" type="text" class="validate">
															<label for="lactate_km_mean" class="active">Lactate mM Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="lactate_km_sd" name="lactate_km_sd" type="text" class="validate">
															<label for="lactate_km_sd" class="active">Lactate mM SD</label>
														</div>
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="grip_strength_mean" name="grip_strength_mean" type="text" class="validate">
															<label for="grip_strength_mean" class="active">Grip Strength Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="grip_strength_sd" name="lactate_km_sd" type="text" class="validate">
															<label for="grip_strength_sd" class="active">Grip Strength SD</label>
														</div>
													</div>
													
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button"  id="submitform"  onclick="updatedata()" class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</div>

										  </div>
										</div>
										
																				
										
										
									<!-- Add Modal -->	
										<div id="myModal1" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Add Fitness Norms</h4>
											  </div>
											  <form name="add_fitness" action="<?php echo $this->config->item('base_path');?>user_management/addsportsfitness" method="POST">
											  <div class="modal-body">
											  	<div class="row">
												        <div class="input-field col s6">
															<input id="sports1" name="sports1" type="text" class="validate">
															<label for="sports" class="active">sports</label>
														</div>
														<div class="input-field col s6">
															<input id="gen1" name="gen1" type="text" class="validate">
															<label for="gen1" class="active">Gender</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="vjmean1" name="vjmean1" type="text" class="validate">
															<label for="vjmean1" class="active">VJ Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="vjsd1" name="vjsd1" type="text" class="validate">
															<label for="vjsd1" class="active">VJ SD</label>
														</div>
														
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="flight_mean1" name="flight_mean1" type="text" class="validate">
															<label for="flight_mean1" class="active">Flight Mean</label>
														</div>
														
														<div class="input-field col s6">
															<input id="flight_sd1" name="flight_sd1" type="text" class="validate">
															<label for="flight_sd1" class="active">Flight SD</label>
														</div>
														
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="peak_power_mean1" name="peak_power_mean1" type="text" class="validate">
															<label for="peak_power_mean1" class="active">Peak Power Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="peak_power_sd1" name="peak_power_sd1" type="text" class="validate">
															<label for="peak_power_sd1" class="active">Peak Power SD</label>
														</div>
													
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="thirtys_total_w_Mean1" name="thirtys_total_w_Mean1" type="text" class="validate">
															<label for="thirtys_total_w_Mean1" class="active">30S Total Work Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="thirtys_total_w_sd1" name="thirtys_total_w_sd1" type="text" class="validate">
															<label for="thirtys_total_w_sd1" class="active">30S Total Work SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="endurance_mean1" name="endurance_mean1" type="text" class="validate">
															<label for="endurance_mean1" class="active">Endurance Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="endurance_sd1" name="endurance_sd1" type="text" class="validate">
															<label for="endurance_sd1" class="active">Endurance SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="lactate_w_mean1" name="lactate_w_mean1" type="text" class="validate">
															<label for="lactate_w_mean1" class="active">Lactate w Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="lactate_w_sd1" name="lactate_w_sd1" type="text" class="validate">
															<label for="lactate_w_sd1" class="active">Lactate w SD</label>
														</div>
														
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="lactate_km_mean1" name="lactate_km_mean1" type="text" class="validate">
															<label for="lactate_km_mean1" class="active">Lactate mM Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="lactate_km_sd1" name="lactate_km_sd1" type="text" class="validate">
															<label for="lactate_km_sd1" class="active">Lactate mM SD</label>
														</div>
													</div>
													<div class="row">
													<div class="input-field col s6">
															<input id="grip_strength_mean1" name="grip_strength_mean1" type="text" class="validate">
															<label for="grip_strength_mean1" class="active">Grip Strength Mean</label>
														</div>
														<div class="input-field col s6">
															<input id="grip_strength_sd1" name="grip_strength_sd1" type="text" class="validate">
															<label for="grip_strength_sd1" class="active">Grip Strength SD</label>
														</div>
													</div>
													
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="submit"  id="submitform"   class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</form>
											</div>

										  </div>
										</div>
										
									
										
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>