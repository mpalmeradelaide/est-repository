

<?php
session_start();

//print_r($sportdata); die; 
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <title>Exercise Science Toolkit</title>
    <!--== META TAGS ==-->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!--== FAV ICON ==-->
    <link rel="shortcut icon" href="images/fav.ico">

    <!-- GOOGLE FONTS -->
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,600,700" rel="stylesheet">

    <!-- FONT-AWESOME ICON CSS -->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/font-awesome.min.css">

    <!--== ALL CSS FILES ==-->
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/mob.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo $this->config->item('path');?>/css/materialize.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap.min.css" />

    <script src="https://code.jquery.com/jquery-1.11.1.min.js"></script>
  
	
	<script type="text/javascript">
	
	 $(document).on('click','#export', function(){
		$("#exportnorm").attr("action", "<?php echo base_url(); ?>/index.php/user_management/exportNorm");     
        $("#exportnorm")[0].submit();     
    }); 
	
	
	
	
	

	     function showModal(id)
		 {
			  //alert(id);
			 $('#sprtid').val(id);

			 
			 var sportid="sport"+id;  $('#sportid').val(sportid);			 
			 var sportname=$('#sport'+id).html(); 	   $('#sport_name').val(sportname);            
			 var sporttype=$('#sporttype'+id).html();  $('#sport_type').val(sporttype);
			 var gen=$('#gender'+id).html();           $('#gen').val(gen);
			 var bfper=$('#bodyfatper'+id).html();     $('#body_fatper').val(bfper);
			 var bfsd=$('#bodyfatsd'+id).html();       $('#body_fatsd').val(bfsd);
			 var endo=$('#endomorph'+id).html();       $('#endomorph').val(endo);
			 var meso=$('#mesomorph'+id).html();       $('#mesomorph').val(meso);
			 var ecto=$('#ectomorph'+id).html();       $('#ectomorph').val(ecto);
			 var endosd=$('#endosd'+id).html();        $('#endosd').val(endosd);
			 var mesosd=$('#mesosd'+id).html();        $('#mesosd').val(mesosd);
			 var ectosd=$('#ectosd'+id).html();        $('#ectosd').val(ectosd);
			 var ht=$('#height'+id).html();	           $('#height').val(ht);
			 var htsd=$('#heightsd'+id).html();	       $('#heightsd').val(htsd);
			 var bm=$('#bodymass'+id).html();	       $('#bodymass').val(bm);
			 var bmsd=$('#bodymasssd'+id).html();	   $('#bodymasssd').val(bmsd);
            
			 $( '#myModal' ).show();
			 
		 }		 
		
		
		
		function updatedata()
		{
			 var sportid= $('#sprtid').val();//alert(sportid);
			 var sportname=	$('#sport_name').val();            
			 var sporttype= $('#sport_type').val();
			 var gen=   $('#gen').val();
			 var bfper= $('#body_fatper').val();
			 var bfsd=  $('#body_fatsd').val();
			 var endo=  $('#endomorph').val();
			 var meso=  $('#mesomorph').val();
			 var ecto=  $('#ectomorph').val();
			 var endosd=$('#endosd').val();
			 var mesosd=$('#mesosd').val();
			 var ectosd=$('#ectosd').val();
			 var ht=    $('#height').val();
			 var htsd=  $('#heightsd').val();
			 var bm=    $('#bodymass').val();
			 var bmsd=  $('#bodymasssd').val();
			
			
			
			$.ajax({
                type: "POST",
                url: "<?php echo $this->config->item('base_path');?>user_management/updatesport",
                data: { sid:sportid,sport_name:sportname,sport_type:sporttype,gend:gen,
		                               bf_per:bfper,bf_sd:bfsd,endo_morph:endo,
								       meso_morph:meso,ecto_morph:ecto,endo_sd:endosd,	
								       meso_sd:mesosd,ecto_sd:ectosd,hght:ht,
		                               ht_sd:htsd,b_m:bm,bm_sd:bmsd },
				success: function (data){
					         // alert(data);
					 		window.location='<?php echo $this->config->item('base_path');?>user_management/showsportlist';
				}
            });
		}
		
        
	 
	</script>
	
</head>

<body>
    <!--== MAIN CONTRAINER ==-->
    <!--== MAIN CONTRAINER ==-->
    <div class="container-fluid sb1">
        <div class="row">
            <!--== LOGO ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 sb1-1">
                <a href="#" class="btn-close-menu"><i class="fa fa-times" aria-hidden="true"></i></a>
                <a href="#" class="atab-menu"><i class="fa fa-bars tab-menu" aria-hidden="true"></i></a>
                <a href="#" class="logo">Exercise Science Toolkit</a>
            </div>
            <!--== MY ACCCOUNT ==-->
            <div class="col-md-2 col-sm-3 col-xs-6 pull-right">
                <!-- Dropdown Trigger -->
                <a class='waves-effect dropdown-button top-user-pro' href='#' data-activates='top-menu'><img src="<?php echo $this->config->item('path');?>/images/user/6.png" alt="" /><?php echo $_SESSION['uname']; ?>  <i class="fa fa-angle-down" aria-hidden="true"></i>
                </a>

                <!-- Dropdown Structure -->
                <ul id='top-menu' class='dropdown-content top-menu-sty'>
                   <li><!--a href="#" class="waves-effect"><i class="fa fa-undo" aria-hidden="true"></i> Backup Data</a-->
					
					<form id="exportnorm" name="exportnorm" action="" method="post">
						 <!--input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/-->
						 <a href="#" class="waves-effect lite_btn grey_btn f_left btn_red" id="export"><i class="fa fa-undo" aria-hidden="true" ></i> Backup Data</a>
						  <input type="hidden" id="tablename" name="tablename" value="sport_match"/>
					</form>
					
                    </li>
                    <li class="divider"></li>
                    <li><a href="<?php echo $this->config->item('base_path');?>user_management/logout" class="ho-dr-con-last waves-effect"><i class="fa fa-sign-in" aria-hidden="true"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>

    <!--== BODY CONTNAINER ==-->
    <div class="container-fluid sb2">
        <div class="row">
            <div class="sb2-1">
                <!--== USER INFO ==-->
                <div class="sb2-12">
                    <ul>
                        <li><img src="images/placeholder.jpg" alt="">
                        </li>
                        <li>
                            <h5>Kevin Norton <span> Australia</span></h5>
                        </li>
                        <li></li>
                    </ul>
                </div>
                <!--== LEFT MENU ==-->
                <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-bar-chart" aria-hidden="true"></i> Dashboard</a></li>
                    </ul>
                </div>
			<!-- For User Listing-->
				 <div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/users"><i class="fa fa-bar-chart" aria-hidden="true"></i> Users</a></li>
                    </ul>
                </div>
				
				<div class="sb2-13">
                    <ul class="collapsible" data-collapsible="accordion">
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/create_project"><i class="fa fa-bar-chart" aria-hidden="true"></i> Project</a></li>
                    </ul>
                </div> 
				
		   </div>
			
            <!--== BODY INNER CONTAINER ==--> 
            <div class="sb2-2">
                <!--== breadcrumbs ==-->
                <div class="sb2-2-2">
                    <ul>
                        <li><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-home" aria-hidden="true"></i> Home</a>
                        </li>
                        <li class="active-bre"><a href="#">Sports List Body Composition Norms</a></li>
                        <li class="page-back"><a href="<?php echo $this->config->item('base_path');?>user_management/load_panel"><i class="fa fa-backward" aria-hidden="true"></i> Back</a></li>
						<li class="page-back"><a class="" href="#" data-toggle="modal" data-target="#myModal1" >Add Sport Norm</a></li> 
                    </ul>
					
					<!--form id="exportnorm" name="exportnorm" action="" method="post">
					 <input type="button" class="lite_btn grey_btn f_left btn_red" value="Export" id="export" style="margin-top:32px;"/>
					 <input type="hidden" id="tablename" name="tablename" value="sport_match"/>
					</form-->
					
                </div>
                <div class="sb2-2-3">
                    <div class="row">
                        <!--== Country Campaigns ==-->
                        <div class="col-md-12">
                            <div class="box-inn-sp">
                                <div class="inn-title">
                                    <h4>Sports List Body Composition Norms</h4>
                                    
                                </div>
                                <div class="tab-inn">
                                    <div class="table-responsive table-desi">
                                        <table class="table table-hover" id="example">
                                            <thead>
                                                <tr>
                                                    <th>Sport Name</th>
                                                    <th>Sport Type</th>
                                                    <th>Gender</th>
                                                    <th>BodyFat PER</th>
                                                    <th>BodyFat SD</th>
													<th>Endomorph</th>
                                                    <th>Mesomorph</th>
                                                    <th>Ectomorph</th>
                                                    <th>Endo SD</th>
                                                    <th>Meso SD</th>
													<th>Ecto SD</th>
                                                    <th>Height</th>
                                                    <th>Height SD</th>
                                                    <th>Body Mass</th>
                                                    <th>Body Mass SD</th>
                                                    <th>Action</th>
                                                </tr>
                                            </thead>
                                            <tbody>
											
											 <?php
											 //while($sport=mysql_fetch_assoc($sportres))
											foreach($sportdata as $key=>$val)
											{
										      
											 ?>
										        <tr>
												  <td><span class="txt-dark weight-500" id="sport<?php echo $val->id; ?>"><?php echo $val->sport; ?></span></td>
												  <td><span class="txt-dark weight-500" id="sporttype<?php echo $val->id; ?>"><?php echo $val->sport_type;?></span></td>
												  <td><span class="txt-dark weight-500" id="gender<?php echo $val->id; ?>"><?php echo $val->gender;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodyfatper<?php echo $val->id; ?>"><?php echo $val->body_fat_per;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodyfatsd<?php echo $val->id; ?>"><?php echo $val->body_fat_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="endomorph<?php echo $val->id; ?>"><?php echo $val->endomorph;?></span></td>
												  <td><span class="txt-dark weight-500" id="mesomorph<?php echo $val->id; ?>"><?php echo $val->mesomorph;?></span></td>
												  <td><span class="txt-dark weight-500" id="ectomorph<?php echo $val->id; ?>"><?php echo $val->ectomorph;?></span></td>
												  <td><span class="txt-dark weight-500" id="endosd<?php echo $val->id; ?>"><?php echo $val->endo_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="mesosd<?php echo $val->id; ?>"><?php echo $val->meso_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="ectosd<?php echo $val->id; ?>"><?php echo $val->ecto_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="height<?php echo $val->id; ?>"><?php echo $val->height;?></span></td>
												  <td><span class="txt-dark weight-500" id="heightsd<?php echo $val->id; ?>"><?php echo $val->height_sd;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodymass<?php echo $val->id; ?>"><?php echo $val->body_mass;?></span></td>
												  <td><span class="txt-dark weight-500" id="bodymasssd<?php echo $val->id; ?>"><?php echo $val->body_mass_sd;?></span></td>
												  <td><a href="#" data-toggle="modal" data-target="#myModal" onclick="showModal('<?php echo $val->id;?>');"><i class="fa fa-pencil-square-o  modalLink" aria-hidden="true" ></i></a></td> 
												  <input type="hidden" value="<?php echo $val->id;?>" name="sportid"></input>
												</tr>
											 <?php
											 }
											 ?>
										
                                            </tbody>
                                        </table>
                                        
                                        <!-- Modal -->
										<div id="myModal" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Edit Sport</h4>
											  </div>
											  <div class="modal-body">
											  	    <div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="sport_name" name="sport_name" type="text" class="validate">
															<label for="sport_name" class="active">Sport Name</label>
														</div>
														<div class="input-field col s6">
															<input id="sport_type" name="sport_type" type="text" class="validate">
															<label for="sport_type" class="active">Sport Type</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="gen" name="gen" type="text" class="validate">
															<label for="gen" class="active">Gender</label>
														</div>
														<div class="input-field col s6">
															<input id="body_fatper" name="body_fatper" type="text" class="validate">
															<label for="body_fatper" class="active">BodyFat PER</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="body_fatsd" name="body_fatsd" type="text" class="validate">
															<label for="body_fatsd" class="active">BodyFat SD</label>
														</div>
														<div class="input-field col s6">
															<input id="endomorph" name="endomorph" type="text" class="validate">
															<label for="endomorph" class="active">Endomorph</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mesomorph" name="mesomorph" type="text" class="validate">
															<label for="mesomorph" class="active">Mesomorph</label>
														</div>
														<div class="input-field col s6">
															<input id="ectomorph" name="ectomorph" type="text" class="validate">
															<label for="ectomorph" class="active">Ectomorph</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="endosd" name="endosd" type="text" class="validate">
															<label for="endosd" class="active">Endo SD</label>
														</div>
														<div class="input-field col s6">
															<input id="mesosd" name="mesosd" type="text" class="validate">
															<label for="mesosd" class="active">Meso SD</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="ectosd" name="ectosd" type="text" class="validate">
															<label for="ectosd" class="active">Ecto SD</label>
														</div>
														<div class="input-field col s6">
															<input id="height" name="height" type="text" class="validate">
															<label for="height" class="active">Height</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="heightsd" name="heightsd" type="text" class="validate">
															<label for="heightsd" class="active">Height SD</label>
														</div>
														<div class="input-field col s6">
															<input id="bodymass" name="bodymass" type="text" class="validate">
															<label for="bodymass" class="active">Body Mass</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="bodymasssd" name="bodymasssd" type="text" class="validate">
															<label for="bodymasssd" class="active">Body Mass SD</label>
														</div>
													</div>
											  </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="button"  id="submitform"  onclick="updatedata()" class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</div>

										  </div>
										</div>
										
										
										
										
										
									<!-- Add Modal -->	
										<div id="myModal1" class="modal fade" role="dialog">
										  <div class="modal-dialog">

											<!-- Modal content-->
											<div class="modal-content">
											  <div class="modal-header">
												<button type="button" class="close" data-dismiss="modal">&times;</button>
												<h4 class="modal-title">Add Sport Norm</h4>
											  </div>
											  <form name="test" action="<?php echo $this->config->item('base_path');?>user_management/addsportnorm" method="POST">
											  <div class="modal-body">
											  	 <div class="row">
												        <input type="hidden" id="sprtid" name="sprtid" value="">
												        <input type="hidden" id="sportid" name="sportid" value="">
														<div class="input-field col s6">
															<input id="sport_name1" name="sport_name1" type="text" class="validate">
															<label for="sport_name1" class="active">Sport Name</label>
														</div>
														<div class="input-field col s6">
															<input id="sport_type1" name="sport_type1" type="text" class="validate">
															<label for="sport_type1" class="active">Sport Type</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="gen1" name="gen1" type="text" class="validate">
															<label for="gen1" class="active">Gender</label>
														</div>
														<div class="input-field col s6">
															<input id="body_fatper1" name="body_fatper1" type="text" class="validate">
															<label for="body_fatper1" class="active">BodyFat PER</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="body_fatsd1" name="body_fatsd1" type="text" class="validate">
															<label for="body_fatsd1" class="active">BodyFat SD</label>
														</div>
														<div class="input-field col s6">
															<input id="endomorph1" name="endomorph1" type="text" class="validate">
															<label for="endomorph1" class="active">Endomorph</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="mesomorph1" name="mesomorph1" type="text" class="validate">
															<label for="mesomorph1" class="active">Mesomorph</label>
														</div>
														<div class="input-field col s6">
															<input id="ectomorph1" name="ectomorph1" type="text" class="validate">
															<label for="ectomorph1" class="active">Ectomorph</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="endosd1" name="endosd1" type="text" class="validate">
															<label for="endosd1" class="active">Endo SD</label>
														</div>
														<div class="input-field col s6">
															<input id="mesosd1" name="mesosd1" type="text" class="validate">
															<label for="mesosd1" class="active">Meso SD</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="ectosd1" name="ectosd1" type="text" class="validate">
															<label for="ectosd1" class="active">Ecto SD</label>
														</div>
														<div class="input-field col s6">
															<input id="height1" name="height1" type="text" class="validate">
															<label for="height1" class="active">Height</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="heightsd1" name="heightsd1" type="text" class="validate">
															<label for="heightsd1" class="active">Height SD</label>
														</div>
														<div class="input-field col s6">
															<input id="bodymass1" name="bodymass1" type="text" class="validate">
															<label for="bodymass1" class="active">Body Mass</label>
														</div>
													</div>
													<div class="row">
														<div class="input-field col s6">
															<input id="bodymasssd1" name="bodymasssd1" type="text" class="validate">
															<label for="bodymasssd1" class="active">Body Mass SD</label>
														</div>
													</div>
											        
                                              </div>
											  <div class="modal-footer">											  	
												<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
												<button type="submit"  id="submitform"   class="btn btn-success" style="margin-right: 15px;">Save</button>
											  </div>
											</form>
											</div>

										  </div>
										</div>
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
										
                                    </div>
                                </div>
                            </div>
                        </div>                       
                    </div>
                </div>
            </div>

        </div>
    </div>



    <!--======== SCRIPT FILES =========-->
    <script src="<?php echo $this->config->item('path');?>/js/jquery.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/bootstrap.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/materialize.min.js"></script>
    <script src="<?php echo $this->config->item('path');?>/js/custom.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap.min.js"></script>
    
	<script type="text/javascript">
		$(document).ready(function() {
			$('#example').DataTable();
		});
	</script>
</body>
</html>