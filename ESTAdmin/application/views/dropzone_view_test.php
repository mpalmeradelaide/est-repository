<!--html>
<head>
<link href="<?php// echo base_url(); ?>/assets/dropzone/dist/dropzone.css" type="text/css" rel="stylesheet" />
<script src="<?php //echo base_url(); ?>/assets/dropzone/dist/min/dropzone.min.js"></script>
</head>
<body>
<h1>File Upload</h1>
<form action="<?php //echo site_url('/dropzone/upload'); ?>" class="dropzone"  >
</form>
</body>
</html-->



<!DOCTYPE html>
<!--html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>ImageUploadify - Example</title>
<link href="http://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" href="http://netdna.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="<?php echo base_url(); ?>/assets/dropzone/dist/imageuploadify.min.css" rel="stylesheet">
</head>

<body>
<div class="container" style="margin-top: 150px;">
  <form>
    <input type="file" accept=".xlsx,.xls,image/*,.doc,audio/*,.docx,video/*,.ppt,.pptx,.txt,.pdf" class="upload_img" multiple>
  </form>
</div>



<script src="http://code.jquery.com/jquery-1.12.4.min.js"></script> 
<script src="http://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> 
<script type="text/javascript" src="<?php echo base_url(); ?>/assets/dropzone/dist/imageuploadify.min.js"></script> 
<script type="text/javascript">
	$(document).ready(function() {
		$('.upload_img').imageuploadify();
	})
</script> 

</body>
</html-->












<!DOCTYPE HTML>
<html lang="en-US">
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" type="text/css" href="<?php echo base_url() ?>assets/uploadify/uploadify.css" />
  <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
  <script type="text/javascript" src="<?php echo base_url() ?>assets/uploadify/jquery.uploadify.min.js"></script>
  <title></title>
</head>
<body>
 
    <div class="uploadify-queue" id="file-queue"></div>
    <input type="file" name="userfile" id="upload_btn" />
 
    <script type='text/javascript' >
    $(function() {
     $('#upload_btn').uploadify({
	//  'swf'      : 'uploadify.swf',
	 // 'uploader' : 'uploadify.php'
		 
      'debug'   : false,
 
      'swf'   : '<?php echo base_url() ?>assets/uploadify/uploadify.swf',
      'uploader'  : '<?php echo base_url('Dropzone/uploadify')?>',
     // 'cancelImage' : '<?php echo base_url() ?>assets/js/lib/uploadify-cancel.png',
      'queueID'  : 'file-queue',
      'buttonClass'  : 'button',
      'buttonText' : "Upload Files",
      'multi'   : true,
      'auto'   : true,
 
      'fileTypeExts' : '*.jpg; *.png; *.gif; *.PNG; *.JPG; *.GIF;',
      'fileTypeDesc' : 'Image Files',
 
      'method'  : 'post',
      'fileObjName' : 'userfile',
 
      'queueSizeLimit': 40,
      'simUploadLimit': 2,
      'sizeLimit'  : 10240000,
      /*'onUploadSuccess' : function(file, data, response) {
            alert('The file ' + file.name + ' was successfully uploaded with a response of ' + response + ':' + data);
       },*/
       'onUploadComplete' : function(file) {
            alert('The file ' + file.name + ' finished processing.');
       },
        'onQueueFull': function(event, queueSizeLimit) {
            alert("Please don't put anymore files in me! You can upload " + queueSizeLimit + " files at once");
            return false;
        },
        });
     });
    </script>
 
</body>
</html>



