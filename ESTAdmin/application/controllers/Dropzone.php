<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 
class Dropzone extends CI_Controller {
  
    public function __construct() {
       parent::__construct();
       $this->load->helper(array('url','html','form')); 
    }
 
    public function index() {
        $this->load->view('dropzone_view');
    }
    
    public function upload() {
        if (!empty($_FILES)) {
        $tempFile = $_FILES['file']['tmp_name'];
        $fileName = $_FILES['file']['name'];
        $targetPath = getcwd() . '/uploads/';
        $targetFile = $targetPath . $fileName ;
        move_uploaded_file($tempFile, $targetFile);
        // if you want to save in db,where here
        // with out model just for example
        // $this->load->database(); // load database
        // $this->db->insert('file_table',array('file_name' => $fileName));
        }
    }
	
	
	 public function uploadify()
    {
        $config['upload_path'] = "uploads/";
        $config['allowed_types'] = '*';
        $config['max_size'] = 0;
        $this->load->library('upload', $config);
 
        if (!$this->upload->do_upload("file_upload"))
        {
            $response = $this->upload->display_errors();
        }
        else
        {
            $response = $this->upload->data();
        }
        $this->output->set_content_type('application/json')->set_output(json_encode($response));
    }
	
	
}