<?php
error_reporting(0);

defined('BASEPATH') OR exit('No direct script access allowed');

class login extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 public function __construct(){  
    // Call the Controller constructor
    parent::__construct();
	 	$this->load->model('usermanagement_model');
		$this->load->helper(array('form', 'url'));
		$this->load->library(array('session','form_validation'));  
        $this->load->library('email');
		//echo '<pre>';print_r($this->session->all_userdata());die;
		}
  
	public function index()
	{    
		//$this->load->helper('url');
		  //$this->load->view('assets/userheader');
		$this->load->view('client_login');
	}
	
	public function checkUser()
	{   
	
	$postData = $this->input->post();
			
			if ($this->input->server('REQUEST_METHOD') === 'POST')
			{
			$this->form_validation->set_rules('userid', 'userid', 'required');
	
            $this->form_validation->set_rules('pass', 'pass', 'required');
			$this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');

            //if the form has passed through the validation
            if ($this->form_validation->run())
            {
			//print_r($postData);die;
			$login_username=$this->input->post('userid'); 
			$login_password=$this->input->post('pass'); 
			$check = $this->usermanagement_model->checkLoginUser($login_username,$login_password);
			 if(!empty($check))
			 {
				if($check['login_userlevel'] == '999'){
						$sessiondata = array(
						'login_id'  => $check['login_id'],
					    'login_name'  => $check['login_name'],
					    'login_username'     => $check['login_username'],
					    'login_ind'     => $check['login_ind'],
					    'login_idco'     => $check['login_idco'],
					    'login_company'=>$check['login_company'],
					    'company_url'=>$check['company_url'],
					    'category_name'=>$check['category_name'],
					    'category_id'=>$check['category_id'],
						 'company_id'=>$check['id'],
					    'end_date'=>$check['end_date'],
					    'type'=>'Admin',
					    'logged_in' => TRUE
					);

					$this->session->set_userdata($sessiondata);
					redirect('user_management'); 
				}
				else
				{
						$sessiondata = array(
						'login_id'  => $check['login_id'],
					   'login_name'  => $check['login_name'],
					   'login_username'     => $check['login_username'],
					   'login_ind'     => $check['login_ind'],
					   'login_idco'     => $check['login_idco'],
					   'login_company'=>$check['login_company'],
					   'company_url'=>$check['company_url'],
					   'category_name'=>$check['category_name'],
					   'category_id'=>$check['category_id'],
					   'company_id'=>$check['id'],
					   'end_date'=>$check['end_date'],
					   'type'=>'User',
					   'logged_in' => TRUE
					);


					$this->session->set_userdata($sessiondata);
					redirect('user_management'); 
				}
				
			  }
			  else{
				$this->session->set_flashdata('error_message','username or password is not correct.');
				//echo $this->session->flashdata('error_message');die;
				redirect('login');
			  }
			 }
			 else{
				 redirect('login');
			 }
			}
			 else{
				 redirect('login');
			 }
	}
}
