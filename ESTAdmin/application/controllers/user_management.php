<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class user_management extends CI_Controller {
public function __construct() {
		parent::__construct();          
                            // Load form helper library
                            $this->load->helper('form');
                            $this->load->library('upload');
                            $this->load->library('form_validation');
                            $this->load->library('session');
				            $this->load->model('usermanagement_model');
			    			$this->load->helper(array('url'));
			    			$this->load->helper('email');
                            $this->load->helper('date');
							
							$this->load->dbutil();
							$this->load->helper('file');
							$this->load->helper('download');
                 
           	
			}
			
			
   public function index()
	{
		$this->load->view('login');		
	}
    
   public function checkUser()
	{   	
	        $postData = $this->input->post();	
			$uname=$this->input->post('username');
			$pswrd=$this->input->post('passwrd');
			$_SESSION['uname']=$uname;
			$_SESSION['pswrd']=$pswrd;
			
		    $res=$this->usermanagement_model->checkuser($uname,$pswrd);
			$usname = $res[0]->username;
			$psswrd = $res[0]->password;
		
			if($uname == $usname && $pswrd == $psswrd)
			{
				$data['sportdata']=$this->usermanagement_model->get_sportlist();
				$data['normdata']=$this->usermanagement_model->get_normlist();
				$data['fitnessnormdata']=$this->usermanagement_model->get_fitnessnormlist();
				$data['bminormdata']=$this->usermanagement_model->get_bminormlist();
				$data['bloodnormdata']=$this->usermanagement_model->get_bloodnormlist();
				$data['fitnesssportsnormdata']=$this->usermanagement_model->sports_fitness_norms();
				$this->load->view('panel',$data);	
			}
			else
            {
				$this->load->view('login');	
			}
	}
	
	
	public function create_project()
	{
		$this->load->view('createproject');	
	}
	public function saveproject()
	{
		$project=$this->input->post('project');
		$data=$this->usermanagement_model->save_project($project);
		if($data)
		{
			$this->load->view('createproject');	
		}
	}
	
	public function forgot()
	{
		$this->load->view('changepassword');	
	}
	
    public function checkpassword()
	{
		    $postData = $this->input->post();	
			//print_r($postData); die;
			
			$old=$this->input->post('oldpassword'); // old password
			$new=$this->input->post('newpasswrd');  // new password
			$uname=$this->input->post('uname');  // new password
			
			$data=$this->usermanagement_model->update_password($old,$new,$uname);
			if($data)
			{
			 $this->load->view('login');	
			}
			else{
				$this->load->view('changepassword');	
			}
					 
	}  

    public function load_panel()
	{
		        $data['sportdata']=$this->usermanagement_model->get_sportlist();
				$data['normdata']=$this->usermanagement_model->get_normlist();
				$data['fitnessnormdata']=$this->usermanagement_model->get_fitnessnormlist();
				$data['bminormdata']=$this->usermanagement_model->get_bminormlist();
				$data['bloodnormdata']=$this->usermanagement_model->get_bloodnormlist();
				$data['fitnesssportsnormdata']=$this->usermanagement_model->sports_fitness_norms();
				$this->load->view('panel',$data);	
	}
	
	

	
	//Get User Data
	public function users()	
	{
		$data['userdata']=$this->usermanagement_model->get_users();			
		$this->load->view('userlist',$data);	
	}
	
	public function showsportlist()	
	{
		$data['sportdata']=$this->usermanagement_model->get_sportlist();			
		$this->load->view('sportlist',$data);	
	}
	
	
		// Add sport normdata
	public function addsportnorm()
	{
		$res=$this->usermanagement_model->add_sportnormlist();
		if($res)
		{
		redirect('/user_management/showsportlist');
		}	
	}
	
	
	public function updatesport()
	{
		$res=$this->usermanagement_model->update_sportlist();
		echo $res;		
	}

	//Update Sports Fitness Norms
		public function update_sports_fitness_norm()
	{
		$res=$this->usermanagement_model->update_fitness_sports_norm();
		echo $res;		
	}
	
    public function shownormlist()	
	{
		$data['normdata']=$this->usermanagement_model->get_normlist();
		$this->load->view('normlist',$data);	
	} 

    public function showfitnessnormlist()	
	{
		$data['fitnessnormdata']=$this->usermanagement_model->get_fitnessnormlist();
		$this->load->view('fitnessnorm',$data);	
	} 	
	
	public function updatenorm()
	{
		$res=$this->usermanagement_model->update_normlist();
		echo $res;		
	}
	
	public function updatebloodnorm()
	{
		$res=$this->usermanagement_model->update_bloodnormlist();
		echo $res;		
	}
	
	
	
	public function addbloodnorm()
	{
		$res=$this->usermanagement_model->add_bloodnormlist();
		if($res)
		{
		redirect('/user_management/showbloodnormlist');
		}		
	}
	

	
	//Add Sports Fitness Norms
	public function addsportsfitness()
	{
		$res=$this->usermanagement_model->add_sports_fitness_norms();
		if($res)
		{
		redirect('/user_management/fitness_norms');
		}		
	}
	
	public function updatefitnessnorm()
	{
		$res=$this->usermanagement_model->update_fitnessnormlist();
		echo $res;		
	}
	
	
	public function showbminormlist()
	{
		$data['bminormdata']=$this->usermanagement_model->get_bminormlist();
		$this->load->view('bminorm',$data);	
	}
	
	public function updatebminorm()
	{
		$res=$this->usermanagement_model->update_bminormlist();
		echo $res;
	}
	
	
	public function logout()
	{
		session_start();
		session_destroy();
		$this->load->view('login');
	}
	
	
	public function showbloodnormlist()
	{
		$data['bloodnormdata']=$this->usermanagement_model->get_bloodnormlist();
		$this->load->view('bloodnorm',$data);	
	}
	//Get all Fitness norms with sports List
	public function fitness_norms()
	{
		$data['sports_fitnessnorms']=$this->usermanagement_model->sports_fitness_norms();
		$this->load->view('sports_fitnessnorms',$data);	
	}
	
   	
	public function exportUsers()
       {
		 $table=$_REQUEST['tablename'];  
		$delimiter = ",";
        $newline = "\r\n";
		$filename = date('Y-m-d H:i:s')."_Users.csv"; 
        $query = "SELECT * from ".$table." order by id asc ";
	    $result = $this->db->query($query);
	    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	    force_download($filename, $data); 
  	   } 
	   
	   
	   
	   
		  
       public function exportNorm()
       {
		 $table=$_REQUEST['tablename'];  
		//$table=$this->input->post('name');
		//echo $table; 
		$delimiter = ",";
        $newline = "\r\n";
		$filename = date('Y-m-d H:i:s')."_Norms.csv"; 
        $query = "SELECT * from ".$table." order by id asc ";
	    $result = $this->db->query($query);
	    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	    force_download($filename, $data); 
  	   }   
     
}
