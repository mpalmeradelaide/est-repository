<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	public function __construct() {
		
		parent::__construct();          
                            // Load form helper library
                            $this->load->helper('form');
                            $this->load->library('upload');
                            $this->load->library('form_validation');
                            //date_default_timezone_set("Asia/Calcutta");
                            // Load session library
                            $this->load->library('session');
				// Load database
                            $this->load->model('welcome_model');
			    $this->load->helper(array('url'));
                            $this->load->helper('date');
                             
           	
			}
	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		//$this->load->view('welcome_message');
            //echo $this->config->item('path'); die;
                if($this->session->userdata('is_logged_in'))
                {
                redirect('dashboard');
                }
                else
                {
                $this->load->view('welcome_message');
                }
	}
        
        public function dashboard()
        {
           // die('ddd');
                    $data['user']= $this->welcome_model->user_list();
                    $this->load->view('templates/header');
                    $this->load->view('dashboard', $data);
                    $this->load->view('templates/footer');
        }
        
	// Check for user login process
            public function login() 
            {
            
                if($this->input->post('login_hidden'))
                { 
                 $this->form_validation->set_rules('user_name', 'Username', 'trim|required|xss_clean');
                 $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean|callback_check_database');
                      if($this->form_validation->run() == FALSE)
                      {
                        //Field validation failed.  User redirected to login page
                        $this->load->view('welcome_message');
                      }
                      else
                      {
                        //Go to private area
                        redirect('welcome/dashboard', 'refresh');
                      }
                }
                else if($this->input->post('login_blog_hidden'))
                {
                 die('ddd');   
                }
            }
            
            //Check login credintials
             function check_database($password)
                {
                  //Field validation succeeded.  Validate against database
                  $username = $this->input->post('user_name');

                  //query the database
                  $result = $this->welcome_model->login($username, $password);

                  if($result)
                  {
                    $sess_array = array();
                    foreach($result as $row)
                    {
                      $sess_array = array(
                        'id' => $row->id,
                        'username' => $row->user_name
                      );
                      $this->session->set_userdata('logged_in', $sess_array);
                    }
                    return TRUE;
                  }
                  else
                  {
                    $this->form_validation->set_message('check_database', 'Invalid username or password');
                    return false;
                  }
                }
	/**
	 * logout function.
	 * 
	 * @access public
	 * @return void
	 */
            public function logout()
            {
                
                    $this->session->unset_userdata('logged_in');
                     session_destroy();
                     redirect('default_controller', 'refresh');
           
            }
            
            /**
	 * Add Single User function.
	 * 
	 * @access public
	 * @return void
	 */
	public function add_single_user()
        {   
            
            $data='';
             if ($this->input->server('REQUEST_METHOD') === 'POST')
                    {
                 
//               echo "<pre>";
//print_r($_POST);
//echo "</pre>";die;
                    //form validation
                    $this->form_validation->set_rules('first_name', 'First Name', 'required');
                    $this->form_validation->set_rules('last_name', 'last_name', 'required');
                    $this->form_validation->set_rules('company_name', 'company_name', 'required');
                    $this->form_validation->set_rules('user_name', 'user_name', 'required');
                    $this->form_validation->set_rules('password', 'password', 'required');
                    $this->form_validation->set_rules('user-level', 'user-level', 'required');
                    $this->form_validation->set_rules('user_type', 'user_type', 'required');
                    $this->form_validation->set_rules('user_type', 'user_type', 'required');
                    $this->form_validation->set_rules('ipad_type', 'ipad_type', 'required|numeric');
                    $this->form_validation->set_rules('version_no', 'version_no', 'required|numeric');
                    $this->form_validation->set_rules('skip_status', 'skip_status', 'required|numeric');
                    $this->form_validation->set_rules('iphone', 'iphone', 'required|numeric');
                    $this->form_validation->set_rules('user_type1', 'user_type1', 'required|numeric');
                    $this->form_validation->set_rules('category', 'category', 'required');
                    $this->form_validation->set_rules('iphone_type', 'iphone_type', 'required|numeric');
                    $this->form_validation->set_rules('iphone_version', 'iphone_version', 'required|numeric');
                    $this->form_validation->set_rules('iphone_skip_status', 'iphone_skip_status', 'required|numeric');
                    $this->form_validation->set_error_delimiters('<div class="alert alert-error"><a class="close" data-dismiss="alert">×</a><strong>', '</strong></div>');
                    //if the form has passed through the validation
                    if ($this->form_validation->run())
                    {
                            
                        if($this->input->post('user_type')==1)
                        {
                          $ipad_day=$this->input->post('ipaddays');  
                          $start_date='';
                          $end_date='';
                        }
                        else
                        {
                         $start_date=$this->input->post('start_date');  
                         $end_date=$this->input->post('end_date');
                         $ipad_day=0;
                        }
                        
                        if($this->input->post('user_type1')==1)
                        {
                          $iphone_day=$this->input->post('iphonedays');  
                          $iphone_start_date='';
                          $iphone_end_date='';
                        }
                        else
                        {
                         $iphone_start_date=$this->input->post('start_name1');  
                         $iphone_end_date=$this->input->post('start_name2'); 
                         $iphone_day=0;
                        }
                        
                        $data_to_store = array(
                        'login_name' =>$this->input->post('first_name').' '.$this->input->post('last_name') ,
                        'login_company' => trim($this->input->post('company_name')),
                        'login_username' => trim($this->input->post('user_name')),
                        'login_password' => trim($this->input->post('password')), 
                        'is_website' => trim($this->input->post('website')),
                        'is_ipad' => trim($this->input->post('ipad')),
                        'is_iphone' => trim($this->input->post('iphone')),
                        'login_userlevel' => trim($this->input->post('user-level')),
                        'user_type' => trim($this->input->post('user_type')),
                        'days' =>$ipad_day,
                        'start_date' => $start_date ,
                        'end_date' => $end_date ,
                        'version_no' => trim($this->input->post('version_no')),
                        'skip_status' => trim($this->input->post('skip_status')),
                            //Iphone data
                        'iphone_user_type' => trim($this->input->post('user_type1')),
                        'category' => trim($this->input->post('category')),
                        'iphone_days' =>$iphone_day,
                        'iphone_start_date' =>$iphone_start_date,
                        'iphone_end_date' =>$iphone_end_date,
                        'iphone_version' => trim($this->input->post('iphone_version')),
                        'iphone_skip_status' => trim($this->input->post('iphone_skip_status')),
                        'mail' => trim($this->input->post('mail')));
                           //if the insert has returned true then we show the flash message
                           //print_r($data_to_store);die;
                           if($this->welcome_model->addnew_user($data_to_store))
                           {
                               $data['flash_message'] = TRUE; 
                           }
                           else
                           {
                               $data['flash_message'] = FALSE; 
                           }

                }

                    }
                    $this->load->view('templates/header');
                    $this->load->view('add_single_user',$data);
                    $this->load->view('templates/footer');  
        }
        
        
        
         /**
	 * Add Multiple User function.
	 * 
	 * @access public
	 * @return void
	 */
        public function add_multiple_user()
        {
            
                    $this->load->view('templates/header');
                    $this->load->view('add_multiple_user');
                    $this->load->view('templates/footer');  
        }
        
        
        
        /**
	 * Add edit_all_web_user function.
	 * 
	 * @access public
	 * @return void
	 */
        public function web_user()
        {
                    $data['user']= $this->welcome_model->web_user_list();
                   //print_r($data);die;
                    $this->load->view('templates/header');
                    $this->load->view('web_user',$data);
                    $this->load->view('templates/footer');  
        }
        /**
	 * Add ipad_user function.
	 * 
	 * @access public
	 * @return void
	 */
        public function ipad_user()
        {
                    $data['user']= $this->welcome_model->ipad_user_list();
                    //print_r($data);die;
                    $this->load->view('templates/header');
                    $this->load->view('ipad_user',$data);
                    $this->load->view('templates/footer');  
        }
        
        
         /**
	 * Add advertisement function.
	 * 
	 * @access public
	 * @return void
	 */
        public function advertisement()
        {
                    $data['user']= $this->welcome_model->web_user_list();
                   //print_r($data);die;
                    $this->load->view('templates/header');
                    $this->load->view('advertisement',$data);
                    $this->load->view('templates/footer');  
        }
        
        /**
	 * Add default_advertisement function.
	 * 
	 * @access public
	 * @return void
	 */
        public function default_advertisement()
        {
                    
                   //print_r($data);die;
                    $this->load->view('templates/header');
                    $this->load->view('default_advertisement');
                    $this->load->view('templates/footer');  
        }
        
         /**
	 * Delete all function.
	 * 
	 * @access public
	 * @return void
	 */
        
       
    public function deleteall()
    {
     $this->load->helper('url');
        if($_POST)
         {
            $record=$_POST;
            
           // print_r($record);die;
                $res= $this->welcome_model->deleteselectedrecord($record);
         
               if($res==1)
               {
               $this->session->set_flashdata('flash_message', 'delete');
               }
                else
                {
                    $this->session->set_flashdata('flash_message', 'not_delete');
                }
                redirect('welcome/web_user');
        }
 
           
    }
    
    
    /**
	 * Delete function.
	 * 
	 * @access public
	 * @return void
	 */
     public function delete()
    {
        //product id 
        $id = $this->uri->segment(3);
        $this->welcome_model->delete_user($id);
        redirect('welcome/web_user');
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
        /**
	 End of controller
	 */
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */