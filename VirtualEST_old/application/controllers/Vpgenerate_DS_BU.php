<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vpgenerate extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
         function __construct()
        {
             parent::__construct();
             error_reporting(1);
			 $this->load->helper('form');
             $this->load->helper('url');
             $this->load->model('virtual_model');
             $this->load->library('menu');
             $this->load->library('statistical');
             $this->load->dbutil();
             $this->load->helper('file');
             $this->load->helper('download');
             session_start();
         
        }
	public function index($c =0)
	{
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['upload_count'] = '0';
		$data['selected_var'] = '';
        $data['test_var'] = $this->virtual_model->gettest_variables();  
        $this->load->view('generate_vp', $data);
       }
        
       //
      //export VP in csv file
       public function exportVP()
       {
      $no_of_record=$this->input->post('exportnum');
      //$no_of_record=5;
           $delimiter = ",";
        $newline = "\r\n";
		$filename = date('Y-m-d H:i:s')."_VP_$no_of_record.csv";    
		$selected_var=$this->input->post('selected_var');
      $selected_var=str_replace("name","CONCAT(firstname,' ',lastname) AS `Name`",$selected_var);
	    $query = "SELECT id,$selected_var from virtual_person_info order by id desc limit $no_of_record";
	    $result = $this->db->query($query);
	    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	   force_download($filename, $data);
		
	}        

//Change 
    public function VirtualpersonGeneration()
    {
        $selected_var="";
        $selected_variable=array(); 
        $no_of_vp=$this->input->post('no_of_vp');       
        $gender=$this->input->post('gender');
        $subpopulation=$this->input->post('subpopulation');
		$selected_variable=$this->input->post('sel_itm2');
       if(count($selected_variable)>0)
        {   
        $selected_var=implode(",",$selected_variable);
        }
        $count=0;
        for($i=1;$i<=$no_of_vp;$i++)
        {
       $physical_activity=array();
       $riskfactor=array();
	   $fligt_contact_time=array();
       $data = $this->virtual_model->generateRandomValues($gender,$subpopulation);   
       $firstname=$data['firstname'];
       $lastname=$data['lastname'];
       $dob = $data['daydropdown'] . "/" . $data['monthdropdown'] . "/" .$data['yeardropdown'];
       $occupation=$data['occupation'];
       $country=$data['country']['countryvalue'];
       $age=$data['vp_age'];
       $mass=$data['MASS'];
       $height=$data['HEIGHT'];
       $bmi=$data['BMI'];
       $getgender=$data['gender'];
       $sub_population=$data['sub_population'];
       if($getgender=='Male')
       {
        $user_gender='M';   
       }
       if($getgender=='Female')
       {
        $user_gender='F';   
       }
       $user_subpopulation=$data['sub_population'];
   
/// Pre-exercise Screening ///
        /***********Medical History***********/
   $medical_his_data = $this->virtual_model->fetch_preexercise_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);     
 
//GET Medication & Condition
   $medication_condition = $this->virtual_model->medication_condition($medical_his_data,$user_gender);    
   
   //GET PHYSICAL ACTIVITY
   $physical_activity=$this->virtual_model->VP_physical_activity($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);
   
   //GET RISK FACTOR
   $riskfactor=$this->virtual_model->fetch_risk_factors_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);
   
 //GET Fitness Module VERICAL JUMP
  $virticaljump = $this->virtual_model->get_Vertical_jump($age,$user_subpopulation,$user_gender);       
  
  $fligt_contact_time = $this->virtual_model->get_Flight_ratio($age,$user_subpopulation,$user_gender);  
  
  
  
// kritika Code
     // Required Details       
   /* $measuredheight=$_SESSION['HEIGHT'];
    $measuredmass=$_SESSION['MASS'];
    $measuredBMI=$_SESSION['BMI'];
    $measuredpopulation=$_SESSION['subpopulation'];
    $gender=$_SESSION['user_gender'];
    $measuredage=$_SESSION['vp_age']; */           
            
            
/*     $body_composition_data = $this->virtual_model->body_composition_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);         
    print_r($body_composition_data); die('sss'); */
            
// kritika Code    
  
  
  
  
  
  $setuserdata=array('firstname'=>$firstname,'lastname'=>$lastname,'dob'=>$dob,'gender'=>$getgender,'subpopulation'=>$sub_population,'occupation'=>$occupation,'country'=>$country,'age'=>$age,'mass'=>$mass,'height'=>$height,'bmi'=>$bmi);
  
  
  
  
           
    // kritika Code        
    // Body Composition : kritika
	/* if(count($body_composition_data)>0)
    {
      $body_composition_data_array=array('measured_height'=>$body_composition_data[0]->option_height_measured
              ,'measured_weight'=>$body_composition_data[0]->option_weight_measured,                                         
              'measuredbmi'=>$body_composition_data[0]->option_bmi,
              'measured_population'=>$body_composition_data[0]->option_measuredpopulation,
              'measuredwaist'=>$body_composition_data[0]->option_waist,
              'hip'=>$body_composition_data[0]->option_hip,
              'waist_hip_ratio'=>$body_composition_data[0]->option_whr,
              'triceps'=>$body_composition_data[0]->option_triceps,
              'biceps'=>$body_composition_data[0]->option_biceps,
              'subscapular'=>$body_composition_data[0]->option_subscapular,
              'sos'=>$body_composition_data[0]->option_sos,
              'self_report_height'=>$body_composition_data[0]->option_height,
              'self_report_weight'=>$body_composition_data[0]->option_weight
          ); 
    } */   
  
  
  
  
  
  
  
    if(count($medical_his_data)>0)
    {
     
      $medicalcond=array('heart_condition'=>$medical_his_data[0]->option_1
              ,'pain_in_chaist'=>$medical_his_data[0]->option_2,
              'feel_faint'=>$medical_his_data[0]->option_3,
              'asthma_attack'=>$medical_his_data[0]->option_4,
              'diabetes'=>$medical_his_data[0]->option_5,
              'bone_muscle_prob'=>$medical_his_data[0]->option_6,
              'other_medical_cond'=>$medical_his_data[0]->option_7,
          );   
        
    }
    
    //Physical Activity
    if(count($physical_activity)>0)
    {
     
      $physical_act_array=array('walking'=>$physical_activity[0]->option_1
              ,'minutes_walking'=>$physical_activity[0]->option_2,
              'vigorous'=>$physical_activity[0]->option_3,
              'minute_vigorous'=>$physical_activity[0]->option_4,
              'moderate'=>$physical_activity[0]->option_5,
              'minute_moderate'=>$physical_activity[0]->option_6,
              'tv_hours_games'=>$physical_activity[0]->option_7,
              'driving_hours'=>$physical_activity[0]->option_8,
              'is_setting_long_period'=>$physical_activity[0]->option_9
          );   
        
    }
    //RISK FACTOR
	 if(count($riskfactor)>0)
    {
     
      $riskfactor_array=array('family_history'=>$riskfactor[0]->option_1
              ,'family_gender'=>$riskfactor[0]->option_gender,
              'age_at_heart_attack'=>$riskfactor[0]->option_age,
              'current_smoker'=>$riskfactor[0]->option_2,
              'how_many'=>$riskfactor[0]->option_smoke,
              'quit_smoke_recently'=>$riskfactor[0]->option_3,
              'how_many_recently'=>$riskfactor[0]->option_smoke_6,
              'self_report_BP'=>$riskfactor[0]->option_4,
              'self_report_Chol'=>$riskfactor[0]->option_5,
              'self_report_Glucose'=>$riskfactor[0]->option_6,
              'Measured_SBP'=>$riskfactor[0]->option_7,
              'Measured_DBP'=>$riskfactor[0]->option_8,
              'Measured_fast_blood_glucose'=>$riskfactor[0]->option_12,
              'Measured_blood_Cholesterol'=>$riskfactor[0]->option_11,
              'HDL'=>$riskfactor[0]->option_9,
              'LDL'=>$riskfactor[0]->option_10,
              'triglycerides'=>$riskfactor[0]->option_13
          );   
        
    }
	
    if(count($medication_condition)>0)
    {
     
      $medication=array('medication_selected'=>$medication_condition['medication_selected']
              ,'medical_cond_selected'=>$medication_condition['medical_cond_selected']
          );   
        
    }
    
    if(count($virticaljump)>0)
    {
     
      $vjarray=array('vj'=>$virticaljump['vj']
              ,'perform_percent'=>$virticaljump['perform_percent']
              ,'z_score'=>$virticaljump['z_score']
          );   
        
    }
    
     if(count($fligt_contact_time)>0)
    {
     
      $flightarray=array('FTCT'=>$fligt_contact_time['FTCT']
              ,'flight_percent'=>$fligt_contact_time['flight_percent']
              ,'flight_z_score'=>$fligt_contact_time['flight_z_score']
          );   
        
    }
    $finaldata=array_merge($setuserdata,$medicalcond,$physical_act_array,$riskfactor_array,$medication,$vjarray,$fligt_contact_time);
   // print_r($finaldata);   die;
    $addVpData = $this->virtual_model->add_VP_Data($finaldata); 
     $count++; 
    }
    $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
       $data['upload_count'] = $count;
	   $data['selected_var']=$selected_var;
       $data['test_var'] = $this->virtual_model->gettest_variables();   
        $this->load->view('generate_vp', $data);
    }
    
         
}

/* End of file Vpgenerate.php */
/* Location: ./application/controllers/welcome.php */