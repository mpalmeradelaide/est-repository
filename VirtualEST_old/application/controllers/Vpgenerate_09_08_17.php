<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vpgenerate extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
         function __construct()
        {
             parent::__construct();
              error_reporting(1);
			 $this->load->helper('form');
             $this->load->helper('url');
             $this->load->model('virtual_model');
             $this->load->library('menu');
             $this->load->library('statistical');
             $this->load->dbutil();
             $this->load->helper('file');
             $this->load->helper('download');
             session_start();
         
        }
	public function index($c =0)
	{
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['upload_count'] = '0';
        $this->load->view('generate_vp', $data);
       }
        
       //
      //export VP in csv file
       public function exportVP()
       {
      $no_of_record=$this->input->post('exportnum');
      //$no_of_record=5;
           $delimiter = ",";
        $newline = "\r\n";
		$filename = date('Y-m-d H:i:s')."$no_of_record.csv";    
		
	    $query = "SELECT * from virtual_person_info order by id desc limit $no_of_record";
	    $result = $this->db->query($query);
	    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	    force_download($filename, $data);
	}        

//Change 
    public function VirtualpersonGeneration()
    {
            
        $no_of_vp=$this->input->post('no_of_vp');       
        $gender=$this->input->post('gender');
        $subpopulation=$this->input->post('subpopulation');
       
        $count=0;
        for($i=1;$i<=$no_of_vp;$i++)
        {
       $data = $this->virtual_model->generateRandomValues($gender,$subpopulation);   
       $firstname=$data['firstname'];
       $lastname=$data['lastname'];
       $dob = $data['daydropdown'] . "/" . $data['monthdropdown'] . "/" .$data['yeardropdown'];
       $occupation=$data['occupation'];
       $country=$data['country']['countryvalue'];
       $age=$data['vp_age'];
       $mass=$data['MASS'];
       $height=$data['HEIGHT'];
       $bmi=$data['BMI'];
       $getgender=$data['gender'];
       $sub_population=$data['sub_population'];
       if($getgender=='Male')
       {
        $user_gender='M';   
       }
       if($getgender=='Female')
       {
        $user_gender='F';   
       }
       $user_subpopulation=$data['sub_population'];
   
/// Pre-exercise Screening ///
        /***********Medical History***********/
   $medical_his_data = $this->virtual_model->fetch_preexercise_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);     
 
//GET Medication & Condition
   $medication_condition = $this->virtual_model->medication_condition($medical_his_data,$user_gender);     
  
   
 //GET Fitness Module VERICAL JUMP
  $virticaljump = $this->virtual_model->get_Vertical_jump($age,$user_subpopulation,$user_gender);       
  $setuserdata=array('firstname'=>$firstname,'lastname'=>$lastname,'dob'=>$dob,'gender'=>$getgender,'subpopulation'=>$sub_population,'occupation'=>$occupation,'country'=>$country,'age'=>$age,'mass'=>$mass,'height'=>$height,'bmi'=>$bmi);
  
    if(count($medical_his_data)>0)
    {
     
      $medicalcond=array('heart_condition'=>$medical_his_data[0]->option_1
              ,'pain_in_chaist'=>$medical_his_data[0]->option_2,
              'feel_faint'=>$medical_his_data[0]->option_3,
              'asthma_attack'=>$medical_his_data[0]->option_4,
              'diabetes'=>$medical_his_data[0]->option_5,
              'bone_muscle_prob'=>$medical_his_data[0]->option_6,
              'other_medical_cond'=>$medical_his_data[0]->option_7,
          );   
        
    }
    
    if(count($medication_condition)>0)
    {
     
      $medication=array('medication_selected'=>$medication_condition['medication_selected']
              ,'medical_cond_selected'=>$medication_condition['medical_cond_selected']
          );   
        
    }
    
    if(count($virticaljump)>0)
    {
     
      $vjarray=array('vj'=>$virticaljump['vj']
              ,'perform_percent'=>$virticaljump['perform_percent']
              ,'z_score'=>$virticaljump['z_score']
          );   
        
    }
    
    $finaldata=array_merge($setuserdata,$medicalcond,$medication,$vjarray);
    
    $addVpData = $this->virtual_model->add_VP_Data($finaldata); 
     $count++; 
    }
    $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
       $data['upload_count'] = $count;
        $this->load->view('generate_vp', $data);
    }
    
         
}

/* End of file Vpgenerate.php */
/* Location: ./application/controllers/welcome.php */