<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Body extends CI_Controller {

   public function __construct()
   {  
    // Call the Controller constructor
        
            error_reporting(1);
            ini_set('memory_limit', '512M');
            parent::__construct();
            $this->load->helper(array('form', 'url','common'));
	    $this->load->library(array('session','form_validation'));  
            $this->load->model('body_model','',TRUE);
            $this->load->model('client_model','',TRUE);
            $this->load->model('fitness_model','',TRUE);
            $this->load->helper('text');               
            $this->load->library('menu');
            session_start();
            
            $date = str_replace('/', '-', $_SESSION['user_dob']);                      
	    $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
	    $dob = strtotime( $dateOfBirth);						 
	    $tdate = strtotime(date('Y-m-d')) ;			
	    $_SESSION['age'] = $this->getAge($dob, $tdate) ;
            
            if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
             {
                 $_SESSION['age_range'] = '18-29' ;
             }
             elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
             {
                 $_SESSION['age_range'] = '30-39' ;
             }
             elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
             {
		 $_SESSION['age_range'] = '40-49' ;
             }
             elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
             {
		 $_SESSION['age_range'] = '50-59' ;
             }
             elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
             {
		 $_SESSION['age_range'] = '60-69' ;
             }
             elseif($_SESSION['age'] >=70)
             {
		 $_SESSION['age_range'] = '70+' ;
             }
    }  
 
    function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                  ++$age;
                }
                return $age;
        }
	
    public function index() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
			 
             $this->load->view('body_tests',$data);	    
	 
	}	
	//Get Information Details 
	 public function get_info_details() {
        $info_id = $this->input->post('info_id');
        $get_info_data = $this->body_model->fetch_info_data($info_id);
        echo json_encode($get_info_data[0]);
        }

		
		
	   //Generate Virtual Person for Restricted Profile and Full Profile
        public function BCVirtualpersonGeneration()
		{
		session_unset();
		session_destroy();
		session_write_close();
		session_regenerate_id(true);
		$vptype=$this->input->post('vptype');      // Restricted or Full profile
        //echo $vptype; 
        session_start();
				$data['randomdata'] = $this->client_model->generateRandomValues();   
				$dob = $data['randomdata']['daydropdown']. "/" .$data['randomdata']['monthdropdown']. "/" .$data['randomdata']['yeardropdown'];
                $_SESSION['age'] = $data['randomdata']['vp_age'];
                $_SESSION['user_first_name']=$data['randomdata']['firstname'];
                $_SESSION['user_last_name']=$data['randomdata']['lastname'];
                $_SESSION['user_dob'] = $dob;
                $_SESSION['user_daydropdown'] = $data['randomdata']['daydropdown'];
                $_SESSION['user_monthdropdown'] =$data['randomdata']['monthdropdown'];
                $_SESSION['user_yeardropdown'] = $data['randomdata']['yeardropdown'];
                if($data['randomdata']['gender']=='Male')
                {
                 $_SESSION['user_gender'] ='M';    
                }
                else
                {
                 $_SESSION['user_gender'] ='F';    
                }  
				
				
				 $_SESSION['is_virtual']=1;
               
                $_SESSION['user_country'] = $data['randomdata']['country']['countrykey'];
                $_SESSION['user_occupation'] = $data['randomdata']['occupation'];
                $_SESSION['p_name'] = '';
                $_SESSION['p_desc'] ='';
                $_SESSION['email'] ='';
              
                $_SESSION['subpopulation'] = $data['randomdata']['sub_population'];
                $_SESSION['vp_age'] = $data['randomdata']['vp_age'];
                $_SESSION['age'] = $data['randomdata']['vp_age'];
                $_SESSION['gender']=$data['randomdata']['gender'];
                //Adding Random Mass BMI and Height
                $_SESSION['MASS'] = $data['randomdata']['MASS'];
                $_SESSION['BMI'] = $data['randomdata']['BMI'];
                $_SESSION['HEIGHT'] = $data['randomdata']['HEIGHT'];
                $_SESSION['vj']="";
				$_SESSION['FTCT']="";
				$_SESSION['PPWKg']="";
                         
             if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 }
				
		$registerVP=$this->client_model->register_VP();
		$_SESSION['userid']=$registerVP; 
               	if($vptype=='restricted')
                {
                  $this->restricted_profile();
                }
                if($vptype=='fullprofile')
                {
                  $this->full_profile();
                }
				if($vptype=='vp_changemass')
                {
                  $this->changing_body_mass();   
                }
				// added
				 if($vptype =='sport_match')
                {
                  $this->sport_match();   
                }
				
				
				
    	}  
    	

   public function restricted_profile() {  
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];
             $data['fieldData'] = $this->body_model->fetchDetail('restricted_profile');
                 // kritika Code 
                   $age=$_SESSION['vp_age'];
                    $gender=$_SESSION['user_gender'];
                          if($_SESSION['filename']==""  && isset($_SESSION['is_virtual']))
                          {   
                           $file=$this->file_exist($age,$gender);	
						   $_SESSION['filename']=$file;
                          }	
                          $filename=$_SESSION['filename'];
				   if(!( isset($_SESSION['triceps']) && isset($_SESSION['biceps']) && isset($_SESSION['subscapular'])   ))
                   {
					   if(count($data['fieldData']) == 0 && isset($_SESSION['is_virtual']) )
					   {				   
                        $data['fieldData'] = $this->client_model->fetch_body_composition();
					   }	
                   }
				   
				   else{
					  // echo "Restricted Session set";
				   }
				   
			 $data['filename']=$filename;	   
             $this->load->view('restricted_profile',$data);	    
            
	}
    public function changing_body_mass() {
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['id'] = $_SESSION['userid'];
        $data['fieldData'] = $this->body_model->fetchDetail('restricted_profile');
        $age = $_SESSION['vp_age'];
        $gender = $_SESSION['user_gender'];
        if($_SESSION['filename']==""  && isset($_SESSION['is_virtual']) )
			  {   
			   $file=$this->file_exist($age,$gender);	
			   $_SESSION['filename']=$file;
			  }	
         $filename=$_SESSION['filename'];
	if (!( isset($_SESSION['triceps']) && isset($_SESSION['biceps']) && isset($_SESSION['subscapular']) )) {
            if (count($data['fieldData']) == 0  && isset($_SESSION['is_virtual']) ) {
                $data['fieldData'] = $this->client_model->fetch_body_composition();
            }
        } 
         $data['filename']=$filename;
		$this->load->view('changing_body_mass', $data);
    }
 
  //Sport Match
	public function sport_match()
	{
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['id'] = $_SESSION['userid'];
        $data['fieldData'] = $this->body_model->fetchDetail('restricted_profile');
        $age = $_SESSION['vp_age'];
        $gender = $_SESSION['gender'];
        //$filename = $this->file_exist($age, $gender);
        //$data['filename'] = $filename;
		
		//echo $gender;
		if($gender == 'M' || $gender == 'Male' || $gender == 'male')
		{ $gend = 'M'; }
		else{ $gend = 'F'; }
		
		// Addded
		if($_SESSION['filename']==""  && isset($_SESSION['is_virtual']))
			  {   
			   $file=$this->file_exist($age,$gend);	
			   $_SESSION['filename']=$file;
			  }	
         $filename=$_SESSION['filename'];
	     if (!( isset($_SESSION['triceps']) && isset($_SESSION['biceps']) && isset($_SESSION['subscapular']) )) {
            if (count($data['fieldData']) == 0   && isset($_SESSION['is_virtual']) ) {
                $data['fieldData1'] = $this->client_model->fetch_body_composition();
            }
        } 
         $data['filename']=$filename;
		
		// Added
		if($gender == 'M' || $gender == 'Male' || $gender == 'male')
		{ $gen = 'male'; }
		else
		{ $gen = 'female'; }
		
		//echo $gen;
		
        $data['fieldData'] = $this->body_model->fetch_anthropometetry($gen);
        $this->load->view('sport_match',$data);	
		 
	}
 
	// Blood Norms

	public function blood_norm()
	{
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['id'] = $_SESSION['userid'];
		$data['fieldData'] = $this->body_model->fetch_blood_norms();
        $this->load->view('bloodnormscreen',$data);			 
	}
	
	public function getnormvalue()
	{
		$gender = $this->input->post('gender');
		$age=$this->input->post('age');
		$population=$this->input->post('population');
		
		$data = $this->body_model->getnormdata($gender,$age,$population);
		echo json_encode($data);
	}
	
	
 
 //Get file_exist******************
   public function file_exist($age,$gender)
   {
       if($gender=='M')
       {
       $imagefolderpath='assets/images/Males/';    
       if($age < 30)
       {
       /*Random No*/
                $min=0;
                $max=64;
           $prefixtext="M20";    
       }
       if($age >= 30 && $age<40)
       {
                $min=0;
                $max=66;
           $prefixtext="M30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=53;
           $prefixtext="M40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="M50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=43;
           $prefixtext="M60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=42;
           $prefixtext="M70";    
       }
       if($age >= 80 )
       {
            $min=0;
            $max=43;
           $prefixtext="M80";    
       }
      }
      //If Gender Female
      if($gender=='F')
       {
       $imagefolderpath='assets/images/Females/';    
          if($age < 30)
       {
            $min=0;
            $max=59;
              $prefixtext="F20";    
       }
       if($age >= 30 && $age<40)
       {
            $min=0;
            $max=62;
           $prefixtext="F30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=71;
           $prefixtext="F40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="F50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=35;
           $prefixtext="F60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=31;
           $prefixtext="F70";    
       }
       if($age >= 80 )
       {
           $min=0;
            $max=30;
           $prefixtext="F80";    
       }
      }
       $ctr=0;
        while ($ctr<15) {
            $ctr++;
               $randomnum=rand($min,$max); 
              $filename =FCPATH."$imagefolderpath$prefixtext$randomnum.jpg";
              $images="$imagefolderpath$prefixtext$randomnum.jpg";
              if (file_exists($filename))
                {
              
                return ($images) ? $images : false;
               }
            else
             {
                if ($ctr == 10)
                return false;
             continue;
            }
        }
    }
	
    public function full_profile() {   
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('full_profile'); 
			 
			        $age=$_SESSION['vp_age'];
                    $gender=$_SESSION['user_gender'];
                          if($_SESSION['filename']=="" && isset($_SESSION['is_virtual']) )
                          {   
                           $file=$this->file_exist($age,$gender);	
						   $_SESSION['filename']=$file;
                          }	
                    $filename=$_SESSION['filename'];
			 
			        $key="fullprofile";
                   if(!( isset($_SESSION['triceps']) && isset($_SESSION['biceps']) && isset($_SESSION['subscapular']) ))
                   {
					   if(count($data['fieldData']) == 0 && isset($_SESSION['is_virtual']) )
						   {
							   $data['fieldData'] = $this->client_model->fetch_body_composition($key);
						   } 
				   } 
				   else
				   {
					   //echo "Full session set";
				   }
			 $data['filename']=$filename;	   
			 $this->load->view('full_profile',$data);	     
	}			
	
    public function error_analysis() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
			 
             $this->load->view('error_analysis',$data);	    
	 
	}	
	
    public function error_analysis_confidence() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('error_confidence');              
			 
             $this->load->view('error_analysis_confidence',$data);	    
	 
	}
        
        
        
    public function error_real_change() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('error_real_change');       
			 
             $this->load->view('error_real_change',$data);	    
	 
	}
        
         
    public function skinfold_Map() {   
	
            $data['css']  = $this->config->item('css');
            $data['base'] = $this->config->item('base_url');
            $data['image'] = $this->config->item('images');
             
            $triceps = $this->input->get("triceps") ;
            $subscapular = $this->input->get("subscapular") ;
            $illiac = $this->input->get("illiac") ;
            $thigh = $this->input->get("thigh") ;    
            $supraspinale = $this->input->get("supraspinale") ;    
            $biceps = $this->input->get("biceps") ;    
            $abdominal = $this->input->get("abdominal") ;    
            $calf = $this->input->get("calf") ;    
            $height = $this->input->get("height") ;    
            $body_mass = $this->input->get("body_mass") ;    
            
            $data['skinfolds'] = array("triceps"=>$triceps,"subscapular"=>$subscapular,"illiac"=>$illiac,"thigh"=>$thigh,"supraspinale"=>$supraspinale,"biceps"=>$biceps,"abdominal"=>$abdominal,"calf"=>$calf,"height"=>$height,"body_mass"=>$body_mass) ;             
            if($this->input->get("profile") == 'restricted') 
            {                
             $this->load->view('skinfold_Map_restricted',$data);	    
            }
            elseif($this->input->get("profile") == 'full') 
            {    
             $this->load->view('skinfold_Map_full',$data);	    
            }
	}	        


    public function restricted_actions() {   
	
           //Check if user exits the screen 
             if($this->input->post("exit_key") == 1)
             {
                 $num_of_rows =$this->body_model->saveRestrictedInfo();           
                
                 if($num_of_rows>0)
                  {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       $this->load->view('body_tests', $data);                     
                  }
                  else
                  {
                     echo "failure";
                  }                              	   
             }
             else
             {
                $num_of_rows =$this->body_model->saveRestrictedInfo(); 
                
                $zscores = $this->input->post() ;
                
                $data['css']  = $this->config->item('css');
                $data['base'] = $this->config->item('base_url');
                $data['image'] = $this->config->item('images');

              //Check button action  
                if($this->input->post("action_key") == 1) //Phantom ZScore
                { 
                  $data['phantom_Zscores'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"zscore_Triceps"=>$zscores["zscore_Triceps"], "zscore_Subscapular"=>$zscores["zscore_Subscapular"], "zscore_Biceps"=>$zscores["zscore_Biceps"], "zscore_Iliac"=>$zscores["zscore_Iliac"], "zscore_Supspinale"=>$zscores["zscore_Supspinale"], "zscore_Abdominal"=>$zscores["zscore_Abdominal"], "zscore_Thigh"=>$zscores["zscore_Thigh"], "zscore_Calf"=>$zscores["zscore_Calf"], "zscore_RelArmG"=>$zscores["zscore_RelArmG"], "zscore_FlexArmG"=>$zscores["zscore_FlexArmG"], "zscore_WaistG"=>$zscores["zscore_WaistG"], "zscore_HipG"=>$zscores["zscore_HipG"], "zscore_CalfG"=>$zscores["zscore_CalfG"], "zscore_Humerus"=>$zscores["zscore_Humerus"], "zscore_Femur"=>$zscores["zscore_Femur"])  ;                                  
                  $this->load->view('phantom_restricted',$data);	                 
                }
                elseif ($this->input->post("action_key") == 2) //Body Fat 
                {  
                  $data['bodyFat_values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"], "relArmG"=>$zscores["relArmG"], "flexArmG"=>$zscores["flexArmG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;                                    
                  $this->load->view('body_fat_restricted',$data);	  
                }
                elseif ($this->input->post("action_key") == 3) //Skinfolds
                {
                  $data['skinfolds_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"])  ;                
                  $this->load->view('skinfolds_restricted',$data);	  
                }
                elseif ($this->input->post("action_key") == 4) //Somatotype
                {  
                  $data['somatotype_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "supraspinale"=>$zscores["supraspinale"], "calf"=>$zscores["calf"], "flexArmG"=>$zscores["flexArmG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;  


				  $gender=$_SESSION['gender'];    
                    
                  if($gender == 'M' || $gender == 'Male' || $gender == 'male')
                    { $gen = 'male'; }
                    else
                    { $gen = 'female'; }

                    //echo $gen;
                    $data['fieldData'] = $this->body_model->fetch_sportslist($gen);


				  
                  $this->load->view('somatotype_restricted',$data);	  
                }
                else if($this->input->post("action_key") == 5) //Anthropometry
                { 
                  $data['anthropometry_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "thigh"=>$zscores["thigh"], "relArmG"=>$zscores["relArmG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"])  ;                                  
                  $this->load->view('anthropometry_restricted',$data);	                 
                }
              }
           } 
        
        
    public function full_actions() {   
             if($this->input->post("exit_key") == 1)
             {               
                 $num_of_rows =$this->body_model->saveFullInfo();           
                
                 if($num_of_rows>0)
                  {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       $this->load->view('body_tests', $data);                     
                  }
                  else
                  {
                     echo "failure";
                  }                      	   
            }             
            else
            {
                $num_of_rows =$this->body_model->saveFullInfo(); 
                
                $zscores = $this->input->post() ;
                //print_r($zscores); die;

                $data['css']  = $this->config->item('css');
                $data['base'] = $this->config->item('base_url');
                $data['image'] = $this->config->item('images');

              if($this->input->post("action_key") == 1) //Phantom ZScore
              {
                $data['phantom_Zscores'] = array("height"=>$zscores["height"],
                                                 "body_mass"=>$zscores["body_mass"],
                                                 "zscore_Triceps"=>$zscores["zscore_Triceps"],
                                                 "zscore_Subscapular"=>$zscores["zscore_Subscapular"], 
                                                 "zscore_Biceps"=>$zscores["zscore_Biceps"],
                                                 "zscore_Iliac"=>$zscores["zscore_Iliac"],
                                                 "zscore_Supspinale"=>$zscores["zscore_Supspinale"], 
                                                 "zscore_Abdominal"=>$zscores["zscore_Abdominal"],
                                                 "zscore_Thigh"=>$zscores["zscore_Thigh"],
                                                 "zscore_Calf"=>$zscores["zscore_Calf"],
                                                 "zscore_HeadG"=>$zscores["zscore_HeadG"],
                                                 "zscore_NeckG"=>$zscores["zscore_NeckG"], 
                                                 "zscore_RelArmG"=>$zscores["zscore_RelArmG"],
                                                 "zscore_FlexArmG"=>$zscores["zscore_FlexArmG"],
                                                 "zscore_ForearmG"=>$zscores["zscore_ForearmG"],
                                                 "zscore_WristG"=>$zscores["zscore_WristG"],
                                                 "zscore_ChestG"=>$zscores["zscore_ChestG"],
                                                 "zscore_WaistG"=>$zscores["zscore_WaistG"],
                                                 "zscore_HipG"=>$zscores["zscore_HipG"],
                                                 "zscore_ThighG"=>$zscores["zscore_ThighG"],
                                                 "zscore_MidThighG"=>$zscores["zscore_MidThighG"],                                                 
                                                 "zscore_CalfG"=>$zscores["zscore_CalfG"],
                                                 "zscore_AnkleG"=>$zscores["zscore_AnkleG"],
                                                 "zscore_AcRad"=>$zscores["zscore_AcRad"],
                                                 "zscore_RadStyl"=>$zscores["zscore_RadStyl"],
                                                 "zscore_midStylDact"=>$zscores["zscore_midStylDact"],
                                                 "zscore_Iliospinale"=>$zscores["zscore_Iliospinale"],
                                                 "zscore_Troch"=>$zscores["zscore_Troch"],
                                                 "zscore_TrochTib"=>$zscores["zscore_TrochTib"],                                                 
                                                 "zscore_TibLat"=>$zscores["zscore_TibLat"],
                                                 "zscore_TibMed"=>$zscores["zscore_TibMed"],
                                                 "zscore_Biac"=>$zscores["zscore_Biac"],
                                                 "zscore_Bideltoid"=>$zscores["zscore_Bideltoid"],
                                                 "zscore_Billio"=>$zscores["zscore_Billio"],
                                                 "zscore_Bitrochanteric"=>$zscores["zscore_Bitrochanteric"],
                                                 "zscore_Foot"=>$zscores["zscore_Foot"],
                                                 "zscore_Sitting"=>$zscores["zscore_Sitting"],
                                                 "zscore_TrChest"=>$zscores["zscore_TrChest"],                                                 
                                                 "zscore_ApChest"=>$zscores["zscore_ApChest"],
                                                 "zscore_ArmSpan"=>$zscores["zscore_ArmSpan"],                                       
                                                 "zscore_Humerus"=>$zscores["zscore_Humerus"],
                                                 "zscore_Femur"=>$zscores["zscore_Femur"])  ;

                  $this->load->view('phantom_full',$data);	                 
              }
              elseif ($this->input->post("action_key") == 2) //Body Fat 
              {                  
                  $data['bodyFat_values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"], "relArmG"=>$zscores["relArmG"], "flexArmG"=>$zscores["flexArmG"], "forearmG"=>$zscores["forearmG"], "wristG"=>$zscores["wristG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"], "thighG"=>$zscores["thighG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;                                    
                  $this->load->view('body_fat_full',$data);	  
                }
              elseif ($this->input->post("action_key") == 3) //Skinfolds
                {
                  $data['skinfolds_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"])  ;                
                  $this->load->view('skinfolds_full',$data);	  
                }
              elseif ($this->input->post("action_key") == 4) //Somatotype
                {
                  $data['somatotype_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "supraspinale"=>$zscores["supraspinale"], "calf"=>$zscores["calf"], "flexArmG"=>$zscores["flexArmG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;                
				  
				  
				  
				   $gender=$_SESSION['gender'];    
                    
                    if($gender == 'M' || $gender == 'Male' || $gender == 'male')
                    { $gen = 'male'; }
                    else
                    { $gen = 'female'; }

                    //echo $gen;
                    $data['fieldData'] = $this->body_model->fetch_sportslist($gen); 
				  
				  
				  
				  
				  
                  $this->load->view('somatotype_full',$data);	  
                }  
              else if($this->input->post("action_key") == 5) //Anthropometry
                { 
                  $data['anthropometry_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "biceps"=>$zscores["biceps"], "subscapular"=>$zscores["subscapular"], "thigh"=>$zscores["thigh"], "relArmG"=>$zscores["relArmG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"], "thighG"=>$zscores["thighG"])  ;                                  
                  $this->load->view('anthropometry_full',$data);	                 
                } 
              else if($this->input->post("action_key") == 6) //Fractionation
                { 
                 $data['fractionation_Values'] = array("height"=>$zscores["height"],
                                                 "body_mass"=>$zscores["body_mass"],
                                                 "zscore_Triceps"=>$zscores["zscore_Triceps"],
                                                 "zscore_Subscapular"=>$zscores["zscore_Subscapular"], 
                                                 "zscore_Biceps"=>$zscores["zscore_Biceps"],
                                                 "zscore_Iliac"=>$zscores["zscore_Iliac"],
                                                 "zscore_Supspinale"=>$zscores["zscore_Supspinale"], 
                                                 "zscore_Abdominal"=>$zscores["zscore_Abdominal"],
                                                 "zscore_Thigh"=>$zscores["zscore_Thigh"],
                                                 "zscore_Calf"=>$zscores["zscore_Calf"],
                                                 "zscore_HeadG"=>$zscores["zscore_HeadG"],
                                                 "zscore_NeckG"=>$zscores["zscore_NeckG"], 
                                                 "zscore_RelArmG"=>$zscores["zscore_RelArmG"],
                                                 "zscore_FlexArmG"=>$zscores["zscore_FlexArmG"],
                                                 "zscore_ForearmG"=>$zscores["zscore_ForearmG"],
                                                 "zscore_WristG"=>$zscores["zscore_WristG"],
                                                 "zscore_ChestG"=>$zscores["zscore_ChestG"],
                                                 "zscore_WaistG"=>$zscores["zscore_WaistG"],
                                                 "zscore_HipG"=>$zscores["zscore_HipG"],
                                                 "zscore_ThighG"=>$zscores["zscore_ThighG"],
                                                 "zscore_MidThighG"=>$zscores["zscore_MidThighG"],                                                 
                                                 "zscore_CalfG"=>$zscores["zscore_CalfG"],
                                                 "zscore_AnkleG"=>$zscores["zscore_AnkleG"],
                                                 "zscore_AcRad"=>$zscores["zscore_AcRad"],
                                                 "zscore_RadStyl"=>$zscores["zscore_RadStyl"],
                                                 "zscore_midStylDact"=>$zscores["zscore_midStylDact"],
                                                 "zscore_Iliospinale"=>$zscores["zscore_Iliospinale"],
                                                 "zscore_Troch"=>$zscores["zscore_Troch"],
                                                 "zscore_TrochTib"=>$zscores["zscore_TrochTib"],                                                 
                                                 "zscore_TibLat"=>$zscores["zscore_TibLat"],
                                                 "zscore_TibMed"=>$zscores["zscore_TibMed"],
                                                 "zscore_Biac"=>$zscores["zscore_Biac"],
                                                 "zscore_Bideltoid"=>$zscores["zscore_Bideltoid"],
                                                 "zscore_Billio"=>$zscores["zscore_Billio"],
                                                 "zscore_Bitrochanteric"=>$zscores["zscore_Bitrochanteric"],
                                                 "zscore_Foot"=>$zscores["zscore_Foot"],
                                                 "zscore_Sitting"=>$zscores["zscore_Sitting"],
                                                 "zscore_TrChest"=>$zscores["zscore_TrChest"],                                                 
                                                 "zscore_ApChest"=>$zscores["zscore_ApChest"],
                                                 "zscore_ArmSpan"=>$zscores["zscore_ArmSpan"],                                       
                                                 "zscore_Humerus"=>$zscores["zscore_Humerus"],
                                                 "zscore_Femur"=>$zscores["zscore_Femur"],
                                                 "triceps"=>$zscores["triceps"],
                                                 "subscapular"=>$zscores["subscapular"],
                                                 "thigh"=>$zscores["thigh"], 
                                                 "calf"=>$zscores["calf"],
                                                 "relArmG"=>$zscores["relArmG"],
                                                 "chestG"=>$zscores["chestG"],
                                                 "thighG"=>$zscores["thighG"],
                                                 "calfG"=>$zscores["calfG"])  ;
                  
                    $this->load->view('fractionation_full',$data);	                 
                }   
                    
            }       
	 
	}
          //Go on Body Composition Menu      
         public function go_on_menu() 
		 { 
		 $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       $this->load->view('body_tests', $data);         
		 }		 
        
      function saveClientErrorConfidenceInfo()
        {        
             $num_of_rows =$this->body_model->saveErrorConfidenceInfo(); 
             
             if($num_of_rows>0)
                {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       
                        if($this->input->post("exit_key") == "tem")
                        { 
                           $this->error_analysis() ;	                      
                        }
                        else if($this->input->post("exit_key") == "single_measure")
                        { 
                           $this->error_analysis_confidence() ;	                                     
                        }
                        else if($this->input->post("exit_key") == "real_change")
                        { 
                           $this->error_real_change() ;	                                     
                        }
                        else
                        {
                            $this->index();
                        }                   
                  }
                  else
                  {
                     echo "failure";
                  }   
        }
        
        
      function saveClientErrorRealChangeInfo()
        {        
             $num_of_rows =$this->body_model->savetErrorRealChangeInfo(); 
             
             if($num_of_rows>0)
                {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       
                        if($this->input->post("exit_key") == "tem")
                        { 
                           $this->error_analysis() ;	                      
                        }
                        else if($this->input->post("exit_key") == "single_measure")
                        { 
                           $this->error_analysis_confidence() ;	                                     
                        }
                        else if($this->input->post("exit_key") == "real_change")
                        { 
                           $this->error_real_change() ;	                                     
                        }
                        else
                        {
                            $this->index() ;
                        }                  
                  }
                  else
                  {
                     echo "failure";
                  }   
        }        
        
         
}
