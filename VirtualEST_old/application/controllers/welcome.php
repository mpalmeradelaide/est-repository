<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Welcome extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
   public $firstname ,$lastname ,$userid ,$dob,$country,$occupation,$gender;
    
        function __construct()
        {
            error_reporting(1);
             ini_set('memory_limit', '512M'); 
             parent::__construct();
             $this->load->helper('form');
             $this->load->helper('url');
             $this->load->model('client_model');
             $this->load->library('menu');
			 $this->load->library('statistical');
             session_start();
           

            
        }
	public function index($c =0)
	{
		/* session_destroy();
        session_start(); //to ensure you are using same session
        session_unset();
        session_destroy(); */
        session_start();
       
        error_reporting(0);
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        //$_SESSION['userid'] = 0;
		$filename=$_SESSION['filename'];
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['gender'] = 'M';
        $data['filename']=$filename;
        $data['projects'] = $this->client_model->getAllProjects();
        
        $this->load->view('client_detail', $data);
        
    }
    // for athelete random age    
/*  	public function get_randomage()
	{
			$today_date=date('Y-m-d');
			$start = strtotime("$today_date -40 year");
			$end= strtotime("$today_date -18 year");
			$int= mt_rand($start,$end);
			$between_date=date('Y-m-d',$int);
			$get_date_values=explode("-",$between_date)	;
			$valueyears=$get_date_values[0];
			$valuemonth=$get_date_values[1];
			$valuedate=$get_date_values[2];
			$dateofBirth = $valueyears.'/'.$valuemonth.'/'.$valuedate;
			//return $dateofBirth;
			echo $dateofBirth;
		
	} */	
		 
		
		
    
    public function VirtualpersonGeneration()
    {
		
		
    session_unset();
/*     session_destroy();
    session_write_close(); */
    session_regenerate_id(true); 
        $valarr=$_REQUEST['gen'];
        $valage=$_REQUEST['age'];
        $fname=$_REQUEST['firstname'];
        $lname=$_REQUEST['lastname'];
        $emailid=$_REQUEST['emailid'];
        $day=$_REQUEST['day'];
        $month=$_REQUEST['month'];
        $year=$_REQUEST['year'];
        $agerange=$_REQUEST['agerange'];
        $country=$_REQUEST['country'];    
       
        
        $subpop=$_REQUEST['subpop'];
     
        $data['randomdata'] = $this->client_model->generateRandomValues($valarr,$subpop,$vp_age);  
		
	// $data['randomdata'] = $this->client_model->generateRandomValues();   
     error_reporting(0);
        $menu = new Menu;
		$gender=$_SESSION['gender'];
		$age=$_SESSION['vp_age'];
         if($_SESSION['filename']=="")
        {   
        $file=$this->file_exist($age,$gender);	
        $_SESSION['filename']=$file;
        }
        $filename=$_SESSION['filename'];
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['filename']=$filename;
		$data['projects'] = $this->client_model->getAllProjects();
        $this->load->view('client_detail', $data);
		$_SESSION['is_virtual']=1;
   }
	
	//Session Destroy
    public function destroy_VP()
    {
		session_unset();
		redirect("welcome");
    }	
	 public function  VirtualpersonGeneration_change()
    {
        session_start();
        $valarr=$_REQUEST['gen'];
        $valage=$_REQUEST['age'];
        $fname=$_REQUEST['firstname'];
        $lname=$_REQUEST['lastname'];
        $emailid=$_REQUEST['emailid'];
        $day=$_REQUEST['day'];
        $month=$_REQUEST['month'];
        $year=$_REQUEST['year'];
        $agerange=$_REQUEST['agerange'];
        $country=$_REQUEST['country'];    
       // echo $agerange;
	   
	   
	    $case=$_REQUEST['case'];    
        $day=$_REQUEST['day'];    
        $month=$_REQUEST['month'];    
        $year=$_REQUEST['year'];    
        $range=$_REQUEST['range'];  
	   
        
        $subpop=$_REQUEST['subpop'];
       // echo $valage;
	     if($valarr == 'M')
         {
             $gender="Male";
         }
         else
         {
             $gender="Female";
         }
		 
		$file=$this->file_exist($valage,$gender);	
        $_SESSION['filename']=$file;
        $filename=$_SESSION['filename'];
        
        //$data['randomdata'] = $this->client_model->generateRandomValues($valarr,$subpop,$vp_age);  
		
		$data['randomdata'] = $this->client_model->generateRandomValues($valarr,$subpop,$valage,$case,$day,$month,$year,$range);  
	    
	//  $data['randomdata'] = $this->client_model->generateRandomValues();   
        error_reporting(0);
        $menu = new Menu;
		$gender=$_SESSION['gender'];
        $vppage=$_SESSION['vp_age'];
        $filename=$_SESSION['filename'];
        
        //echo "After Virtual Person".$filename;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['filename']=$filename;
		$data['projects'] = $this->client_model->getAllProjects();
        
        $_SESSION['is_virtual']=1;
        
		$this->load->view('client_detail', $data);
 }
	
	         //Get file_exist******************
   public function file_exist($age,$gender)
   {
       if($gender=='Male')
       {
       $imagefolderpath='assets/images/Males/';    
       
	    if($age <= 18)
       {
            $min=1;
            $max=26;
            $prefixtext="M18";    
       }
	   
	   if($age >= 19 && $age < 30)
       {
       /*Random No*/
                $min=0;
                $max=64;
           $prefixtext="M20";    
       }
       if($age >= 30 && $age<40)
       {
                $min=0;
                $max=81;
           $prefixtext="M30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=57;
           $prefixtext="M40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="M50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=43;
           $prefixtext="M60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=44;
           $prefixtext="M70";    
       }
       if($age >= 80 )
       {
            $min=0;
            $max=48;
           $prefixtext="M80";    
       }
      }
      //If Gender Female
      if($gender=='Female')
       {
       $imagefolderpath='assets/images/Females/';    
		 if($age <= 18)
       {
            $min=1;
            $max=22;
            $prefixtext="F18";    
       }	

		 if($age >= 19 && $age < 30)
       {
            $min=0;
            $max=60;
            $prefixtext="F20";    
       }
       if($age >= 30 && $age<40)
       {
            $min=0;
            $max=66;
           $prefixtext="F30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=71;
           $prefixtext="F40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="F50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=35;
           $prefixtext="F60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=31;
           $prefixtext="F70";    
       }
       if($age >= 80 )
       {
           $min=0;
            $max=31;
           $prefixtext="F80";    
       }
      }
       $ctr=0;
        while ($ctr<15) {
            $ctr++;
               $randomnum=rand($min,$max); 
              $filename =FCPATH."$imagefolderpath$prefixtext$randomnum.jpg";
              $images="$imagefolderpath$prefixtext$randomnum.jpg";
              if (file_exists($filename))
                {
              
                return ($images) ? $images : false;
               }
            else
             {
                if ($ctr == 10)
                return false;
             continue;
            }
        }
    }
	
	
	 public function file_exist_age()
   {
       
       $age=$this->input->post('age');
       $gen=$this->input->post('gender');
    
       if($gen == 'F' || $gen=='Female')
       {
           $gender='Female';
       }
       
       if($gen == 'M' || $gen=="Male")
       {
           $gender='Male';
       }
       
       if($gender=='Male')
       {
       $imagefolderpath='assets/images/Males/';    
       if($age < 30)
       {
       /*Random No*/
                $min=0;
                $max=64;
           $prefixtext="M20";    
       }
       if($age >= 30 && $age<40)
       {
                $min=0;
                $max=66;
           $prefixtext="M30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=53;
           $prefixtext="M40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="M50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=43;
           $prefixtext="M60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=42;
           $prefixtext="M70";    
       }
       if($age >= 80 )
       {
            $min=0;
            $max=43;
           $prefixtext="M80";    
       }
      }
      //If Gender Female
      if($gender=='Female')
       {
       $imagefolderpath='assets/images/Females/';    
          if($age < 30)
       {
            $min=0;
            $max=59;
              $prefixtext="F20";    
       }
       if($age >= 30 && $age<40)
       {
            $min=0;
            $max=62;
           $prefixtext="F30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=71;
           $prefixtext="F40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="F50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=35;
           $prefixtext="F60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=31;
           $prefixtext="F70";    
       }
       if($age >= 80 )
       {
           $min=0;
            $max=30;
           $prefixtext="F80";    
       }
      }
       $ctr=0;
        while ($ctr<15) {
            $ctr++;
            $bool="";
              $randomnum=rand($min,$max); 
              $filename =FCPATH."$imagefolderpath$prefixtext$randomnum.jpg";
              $images="$imagefolderpath$prefixtext$randomnum.jpg";
              if (file_exists($filename))
                {
                 $bool="true";   
                    /*$filename=$_SESSION['filename'];    
                   echo $images;*/
//                //return ($images) ? $images : false;
                }
           else
             {
                if ($ctr == 10)
                return false;
             continue;
            }
        }
       $_SESSION['filename']=$images;
       
       echo $_SESSION['filename'];
          
    }
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
        function saveClientInfo()
        {
        $this->load->library('form_validation');
                
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');		
		$this->form_validation->set_rules('daydropdown', 'Day of Birth', 'required|callback_daydropdown_check');
		$this->form_validation->set_rules('monthdropdown', 'Month of Birth', 'required|callback_monthdropdown_check');
		$this->form_validation->set_rules('yeardropdown', 'Year of Birth', 'required|callback_yeardropdown_check');		
		//$this->form_validation->set_rules('country', 'country', 'required|callback_country_check');
		//$this->form_validation->set_rules('occupation', 'occupation', 'required|callback_occupation_check');
		

		if ($this->form_validation->run() == TRUE) {
			
            $num_of_rows = $this->client_model->save();
           
            $dob = $this->input->post('daydropdown') . "/" . $this->input->post('monthdropdown') . "/" . $this->input->post('yeardropdown');

            if ($num_of_rows > 0) {
                $menu = new Menu;
                $data['menu'] = $menu->show_menu();
                $data['css'] = $this->config->item('css');
                $data['base'] = $this->config->item('base_url');
                $data['image'] = $this->config->item('images');
                $data['health_logo'] = $this->config->item('health_logo');
                $data['exercise_logo'] = $this->config->item('exercise_logo');
                $this->userid = $num_of_rows;
                $_SESSION['userid'] = $this->userid;
                $data['id'] = $_SESSION['userid'];
                $_SESSION['user_first_name'] =$this->input->post('first_name');
                $_SESSION['user_last_name'] = $this->input->post('last_name');
                $_SESSION['user_dob'] = $dob;
                $_SESSION['user_daydropdown'] = $this->input->post('daydropdown');
                $_SESSION['user_monthdropdown'] = $this->input->post('monthdropdown');
                $_SESSION['user_yeardropdown'] = $this->input->post('yeardropdown');
                $_SESSION['user_gender'] = $this->input->post('gender');
                $_SESSION['user_country'] = $this->input->post('country');
                $_SESSION['user_occupation'] = $this->input->post('occupation');
                $_SESSION['p_name'] = $this->input->post('p_name');
                $_SESSION['p_desc'] = $this->input->post('p_desc');
                $_SESSION['email'] = $this->input->post('email');
				$_SESSION['age_category'] = $this->input->post('age_cate');
                $_SESSION['subpopulation'] = $this->input->post('subpopulation');
				 $_SESSION['filename'] = $this->input->post('profile_img');
                 if($this->input->post('vp_age')!="")
                {
                $_SESSION['vp_age'] = $this->input->post('vp_age');
                $_SESSION['gender']=$this->input->post('gender');
                //Adding Random Mass BMI and Height
                $_SESSION['MASS'] = $this->input->post('MASS');
                $_SESSION['BMI'] = $this->input->post('BMI');
                $_SESSION['HEIGHT'] = $this->input->post('HEIGHT');
                }
				$data['image'] = $this->config->item('images');
                $data['firstName'] = $this->input->post('first_name');
                $data['lastName'] = $this->input->post('last_name');
                $this->firstname = $this->input->post('first_name');
                $this->lastname = $this->input->post('last_name');
                $this->dob = $dob;
                $this->gender = $this->input->post('gender');
                $this->country = $this->input->post('country');
                $this->occupation = $this->input->post('occupation');
                $this->p_name = $this->input->post('p_name');
                $this->p_desc = $this->input->post('p_desc');
                $this->email = $this->input->post('email');

                $data['screening'] = $this->client_model->fetchScreeningInfo();
                $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
                //Fetch Pre-Exercise Details 
                if(count($data['fieldData'])==0)
                 {
                 $data['fieldData'] = $this->client_model->fetch_preexercise_details();
                 }
                
                
                 if (isset($_POST['mysubmit1'])) {
                  $this->load->view('client_medical_history', $data);
                } elseif (isset($_POST['mysubmit2'])) {
                    $this->load->view('tests', $data);
                } elseif (isset($_POST['mysubmit3'])) {
                    $this->load->view('body_tests', $data);
                }
				  elseif (isset($_POST['mysubmit4'])) {
                     redirect("Body/sport_match");
                }
				 elseif (isset($_POST['mysubmit5'])) {
                     redirect("Vpgenerate");
                }
				 elseif (isset($_POST['mysubmit6'])) {
                     redirect("Body/blood_norm");
                }
            } else {
                echo "failure";
            }
        } else
		{
            $data['css']        = $this->config->item('css');
            $data['base'] = $this->config->item('base_url');
            $data['image'] = $this->config->item('images');
            $data['health_logo'] = $this->config->item('health_logo');
            $data['exercise_logo'] = $this->config->item('exercise_logo');
            $data['firstName'] = $this->input->post('first_name');
            $data['lastName'] = $this->input->post('last_name');
            $data['gender'] = $this->input->post('gender');
            $data['country'] = $this->input->post('country');
            $data['occupation'] = $this->input->post('occupation');
            $data['daydropdown'] = $this->input->post('daydropdown');
            $data['monthdropdown'] = $this->input->post('monthdropdown');
            $data['yeardropdown'] = $this->input->post('yeardropdown');
            $data['dob'] = $this->input->post('dob');
            $data['p_name'] = $this->input->post('p_name');
            $data['p_desc'] = $this->input->post('p_desc');
            $data['email'] = $this->input->post('email');
            $data['projects'] = $this->client_model->getAllProjects();
			$this->load->view('client_detail',$data);
		}
                
        } 
        
        function clearClientInfo()
            {
						 $data['css']        = $this->config->item('css');
                         $data['base']       = $this->config->item('base_url');
                         $data['image']       = $this->config->item('images');
						 $data['health_logo']       = $this->config->item('health_logo');	
						 $data['exercise_logo']       = $this->config->item('exercise_logo');
                         $data['firstName']       = "";
                         $data['lastName']       = "";
                         $data['gender']       = "";
                         $data['country']       = "";
                         $data['occupation']       = "";
                         $data['daydropdown']       = "";
                         $data['monthdropdown']       = "";
                         $data['yeardropdown']       = "";
                         $data['dob']       = "";
                         $data['email']       = "";
                         $data['p_name']       = "";
                         $data['p_desc']       = "";
                        $this->load->view('client_detail',$data);
		}
   
         
		 
		 
   function saveClientCVDriskinfo()
         {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            /*  $num_of_rows =$this->client_model->saveMedicalInfo();
           //  print_r($this->input->post());
      
            
                if($num_of_rows>0){
                 
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //  $data['id']       = $this->session->userdata('userid');
                    $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info'); */
               /*     if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  // kritika
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  }  */
				  
				  
				  
				/*   elseif (isset($_POST['submitMedical']))   {
                     //$num_of_rows =$this->client_model->saveMedicalInfo();
                  //    $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
               //    $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                  // $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                 //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                    $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                  
                   
                     $this->load->view('client_detail',$data); */

               ///   } 
                    
                  
                   //$this->load->view('client_medical_history', $data);
                   

             //   }else{
              //       echo "failure";
             //   }
     
        }
		 
		  
         function saveClientMedicalInfo()
         {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->client_model->saveMedicalInfo();
           //  print_r($this->input->post());
      
            
                if($num_of_rows>0){
                 
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //  $data['id']       = $this->session->userdata('userid');
                    $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
				  
				  
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  
				  
				  elseif (isset($_POST['submitMedical']))   {
                     //$num_of_rows =$this->client_model->saveMedicalInfo();
                  //    $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
               //    $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                  // $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                 //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                    $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                  
                   
                     $this->load->view('client_detail',$data);

                  } 
                    
                  
                   //$this->load->view('client_medical_history', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
        
        
        function saveClientPhysicalActivityInfo()
         {
            
           $num_of_rows =$this->client_model->savePhysicalActivityInfo();
      
                if($num_of_rows>0){
                   
                    
                   $menu = new Menu;
                   $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_risk_factor_info');
                    
                    
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                   //print_r($data); 
                   //die(__FILE__);
                   //$this->load->view('client_physical_activity', $data);
                    
                     if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();
                  } 
                  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				   
                   				  
				  
				  
				  elseif (isset($_POST['submitPhysicalActivity']))   {
                    // $num_of_rows =$this->client_model->savePhysicalActivityInfo();
                  //    $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
               //    $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
               //    $data['occupation']       = $this->session->userdata('user_occupation');
              //     $data['country']       = $this->session->userdata('user_country');
                   
                    
                   
                     $this->load->view('client_detail',$data);

                  } 
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function saveClientRiskFactorInfo()
         {
            // print_r($this->input->post());
          // die;
             $num_of_rows =$this->client_model->saveRiskFactorInfo();
         //$num_of_rows=1;
                if($num_of_rows>0){
                  
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                //  $data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                     $data['image']       = $this->config->item('images');
                      $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    //$this->load->view('client_risk_factor', $data);
                   
                    if (isset($_POST['mysubmit1'])) {
                    $this->saveClientInfoLink();

                  } elseif (isset($_POST['mysubmit2'])) {
                     $this->saveClientMedicalInfoLink();

                  } elseif (isset($_POST['mysubmit3'])) {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif (isset($_POST['mysubmit4'])) {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif (isset($_POST['mysubmit5'])) {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6'])) {
                    $this->fetchScreenInfoLink();

                  }
				 
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  
				  
				  elseif (isset($_POST['submitRiskFactor'])) {
                    
               //       $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                  // $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                //   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                   
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                   $data['projects'] = $this->client_model->getAllProjects();
                   
                   $this->load->view('client_detail',$data);

                  }                  

                }
                else
                {
                  echo "failure";
                }
     
        }
        
        function saveClientBodyCompositionInfo()
         {
            
            $num_of_rows =$this->client_model->saveBodyCompositionInfo();
            $num_of_rows=1;
                if($num_of_rows>0){
                   
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                  $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                  // $this->load->view('client_body_composition', $data);
                    if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  
				  
				  elseif (isset($_POST['submitBodyComposition']))   {
                    
                 //  $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                //   $data['dob']       = $this->session->userdata('user_dob');
                //   $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                //   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                  
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                      
                     $this->load->view('client_detail',$data);

                  } 
                  
                  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->fetchClientInfoLink();

                  }
                  
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function saveClientMeditationInfo()
         {
           // $this->input->post['medicalConditions'];
            //print_r($this->input->post());
            //die;
             $num_of_rows =$this->client_model->saveMeditationInfo();
         //$num_of_rows=1;
                if($num_of_rows>0){
              
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                    $data['css']        = $this->config->item('css');
                    $data['base']       = $this->config->item('base_url');
                   // $data['id']       = $this->session->userdata('userid');
                    $data['id']       = $_SESSION['userid'];
                    $data['image']       = $this->config->item('images');
                    $data['firstName']       =$this->firstname;
                    $data['lastName']       = $this->lastname;
                  $data['screening'] = $this->client_model->fetchScreeningInfo();
               
                  // $this->load->view('client_medication', $data);
                  
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  
				   elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  
				  
				  elseif (isset($_POST['submitMedication']))   {
                    
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                  // $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                 //  $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                 //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                      
                   $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                   $data['projects'] = $this->client_model->getAllProjects();
                   
                     $this->load->view('client_detail',$data);

                  }
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function fetchScreenInfo()
         {
		 
			
             //$this->client_model->fetchScreeningInfo();
			
            // $num_of_rows =$this->client_model->saveRiskFactorInfo();
				$num_of_rows=1;
                if($num_of_rows>0)
				{
                  
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
				   
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
				   //print_r($data);
				    
                 //  $data['screening'] = $this->client_model->fetchScreeningInfo();
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )  
				  {
				 
                    $this->saveClientBodyCompositionInfoLink();
					 //echo "fetchScreeningInfo";die;

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  
				  elseif (isset($_POST['submitScreening']))   {
                    
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                  // $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                  // $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                  // $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                  // $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                   
                        $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
					
                     $this->load->view('client_detail',$data);

                  }
                   
              //$this->load->view('client_screening', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
       
		  //Save CVD RISK
			function save_cvd_risk()
         {
            
                   $menu = new Menu;
                   $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   
                     if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();
                  } 
				  
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  elseif (isset($_POST['submitPhysicalActivity']))   {
                  
                     $this->load->view('client_detail',$data);
				  }
                 
     
        }
		
		
		
		
		
		
		  //Save Life Expectancy
			function save_life_expectancy()
         {
            
                   $menu = new Menu;
                   $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   
                     if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();
                  } 
				  
				  elseif ( isset($_POST['mysubmit8']) )   {
                    $this->life_expectancy();
                  } 
				  
				  elseif (isset($_POST['submitPhysicalActivity']))   {
                  
                     $this->load->view('client_detail',$data);
				  }
                 
     
        }
        
		
		
		
		
		
		
		
        
	   
   public function country_check()
    {
            if ($this->input->post('country') === 'selectcountry')  {
            $this->form_validation->set_message('country_check', 'Please choose your Country.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function occupation_check()
    {
            if ($this->input->post('occupation') === 'selectoccupation')  {
            $this->form_validation->set_message('occupation_check', 'Please choose your Occupation.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function daydropdown_check()
    {
            if ($this->input->post('daydropdown') === 'dd')  {
            $this->form_validation->set_message('daydropdown_check', 'Please choose your Day of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function monthdropdown_check()
    {
            if ($this->input->post('monthdropdown') === 'mm')  {
            $this->form_validation->set_message('monthdropdown_check', 'Please choose your Month of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function yeardropdown_check()
    {
            if ($this->input->post('yeardropdown') === '0000')  {
            $this->form_validation->set_message('yeardropdown_check', 'Please choose your Year of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function saveClientInfoLink()
        {
         
                               $data['css']        = $this->config->item('css');
                               $data['base']       = $this->config->item('base_url');
                              // $data['id']       =  $this->session->userdata('userid');
                               $data['id']       =  $_SESSION['userid'];
                               $data['image']       = $this->config->item('images');
                               $data['screening'] = $this->client_model->fetchScreeningInfo();
                               $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
                               $this->load->view('client_medical_history', $data);
        }
        
    function saveClientMedicalInfoLink()
        {
         
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   //$data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                    $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_physical_activity_info');
                     if(count($data['fieldData'])==0 && isset($_SESSION['is_virtual']))
					{
                 $data['fieldData'] = $this->client_model->fetch_physical_activity_details();
					}
				   $this->load->view('client_physical_activity', $data);
        } 
    function saveClientPhysicalActivityInfoLink()
         {
       
                   
                  
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 //  $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetail('client_risk_factor_info');
                     if(count($data['fieldData'])==0 && isset($_SESSION['is_virtual']))
                 {
                 $data['fieldData'] = $this->client_model->fetch_risk_factors_details();
                 }
				 $this->load->view('client_risk_factor', $data);
         }   
         
      function saveClientRiskFactorInfoLink()
         {
              
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   //$data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['screening'] = $this->client_model->fetchScreeningInfo();  
                   $data['fieldData'] = $this->client_model->fetchDetail('client_body_composition_info');
				   
				   // Kritika (Modified 28 september)
				   // if(count($data['fieldData'])==0)
                     if((!isset($_SESSION['triceps'])) && (!isset($_SESSION['biceps'])) && (!isset($_SESSION['subscapular'])) && isset($_SESSION['is_virtual'])) 
                     {
                       $data['fieldData'] = $this->client_model->fetch_body_composition();
                     }
				   
                  /*   if(count($data['fieldData'])==0)
                 {
                 $data['fieldData'] = $this->client_model->fetch_body_composition();
                 } */
				   $this->load->view('client_body_composition', $data);

     
        }   
        
         function saveClientBodyCompositionInfoLink()
         {
		
            
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  // $data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
				   
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetail('client_medication_info');
				   
				   if(count($data['fieldData']) == 0 && isset($_SESSION['is_virtual']))
                   {
                     $medical_history = $this->client_model->fetchDetail('client_medical_info'); 
                     $data['calculate_medication']=$this->client_model->calc_meditation($medical_history);
                   }
             
                   $data['fieldData'] = $this->client_model->fetchDetail('client_medication_info');
                   $this->load->view('client_medication', $data);
				
        }
		
		
    function cvd_risk() {
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        //$data['id']       = $this->session->userdata('userid');
        $data['id'] = $_SESSION['userid'];
        $data['fieldData'] = $this->client_model->cvdRiskInfo();
        $this->load->view('absolute_cvdrisk', $data);
    }
	
	//life_expectancy
     function life_expectancy() {
       $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['id'] = $_SESSION['userid'];
        $data['fieldData'] = $this->client_model->lifeExpectancyInfo();
        $this->load->view('lifeexpectancy', $data);
    }
		
		
        function fetchScreenInfoLink()
         {
          
				$num_of_rows=1;
                if($num_of_rows>0){
                   
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['myClass'] = $this;
                 
                   
                   $this->load->view('client_screening', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
       
        
         function fetchClientInfoLink()
         {
                 $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   $data['id']       = $this->session->userdata('userid');
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetailFromClientInfo('client_info');
                   $this->firstname =$this->input->post('first_name');
                   $this->lastname =$this->input->post('last_name');
                  /*
                   $data['firstName']       =$this->session->userdata('user_first_name');
                   $data['lastName']       = $this->session->userdata('user_last_name');
                   $data['dob']       = $this->session->userdata('user_dob');
                   $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                   $data['gender']       = $this->session->userdata('user_gender');
                   $data['occupation']       = $this->session->userdata('user_occupation');
                   $data['country']       = $this->session->userdata('user_country');
                  */ 
                     $this->load->view('client_detail',$data);
                 
        
             
         }
       
        function fetchClientInfo()
         {
            
             if (isset($_POST['submitMedical']))   {
                     $num_of_rows =$this->client_model->saveMedicalInfo();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
				   elseif ( isset($_POST['mysubmit7']) )   {
                    $this->cvd_risk();

                  } 
             
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $this->firstname =$this->input->post('first_name');
                   $this->lastname =$this->input->post('last_name');
                  
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                 //  $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                   
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                   
                     $this->load->view('client_detail',$data);
                  // $data['screening'] = $this->client_model->fetchScreeningInfo();
                  
                   //print_r($data); 
                  //
          
                   

               
     
        }
        
/*
         function updateClientInfo($rf)
         {
            $this->client_model->updateRiskFactorInfo($rf) ;
         }
*/

  function updateClientInfo($rf,$variable)
         {
            $this->client_model->updateRiskFactorInfo($rf,$variable) ;
         }
    

function updateMedicationInfo()
         {		
	    $column=$_GET['column'];
	    $status=$_GET['val'];
		  
            $this->client_model->updateMedicationInfo($column,$status) ;
         }    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */