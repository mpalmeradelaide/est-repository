<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Vpgenerate extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
         function __construct()
        {
             parent::__construct();
             error_reporting(1);
			 $this->load->helper('form');
             $this->load->helper('url');
             $this->load->model('virtual_model');
             $this->load->library('menu');
             $this->load->library('statistical');
             $this->load->dbutil();
             $this->load->helper('file');
             $this->load->helper('download');
             session_start();
             $date = str_replace('/', '-', $_SESSION['user_dob']);                      
			 $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
			 $dob = strtotime( $dateOfBirth);						 
			 $tdate = strtotime(date('Y-m-d'));			
			 $_SESSION['age'] = $this->getAge($dob, $tdate) ;
			 
			 if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 } 
         
        }
	function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        public function index($c =0)
	{
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['upload_count'] = '0';
		$data['selected_var'] = '';
        $data['test_var'] = $this->virtual_model->gettest_variables();  
        $this->load->view('generate_vp', $data);
       }
        
       //
      //export VP in csv file
       public function exportVP()
       {
      $no_of_record=$this->input->post('exportnum');
      //$no_of_record=5;
           $delimiter = ",";
        $newline = "\r\n";
		$filename = date('Y-m-d H:i:s')."_VP_$no_of_record.csv";    
		$selected_var=$this->input->post('selected_var');
        $selected_var=str_replace("name","CONCAT(firstname,' ',lastname) AS `Name`",$selected_var);
        //$selected_var=str_replace("gender","gender AS `Gender`",$selected_var);
        $selected_var=str_replace("subpopulation","subpopulation AS `Subpopulation`",$selected_var);
        $selected_var=str_replace("occupation","occupation AS `Occupation`",$selected_var);
        $selected_var=str_replace("country","country AS `Country`",$selected_var);
        //$selected_var=str_replace("age","age AS `Age`",$selected_var);
       // $selected_var=str_replace("mass","mass AS `Body mass (kg)`",$selected_var);
       // $selected_var=str_replace("height","height AS `Height (cm)`",$selected_var);
        $selected_var=str_replace("bmi","bmi AS `BMI`",$selected_var);
        //$selected_var=str_replace("heart_condition","gender AS Gender",$selected_var);
        $selected_var=str_replace("pain_in_chaist","pain_in_chaist AS `Pain in chest`",$selected_var);
       // $selected_var=str_replace("gender","gender AS Gender",$selected_var);
        $selected_var=str_replace("vj","vj AS `Vertical jump (cm)`",$selected_var);
        $selected_var=str_replace("triceps","triceps AS `Triceps skinfold`",$selected_var);
        $selected_var=str_replace("biceps","biceps AS `Biceps Skinfold`",$selected_var);
        $selected_var=str_replace("subscapular","subscapular AS `Subscapular Skinfold`",$selected_var);
        $selected_var=str_replace("Sum_of_3_skinfolds","Sum_of_3_skinfolds AS `Sum of 3 skinfolds`",$selected_var);
        $selected_var=str_replace("MBJSites","MBJSites AS `Muscle and joint sites`",$selected_var);
        $selected_var=str_replace("MBJOption","MBJOption AS `Muscle and joint sites option`",$selected_var);
        $selected_var=str_replace("L_grip","L_grip AS `Grip strength L`",$selected_var);
        $selected_var=str_replace("R_grip","R_grip AS `Grip strength R`",$selected_var);
        $selected_var=str_replace("calfmaximum","calfmaximum AS `Calf max (cm)`",$selected_var);
        $selected_var=str_replace("mean_Body_Fat","mean_Body_Fat AS `Sum of %body fat`",$selected_var);
        $selected_var=str_replace("fat_mass","fat_mass AS `Fat mass (kg)`",$selected_var);
        $selected_var=str_replace("muscle_mass","muscle_mass AS `muscle mass (kg)`",$selected_var);
        $selected_var=str_replace("bone_mass","bone_mass AS `bone mass (kg)`",$selected_var);
        $selected_var=str_replace("residual_mass","residual_mass AS `residual mass (kg)`",$selected_var);
        $selected_var=str_replace("total_work_done_in30s_kJ","total_work_done_in30s_kJ AS `total work done in 30s (kJ)`",$selected_var);
        $selected_var=str_replace("PPW_val","PPW_val AS `Peak power (W)`",$selected_var);
        $selected_var=str_replace("PPWKg","PPWKg AS `Peak power (W/kg)`",$selected_var);
        $selected_var=str_replace("shuttle_VO2max","shuttle_VO2max AS `Shuttle test VO2max`",$selected_var);
        $selected_var=str_replace("Watts_HRmax","Watts_HRmax AS `Pred watts at HR max`",$selected_var);
        $selected_var=str_replace("Predicted_VO2_submax_test","Predicted_VO2_submax_test AS `Submax test VO2max (HR)`",$selected_var);
        $selected_var=str_replace("Max_Power_RPE","Max_Power_RPE AS `Pred watts at RPE max`",$selected_var);
        $selected_var=str_replace("VO2max_RPE","VO2max_RPE AS `Submax test VO2max (RPE)`",$selected_var);
        
		$selected_var=str_replace("Continous_vo2max_treadmill","Continous_vo2max_treadmill AS `Continuous treadmill VO2max`",$selected_var);
        $selected_var=str_replace("Discontinous_vo2max_distreadmill","Discontinous_vo2max_distreadmill AS `Discontinous treadmill VO2max`",$selected_var);
       
        
		
		$selected_var=str_replace("Bench_press","Bench_press_kg,Bench_press_reps",$selected_var);
        $selected_var=str_replace("Arm_curl","Arm_curl_kg,Arm_curl_reps",$selected_var);
        $selected_var=str_replace("Lateral_pulldown","Lateral_pulldown_kg,Lateral_pulldown_reps",$selected_var);
        $selected_var=str_replace("Leg_press","Leg_press_kg,Leg_press_reps",$selected_var);
        $selected_var=str_replace("Leg_extension","Leg_extension_kg,Leg_extension_reps",$selected_var);
        $selected_var=str_replace("Leg_curl","Leg_curl_kg,Leg_curl_reps",$selected_var);
	    $query = "SELECT id,$selected_var from virtual_person_info order by id desc limit $no_of_record";
	    $result = $this->db->query($query);
	    $data = $this->dbutil->csv_from_result($result, $delimiter, $newline);
	   force_download($filename, $data);
		
	}        

//Change 
    public function VirtualpersonGeneration()
    {
        $selected_var="";
        $selected_variable=array(); 
        $no_of_vp=$this->input->post('no_of_vp');       
        $gender=$this->input->post('gender');
        $subpopulation=$this->input->post('subpopulation');
		$selected_variable=$this->input->post('sel_itm2');
       if(count($selected_variable)>0)
        {   
        $selected_var=implode(",",$selected_variable);
        }
        $count=0;
for($i=1;$i<=$no_of_vp;$i++)
{
       $physical_activity=array();
       $riskfactor=array();
	   $fligt_contact_time=array();
       $data = $this->virtual_model->generateRandomValues($gender,$subpopulation);   
       $firstname=$data['firstname'];
       $lastname=$data['lastname'];
       $dob = $data['daydropdown'] . "/" . $data['monthdropdown'] . "/" .$data['yeardropdown'];
       $occupation=$data['occupation'];
       $country=$data['country']['countryvalue'];
       $age=$data['vp_age'];
       $mass=$data['MASS'];
       $height=$data['HEIGHT'];
       $bmi=$data['BMI'];
       $getgender=$data['gender'];
       $sub_population=$data['sub_population'];
        $agerange=$data['age_category'];     
       // echo $sub_population; die;  
            
       if($getgender=='Male')
       {
        $user_gender='M';   
       }
       if($getgender=='Female')
       {
        $user_gender='F';   
       }
       $user_subpopulation=$data['sub_population'];
   
/// Pre-exercise Screening ///
        /***********Medical History***********/
   $medical_his_data = $this->virtual_model->fetch_preexercise_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);     
 
//GET Medication & Condition
   $medication_condition = $this->virtual_model->medication_condition($medical_his_data,$user_gender);    
   
   //GET PHYSICAL ACTIVITY
   $physical_activity=$this->virtual_model->VP_physical_activity($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);
   
   //GET RISK FACTOR
   $riskfactor=$this->virtual_model->fetch_risk_factors_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);
   
 //GET Fitness Module VERICAL JUMP
  $virticaljump = $this->virtual_model->get_Vertical_jump($age,$user_subpopulation,$user_gender);       
  
  $fligt_contact_time = $this->virtual_model->get_Flight_ratio($age,$user_subpopulation,$user_gender);  

// Strength Tests
  $Strength_Tests=$this->virtual_model->strength_test($age,$mass,$height,$bmi,$user_gender,$user_subpopulation); 	
  // kritika Code


  // Peak Test
  $peaktest= $this->virtual_model->peak_power_test($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange); 
    
  
  $vo2max=$this->virtual_model->get_VO2max($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange); 

  
  
   
  // PWC Screen get variables
  $pwc_vo2max = $vo2max['vo2max'];
  $pwc_vo2maxLM=$vo2max['vo2maxLM'];
  $pwc_vo2rest=$vo2max['vo2rest'];
  
  $pwc_stage1HR = $vo2max['stage1HR'];
  $pwc_stage2HR = $vo2max['stage2HR'];
  $pwc_stage3HR = $vo2max['stage3HR']; 
  
  $pwc_stage1WATT = $vo2max['stage1WATT'];
  $pwc_stage2WATT = $vo2max['stage2WATT'];
  $pwc_stage3WATT = $vo2max['stage3WATT'];
 
  $pwc_stage1RPE = $vo2max['stage1RPE'];
  $pwc_stage2RPE = $vo2max['stage2RPE'];
  $pwc_stage3RPE = $vo2max['stage3RPE'];

 
  $pwc_HRmax = $vo2max['HRmax'];   
  $pwc=$this->virtual_model->calculate_PWC($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$pwc_vo2max,$pwc_stage1HR,$pwc_stage2HR,$pwc_stage3HR,$pwc_stage1WATT,$pwc_stage2WATT,$pwc_stage3WATT,$pwc_HRmax,$pwc_stage1RPE,$pwc_stage2RPE,$pwc_stage3RPE);
  
  
  $pwc_rpe=$this->virtual_model->calculate_PWC_RPE($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$pwc_vo2max,$pwc_stage1WATT,$pwc_stage2WATT,$pwc_stage3WATT,$pwc_HRmax,$pwc_stage1RPE,$pwc_stage2RPE,$pwc_stage3RPE);
  
 // print_r($pwc_rpe); die;
  
   //20 m shuttle test	
  $shuttletest_vo2max = $vo2max['vo2max']; 
  $twenty_shuttletest = $this->virtual_model->twenty_shuttle_test($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$shuttletest_vo2max); 
    
  // print_r($twenty_shuttletest);
   // Bike Tests
  $biketest_vo2max = $vo2max['vo2max'];
  $biketest_vo2maxLM=$vo2max['vo2maxLM'];
  $biketest_vo2rest=$vo2max['vo2rest'];
  $biketest=$this->virtual_model->calculate_biketest($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$biketest_vo2max,$biketest_vo2maxLM,$biketest_vo2rest); 


  // 30s total work
  $total_work=$this->virtual_model->total_work($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange); 
   
 
      
  
   
     // Required Details       
  $body_composition_data = $this->virtual_model->body_composition_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation);
  
 // get SItes Points            
  $sites_points = $this->virtual_model->mbjsites_data();      
  if($sites_points['mbjsite'] == 0)
  {
      $sites_point="0";
  } else
  { $sites_point=implode(" ," , array_unique($sites_points['mbjsite']) ); }
  if($sites_points['MBJoption'] == "")
  { $mbjoption= ""; }
    else
    {   $mbjoption= $sites_points['MBJoption']; }
//  kritika Code            
             
  
  $setuserdata=array('firstname'=>$firstname,'lastname'=>$lastname,'dob'=>$dob,'Gender'=>$getgender,'subpopulation'=>$sub_population,'occupation'=>$occupation,'country'=>$country,'Age'=>$age,'Mass'=>$mass,'Height'=>$height,'bmi'=>$bmi);
  
            
    // kritika Code  
   // PWC Variables
   
   if(count($pwc_rpe) > 0)
   {
	    $pwc_rpe_array=array(
                 'Max_Power_RPE'=>$pwc_rpe['Max_Power_RPE'] , 
	             'VO2max_RPE'=>$pwc_rpe['VO2max_RPE'] 
				 );
   }
   
   if(count($pwc) > 0)
   {
	   $pwc_array=array(
                 'Watts_HRmax'=>$pwc['max_wl_regheartrate'] ,
                 'Predicted_VO2_submax_test'=>$pwc['predicted_VO_regheartrate']
				 );
   } 
	//print_r($pwc_array); die;
	//Peak Power
	if(count($peaktest) > 0)
	{
		 $peak_power_array=array(
                 'PPW_val'=>$peaktest['watts_val'] ,
                 'PPWKg'=>$peaktest['PPWKg'] 				 
				 );
	}
	
	
	//20m shuttle test
	if(count($twenty_shuttletest) > 0 )
	{	
        $shuttle_test_array=array(
                 'shuttle_level'=>$twenty_shuttletest['shuttle_level'] ,
                 'shuttle_VO2max'=>$twenty_shuttletest['shuttle_VO2max'] 				 
				 );
			
	//	$shuttle_test = array('shuttle_test'=>$shuttle_val);
	}  
	
	
	 // #30S Total Work
	if(count($total_work) > 0)
	{
		 $total_work_array=array(
                 'total_work_done_in30s_kJ'=>$total_work['total_work_done_in30s_kJ']               
				 );
	}
	
	// Bike Test
	if(count($biketest) > 0)
	{
		 $vo2max_biketest_array=array(
                 'Continous_vo2max_treadmill'=>$biketest['Continous_vo2max_treadmill'],
                 'Discontinous_vo2max_distreadmill'=>$biketest['Discontinous_vo2max_distreadmill'],
                 'Ergometer_Continous_vo2max'=>$biketest['Ergometer_Continous_vo2max']
				 );
	}
	
     if(count($vo2max) > 0)
	{
		 $vo2max_array=array(
                 'vo2max'=>$vo2max['vo2max']);
	}
    
	
    // Body Composition : kritika
	if(count($body_composition_data)>0)
    {
            $body_composition_data_array=array(
              //'measured_height'=>$body_composition_data[0]->option_height_measured,
             // 'measured_weight'=>$body_composition_data[0]->option_weight_measured,   
             // 'measuredbmi'=>$body_composition_data[0]->option_bmi,
              //'measured_population'=>$body_composition_data[0]->option_measuredpopulation,
              //'measuredwaist'=>$body_composition_data[0]->option_waist,
              'hip'=>$body_composition_data[0]->option_hip,
              'waist_hip_ratio'=>$body_composition_data[0]->option_whr,
              'triceps'=>$body_composition_data[0]->option_triceps,
              'biceps'=>$body_composition_data[0]->option_biceps,
              'subscapular'=>$body_composition_data[0]->option_subscapular,
              'Sum_of_3_skinfolds'=>$body_composition_data[0]->option_sos,
              'self_report_height'=>$body_composition_data[0]->option_height,
              'self_report_weight'=>$body_composition_data[0]->option_weight,
                                         
                                         
              'illiacrest'=>$body_composition_data[0]->iliac_crest,
              'supraspinale'=>$body_composition_data[0]->supraspinale,
              'abdominal'=>$body_composition_data[0]->abdominal,
              'frontthigh'=>$body_composition_data[0]->thigh,
              'Medialcalf'=>$body_composition_data[0]->calf,
              'midaxilla'=>$body_composition_data[0]->mid_axilla,
              'armrelaxed'=>$body_composition_data[0]->relArmG,
              'armflexed'=>$body_composition_data[0]->flexArmG,
              'waistminimum'=>$body_composition_data[0]->waistG,
              'Gluteal_hips'=>$body_composition_data[0]->hipG,
              'calfmaximum'=>$body_composition_data[0]->calfG,
              'humerus'=>$body_composition_data[0]->humerus,
              'femur'=>$body_composition_data[0]->femur,
                                                      
                                                      
              'head'=>$body_composition_data[0]->headG,
              'neck'=>$body_composition_data[0]->neckG,
              'forearmmax'=>$body_composition_data[0]->forearmG,
              'waist_distal_styloids'=>$body_composition_data[0]->wristG,
              'chest_mesosternale'=>$body_composition_data[0]->chestG,
              'thigh_onecmdistal'=>$body_composition_data[0]->thighG,
              'thigh_mid'=>$body_composition_data[0]->midThighG,
              'ankle_minimum'=>$body_composition_data[0]->ankleG,
              'acromiale_radiale'=>$body_composition_data[0]->acRad,
              'radiale_styl'=>$body_composition_data[0]->radStyl,
              'midstyl_dacty'=>$body_composition_data[0]->midStylDact,
              'illiospinale'=>$body_composition_data[0]->iliospinale,
              'trochant_tibial'=>$body_composition_data[0]->trochTib,
              'tib_med_sphytib'=>$body_composition_data[0]->tibMed,
              'trochanterion'=>$body_composition_data[0]->troch,
              'tibiale_laterale'=>$body_composition_data[0]->tibLat,
              'biacromial'=>$body_composition_data[0]->biac,
              'bideltoid'=>$body_composition_data[0]->bideltoid,
              'biiliocristal'=>$body_composition_data[0]->billio,
              
              'bitrochanteric'=>$body_composition_data[0]->bitrochanteric,
              'footlength'=>$body_composition_data[0]->foot,
              'sitting_length'=>$body_composition_data[0]->sitting,
              'transverse_chest'=>$body_composition_data[0]->trChest,
              'A_PChest'=>$body_composition_data[0]->apChest,
              'arm_span'=>$body_composition_data[0]->armSpan,
              
              // BODY FAT mean
              'mean_Body_Fat'=>$body_composition_data[0]->meanbodyfat,
          
              // Fractionation Variables                                                  
              'fat_mass'=>$body_composition_data[0]->fat_mass,
              'muscle_mass'=>$body_composition_data[0]->muscle_mass,
              'bone_mass'=>$body_composition_data[0]->bone_mass,
              'residual_mass'=>$body_composition_data[0]->residual_mass,


              // Somatotype Variables
              'ectomorph_score'=>$body_composition_data[0]->ectomorph_score,
              'mesomorph_score'=>$body_composition_data[0]->mesomorph_score,
              'endomorph_score'=>$body_composition_data[0]->endomorph_score
          
          );
    }      
            
            
    if(count($sites_points['mbjsite'] > 0 ) && !is_null($sites_point))
    {
        $sites_arr=array('MBJSites'=>$sites_point,'MBJOption'=>$mbjoption);
    }
          
                           
   // print_r($sites_arr);                           
                           
    // kritika Code        
            
         
    if(count($medical_his_data)>0)
    {
     
      $medicalcond=array('heart_condition'=>$medical_his_data[0]->option_1
              ,'pain_in_chaist'=>$medical_his_data[0]->option_2,
              'feel_faint'=>$medical_his_data[0]->option_3,
              'asthma_attack'=>$medical_his_data[0]->option_4,
              'diabetes'=>$medical_his_data[0]->option_5,
              'bone_muscle_prob'=>$medical_his_data[0]->option_6,
              'other_medical_cond'=>$medical_his_data[0]->option_7,
          );   
        
    }
    
    //Physical Activity
    if(count($physical_activity)>0)
    {
     
      $physical_act_array=array('walking'=>$physical_activity[0]->option_1
              ,'minutes_walking'=>$physical_activity[0]->option_2,
              'vigorous'=>$physical_activity[0]->option_3,
              'minute_vigorous'=>$physical_activity[0]->option_4,
              'moderate'=>$physical_activity[0]->option_5,
              'minute_moderate'=>$physical_activity[0]->option_6,
              'tv_hours_games'=>$physical_activity[0]->option_7,
              'driving_hours'=>$physical_activity[0]->option_8,
              'is_setting_long_period'=>$physical_activity[0]->option_9
          );   
        
    }
    //RISK FACTOR
	 if(count($riskfactor)>0)
    {
     
      $riskfactor_array=array('family_history'=>$riskfactor[0]->option_1
              ,'family_gender'=>$riskfactor[0]->option_gender,
              'age_at_heart_attack'=>$riskfactor[0]->option_age,
              'current_smoker'=>$riskfactor[0]->option_2,
              'how_many'=>$riskfactor[0]->option_smoke,
              'quit_smoke_recently'=>$riskfactor[0]->option_3,
              'how_many_recently'=>$riskfactor[0]->option_smoke_6,
              'self_report_BP'=>$riskfactor[0]->option_4,
              'self_report_Chol'=>$riskfactor[0]->option_5,
              'self_report_Glucose'=>$riskfactor[0]->option_6,
              'Measured_SBP'=>$riskfactor[0]->option_7,
              'Measured_DBP'=>$riskfactor[0]->option_8,
              'Measured_fast_blood_glucose'=>$riskfactor[0]->option_12,
              'Measured_blood_Cholesterol'=>$riskfactor[0]->option_11,
              'HDL'=>$riskfactor[0]->option_9,
              'LDL'=>$riskfactor[0]->option_10,
              'triglycerides'=>$riskfactor[0]->option_13
          );   
        
    }
     
            
    if(count($medication_condition)>0)
    {
      $medication=array('medication_selected'=>$medication_condition['medication_selected']
              ,'medical_cond_selected'=>$medication_condition['medical_cond_selected']
          );   
        
    }
    
    if(count($virticaljump)>0)
    {
     
      $vjarray=array('vj'=>$virticaljump['vj']
              ,'perform_percent'=>$virticaljump['perform_percent']
              ,'z_score'=>$virticaljump['z_score']
          );   
        
    }
    
     if(count($fligt_contact_time)>0)
    {
      $flightarray=array('FTCT'=>$fligt_contact_time['FTCT']
              ,'flight_percent'=>$fligt_contact_time['flight_percent']
              ,'flight_z_score'=>$fligt_contact_time['flight_z_score']
          );   
    }
	//Strength
     if(count($Strength_Tests)>0)
    {
     
      $strength_array=array('L_grip'=>$Strength_Tests['GSLEFT'],'R_Grip'=>$Strength_Tests['GSRIGHT'],'Bench_press_reps'=>$Strength_Tests['BPreps'],
          'Bench_press_kg'=>$Strength_Tests['BPmass'],'Arm_curl_reps'=>$Strength_Tests['ACreps'],'Arm_curl_kg'=>$Strength_Tests['ACmass'],
          'Lateral_pulldown_reps'=>$Strength_Tests['LPDreps'],'Lateral_pulldown_kg'=>$Strength_Tests['LPDmass'],'Leg_press_reps'=>$Strength_Tests['LPreps'],
          'Leg_press_kg'=>$Strength_Tests['LPmass'],'Leg_extension_reps'=>$Strength_Tests['LEreps'],'Leg_extension_kg'=>$Strength_Tests['LEmass'],
          'Leg_curl_reps'=>$Strength_Tests['LCreps'],'Leg_curl_kg'=>$Strength_Tests['LCmass']
          );   
        
   
    }
    $finaldata=array_merge($setuserdata,$medicalcond,$physical_act_array,$riskfactor_array,$medication,$vjarray,$fligt_contact_time,$body_composition_data_array,$sites_arr,$strength_array,$vo2max_array,$vo2max_biketest_array,$total_work_array,$shuttle_test_array,$peak_power_array,$pwc_array,$pwc_rpe_array);
    //print_r($finaldata);  die;
            
    $addVpData = $this->virtual_model->add_VP_Data($finaldata); 
    $count++; 
}
        
        
        
        
    $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['upload_count'] = $count;
	    $data['selected_var']=$selected_var;
        $data['test_var'] = $this->virtual_model->gettest_variables();   
        $this->load->view('generate_vp', $data);
    }
    
         
}

/* End of file Vpgenerate.php */
/* Location: ./application/controllers/welcome.php */