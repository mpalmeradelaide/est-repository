<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Fitness extends CI_Controller {

    public function __construct()
   {  
    // Call the Controller constructor
        
            error_reporting(1);
            ini_set('memory_limit', '512M');
            parent::__construct();
			$this->load->helper(array('form', 'url','common'));
			$this->load->library(array('session','form_validation'));  
            $this->load->model('fitness_model','',TRUE);
			 $this->load->model('client_model');
            $this->load->helper('text');               
	        $this->load->library('menu');
            session_start();
			
			 $date = str_replace('/', '-', $_SESSION['user_dob']);                      
			 $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
			 $dob = strtotime( $dateOfBirth);						 
			 $tdate = strtotime(date('Y-m-d'));			
			 $_SESSION['age'] = $this->getAge($dob, $tdate) ;
			 
			 if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 }
			
  }  
	
	public function index() {   
	
			 $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');     
			 $data['health_logo'] = $this->config->item('health_logo');
					 
             $this->load->view('tests',$data);	    
	 
	}
	
	function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
        
        public function get_view_ajax_male()
        {          
            $sports_array = $this->fitness_model->all_sports_male();            
         
            echo "<option>Sports</option>";
            
            foreach($sports_array as $val)
             {
              echo "<option>".$val->sport."</option>";
             }
        }

         public function get_view_ajax_female()
        {           
            $sports_array = $this->fitness_model->all_sports_female();            
        
            echo "<option>Sports</option>";
            
            foreach($sports_array as $val)
              {
               echo "<option>".$val->sport."</option>";
              }
        }
		
		
		
	   public function calcu_biketest()
	   {
		    $vo2max=$this->input->post('vo2max');
			$biketest = $this->fitness_model->calculate_norm($vo2max); 
			echo $biketest; 
	   }
		
		
		
		
		
	  //Change 
		public function FitnessVirtualpersonGeneration()
		{
		session_unset();
		session_destroy();
		session_write_close();
		session_regenerate_id(true);
		$vptype=$this->input->post('vptype');
		session_start();
				$data['randomdata'] = $this->client_model->generateRandomValues();   
				$dob = $data['randomdata']['daydropdown']. "/" .$data['randomdata']['monthdropdown']. "/" .$data['randomdata']['yeardropdown'];
                $_SESSION['age'] = $data['randomdata']['vp_age'];
                $_SESSION['user_first_name']=$data['randomdata']['firstname'];
                $_SESSION['user_last_name']=$data['randomdata']['lastname'];
                $_SESSION['user_dob'] = $dob;
                $_SESSION['user_daydropdown'] = $data['randomdata']['daydropdown'];
                $_SESSION['user_monthdropdown'] =$data['randomdata']['monthdropdown'];
                $_SESSION['user_yeardropdown'] = $data['randomdata']['yeardropdown'];
                if($data['randomdata']['gender']=='Male')
                {
                 $_SESSION['user_gender'] ='M';    
                }
                else
                {
                $_SESSION['user_gender'] ='F';    
                }  
				 $_SESSION['is_virtual'] ='1';    
                
				$_SESSION['user_country'] = $data['randomdata']['country']['countrykey'];
                $_SESSION['user_occupation'] = $data['randomdata']['occupation'];
                $_SESSION['p_name'] = '';
                $_SESSION['p_desc'] ='';
                $_SESSION['email'] ='';
              
                $_SESSION['subpopulation'] = $data['randomdata']['sub_population'];
                 $_SESSION['vp_age'] = $data['randomdata']['vp_age'];
                 $_SESSION['age'] = $data['randomdata']['vp_age'];
                 $_SESSION['gender']=$data['randomdata']['gender'];
                //Adding Random Mass BMI and Height
                $_SESSION['MASS'] = $data['randomdata']['MASS'];
                $_SESSION['BMI'] = $data['randomdata']['BMI'];
                $_SESSION['HEIGHT'] = $data['randomdata']['HEIGHT'];
                $_SESSION['vj']="";
				$_SESSION['FTCT']="";
				$_SESSION['PPWKg']="";
                         
             if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 }
				$_SESSION['showvp']='1';  
				$registerVP=$this->fitness_model->register_VP();
				$_SESSION['userid']=$registerVP;        
				if($vptype=='vjump_vp')
                {
                $this->vertical_jump();      
                }
                if($vptype=='flight_vp')
                {
                 $this->flight_time();     
                }  
				if($vptype=='peak_power_vp')
                {
                 $this->peak_power();     
                }
				 if($vptype=='strength_vp')
                {
                 $this->strength();     
                }  
				 if($vptype=='vptotalwork')
                {
                 $this->total_work_done_30s();     
                }
				if($vptype=='vpvo2max')
                {
                 $this->submaximal_test();     
                }
				if($vptype=='vpshuttle_test')
                {
                 $this->shuttle_test();     
                }
				 if($vptype=='vp_measured_vo2max')
                {
                 $this->v02max_test();     
                } 
			 if($vptype=='vp_lactate')
                {
                $this->lactate_threshold();
                }	
			 if($vptype=='maod_vp')
                {
                $this->maod_test();
                }		
    	
		}

         //Get file_exist******************
   public function file_exist($age,$gender)
   {
       if($gender=='M')
       {
       $imagefolderpath='assets/images/Males/';    
       if($age < 30)
       {
       /*Random No*/
                $min=0;
                $max=64;
           $prefixtext="M20";    
       }
       if($age >= 30 && $age<40)
       {
                $min=0;
                $max=66;
           $prefixtext="M30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=53;
           $prefixtext="M40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="M50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=43;
           $prefixtext="M60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=42;
           $prefixtext="M70";    
       }
       if($age >= 80 )
       {
            $min=0;
            $max=43;
           $prefixtext="M80";    
       }
      }
      //If Gender Female
      if($gender=='F')
       {
       $imagefolderpath='assets/images/Females/';    
          if($age < 30)
       {
            $min=0;
            $max=59;
              $prefixtext="F20";    
       }
       if($age >= 30 && $age<40)
       {
            $min=0;
            $max=62;
           $prefixtext="F30";    
       }
       if($age >= 40 && $age<50)
       {
            $min=0;
            $max=71;
           $prefixtext="F40";    
       }
       if($age >= 50 && $age<60)
       {
            $min=0;
            $max=53;
           $prefixtext="F50";    
       }
       if($age >= 60 && $age<70)
       {
            $min=0;
            $max=35;
           $prefixtext="F60";    
       }
       if($age >= 70 && $age<80)
       {
            $min=0;
            $max=31;
           $prefixtext="F70";    
       }
       if($age >= 80 )
       {
           $min=0;
            $max=30;
           $prefixtext="F80";    
       }
      }
       $ctr=0;
        while ($ctr<15) {
            $ctr++;
               $randomnum=rand($min,$max); 
              $filename =FCPATH."$imagefolderpath$prefixtext$randomnum.jpg";
              $images="$imagefolderpath$prefixtext$randomnum.jpg";
              if (file_exists($filename))
                {
              
                return ($images) ? $images : false;
               }
            else
             {
                if ($ctr == 10)
                return false;
             continue;
            }
        }
    }
		
		// Vertical jump..
		public function vertical_jump()
         {
                        
						 $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;			 
						 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						 if($_SESSION['vj']=="" && isset($_SESSION['is_virtual']))
                         {
                          $getVJ=$this->fitness_model->get_Vertical_jump();                         
						 }
						
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');						 
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;                                    
                         $data['id']       = $_SESSION['userid']; 
						 $data['filename']=$filename;	
                         $data['fieldData'] = $this->fitness_model->fetchDetail('verticaljump_test');
                         $this->load->view('vertical_jump',$data);   
         }
    
	public function flight_time()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); 
						 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];	
						 if($_SESSION['FTCT']=="" && isset($_SESSION['is_virtual']))
                         {
                          
                             $FTCT=$this->fitness_model->get_Flight_ratio(); 
                         }
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
						 $data['id']       = $_SESSION['userid']; 
						  $data['filename']=$filename;	
                         $data['fieldData'] = $this->fitness_model->fetchDetail('flightcontact_test');                         
                         $this->load->view('flight_time',$data);	   
         }
	

         public function peak_power()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges 
                         //print_r(array_reverse($prob_array)); die;
                          $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						  if($_SESSION['PPWKg']=="" && isset($_SESSION['is_virtual']))
                         {
                           $PPWKg=$this->fitness_model->peak_power_test(); 
                         }
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
                         $data['id']       = $_SESSION['userid']; 
						  $data['filename']=$filename;	
                         $data['fieldData'] = $this->fitness_model->fetchDetail('peakpower_test');
                         $this->load->view('peak_power',$data);	   
         }
	
        public function strength()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						 $data['id']       = $_SESSION['userid'];
						 
                         $data['fieldData'] = $this->fitness_model->fetchDetail('strength');
						 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						  $data['filename']=$filename;	
						  if($_SESSION['GSLEFT']=="" && isset($_SESSION['is_virtual']))
                         {
                           $GSLEFT=$this->fitness_model->strength_test(); 
                         }
                         $this->load->view('strength',$data);	   
         }
	
       public function total_work_done_30s()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
                         $data['id']       = $_SESSION['userid'];          
						 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						  $data['filename']=$filename;
						 if($_SESSION['TW']=="" && isset($_SESSION['is_virtual']))
                         {
                           $TW=$this->fitness_model->total_work(); 
                         } 
						 $data['fieldData'] = $this->fitness_model->fetchDetail('total_work_30s');
                         $this->load->view('30s_total_work_done',$data);	   
         }
         
      public function submaximal_test()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						 $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('submaximal');
						 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];	
						 if($_SESSION['VO2max']=="" && isset($_SESSION['is_virtual']))
                         {
                           $submaximal_test=$this->fitness_model->get_VO2max(); 
                         }                           
                         $data['filename']=$filename;
						 $this->load->view('3x3min_submaximal_test',$data);	   
         }
	
       public function shuttle_test()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;     
                         $data['id']       = $_SESSION['userid'];          
                          $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						 if($_SESSION['lap']=="" && isset($_SESSION['is_virtual']))
                         {
                           $submaximal_test=$this->fitness_model->twenty_shuttle_test(); 
                         } 
						 $data['fieldData'] = $this->fitness_model->fetchDetail('shuttle_test');
                         $data['filename']=$filename;
						 $this->load->view('shuttle_test',$data);	   
         }
		 
		 
		 // Dt: 19 Dec DAYA
		 public function bike_test()
         {
                         /* $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;   */
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         /* $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;     
                         $data['id']       = $_SESSION['userid'];          
                          $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						 if($_SESSION['lap']=="" && isset($_SESSION['is_virtual']))
                         {
                           $submaximal_test=$this->fitness_model->twenty_shuttle_test(); 
                         } 
						 $data['fieldData'] = $this->fitness_model->fetchDetail('shuttle_test');
                         $data['filename']=$filename; */ 
						 if($_SESSION['VO2max']=="" && isset($_SESSION['is_virtual']))
                         {
                           $submaximal_test=$this->fitness_model->get_VO2max(); 
                         }
						 
						 $this->load->view('bike_test',$data);	   
         } 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
		 
         
      
       public function v02max_test()
         {
                        $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;                                                   
                         $data['id']       = $_SESSION['userid'];          
						  $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];	
						 if($_SESSION['VO2max']=="" && isset($_SESSION['is_virtual']))
                         {
                           $submaximal_test=$this->fitness_model->get_VO2max(); 
                         }   	
						$data['fieldData'] = $this->fitness_model->fetchDetail('vo2max_test');                
                         $data['filename']=$filename;
						 $this->load->view('v02max_test',$data);	   
         }
         
           public function lactate_threshold()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						  $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']=="")
                         {   
                         $file=$this->file_exist($age,$gender);	
						 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];
						 $data['filename']=$filename;	
						 if($_SESSION['watts1']=="" && isset($_SESSION['is_virtual']))
                         {
                           $lactate_threshold=$this->fitness_model->Lactate_Threshold(); 
                         }  
                         $this->load->view('lactate_threshold',$data);	   
         }
		 
		 	 //MAOD TEST
         public function maod_test()
         {
           
             $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
                         $data['health_logo'] = $this->config->item('health_logo');
                         $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('submaximal');
			 $age=$_SESSION['vp_age'];
                         $gender=$_SESSION['user_gender'];
                         if($_SESSION['filename']==""  && isset($_SESSION['is_virtual']) )
                         {   
                         $file=$this->file_exist($age,$gender);	
			 $_SESSION['filename']=$file;
                         }
                         $filename=$_SESSION['filename'];	
	                 if($_SESSION['stage1_maod']=="" && isset($_SESSION['is_virtual']))
                         {
                           $maod_test=$this->fitness_model->calculate_maod(); 
                         }                           
                         $data['filename']=$filename;
		         $this->load->view('maod_test',$data);	   
         } 
       //
	   function saveClientVo2MaxInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveVo2MaxInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                            
                        if($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }
                        elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }
        
        function saveClientVerticalJumpInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveVerticalJumpInfo();
                    
                if($num_of_rows>0){
					
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                 
                                           
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        }                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }
                        elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }                 




                     }else{
                          echo "failure";
                     }
     
        }
		

        function saveClientFlightContactInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->saveFlightContactInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                        
					     $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                            
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }
		
		
        function saveClientPeakPowerInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->savePeakPowerInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                                                  
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }						
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }		
		
        function saveClientShuttleInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->saveShuttleInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                        
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
                            
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }                        
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }


		function saveClientStrengthInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveStrengthInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						 
                        $this->load->view('tests', $data); 
						
                     }else{
                          echo "failure";
                     }
		}



		function saveClientSubmaximalInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveSubmaximalInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						 
                        $this->load->view('tests', $data); 
						
                     }else{
                          echo "failure";
                     }
		}


        function saveClient30sTotalWorkInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->save30sTotalWorkInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                                                  
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');     
						 $data['health_logo'] = $this->config->item('health_logo');
						
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }						
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }				
         
}
