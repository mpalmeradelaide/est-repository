<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="<?php echo "$base/assets/js/"?>jquery.validate.min.js"></script>
    
<!--script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script--> 

<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script>
   $(function(){     
   $('#vpgenerate').validate({ 
    rules:{    
      no_of_vp  :  {required:true},
       "sel_itm2[]": "required"
     },    
     messages:{ 
      no_of_vp  : {required:"Please enter number."},
       "sel_itm2[]": "Please add some Variable ."
     }
     });     
    });
</script>
<script>
$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});    


</script>
</head>

<body>

 <div class="header">
    <div class="wrapper">
    	<div class="head_left">Virtual Person Generator Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container green_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit"><img src="<?php echo "$base/assets/images/"?>back_green.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Virtual Person Generator Toolkit</h3>
            <p></p>
        </div>
        
        
		
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_green.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_green.jpg"></a>
        </div>
		
        <div class="overlay">&nbsp;</div>
        <div class="info_block">
        	<div class="info_block_head">Generate Virtual Person</div>
            <p>The Virtual Person Generator is a standard Toolkit.In which user can genarated the Virtual person profile with the given number of count with its selected test , User can also select gender and sub-population for generating VP.</p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div> 
    
    </div>
</div>

<div class="wrapper">
<!-- Form begins -->    
 <?php 
        $attributes = array('id' => 'vpgenerate','name'=>'vpgenerate');
       echo form_open('Vpgenerate/VirtualpersonGeneration',$attributes); ?>
    <div class="contain">
    <!--Start right -->         
        <div class="right-section right-section_new section_121">
            <div class="right-head">VP GENERATOR</div>
            <?php if($upload_count>0){
			   echo "<div class='form_message success' id='msg'>You have successfully genearated $upload_count profiles. please click 'Export' to download.</div>";
             }
             ?>
            
            <div class="field_row verticle_field_row"> 			
                <div class="field_50" style="float: left;">
                    <label style="margin-top: 10px;">Generate Profiles with the following characteristics:</label>
                </div>
                <div class="field_24">
                    <input type="number" name="no_of_vp" id="no_of_vp" value="">
                </div>
            </div>

            <div class="field_row verticle_field_row"> 
                <div class="field_24">
                    <label for="value">Gender</label>
                    <select id="gender" name="gender">
                        <option value="">Select</option>                   
                        <option value="M">Male</option>                   
                        <option value="F">Female</option>                   
                    </select>
                </div>

                <div class="field_24">
                    <label for="percentile">Sub-Population</label>
                    <?php 
                $subpop_array=array(''=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array); 
                ?>  
                </div>

                <input type="button" class="lite_btn grey_btn f_right btn_green" id="Generate" value="Generate" style="margin-top:32px;"/>            
            </div>

            <div class="field_row verticle_field_row"> 
                <div class="field_24">
                    <label>Include These items:</label>
                    <select id="sel_itm" name="" multiple style="height: 90px;">
                         <?php foreach($test_var as $val)
                        echo  "<option value='$val->option_val'>$val->option_text</option>";   
                         ?>         
                     </select>
                </div>

                <div class="field_24" style="text-align: center;">
                    <input type="button" class="lite_btn grey_btn  btn_green" id="itm_all_add_btn" value="Add All >>" style="height: 30px;line-height: 30px !important;"/>            
                    <input type="button" class="lite_btn grey_btn  btn_green" id="itm_add_btn" value="Add >>" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>            
                    <input type="button" class="lite_btn grey_btn btn_green" id="itm_remove_btn" value=" << Remove" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>   
                    <input type="button" class="lite_btn grey_btn btn_green" id="itm_remove_all_btn" value=" << Remove All" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>   
                </div>

                <div class="field_24">
                    <select id="sel_itm2" name="sel_itm2[]" multiple style="height: 90px; margin-top: 31px;">
					 <?php 
                        if($selected_var!="")
                        {
                        $selected_array=explode(",",$selected_var);    
                         foreach($selected_array as $val)
                         {
                         echo "<option value='$val'>$val</option>";         
                         }
                        }
                      ?> 
                    </select>
                </div>
                <div class="field_24 f_right">
                    <input type="hidden" name="exportnum" value="<?php echo $upload_count;?>" id="exportnum">
					<input type="hidden" name="selected_var" value="<?php echo $selected_var;?>" id="selected_var">
                    <input type="button" class="lite_btn grey_btn f_right btn_green" value="Stop"/>            
                    <input type="button" class="lite_btn grey_btn f_right btn_green" value="Export" id="export" style="margin-top:32px;"/>            
                    <input type="button" class="lite_btn grey_btn f_right btn_green" value="Done" style="margin-top:32px;"/>      
                </div>      
            </div>

            <div class="field_row verticle_field_row" style="border: 0;"></div>



        </div> 
    </div>

<?php echo form_close(); ?>
<!-- Form ends -->

</div>
<script type="text/javascript">

	$(document).on('click','#exit', function(){
		document.forms["myform"].submit();
        //return false;
    }); 
	$(document).on('click','#export', function(){
    $("#msg").hide();
	$('#sel_itm2 option').remove();
	$("#vpgenerate").attr("action", "<?php echo base_url(); ?>/index.php/Vpgenerate/exportVP");     
     $("#vpgenerate")[0].submit();     
    //return false;
    }); 
        
$(document).on('click','#Generate', function(){
     $('#vpgenerate').validate();
    if ($("#vpgenerate").valid())
    {
    $("#vpgenerate").attr("action", "<?php echo base_url(); ?>/index.php/Vpgenerate/VirtualpersonGeneration");     
    $("#vpgenerate").submit();  
    }
    }); 

    $(document).on('click','#itm_add_btn', function(){
		return !$('#sel_itm option:selected').remove().appendTo('#sel_itm2');  		
    }); 
	$(document).on('click','#itm_all_add_btn', function(){
		return !$('#sel_itm option').remove().appendTo('#sel_itm2').prop('selected', true);  		
    }); 
	$('#itm_remove_btn').click(function() {  
		return !$('#sel_itm2 option:selected').remove().appendTo('#sel_itm');  
	 }); 
	$('#itm_remove_all_btn').click(function() {  
		return !$('#sel_itm2 option').remove().appendTo('#sel_itm').prop('selected', false);  
	 }); 
	
</script> 
</body>
</html>
