<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Physical Activity</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<script>
var CUSTOM_ALERT = function(){//Namespace pattern
 var alertBox = null, titleEl = null, messageEl = null;
 return {
  hide : function(){
   alertBox.style.visibility = 'hidden';
  },
  alert : function(aTitle, aMessage){
   if(!alertBox) alertBox = document.getElementById("CustomAlert");
   //if(!titleEl) titleEl = document.getElementById("CustomAlertTitle");
   if(!messageEl) messageEl = document.getElementById("CustomAlertMessage");
   //titleEl.innerHTML = aTitle;
   messageEl.innerHTML = aMessage;
   thisText = aMessage.length;
   //if (aTitle.length > aMessage.length){ thisText = aTitle.length;}
   aWidth = Math.min(Math.max(thisText*5+80, 150), 350);
   aHeight = (thisText>610) ? 290 : (thisText>550) ? 270 : (thisText > 490) ? 250 : (thisText > 420) ? 230 : (thisText > 360) ? 210 : (thisText > 300) ? 190 : (thisText > 240) ? 170 : (thisText > 180) ? 150 : (thisText > 120) ? 130 : (thisText > 60)? 120 : 100;
   alertBox.style.width = aWidth + 'px';
   alertBox.style.height = aHeight + 'px';
   var left = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
   var top = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
   var hScroll = window.pageXOffset || document.body.scrollLeft || document.documentElement.scrollLeft || 0;
   var vScroll = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop || 0;
   alertBox.style.left = hScroll + (left - aWidth)/2 + 'px';
   alertBox.style.top =  vScroll + (top - aHeight)/2 + 'px';
   alertBox.style.visibility = 'visible';
  }
 };
}();

CUSTOM_ALERT.alert('Your Alert Message Goes Here<br>hello');
</script>
<!---<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->
<script type="text/javascript">

    
var specialKeys = new Array();
        specialKeys.push(8,37, 38, 39, 40); //Backspace
        function IsNumeric(e) {
            
            var keyCode = e.which ? e.which : e.keyCode
           
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 );
        if(!ret)  { 
        CUSTOM_ALERT.alert('','Number Only');
        }
            return ret;
        }

  $(function() {
	
	$(window).bind('scroll', function() {
		  
	    var navHeight = $( window ).height() - 400;
			 if ($(window).scrollTop() > 100) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});  
                
          $('.one-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
         }            
         return this; //for chaining
      });        
	
}); 




</script>
</head>
<script type="text/javascript">
$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
  <?php // print_r($fieldData);
//var_dump($this->session->all_userdata());
?>
<body>
<?php echo  form_open('welcome/saveClientPhysicalActivityInfo'); ?>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitPhysicalActivity" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg" title="Back to Home Screen"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Physical Activity</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Physical Activity</div>
            <p>These questions are the same ones used in the national surveys of adult physical activity. We therefore have a lot of information on what people typically do in their leisure 
               time or in active transport. The PHYSICAL ACTIVITY questions are split into three types of activity (1) Walking, (2) Vigorous activity which involves more strenuous exercise,
               and (3) any other Moderate forms of exercise such as golf, swimming, leisurely cycling or social tennis etc. The questions on sedentary activity are important because of the 
               health risks of long periods of sitting or lying about. Please note the SEDENTARY question about sitting watching TV, videos, DVDs or playing computer games or surfing the net 
               does NOT include sitting down at WORK. The sedentary patterns of your job are captured in the final question on this page. The default value has been set to 'Yes'.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>  
    
    </div>
</div>
<!--Start Wrapper -->
<div class="wrapper">
  <div id="CustomAlert">
    <div id="CustomAlertMessage"></div>
    <input type="button" value="OK" onClick="CUSTOM_ALERT.hide();" id="CustomAlertSampleokButton">
  </div>
  
  <!--Start login -->
  <div class="login-cont" style="display:none;"> 
    <!--<form action="<?php //echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> --> 
    
    <div class="section"> <span><b>First name</b>
      <input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required>
      </span> <span><b>Last name</b>
      <input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required>
      <input name="submitPhysicalActivity" type="submit" value="" title="edit client details"/>
      </span> </div>
    <!--	</form> --> 
  </div>
  <!--End login --> 
  
    
    <!--Start contain -->
    <div class="contain">
      <?php //echo  form_open('welcome/saveClientPhysicalActivityInfo'); ?>
       <!--Start left --> 
       <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn active" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			 <div class="btn">
            <button type="submit" name="mysubmit7" class="left_panel_btn" id="myform7"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Absolute CVD risk</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			
			<div class="btn">
            <button type="submit" name="mysubmit8" class="left_panel_btn active" id="myform8"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Life Expectancy</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
     
       </div>
       <!--End Left --> 
      
      <?php //print_r($fieldData); ?>
      <!--Start right -->
      <div class="right">
        <div class="right-head">Physical Activity</div>
        <div class="right-section">
          <?php // $hidden = array('userid' => $id);
            $option_1 = $fieldData[0]->option_1==''?'':$fieldData[0]->option_1;
            
                  
                     $attrib = array(
                        'name'        => 'options_1',
                        'id'          => 'options_1',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_1,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'type'=>'number',
                         'min'=>'0'
                         
                      );
      ?>
          <div class="field_row">
              <div class="field_75">In the last week how many times have you walked continuously for at least 10 minutes, for recreation, exercise or to get to and from places?</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          <?php 
       $option_2 = $fieldData[0]->option_2==''?'':$fieldData[0]->option_2;
                     $attrib = array(
                        'name'        => 'options_2',
                        'id'          => 'options_2',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_2,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>		<div class="clearfix">&nbsp;</div>
              <div class="field_75">Estimate the total time that you  spent walking in this way in the last week? (minutes)</div>
          	  <div class="field_25"><?php echo form_input($attrib); ?></div>          
          </div>
          <?php 
        $option_3 = $fieldData[0]->option_3==''?'':$fieldData[0]->option_3;
                     $attrib = array(
                        'name'        => 'options_3',
                        'id'          => 'options_3',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_3,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
		         'type'=>'number',
                         'min'=>'0'
                      );
       ?>
          <div class="field_row">
              <div class="field_75">In the last week, how many times did you do any vigorous physical activity which made you breathe harder or puff and pant? (excluding household chores and gardening)</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          <?php 
        $option_4 = $fieldData[0]->option_4==''?'':$fieldData[0]->option_4;
                     $attrib = array(
                        'name'        => 'options_4',
                        'id'          => 'options_4',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_4,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>		
       			<div class="clearfix">&nbsp;</div>
              <div class="field_75">Estimate the total time that you  spent doing this vigorous physical activity in the last week? (minutes)</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          </div>
          <?php 
       $option_5 = $fieldData[0]->option_5==''?'':$fieldData[0]->option_5;
                     $attrib = array(
                        'name'        => 'options_5',
                        'id'          => 'options_5',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_5,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>
          <div class="field_row">
              <div class="field_75">In the last week, how many times did you do any other more moderate physical activities that you have not already mentioned?</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          <?php 
        $option_6 = $fieldData[0]->option_6==''?'':$fieldData[0]->option_6;
                     $attrib = array(
                        'name'        => 'options_6',
                        'id'          => 'options_6',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_6,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>     <div class="clearfix">&nbsp;</div>
              <div class="field_75">Estimate the total time that you spent doing these moderate activities in the last week? (minutes)</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          </div>
          
          
          <?php 
       $option_7 = $fieldData[0]->option_7==''?'':$fieldData[0]->option_7;
                     $attrib = array(
                        'name'        => 'options_7',
                        'id'          => 'options_7',
                        'class'       => 'physi-text  one-decimal ',
                         'size'=>'20',
                         'value'=>$option_7,
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0',
                          'step'=>'any'
                      );
       ?>  <div class="clearfix">&nbsp;</div>
       		<p style="font-size:18px; margin-bottom:15px;">Sedentary Activity</p>
          <div class="field_row">
              <div class="field_75">On average how many hours per day do you spend sitting watching TV, videos, DVDs or playing video or computer games  or surfing the internet for pleasure? (hours)</div>
              <div class="field_25"><?php echo form_input($attrib); ?></div>
          <?php 
          $option_8 = $fieldData[0]->option_8==''?'':$fieldData[0]->option_8;
                     $attrib = array(
                        'name'        => 'options_8',
                        'id'          => 'options_8',
                        'class'       => 'physi-text  one-decimal ',
                         'size'=>'20',
                         'value'=>$option_8,
                        // 'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
						 'type'=>'number',
                         'min'=>'0',
                         'step'=>'any'
                      );
       ?> <div class="clearfix">&nbsp;</div>
          <div class="field_75">On average how many hours per day do you spend driving or being driven? (hours)</div>
          <div class="field_25"><?php echo form_input($attrib); ?></div> 
          
          <div class="clearfix" style="margin-bottom:20px;">&nbsp;</div>
          <div class="field_75">Does your job require sitting for long periods?</div>
          <div class="field_25" style="text-align:left;">
          <?php $radio_is_checked = ($fieldData[0]->option_9 === 'Y')?"checked":"N";
           echo form_radio(array("name"=>"options_9","id"=>"options_9Y","value"=>"Y",'class'=>'gender','checked'=>$radio_is_checked)); ?> <label for="options_9Y">Yes<br><span></span></label>
          <?php $radio_is_checked = ($fieldData[0]->option_9 === 'N')?"checked":"";
           echo form_radio(array("name"=>"options_9","id"=>"options_9N","value"=>"N",'class'=>'gender','checked'=>$radio_is_checked)); ?> <label for="options_9N">No<br><span></span></label>
          </div>
         </div>           
         
          <?php echo form_submit('mysubmit1','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform1'");?>
      	  <?php echo form_submit('mysubmit3','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform3'");?>
          
          <?php echo form_close(); ?> 
         </div>
        <!--End right section--> 
        
      </div>
      <!--End right --> 
      
    </div>
    <!--End contain --> 
    
  
</div>
<!--End Wrapper -->
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
</body>
</html>
