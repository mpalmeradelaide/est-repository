<!doctype html>
<?php 
if(count($randomdata)>0)
{
$firstName =$randomdata['firstname'];
                        $lastName =$randomdata['lastname'];
                        if($randomdata['gender']=='Male')
                        {
                        $gender='M';    
                        }
                        else
                        {
                         $gender='F';   
                        }
                       $daydropdown=$randomdata['daydropdown'];
                       $monthdropdown=$randomdata['monthdropdown'];
                       $yeardropdown=$randomdata['yeardropdown'];
                       $occupation =$randomdata['occupation'];
                       $country =$randomdata['country']['countrykey'];
                       $subpopulation=$randomdata['sub_population'];
                       $age_category=$randomdata['age_category'];
                             
}
else
{
					   $firstName =$_SESSION['user_first_name'];
                       $lastName =$_SESSION['user_last_name'];
                       $gender=$_SESSION['user_gender'];    
                       $daydropdown=$_SESSION['user_daydropdown'];
                       $monthdropdown=$_SESSION['user_monthdropdown'];
                       $yeardropdown=$_SESSION['user_yeardropdown'];
                       $occupation =$_SESSION['user_occupation'];
                       $country =$_SESSION['user_country'];
                       $subpopulation=$_SESSION['subpopulation'];
                       $age_category=$_SESSION['age_category'];   
}
?>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>Client Details</title>
<link href='<?php echo "$base/$css" ;?>' rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="http://fitnessriskmanagement.com.au/screening-tool/assets/css/jquery.datetimepicker.css"/>
<script type="text/javascript" src="<?php echo base_url() . 'assets/'; ?>js/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
  <script>
  $( function() {
    $( ".virtual_container" ).draggable();
  } );
  </script>
<script>
  $(function(){ 
     gender = <?php $gender;  ?> 
       genderTest = gender.value            
      if( genderTest == 'M' ){   
       $("#male").prop('checked', true);
      }      
    });
	
	$(document).on("click",".btn_generate", function(){
            
         window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration');?>";
  
		$(".virtual_box").css("visibility","visible");
	});
</script>




    
<script type="text/javascript">
   $(document).on("click",".genderclass", function(){
       var gender=$(this).attr('value');
       //alert(gender);
       var emailid=$('#emailid').attr('value');
       var montharr=["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
       var day = $('#daydropdown').find(":selected").text();
       //alert(day);
       //var month = $('#monthdropdown').find(":selected").text();
       //month=inArray(month,montharr);
       var month=$("#monthdropdown option:selected").val();
       var mnthname=montharr[month-1];
       //alert(mnthname);
       var year= $('#yeardropdown').find(":selected").text();
       var lastname=$('#lastname').val();
       var firstname=$('#firstname').val();
       //alert(lastname);
       //alert(firstname);
       var subpopulation="";
       var dateofBirth = year+'-'+month+'-'+day;
       //alert(dateofBirth);
       var vp_age=getAge(dateofBirth);
       //alert(vp_age);  
       
       
       //alert(agerange);
       //alert(country);
       $('#agerangeid').val(agerange);
       //window.location.href = "<?php //echo site_url('welcome/VirtualpersonGeneration?gen=');?>"+gender;    
        window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration?gen=');?>"+gender+"&age="+vp_age+"&firstname="+firstname+"&lastname="+lastname+"&email="+emailid+"&day="+day+"&month="+mnthname+"&year="+year+"&agerange="+agerange;    
});
    

    
    
$(document).on("change",".dateclass",function(){ 
   var selval=$("#monthdropdown option:selected").val();
   var month=$("#monthdropdown option:selected").text();
   var day = $('#daydropdown').find(":selected").text();  
   var year= $('#yeardropdown').find(":selected").text();  
   var dateofBirth = year+'-'+selval+'-'+day;
   var vp_age=getAge(dateofBirth);
   
   var agerange=getAgeRange(vp_age);
   $('#agerangeid').val(agerange);
});
    


  //GET AGE RANGE
       function getAgeRange(age)
       {
       	     var agerange="";
            //alert("age"+age) ;
             if(age >=1 && age <=17)
             {
                 agerange='18-29';
             }
             else if(age >= 18 && age <=29)
			 {
				 agerange = '18-29' ;
			 }
          	 else if(age >= 30 && age <=39)
			 {
				 agerange = '30-39' ;
			 }
			 else if(age >= 40 && age <=49)
			 {
				 agerange = '40-49' ;
			 }
			 else if(age >= 50 && age <=59)
			 {
				 agerange = '50-59' ;
			 }
			 else if(age >= 60 && age <=69)
			 {
				 agerange = '60-69' ;
			 }
			 else if(age >=70 && age <=79)
			 {
				 agerange = '70-79' ;
			 }
             else if(age >=80 && age <=84)
			 {
				 agerange = '80-84' ;
			 }
             else if(age >84)
			 {
				 agerange = '84+' ;
			 }
                               
            return agerange;
        }  
    
    
 function getAge(dateString) {
                var today = new Date();
                var birthDate = new Date(dateString);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
             }
    
function inArray(needle,haystack)
{
    var count=haystack.length;
    for(var i=1;i<=count;i++)
    {
        if(haystack[i]===needle){return i+1;}
    }
    return false;
}       
   
    
/*function reverse_birthday( $years ){
return date('Y-m-d', strtotime($years . ' years ago'));
}  */  
    
</script>  





<style type="text/css">
    .vie_table {
    position: absolute;
    width: 250px;
    left: 50px;
    top: 100px;
    text-align: center;
}
 .vie_table td{padding: 20px;}
</style>
</head>

<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right">Firstname surname <img src="<?php echo "$base/assets/images/"?>user_icon.png"></div>
    </div>
</div>
    <div class="vie_table">
        <table border="1" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>Random Probability for Log BMI</td>
                <td><?php echo $randomdata['probability_LOG_BMI'];?></td>
            </tr>
            <tr>
                <td>Random_generated_log_BMI</td>
                <td><?php echo $randomdata['Random_generated_log_BMI'];?></td>
            </tr>
             <tr>
                <td>MASS</td>
                <td><?php echo $randomdata['MASS'];?></td>
            </tr>
             <tr>
                <td>Random Probability of BMI</td>
                <td><?php echo $randomdata['probability_random_BMI'];?></td>
            </tr>
            
            <tr>
                <td>Random generated BMI</td>
                <td><?php echo $randomdata['BMI'];?></td>
            </tr>
            <tr>
                <td>HEIGHT</td>
                <td><?php echo $randomdata['HEIGHT'];?></td>
            </tr>
            <tr>
                <td>m_BMI</td>
                <td><?php echo $randomdata['m_BMI'];?></td>
            </tr>
            <tr>
                <td>b_BMI</td>
                <td><?php echo $randomdata['b_BMI'];?></td>
            </tr>
            <tr>
                <td>s_BMI</td>
                <td><?php echo $randomdata['s_BMI'];?></td>
            </tr>
            <tr>
                <td>mean_log_Mass</td>
                <td><?php echo $randomdata['mean_log_Mass'];?></td>
            </tr>
            <tr>
                <td>sd_log_Mass</td>
                <td><?php echo $randomdata['sd_log_Mass'];?></td>
            </tr>
        </table>
    </div>
<!--Start Wrapper --> 
<div class="wrapper">
	<div class="welc_form">
    	 <?php 
  
    	 $attributes = array('id' => 'clientInfo');
       echo form_open('welcome/saveClientInfo',$attributes); ?>
       	
        <div class="welc_form_row">
            <h4>Choose your module</h4>            
           
            <?php echo form_submit('mysubmit1','Pre-exercise Screening',"class='lite_btn program_btn active'"); ?> 
            <?php echo form_submit('mysubmit2','Fitness Testing',"class='lite_btn program_btn active'"); ?>
            <?php echo form_submit('mysubmit3','Body Composition',"class='lite_btn program_btn active'"); ?>
			<a href="http://115.112.118.252/VirtualEST/index.php/Vpgenerate/" class="lite_btn program_btn active" target="_blank">VP Generator</a>
            
           <!-- <button class="lite_btn program_btn active" id="pre_exercise">Pre-exercise Screening</button>
            <button class="lite_btn program_btn" id="fitness">Fitness Testing</button>
            <button class="lite_btn program_btn" id="body_composition">Body Composition</button> -->
        </div>
        
    
  	    <div class="welc_form_row">
             <h4>Enter your details</h4>
            <p>Fields marked with an asterisk * are mandatory.</p>
            
              <div id="validate">
                <?php echo validation_errors(); ?>
              </div>
                  
                  <?php $firstName =$firstName !='' ?$firstName:'';
                        $lastName =$lastName===''?'':$lastName;
                        $email =$email===''?'':$email;
                        $p_desc =$p_desc===''?'':$p_desc;
                        $gender =$gender==='M'?'M':$gender;
                        $genderM =$gender==='M'?'Checked':'';
                        $genderF =$gender==='F'?'Checked':'';
               
                        $dobirth =$dob===''?'':$dob;
                        $occupation =$occupation===''?'selectoccupation':$occupation;
                        $country =$country===''?'Select':$country;
                        $p_name =$p_name===''?'Select':$p_name;
                        $daydropdown =$daydropdown===''?'Select':$daydropdown;
                        $monthdropdown =$monthdropdown===''?'Select':$monthdropdown;
                        $yeardropdown =$yeardropdown===''?'Select':$yeardropdown; 
                        $subpopulation =$subpopulation===''?'Select':$subpopulation;
                        $age_category=$age_category===''?'Select':$age_category;
                  ?>
                          
              <div class="field_row">
                  <div class="field_50">
                    <label>First name *</label><?php echo form_input('first_name', $firstName,"class='cus-input'"); ?>
                  </div>
                  
                  <div class="field_50">
                    <label>Last name *</label><?php echo form_input('last_name', $lastName,"class='cus-input'"); ?>
                  </div>
              </div>
              
              <div class="field_row">
                  <div class="field_50">
                    <label>Email</label><?php echo form_input('email', $email); ?>
                  </div>
                  <div class="field_50 gen">
                    <label>Gender *</label>
                    <?php echo form_radio(array("name"=>"gender","id"=>"male","class"=>"genderclass","value"=>"M","checked"=> $genderM )); ?> <label for="male"><span style="margin-right:8px;"></span>Male</label>
                    <?php echo form_radio(array("name"=>"gender","id"=>"female","class"=>"genderclass","value"=>"F","checked"=> $genderF )); ?>  <label for="female"><span style="margin-right:8px;"></span>Female</label>
                  </div>      
              </div>
              
              
              <?php
        
                $dob = array(
                    'name'=>'datetimepicker2',
                    'id'=> 'datetimepicker2',
                    'placeholder'=>"mm/dd/yyyy",
                    'value'=>$dobirth, 
                    'type'=>'numb',
                    'class'=>'webDate'                    
                            );         
               ?>
               <div class="field_row">
                  <div class="field_50 dob">
                    <label>Date of birth *</label>                    
                    <span><?php
                            $options = array(
                                        'dd' => 'DD',
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
                            $js = 'id="daydropdown" ';
                            echo form_dropdown('daydropdown', $options, $daydropdown,'class="cus-input dateclass" id="daydropdown"', $js);
                  
                    ?></span>
                    <span><?php
                                    $options = array(
                                    'mm' => 'MM',
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
                                    
                                    
                                $js = 'id="monthdropdown"';
                                  echo form_dropdown('monthdropdown', $options, $monthdropdown,'class="cus-input dateclass" id="monthdropdown"', $js);
                          
                            ?></span>            
                    <span><?php
                                //    $options = array('0' => 'YYYY','1910'=>'1910','1910'=>"1910", '1911'=>"1911", '1912'=>"1912", '1913'=>"1913", '1914'=>"1914", '1915'=>"1915", '1916'=>"1916", '1917'=>"1917", '1918'=>"1918", '1919'=>"1919", '1920'=>"1920", '1921'=>"1921", '1922'=>"1922", '1923'=>"1923", '1924'=>"1924", '1925'=>"1925", '1926'=>"1926", '1927'=>"1927", '1928'=>"1928", '1929'=>"1929", '1930'=>"1930", '1931'=>"1931", '1932'=>"1932", '1933'=>"1933", '1934'=>"1934", '1935'=>"1935", '1936'=>"1936", '1937'=>"1937", '1938'=>"1938", '1939'=>"1939", '1940'=>"1940", '1941'=>"1941", '1942'=>"1942", '1943'=>"1943", '1944'=>"1944", '1945'=>"1945", '1946'=>"1946", '1947'=>"1947", '1948'=>"1948", '1949'=>"1949", '1950'=>"1950", '1951'=>"1951", '1952'=>"1952", '1953'=>"1953", '1954'=>"1954", '1955'=>"1955", '1956'=>"1956", '1957'=>"1957", '1958'=>"1958", '1959'=>"1959", '1960'=>"1960", '1961'=>"1961", '1962'=>"1962", '1963'=>"1963", '1964'=>"1964", '1965'=>"1965", '1966'=>"1966", '1967'=>"1967", '1968'=>"1968", '1969'=>"1969", '1970'=>"1970", '1971'=>"1971", '1972'=>"1972", '1973'=>"1973", '1974'=>"1974", '1975'=>"1975", '1976'=>"1976", '1977'=>"1977", '1978'=>"1978", '1979'=>"1979", '1980'=>"1980", '1981'=>"1981", '1982'=>"1982", '1983'=>"1983", '1984'=>"1984", '1985'=>"1985", '1986'=>"1986", '1987'=>"1987", '1988'=>"1988", '1989'=>"1989", '1990'=>"1990", '1991'=>"1991", '1992'=>"1992", '1993'=>"1993", '1994'=>"1994", '1995'=>"1995", '1996'=>"1996", '1997'=>"1997", '1998'=>"1998", '1999'=>"1999", '2000'=>"2000", '2001'=>"2001", '2002'=>"2002", '2003'=>"2003", '2004'=>"2004", '2005'=>"2005", '2006'=>"2006", '2007'=>"2007", '2008'=>"2008", '2009'=>'2009', '2010'=>'2010', '2011'=>'2011', '2012'=>'2012', '2013'=>'2013', '2014'=>'2014');
                                    // rsort($options  , SORT_DESC);
                            $options = array('0000' => 'YYYY','2010'=>'2010', '2009'=>'2009', '2008'=>'2008', '2007'=>'2007', '2006'=>'2006', '2005'=>'2005', '2004'=>'2004', '2003'=>'2003', '2002'=>'2002', '2001'=>'2001', '2000'=>'2000', '1999'=>'1999', '1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927', '1926'=>'1926', '1925'=>'1925', '1924'=>'1924', '1923'=>'1923', '1922'=>'1922', '1921'=>'1921', '1920'=>'1920', '1919'=>'1919', '1918'=>'1918', '1917'=>'1917', '1916'=>'1916', '1915'=>'1915', '1914'=>'1914', '1913'=>'1913', '1912'=>'1912', '1911'=>'1911', '1910'=>'1910', '1910'=>'1910');		
                                $js = 'id="yeardropdown" ';
                                  echo form_dropdown('yeardropdown', $options, $yeardropdown,'class="cus-input dateclass" id="yeardropdown"', $js);
                          
                            ?></span>
                  </div>
                  
                  <div class="field_50">	
                  <label>Age Categories</label>
					
                  <?php 
                  $age_cate = array(
                                    'selectage' => 'Select',
                                    '18-29' => '18-29',
                                    '30-39' => '30-39',
                                    '40-49' => '40-49',
                                    '50-59' => '50-59',
                                    '60-69' => '60-69',
                                    '70-79' => '70-79',
                                    '80-84' => '80-84',
                                    '84+' => '84+');
                  
                  echo form_dropdown('age_cate',$age_cate, $age_category ,'class="ageclass" id="agerangeid"');
                  ?>
                  </div>
                  
                </div>	
                
              <div class="field_row">  
                 <div class="field_50">	
                  <label>Occupation</label>
                  <?php
                             $options = array(
                                                'selectoccupation'  => 'Select',
                                                'Accounting'  => 'Accounting',
                                                'Administrative/offic'  => 'Administrative/office',
                                                'Advertising'   => 'Advertising',
                                                'Animal care'   => 'Animal care',
                                                'Architect'   => 'Architect',
                                                'Art-creative'   => 'Art-creative',
                                                'Athlete/coach'   => 'Athlete/coach',
                                                'Building/surveying'   => 'Building/surveying',
                                                'Customer service'   => 'Customer service',
                                                'Design'   => 'Design',
                                                'Emergency services'   => 'Emergency services',
                                                'Education'   => 'Education',
                                                'Engineering'   => 'Engineering',
                                                'Entertainer'   => 'Entertainer',
                                                'Farmer/primary production'   => 'Farmer/primary production',
                                                'Finance'   => 'Finance',
                                                'Garden care'   => 'Garden care',
                                                'General business'   => 'General business',
                                                'Health care'   => 'Health care',
                                                'Human resources'   => 'Human resources',
                                                'Information technology'   => 'Information technology',
                                                'Legal'   => 'Legal',			
                                                                            'Management' => 'Management',
                                                'Manufacturing' => 'Manufacturing',
                                                'Manufacturing' => 'Manufacturing',
                                                'Medical/dental' => 'Medical/dental',
                                                'Nurse'   => 'Nurse',
                                                'Pilot'   => 'Pilot',
                                                'Production'   => 'Production',
                                                'Public service'   => 'Public service',
                                                'Research'   => 'Research',
												 'retired'   => 'retired'
                             );  
        
                      echo form_dropdown('occupation', $options, $occupation);?>
                  </div>
                   <div class="field_50">	
                  <label>Sub-Population</label>
		<?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$subpopulation); 
                ?>  
                  </div>
                  
                            
              </div> 
              
               <div class="field_row">  
                   <div class="field_50">	
                  <label>Country</label>
                  <?php
                           $options = array(
                            'selectcountry'  => 'Select',
                            'AF'=>'Afghanistan',
                            'AX'=>'Aland Islands',
                            'AL'=>'Albania',
                            'DZ'=>'Algeria',
                            'AS'=>'American Samoa',
                            'AD'=>'Andorra',
                            'AO'=>'Angola',
                            'AI'=>'Anguilla',
                            'AQ'=>'Antarctica',
                            'AG'=>'Antigua and Barbuda',
                            'AR'=>'Argentina',
                            'AM'=>'Armenia',
                            'AW'=>'Aruba',
                            'AU'=>'Australia',
                            'AT'=>'Austria',
                            'AZ'=>'Azerbaijan',
                            'BS'=>'Bahamas',
                            'BH'=>'Bahrain',
                            'BD'=>'Bangladesh',
                            'BB'=>'Barbados',
                            'BY'=>'Belarus',
                            'BE'=>'Belgium',
                            'BZ'=>'Belize',
                            'BJ'=>'Benin',
                            'BM'=>'Bermuda',
                            'BT'=>'Bhutan',
                            'BO'=>'Bolivia',
                            'BQ'=>'Bonaire, Sint Eustatius and Saba',
                            'BA'=>'Bosnia and Herzegovina',
                            'BW'=>'Botswana',
                            'BR'=>'Brazil',
                            'IO'=>'British Indian Ocean Territory',
                            'BN'=>'Brunei Darussalam',
                            'BG'=>'Bulgaria',
                            'BF'=>'Burkina Faso',
                            'BI'=>'Burundi',
                            'KH'=>'Cambodia',
                            'CM'=>'Cameroon',
                            'CA'=>'Canada',
                            'CV'=>'Cape Verde',
                            'KY'=>'Cayman Islands',
                            'CF'=>'Central African Republic',
                            'TD'=>'Chad',
                            'CL'=>'Chile',
                            'CN'=>'China',
                            'CX'=>'Christmas Island',
                            'CC'=>'Cocos (Keeling) Islands',
                            'CO'=>'Colombia',
                            'KM'=>'Comoros',
                            'CG'=>'Congo',
                            'CD'=>'Congo, The Democratic Republic of the',
                            'CK'=>'Cook Islands',
                            'CR'=>'Costa Rica',
                            'CI'=>'Cote d Ivoire',
                            'HR'=>'Croatia',
                            'CU'=>'Cuba',
                            'CW'=>'Curaçao',
                            'CY'=>'Cyprus',
                            'CZ'=>'Czech Republic',
                            'DK'=>'Denmark',
                            'DJ'=>'Djibouti',
                            'DM'=>'Dominica',
                            'DO'=>'Dominican Republic',
                            'EC'=>'Ecuador',
                            'EG'=>'Egypt',
                            'SV'=>'El Salvador',
                            'GQ'=>'Equatorial Guinea',
                            'ER'=>'Eritrea',
                            'EE'=>'Estonia',
                            'ET'=>'Ethiopia',
                            'FK'=>'Falkland Islands(Malvinas)',
                            'FO'=>'Faroe Islands',
                            'FJ'=>'Fiji',
                            'FI'=>'Finland',
                            'FR'=>'France',
                            'GF'=>'French Guiana',
                            'PF'=>'French Polynesia',
                            'TF'=>'French Southern Territories',
                            'GA'=>'Gabon',
                            'GM'=>'Gambia',
                            'GE'=>'Georgia',
                            'DE'=>'Germany',
                            'GH'=>'Ghana',
                            'GI'=>'Gibraltar',
                            'GR'=>'Greece',
                            'GL'=>'Greenland',
                            'GD'=>'Grenada',
                            'GP'=>'Guadeloupe',
                            'GU'=>'Guam',
                            'GT'=>'Guatemala',
                            'GG'=>'Guernsey',
                            'GN'=>'Guinea',
                            'GW'=>'Guinea-Bissau',
                            'GY'=>'Guyana',
                            'HT'=>'Haiti',
                            'HM'=>'Heard Island and McDonald Islands',
                            'VA'=>'Holy See (Vatican City State)',
                            'HN'=>'Honduras',
                            'HK'=>'Hong Kong',
                            'HU'=>'Hungary',
                            'IS'=>'Iceland',
                            'IN'=>'India',
                            'ID'=>'Indonesia',
                            'XZ'=>'Installations in International Waters',
                            'IR'=>'Iran, Islamic Republic of',
                            'IQ'=>'Iraq',
                            'IE'=>'Ireland',
                            'IM'=>'Isle of Man',
                            'IL'=>'Israel',
                            'IT'=>'Italy',
                            'JM'=>'Jamaica',
                            'JP'=>'Japan',
                            'JE'=>'Jersey',
                            'JO'=>'Jordan',
                            'KZ'=>'Kazakhstan',
                            'KE'=>'Kenya',
                            'KI'=>'Kiribati',
                            'KP'=>'Korea, Democratic People s Republic of',
                            'KR'=>'Korea, Republic of',
                            'KW'=>'Kuwait',
                            'KG'=>'Kyrgyzstan',
                            'LA'=>'Lao People s Democratic Republic',
                            'LV'=>'Latvia',
                            'LB'=>'Lebanon',
                            'LS'=>'Lesotho',
                            'LR'=>'Liberia',
                            'LY'=>'Libya',
                            'LI'=>'Liechtenstein',
                            'LT'=>'Lithuania',
                            'LU'=>'Luxembourg',
                            'MO'=>'Macao',
                            'MK'=>'Macedonia, The former Yugoslav Republic of',
                            'MG'=>'Madagascar',
                            'MW'=>'Malawi',
                            'MY'=>'Malaysia',
                            'MV'=>'Maldives',
                            'ML'=>'Mali',
                            'MT'=>'Malta',
                            'MH'=>'Marshall Islands',
                            'MQ'=>'Martinique',
                            'MR'=>'Mauritania',
                            'MU'=>'Mauritius',
                            'YT'=>'Mayotte',
                            'MX'=>'Mexico',
                            'FM'=>'Micronesia, Federated States of',
                            'MD'=>'Moldova, Republic of',
                            'MC'=>'Monaco',
                            'MN'=>'Mongolia',
                            'ME'=>'Montenegro',
                            'MS'=>'Montserrat',
                            'MA'=>'Morocco',
                            'MZ'=>'Mozambique',
                            'MM'=>'Myanmar',
                            'NA'=>'Namibia',
                            'NR'=>'Nauru',
                            'NP'=>'Nepal',
                            'NL'=>'Netherlands',
                            'NC'=>'New Caledonia',
                            'NZ'=>'New Zealand',
                            'NI'=>'Nicaragua',
                            'NE'=>'Niger',
                            'NG'=>'Nigeria',
                            'NU'=>'Niue',
                            'NF'=>'Norfolk Island',
                            'MP'=>'Northern Mariana Islands',
                            'NO'=>'Norway',
                            'OM'=>'Oman',
                            'PK'=>'Pakistan',
                            'PW'=>'Palau',
                            'PS'=>'Palestine, State of',
                            'PA'=>'Panama',
                            'PG'=>'Papua New Guinea',
                            'PY'=>'Paraguay',
                            'PE'=>'Peru',
                            'PH'=>'Philippines',
                            'PN'=>'Pitcairn',
                            'PL'=>'Poland',
                            'PT'=>'Portugal',
                            'PR'=>'Puerto Rico',
                            'QA'=>'Qatar',
                            'RE'=>'Reunion',
                            'RO'=>'Romania',
                            'RU'=>'Russian Federation',
                            'RW'=>'Rwanda',
                            'BL'=>'Saint Barthelemy',
                            'SH'=>'Saint Helena, Ascension and Tristan Da Cunha',
                            'KN'=>'Saint Kitts and Nevis',
                            'LC'=>'Saint Lucia',
                            'MF'=>'Saint Martin (French Part)',
                            'PM'=>'Saint Pierre and Miquelon',
                            'VC'=>'Saint Vincent and the Grenadines',
                            'WS'=>'Samoa',
                            'SM'=>'San Marino',
                            'ST'=>'Sao Tome and Principe',
                            'SA'=>'Saudi Arabia',
                            'SN'=>'Senegal',
                            'RS'=>'Serbia',
                            'SC'=>'Seychelles',
                            'SL'=>'Sierra Leone',
                            'SG'=>'Singapore',
                            'SX'=>'Sint Maarten (Dutch Part)',
                            'SK'=>'Slovakia',
                            'SI'=>'Slovenia',
                            'SB'=>'Solomon Islands',
                            'SO'=>'Somalia',
                            'ZA'=>'South Africa',
                            'GS'=>'South Georgia and the South Sandwich Islands',
                            'SS'=>'South Sudan',
                            'ES'=>'Spain',
                            'LK'=>'Sri Lanka',
                            'SD'=>'Sudan',
                            'SR'=>'Suriname',
                            'SJ'=>'Svalbard and Jan Mayen',
                            'SZ'=>'Swaziland',
                            'SE'=>'Sweden',
                            'CH'=>'Switzerland',
                            'SY'=>'Syrian Arab Republic',
                            'TW'=>'Taiwan, Province of China',
                            'TJ'=>'Tajikistan',
                            'TZ'=>'Tanzania, United Republic of',
                            'TH'=>'Thailand',
                            'TL'=>'Timor-Leste',
                            'TG'=>'Togo',
                            'TK'=>'Tokelau',
                            'TO'=>'Tonga',
                            'TT'=>'Trinidad and Tobago',
                            'TN'=>'Tunisia',
                            'TR'=>'Turkey',
                            'TM'=>'Turkmenistan',
                            'TC'=>'Turks and Caicos Islands',
                            'TV'=>'Tuvalu',
                            'UG'=>'Uganda',
                            'UA'=>'Ukraine',
                            'AE'=>'United Arab Emirates',
                            'GB'=>'United Kingdom',
                            'US'=>'United States',
                            'UM'=>'United States Minor Outlying Islands',
                            'UY'=>'Uruguay',
                            'UZ'=>'Uzbekistan',
                            'VU'=>'Vanuatu',
                            'VE'=>'Venezuela',
                            'VN'=>'Viet Nam',
                            'VG'=>'Virgin Islands, British',
                            'VI'=>'Virgin Islands, U.S.',
                            'WF'=>'Wallis and Futuna',
                            'EH'=>'Western Sahara',
                            'YE'=>'Yemen',
                            'ZM'=>'Zambia',
                            'ZW'=>'Zimbabwe');                               
            
                          echo form_dropdown('country', $options, $country);
                  
                    ?>
                  </div>
                    <div class="field_50">
                    <label>Notes</label>
                    <?php echo form_textarea('p_desc', $p_desc,"class='notenew'"); ?>
				   </div>
              		<div class="field_50">
						<label>Project</label>
						<?php $p_options = ['Select'];  
							  $p_options = array_merge($p_options,$projects) ;			
						?> 
						<?php echo form_dropdown('p_name', $p_options, $p_name); ?>
					  </div>
               </div>
               
               <div class="field_row">  		
                    <div><?php echo $msg ;  ?></div>
                    <label>Access code (if applicable)</label>
                    <input type="text" name="code" value="" style="width:150px;">
              
                    <input type="hidden" name="Random_generated_log_BMI" value="<?php echo $randomdata['Random_generated_log_BMI']?>">
                    <input type="hidden" name="MASS" value="<?php echo $randomdata['MASS'];?>"> 
                    <input type="hidden" name="BMI" value="<?php echo $randomdata['BMI']; ?>">
                     <input type="hidden" name="HEIGHT" value="<?php echo $randomdata['HEIGHT']; ?>">
                      <input type="hidden" name="vp_age" value="<?php echo $randomdata['vp_age']; ?>">
               
               </div> 
              
              <!-- <span>         
                  <?php echo form_submit('mysubmit1','Pre-exercise screening');  ?> 
                  <?php echo form_submit('mysubmit2','Fitness testing',"class='start-btn fitness_test_btn'"); ?>
                  <?php echo form_submit('mysubmit3','Body composition',"class='start-btn body_comp_btn'"); ?>
               </span>
              
               <button class="lite_btn f_right">Continue</button>
              --> 
                      
        </div>
		<?php echo form_close(); ?>
  </div>
  
 
</div><!--End Wrapper --> 


<div class="footer"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; Professor Kevin Norton, Dr Lynda Norton and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>

	<div class="virtual_container">		
		<div class="virtual_box">
			<div class="male_person"><img src="<?php echo "$base/assets/images/"?>human.jpg"></div>
		</div>
		<a href="#" class="btn_generate">Generate Virtual Person</a>
	</div>    
</body>
</html>
