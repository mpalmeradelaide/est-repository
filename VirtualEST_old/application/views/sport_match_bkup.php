<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<style>

sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>

<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>
<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Sport Match</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Sport Match</div>
            <p>The morphological overlap between the client and athletes within each sport is calculated and shown in the table. The table shows how well the match is between the client and elite-level athlete. This is based on height and mass, % body fat, and calculated somatotype  [if all the necessary anthropometric variables have been entered]. A rating of 100 is a perfect match, overlap values above 80% are excellent matches, values between 0.61 and 0.80 represent substantial morphological overlap, 0.41 to 0.60 represent a moderate match, 0.10 to 0.40 suggest a poor to fair match, while values less than 0.1 show a very poor match.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/go_on_menu', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Sport Match</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<div class="f_left width_70px"><label>Height (cm)</label><input type="text" id="height_text" name="height_text" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="height_slider" type="range" value="<?php echo $_SESSION['HEIGHT'];?>" min="130" max="210" step="0.1" link-to="height_text" data-rangeslider>
						<ul class="range_numbers range_hori">
							<li>130</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>210</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>Mass (kg)</label><input type="text" id="mass_text" name="mass_text" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="mass_slider" type="range" value="<?php echo $_SESSION['MASS'];?>" min="30" max="160" step="0.1" link-to="mass_text" data-rangeslider>
						<ul class="range_numbers range_hori range_hori2">
							<li>30</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>160</li>
						</ul>
					</div>	
																			
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>% body fat</label><input type="text" id="body_fat_per" name="body_fat_per" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="body_fat_slider" type="range"  value="<?php echo $_SESSION['bodyfat'];?>" min="3" max="40" step="0.1" link-to="body_fat_per" data-rangeslider>
						<ul class="range_numbers range_hori range_hori2">
							<li>3</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>40</li>
						</ul>
					</div>																							
					
					<div class="clearfix">&nbsp;</div>
					<strong class="right_sp_head" style="margin: 20px 0;">Somatotype</strong>	
																																	
					<div class="f_left width_70px"><label>Endomorph</label><input type="text" id="endomorph_txt" name="endomorph_txt" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="endomorph_slider" type="range" value="0" min="0" max="10" step="0.1" link-to="endomorph_txt" data-rangeslider>
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Mesomorph</label><input type="text" id="mesomorph_txt" name="mesomorph_txt" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="mesomorph_slider" type="range" value="0" min="0" max="10" step="0.1" link-to="mesomorph_txt" data-rangeslider>
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>		
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Ectomorph</label><input type="text" id="ectomorph_txt" name="ectomorph_txt" value="" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="ectomorph_slider" type="range" value="0" min="0" max="10" step="0.1" link-to="ectomorph_txt" data-rangeslider>
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>
					
					<div class="clearfix">&nbsp;</div>
					<button class="lite_btn f_left" type="button" style="margin-top:30px;">Recalculate</button>
				</div>     
				
				<div class="field_30 f_right">
					<strong class="right_sp_head color_vio">Sport Overlap</strong>
					
					<div class="sport_table">
						<table width="100%" border="0">
					   <thead>
					   		<tr>
							  <th align="left" height="35px">Sport</th>
							  <th align="right" height="35px">OZ rating (%)</th>
							</tr>					   	
					   </thead>
					  <tbody>
						<?php foreach($fieldData as $val)
                                                {
                                                echo "<tr>
						  <td align='left'>".$val->sport."</td>
						  <td align='right'>XX.X</td>
						</tr>
                                                 " ; 
                                                }
                                              ?>
                                             
					  </tbody>
					</table>
					</div>
				</div>     
			</div>
        </div>
    </div>
        
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
	
  <script>
  $(document).on('click','#exit', function(){           
          document.forms["myform"].submit();
        //return false;
        });	
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
});        

 $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
     
    var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
	$text_box.val(this.value);
	calculate_currentFat($text_box);	
        });
  $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
});
      $text_box.val(this.value);   
	});
}); 
 </script>  
</body>
</html>
