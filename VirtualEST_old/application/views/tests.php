<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
</script>
</head>

<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container green_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_green.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Fitness Testing</h3>           
        </div>
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_green.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_green.jpg"></a>
        </div>
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Fitness Testing</div>
            <p>The Fitness Testing module has a number of tests available across the three energy systems.<br/>
			   The anaerobic strength and power tests involve short, high-intensity efforts and include vertical jump,
			   flight time : contact time ratio, and peak power measured on a bicycle ergometer. There is also a strength 
			   testing calculator to predict one repetition maximum [1 RM] from 5-15 submaximal lifts across a range of individual movements. 
			   General population norms and athlete values can be explored.<br/> The anaerobic capacity test is a measure of maximal work performed
			   [kJ] in a 30 s time trial on a bicycle ergometer.<br/>The aerobic tests include two submaximal tests to predict VO2max or physical
			   working capacity [PWC], the 20 m shuttle test to predict VO2max and several methods to calculate lactate threshold based on measured 
			   lactates and heart rates using progressive exercise loads. Most tests have data from age- and sex-based population norms as well as 
			   values from the best quality athletes reported in the scientific literature.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>  
    
    </div>
</div>
<div class="wrapper">
        <div class="drop_main">
        	<ul>
            	<li><a href="#" id="anaerobic_strength"><img src="<?php echo "$base/assets/images/"?>icon_anaerobic.png"> Anaerobic strength and power</a>
                	<ul class="sub_menu">
                    	<li><a href="<?php echo site_url('Fitness/vertical_jump'); ?>">vertical jump</a></li>
                        <li><a href="<?php echo site_url('Fitness/flight_time'); ?>">flight:contact time</a></li>
                        <li><a href="<?php echo site_url('Fitness/peak_power'); ?>">peak power [W]</a></li>
                        <li><a href="<?php echo site_url('Fitness/strength'); ?>">strength tests</a></li>
                    </ul>
                </li>
                <li><a href="#" id="anaerobic_capacity"><img src="<?php echo "$base/assets/images/"?>icon_capacity.png"> Anaerobic capacity</a>
                	<ul class="sub_menu">
                      <li><a href="<?php echo site_url('Fitness/total_work_done_30s'); ?>">30 s total work [kJ]</a></li>
					   <li><a href="<?php echo site_url('Fitness/maod_test');?>">maximal accumulated oxygen deficit (MAOD)</a></li>
                    </ul>
                </li>
                <li><a href="#" id="aerobic_fitness"><img src="<?php echo "$base/assets/images/"?>icon_fitness.png"> Aerobic fitness</a>
                	<ul class="sub_menu">
                    	<li><a href="#" id="VO2max">VO<sub>2max</sub> [mL/kg/min]</a>
                        	<ul class="inner_sub_menu">
                                <li><a href="<?php echo site_url('Fitness/submaximal_test'); ?>">submaximal tests</a></li>
                                <li><a href="#" id="max-txt">maximal tests</a>
									<ul class="inner_sub_menu2">
										<li><a href="<?php echo site_url('Fitness/shuttle_test'); ?>">20 m shuttle test</a></li>
										 <li><a href="<?php echo site_url('Fitness/bike_test'); ?>">Predicting VO2max using maximal-effort treadmill or bike tests</a></li>
									</ul>
								</li>
								
                                <li><a href="<?php echo site_url('Fitness/v02max_test'); ?>">measured VO<sub>2max</sub></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('Fitness/lactate_threshold'); ?>">lactate threshold</a></li>
                    </ul>
                </li>
            </ul>
        </div>
</div>
</body>
</html>
