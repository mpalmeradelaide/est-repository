<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Body Composition</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<script type="text/javascript">
    
  
var specialKeys = new Array();
        specialKeys.push(8,37, 38, 39, 40); //Backspace
        function IsNumeric(e) {
            
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        if(!ret)  { 
        alert('Only Numbers');
        }
            return ret;
        }  
    
  $( document ).ready(function() { 
     
     $('.two-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(2);
            }  
         }            
         return this; //for chaining
      }); 
      
      $('.one-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
         }            
         return this; //for chaining
      }); 
      
      
   
      
      $(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
			 if ($(window).scrollTop() > 100) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});  
      
    $("#content").hide();
    $("#content_option").hide();
    $("#content_option2").hide();
    
     $("#options_height").blur(function(){
           if($('#options_height_measured').val()=='' && $('#options_weight_measured').val()=='') {
                if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
           }else{
             // $('#bmi').val('');
             
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
               
           
         }
         
      
     });

 
     
      $("#options_weight").blur(function(){
         
         if($('#options_height_measured').val()=='' && $('#options_weight_measured').val()=='') {  
                if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                   $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                   $val = $val.toFixed(2); 
                   $('#bmi').val($val);
                   }else{
                     $('#bmi').val('');
                 }
         }else{
            // alert('hi');
            //  console.log($('#options_height_measured').val());
         // console.log($('#options_weight_measured').val());
               
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
         }
      });
      
      $("#options_height_measured").blur(function(){
         // alert($('#options_height_measured').val()!='');
        if($('#options_height_measured').val()!='' && $('#options_weight_measured').val()!='') {
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
               
           }else{
              if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
         }
           
      });
      
      $("#options_weight_measured").blur(function(){
       if($('#options_height_measured').val()!='' && $('#options_weight_measured').val()!='') {
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(2); 
                 $('#bmi').val($val);
               
           }else{
             // $('#bmi').val('');
              if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
         }
      });
      
      ////////////////Waist Hip Ratio///////
      
     
     $("#waist").blur(function(){
       if($('#waist').val()!='' && $('#hip').val()!='') {
                
                 $val = $('#waist').val() / $('#hip').val();
                 $val = $val.toFixed(2); 
                 $('#whr').val($val);
               
           }else{
              $('#whr').val('');
         }
      });
     
      $("#hip").blur(function(){
       
                if($('#waist').val()!='' && $('#hip').val()!='') {
                   $val = $('#waist').val() / $('#hip').val();
                   $val = $val.toFixed(2); 
                   $('#whr').val($val);
                   }
         else{
              $('#whr').val('');
         }
      });
  
  //////////////// SOS /////////////////////
  
  
  $("#triceps").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
                 $val = $val.toFixed(1); 
                 $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
     
     $("#biceps").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
                 $val = $val.toFixed(1);  
                 $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
  
  $("#subscapular").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
            $val = $val.toFixed(1); 
            $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
  
  
});  

        $(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
</script>

</head>

<body>
<?php  echo form_open('welcome/saveClientBodyCompositionInfo','',$hidden); ?> 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitBodyComposition" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg" title="Back to Home Screen"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Body Composition</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Body Composition</div>
            <p>There are fields to enter information about height and weight, waist and hip girths, and skinfold measures if known.
               If you are confident of accurate measures for height and weight then enter these in the ‘SELF-REPORT’ fields. MEASURED values are entered in the Measured fields.
               Your BMI will be calculated automatically based on the information you enter. Risk for cardiovascular disease and diabetes increases when BMI is above about 27 
               and if waist girth is greater than 94 cm for males or over 80 cm for females.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>      
    </div>
</div>

<!--Start Wrapper --> 
<div class="wrapper">

<!--Start login --> 
<div class="login-cont" style="display:none;">
    
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="post" id="inputs"> -->
    <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required><input name="submitBodyComposition" type="submit" value="" title="edit client details"/></span>
     </div>
<!--	</form> -->
</div><!--End login --> 

 <?php $hidden = array('userid' => $id);
        //echo form_open('welcome/saveClientBodyCompositionInfo','',$hidden); ?> 

<!--Start contain --> 
<div class="contain comp-contain-new">
   
   <!--Start left --> 
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn active" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			<div class="btn">
            <button type="submit" name="mysubmit7" class="left_panel_btn" id="myform7"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Absolute CVD risk</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			
			
			<div class="btn">
            <button type="submit" name="mysubmit8" class="left_panel_btn active" id="myform8"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Life Expectancy</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			
			
       </div>
   <!--End Left --> 
 
  <?php //print_r($fieldData); ?>
 <!--   Kritika   -->
   <!--Start right --> 
   <div class="right">
   		<div class="right-head">Body Composition</div>
   		<div class="right-section">
       
       	 <div class="field_row">
            <div class="half_container f_left">
              <p style="font-size:18px; margin-bottom:15px;">Self-Report</p>
               <?php
              if(isset($_SESSION['option_height']))
               {
                   $options_height = $_SESSION['option_height'];
               }else{
               $options_height = $fieldData[0]->option_height==''?'':$fieldData[0]->option_height;
               }
			   
			   if($options_height == 0)
			   {
				   $options_height="";
			   }
               $attrib = array(
                        'name'        => 'options_height',
                        'id'          => 'options_height',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$options_height,
                       // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
                         'tabindex'=>'1',
                         'type'=>'number',
                         'min'=>'0',
                         'max'=>99999,'step'=>'any' 
                      
                      );
               ?>
              <div class="field_50">Height (cm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              <div class="clearfix">&nbsp;</div>
              <?php  
                
                if(isset($_SESSION['option_weight']))
               {
                   $options_weight = $_SESSION['option_weight'];
               }else{
              $options_weight = $fieldData[0]->option_weight==''?'':$fieldData[0]->option_weight;
               }
			   
			   if($options_weight == 0)
			   {
				   $options_weight="";
			   }
              $attrib = array(
                        'name'        => 'options_weight',
                        'id'          => 'options_weight',
                        //'class'       => 'compo-text two-decimal',
                         'size'=>'20',
                         'value'=>$options_weight,
                       //  'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                         'tabindex'=>'2',
                         'type'=>'number',
                         'min'=>'0',
                         'max'=>'1000','max'=>99999 ,'step'=>'any' 
                      
                      );
               ?>
              <div class="field_50">Body mass (kg)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
            </div>
            
           
             
             
             <div class="half_container f_right">
            <p style="font-size:18px; margin-bottom:15px;">Measured</p>
             <?php 
              if(isset($_SESSION['option_height_measured']))
               {
                   $option_height_measured = $_SESSION['option_height_measured'];
               }else{
               $option_height_measured = $fieldData[0]->option_height_measured==''?'':$fieldData[0]->option_height_measured;
              }
             $attrib = array(
                        'name'        => 'options_height_measured',
                        'id'          => 'options_height_measured',
                        //'class'       => 'compo-text  one-decimal',
                         'size'=>'20',
                         'value'=>$option_height_measured,
                     //   'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false' 
                         'tabindex'=>'3',
                        'type'=>'number',
                         'min'=>'0','max'=>99999,'step'=>'any' 
                      
                      );
               ?>
              <div class="field_50">Height (cm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              <?php
                 
                   if(isset($_SESSION['option_weight_measured']))
               {
                   $option_weight_measured = $_SESSION['option_weight_measured'];
               }else{
              $option_weight_measured = $fieldData[0]->option_weight_measured==''?'':$fieldData[0]->option_weight_measured;
               }
                   
              $attrib = array(
                        'name'        => 'options_weight_measured',
                        'id'          => 'options_weight_measured',
                        //'class'       => 'compo-text  two-decimal',
                         'size'=>'20',
                         'value'=>$option_weight_measured,
                       // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                              'tabindex'=>'4',
             'type'=>'number',
                         'min'=>'0','max'=>99999,'step'=>'any' 
                      
                      );
               ?>
               <div class="clearfix">&nbsp;</div>
               <div class="field_50">Body mass (kg)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              
              
               <?php 
                 if(isset($_SESSION['option_bmi']))
               {
                   $option_bmi = $_SESSION['option_bmi'];
               }else{
                 $option_bmi = $fieldData[0]->option_bmi==''?'':$fieldData[0]->option_bmi;
               }
                 $attrib = array(
                        'name'        => 'bmi',
                        'id'          => 'bmi',
                        //'class'       => 'compo-text  two-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_bmi ==0?'':$option_bmi,
            'type'=>'number',
                         'min'=>'0','step'=>'any' 
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                      
                      );
               ?>
               <div class="field_50">Body mass index</div>
			   <div class="field_50"><?php echo form_input($attrib); ?></div>
              
            </div>
         </div>
      
      <!--Start Row 2--> 
          
       <div class="field_row">
            <div class="half_container f_right">
                 <?php
                
                 if(isset($_SESSION['option_waist']))
               {
                   $option_waist = $_SESSION['option_waist'];
               }else{
                  $option_waist = $fieldData[0]->option_waist==''?'':$fieldData[0]->option_waist;
               }
                  $attrib = array(
                        'name'        => 'waist',
                        'id'          => 'waist',
                        //'class'       => 'compo-text  one-decimal',
                         'size'=>'20',
                         'value'=>$option_waist,
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                         'tabindex'=>'5',
			             'type'=>'number',
                         'min'=>'0','step'=>'any' 
                      
                      );
               ?>
              <div class="field_50">Waist (cm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
               <?php
                 if(isset($_SESSION['option_hip']))
               {
                   $option_hip = $_SESSION['option_hip'];
               }else{
                   $option_hip = $fieldData[0]->option_hip==''?'':$fieldData[0]->option_hip;
               }
                
               $attrib = array(
                        'name'        => 'hip',
                        'id'          => 'hip',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_hip,
                       //  'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                    'tabindex'=>'6',
		'type'=>'number',
                         'min'=>'0','step'=>'any' 
                      
                      );
               ?>
              <div class="clearfix">&nbsp;</div>
              <div class="field_50">Hip (cm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              
              <?php 
                 if(isset($_SESSION['option_whr']))
               {
                   $option_whr = $_SESSION['option_whr'];
               }else{
                 $option_whr = $fieldData[0]->option_whr==''?'':$fieldData[0]->option_whr;
               }
                 $attrib = array(
                        'name'        => 'whr',
                        'id'          => 'whr',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_whr==0?'':$option_whr,
			             'type'=>'number',
                         'min'=>'0','step'=>'any' 
                        // 'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                      
                      );
               ?>
              <div class="field_50">Waist: hip ratio</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
            </div>
            
            
          </div><!--End Row 2-->
          
           <!--Start Row 3-->
           <div class="field_row">
            <div class="half_container f_right">
                 <?php 
                if(isset($_SESSION['triceps']))
               {
                   $option_triceps = $_SESSION['triceps'];
               }else{
                 $option_triceps = $fieldData[0]->option_triceps==''?'':$fieldData[0]->option_triceps;
               }
                 $attrib = array(
                        'name'        => 'triceps',
                        'id'          => 'triceps',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_triceps,
                     'type'=>'number',
                         'step'=>'any' ,
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                      'tabindex'=>'7',
                         'min'=>'0'
                      
                      );
               ?>              
              <div class="field_50">Triceps (mm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              
            <?php
               if(isset($_SESSION['biceps']))
               {
                   $option_biceps = $_SESSION['biceps'];
               }else{
                   $option_biceps = $fieldData[0]->option_biceps==''?'':$fieldData[0]->option_biceps;
               }
               $attrib = array(
                        'name'        => 'biceps',
                        'id'          => 'biceps',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_biceps,
                         'type'=>'number',
                         'step'=>'any' ,
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                        'tabindex'=>'8',
                         'min'=>'0'
                      
                      );
               ?>              
              <div class="clearfix">&nbsp;</div>
              <div class="field_50">Biceps (mm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              <?php 
                
                if(isset($_SESSION['subscapular']))
               {
                   $option_subscapular = $_SESSION['subscapular'];
               }else{
                   $option_subscapular = $fieldData[0]->option_subscapular==''?'':$fieldData[0]->option_subscapular;
               }
              
              $attrib = array(
                        'name'        => 'subscapular',
                        'id'          => 'subscapular',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_subscapular,
                         'type'=>'number',
                         'step'=>'any' ,
                        // 'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                   'tabindex'=>'9',
                         'min'=>'0'
                      
                      );
               ?>
               
              <div class="clearfix">&nbsp;</div>
              <div class="field_50">Subscapular (mm)</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
              
              <?php  
                if(isset( $_SESSION['option_sos']))
               {
                   $option_sos =  $_SESSION['option_sos'];
               }else{
                 $option_sos = $fieldData[0]->option_sos==''?'':$fieldData[0]->option_sos;
               }
                 $attrib = array(
                        'name'        => 'sos',
                        'id'          => 'sos',
                        //'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_sos==0?'':$option_sos,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'min'=>'0',
                         'max'=>'9999'
                      
                      );
               ?>
              <div class="field_50">Sum of skinfolds</div>
			  <div class="field_50"><?php echo form_input($attrib); ?></div>
            </div>
            
            
          </div><!--End Row 3-->
          
      <?php echo form_submit('mysubmit3','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform3'");?>
      <?php echo form_submit('mysubmit5','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform5'");?>
               
      <?php echo form_close(); ?>
           

          
      </div><!--End right section--> 
   </div><!--End right --> 
   
   
</div><!--End contain -->


</div><!--End Wrapper --> 
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
</body>
</html>
