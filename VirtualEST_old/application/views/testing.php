<script src="jStat.js"></script>
<script>

var x=0.09531;
var mean=0.1441 ;
var sd=0.219;
var cumulative=1;

var norm = normCumProb(x, mean, sd, cumulative);
console.log("norm"+norm);






function normCumProb(x,mean,stdDev,zScoreDist)
{
// First, convert x to a z-score.
var zScore = (x - mean) / stdDev;
// Next, find the number of entries in zScoreDist that are less than or equal to the z-score of x.  Divide this number by the length of the distribution array to get the percent of all z-scores that are less than or equal to the z-score of x.  This is the probability you want to calculate.
var valueCount = 0;
for (var z=0; z<zScoreDist.length; z++)
{
if (zScoreDist[z] <= zScore)
{
valueCount = valueCount + 1;
};
};
var normProbability = valueCount / zScoreDist.length;
return normProbability;
}



function NORMDIST(x, mean, sd, cumulative) {
  // Check parameters
  if (isNaN(x) || isNaN(mean) || isNaN(sd)) return '#VALUE!';
  if (sd <= 0) return '#NUM!';

  // Return normal distribution computed by jStat [http://jstat.org]
  return (cumulative) ? jStat.normal.cdf(x, mean, sd) : jStat.normal.pdf(x, mean, sd);
}

</script>