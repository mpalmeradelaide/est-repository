<?php 
$age_range=$fieldData['age_range'];
$country=$fieldData['country'];
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Blood Norm Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<style>

sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>

<style>
	body{overflow-x: hidden;}	
.range_div{ display: inline-block; width:calc(100% - 90px); margin:26px 0 0;height: auto; position: relative;} 	
.rangeslider{background:#e6e6e6 !important; box-shadow: none;}
.rangeslider__fill{background:#b3b3b3 !important;box-shadow: none;}
.rangeslider__handle{ background:#4d4d4d !important; width: 20px !important; height: 20px !important; z-index:99;}
.rangeslider--vertical { width:8px !important; height: 100%; left: 50%; margin-left: -4px;}
.rangeslider--vertical .rangeslider__handle { left: -6px !important; border: 0;}
.rangeslider__handle:after{display: none;}	
.vio_range_div .rangeslider__fill{background:#c7b5e7 !important;}
.vio_range_div .rangeslider__handle{ background:#906bd0 !important;}
ul.range_numbers {list-style: none; margin: 0; padding: 0; position: absolute; left: 0; top:0; bottom: 0; z-index: -1;} 
ul.range_numbers li {color: #808080; font-size: 13px; width:74px; text-align: right; position:relative; margin-top:3px;}
ul.range_numbers li:before,ul.range_limits li:before{content: ''; width:20px; height: 1px; position: absolute; background:#e9e9e9; right:-35%; top: 8px;}
ul.range_numbers li:first-child{margin-top:2px;}
.rang_slider{bottom: 0; left: 0; width: 100%;}	
	ul.range_numbers.range_hori{top: 10px;}
	ul.range_numbers.range_hori li{ width: 10%;
}
	

.cvd_row .field_25 .range_div output {transform: translateX(220%) !important;}
.range_div.range_horiz output{margin-left: -25px;}
.range_div.range_horiz output.blue:before {
    border-left: 5px solid transparent;
    border-right: 5px solid transparent;
    border-top: 6px solid #1966b1;
    left: 50%;
    top: auto;
    bottom: -11px;
    margin-left: -5px;
}
</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>
<script type="text/javascript" src="../../assets/js/JStat.js"></script>

    
    
    
<script type="text/javascript">
	$(document).ready(function() {
        
         var isvp='<?php echo $isvp;?>';
        
        var divHeight = $('.contain').height(); 
		 $('.side_menu').css('height', divHeight+'px');
		if(isvp == 1)
        {
          $(".v_person").fadeTo( "slow" , 1, function() {});
		  $(".v_detail").toggle();
		  //calculate_oz_rating();
		}
		//Show Sports list with oz Ranking
		
      
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 


$(function() {
  $('input[type="range"]').on('input', function() {
	var control = $(this),
    controlMin = control.attr('min'),
    controlMax = control.attr('max'),
    controlVal = control.val(),
    controlThumbWidth = control.data('thumbwidth');

  var range = controlMax - controlMin;
  
  var position = ((controlVal - controlMin) / range) * 100;
  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
  var output = control.next('output');
  
  output
    .css('left', 'calc(' + position + '% - ' + positionOffset + 'px)')
    .text(controlVal);
  var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
   // calculate_healthage();
   $text_box.val(this.value);
   
var slider_id=$(this).attr("link-to");
textchange(this.id);	
var slider_val=$(this).val();


var change_gender="";
var slider_gend = $("#gender").find("option:first-child").val();   //alert(gend);
var change_gend = $('#gend').val();  	
if(change_gend == "" || change_gend == undefined)
{
	change_gender = slider_gend;
}	
else{
	change_gender = change_gend;
}
if(slider_id=='tot_col')
{
 if(slider_val>=5.2)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}  
//HDL CHOL
if(slider_id=='hdl_chol')
{
 if(slider_val<=1.0)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//LDL 
if(slider_id=='ldl_chol')
{
 if(slider_val>=3.4)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//Triglycerides  
if(slider_id=='Trig')
{
 if(slider_val>=1.7)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//SBP
if(slider_id=='sbp')
{
 if(slider_val>=140)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//DBP
if(slider_id=='dbp')
{
 if(slider_val>=90)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//Haemoglobin 
if(slider_id=='Haemoglobin')
{
 if(slider_val< 11 || slider_val>18)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//Haematocrit  
if(slider_id=='Haematocrit')
{
	if(change_gender == 'M')
	{   console.log("M");
        
		 if(slider_val< 39 || slider_val>50 )
		 {
		  var colr='#FF0000';   
		 }
		 else{
			 colr='#75a1cb';
		 }
	}
 //else
	 if(change_gender == 'F')
	 { console.log("F");
		 if(slider_val< 35 || slider_val>46)
		 {
		 colr='#FF0000';    
		 }
		 else{
			 colr='#75a1cb';
		 }
	 }
}
//Fasting blood glucose 
if(slider_id=='fbg')
{
 if(slider_val>5.5)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}

var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
   
    $(this).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ','+colr+'), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')'
                );
});
});

$(function() {
  
  if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
  $('input[type="range"]').bind('touchstart', function() {
	 var control = $(this),
    controlMin = control.attr('min'),
    controlMax = control.attr('max'),
    controlVal = control.val(),
    controlThumbWidth = control.data('thumbwidth');

  var range = controlMax - controlMin;
  
  var position = ((controlVal - controlMin) / range) * 100;
  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
  var output = control.next('output');
  
  output
    .css({'display':'block','transform':'translateX(0)','-webkit-transform':'translateX(0)','-webkit-backface-visibility':'hidden','backface-visibility':'hidden','bottom':'32px','left':'calc(' + position + '% - ' + positionOffset + 'px)'})
    .text(controlVal);
  var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
   // calculate_healthage();
   $text_box.val(this.value);
});
$('input[type="range"]').bind('touchend', function() {
    $("output").css({'display':'none'});
});
 }
 else
 {
	$('input[type="range"]').on('mouseenter', function() {
		 var control = $(this),
		controlMin = control.attr('min'),
		controlMax = control.attr('max'),
		controlVal = control.val(),
		controlThumbWidth = control.data('thumbwidth');

	  var range = controlMax - controlMin;
	  
	  var position = ((controlVal - controlMin) / range) * 100;
	  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
	  var output = control.next('output');
	  console.log(controlVal);
	  output
		.css({'display':'block','transform':'translateX(0)','-webkit-transform':'translateX(0)','-webkit-backface-visibility':'hidden','backface-visibility':'hidden','bottom':'32px','left':'calc(' + position + '% - ' + positionOffset + 'px)'})
		.text(controlVal);
	  var $slider = $(this),
		 $text_box = $('#'+$(this).attr('link-to'));
		$text_box.val(this.value);
	});
	$('input[type="range"]').bind('mouseleave', function() {
		$("output").css({'display':'none'});
	});	 
 }
});


 $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
      var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     console.log($text_box);
	 $slider.change(function(){
             $text_box.val(this.value);
             
             console.log('sliding'); 
       
        var slider_id=$(this).attr("link-to");

var slider_val=$(this).val();
textchange(slider_id);
var change_gender="";
var slider_gend = $("#gender").find("option:first-child").val();   //alert(gend);
var change_gend = $('#gend').val();  	
if(change_gend == "" || change_gend == undefined)
{
	change_gender = slider_gend;
}	
else{
	change_gender = change_gend;
}


if(slider_id=='tot_col')
{
 if(slider_val>=5.2)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}  
//HDL CHOL
if(slider_id=='hdl_chol')
{
 if(slider_val<=1.0)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//LDL 
if(slider_id=='ldl_chol')
{
 if(slider_val>=3.4)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//Triglycerides  
if(slider_id=='Trig')
{
 if(slider_val>=1.7)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
} 
//SBP
if(slider_id=='sbp')
{
 if(slider_val>=140)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//DBP
if(slider_id=='dbp')
{
 if(slider_val>=90)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//Haemoglobin 
if(slider_id=='Haemoglobin')
{
 if(slider_val< 11 || slider_val>18)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//Haematocrit  
if(slider_id=='Haematocrit')
{
 if(slider_val< 39 || slider_val>46 && (change_gender == 'M'))
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
//Fasting blood glucose 
if(slider_id=='fbg')
{
 if(slider_val>5.5)
 {
  var colr='#FF0000';   
 }
 else
 {
 colr='#75a1cb';    
 }
}
 var val = ($(this).val() - $(this).attr('min')) / ($(this).attr('max') - $(this).attr('min'));
   $(this).css('background-image',
                '-webkit-gradient(linear, left top, right top, '
                + 'color-stop(' + val + ','+colr+'), '
                + 'color-stop(' + val + ', #C5C5C5)'
                + ')'
                );
	});
   $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
 
    });
      $text_box.val(this.value);   
	});
}); 


	
/* $(document).on('click', '.info_block_new_btn', function () {  
			$(".info_block_new").toggle();
			$(".overlay").toggle();
		}); */
 $(document).on('click', '.info_block_new_btn', function () {
                var info_id = this.id;
                 $.ajax({
                  url:'<?php echo site_url('Body/get_info_details');?>',  
                   type:'POST',
                   data:{info_id:info_id},
                   dataType:'json',
                   success:function(data)
                    {
						console.log(data);
					$(".info_block_head").text(data.heading);
                    $("#headings").text(data.heading);       
                    //$("#content").html(data.head_content);
                    $("#method").text(data.method_content);
                  //  var info_images=data.images; 
		    //var ajax_image = "<img src='http://115.112.118.252/VirtualEST/assets/images/est_anthrop/"+info_images+"'/>";
                  //  $(".info_img").html(ajax_image); 
                    }
                 })   
                $(".info_block_new").toggle();
                $(".overlay").toggle();
       });

			
$(document).on('click','.close_new', function(){
	$(".info_block_new").toggle();
	$(".overlay").toggle();
}); 	

</script>    

</head>
<body>
     <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],1);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="<?php echo $_SESSION['bodyfat']; ?>">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
    
    
    
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container well_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_blue.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Blood Biomarkers</h3>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_blue.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_blue.jpg"></a>
        </div>
        
        
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Blood Biomarkers</div>
            <p>This screen allows the user to enter blood biomarker and blood pressure values to determine the percentile rank against age- and sex-specific normative values. The norms are from population surveys and are updated as new national survey data are released. International cut-offs published by the World Health Organisation (WHO) are shown for some of the variables. The sliders will turn red to indicate values are above/below threshold levels for health risk. Conversion factors are as follows: total cholesterol, HDL, LDL (mM to mg/dL) x 38.61 ; triglycerides (mM to mg/dL) x 88.5 ; glucose (mM to mg/dL) x 18.02.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/go_on_menu', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<!--Start right --> 
        <div class="right-section right-section_new blood_screen" style="width: 965px;">
        
        	<div class="field_row verticle_field_row mt_36"> 
                <div class="field_24">
                	<label>Gender</label>
					<select id="gender" name="gender" onchange = '$("#gend").val(this.value);'>
						<option value="M">Male</option>
						<option value="F">Female</option>
					</select>
                </div>
				<div class="field_24">
				<label>Age (yr)</label>
						<select id="age" name="age" onchange = '$("#agechange").val(this.value);' >
						<?php foreach($age_range as $range) 
						{
						echo "<option value='$range->age'>$range->age</option>";
						}?>
						</select>
				</div>
                <div class="field_24">
                 <label>Population norms</label>
					<select  name="population_norm" id="population_norm" onchange = '$("#population").val(this.value);'>
						<?php foreach($country as $ctry) 
						{
						echo "<option value='$ctry->country'>$ctry->country</option>";
						}?>
					</select>
                </div>
				
				<input type="hidden" name="agechange" id="agechange">
				<input type="hidden" name="population" id="population">
				<input type="hidden" name="gend" id="gend">
				
				<input type="hidden" name="text_id" id="text_id">
				
             </div>
             
        	<div class="right-head">Blood lipids</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="42"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>Total cholesterol (mM)</label>
					<div class="f_left width_70px"><input type="text" id="tot_col" name="tot_col" value="" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="tot_col_range" id="tot_col_range" type="range" value="0" min="0" max="12" step="0.1" link-to="tot_col" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>0</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>12</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>Percentile rank</label>
					<input type="text" id="totchol_text" name="totchol_text" value="">
				</div>         
			</div>
       		<div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="43"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>HDL cholesterol (mM)</label>
					<div class="f_left width_70px"><input type="text" id="hdl_chol" name="hdl_chol" value="" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="hdl_chol_slider" id="hdl_chol_slider" type="range" value="0" min="0" max="4" step="0.1" link-to="hdl_chol" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>0</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>4</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>&nbsp;</label>
					<input type="text" id="hdlchol_text" name="hdlchol_text" value="">
				</div>         
			</div>
		    <div class="field_row" style="border: 0;">
					<div class="field_70 f_left">
						<a href="#" class="info_block_new_btn" id="44"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
						<label>LDL cholesterol (mM)</label>
						<div class="f_left width_70px"><input type="text" id="ldl_chol" name="ldl_chol" value="" style="width:54px;" > </div>
						<div class="range_div range_horiz"> 
							<input name="ldl_chol_slider" id="ldl_chol_slider" type="range" value="0" min="0" max="6" step="0.1" link-to="ldl_chol" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
							<output name="rangeVal" class="blue"></output>
							<ul class="range_numbers range_hori">
							  <li>0</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>&nbsp;</li>
							  <li>6</li>
							</ul>
						  </div>	
					</div> 
					<div class="field_30 f_right">
						<label>&nbsp;</label>
						<input type="text" id="ldlchol_text" name="ldlchol_text" value="">
					</div>         
				</div>
       		<div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="45"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>Triglycerides (mM)</label>
					<div class="f_left width_70px"><input type="text" id="Trig" name="Trig" value="0" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="Trig_slider" id="Trig_slider" type="range" value="0" min="0" max="6" step="0.1" link-to="Trig" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>0</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>6</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>&nbsp;</label>
					<input type="text" id="trig_text" name="trig_text" value="">
				</div>         
			</div>
       
       		<div class="right-head">Blood pressure</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="46"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>SBP (mmHg)</label>
					<div class="f_left width_70px"><input type="text" id="sbp" name="sbp" value="" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="sbp_slider" id="sbp_slider" type="range" value="80" min="80" max="180" step="1" link-to="sbp" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>80</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>180</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>Percentile rank</label>
					<input type="text" id="sbp_text" name="sbp_text" value="">
				</div>         
			</div>
       		<div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="47"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>DBP (mmHg)</label>
					<div class="f_left width_70px"><input type="text" id="dbp" name="dbp" value="" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="dbp_slider" id="dbp_slider" type="range" value="40" min="40" max="120" step="1" link-to="dbp" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>40</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>120</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>&nbsp;</label>
					<input type="text" id="dbp_text" name="dbp_text" value="">
				</div>         
			</div>
       
       		<div class="right-head">Other blood measures</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="48"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>Haemoglobin (gm/dL)</label>
					<div class="f_left width_70px"><input type="text" id="Haemoglobin" name="Haemoglobin" value="" style="width:54px;"> </div>
					<div class="range_div range_horiz"> 
						<input name="Haemoglobin_slider" id="Haemoglobin_slider" type="range" value="4" min="4" max="20" step="0.1" link-to="Haemoglobin" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>4</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>20</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>Percentile rank</label>
					<input type="text" id="Haemoglobin_text" name="Haemoglobin_text" value="">
				</div>         
			</div>
       		<div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="49"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>Haematocrit (%)</label>
					<div class="f_left width_70px"><input type="text" id="Haematocrit" name="Haematocrit" value="30" style="width:54px;" > </div>
					<div class="range_div range_horiz"> 
						<input name="Haematocrit_slider" id="Haematocrit_slider" type="range" value="30" min="30" max="60" step="0.1" link-to="Haematocrit" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>30</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>60</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>&nbsp;</label>
					<input type="text" id="haematocrit_text" name="haematocrit_text" value="">
				</div>         
			</div>`
       		<div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<a href="#" class="info_block_new_btn" id="50"><img src="<?php echo "$base/assets/images/"?>blue_info4.png" class="info_icon"></a>
					<label>Fasting blood glucose (mM)</label>
					<div class="f_left width_70px"><input type="text" id="fbg" name="fbg" value="2" style="width:54px;"> </div>
					<div class="range_div range_horiz"> 
						<input name="fbg_slider" id="fbg_slider" type="range" value="2" min="2" max="12" step="0.1" link-to="fbg" data-thumbwidth="20" data-rangeslider class="rang_slider blue">
						<output name="rangeVal" class="blue"></output>
						<ul class="range_numbers range_hori">
						  <li>2</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>&nbsp;</li>
						  <li>12</li>
						</ul>
					  </div>	
				</div> 
				<div class="field_30 f_right">
					<label>&nbsp;</label>
					<input type="text" id="glu_text" name="glu_text" value="">
				</div>         
			</div>
        </div>
    </div>
	
	
	
	 <!-- Block for Information Popup-->
                    <div class="info_block_new">
                        <div class="info_block_head" style="background: #1866B1;">Full Profile</div>
                        <div class="info_content">
                            <!--div class="info_img">
                               
                            </div-->
                            <div class="info_txt" style="width:100%">
                                <h3 id="headings"></h3>	
                                <div class="clearfix">&nbsp;</div>
                                <h3></h3>
                                <p id="method"></p>
                            </div>
                        </div>
                        <div class="info_block_foot">
                            <a href="#" class="lite_btn grey_btn f_right close_new">Close</a>
                        </div>                
                    </div> 
	
	
      
     <!--div class="info_block_new">
	  <div class="info_block_head" style="background: #1966b1;">Mid-axilla</div>
	  <div class="info_content">
		<div class="info_txt">
		  <h3 id="headings">Mid-axilla</h3>
		</div>
	  </div>
	  <div class="info_block_foot"> <a href="#" class="lite_btn grey_btn f_right close_new">Close</a> </div>
	</div-->
 
      
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>	




<script type="text/javascript">
var pop="";
var gend="";
var age="";
$(document).ready(function(){
	    pop = $("#population_norm").find("option:first-child").val(); //alert(pop);
	    gend = $("#gender").find("option:first-child").val();   //alert(gend);
	    age = $("#age").find("option:first-child").val();       //alert(age);
});

    function textchange(id)
	{
		console.log(id);
		var norm_age="";
		var norm_pop="";
		var norm_gend="";
		
		var agechange = $('#agechange').val();  				
		if(agechange == "" || agechange == undefined) 
		{norm_age = age;
		}else{ norm_age = agechange;
		}
		//console.log("After Slect"+norm_age);
		
		var population = $('#population').val(); 
		if(population == "" || population == undefined) 
		{ norm_pop=pop;    // if not chnage on select , 
		}else{
			norm_pop=population; // get the value on change					
		}
		//console.log("After Slect Pop"+norm_pop);
		var gender = $('#gend').val();
		if(gender == "" || gender == undefined) 
		{ norm_gend=gend; // By Default
		} else{
		  norm_gend=gender;  // On select change
		}
				
		id_getnorm(norm_age,norm_pop,norm_gend,id);
				 				
	}


     $("select").change( function(){ // any select that changes.
	 console.log("Select change"); 
				var norm_age="";
				var norm_pop="";
				var norm_gend="";
				
				var agechange = $('#agechange').val();  				
				if(agechange == "" || agechange == undefined) 
				{norm_age = age;
				}else{ norm_age = agechange;
				}
				//console.log("After Slect"+norm_age);
				
				var population = $('#population').val(); 
				if(population == "" || population == undefined) 
				{ norm_pop=pop;    // if not chnage on select , 
				}else{
					norm_pop=population; // get the value on change					
				}
				//console.log("After Slect Pop"+norm_pop);
				var gender = $('#gend').val();
				if(gender == "" || gender == undefined) 
				{ norm_gend=gend; // By Default
				} else{
   				  norm_gend=gender;  // On select change
				}
				
				
				getnorm(norm_age,norm_pop,norm_gend);
       });    // End of Select

 	function getnorm(norm_age,norm_pop,norm_gend)
	{
		       $.ajax({
						type: "POST",
						url: '<?php echo base_url(); ?>index.php/Body/getnormvalue',
						data:{'age': norm_age , 'gender' : norm_gend, 'population' : norm_pop },
						success: function(data){
							
							var obj = jQuery.parseJSON( data );
											// Colestrol
			                				var mean_cols = obj[0].mean_cols;
											var sd_cols = obj[0].sd_cols;							
											var colsvalue=$('#tot_col').val();
												 if(colsvalue>=5.2)
												 {
												  var colr='#FF0000';   
												 }
												 else
												 {
												     colr='#75a1cb';    
												 }
											//var $slider_id=$('#tot_col_range');
											var val = ($('#tot_col_range').val() - $('#tot_col_range').attr('min')) / ($('#tot_col_range').attr('max') - $('#tot_col_range').attr('min'));
										    $('#tot_col_range').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);
											
											//alert("cols_value"+colsvalue);
											var z_score_cols = (parseFloat(colsvalue) - parseFloat(mean_cols)) / parseFloat(sd_cols) ;
											var cols_percentile = calculate_percentile(z_score_cols);
											cols_percentile = cols_percentile.toFixed(1);
										//	console.log("cols_percentile"+cols_percentile);											
											$('#totchol_text').val(cols_percentile);
											
											
											//SBP 
											var mean_sbp = obj[0].mean_sbp;
											var sd_sbp = obj[0].sd_sbp;							
											var sbpvalue=$('#sbp').val();
											console.log("sbpvalue"+sbpvalue);
											 if(sbpvalue>=140)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											 colr='#75a1cb';    
											 }
											var val = ($('#sbp_slider').val() - $('#sbp_slider').attr('min')) / ($('#sbp_slider').attr('max') - $('#sbp_slider').attr('min'));
										    $('#sbp_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);											
											
											
											var z_score_sbp = (parseFloat(sbpvalue) - parseFloat(mean_sbp)) / parseFloat(sd_sbp) ;
											var sbp_percentile = calculate_percentile(z_score_sbp);  sbp_percentile = sbp_percentile.toFixed(1);
										//	console.log("sbp_percentile"+sbp_percentile);							
											$('#sbp_text').val(sbp_percentile);
											
											
											
											
											// DBP
											var mean_dbp = obj[0].mean_dbp;
											var sd_dbp = obj[0].sd_dbp;							
											var dbpvalue=$('#dbp').val();
											console.log("dbpvalue"+dbpvalue);
											if(dbpvalue>=90)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											 colr='#75a1cb';    
											 }
											 
											var val = ($('#dbp_slider').val() - $('#dbp_slider').attr('min')) / ($('#dbp_slider').attr('max') - $('#dbp_slider').attr('min'));
										    $('#dbp_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);	
											
											var z_score_dbp = (parseFloat(dbpvalue) - parseFloat(mean_dbp)) / parseFloat(sd_dbp) ;
											var dbp_percentile = calculate_percentile(z_score_dbp); dbp_percentile = dbp_percentile.toFixed(1);
										//	console.log("dbp_percentile"+dbp_percentile);							
											$('#dbp_text').val(dbp_percentile);
											
											
											
											
											
											// HDl
											var mean_ln_hdl = obj[0].mean_ln_hdl;
											var sd_ln_hdl = obj[0].sd_ln_hdl;							
											var hdlvalue=$('#hdl_chol').val();
											console.log("hdlvalue"+hdlvalue);
											
											 if(hdlvalue<=1.0)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											 colr='#75a1cb';    
											 }
											var val = ($('#hdl_chol_slider').val() - $('#hdl_chol_slider').attr('min')) / ($('#hdl_chol_slider').attr('max') - $('#hdl_chol_slider').attr('min'));
										    $('#hdl_chol_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);	
											
											var log_hdl=Math.log(hdlvalue);                                     
											    console.log("log_hdl"+log_hdl);
											var hdl_percentile = NORMDIST(log_hdl,mean_ln_hdl,sd_ln_hdl,1);  
											hdl_percentile = hdl_percentile * 100;
											hdl_percentile = hdl_percentile.toFixed(1);
											    console.log("hdl_percentile"+hdl_percentile);
											$('#hdlchol_text').val(hdl_percentile); 
											
											/* var z_score_hdl = (parseFloat(hdlvalue) - parseFloat(mean_ln_hdl)) / parseFloat(sd_ln_hdl) ;
											z_score_hdl = Math.exp(z_score_hdl);
											var hdl_percentile = calculate_percentile(z_score_hdl); hdl_percentile = hdl_percentile.toFixed(1); */
										//	console.log("hdl_percentile"+hdl_percentile);
											//$('#hdlchol_text').val(hdl_percentile);
											
											
											// ldl
											var mean_ldl = obj[0].mean_ldl;
											var sd_ldl = obj[0].sd_ldl;							
											var ldlvalue=$('#ldl_chol').val();
											if(ldlvalue>=3.4)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											 colr='#75a1cb';    
											 }	
											var val = ($('#ldl_chol_slider').val() - $('#ldl_chol_slider').attr('min')) / ($('#ldl_chol_slider').attr('max') - $('#ldl_chol_slider').attr('min'));
										    $('#ldl_chol_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);				
											
											console.log("ldlvalue"+ldlvalue);
											var z_score_ldl = (parseFloat(ldlvalue) - parseFloat(mean_ldl)) / parseFloat(sd_ldl) ;
											var ldl_percentile = calculate_percentile(z_score_ldl);  ldl_percentile = ldl_percentile.toFixed(1);
											$('#ldlchol_text').val(ldl_percentile);
											
											
											
											// trig
											var mean_ln_trig = obj[0].mean_ln_trig;
											var sd_ln_trig = obj[0].sd_ln_trig;							
											var trigvalue=$('#Trig').val();
											console.log("trigvalue"+trigvalue);
											if(trigvalue>=1.7)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											 colr='#75a1cb';    
											 }
											 
											 var val = ($('#Trig_slider').val() - $('#Trig_slider').attr('min')) / ($('#Trig_slider').attr('max') - $('#Trig_slider').attr('min'));
										    $('#Trig_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);
											
											var log_trig=Math.log(trigvalue);                                     
											   console.log("log_trig"+log_trig);
											var trig_percentile = NORMDIST(log_trig,mean_ln_trig,sd_ln_trig,1);  
											   console.log("trig_percentile"+trig_percentile);											
											trig_percentile = trig_percentile * 100;
											   console.log("trig_percentile"+trig_percentile);
											trig_percentile = trig_percentile.toFixed(1);
											   console.log("trig_percentile"+trig_percentile);
											
											
											
											/* var z_score_trig = (parseFloat(trigvalue) - parseFloat(mean_ln_trig)) / parseFloat(sd_ln_trig) ;
											z_score_trig = Math.exp(z_score_trig);
											var trig_percentile = calculate_percentile(z_score_trig);  trig_percentile = trig_percentile.toFixed(1); */
										//	console.log("trig_percentile"+trig_percentile);	  
											$('#trig_text').val(trig_percentile);
											
											
											// Glucose
											var mean_ln_glu = obj[0].mean_ln_glu;
											var sd_ln_glu = obj[0].sd_ln_glu;							
											var FBGvalue=$('#fbg').val();
											console.log("FBGvalue"+FBGvalue);
											 if(FBGvalue>5.5)
											 {
											  var colr='#FF0000';   
											 }
											 else
											 {
											   colr='#75a1cb';    
											 }
											var val = ($('#fbg_slider').val() - $('#fbg_slider').attr('min')) / ($('#fbg_slider').attr('max') - $('#fbg_slider').attr('min'));
										    $('#fbg_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);
											
											
											var log_fbg=Math.log(FBGvalue);                                     console.log("log_fbg"+log_fbg);
											var glu_percentile = NORMDIST(log_fbg,mean_ln_glu,sd_ln_glu,1);  
											glu_percentile = glu_percentile * 100;
											glu_percentile = glu_percentile.toFixed(1);
											
											/* var z_score_FBG = (parseFloat(FBGvalue) - parseFloat(mean_ln_glu)) / parseFloat(sd_ln_glu) ;
											z_score_FBG = Math.exp(z_score_FBG);
											var glu_percentile = calculate_percentile(z_score_FBG);  glu_percentile = glu_percentile.toFixed(1); */
											//console.log("glu_percentile"+glu_percentile);							
											$('#glu_text').val(glu_percentile);
											
											
											// Haemoglobin
											var haemoglobin = obj[0].haemoglobin;
											var sd_haemoglobin = obj[0].sd_haemoglobin;							
											var Haemoglobinvalue=$('#Haemoglobin').val();
											console.log("Haemoglobinvalue"+Haemoglobinvalue);
											if(Haemoglobinvalue< 11 || Haemoglobinvalue>18)
												 {
												  var colr='#FF0000';   
												 }
												 else
												 {
												 colr='#75a1cb';    
												 }
												 
											var val = ($('#Haemoglobin_slider').val() - $('#Haemoglobin_slider').attr('min')) / ($('#Haemoglobin_slider').attr('max') - $('#Haemoglobin_slider').attr('min'));
										    $('#Haemoglobin_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);	 
											
											
											var z_score_Haemoglobin = (parseFloat(Haemoglobinvalue) - parseFloat(haemoglobin)) / parseFloat(sd_haemoglobin) ;
											//z_score_Haemoglobin = Math.exp(z_score_Haemoglobin);
											var Haemoglobin_percentile = calculate_percentile(z_score_Haemoglobin);  Haemoglobin_percentile = Haemoglobin_percentile.toFixed(1);
											//console.log("Haemoglobin_percentile"+Haemoglobin_percentile);							
											$('#Haemoglobin_text').val(Haemoglobin_percentile);
											
											
											
											// haematocrit
											var haematocrit = obj[0].haematocrit;
											haematocrit = haematocrit * 100;
											var sd_haematocrit = obj[0].sd_haematocrit;							
											sd_haematocrit = sd_haematocrit * 100;						
											
											var haematocritvalue=$('#Haematocrit').val();
											
											if(norm_gend == 'M')
											{   console.log("M");
												
												 if(haematocritvalue< 39 || haematocritvalue>50 )
												 {
												  var colr='#FF0000';   
												 }
												 else{
													 colr='#75a1cb';
												 }
											}
										 //else
											 if(norm_gend == 'F')
											 { console.log("F");
												 if(haematocritvalue< 35 || haematocritvalue>46)
												 {
												 colr='#FF0000';    
												 }
												 else{
													 colr='#75a1cb';
												 }
											 }
											var val = ($('#Haematocrit_slider').val() - $('#Haematocrit_slider').attr('min')) / ($('#Haematocrit_slider').attr('max') - $('#Haematocrit_slider').attr('min'));
										    $('#Haematocrit_slider').css('background-image',
														'-webkit-gradient(linear, left top, right top, '
														+ 'color-stop(' + val + ','+colr+'), '
														+ 'color-stop(' + val + ', #C5C5C5)'
														+ ')'
														);
											
											
											
											
											
											
											
											console.log("haematocritvalue"+haematocritvalue);
											var z_score_haematocrit = (parseFloat(haematocritvalue) - parseFloat(haematocrit)) / parseFloat(sd_haematocrit) ;
											//z_score_haematocrit = Math.exp(z_score_haematocrit);
											var haematocrit_percentile = calculate_percentile(z_score_haematocrit);  haematocrit_percentile = haematocrit_percentile.toFixed(1);
										//	console.log("haematocrit_percentile"+haematocrit_percentile);							
											$('#haematocrit_text').val(haematocrit_percentile);
											
						}  // end of select change when id=null
					
				}); 
    }
	 	
	
	
	
	function id_getnorm(norm_age,norm_pop,norm_gend,txtid)
	{
		   //console.log(txtid);
		   var obj="";
		   
		   
 		   $.ajax({
						type: "POST",
						url: '<?php echo base_url(); ?>index.php/Body/getnormvalue',
						data:{'age': norm_age , 'gender' : norm_gend, 'population' : norm_pop },
						success: function(data){
							//console.log(data);
							obj = jQuery.parseJSON(data);
						console.log(obj);
						// Colestrol
					 	if(txtid == "tot_col")
						{
						var mean_cols = obj[0].mean_cols;
						var sd_cols = obj[0].sd_cols;							
						var colsvalue=$('#tot_col').val();
						var z_score_cols = (parseFloat(colsvalue) - parseFloat(mean_cols)) / parseFloat(sd_cols) ;
						var cols_percentile = calculate_percentile(z_score_cols); cols_percentile = cols_percentile.toFixed(1);
						$('#totchol_text').val(cols_percentile);
						}
						if(txtid == "hdl_chol")    // hdl ln 
						{
							var mean_ln_hdl = obj[0].mean_ln_hdl;
							var sd_ln_hdl = obj[0].sd_ln_hdl;							
							var hdlvalue=$('#hdl_chol').val();
							var log_hdl=Math.log(hdlvalue);                                     
								console.log("log_hdl"+log_hdl);
							var hdl_percentile = NORMDIST(log_hdl,mean_ln_hdl,sd_ln_hdl,1);  
							hdl_percentile = hdl_percentile * 100;
							hdl_percentile = hdl_percentile.toFixed(1);
								console.log("hdl_percentile"+hdl_percentile);
							$('#hdlchol_text').val(hdl_percentile); 
						}  
						if(txtid == "ldl_chol")
						{
							var mean_ldl = obj[0].mean_ldl;
							var sd_ldl = obj[0].sd_ldl;							
							var ldlvalue=$('#ldl_chol').val();	
							var z_score_ldl = (parseFloat(ldlvalue) - parseFloat(mean_ldl)) / parseFloat(sd_ldl) ;
							var ldl_percentile = calculate_percentile(z_score_ldl);  ldl_percentile = ldl_percentile.toFixed(1);
							$('#ldlchol_text').val(ldl_percentile);
						} 
						if(txtid == "Trig")
						{
						// trig
							var mean_ln_trig = obj[0].mean_ln_trig;      // ln trig
							var sd_ln_trig = obj[0].sd_ln_trig;							
							var trigvalue=$('#Trig').val();
							var log_trig=Math.log(trigvalue);                                     
								console.log("log_trig"+log_trig);
							var trig_percentile = NORMDIST(log_trig,mean_ln_trig,sd_ln_trig,1);  
								console.log("trig_percentile"+trig_percentile);
							
							trig_percentile = trig_percentile * 100;
								console.log("trig_percentile"+trig_percentile);
							trig_percentile = trig_percentile.toFixed(1);
								console.log("trig_percentile"+trig_percentile);
							$('#trig_text').val(trig_percentile);
						} 
						if(txtid == "sbp")
						{
						//SBP 
							var mean_sbp = obj[0].mean_sbp;
							var sd_sbp = obj[0].sd_sbp;							
							var sbpvalue=$('#sbp').val();
							var z_score_sbp = (parseFloat(sbpvalue) - parseFloat(mean_sbp)) / parseFloat(sd_sbp) ;
							var sbp_percentile = calculate_percentile(z_score_sbp);   sbp_percentile = sbp_percentile.toFixed(1);
							$('#sbp_text').val(sbp_percentile);
						} 
						if(txtid == "dbp")
						{
						// DBP
							var mean_dbp = obj[0].mean_dbp;
							var sd_dbp = obj[0].sd_dbp;							
							var dbpvalue=$('#dbp').val();
							var z_score_dbp = (parseFloat(dbpvalue) - parseFloat(mean_dbp)) / parseFloat(sd_dbp) ;
							var dbp_percentile = calculate_percentile(z_score_dbp);  dbp_percentile = dbp_percentile.toFixed(1);
							$('#dbp_text').val(dbp_percentile);
						} 
						if(txtid == "fbg")
						{
						// Glucose
							var mean_ln_glu = obj[0].mean_ln_glu;
							var sd_ln_glu = obj[0].sd_ln_glu;							
							var FBGvalue=$('#fbg').val();
							var log_fbg=Math.log(FBGvalue);                                     
									console.log("log_fbg"+log_fbg);
							var glu_percentile = NORMDIST(log_fbg,mean_ln_glu,sd_ln_glu,1);  
									console.log("glu_percentile"+glu_percentile);		
							glu_percentile = glu_percentile * 100;
									console.log("glu_percentile"+glu_percentile);		
							glu_percentile = glu_percentile.toFixed(2);
									console.log("glu_percentile"+glu_percentile);			
							
							$('#glu_text').val(glu_percentile);
						} 
						
						if(txtid == "Haemoglobin")
						{
							var haemoglobin = obj[0].haemoglobin;
							var sd_haemoglobin = obj[0].sd_haemoglobin;							
							var Haemoglobinvalue=$('#Haemoglobin').val();
							var z_score_Haemoglobin = (parseFloat(Haemoglobinvalue) - parseFloat(haemoglobin)) / parseFloat(sd_haemoglobin) ;
							//z_score_Haemoglobin = Math.exp(z_score_Haemoglobin);
							var Haemoglobin_percentile = calculate_percentile(z_score_Haemoglobin);  Haemoglobin_percentile = Haemoglobin_percentile.toFixed(1);
						//	console.log("Haemoglobin_percentile"+Haemoglobin_percentile);							
							$('#Haemoglobin_text').val(Haemoglobin_percentile);
						}
						
						if(txtid == "Haematocrit")
						{
							// make spreadsheet values multiple by 100
							var haematocrit = obj[0].haematocrit;
							    haematocrit = haematocrit * 100;
							var sd_haematocrit = obj[0].sd_haematocrit;							
							    sd_haematocrit = sd_haematocrit * 100;
							
							var haematocritvalue=$('#Haematocrit').val();
							var z_score_haematocrit = (parseFloat(haematocritvalue) - parseFloat(haematocrit)) / parseFloat(sd_haematocrit) ;
							//z_score_haematocrit = Math.exp(z_score_haematocrit);
							var haematocrit_percentile = calculate_percentile(z_score_haematocrit);  haematocrit_percentile = haematocrit_percentile.toFixed(1);
						//	console.log("haematocrit_percentile"+haematocrit_percentile);							
							$('#haematocrit_text').val(haematocrit_percentile);
						}
						
						
						
				}	
         }); 
	}	
	
	
	
	
	

function NORMDIST(x, mean, sd, cumulative) {
  // Check parameters
  if (isNaN(x) || isNaN(mean) || isNaN(sd)) return '#VALUE!';
  if (sd <= 0) return '#NUM!';

  // Return normal distribution computed by jStat [http://jstat.org]
  return (cumulative) ? jStat.normal.cdf(x, mean, sd) : jStat.normal.pdf(x, mean, sd);
} 
	
	
   function calculate_percentile(z_score)
	{
		var z_scoree = Math.floor(parseFloat(z_score) * 1000) / 1000 ;
	// NORMDIST for z-score 
		var perform_percent = normalcdf(z_scoree);	
		console.log("perform percent"+perform_percent);
        /* perform_percent=Math.exp(perform_percent);
        console.log("perform_percent after exponent"+perform_percent); */
		if(isNaN(parseFloat(perform_percent * 100)))
		{              
		 return ;
		}
		else
		{                
  		 return ( Math.floor(parseFloat(perform_percent * 100) * 1000000) / 1000000 );
		} 
	}
	
    
   function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    }   
	
    

</script>




</body>
</html>
