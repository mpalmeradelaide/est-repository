<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>overlay.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
.left_block{float:left; width:100%;}
.left_block ul{margin:0; padding:0; list-style:none;}
.left_block ul li{text-align: left;  width: 50%;  display: inline-block; font-size: 14px; color: #906bd0; float: left; border-bottom: 1px solid #eee; padding: 20px 0;} 
.left_block ul li .value_box{color: #0e151a; font-size: 20px;} 
.right_block{margin-bottom: 20px; float: right; width: 100%; background: #f2f2f2; margin-top: 25px;} 
.right_block img{float: right; margin: 25px;}
.right_content{float: left; position: relative; margin: 20px 0 0 30px;}
.right_content .total_pred{font-size: 14px; color: #906bd0;}
.right_content .total_pred strong{font-size: 20px; color: #0e151a; font-weight: normal !important;}
.right_content .total_pred strong span{float:none; margin:0; font-size:20px;}
.right_content .body_txt{margin-top:20px; text-align: left; font-size: 14px; line-height:26px; color:#0e151a;}
.right_content .body_txt p{margin:0;}
.right_content .body_txt span{margin:0; float:none;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head> 
<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Full Profile / Fractionation of body mass</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>    
    </div>
</div>

<div class="wrapper">
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Body/skinfold_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?> 

    <div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Fractionation of body mass</div>
            
            <div class="field_row verticle_field_row" style="margin-bottom:0;"> 
                <div class="field_24">
                	<label>Gender</label>
                	<input type="text" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == 'M'){ echo "Male" ;}else{ echo "Female" ;} ;?>"> 
                </div>
                <div class="field_24">
                	<label>Actual body mass [kg]</label>
                	<input type="text" id="actual_mass" name="actual_mass" value="<?php echo isset($fractionation_Values["body_mass"])?$fractionation_Values["body_mass"]:""; ?>"> 
                </div>  
             </div>
             
             <div class="population_div_main">                             
                <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>"> 
                <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                <input type="hidden" id="height" name="height" value="<?php echo isset($fractionation_Values["height"])?$fractionation_Values["height"]:""; ?>"> 
                <input type="hidden" id="body_mass" name="body_mass" value="<?php echo isset($fractionation_Values["body_mass"])?$fractionation_Values["body_mass"]:""; ?>"> 
                <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>">                          
            </div>   
            
            <div class="field_row verticle_field_row" style="border:0;"> 
                <div class="half_container f_left">
                	<div class="left_block">
                    	<ul>
                            <li>FAT mass [kg] <div class="value_box" id="fat_mass"></div></li>
                            <li>MUSCLE mass [kg] <div class="value_box"  id="muscle_mass"></div></li>
                            <li>BONE mass [kg] <div class="value_box"  id="bone_mass"></div></li>
                            <li>RESIDUAL mass [kg] <div class="value_box"  id="residual_mass"></div></li>
                        </ul>
                    </div>
                </div>
                <div class="half_container f_right">
                	<div class="right_block">
                    	<div class="right_content">
                        	<div class="total_pred">
                            	<p>TOTAL predicted</p>
                                <strong><span id="total_mass"></span> kg</strong>
                            </div>
                            <div class="body_txt">
                            	<p>Lean body mass = <span id="lean_mass"></span> kg</p>
                                <p>Z Lean body mass = <span id="z_lean_mass"></span></p>
                                <p>% body fat = <span id="body_fat"></span>%</p>
                                <p>Z % body fat = <span id="z_body_fat"></span></p>
                            </div>                           
                        </div>
                    	<img src="<?php echo "$base/$image"?>/human_skelton.png" alt="">
                    </div>
                </div>
            </div>
            
            <div style="display:none;">
            	<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
            </div>
    </div>

             <input type="hidden" id="client_x" name="client_x" value="">
             <input type="hidden" id="client_y" name="client_y" value="">
             
             <input type="hidden" id="selectedSports_x" name="selectedSports_x">
             <input type="hidden" id="selectedSports_y" name="selectedSports_y">
             
             <input type="hidden" id="triceps" name="triceps" value="<?php echo isset($fractionation_Values["triceps"])?$fractionation_Values["triceps"]:""; ?>">              
             <input type="hidden" id="subscapular" name="subscapular" value="<?php echo isset($fractionation_Values["subscapular"])?$fractionation_Values["subscapular"]:""; ?>">              
             <input type="hidden" id="thigh" name="thigh" value="<?php echo isset($fractionation_Values["thigh"])?$fractionation_Values["thigh"]:""; ?>"> 
             <input type="hidden" id="calf" name="calf" value="<?php echo isset($fractionation_Values["calf"])?$fractionation_Values["calf"]:""; ?>"> 
             <input type="hidden" id="relArmG" name="relArmG" value="<?php echo isset($fractionation_Values["relArmG"])?$fractionation_Values["relArmG"]:""; ?>"> 
             <input type="hidden" id="chestG" name="chestG" value="<?php echo isset($fractionation_Values["chestG"])?$fractionation_Values["chestG"]:""; ?>"> 
             <input type="hidden" id="calfG" name="calfG" value="<?php echo isset($fractionation_Values["calfG"])?$fractionation_Values["calfG"]:""; ?>"> 
             <input type="hidden" id="thighG" name="thighG" value="<?php echo isset($fractionation_Values["thighG"])?$fractionation_Values["thighG"]:""; ?>">
             
             <input type="hidden" id="zscore_Triceps" name="zscore_Triceps" value="<?php echo isset($fractionation_Values["zscore_Triceps"])?$fractionation_Values["zscore_Triceps"]:""; ?>">
             <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular" value="<?php echo isset($fractionation_Values["zscore_Subscapular"])?$fractionation_Values["zscore_Subscapular"]:""; ?>">
             <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale" value="<?php echo isset($fractionation_Values["zscore_Supspinale"])?$fractionation_Values["zscore_Supspinale"]:""; ?>">
             <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_Thigh" name="zscore_Thigh" value="<?php echo isset($fractionation_Values["zscore_Thigh"])?$fractionation_Values["zscore_Thigh"]:""; ?>">
             <input type="hidden" id="zscore_Calf" name="zscore_Calf" value="<?php echo isset($fractionation_Values["zscore_Calf"])?$fractionation_Values["zscore_Calf"]:""; ?>">
             <input type="hidden" id="zscore_ForearmG" name="zscore_ForearmG" value="<?php echo isset($fractionation_Values["zscore_ForearmG"])?$fractionation_Values["zscore_ForearmG"]:""; ?>">
             <input type="hidden" id="zscore_WristG" name="zscore_WristG" value="<?php echo isset($fractionation_Values["zscore_WristG"])?$fractionation_Values["zscore_WristG"]:""; ?>">
             <input type="hidden" id="zscore_AnkleG" name="zscore_AnkleG" value="<?php echo isset($fractionation_Values["zscore_AnkleG"])?$fractionation_Values["zscore_AnkleG"]:""; ?>">
             <input type="hidden" id="zscore_Humerus" name="zscore_Humerus" value="<?php echo isset($fractionation_Values["zscore_Humerus"])?$fractionation_Values["zscore_Humerus"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_Femur" name="zscore_Femur" value="<?php echo isset($fractionation_Values["zscore_Femur"])?$fractionation_Values["zscore_Femur"]:""; ?>">
             <input type="hidden" id="zscore_Biac" name="zscore_Biac" value="<?php echo isset($fractionation_Values["zscore_Biac"])?$fractionation_Values["zscore_Biac"]:""; ?>">
             <input type="hidden" id="zscore_Billio" name="zscore_Billio" value="<?php echo isset($fractionation_Values["zscore_Billio"])?$fractionation_Values["zscore_Billio"]:""; ?>">
             <input type="hidden" id="zscore_TrChest" name="zscore_TrChest" value="<?php echo isset($fractionation_Values["zscore_TrChest"])?$fractionation_Values["zscore_TrChest"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_ApChest" name="zscore_ApChest" value="<?php echo isset($fractionation_Values["zscore_ApChest"])?$fractionation_Values["zscore_ApChest"]:""; ?>">             
             
                                     
             <input type="hidden" id="exit_key" name="exit_key" value="">
             <input type="hidden" id="action_key" name="action_key" value="">
             
	<!-- <?php echo form_close(); ?>-->
<!-- Form ends -->
	
</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
	
	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
    }); 
	
	$(document).ready(function() {	  
		$(".overlay").overlay();
	});
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>overlay.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<link rel="stylesheet" type="text/css" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
 
   $(window).bind("load", function() {
      
    var age = document.getElementById("age").value ; 
    var ht = document.getElementById("height").value ;
    var body_mass = document.getElementById("body_mass").value ;
    
    var triceps = document.getElementById("triceps").value ;            
    var subscapular = document.getElementById("subscapular").value ;
    var thigh = document.getElementById("thigh").value ;  
    var calf = document.getElementById("calf").value ;  
    var relArmG = document.getElementById("relArmG").value ;            
    var chestG = document.getElementById("chestG").value ;    
    var calfG = document.getElementById("calfG").value ; 
    var thighG = document.getElementById("thighG").value ; 
    
    var zscore_Triceps = document.getElementById("zscore_Triceps").value ;            
    var zscore_Subscapular = document.getElementById("zscore_Subscapular").value ;   
    var zscore_Supspinale = document.getElementById("zscore_Supspinale").value ; 
    var zscore_Abdominal = document.getElementById("zscore_Abdominal").value ;  
    var zscore_Thigh = document.getElementById("zscore_Thigh").value ;  
    var zscore_Calf = document.getElementById("zscore_Calf").value ;
    var zscore_ForearmG = document.getElementById("zscore_ForearmG").value ;   
    var zscore_WristG = document.getElementById("zscore_WristG").value ;    
    var zscore_AnkleG = document.getElementById("zscore_AnkleG").value ; 
    var zscore_Humerus = document.getElementById("zscore_Humerus").value ; 
    var zscore_Femur = document.getElementById("zscore_Femur").value ; 
    var zscore_Biac = document.getElementById("zscore_Biac").value ; 
    var zscore_Billio = document.getElementById("zscore_Billio").value ; 
    var zscore_TrChest = document.getElementById("zscore_TrChest").value ; 
    var zscore_ApChest = document.getElementById("zscore_ApChest").value ; 
    
    var pi = 3.1415962 ;    
     
    var meanFatZ = (parseFloat(zscore_Calf) + parseFloat(zscore_Thigh) + parseFloat(zscore_Abdominal) + parseFloat(zscore_Triceps) + parseFloat(zscore_Subscapular) + parseFloat(zscore_Supspinale))/6 ; 
    var fat_mass = (meanFatZ * 3.25 + 12.13)/[(170.18/ht) * (170.18/ht) * (170.18/ht)];    
    document.getElementById("fat_mass").innerHTML = Math.floor(parseFloat(fat_mass) * 10)/10 ; // Fat Mass
    
    var ZCorrChestG = ((parseFloat(chestG) - parseFloat(subscapular) * pi / 10) * 170.18 / ht - 82.46) / 4.86 ; 
    var ZCorrRelArmG = ((parseFloat(relArmG) - parseFloat(triceps) * pi / 10) * 170.18 / ht - 22.05) / 1.91 ; 
    var ZCorrThighG = ((parseFloat(thighG) - parseFloat(thigh) * pi / 10) * 170.18 / ht - 47.34) / 3.59 ; 
    var ZCorrCalfG = ((parseFloat(calfG) - parseFloat(calf) * pi / 10) * 170.18 / ht - 30.22) / 1.97 ; 
    //var meanLeanZ = (ZCorrCalfG + ZCorrThighG + ZCorrRelArmG + ZCorrChestG + zscore_ForearmG) / 5 ; 
	
	// Kritika Code Start
	 var grades = new Array();
       grades[0]=parseFloat(ZCorrCalfG);
       grades[1]=parseFloat(ZCorrThighG);
       grades[2]=parseFloat(ZCorrRelArmG);
       grades[3]=parseFloat(ZCorrChestG);
       grades[4]=parseFloat(zscore_ForearmG);
    
    var meanLeanZ = getAvg(grades) ;   
    var meanLeanZ = parseFloat(meanLeanZ) ;  
	// Kritika Code End
	
    var muscle_mass = (meanLeanZ * 2.99 + 25.55)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("muscle_mass").innerHTML = Math.floor(parseFloat(muscle_mass) * 10)/10 ; // Muscle Mass
        
    var meanSkeletalZ = (parseFloat(zscore_Femur) + parseFloat(zscore_Humerus) + parseFloat(zscore_AnkleG) + parseFloat(zscore_WristG)) / 4 ; 
    var bone_mass = (meanSkeletalZ * 1.57 + 10.49)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("bone_mass").innerHTML = Math.floor(parseFloat(bone_mass) * 10)/10 ; // Bone Mass
    
    var MeanResidualZ = (parseFloat(zscore_Biac) + parseFloat(zscore_TrChest) + parseFloat(zscore_Billio) + parseFloat(zscore_ApChest)) / 4 ; 
    var residual_mass = (MeanResidualZ * 1.9 + 16.41)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("residual_mass").innerHTML = Math.floor(parseFloat(residual_mass) * 10)/10 ; // Residual Mass
    
    var total_mass = fat_mass + muscle_mass + bone_mass + residual_mass ;
    document.getElementById("total_mass").innerHTML = Math.floor(parseFloat(total_mass) * 10)/10 ; // Total predicted mass
    
    var lean_mass = muscle_mass + bone_mass + residual_mass ; 
    document.getElementById("lean_mass").innerHTML = Math.floor(parseFloat(lean_mass) * 10)/10 ; // Lean body mass
    
    var ZLean_mass = (lean_mass * ((170.18/ht) * (170.18/ht) * (170.18/ht)) - 52.45) / 6.14 ;
    document.getElementById("z_lean_mass").innerHTML = Math.floor(parseFloat(ZLean_mass) * 10)/10 ; // Z Lean body mass
        
    var body_fat = 100 * fat_mass / (residual_mass + bone_mass + muscle_mass + fat_mass) ; 
    document.getElementById("body_fat").innerHTML = Math.floor(parseFloat(body_fat) * 10)/10 ;  // % body fat
    
    var ZBody_fat = (body_fat * ((170.18/ht) * (170.18/ht) * (170.18/ht)) - 18.78) / 5.2 ; 
    document.getElementById("z_body_fat").innerHTML = Math.floor(parseFloat(ZBody_fat) * 10)/10 ;  // Z % body fat
    
}); 


    
function getAvg (grades) {
    var fLen = grades.length;
    var sum=0;
    for (i = 0; i < fLen; i++) {
            sum += grades[i];
     }
    return (sum/fLen);
}


</script>
</body>
</html>
