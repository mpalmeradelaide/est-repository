<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script type="text/javascript">	
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
</script>
</head>
 
<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container green_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit"><img src="<?php echo "$base/assets/images/"?>back_green.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Fitness Testing</h3>
            <p>Anaerobic strength and power / strength tests</p>
        </div>
        
        
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_green.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_green.jpg"></a>
        </div>
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
        	<div class="info_block_head">Strength Tests</div>
            <p>The strength tests include up to 7 different tests [L and R grip, bench press, arm curl, lateral pulldown, leg press, 
			   leg extension and leg curl]. It is not necessary to do all the tests in order to get an overall percentile rank for 
			   strength against sex-specific population norms. Whatever tests are completed will result in a ranking for that test
			   plus the result will be included in the overall percentile ranking.</p>
            <p>The tests are submaximal [except the grip strength test which is a maximum effort using a grip dynamometer].
     		   It is important that the lifting techniques are strict and that, ideally, the client completes between 5-15 reps per 
			   strength test before failure. This means the predicted 1 RM and their calculated strength : weight ratio will be more 
			   accurate than if they complete more or less reps. There is an age-correction factor to account for age-related declines
			   in strength, generally from about 30 years of age.</p>
               <div class="info_block_foot">
                    <a href="#" class="lite_btn grey_btn f_right close">Close</a>
                 </div>  
        </div> 
    
    </div>
</div>

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientStrengthInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
            	<li><a href="#" id="anaerobic_strength"><img src="<?php echo "$base/assets/images/"?>icon_anaerobic.png"> Anaerobic strength and power</a>
                	<ul class="sub_menu">
                    	<li><a href="<?php echo site_url('Fitness/vertical_jump'); ?>">vertical jump</a></li>
                        <li><a href="<?php echo site_url('Fitness/flight_time'); ?>">flight:contact time</a></li>
                        <li><a href="<?php echo site_url('Fitness/peak_power'); ?>">peak power [W]</a></li>
                        <li><a href="<?php echo site_url('Fitness/strength'); ?>">strength tests</a></li>
                    </ul>
                </li>
                <li><a href="#" id="anaerobic_capacity"><img src="<?php echo "$base/assets/images/"?>icon_capacity.png"> Anaerobic capacity</a>
                	<ul class="sub_menu">
                      <li><a href="<?php echo site_url('Fitness/total_work_done_30s'); ?>">30 s total work [kJ]</a></li>
                    </ul>
                </li>
                <li><a href="#" id="aerobic_fitness"><img src="<?php echo "$base/assets/images/"?>icon_fitness.png"> Aerobic fitness</a>
                	<ul class="sub_menu">
                    	<li><a href="#" id="VO2max">VO<sub>2max</sub> [mL/kg/min]</a>
                        	<ul class="inner_sub_menu">
                                <li><a href="<?php echo site_url('Fitness/submaximal_test'); ?>">submaximal tests</a></li>
                                <li><a href="<?php echo site_url('Fitness/shuttle_test'); ?>">20 m shuttle test</a></li>
                                <li><a href="<?php echo site_url('Fitness/v02max_test'); ?>">measured VO<sub>2max</sub></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('Fitness/lactate_threshold'); ?>">lactate threshold</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Strength Tests</div>
        	<div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label>Age (yr)</label>
                    <input type="text" id="age" name="age" onKeyUp="clear_fetch()" class="cus-input" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->age)?$fieldData[0]->age:$_SESSION['age']; ?>" tabindex="1">
                </div>
                <div class="field_24">
                	<label>Body mass (kg)</label> 
                    <input type="text" id="weight" name="weight" onKeyUp="clear_fetch()" class="cus-input" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->weight)?$fieldData[0]->weight:""; ?>" tabindex="2">
                </div>
                <div class="field_24" style="margin-top:42px;">
                	<input type="radio" name="gender" id="gender_Male" onchange="clear_fetch()" value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "checked";}?>><label for="gender_Male" style="display:inline-block;"><span></span> &nbsp;Male</label>
                    <input type="radio" name="gender" id="gender_Female" onchange="clear_fetch()" value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "checked";}?>><label for="gender_Female" style="display:inline-block; margin-left:20px;"><span></span> &nbsp;Female</label>
                </div>
            </div>
            
            <div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label>Left grip (kg)</label>
                    <input type="text" id="leftGrip" class="cus-input" name="leftGrip" onKeyUp="getLeftAverage()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->leftGrip)?$fieldData[0]->leftGrip:""; ?>" tabindex="3">
                </div>
                <div class="field_24">
                	<label>Right grip (kg)</label>
                    <input type="text" id="rightGrip" class="cus-input" name="rightGrip" onKeyUp="getRightAverage()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->rightGrip)?$fieldData[0]->rightGrip:""; ?>" tabindex="4">
                </div>
                <div class="field_24">
                	<label>Average grip strength (kg)</label>
                    <input type="text" id="avgGrip" name="avgGrip" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->avgGrip)?$fieldData[0]->avgGrip:""; ?>" readonly>
                </div>
                <div class="field_24">
                	<label>Grip strength rank (%)</label>
                    <input type="text" id="gripRank" name="gripRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->gripRank)?$fieldData[0]->gripRank:""; ?>" readonly>
                </div>
            </div>
            
            <table class="graph_table strength_table" width="100%">
          <tbody>
            <tr>
              <td>&nbsp;</td>
              <td align="center">kg</td>
              <td align="center">Reps</td>
              <td width="50">&nbsp;</td>
              <td align="center">Pred 1RM</td>
              <td align="center">S:W ratio</td>
              <td align="center">%rank</td>
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right" width="160"><label>Bench press</label></td>
              <td><input type="text" id="benchPressKG" name="benchPressKG" class="cus-input" onKeyUp="benchPress()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->benchPressKG)?$fieldData[0]->benchPressKG:""; ?>" tabindex="5"></td>
              <td><input type="text" id="benchPressRM" name="benchPressRM" class="cus-input" onKeyUp="benchPress()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->benchPressRM)?$fieldData[0]->benchPressRM:""; ?>" tabindex="6"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="bpPred" name="bpPred"  onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->bpPred)?$fieldData[0]->bpPred:""; ?>" readonly></td>
              <td><input type="text" id="bpSW" name="bpSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->bpSW)?$fieldData[0]->bpSW:""; ?>" readonly></td>
              <td><input type="text" id="bpRank" name="bpRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->bpRank)?$fieldData[0]->bpRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank1" name="per_rank1">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right"><label>Arm curl</label></td>
              <td><input type="text" id="armCurlKG" name="armCurlKG" onKeyUp="armCurl()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->armCurlKG)?$fieldData[0]->armCurlKG:""; ?>" class="cus-input" tabindex="7"></td>
              <td><input type="text" id="armCurlRM" name="armCurlRM" class="cus-input" onKeyUp="armCurl()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->armCurlRM)?$fieldData[0]->armCurlRM:""; ?>" tabindex="8"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="acPred" name="acPred" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->acPred)?$fieldData[0]->acPred:""; ?>" readonly></td>
              <td><input type="text" id="acSW" name="acSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->acSW)?$fieldData[0]->acSW:""; ?>" readonly></td>
              <td><input type="text" id="acRank" name="acRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->acRank)?$fieldData[0]->acRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank2" name="per_rank2">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right"><label>Lateral pulldown</label></td>
              <td><input type="text" id="lPullKG" name="lPullKG" onKeyUp="leteralPulldown()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPullKG)?$fieldData[0]->lPullKG:""; ?>" class="cus-input" tabindex="9"></td>
              <td><input type="text" id="lPullRM" name="lPullRM" class="cus-input" onKeyUp="leteralPulldown()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPullRM)?$fieldData[0]->lPullRM:""; ?>" tabindex="10"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="lPullPred" name="lPullPred" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPullPred)?$fieldData[0]->lPullPred:""; ?>" readonly></td>
              <td><input type="text" id="lPullSW" name="lPullSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPullSW)?$fieldData[0]->lPullSW:""; ?>" readonly></td>
              <td><input type="text" id="lPullRank" name="lPullRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPullRank)?$fieldData[0]->lPullRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank3" name="per_rank3">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right"><label>Leg press</label></td>
              <td><input type="text" id="lPressKG" name="lPressKG" onKeyUp="legPress()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPressKG)?$fieldData[0]->lPressKG:""; ?>" class="cus-input" tabindex="11"></td>
              <td><input type="text" id="lPressRM" name="lPressRM" class="cus-input" onKeyUp="legPress()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPressRM)?$fieldData[0]->lPressRM:""; ?>" tabindex="12"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="lPressPred" name="lPressPred" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPressPred)?$fieldData[0]->lPressPred:""; ?>" readonly></td>
              <td><input type="text" id="lPressSW" name="lPressSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPressSW)?$fieldData[0]->lPressSW:""; ?>" readonly></td>
              <td><input type="text" id="lPressRank" name="lPressRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lPressRank)?$fieldData[0]->lPressRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank4" name="per_rank4">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right"><label>Leg extension</label></td>
              <td><input type="text" id="lExtKG" name="lExtKG" onKeyUp="legExtension()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lExtKG)?$fieldData[0]->lExtKG:""; ?>" class="cus-input" tabindex="13"></td>
              <td><input type="text" id="lExtRM" name="lExtRM" class="cus-input" onKeyUp="legExtension()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lExtRM)?$fieldData[0]->lExtRM:""; ?>" tabindex="14"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="lExtPred" name="lExtPred" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lExtPred)?$fieldData[0]->lExtPred:""; ?>" readonly></td>
              <td><input type="text" id="lExtSW" name="lExtSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lExtSW)?$fieldData[0]->lExtSW:""; ?>" readonly></td>
              <td><input type="text" id="lExtRank" name="lExtRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lExtRank)?$fieldData[0]->lExtRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank5" name="per_rank5">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td align="right"><label>Leg curl</label></td>
              <td><input type="text" id="lCurlKG" name="lCurlKG" onKeyUp="legCurl()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lCurlKG)?$fieldData[0]->lCurlKG:""; ?>" class="cus-input" tabindex="15"></td>
              <td><input type="text" id="lCurlRM" name="lCurlRM" class="cus-input" onKeyUp="legCurl()" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lCurlRM)?$fieldData[0]->lCurlRM:""; ?>" tabindex="16"></td>
              <td width="50">&nbsp;</td>
              <td><input type="text" id="lCurlPred" name="lCurlPred" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lCurlPred)?$fieldData[0]->lCurlPred:""; ?>" readonly></td>
              <td><input type="text" id="lCurlSW" name="lCurlSW" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lCurlSW)?$fieldData[0]->lCurlSW:""; ?>" readonly></td>
              <td><input type="text" id="lCurlRank" name="lCurlRank" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->lCurlRank)?$fieldData[0]->lCurlRank:""; ?>" readonly></td>
                  <input type="hidden" id="per_rank6" name="per_rank6">
              <td>&nbsp;</td>
            </tr>
            <tr>
              <td colspan="6" align="right"><label>Average percent rank</label></td>
              <td><input type="text" id="avgPerRank" name="avgPerRank" value="<?php echo isset($fieldData[0]->avgPerRank)?$fieldData[0]->avgPerRank:""; ?>" readonly></td>
              <td align="left">%</td>
            </tr>
          </tbody>
        </table>
            
        </div>
    </div>
		
	
	<?php echo form_close(); ?>
<!-- Form ends -->	
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

	$(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
    });	
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script>
<script>     
       
   //get average grip value(Mean grip)  
    function getLeftAverage()
    {
        var ageValue = document.getElementById("age").value;
		alert(ageValue);
	   var leftValue = document.getElementById("leftGrip");
        var left = leftValue.value;

        var avgValue = document.getElementById("avgGrip");
        if(left == "")
        {        
             document.getElementById("avgGrip").value = "" ; 
             document.getElementById("gripRank").value = "" ;
             document.getElementById("avgPerRank").value = "" ;
        }
       else
       {
            avgValue.value = parseInt(left) ;
       }                
                
        if(document.getElementById('gender_Male').checked) {
                
        if(parseInt(ageValue) >= 18 && parseInt(ageValue) <= 29)
        {
            var mean = 46.6 ;
            var sd = 8.3 ;
            
        }else if(parseInt(ageValue) >= 30 && parseInt(ageValue) <= 39) {
        
           var mean = 49.8 ;
           var sd = 8.5 ;
        
      }else if(parseInt(ageValue) >= 40 && parseInt(ageValue) <= 49) {
        
           var mean = 48.5 ;
           var sd = 8.2 ;
        
      }else if(parseInt(ageValue) >= 50 && parseInt(ageValue) <= 59) {
        
           var mean = 46.25 ;
           var sd = 7.2 ;
        
      }else if(parseInt(ageValue) >= 60 && parseInt(ageValue) <= 69) {
        
           var mean = 40.3 ;
           var sd = 7.1 ;
        
      }else if(parseInt(ageValue) >= 70) {
        
           var mean = 35.5 ;
           var sd = 7 ;        
      }
        
      }else if(document.getElementById('gender_Female').checked) {
              
        if(parseInt(ageValue) >= 18 && parseInt(ageValue) <= 29)
        {
            var mean = 29.1 ;
            var sd = 4.95 ;
            
        }else if(parseInt(ageValue) >= 30 && parseInt(ageValue) <= 39) {
        
           var mean = 29.96 ;
           var sd = 5.25 ;
        
      }else if(parseInt(ageValue) >= 40 && parseInt(ageValue) <= 49) {
        
           var mean = 29.5 ;
           var sd = 4.9 ;
        
      }else if(parseInt(ageValue) >= 50 && parseInt(ageValue) <= 59) {
        
           var mean = 27.45 ;
           var sd = 5.05 ;
        
      }else if(parseInt(ageValue) >= 60 && parseInt(ageValue) <= 69) {
        
           var mean = 25.4 ;
           var sd = 4.56 ;
        
      }else if(parseInt(ageValue) >= 70) {
        
           var mean = 22.3 ;
           var sd = 5.2 ;        
        }               
        
      }
      
      
       var grip_score = (parseFloat(mean) - parseInt(avgValue.value)) / parseFloat(sd) ;    
      if(isNaN(grip_score)){            
            document.getElementById("percentile").value = 0 ;  
        }
      
      var grip_rank = normalcdf(grip_score);
      document.getElementById("gripRank").value = Math.floor((100 - (Math.floor(parseFloat(grip_rank * 100) * 10000) / 10000)) * 1000) / 1000  ;         
      avgPerRank() ;  
      
    }
    
    function getRightAverage() 
    {
        var ageValue = document.getElementById("age").value;
        var leftValue = document.getElementById("leftGrip");
        var left = leftValue.value;
        var rightValue = document.getElementById("rightGrip");
        var right = rightValue.value;
     
     if(right != "")
     {
       var total =  parseInt(left) + parseInt(right);
       document.getElementById("avgGrip").value = parseInt(total)/2 ; 
       var avg = document.getElementById("avgGrip").value ;
     }
     else{
      
         document.getElementById("avgGrip").value = parseInt(left) ; 
         var avg = document.getElementById("avgGrip").value ;
     }
     
     if(document.getElementById('gender_Male').checked) {
                
        if(parseInt(ageValue) >= 18 && parseInt(ageValue) <= 29)
        {
            var mean = 46.6 ;
            var sd = 8.3 ;
            
        }else if(parseInt(ageValue) >= 30 && parseInt(ageValue) <= 39) {
        
           var mean = 49.8 ;
           var sd = 8.5 ;
        
      }else if(parseInt(ageValue) >= 40 && parseInt(ageValue) <= 49) {
        
           var mean = 48.5 ;
           var sd = 8.2 ;
        
      }else if(parseInt(ageValue) >= 50 && parseInt(ageValue) <= 59) {
        
           var mean = 46.25 ;
           var sd = 7.2 ;
        
      }else if(parseInt(ageValue) >= 60 && parseInt(ageValue) <= 69) {
        
           var mean = 40.3 ;
           var sd = 7.1 ;
        
      }else if(parseInt(ageValue) >= 70) {
        
           var mean = 35.5 ;
           var sd = 7 ;        
       }
        
      }else if(document.getElementById('gender_Female').checked) {
              
        if(parseInt(ageValue) >= 18 && parseInt(ageValue) <= 29)
        {
            var mean = 29.1 ;
            var sd = 4.95 ;
            
        }else if(parseInt(ageValue) >= 30 && parseInt(ageValue) <= 39) {
        
           var mean = 29.96 ;
           var sd = 5.25 ;
        
      }else if(parseInt(ageValue) >= 40 && parseInt(ageValue) <= 49) {
        
           var mean = 29.5 ;
           var sd = 4.9 ;
        
      }else if(parseInt(ageValue) >= 50 && parseInt(ageValue) <= 59) {
        
           var mean = 27.45 ;
           var sd = 5.05 ;
        
      }else if(parseInt(ageValue) >= 60 && parseInt(ageValue) <= 69) {
        
           var mean = 25.4 ;
           var sd = 4.56 ;
        
      }else if(parseInt(ageValue) >= 70) {
        
           var mean = 22.3 ;
           var sd = 5.2 ;        
        }               
        
      }
      
      var grip_score = (parseFloat(mean) - parseInt(avg)) / parseFloat(sd) ;    
      if(isNaN(grip_score)){            
            document.getElementById("percentile").value = 0 ;  
        }
      
      var grip_rank = normalcdf(grip_score);
      document.getElementById("gripRank").value = Math.floor((100 - (Math.floor(parseFloat(grip_rank * 100) * 10000) / 10000)) * 1000) / 1000  ;                  
      avgPerRank() ;      
        
    } 
    
    
     function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    }      
    
    
    //get Bench Press calculations 
    function benchPress() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var bpKG = document.getElementById("benchPressKG").value ;        
        var bpResp = document.getElementById("benchPressRM").value ; 
        
       if(parseInt(bpKG) > 0 && parseInt(bpResp) > 0)  
       {
         var bprm = parseInt(bpKG)*2.2/(1.0278-(parseInt(bpResp)*0.0278)) ;
         var BPRMkg =  parseInt(bprm)/2.2 ;
         document.getElementById("bpPred").value = Math.floor(parseFloat(BPRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var BPratio  =  parseFloat(BPRMkg)/parseInt(weight);
        var adjBPratio  =  ageCorrection * parseFloat(BPratio);      
        //alert(adjBPratio); die;
        document.getElementById("bpSW").value = Math.floor(parseFloat(adjBPratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank1 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
       if(adjBPratio > 0){
                
                if(adjBPratio <= 1.75 && adjBPratio > 1.55){
                    
                    if (adjBPratio > 1.55 && adjBPratio <= 1.57) {
                        per_rank1 = 91;
                    }
                    else if (adjBPratio > 1.57 && adjBPratio <= 1.59) {
                        
                        per_rank1 = 92;
                    }
                    else if (adjBPratio > 1.59 && adjBPratio <= 1.61) {
                        
                        per_rank1 = 93;
                    }
                    else if (adjBPratio > 1.61 && adjBPratio <= 1.63) {
                        per_rank1 = 94;
                    }
                    else if (adjBPratio > 1.63 && adjBPratio <= 1.65) {
                        per_rank1 = 95;
                        
                    }
                    else if (adjBPratio > 1.65 && adjBPratio <= 1.67) {
                        per_rank1 = 96;
                    }
                    else if (adjBPratio > 1.67 && adjBPratio <= 1.69) {
                        per_rank1 = 97;
                        
                    }
                    else if (adjBPratio > 1.69 && adjBPratio <= 1.71) {
                        per_rank1 = 98;
                        
                    }
                    else if (adjBPratio > 1.71 && adjBPratio <= 1.73) {
                        per_rank1 = 99;
                    }
                    
                    else if (adjBPratio > 1.73 && adjBPratio <= 1.75) {
                        per_rank1 = 100;
                    }
                    else  {
                        per_rank1 = 100;
                    }
                    
                }
                
                if(adjBPratio <= 1.55 && adjBPratio > 1.40){
                    
                    if (adjBPratio > 1.400 && adjBPratio <= 1.415) {
                        per_rank1 = 81;
                    }
                    else if (adjBPratio > 1.415 && adjBPratio <= 1.430) {
                        
                        per_rank1 = 82;
                    }
                    else if (adjBPratio > 1.430 && adjBPratio <= 1.445) {
                        
                        per_rank1 = 83;
                    }
                    else if (adjBPratio > 1.445 && adjBPratio <= 1.460) {
                        per_rank1 = 84;
                    }
                    else if (adjBPratio > 1.460 && adjBPratio <=  1.475) {
                        per_rank1 = 85;
                        
                    }
                    else if (adjBPratio > 1.475 && adjBPratio <= 1.490) {
                        per_rank1 = 86;
                    }
                    else if (adjBPratio > 1.490 && adjBPratio <= 1.505) {
                        per_rank1 = 87;
                        
                    }
                    else if (adjBPratio > 1.505 && adjBPratio <= 1.520) {
                        per_rank1 = 88;
                        
                    }
                    else if (adjBPratio > 1.520 && adjBPratio <= 1.535) {
                        per_rank1 = 89;
                    }
                    
                    else if (adjBPratio > 1.535 && adjBPratio <= 1.55) {
                        per_rank1 = 90;
                    }
                    else  {
                        per_rank1 = 90;
                    }
                    
                }
                
                else if(adjBPratio <= 1.40  && adjBPratio > 1.30){
                    
                    if (adjBPratio > 1.30 && adjBPratio <=   1.31) {
                        per_rank1 = 71;
                    }
                    else if (adjBPratio >  1.31 && adjBPratio <=   1.32) {
                        per_rank1 = 72;
                    }
                    else if (adjBPratio >  1.32 && adjBPratio <=   1.33) {
                        per_rank1 = 73;
                    }
                    else if (adjBPratio >  1.33 && adjBPratio <=   1.34) {
                        per_rank1 = 74;
                    }
                    else if (adjBPratio >  1.34 && adjBPratio <=   1.35) {
                        per_rank1 = 75;
                    }
                    else if (adjBPratio >  1.35 && adjBPratio <=   1.36) {
                        per_rank1 = 76;
                    }
                    else if (adjBPratio >  1.36 && adjBPratio <=   1.37) {
                        per_rank1 = 77;
                    }
                    else if (adjBPratio >  1.37 && adjBPratio <=   1.38) {
                        per_rank1 = 78;
                    }
                    else if (adjBPratio >  1.38 && adjBPratio <=   1.39) {
                        per_rank1 = 79;
                    }
                    else  {
                        per_rank1 = 80;
                    }
                    
                    
                }
                else if(adjBPratio <=  1.3 && adjBPratio >   1.2){
                    
                   
                    if (adjBPratio >  1.20 && adjBPratio <=   1.21) {
                        per_rank1 = 61;
                    }
                    else if (adjBPratio >  1.21 && adjBPratio <=   1.22) {
                        per_rank1 = 62;
                    }
                    else if (adjBPratio >  1.22 && adjBPratio <=   1.23) {
                        per_rank1 = 63;
                    }
                    else if (adjBPratio >  1.23 && adjBPratio <=   1.24) {
                        per_rank1 = 64;
                    }
                    else if (adjBPratio >  1.24 && adjBPratio <=   1.25) {
                        per_rank1 = 65;
                    }
                    else if (adjBPratio >  1.25 && adjBPratio <=   1.26) {
                        per_rank1 = 66;
                    }
                    else if (adjBPratio >  1.26 && adjBPratio <=   1.27) {
                        per_rank1 = 67;
                    }
                    else if (adjBPratio >  1.27 && adjBPratio <=   1.28) {
                        per_rank1 = 68;
                    }
                    else if (adjBPratio >  1.28 && adjBPratio <=   1.29) {
                        per_rank1 = 69;
                    }
                    else  {
                        per_rank1 = 70;
                    }
                 
                }
                else if(adjBPratio <=  1.20 && adjBPratio >   1.10){
                    
                    if (adjBPratio >  1.10 && adjBPratio <=   1.11) {
                        per_rank1 = 51;
                    }
                    else if (adjBPratio >  1.11 && adjBPratio <=   1.12) {
                        per_rank1 = 52;
                    }
                    else if (adjBPratio >  1.12 && adjBPratio <=   1.13) {
                        per_rank1 = 53;
                    }
                    else if (adjBPratio >  1.13 && adjBPratio <=   1.14) {
                        per_rank1 = 54;
                    }
                    else if (adjBPratio >  1.14 && adjBPratio <=   1.15) {
                        per_rank1 = 55;
                    }
                    else if (adjBPratio >  1.15 && adjBPratio <=   1.16) {
                        per_rank1 = 56;
                    }
                    else if (adjBPratio >  1.16 && adjBPratio <=   1.17) {
                        per_rank1 = 57;
                    }
                    else if (adjBPratio >  1.17 && adjBPratio <=   1.18) {
                        per_rank1 = 58;
                    }
                    else if (adjBPratio >  1.18 && adjBPratio <=   1.19) {
                        per_rank1 = 59;
                    }
                    else  {
                        per_rank1 = 60;
                    }
                   
                }
                else if(adjBPratio <=  1.1 && adjBPratio >   1.0){
                    
                    if (adjBPratio >  1.00 && adjBPratio <=   1.01) {
                        per_rank1 = 41;
                    }
                    else if (adjBPratio >  1.01 && adjBPratio <=   1.02) {
                        per_rank1 = 42;
                    }
                    else if (adjBPratio >  1.02 && adjBPratio <=   1.03) {
                        per_rank1 = 43;
                    }
                    else if (adjBPratio >  1.03 && adjBPratio <=   1.04) {
                        per_rank1 = 44;
                    }
                    else if (adjBPratio >  1.04 && adjBPratio <=   1.05) {
                        per_rank1 = 45;
                    }
                    else if (adjBPratio >  1.05 && adjBPratio <=   1.06) {
                        per_rank1 = 46;
                    }
                    else if (adjBPratio >  1.06 && adjBPratio <=   1.07) {
                        per_rank1 = 47;
                    }
                    else if (adjBPratio >  1.07 && adjBPratio <=   1.08) {
                        per_rank1 = 48;
                    }
                    else if (adjBPratio >  1.08 && adjBPratio <=   1.09) {
                        per_rank1 = 49;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                   
                }
                else if(adjBPratio <=  1.00  && adjBPratio >  0.90){
                    
                    if (adjBPratio >  0.90 && adjBPratio <=   0.91) {
                        per_rank1 = 31;
                    }
                    else if (adjBPratio >  0.91 && adjBPratio <=   0.92) {
                        per_rank1 = 32;
                    }
                    else if (adjBPratio >  0.92 && adjBPratio <=   0.93) {
                        per_rank1 = 33;
                    }
                    else if (adjBPratio >  0.93 && adjBPratio <=   0.94) {
                        per_rank1 = 34;
                    }
                    else if (adjBPratio >  0.94 && adjBPratio <=   0.95) {
                        per_rank1 = 35;
                    }
                    else if (adjBPratio >  0.95 && adjBPratio <=   0.96) {
                        per_rank1 = 36;
                    }
                    else if (adjBPratio >  0.96 && adjBPratio <=   0.97) {
                        per_rank1 = 37;
                    }
                    else if (adjBPratio >  0.97 && adjBPratio <=   0.98) {
                        per_rank1 = 38;
                    }
                    else if (adjBPratio >  0.98 && adjBPratio <=   0.99) {
                        per_rank1 = 39;
                    }
                    else  {
                        per_rank1 = 40;
                    }
                   
                }
                else if(adjBPratio <=  0.90  && adjBPratio >  0.75){
                    
                    
                    if (adjBPratio >  0.750 && adjBPratio <=   0.765) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio >  0.765 && adjBPratio <=   0.780) {
                        
                        per_rank1 = 22;
                    }
                    else if (adjBPratio >  0.780 && adjBPratio <=   0.795) {
                        
                        per_rank1 = 23;
                    }
                    else if (adjBPratio >  0.795 && adjBPratio <=   0.810) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio >   0.810 && adjBPratio <=   0.825) {
                        per_rank1 = 25;
                        
                    }
                    else if (adjBPratio >   0.825 && adjBPratio <=   0.840) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio >   0.840 && adjBPratio <=   0.855) {
                        per_rank1 = 27;
                        
                    }
                    else if (adjBPratio >   0.855 && adjBPratio <=   0.870) {
                        per_rank1 = 28;
                        
                    }
                    else if (adjBPratio >   0.870 && adjBPratio <=   0.885) {
                        per_rank1 = 29;
                    }
                    
                    else if (adjBPratio >   0.885 && adjBPratio <=   0.90) {
                        per_rank1 = 30;
                    }
                    else  {
                        per_rank1 = 30;
                    }
                    
                }
                else if(adjBPratio <=  0.75  && adjBPratio >  0.60){
                    
                    
                    if (adjBPratio >  0.60 && adjBPratio <=   0.615) {
                        per_rank1 = 11;
                    }
                    else if (adjBPratio >  0.615 && adjBPratio <=   0.630) {
                        
                        per_rank1 = 12;
                    }
                    else if (adjBPratio >  0.630 && adjBPratio <=   0.645) {
                        
                        per_rank1 = 13;
                    }
                    else if (adjBPratio >  0.645 && adjBPratio <=   0.660) {
                        per_rank1 = 14;
                    }
                    else if (adjBPratio >   0.660 && adjBPratio <=   0.675) {
                        per_rank1 = 15;
                        
                    }
                    else if (adjBPratio >   0.675 && adjBPratio <=   0.680) {
                        per_rank1 = 16;
                    }
                    else if (adjBPratio >   0.680 && adjBPratio <=   0.695) {
                        per_rank1 = 17;
                        
                    }
                    else if (adjBPratio >   0.695 && adjBPratio <=   0.710) {
                        per_rank1 = 18;
                        
                    }
                    else if (adjBPratio >   0.710 && adjBPratio <=   0.725) {
                        per_rank1 = 19;
                    }
                    
                    else if (adjBPratio >   0.725 && adjBPratio <=   0.740) {
                        per_rank1 = 20;
                    }
                    else  {
                        per_rank1 = 20;
                    }
                    
                }
                else if(adjBPratio <=  0.60  && adjBPratio >  0.50){
                    

                    if (adjBPratio >  0.50 && adjBPratio <=   0.51) {
                        per_rank1 = 1;
                    }
                    else if (adjBPratio >  0.51 && adjBPratio <=   0.52) {
                        per_rank1 = 2;
                    }
                    else if (adjBPratio >  0.52 && adjBPratio <=   0.53) {
                        per_rank1 = 3;
                    }
                    else if (adjBPratio >  0.53 && adjBPratio <=   0.54) {
                        per_rank1 = 4;
                    }
                    else if (adjBPratio >  0.54 && adjBPratio <=   0.55) {
                        per_rank1 = 5;
                    }
                    else if (adjBPratio >  0.55 && adjBPratio <=   0.56) {
                        per_rank1 = 6;
                    }
                    else if (adjBPratio >  0.56 && adjBPratio <=   0.57) {
                        per_rank1 = 7;
                    }
                    else if (adjBPratio >  0.57 && adjBPratio <=   0.58) {
                        per_rank1 = 8;
                    }
                    else if (adjBPratio >  0.58 && adjBPratio <=   0.59) {
                        per_rank1 = 9;
                    }
                    else  {
                        per_rank1 = 10;
                    }
                    
                    
                }
                else if(adjBPratio >  1.75){
                    per_rank1 = 100;
                }
                else if(adjBPratio <=   0.50){
                    per_rank1 = 0;
                }
                if(per_rank1 == 2222){
                    var bpRank  =  "--" ;                         
                    document.getElementById("bpRank").value = bpRank ;  // Rank %
                }
                else{                   
                     var bpRank = parseInt(per_rank1);                        
                    document.getElementById("bpRank").value = parseInt(bpRank) ;  // Rank %
                }
            }
            
            else{
                    per_rank1 = 2222;
                    var bpRank  =  "--" ;                         
                    document.getElementById("bpRank").value = bpRank ;  // Rank %
            } 
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

          if(adjBPratio >  0){
                
                if(adjBPratio <=  0.90 && adjBPratio >   0.85){
                    
                    if (adjBPratio >  0.850 && adjBPratio <=  0.855) {
                        per_rank1 = 91;
                    }
                    else if (adjBPratio >  0.855 && adjBPratio <=  0.860) {
                        per_rank1 = 92;
                    }
                    else if (adjBPratio >  0.860 && adjBPratio <=  0.865) {
                        per_rank1 = 93;
                    }
                    else if (adjBPratio >  0.865 && adjBPratio <=  0.870) {
                        per_rank1 = 94;
                    }
                    else if (adjBPratio >  0.870 && adjBPratio <=  0.875) {
                        per_rank1 = 95;
                    }
                    else if (adjBPratio >  0.875 && adjBPratio <=  0.880) {
                        per_rank1 = 96;
                    }
                    else if (adjBPratio >  0.880 && adjBPratio <=  0.885) {
                        per_rank1 = 97;
                    }
                    else if (adjBPratio >  0.885 && adjBPratio <=  0.890) {
                        per_rank1 = 98;
                    }
                    else if (adjBPratio >  0.890 && adjBPratio <=  0.895) {
                        per_rank1 = 99;
                    }
                    else  {
                        per_rank1 = 100;
                    }
                }
                else if(adjBPratio <=  0.85  && adjBPratio >   0.80){
                    
                    if (adjBPratio >  0.800 && adjBPratio <=  0.805) {
                        per_rank1 = 81;
                    }
                    else if (adjBPratio >  0.805 && adjBPratio <=  0.810) {
                        per_rank1 = 82;
                    }
                    else if (adjBPratio >  0.810 && adjBPratio <=  0.815) {
                        per_rank1 = 83;
                    }
                    else if (adjBPratio >  0.815 && adjBPratio <=  0.820) {
                        per_rank1 = 84;
                    }
                    else if (adjBPratio >  0.820 && adjBPratio <=  0.825) {
                        per_rank1 = 85;
                    }
                    else if (adjBPratio >  0.825 && adjBPratio <=  0.830) {
                        per_rank1 = 86;
                    }
                    else if (adjBPratio >  0.830 && adjBPratio <=  0.835) {
                        per_rank1 = 87;
                    }
                    else if (adjBPratio >  0.835 && adjBPratio <=  0.840) {
                        per_rank1 = 88;
                    }
                    else if (adjBPratio >  0.840 && adjBPratio <=  0.845) {
                        per_rank1 = 89;
                    }
                    else  {
                        per_rank1 = 90;
                    }
                }
                else if(adjBPratio <=  0.80 && adjBPratio >   0.75){
                    
                    if (adjBPratio >  0.750 && adjBPratio <=  0.755) {
                        per_rank1 = 71;
                    }
                    else if (adjBPratio >  0.755 && adjBPratio <=  0.760) {
                        per_rank1 = 72;
                    }
                    else if (adjBPratio >  0.760 && adjBPratio <=  0.765) {
                        per_rank1 = 73;
                    }
                    else if (adjBPratio >  0.765 && adjBPratio <=  0.770) {
                        per_rank1 = 74;
                    }
                    else if (adjBPratio >  0.770 && adjBPratio <=  0.775) {
                        per_rank1 = 75;
                    }
                    else if (adjBPratio >  0.775 && adjBPratio <=  0.780) {
                        per_rank1 = 76;
                    }
                    else if (adjBPratio >  0.780 && adjBPratio <=  0.785) {
                        per_rank1 = 77;
                    }
                    else if (adjBPratio >  0.785 && adjBPratio <=  0.790) {
                        per_rank1 = 78;
                    }
                    else if (adjBPratio >  0.790 && adjBPratio <=  0.795) {
                        per_rank1 = 89;
                    }
                    else  {
                        per_rank1 = 80;
                    }
                }
                else if(adjBPratio <=  0.75 && adjBPratio >   0.70){
                    
                    if (adjBPratio >  0.700 && adjBPratio <=  0.705) {
                        per_rank1 = 61;
                    }
                    else if (adjBPratio >  0.705 && adjBPratio <=  0.710) {
                        per_rank1 = 62;
                    }
                    else if (adjBPratio >  0.710 && adjBPratio <=  0.715) {
                        per_rank1 = 63;
                    }
                    else if (adjBPratio >  0.715 && adjBPratio <=  0.720) {
                        per_rank1 = 64;
                    }
                    else if (adjBPratio >  0.720 && adjBPratio <=  0.725) {
                        per_rank1 = 65;
                    }
                    else if (adjBPratio >  0.725 && adjBPratio <=  0.730) {
                        per_rank1 = 66;
                    }
                    else if (adjBPratio >  0.730 && adjBPratio <=  0.735) {
                        per_rank1 = 67;
                    }
                    else if (adjBPratio >  0.735 && adjBPratio <=  0.740) {
                        per_rank1 = 68;
                    }
                    else if (adjBPratio >  0.740 && adjBPratio <=  0.745) {
                        per_rank1 = 69;
                    }
                    else  {
                        per_rank1 = 70;
                    }
                }
                else if(adjBPratio <=  0.70 && adjBPratio >   0.65){
                    
                    if (adjBPratio >  0.650 && adjBPratio <=  0.655) {
                        per_rank1 = 51;
                    }
                    else if (adjBPratio >  0.655 && adjBPratio <=  0.660) {
                        per_rank1 = 52;
                    }
                    else if (adjBPratio >  0.660 && adjBPratio <=  0.665) {
                        per_rank1 = 53;
                    }
                    else if (adjBPratio >  0.665 && adjBPratio <=  0.670) {
                        per_rank1 = 54;
                    }
                    else if (adjBPratio >  0.670 && adjBPratio <=  0.675) {
                        per_rank1 = 55;
                    }
                    else if (adjBPratio >  0.675 && adjBPratio <=  0.680) {
                        per_rank1 = 56;
                    }
                    else if (adjBPratio >  0.680 && adjBPratio <=  0.685) {
                        per_rank1 = 57;
                    }
                    else if (adjBPratio >  0.685 && adjBPratio <=  0.690) {
                        per_rank1 = 58;
                    }
                    else if (adjBPratio >  0.690 && adjBPratio <=  0.695) {
                        per_rank1 = 59;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                }
                else if(adjBPratio <=  0.65  && adjBPratio >  0.60){
                    
                    if (adjBPratio >  0.600 && adjBPratio <=  0.605) {
                        per_rank1 = 41;
                    }
                    else if (adjBPratio >  0.605 && adjBPratio <=  0.610) {
                        per_rank1 = 42;
                    }
                    else if (adjBPratio >  0.610 && adjBPratio <=  0.615) {
                        per_rank1 = 43;
                    }
                    else if (adjBPratio >  0.615 && adjBPratio <=  0.620) {
                        per_rank1 = 44;
                    }
                    else if (adjBPratio >  0.620 && adjBPratio <=  0.625) {
                        per_rank1 = 45;
                    }
                    else if (adjBPratio >  0.625 && adjBPratio <=  0.630) {
                        per_rank1 = 46;
                    }
                    else if (adjBPratio >  0.630 && adjBPratio <=  0.635) {
                        per_rank1 = 47;
                    }
                    else if (adjBPratio >  0.635 && adjBPratio <=  0.640) {
                        per_rank1 = 48;
                    }
                    else if (adjBPratio >  0.640 && adjBPratio <=  0.645) {
                        per_rank1 = 49;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                }
                else if(adjBPratio <=  0.60  && adjBPratio >  0.55){
                    
                    if (adjBPratio >  0.550 && adjBPratio <=  0.555) {
                        per_rank1 = 31;
                    }
                    else if (adjBPratio >  0.555 && adjBPratio <=  0.560) {
                        per_rank1 = 32;
                    }
                    else if (adjBPratio >  0.560 && adjBPratio <=  0.565) {
                        per_rank1 = 33;
                    }
                    else if (adjBPratio >  0.565 && adjBPratio <=  0.570) {
                        per_rank1 = 34;
                    }
                    else if (adjBPratio >  0.570 && adjBPratio <=  0.575) {
                        per_rank1 = 35;
                    }
                    else if (adjBPratio >  0.575 && adjBPratio <=  0.580) {
                        per_rank1 = 36;
                    }
                    else if (adjBPratio >  0.580 && adjBPratio <=  0.585) {
                        per_rank1 = 37;
                    }
                    else if (adjBPratio >  0.585 && adjBPratio <=  0.590) {
                        per_rank1 = 38;
                    }
                    else if (adjBPratio >  0.590 && adjBPratio <=  0.595) {
                        per_rank1 = 39;
                    }
                    else  {
                        per_rank1 = 40;
                    }
                }
                else if(adjBPratio <=  0.550  && adjBPratio >  0.500){
                    
                    if (adjBPratio >  0.500 && adjBPratio <=  0.505) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio >  0.505 && adjBPratio <=  0.510) {
                        per_rank1 = 22;
                    }
                    else if (adjBPratio >  0.510 && adjBPratio <=  0.515) {
                        per_rank1 = 23;
                    }
                    else if (adjBPratio >  0.515 && adjBPratio <=  0.520) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio >  0.520 && adjBPratio <=  0.525) {
                        per_rank1 = 25;
                    }
                    else if (adjBPratio >  0.525 && adjBPratio <=  0.530) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio >  0.530 && adjBPratio <=  0.535) {
                        per_rank1 = 27;
                    }
                    else if (adjBPratio >  0.535 && adjBPratio <=  0.540) {
                        per_rank1 = 28;
                    }
                    else if (adjBPratio >  0.540 && adjBPratio <=  0.545) {
                        per_rank1 = 29;
                    }
                    else  {
                        per_rank1 = 30;
                    }
                }
                else if(adjBPratio <=  0.50  && adjBPratio >  0.45){

                    if (adjBPratio >  0.450 && adjBPratio <=  0.455) {
                        per_rank1 = 11;
                    }
                    else if (adjBPratio >  0.455 && adjBPratio <=  0.460) {
                        per_rank1 = 12;
                    }
                    else if (adjBPratio >  0.460 && adjBPratio <=  0.465) {
                        per_rank1 = 13;
                    }
                    else if (adjBPratio >  0.465 && adjBPratio <=  0.470) {
                        per_rank1 = 14;
                    }
                    else if (adjBPratio >  0.470 && adjBPratio <=  0.475) {
                        per_rank1 = 15;
                    }
                    else if (adjBPratio >  0.475 && adjBPratio <=  0.480) {
                        per_rank1 = 16;
                    }
                    else if (adjBPratio >  0.480 && adjBPratio <=  0.485) {
                        per_rank1 = 17;
                    }
                    else if (adjBPratio >  0.485 && adjBPratio <=  0.490) {
                        per_rank1 = 18;
                    }
                    else if (adjBPratio >  0.490 && adjBPratio <=  0.495) {
                        per_rank1 = 19;
                    }
                    else  {
                        per_rank1 = 20;
                    }
                }
                else if(adjBPratio  <=  0.450  && adjBPratio >  0.400){

                    if (adjBPratio >  0.400 && adjBPratio <=  0.405) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio >  0.405 && adjBPratio <=  0.410) {
                        per_rank1 = 22;
                    }
                    else if (adjBPratio >  0.410 && adjBPratio <=  0.415) {
                        per_rank1 = 23;
                    }
                    else if (adjBPratio >  0.415 && adjBPratio <=  0.420) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio >  0.420 && adjBPratio <=  0.425) {
                        per_rank1 = 25;
                    }
                    else if (adjBPratio >  0.425 && adjBPratio <=  0.430) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio >  0.430 && adjBPratio <=  0.435) {
                        per_rank1 = 27;
                    }
                    else if (adjBPratio >  0.435 && adjBPratio <=  0.440) {
                        per_rank1 = 28;
                    }
                    else if (adjBPratio >  0.440 && adjBPratio <=  0.445) {
                        per_rank1 = 29;
                    }
                    else  {
                        per_rank1 = 30;
                    }

                }
                else if (adjBPratio <=   0.40) {
                    per_rank1 = 0;
                }
                else if(adjBPratio >  0.90){
                    per_rank1 = 100;
                }      
                
                 if(per_rank1 == 2222){
                    var bpRank  =  "--" ;                         
                    document.getElementById("bpRank").value = bpRank ;  // Rank %
                }
                else{                   
                     var bpRank = parseInt(per_rank1);                        
                    document.getElementById("bpRank").value = parseInt(bpRank) ;  // Rank %
                }
                
            }
            else{
                    per_rank1 = 2222;
                    var bpRank  =  "--" ;                         
                    document.getElementById("bpRank").value = bpRank ;  // Rank %
            }               

        }            
        
       }
       
        document.getElementById("per_rank1").value = parseInt(per_rank1) ;  // Rank1 %
        avgPerRank() ;
      
    }
    
    
  //get Arm Curl calculations 
    function armCurl() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var acKG = document.getElementById("armCurlKG").value ;        
        var acResp = document.getElementById("armCurlRM").value ; 
        
       if(parseInt(acKG) > 0 && parseInt(acResp) > 0)  
       {
         var acrm = parseInt(acKG)*2.2/(1.0278-(parseInt(acResp)*0.0278)) ;
         var ACRMkg =  parseInt(acrm)/2.2 ;
         document.getElementById("acPred").value = Math.floor(parseFloat(ACRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var ACratio  =  parseFloat(ACRMkg)/parseInt(weight);
        var adjACratio  =  ageCorrection * parseFloat(ACratio);      
        //alert(adjBPratio); die;
        document.getElementById("acSW").value = Math.floor(parseFloat(adjACratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank2 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
           if(adjACratio > 0){
                
                if(adjACratio <=  0.800 && adjACratio >   0.650){
                    
                    if (adjACratio >  0.650 && adjACratio <=   0.665) {
                        per_rank2 = 91;
                    }
                    else if (adjACratio >  0.665 && adjACratio <=   0.680) {
                        per_rank2 = 92;
                    }
                    else if (adjACratio >  0.680 && adjACratio <=   0.695) {
                        per_rank2 = 93;
                    }
                    else if (adjACratio >  0.695 && adjACratio <=   0.710) {
                        per_rank2 = 94;
                    }
                    else if (adjACratio >   0.710 && adjACratio <=   0.725) {
                        per_rank2 = 95;
                    }
                    else if (adjACratio >  0.725 && adjACratio <=   0.740) {
                        per_rank2 = 96;
                    }
                    else if (adjACratio >  0.740 && adjACratio <=   0.755) {
                        per_rank2 = 97;
                    }
                    else if (adjACratio >  0.755 && adjACratio <=   0.770) {
                        per_rank2 = 98;
                    }
                    else if (adjACratio >   0.770 && adjACratio <=   0.785) {
                        per_rank2 = 99;
                    }
                    else {
                        per_rank2 = 100;
                    }
                }
                else if(adjACratio <=  0.65  && adjACratio >   0.60){
                    
                    if (adjACratio >  0.600 && adjACratio <=  0.605) {
                        per_rank2 = 81;
                    }
                    else if (adjACratio >  0.605 && adjACratio <=  0.610) {
                        per_rank2 = 82;
                    }
                    else if (adjACratio >  0.610 && adjACratio <=  0.615) {
                        per_rank2 = 83;
                    }
                    else if (adjACratio >  0.615 && adjACratio <=  0.620) {
                        per_rank2 = 84;
                    }
                    else if (adjACratio >  0.620 && adjACratio <=  0.625) {
                        per_rank2 = 85;
                    }
                    else if (adjACratio >  0.625 && adjACratio <=  0.630) {
                        per_rank2 = 86;
                    }
                    else if (adjACratio >  0.630 && adjACratio <=  0.635) {
                        per_rank2 = 87;
                    }
                    else if (adjACratio >  0.635 && adjACratio <=  0.640) {
                        per_rank2 = 88;
                    }
                    else if (adjACratio >  0.640 && adjACratio <=  0.645) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 90;
                    }

                }
                else if(adjACratio <=  0.60 && adjACratio >   0.55){
                    
                    if (adjACratio >  0.550 && adjACratio <=  0.555) {
                        per_rank2 = 71;
                    }
                    else if (adjACratio >  0.555 && adjACratio <=  0.560) {
                        per_rank2 = 72;
                    }
                    else if (adjACratio >  0.560 && adjACratio <=  0.565) {
                        per_rank2 = 73;
                    }
                    else if (adjACratio >  0.565 && adjACratio <=  0.570) {
                        per_rank2 = 74;
                    }
                    else if (adjACratio >  0.570 && adjACratio <=  0.575) {
                        per_rank2 = 75;
                    }
                    else if (adjACratio >  0.575 && adjACratio <=  0.580) {
                        per_rank2 = 76;
                    }
                    else if (adjACratio >  0.580 && adjACratio <=  0.585) {
                        per_rank2 = 77;
                    }
                    else if (adjACratio >  0.585 && adjACratio <=  0.590) {
                        per_rank2 = 78;
                    }
                    else if (adjACratio >  0.590 && adjACratio <=  0.595) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 80;
                    }
                    
                }
                else if(adjACratio <=  0.55 && adjACratio >   0.50){
                    
                    if (adjACratio >  0.500 && adjACratio <=  0.505) {
                        per_rank2 = 61;
                    }
                    else if (adjACratio >  0.505 && adjACratio <=  0.510) {
                        per_rank2 = 62;
                    }
                    else if (adjACratio >  0.510 && adjACratio <=  0.515) {
                        per_rank2 = 63;
                    }
                    else if (adjACratio >  0.515 && adjACratio <=  0.520) {
                        per_rank2 = 64;
                    }
                    else if (adjACratio >  0.520 && adjACratio <=  0.525) {
                        per_rank2 = 65;
                    }
                    else if (adjACratio >  0.525 && adjACratio <=  0.530) {
                        per_rank2 = 66;
                    }
                    else if (adjACratio >  0.530 && adjACratio <=  0.535) {
                        per_rank2 = 67;
                    }
                    else if (adjACratio >  0.535 && adjACratio <=  0.540) {
                        per_rank2 = 68;
                    }
                    else if (adjACratio >  0.540 && adjACratio <=  0.545) {
                        per_rank2 = 69;
                    }
                    else  {
                        per_rank2 = 70;
                    }

                }
                else if(adjACratio <=  0.500 && adjACratio >   0.450){
                    
                    if (adjACratio >  0.450 && adjACratio <=  0.455) {
                        per_rank2 = 51;
                    }
                    else if (adjACratio >  0.455 && adjACratio <=  0.460) {
                        per_rank2 = 52;
                    }
                    else if (adjACratio >  0.460 && adjACratio <=  0.465) {
                        per_rank2 = 53;
                    }
                    else if (adjACratio >  0.465 && adjACratio <=  0.470) {
                        per_rank2 = 54;
                    }
                    else if (adjACratio >  0.470 && adjACratio <=  0.475) {
                        per_rank2 = 55;
                    }
                    else if (adjACratio >  0.475 && adjACratio <=  0.480) {
                        per_rank2 = 56;
                    }
                    else if (adjACratio >  0.480 && adjACratio <=  0.485) {
                        per_rank2 = 57;
                    }
                    else if (adjACratio >  0.485 && adjACratio <=  0.490) {
                        per_rank2 = 58;
                    }
                    else if (adjACratio >  0.490 && adjACratio <=  0.495) {
                        per_rank2 = 59;
                    }
                    else  {
                        per_rank2 = 60;
                    }

                    
                }
                else if(adjACratio <=  0.45  && adjACratio >  0.40){
                    
                    
                    if (adjACratio >  0.400 && adjACratio <=  0.405) {
                        per_rank2 = 41;
                    }
                    else if (adjACratio >  0.405 && adjACratio <=  0.410) {
                        per_rank2 = 42;
                    }
                    else if (adjACratio >  0.410 && adjACratio <=  0.415) {
                        per_rank2 = 43;
                    }
                    else if (adjACratio >  0.415 && adjACratio <=  0.420) {
                        per_rank2 = 44;
                    }
                    else if (adjACratio >  0.420 && adjACratio <=  0.425) {
                        per_rank2 = 45;
                    }
                    else if (adjACratio >  0.425 && adjACratio <=  0.430) {
                        per_rank2 = 46;
                    }
                    else if (adjACratio >  0.430 && adjACratio <=  0.435) {
                        per_rank2 = 47;
                    }
                    else if (adjACratio >  0.435 && adjACratio <=  0.440) {
                        per_rank2 = 48;
                    }
                    else if (adjACratio >  0.440 && adjACratio <=  0.445) {
                        per_rank2 = 49;
                    }
                    else  {
                        per_rank2 = 50;
                    }
                }
                else if(adjACratio <=  0.400  && adjACratio >  0.350){
                    
                    
                    if (adjACratio >  0.350 && adjACratio <=  0.355) {
                        per_rank2 = 31;
                    }
                    else if (adjACratio >  0.355 && adjACratio <=  0.360) {
                        per_rank2 = 32;
                    }
                    else if (adjACratio >  0.360 && adjACratio <=  0.365) {
                        per_rank2 = 33;
                    }
                    else if (adjACratio >  0.365 && adjACratio <=  0.370) {
                        per_rank2 = 34;
                    }
                    else if (adjACratio >  0.370 && adjACratio <=  0.375) {
                        per_rank2 = 35;
                    }
                    else if (adjACratio >  0.375 && adjACratio <=  0.380) {
                        per_rank2 = 36;
                    }
                    else if (adjACratio >  0.380 && adjACratio <=  0.385) {
                        per_rank2 = 37;
                    }
                    else if (adjACratio >  0.385 && adjACratio <=  0.390) {
                        per_rank2 = 38;
                    }
                    else if (adjACratio >  0.390 && adjACratio <=  0.395) {
                        per_rank2 = 39;
                    }
                    else  {
                        per_rank2 = 40;
                    }

                }
                else if(adjACratio <=  0.35  && adjACratio >  0.30){
                    
                    
                    if (adjACratio >  0.300 && adjACratio <=  0.305) {
                        per_rank2 = 21;
                    }
                    else if (adjACratio >  0.305 && adjACratio <=  0.310) {
                        per_rank2 = 22;
                    }
                    else if (adjACratio >  0.310 && adjACratio <=  0.315) {
                        per_rank2 = 23;
                    }
                    else if (adjACratio >  0.315 && adjACratio <=  0.320) {
                        per_rank2 = 24;
                    }
                    else if (adjACratio >  0.320 && adjACratio <=  0.325) {
                        per_rank2 = 25;
                    }
                    else if (adjACratio >  0.325 && adjACratio <=  0.330) {
                        per_rank2 = 26;
                    }
                    else if (adjACratio >  0.330 && adjACratio <=  0.335) {
                        per_rank2 = 27;
                    }
                    else if (adjACratio >  0.335 && adjACratio <=  0.340) {
                        per_rank2 = 28;
                    }
                    else if (adjACratio >  0.340 && adjACratio <=  0.345) {
                        per_rank2 = 29;
                    }
                    else  {
                        per_rank2 = 30;
                    }
                }
                else if(adjACratio <=  0.30  && adjACratio >  0.20){
                    
                    if (adjACratio >  0.20 && adjACratio <=   0.21) {
                        per_rank2 = 11;
                    }
                    else if (adjACratio >  0.21 && adjACratio <=   0.22) {
                        per_rank2 = 12;
                    }
                    else if (adjACratio >  0.22 && adjACratio <=   0.23) {
                        per_rank2 = 13;
                    }
                    else if (adjACratio >  0.23 && adjACratio <=   0.24) {
                        per_rank2 = 14;
                    }
                    else if (adjACratio >   0.24 && adjACratio <=   0.25) {
                        per_rank2 = 15;
                    }
                    else if (adjACratio >  0.25 && adjACratio <=   0.26) {
                        per_rank2 = 16;
                    }
                    else if (adjACratio >  0.26 && adjACratio <=   0.27) {
                        per_rank2 = 17;
                    }
                    else if (adjACratio >  0.27 && adjACratio <=   0.28) {
                        per_rank2 = 18;
                    }
                    else if (adjACratio >   0.28 && adjACratio <=   0.29) {
                        per_rank2 = 19;
                    }
                    else {
                        per_rank2 = 10;
                    }
                  
                }
                else if(adjACratio <=  0.20  && adjACratio >  0.10){
                    
                    if (adjACratio >  0.10 && adjACratio <=   0.11) {
                        per_rank2 = 1;
                    }
                    else if (adjACratio >  0.11 && adjACratio <=   0.12) {
                        per_rank2 = 2;
                    }
                    else if (adjACratio >  0.12 && adjACratio <=   0.13) {
                        per_rank2 = 3;
                    }
                    else if (adjACratio >  0.13 && adjACratio <=   0.14) {
                        per_rank2 = 4;
                    }
                    else if (adjACratio >   0.14 && adjACratio <=   0.15) {
                        per_rank2 = 5;
                    }
                    else if (adjACratio >  0.15 && adjACratio <=   0.16) {
                        per_rank2 = 6;
                    }
                    else if (adjACratio >  0.16 && adjACratio <=   0.17) {
                        per_rank2 = 7;
                    }
                    else if (adjACratio >  0.17 && adjACratio <=   0.18) {
                        per_rank2 = 8;
                    }
                    else if (adjACratio >   0.18 && adjACratio <=   0.19) {
                        per_rank2 = 9;
                    }
                    else {
                        per_rank2 = 10;
                    }
                }
                else if(adjACratio <=  0.10){
                    per_rank2 = 0;
                }
                else if(adjACratio >   0.800){
                    per_rank2 = 100;
                }
                if(per_rank2 == 2222){
                    var acRank  =  "--" ;                         
                    document.getElementById("acRank").value = acRank ;  // Rank %
                }
                else{                    
                    var acRank = parseInt(per_rank2);                        
                    document.getElementById("acRank").value = parseInt(acRank) ;  // Rank %
                }
            }
            
            else{
                    per_rank2 = 2222;
                    var acRank  =  "--" ;                         
                    document.getElementById("acRank").value = acRank ;  // Rank %
            } 
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

        if(adjACratio > 0){
                
                if(adjACratio <=  0.50 && adjACratio >   0.46){
                    
                    
                    if (adjACratio >  0.46 && adjACratio <=  0.464) {
                        per_rank2 = 91;
                    }
                    else if (adjACratio >  0.464 && adjACratio <=  0.468) {
                        per_rank2 = 92;
                    }
                    else if (adjACratio >  0.468 && adjACratio <=  0.472) {
                        per_rank2 = 93;
                    }
                    else if (adjACratio >  0.472 && adjACratio <=  0.476) {
                        per_rank2 = 94;
                    }
                    else if (adjACratio >  0.476 && adjACratio <=  0.480) {
                        per_rank2 = 95;
                    }
                    else if (adjACratio >  0.480 && adjACratio <=  0.484) {
                        per_rank2 = 96;
                    }
                    else if (adjACratio >  0.484 && adjACratio <=  0.488) {
                        per_rank2 = 97;
                    }
                    else if (adjACratio >  0.488 && adjACratio <=  0.492) {
                        per_rank2 = 98;
                    }
                    else if (adjACratio >  0.492 && adjACratio <=  0.496) {
                        per_rank2 = 99;
                    }
                    else  {
                        per_rank2 = 100;
                    }
                    
                }
                else if(adjACratio <=  0.460 && adjACratio >   0.420){
                    
                    
                    if (adjACratio >  0.420 && adjACratio <=  0.424) {
                        per_rank2 = 81;
                    }
                    else if (adjACratio >  0.424 && adjACratio <=  0.428) {
                        per_rank2 = 82;
                    }
                    else if (adjACratio >  0.428 && adjACratio <=  0.432) {
                        per_rank2 = 83;
                    }
                    else if (adjACratio >  0.432 && adjACratio <=  0.436) {
                        per_rank2 = 84;
                    }
                    else if (adjACratio >  0.436 && adjACratio <=  0.440) {
                        per_rank2 = 85;
                    }
                    else if (adjACratio >  0.440 && adjACratio <=  0.444) {
                        per_rank2 = 86;
                    }
                    else if (adjACratio >  0.444 && adjACratio <=  0.448) {
                        per_rank2 = 87;
                    }
                    else if (adjACratio >  0.448 && adjACratio <=  0.452) {
                        per_rank2 = 88;
                    }
                    else if (adjACratio >  0.452 && adjACratio <=  0.456) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 90;
                    }

                    
                }////starts from here
                else if(adjACratio <=  0.42 && adjACratio >   0.38){
                    
                    if (adjACratio >  0.38 && adjACratio <=  0.384) {
                        per_rank2 = 71;
                    }
                    else if (adjACratio >  0.384 && adjACratio <=  0.388) {
                        per_rank2 = 72;
                    }
                    else if (adjACratio >  0.388 && adjACratio <=  0.392) {
                        per_rank2 = 73;
                    }
                    else if (adjACratio >  0.392 && adjACratio <=  0.396) {
                        per_rank2 = 74;
                    }
                    else if (adjACratio >  0.396 && adjACratio <=  0.400) {
                        per_rank2 = 75;
                    }
                    else if (adjACratio >  0.400 && adjACratio <=  0.404) {
                        per_rank2 = 76;
                    }
                    else if (adjACratio >  0.404 && adjACratio <=  0.408) {
                        per_rank2 = 77;
                    }
                    else if (adjACratio >  0.408 && adjACratio <=  0.412) {
                        per_rank2 = 78;
                    }
                    else if (adjACratio >  0.412 && adjACratio <=  0.416) {
                        per_rank2 = 79;
                    }
                    else  {
                        per_rank2 = 80;
                    }
                    
                }
                else if(adjACratio <=  0.38 && adjACratio >   0.34){
                    
                    if (adjACratio >  0.34 && adjACratio <=  0.344) {
                        per_rank2 = 61;
                    }
                    else if (adjACratio >  0.344 && adjACratio <=  0.348) {
                        per_rank2 = 62;
                    }
                    else if (adjACratio >  0.348 && adjACratio <=  0.352) {
                        per_rank2 = 63;
                    }
                    else if (adjACratio >  0.352 && adjACratio <=  0.356) {
                        per_rank2 = 64;
                    }
                    else if (adjACratio >  0.356 && adjACratio <=  0.360) {
                        per_rank2 = 65;
                    }
                    else if (adjACratio >  0.360 && adjACratio <=  0.364) {
                        per_rank2 = 66;
                    }
                    else if (adjACratio >  0.364 && adjACratio <=  0.368) {
                        per_rank2 = 67;
                    }
                    else if (adjACratio >  0.368 && adjACratio <=  0.372) {
                        per_rank2 = 68;
                    }
                    else if (adjACratio >  0.372 && adjACratio <=  0.376) {
                        per_rank2 = 69;
                    }
                    else  {
                        per_rank2 = 70;
                    }
                    
                }
                else if(adjACratio <=  0.340 && adjACratio >   0.300){
                    
                    if (adjACratio >  0.300 && adjACratio <=  0.304) {
                        per_rank2 = 51;
                    }
                    else if (adjACratio >  0.304 && adjACratio <=  0.308) {
                        per_rank2 = 52;
                    }
                    else if (adjACratio >  0.308 && adjACratio <=  0.312) {
                        per_rank2 = 53;
                    }
                    else if (adjACratio >  0.312 && adjACratio <=  0.316) {
                        per_rank2 = 54;
                    }
                    else if (adjACratio >  0.316 && adjACratio <=  0.320) {
                        per_rank2 = 55;
                    }
                    else if (adjACratio >  0.320 && adjACratio <=  0.324) {
                        per_rank2 = 56;
                    }
                    else if (adjACratio >  0.324 && adjACratio <=  0.328) {
                        per_rank2 = 57;
                    }
                    else if (adjACratio >  0.328 && adjACratio <=  0.332) {
                        per_rank2 = 58;
                    }
                    else if (adjACratio >  0.332 && adjACratio <=  0.336) {
                        per_rank2 = 59;
                    }
                    else  {
                        per_rank2 = 60;
                    }

                }
                else if(adjACratio <=  0.30 && adjACratio >   0.26){
                    
                    
                    if (adjACratio >  0.260 && adjACratio <=  0.264) {
                        per_rank2 = 41;
                    }
                    else if (adjACratio >  0.264 && adjACratio <=  0.268) {
                        per_rank2 = 42;
                    }
                    else if (adjACratio >  0.268 && adjACratio <=  0.272) {
                        per_rank2 = 43;
                    }
                    else if (adjACratio >  0.272 && adjACratio <=  0.276) {
                        per_rank2 = 44;
                    }
                    else if (adjACratio >  0.276 && adjACratio <=  0.280) {
                        per_rank2 = 45;
                    }
                    else if (adjACratio >  0.280 && adjACratio <=  0.284) {
                        per_rank2 = 46;
                    }
                    else if (adjACratio >  0.284 && adjACratio <=  0.288) {
                        per_rank2 = 47;
                    }
                    else if (adjACratio >  0.288 && adjACratio <=  0.292) {
                        per_rank2 = 48;
                    }
                    else if (adjACratio >  0.292 && adjACratio <=  0.296) {
                        per_rank2 = 49;
                    }
                    else  {
                        per_rank2 = 50;
                    }
    
                }
                else if(adjACratio <=  0.26  && adjACratio >  0.22){
                    
                    
                    if (adjACratio >  0.220 && adjACratio <=  0.224) {
                        per_rank2 = 31;
                    }
                    else if (adjACratio >  0.224 && adjACratio <=  0.228) {
                        per_rank2 = 32;
                    }
                    else if (adjACratio >  0.228 && adjACratio <=  0.232) {
                        per_rank2 = 33;
                    }
                    else if (adjACratio >  0.232 && adjACratio <=  0.236) {
                        per_rank2 = 34;
                    }
                    else if (adjACratio >  0.236 && adjACratio <=  0.240) {
                        per_rank2 = 35;
                    }
                    else if (adjACratio >  0.240 && adjACratio <=  0.244) {
                        per_rank2 = 36;
                    }
                    else if (adjACratio >  0.244 && adjACratio <=  0.248) {
                        per_rank2 = 37;
                    }
                    else if (adjACratio >  0.248 && adjACratio <=  0.252) {
                        per_rank2 = 38;
                    }
                    else if (adjACratio >  0.252 && adjACratio <=  0.256) {
                        per_rank2 = 39;
                    }
                    else  {
                        per_rank2 = 40;
                    }
                    
                }
                else if(adjACratio <=  0.22  && adjACratio >  0.18){
                    
                    if (adjACratio >  0.18 && adjACratio <=  0.184) {
                        per_rank2 = 21;
                    }
                    else if (adjACratio >  0.184 && adjACratio <=  0.188) {
                        per_rank2 = 22;
                    }
                    else if (adjACratio >  0.188 && adjACratio <=  0.192) {
                        per_rank2 = 23;
                    }
                    else if (adjACratio >  0.192 && adjACratio <=  0.196) {
                        per_rank2 = 24;
                    }
                    else if (adjACratio >  0.196 && adjACratio <=  0.200) {
                        per_rank2 = 25;
                    }
                    else if (adjACratio >  0.200 && adjACratio <=  0.204) {
                        per_rank2 = 26;
                    }
                    else if (adjACratio >  0.204 && adjACratio <=  0.208) {
                        per_rank2 = 27;
                    }
                    else if (adjACratio >  0.208 && adjACratio <=  0.212) {
                        per_rank2 = 28;
                    }
                    else if (adjACratio >  0.212 && adjACratio <=  0.216) {
                        per_rank2 = 29;
                    }
                    else  {
                        per_rank2 = 30;
                    }

                }
                else if(adjACratio <=  0.18  && adjACratio >  0.14){
                    
                    
                    if (adjACratio >  0.14 && adjACratio <=  0.144) {
                        per_rank2 = 11;
                    }
                    else if (adjACratio >  0.144 && adjACratio <=  0.148) {
                        per_rank2 = 12;
                    }
                    else if (adjACratio >  0.148 && adjACratio <=  0.152) {
                        per_rank2 = 13;
                    }
                    else if (adjACratio >  0.152 && adjACratio <=  0.156) {
                        per_rank2 = 14;
                    }
                    else if (adjACratio >  0.156 && adjACratio <=  0.160) {
                        per_rank2 = 15;
                    }
                    else if (adjACratio >  0.160 && adjACratio <=  0.164) {
                        per_rank2 = 16;
                    }
                    else if (adjACratio >  0.164 && adjACratio <=  0.168) {
                        per_rank2 = 17;
                    }
                    else if (adjACratio >  0.168 && adjACratio <=  0.172) {
                        per_rank2 = 18;
                    }
                    else if (adjACratio >  0.172 && adjACratio <=  0.176) {
                        per_rank2 = 19;
                    }
                    else  {
                        per_rank2 = 20;
                    }

                }
                else if(adjACratio <=  0.14  && adjACratio >  0.10){
                    
                    if (adjACratio >  0.100 && adjACratio <=  0.104) {
                        per_rank2 = 1;
                    }
                    else if (adjACratio >  0.104 && adjACratio <=  0.108) {
                        per_rank2 = 2;
                    }
                    else if (adjACratio >  0.108 && adjACratio <=  0.112) {
                        per_rank2 = 3;
                    }
                    else if (adjACratio >  0.112 && adjACratio <=  0.116) {
                        per_rank2 = 4;
                    }
                    else if (adjACratio >  0.116 && adjACratio <=  0.120) {
                        per_rank2 = 5;
                    }
                    else if (adjACratio >  0.120 && adjACratio <=  0.124) {
                        per_rank2 = 6;
                    }
                    else if (adjACratio >  0.124 && adjACratio <=  0.128) {
                        per_rank2 = 7;
                    }
                    else if (adjACratio >  0.128 && adjACratio <=  0.132) {
                        per_rank2 = 8;
                    }
                    else if (adjACratio >  0.132 && adjACratio <=  0.136) {
                        per_rank2 = 9;
                    }
                    else  {
                        per_rank2 = 10;
                    }

                }
                else if(adjACratio <=  0.10){
                    per_rank2 = 0;
                }
                else if(adjACratio >   0.50){
                    per_rank2 = 100;
                }
                if(per_rank2 == 2222){
                    var acRank  =  "--" ;                         
                    document.getElementById("acRank").value = acRank ;  // Rank %
                }
                else{                    
                    var acRank = parseInt(per_rank2);                        
                    document.getElementById("acRank").value = parseInt(acRank) ;  // Rank %
                }
                
            }

            else{
                    per_rank2 = 2222;
                    var acRank  =  "--" ;                         
                    document.getElementById("acRank").value = acRank ;  // Rank %
            }        

        }
      

        
       }
       
        document.getElementById("per_rank2").value = parseInt(per_rank2) ;  // Rank1 %
        avgPerRank() ;
      
    }
    
    
    //get Leteral Pulldown calculations 
    function leteralPulldown() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var letPullKG = document.getElementById("lPullKG").value ;        
        var letPullResp = document.getElementById("lPullRM").value ; 
        
       if(parseInt(letPullKG) > 0 && parseInt(letPullResp) > 0)  
       {
         var letPullrm = parseInt(letPullKG)*2.2/(1.0278-(parseInt(letPullResp)*0.0278)) ;
         var letPullRMkg =  parseInt(letPullrm)/2.2 ;
         document.getElementById("lPullPred").value = Math.floor(parseFloat(letPullRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var LetPullratio  =  parseFloat(letPullRMkg)/parseInt(weight);
        var adjLetPullratio  =  ageCorrection * parseFloat(LetPullratio);      
        //alert(adjBPratio); die;
        document.getElementById("lPullSW").value = Math.floor(parseFloat(adjLetPullratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank3 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
       if(adjLetPullratio > 0){
                
                if(adjLetPullratio <=  1.35 && adjLetPullratio >   1.25){
                    
                    
                    if (adjLetPullratio >  1.25 && adjLetPullratio <=   1.26) {
                        per_rank3 = 91;
                    }
                    else if (adjLetPullratio >   1.26 && adjLetPullratio <=   1.27) {
                        per_rank3 = 92;
                    }
                    else if (adjLetPullratio >   1.27 && adjLetPullratio <=   1.28) {
                        per_rank3 = 93;
                    }
                    else if (adjLetPullratio >   1.28 && adjLetPullratio <=   1.29) {
                        per_rank3 = 94;
                    }
                    else if (adjLetPullratio >   1.29 && adjLetPullratio <=   1.30) {
                        per_rank3 = 95;
                    }
                    else if (adjLetPullratio >   1.30 && adjLetPullratio <=   1.31) {
                        per_rank3 = 96;
                    }
                    else if (adjLetPullratio >   1.31 && adjLetPullratio <=   1.32) {
                        per_rank3 = 97;
                    }
                    else if (adjLetPullratio >   1.32 && adjLetPullratio <=   1.33) {
                        per_rank3 = 98;
                    }
                    else if (adjLetPullratio >   1.33 && adjLetPullratio <=   1.34) {
                        per_rank3 = 99;
                    }
                    else {
                        per_rank3 = 100;
                    }
                    
                }
                else if(adjLetPullratio <=  1.25  && adjLetPullratio >   1.20){
                    
                    
                    if (adjLetPullratio >  1.20 && adjLetPullratio <=   1.205) {
                        per_rank3 = 81;
                    }
                    else if (adjLetPullratio >   1.205 && adjLetPullratio <=   1.210) {
                        per_rank3 = 82;
                    }
                    else if (adjLetPullratio >   1.210 && adjLetPullratio <=   1.215) {
                        per_rank3 = 83;
                    }
                    else if (adjLetPullratio >   1.215 && adjLetPullratio <=   1.220) {
                        per_rank3 = 84;
                    }
                    else if (adjLetPullratio >   1.220 && adjLetPullratio <=   1.225) {
                        per_rank3 = 85;
                    }
                    else if (adjLetPullratio >   1.225 && adjLetPullratio <=   1.230) {
                        per_rank3 = 86;
                    }
                    else if (adjLetPullratio >   1.230 && adjLetPullratio <=   1.235) {
                        per_rank3 = 87;
                    }
                    else if (adjLetPullratio >   1.235 && adjLetPullratio <=   1.240) {
                        per_rank3 = 88;
                    }
                    else if (adjLetPullratio >   1.240 && adjLetPullratio <=   1.245) {
                        per_rank3 = 89;
                    }
                    else {
                        per_rank3 = 90;
                    }
                    
                }
                else if(adjLetPullratio <=  1.20 && adjLetPullratio >   1.15){
                    
                    
                    if (adjLetPullratio >  1.150 && adjLetPullratio <=   1.155) {
                        per_rank3 = 71;
                    }
                    else if (adjLetPullratio >   1.155 && adjLetPullratio <=   1.160) {
                        per_rank3 = 72;
                    }
                    else if (adjLetPullratio >   1.160 && adjLetPullratio <=   1.165) {
                        per_rank3 = 73;
                    }
                    else if (adjLetPullratio >   1.165 && adjLetPullratio <=   1.170) {
                        per_rank3 = 74;
                    }
                    else if (adjLetPullratio >   1.170 && adjLetPullratio <=   1.175) {
                        per_rank3 = 75;
                    }
                    else if (adjLetPullratio >   1.175 && adjLetPullratio <=   1.180) {
                        per_rank3 = 76;
                    }
                    else if (adjLetPullratio >   1.180 && adjLetPullratio <=   1.185) {
                        per_rank3 = 77;
                    }
                    else if (adjLetPullratio >   1.185 && adjLetPullratio <=   1.190) {
                        per_rank3 = 78;
                    }
                    else if (adjLetPullratio >   1.190 && adjLetPullratio <=   1.195) {
                        per_rank3 = 79;
                    }
                    else {
                        per_rank3 = 80;
                    }
                }
                else if(adjLetPullratio <=  1.15 && adjLetPullratio >   1.10){
                    
                    
                    if (adjLetPullratio >  1.10 && adjLetPullratio <=   1.105) {
                        per_rank3 = 61;
                    }
                    else if (adjLetPullratio >   1.105 && adjLetPullratio <=   1.110) {
                        per_rank3 = 62;
                    }
                    else if (adjLetPullratio >   1.110 && adjLetPullratio <=   1.115) {
                        per_rank3 = 63;
                    }
                    else if (adjLetPullratio >   1.115 && adjLetPullratio <=   1.120) {
                        per_rank3 = 64;
                    }
                    else if (adjLetPullratio >   1.120 && adjLetPullratio <=   1.125) {
                        per_rank3 = 65;
                    }
                    else if (adjLetPullratio >   1.125 && adjLetPullratio <=   1.130) {
                        per_rank3 = 66;
                    }
                    else if (adjLetPullratio >   1.130 && adjLetPullratio <=   1.135) {
                        per_rank3 = 67;
                    }
                    else if (adjLetPullratio >   1.135 && adjLetPullratio <=   1.140) {
                        per_rank3 = 68;
                    }
                    else if (adjLetPullratio >   1.140 && adjLetPullratio <=   1.145) {
                        per_rank3 = 69;
                    }
                    else {
                        per_rank3 = 70;
                    }
                }
                else if(adjLetPullratio <=  1.10 && adjLetPullratio >   1.05){
                    
                    
                    if (adjLetPullratio >  1.05 && adjLetPullratio <=   1.010) {
                        per_rank3 = 51;
                    }
                    else if (adjLetPullratio >   1.010 && adjLetPullratio <=   1.020) {
                        per_rank3 = 52;
                    }
                    else if (adjLetPullratio >   1.020 && adjLetPullratio <=   1.030) {
                        per_rank3 = 53;
                    }
                    else if (adjLetPullratio >   1.030 && adjLetPullratio <=   1.040) {
                        per_rank3 = 54;
                    }
                    else if (adjLetPullratio >   1.040 && adjLetPullratio <=   1.050) {
                        per_rank3 = 55;
                    }
                    else if (adjLetPullratio >   1.050 && adjLetPullratio <=   1.060) {
                        per_rank3 = 56;
                    }
                    else if (adjLetPullratio >   1.060 && adjLetPullratio <=   1.070) {
                        per_rank3 = 57;
                    }
                    else if (adjLetPullratio >   1.070 && adjLetPullratio <=   1.080) {
                        per_rank3 = 58;
                    }
                    else if (adjLetPullratio >   1.080 && adjLetPullratio <=   1.090) {
                        per_rank3 = 59;
                    }
                    else {
                        per_rank3 = 60;
                    }
                }
                else if(adjLetPullratio <=  1.050  && adjLetPullratio >  1.000){
                    
                    if (adjLetPullratio >  1.00 && adjLetPullratio <=   1.005) {
                        per_rank3 = 41;
                    }
                    else if (adjLetPullratio >   1.005 && adjLetPullratio <=   1.010) {
                        per_rank3 = 42;
                    }
                    else if (adjLetPullratio >   1.010 && adjLetPullratio <=   1.015) {
                        per_rank3 = 43;
                    }
                    else if (adjLetPullratio >   1.015 && adjLetPullratio <=   1.020) {
                        per_rank3 = 44;
                    }
                    else if (adjLetPullratio >   1.020 && adjLetPullratio <=   1.025) {
                        per_rank3 = 45;
                    }
                    else if (adjLetPullratio >   1.025 && adjLetPullratio <=   1.030) {
                        per_rank3 = 46;
                    }
                    else if (adjLetPullratio >   1.030 && adjLetPullratio <=   1.035) {
                        per_rank3 = 47;
                    }
                    else if (adjLetPullratio >   1.035 && adjLetPullratio <=   1.040) {
                        per_rank3 = 48;
                    }
                    else if (adjLetPullratio >   1.040 && adjLetPullratio <=   1.045) {
                        per_rank3 = 49;
                    }
                    else {
                        per_rank3 = 50;
                    }
                    
                }
                else if(adjLetPullratio <=  1.000  && adjLetPullratio >  0.950){
                    
                    if (adjLetPullratio >  0.950 && adjLetPullratio <=   0.955) {
                        per_rank3 = 31;
                    }
                    else if (adjLetPullratio >   0.955 && adjLetPullratio <=   0.960) {
                        per_rank3 = 32;
                    }
                    else if (adjLetPullratio >   0.960 && adjLetPullratio <=   0.965) {
                        per_rank3 = 33;
                    }
                    else if (adjLetPullratio >   0.965 && adjLetPullratio <=   0.970) {
                        per_rank3 = 34;
                    }
                    else if (adjLetPullratio >   0.970 && adjLetPullratio <=   0.975) {
                        per_rank3 = 35;
                    }
                    else if (adjLetPullratio >   0.975 && adjLetPullratio <=   0.980) {
                        per_rank3 = 36;
                    }
                    else if (adjLetPullratio >   0.980 && adjLetPullratio <=   0.985) {
                        per_rank3 = 37;
                    }
                    else if (adjLetPullratio >   0.985 && adjLetPullratio <=   0.990) {
                        per_rank3 = 38;
                    }
                    else if (adjLetPullratio >   0.990 && adjLetPullratio <=   0.995) {
                        per_rank3 = 39;
                    }
                    else {
                        per_rank3 = 40;
                    }
                }
                else if(adjLetPullratio <=  0.950  && adjLetPullratio >  0.900){
                    
                    if (adjLetPullratio >  0.900 && adjLetPullratio <=   0.905) {
                        per_rank3 = 21;
                    }
                    else if (adjLetPullratio >   0.905 && adjLetPullratio <=   0.910) {
                        per_rank3 = 22;
                    }
                    else if (adjLetPullratio >   0.910 && adjLetPullratio <=   0.915) {
                        per_rank3 = 23;
                    }
                    else if (adjLetPullratio >   0.915 && adjLetPullratio <=   0.920) {
                        per_rank3 = 24;
                    }
                    else if (adjLetPullratio >   0.920 && adjLetPullratio <=   0.925) {
                        per_rank3 = 25;
                    }
                    else if (adjLetPullratio >   0.925 && adjLetPullratio <=   0.930) {
                        per_rank3 = 26;
                    }
                    else if (adjLetPullratio >   0.930 && adjLetPullratio <=   0.935) {
                        per_rank3 = 27;
                    }
                    else if (adjLetPullratio >   0.935 && adjLetPullratio <=   0.940) {
                        per_rank3 = 28;
                    }
                    else if (adjLetPullratio >   0.940 && adjLetPullratio <=   0.945) {
                        per_rank3 = 29;
                    }
                    else {
                        per_rank3 = 30;
                    }
                    
                }
                else if(adjLetPullratio <=  0.900  && adjLetPullratio >  0.750){
                    
                    if (adjLetPullratio >  0.750 && adjLetPullratio <=   0.765) {
                        per_rank3 = 11;
                    }
                    else if (adjLetPullratio >   0.765 && adjLetPullratio <=   0.780) {
                        per_rank3 = 12;
                    }
                    else if (adjLetPullratio >   0.780 && adjLetPullratio <=   0.795) {
                        per_rank3 = 13;
                    }
                    else if (adjLetPullratio >   0.795 && adjLetPullratio <=   0.810) {
                        per_rank3 = 14;
                    }
                    else if (adjLetPullratio >   0.810 && adjLetPullratio <=   0.825) {
                        per_rank3 = 15;
                    }
                    else if (adjLetPullratio >   0.825 && adjLetPullratio <=   0.840) {
                        per_rank3 = 16;
                    }
                    else if (adjLetPullratio >   0.840 && adjLetPullratio <=   0.855) {
                        per_rank3 = 17;
                    }
                    else if (adjLetPullratio >   0.855 && adjLetPullratio <=   0.970) {
                        per_rank3 = 18;
                    }
                    else if (adjLetPullratio >   0.970 && adjLetPullratio <=   0.985) {
                        per_rank3 = 19;
                    }
                    else {
                        per_rank3 = 20;
                    }
                    
                }
                else if(adjLetPullratio <=  0.75  && adjLetPullratio >  0.65){
                    
                    if (adjLetPullratio >  0.65 && adjLetPullratio <=   0.66) {
                        per_rank3 = 1;
                    }
                    else if (adjLetPullratio >   0.66 && adjLetPullratio <=   0.67) {
                        per_rank3 = 2;
                    }
                    else if (adjLetPullratio >   0.67 && adjLetPullratio <=   0.68) {
                        per_rank3 = 3;
                    }
                    else if (adjLetPullratio >   0.68 && adjLetPullratio <=   0.69) {
                        per_rank3 = 4;
                    }
                    else if (adjLetPullratio >   0.69 && adjLetPullratio <=   0.70) {
                        per_rank3 = 5;
                    }
                    else if (adjLetPullratio >   0.70 && adjLetPullratio <=   0.71) {
                        per_rank3 = 6;
                    }
                    else if (adjLetPullratio >   0.71 && adjLetPullratio <=   0.72) {
                        per_rank3 = 7;
                    }
                    else if (adjLetPullratio >   0.72 && adjLetPullratio <=   0.73) {
                        per_rank3 = 8;
                    }
                    else if (adjLetPullratio >   0.73 && adjLetPullratio <=   0.74) {
                        per_rank3 = 9;
                    }
                    else {
                        per_rank3 = 10;
                    }
                }
                
                else if(adjLetPullratio >  1.35){
                    per_rank3 = 100;
                }
                else if(adjLetPullratio <=  0.65){
                    per_rank3 = 0;
                }
                
                 if(per_rank3 == 2222){
                    var letPullRank  =  "--" ;                         
                    document.getElementById("lPullRank").value = letPullRank ;  // Rank %
                }
                else{                    
                    var letPullRank = parseInt(per_rank3);                        
                    document.getElementById("lPullRank").value = parseInt(letPullRank) ;  // Rank %
                }
            }
             else{
                    per_rank3 = 2222;
                    var letPullRank  =  "--" ;                         
                    document.getElementById("lPullRank").value = letPullRank ;  // Rank %
            } 
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

          if(adjLetPullratio > 0){
                
                if(adjLetPullratio <=  0.85 && adjLetPullratio >   0.80){
                    
                    if (adjLetPullratio >  0.800 && adjLetPullratio <=   0.805) {
                        per_rank3 = 91;
                    }
                    else if (adjLetPullratio >   0.805 && adjLetPullratio <=   0.810) {
                        per_rank3 = 92;
                    }
                    else if (adjLetPullratio >   0.810 && adjLetPullratio <=   0.815) {
                        per_rank3 = 93;
                    }
                    else if (adjLetPullratio >   0.815 && adjLetPullratio <=   0.820) {
                        per_rank3 = 94;
                    }
                    else if (adjLetPullratio >   0.820 && adjLetPullratio <=   0.825) {
                        per_rank3 = 95;
                    }
                    else if (adjLetPullratio >   0.825 && adjLetPullratio <=   0.830) {
                        per_rank3 = 96;
                    }
                    else if (adjLetPullratio >   0.830 && adjLetPullratio <=   0.835) {
                        per_rank3 = 97;
                    }
                    else if (adjLetPullratio >   0.835 && adjLetPullratio <=   0.840) {
                        per_rank3 = 98;
                    }
                    else if (adjLetPullratio >   0.840 && adjLetPullratio <=   0.845) {
                        per_rank3 = 99;
                    }
                    else {
                        per_rank3 = 100;
                    }
                }
                else if(adjLetPullratio <=  0.80  && adjLetPullratio >   0.75){
                    
                    if (adjLetPullratio >  0.750 && adjLetPullratio <=   0.755) {
                        per_rank3 = 81;
                    }
                    else if (adjLetPullratio >   0.755 && adjLetPullratio <=   0.760) {
                        per_rank3 = 82;
                    }
                    else if (adjLetPullratio >   0.760 && adjLetPullratio <=   0.765) {
                        per_rank3 = 83;
                    }
                    else if (adjLetPullratio >   0.765 && adjLetPullratio <=   0.770) {
                        per_rank3 = 84;
                    }
                    else if (adjLetPullratio >   0.770 && adjLetPullratio <=   0.775) {
                        per_rank3 = 85;
                    }
                    else if (adjLetPullratio >   0.775 && adjLetPullratio <=   0.780) {
                        per_rank3 = 86;
                    }
                    else if (adjLetPullratio >   0.780 && adjLetPullratio <=   0.785) {
                        per_rank3 = 87;
                    }
                    else if (adjLetPullratio >   0.785 && adjLetPullratio <=   0.790) {
                        per_rank3 = 88;
                    }
                    else if (adjLetPullratio >   0.790 && adjLetPullratio <=   0.795) {
                        per_rank3 = 89;
                    }
                    else {
                        per_rank3 = 90;
                    }
                }
                else if(adjLetPullratio <=  0.75 && adjLetPullratio >   0.70){
                    
                    
                    if (adjLetPullratio >  0.700 && adjLetPullratio <=   0.705) {
                        per_rank3 = 71;
                    }
                    else if (adjLetPullratio >   0.705 && adjLetPullratio <=   0.710) {
                        per_rank3 = 72;
                    }
                    else if (adjLetPullratio >   0.710 && adjLetPullratio <=   0.715) {
                        per_rank3 = 73;
                    }
                    else if (adjLetPullratio >   0.715 && adjLetPullratio <=   0.720) {
                        per_rank3 = 74;
                    }
                    else if (adjLetPullratio >   0.720 && adjLetPullratio <=   0.725) {
                        per_rank3 = 75;
                    }
                    else if (adjLetPullratio >   0.725 && adjLetPullratio <=   0.730) {
                        per_rank3 = 76;
                    }
                    else if (adjLetPullratio >   0.730 && adjLetPullratio <=   0.735) {
                        per_rank3 = 77;
                    }
                    else if (adjLetPullratio >   0.735 && adjLetPullratio <=   0.740) {
                        per_rank3 = 78;
                    }
                    else if (adjLetPullratio >   0.740 && adjLetPullratio <=   0.745) {
                        per_rank3 = 79;
                    }
                    else {
                        per_rank3 = 80;
                    }

                }
                else if(adjLetPullratio <=  0.70 && adjLetPullratio >   0.65){
                    
                    
                    if (adjLetPullratio >  0.650 && adjLetPullratio <=   0.655) {
                        per_rank3 = 61;
                    }
                    else if (adjLetPullratio >   0.655 && adjLetPullratio <=   0.660) {
                        per_rank3 = 62;
                    }
                    else if (adjLetPullratio >   0.660 && adjLetPullratio <=   0.665) {
                        per_rank3 = 63;
                    }
                    else if (adjLetPullratio >   0.665 && adjLetPullratio <=   0.670) {
                        per_rank3 = 64;
                    }
                    else if (adjLetPullratio >   0.670 && adjLetPullratio <=   0.675) {
                        per_rank3 = 65;
                    }
                    else if (adjLetPullratio >   0.675 && adjLetPullratio <=   0.680) {
                        per_rank3 = 66;
                    }
                    else if (adjLetPullratio >   0.680 && adjLetPullratio <=   0.685) {
                        per_rank3 = 67;
                    }
                    else if (adjLetPullratio >   0.685 && adjLetPullratio <=   0.690) {
                        per_rank3 = 68;
                    }
                    else if (adjLetPullratio >   0.690 && adjLetPullratio <=   0.695) {
                        per_rank3 = 69;
                    }
                    else {
                        per_rank3 = 70;
                    }
                }
                else if(adjLetPullratio <=  0.65 && adjLetPullratio >   0.60){
                    
                    if (adjLetPullratio >  0.600 && adjLetPullratio <=   0.605) {
                        per_rank3 = 51;
                    }
                    else if (adjLetPullratio >   0.605 && adjLetPullratio <=   0.610) {
                        per_rank3 = 52;
                    }
                    else if (adjLetPullratio >   0.610 && adjLetPullratio <=   0.615) {
                        per_rank3 = 53;
                    }
                    else if (adjLetPullratio >   0.615 && adjLetPullratio <=   0.620) {
                        per_rank3 = 54;
                    }
                    else if (adjLetPullratio >   0.620 && adjLetPullratio <=   0.625) {
                        per_rank3 = 55;
                    }
                    else if (adjLetPullratio >   0.625 && adjLetPullratio <=   0.630) {
                        per_rank3 = 56;
                    }
                    else if (adjLetPullratio >   0.630 && adjLetPullratio <=   0.635) {
                        per_rank3 = 57;
                    }
                    else if (adjLetPullratio >   0.635 && adjLetPullratio <=   0.640) {
                        per_rank3 = 58;
                    }
                    else if (adjLetPullratio >   0.640 && adjLetPullratio <=   0.645) {
                        per_rank3 = 59;
                    }
                    else {
                        per_rank3 = 60;
                    }
                }
                else if(adjLetPullratio <=  0.60  && adjLetPullratio >  0.55){
                    
                    if (adjLetPullratio >  0.550 && adjLetPullratio <=   0.555) {
                        per_rank3 = 41;
                    }
                    else if (adjLetPullratio >   0.555 && adjLetPullratio <=   0.560) {
                        per_rank3 = 42;
                    }
                    else if (adjLetPullratio >   0.560 && adjLetPullratio <=   0.565) {
                        per_rank3 = 43;
                    }
                    else if (adjLetPullratio >   0.565 && adjLetPullratio <=   0.570) {
                        per_rank3 = 44;
                    }
                    else if (adjLetPullratio >   0.570 && adjLetPullratio <=   0.575) {
                        per_rank3 = 45;
                    }
                    else if (adjLetPullratio >   0.575 && adjLetPullratio <=   0.580) {
                        per_rank3 = 46;
                    }
                    else if (adjLetPullratio >   0.580 && adjLetPullratio <=   0.585) {
                        per_rank3 = 47;
                    }
                    else if (adjLetPullratio >   0.585 && adjLetPullratio <=   0.590) {
                        per_rank3 = 48;
                    }
                    else if (adjLetPullratio >   0.590 && adjLetPullratio <=   0.595) {
                        per_rank3 = 49;
                    }
                    else {
                        per_rank3 = 50;
                    }
                }
                else if(adjLetPullratio <=  0.55  && adjLetPullratio >  0.50){
                    
                    if (adjLetPullratio >  0.500 && adjLetPullratio <=   0.505) {
                        per_rank3 = 31;
                    }
                    else if (adjLetPullratio >   0.505 && adjLetPullratio <=   0.510) {
                        per_rank3 = 32;
                    }
                    else if (adjLetPullratio >   0.510 && adjLetPullratio <=   0.515) {
                        per_rank3 = 33;
                    }
                    else if (adjLetPullratio >   0.515 && adjLetPullratio <=   0.520) {
                        per_rank3 = 34;
                    }
                    else if (adjLetPullratio >   0.520 && adjLetPullratio <=   0.525) {
                        per_rank3 = 35;
                    }
                    else if (adjLetPullratio >   0.525 && adjLetPullratio <=   0.530) {
                        per_rank3 = 36;
                    }
                    else if (adjLetPullratio >   0.530 && adjLetPullratio <=   0.535) {
                        per_rank3 = 37;
                    }
                    else if (adjLetPullratio >   0.535 && adjLetPullratio <=   0.540) {
                        per_rank3 = 38;
                    }
                    else if (adjLetPullratio >   0.540 && adjLetPullratio <=   0.545) {
                        per_rank3 = 39;
                    }
                    else {
                        per_rank3 = 40;
                    }

                }
                else if(adjLetPullratio <=  0.50  && adjLetPullratio >  0.45){
                    
                    if (adjLetPullratio >  0.450 && adjLetPullratio <=   0.455) {
                        per_rank3 = 21;
                    }
                    else if (adjLetPullratio >   0.455 && adjLetPullratio <=   0.460) {
                        per_rank3 = 22;
                    }
                    else if (adjLetPullratio >   0.460 && adjLetPullratio <=   0.465) {
                        per_rank3 = 23;
                    }
                    else if (adjLetPullratio >   0.465 && adjLetPullratio <=   0.470) {
                        per_rank3 = 24;
                    }
                    else if (adjLetPullratio >   0.470 && adjLetPullratio <=   0.475) {
                        per_rank3 = 25;
                    }
                    else if (adjLetPullratio >   0.475 && adjLetPullratio <=   0.480) {
                        per_rank3 = 26;
                    }
                    else if (adjLetPullratio >   0.480 && adjLetPullratio <=   0.485) {
                        per_rank3 = 27;
                    }
                    else if (adjLetPullratio >   0.485 && adjLetPullratio <=   0.490) {
                        per_rank3 = 28;
                    }
                    else if (adjLetPullratio >   0.490 && adjLetPullratio <=   0.495) {
                        per_rank3 = 29;
                    }
                    else {
                        per_rank3 = 30;
                    }
                }
                else if(adjLetPullratio <=  0.45  && adjLetPullratio > 0.40){
                    
                    if (adjLetPullratio >  0.400 && adjLetPullratio <=   0.405) {
                        per_rank3 = 11;
                    }
                    else if (adjLetPullratio >   0.405 && adjLetPullratio <=   0.410) {
                        per_rank3 = 12;
                    }
                    else if (adjLetPullratio >   0.410 && adjLetPullratio <=   0.415) {
                        per_rank3 = 13;
                    }
                    else if (adjLetPullratio >   0.415 && adjLetPullratio <=   0.420) {
                        per_rank3 = 14;
                    }
                    else if (adjLetPullratio >   0.420 && adjLetPullratio <=   0.425) {
                        per_rank3 = 15;
                    }
                    else if (adjLetPullratio >   0.425 && adjLetPullratio <=   0.430) {
                        per_rank3 = 16;
                    }
                    else if (adjLetPullratio >   0.430 && adjLetPullratio <=   0.435) {
                        per_rank3 = 17;
                    }
                    else if (adjLetPullratio >   0.435 && adjLetPullratio <=   0.440) {
                        per_rank3 = 18;
                    }
                    else if (adjLetPullratio >   0.440 && adjLetPullratio <=   0.445) {
                        per_rank3 = 19;
                    }
                    else {
                        per_rank3 = 20;
                    }

                }
                else if(adjLetPullratio <=  0.40  && adjLetPullratio >  0.35){
                    
                    if (adjLetPullratio >  0.350 && adjLetPullratio <=   0.355) {
                        per_rank3 = 1;
                    }
                    else if (adjLetPullratio >   0.355 && adjLetPullratio <=   0.360) {
                        per_rank3 = 2;
                    }
                    else if (adjLetPullratio >   0.360 && adjLetPullratio <=   0.365) {
                        per_rank3 = 3;
                    }
                    else if (adjLetPullratio >   0.365 && adjLetPullratio <=   0.370) {
                        per_rank3 = 4;
                    }
                    else if (adjLetPullratio >   0.370 && adjLetPullratio <=   0.375) {
                        per_rank3 = 5;
                    }
                    else if (adjLetPullratio >   0.375 && adjLetPullratio <=   0.380) {
                        per_rank3 = 6;
                    }
                    else if (adjLetPullratio >   0.380 && adjLetPullratio <=   0.385) {
                        per_rank3 = 7;
                    }
                    else if (adjLetPullratio >   0.385 && adjLetPullratio <=   0.390) {
                        per_rank3 = 8;
                    }
                    else if (adjLetPullratio >   0.390 && adjLetPullratio <=   0.395) {
                        per_rank3 = 9;
                    }
                    else {
                        per_rank3 = 10;
                    }
                }
                else if(adjLetPullratio >  0.85){
                    per_rank3 = 100;
                }
                else if(adjLetPullratio <=  0.35){
                    per_rank3 = 0;
                }
                
                if(per_rank3 == 2222){
                    var letPullRank  =  "--" ;                         
                    document.getElementById("lPullRank").value = letPullRank ;  // Rank %
                }
                else{                    
                    var letPullRank = parseInt(per_rank3);                        
                    document.getElementById("lPullRank").value = parseInt(letPullRank) ;  // Rank %
                }
            }
             else{
                    per_rank3 = 2222;
                    var letPullRank  =  "--" ;                         
                    document.getElementById("lPullRank").value = letPullRank ;  // Rank %
            }            

        }
      

        
       }
       
        document.getElementById("per_rank3").value = parseInt(per_rank3) ;  // Rank3 %
        avgPerRank() ;
      
    }
    
    
  //get Leg Press calculations 
    function legPress() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var lpKG = document.getElementById("lPressKG").value ;        
        var lpResp = document.getElementById("lPressRM").value ; 
        
       if(parseInt(lpKG) > 0 && parseInt(lpResp) > 0)  
       {
         var lprm = parseInt(lpKG)*2.2/(1.0278-(parseInt(lpResp)*0.0278)) ;
         var lpRMkg =  parseInt(lprm)/2.2 ;
         document.getElementById("lPressPred").value = Math.floor(parseFloat(lpRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var LPratio  =  parseFloat(lpRMkg)/parseInt(weight);
        var adjLPratio  =  ageCorrection * parseFloat(LPratio);      
        //alert(adjBPratio); die;
        document.getElementById("lPressSW").value = Math.floor(parseFloat(adjLPratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank4 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
       if(adjLPratio > 0){
                
                if(adjLPratio <=  3.50 && adjLPratio >   3.20){
                    
                    if (adjLPratio >  3.20 && adjLPratio <=  3.23) {
                        per_rank4 = 91;
                    }
                    else if (adjLPratio >  3.23 && adjLPratio <=  3.26) {
                        per_rank4 = 92;
                    }
                    else if (adjLPratio >  3.26 && adjLPratio <=  3.29) {
                        per_rank4 = 93;
                    }
                    else if (adjLPratio >  3.29 && adjLPratio <=  3.32) {
                        per_rank4 = 94;
                    }
                    else if (adjLPratio >  3.32 && adjLPratio <=  3.35) {
                        per_rank4 = 95;
                    }
                    else if (adjLPratio >  3.35 && adjLPratio <=  3.38) {
                        per_rank4 = 96;
                    }
                    else if (adjLPratio >  3.38 && adjLPratio <=  3.41) {
                        per_rank4 = 97;
                    }
                    else if (adjLPratio >  3.41 && adjLPratio <=  3.44) {
                        per_rank4 = 98;
                    }
                    else if (adjLPratio >  3.44 && adjLPratio <=  3.47) {
                        per_rank4 = 99;
                    }
                    else  {
                        per_rank4 = 100;
                    }
                }
                else if(adjLPratio <=  3.20  && adjLPratio >   2.90){
                    
                    if (adjLPratio >  2.90 && adjLPratio <=  2.93) {
                        per_rank4 = 81;
                    }
                    else if (adjLPratio >  2.93 && adjLPratio <=  2.96) {
                        per_rank4 = 82;
                    }
                    else if (adjLPratio >  2.96 && adjLPratio <=  2.99) {
                        per_rank4 = 83;
                    }
                    else if (adjLPratio >  2.99 && adjLPratio <=  3.02) {
                        per_rank4 = 84;
                    }
                    else if (adjLPratio >  3.02 && adjLPratio <=  3.05) {
                        per_rank4 = 85;
                    }
                    else if (adjLPratio >  3.05 && adjLPratio <=  3.08) {
                        per_rank4 = 86;
                    }
                    else if (adjLPratio >  3.08 && adjLPratio <=  3.11) {
                        per_rank4 = 87;
                    }
                    else if (adjLPratio >  3.11 && adjLPratio <=  3.14) {
                        per_rank4 = 88;
                    }
                    else if (adjLPratio >  3.14 && adjLPratio <=  3.17) {
                        per_rank4 = 89;
                    }
                    else if (adjLPratio >  3.17 && adjLPratio <=  3.20) {
                        per_rank4 = 90;
                    }
                    else  {
                        per_rank4 = 100;
                    }
                }
                else if(adjLPratio <=  2.90 && adjLPratio >   2.60){
                    
                    if (adjLPratio >  2.60 && adjLPratio <=  2.63) {
                        per_rank4 = 71;
                    }
                    else if (adjLPratio >  2.63 && adjLPratio <=  2.66) {
                        per_rank4 = 72;
                    }
                    else if (adjLPratio >  2.66 && adjLPratio <=  2.69) {
                        per_rank4 = 73;
                    }
                    else if (adjLPratio >  2.69 && adjLPratio <=  2.72) {
                        per_rank4 = 74;
                    }
                    else if (adjLPratio >  2.72 && adjLPratio <=  2.75) {
                        per_rank4 = 75;
                    }
                    else if (adjLPratio >  2.75 && adjLPratio <=  2.78) {
                        per_rank4 = 76;
                    }
                    else if (adjLPratio >  2.78 && adjLPratio <=  2.81) {
                        per_rank4 = 77;
                    }
                    else if (adjLPratio >  2.81 && adjLPratio <=  2.84) {
                        per_rank4 = 78;
                    }
                    else if (adjLPratio >  2.84 && adjLPratio <=  2.87) {
                        per_rank4 = 79;
                    }
                    else if (adjLPratio >  2.87 && adjLPratio <=  2.90) {
                        per_rank4 = 80;
                    }
                    else  {
                        per_rank4 = 80;
                    }
                }
                else if(adjLPratio <=  2.60 && adjLPratio >   2.40){
                    
                    if (adjLPratio >  2.40 && adjLPratio <=  2.42) {
                        per_rank4 = 61;
                    }
                    else if (adjLPratio >  2.42 && adjLPratio <=  2.44) {
                        per_rank4 = 62;
                    }
                    else if (adjLPratio >  2.44 && adjLPratio <=  2.46) {
                        per_rank4 = 63;
                    }
                    else if (adjLPratio >  2.46 && adjLPratio <=  2.48) {
                        per_rank4 = 64;
                    }
                    else if (adjLPratio >  2.48 && adjLPratio <=  2.50) {
                        per_rank4 = 65;
                    }
                    else if (adjLPratio >  2.50 && adjLPratio <=  2.52) {
                        per_rank4 = 66;
                    }
                    else if (adjLPratio >  2.52 && adjLPratio <=  2.54) {
                        per_rank4 = 67;
                    }
                    else if (adjLPratio >  2.54 && adjLPratio <=  2.56) {
                        per_rank4 = 68;
                    }
                    else if (adjLPratio >  2.56 && adjLPratio <=  2.58) {
                        per_rank4 = 69;
                    }
                    else if (adjLPratio >  2.58 && adjLPratio <=  2.60) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjLPratio <=  2.40 && adjLPratio >   2.20){
                    
                    if (adjLPratio >  2.20 && adjLPratio <=  2.22) {
                        per_rank4 = 51;
                    }
                    else if (adjLPratio >  2.22 && adjLPratio <=  2.24) {
                        per_rank4 = 52;
                    }
                    else if (adjLPratio >  2.24 && adjLPratio <=  2.26) {
                        per_rank4 = 53;
                    }
                    else if (adjLPratio >  2.26 && adjLPratio <=  2.28) {
                        per_rank4 = 54;
                    }
                    else if (adjLPratio >  2.28 && adjLPratio <=  2.30) {
                        per_rank4 = 55;
                    }
                    else if (adjLPratio >  2.30 && adjLPratio <=  2.32) {
                        per_rank4 = 56;
                    }
                    else if (adjLPratio >  2.32 && adjLPratio <=  2.34) {
                        per_rank4 = 57;
                    }
                    else if (adjLPratio >  2.34 && adjLPratio <=  2.36) {
                        per_rank4 = 58;
                    }
                    else if (adjLPratio >  2.36 && adjLPratio <=  2.38) {
                        per_rank4 = 59;
                    }
                    else if (adjLPratio >  2.38 && adjLPratio <=  2.30) {
                        per_rank4 = 60;
                    }
                    else  {
                        per_rank4 = 60;
                    }
                }
                else if(adjLPratio <=  2.20 && adjLPratio >   2.00){
                    
                    if (adjLPratio >  2.00 && adjLPratio <=  2.02) {
                        per_rank4 = 41;
                    }
                    else if (adjLPratio >  2.02 && adjLPratio <=  2.04) {
                        per_rank4 = 42;
                    }
                    else if (adjLPratio >  2.04 && adjLPratio <=  2.06) {
                        per_rank4 = 43;
                    }
                    else if (adjLPratio >  2.06 && adjLPratio <=  2.08) {
                        per_rank4 = 44;
                    }
                    else if (adjLPratio >  2.08 && adjLPratio <=  2.00) {
                        per_rank4 = 45;
                    }
                    else if (adjLPratio >  2.00 && adjLPratio <=  2.12) {
                        per_rank4 = 46;
                    }
                    else if (adjLPratio >  2.12 && adjLPratio <=  2.14) {
                        per_rank4 = 47;
                    }
                    else if (adjLPratio >  2.14 && adjLPratio <=  2.16) {
                        per_rank4 = 48;
                    }
                    else if (adjLPratio >  2.16 && adjLPratio <=  2.18) {
                        per_rank4 = 49;
                    }
                    else if (adjLPratio >  2.18 && adjLPratio <=  2.10) {
                        per_rank4 = 50;
                    }
                    else  {
                        per_rank4 = 50;
                    }
                }
                else if(adjLPratio <=  2.00  && adjLPratio >  1.80){
                    
                    if (adjLPratio >  1.80 && adjLPratio <=  1.82) {
                        per_rank4 = 31;
                    }
                    else if (adjLPratio >  1.82 && adjLPratio <=  1.84) {
                        per_rank4 = 32;
                    }
                    else if (adjLPratio >  1.84 && adjLPratio <=  1.86) {
                        per_rank4 = 33;
                    }
                    else if (adjLPratio >  1.86 && adjLPratio <=  1.88) {
                        per_rank4 = 34;
                    }
                    else if (adjLPratio >  1.88 && adjLPratio <=  1.90) {
                        per_rank4 = 35;
                    }
                    else if (adjLPratio >  1.90 && adjLPratio <=  1.92) {
                        per_rank4 = 36;
                    }
                    else if (adjLPratio >  1.92 && adjLPratio <=  1.94) {
                        per_rank4 = 37;
                    }
                    else if (adjLPratio >  1.94 && adjLPratio <=  1.96) {
                        per_rank4 = 38;
                    }
                    else if (adjLPratio >  1.96 && adjLPratio <=  1.98) {
                        per_rank4 = 39;
                    }
                    else if (adjLPratio >  1.98 && adjLPratio <=  2.00) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjLPratio <=  1.80  && adjLPratio >  1.50){
                    
                    if (adjLPratio >  1.50 && adjLPratio <=  1.53) {
                        per_rank4 = 21;
                    }
                    else if (adjLPratio >  1.53 && adjLPratio <=  1.56) {
                        per_rank4 = 22;
                    }
                    else if (adjLPratio >  1.56 && adjLPratio <=  1.59) {
                        per_rank4 = 23;
                    }
                    else if (adjLPratio >  1.59 && adjLPratio <=  1.62) {
                        per_rank4 = 24;
                    }
                    else if (adjLPratio >  1.62 && adjLPratio <=  1.65) {
                        per_rank4 = 25;
                    }
                    else if (adjLPratio >  1.65 && adjLPratio <=  1.68) {
                        per_rank4 = 26;
                    }
                    else if (adjLPratio >  1.68 && adjLPratio <=  1.71) {
                        per_rank4 = 27;
                    }
                    else if (adjLPratio >  1.71 && adjLPratio <=  1.74) {
                        per_rank4 = 28;
                    }
                    else if (adjLPratio >  1.74 && adjLPratio <=  1.77) {
                        per_rank4 = 29;
                    }
                    else if (adjLPratio >  1.77 && adjLPratio <=  2.80) {
                        per_rank4 = 30;
                    }
                    else  {
                        per_rank4 = 30;
                    }
                }
                else if(adjLPratio <=  1.50  && adjLPratio >  1.20){
                    
                    if (adjLPratio >  1.20 && adjLPratio <=  1.23) {
                        per_rank4 = 11;
                    }
                    else if (adjLPratio >  1.23 && adjLPratio <=  1.26) {
                        per_rank4 = 12;
                    }
                    else if (adjLPratio >  1.26 && adjLPratio <=  1.29) {
                        per_rank4 = 13;
                    }
                    else if (adjLPratio >  1.29 && adjLPratio <=  1.32) {
                        per_rank4 = 14;
                    }
                    else if (adjLPratio >  1.32 && adjLPratio <=  1.35) {
                        per_rank4 = 15;
                    }
                    else if (adjLPratio >  1.35 && adjLPratio <=  1.38) {
                        per_rank4 = 16;
                    }
                    else if (adjLPratio >  1.38 && adjLPratio <=  1.41) {
                        per_rank4 = 17;
                    }
                    else if (adjLPratio >  1.41 && adjLPratio <=  1.44) {
                        per_rank4 = 18;
                    }
                    else if (adjLPratio >  1.44 && adjLPratio <=  1.47) {
                        per_rank4 = 19;
                    }
                    else if (adjLPratio >  1.47 && adjLPratio <=  2.50) {
                        per_rank4 = 20;
                    }
                    else  {
                        per_rank4 = 20;
                    }
                }
                else if(adjLPratio <=  1.20  && adjLPratio >  1.00){
                    
                    if (adjLPratio >  1.00 && adjLPratio <=  1.02) {
                        per_rank4 = 1;
                    }
                    else if (adjLPratio >  1.02 && adjLPratio <=  1.04) {
                        per_rank4 = 2;
                    }
                    else if (adjLPratio >  1.04 && adjLPratio <=  1.06) {
                        per_rank4 = 3;
                    }
                    else if (adjLPratio >  1.06 && adjLPratio <=  1.08) {
                        per_rank4 = 4;
                    }
                    else if (adjLPratio >  1.08 && adjLPratio <=  1.10) {
                        per_rank4 = 5;
                    }
                    else if (adjLPratio >  1.10 && adjLPratio <=  1.12) {
                        per_rank4 = 6;
                    }
                    else if (adjLPratio >  1.12 && adjLPratio <=  1.14) {
                        per_rank4 = 7;
                    }
                    else if (adjLPratio >  1.14 && adjLPratio <=  1.16) {
                        per_rank4 = 8;
                    }
                    else if (adjLPratio >  1.16 && adjLPratio <=  1.18) {
                        per_rank4 = 9;
                    }
                    else if (adjLPratio >  1.18 && adjLPratio <=  2.20) {
                        per_rank4 = 10;
                    }
                }
                else if(adjLPratio <=  1.00){
                    per_rank4 = 0;
                }
                else if(adjLPratio >  3.50){
                    per_rank4 = 100;
                }
                
                  if(per_rank4 == 2222){
                    var lpRank  =  "--" ;                         
                    document.getElementById("lPressRank").value = lpRank ;  // Rank %
                }
                else{                    
                    var lpRank = parseInt(per_rank4);                        
                    document.getElementById("lPressRank").value = parseInt(lpRank) ;  // Rank %
                }
            }
             else{
                    per_rank4 = 2222;
                    var lpRank  =  "--" ;                         
                    document.getElementById("lPressRank").value = lpRank ;  // Rank %
            }
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

            if(adjLPratio > 0){
                
                if(adjLPratio <=  3.10 && adjLPratio >   2.80){
                    
                    if (adjLPratio >  2.80 && adjLPratio <=  2.83) {
                        per_rank4 = 91;
                    }
                    else if (adjLPratio >  2.83 && adjLPratio <=  2.86) {
                        per_rank4 = 92;
                    }
                    else if (adjLPratio >  2.86 && adjLPratio <=  2.89) {
                        per_rank4 = 93;
                    }
                    else if (adjLPratio >  2.89 && adjLPratio <=  2.92) {
                        per_rank4 = 94;
                    }
                    else if (adjLPratio >  2.92 && adjLPratio <=  2.95) {
                        per_rank4 = 95;
                    }
                    else if (adjLPratio >  2.95 && adjLPratio <=  2.98) {
                        per_rank4 = 96;
                    }
                    else if (adjLPratio >  2.98 && adjLPratio <=  3.01) {
                        per_rank4 = 97;
                    }
                    else if (adjLPratio >  3.01 && adjLPratio <=  3.04) {
                        per_rank4 = 98;
                    }
                    else if (adjLPratio >  3.04 && adjLPratio <=  3.07) {
                        per_rank4 = 99;
                    }
                    else if (adjLPratio >  3.07 && adjLPratio <=  3.10) {
                        per_rank4 = 100;
                    }
                    else{
                        per_rank4 = 100;
                    }
                }
                else if(adjLPratio <=  2.80  && adjLPratio >   2.60){
                    
                    if (adjLPratio >  2.60 && adjLPratio <=  2.62) {
                        per_rank4 = 81;
                    }
                    else if (adjLPratio >  2.62 && adjLPratio <=  2.64) {
                        per_rank4 = 82;
                    }
                    else if (adjLPratio >  2.64 && adjLPratio <=  2.66) {
                        per_rank4 = 83;
                    }
                    else if (adjLPratio >  2.66 && adjLPratio <=  2.68) {
                        per_rank4 = 84;
                    }
                    else if (adjLPratio >  2.68 && adjLPratio <=  2.70) {
                        per_rank4 = 85;
                    }
                    else if (adjLPratio >  2.70 && adjLPratio <=  2.72) {
                        per_rank4 = 86;
                    }
                    else if (adjLPratio >  2.72 && adjLPratio <=  2.74) {
                        per_rank4 = 87;
                    }
                    else if (adjLPratio >  2.74 && adjLPratio <=  2.76) {
                        per_rank4 = 88;
                    }
                    else if (adjLPratio >  2.76 && adjLPratio <=  2.78) {
                        per_rank4 = 89;
                    }
                    else if (adjLPratio >  2.78 && adjLPratio <=  2.80) {
                        per_rank4 = 90;
                    }
                    else  {
                        per_rank4 = 90;
                    }
                }
                else if(adjLPratio <=  2.60  && adjLPratio >   2.40){
                    
                    if (adjLPratio >  2.40 && adjLPratio <=  2.42) {
                        per_rank4 = 71;
                    }
                    else if (adjLPratio >  2.42 && adjLPratio <=  2.44) {
                        per_rank4 = 72;
                    }
                    else if (adjLPratio >  2.44 && adjLPratio <=  2.46) {
                        per_rank4 = 73;
                    }
                    else if (adjLPratio >  2.46 && adjLPratio <=  2.48) {
                        per_rank4 = 74;
                    }
                    else if (adjLPratio >  2.48 && adjLPratio <=  2.50) {
                        per_rank4 = 75;
                    }
                    else if (adjLPratio >  2.50 && adjLPratio <=  2.52) {
                        per_rank4 = 76;
                    }
                    else if (adjLPratio >  2.52 && adjLPratio <=  2.54) {
                        per_rank4 = 77;
                    }
                    else if (adjLPratio >  2.54 && adjLPratio <=  2.56) {
                        per_rank4 = 78;
                    }
                    else if (adjLPratio >  2.56 && adjLPratio <=  2.58) {
                        per_rank4 = 79;
                    }
                    else if (adjLPratio >  2.58 && adjLPratio <=  2.60) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjLPratio <=  2.40 && adjLPratio >   2.20){
                    
                    if (adjLPratio >  2.2 && adjLPratio <=  2.22) {
                        per_rank4 = 61;
                    }
                    else if (adjLPratio >  2.22 && adjLPratio <=  2.24) {
                        per_rank4 = 62;
                    }
                    else if (adjLPratio >  2.24 && adjLPratio <=  2.26) {
                        per_rank4 = 63;
                    }
                    else if (adjLPratio >  2.26 && adjLPratio <=  2.28) {
                        per_rank4 = 64;
                    }
                    else if (adjLPratio >  2.28 && adjLPratio <=  2.30) {
                        per_rank4 = 65;
                    }
                    else if (adjLPratio >  2.30 && adjLPratio <=  2.32) {
                        per_rank4 = 66;
                    }
                    else if (adjLPratio >  2.32 && adjLPratio <=  2.34) {
                        per_rank4 = 67;
                    }
                    else if (adjLPratio >  2.34 && adjLPratio <=  2.36) {
                        per_rank4 = 68;
                    }
                    else if (adjLPratio >  2.36 && adjLPratio <=  2.38) {
                        per_rank4 = 69;
                    }
                    else if (adjLPratio >  2.38 && adjLPratio <=  2.40) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjLPratio <=  2.20 && adjLPratio >   2.00){
                    
                    if (adjLPratio >  2.00 && adjLPratio <=  2.02) {
                        per_rank4 = 51;
                    }
                    else if (adjLPratio >  2.02 && adjLPratio <=  2.04) {
                        per_rank4 = 52;
                    }
                    else if (adjLPratio >  2.04 && adjLPratio <=  2.06) {
                        per_rank4 = 53;
                    }
                    else if (adjLPratio >  2.06 && adjLPratio <=  2.08) {
                        per_rank4 = 54;
                    }
                    else if (adjLPratio >  2.08 && adjLPratio <=  2.10) {
                        per_rank4 = 55;
                    }
                    else if (adjLPratio >  2.10 && adjLPratio <=  2.12) {
                        per_rank4 = 56;
                    }
                    else if (adjLPratio >  2.12 && adjLPratio <=  2.14) {
                        per_rank4 = 57;
                    }
                    else if (adjLPratio >  2.14 && adjLPratio <=  2.16) {
                        per_rank4 = 58;
                    }
                    else if (adjLPratio >  2.16 && adjLPratio <=  2.18) {
                        per_rank4 = 59;
                    }
                    else if (adjLPratio >  2.18 && adjLPratio <=  2.20) {
                        per_rank4 = 60;
                    }
                    else  {
                        per_rank4 = 60;
                    }
                  
                }
                else if(adjLPratio <=  2.00  && adjLPratio >  1.80){
                    
                    if (adjLPratio >  1.80 && adjLPratio <=  1.82) {
                        per_rank4 = 41;
                    }
                    else if (adjLPratio >  1.82 && adjLPratio <=  1.84) {
                        per_rank4 = 42;
                    }
                    else if (adjLPratio >  1.84 && adjLPratio <=  1.86) {
                        per_rank4 = 43;
                    }
                    else if (adjLPratio >  1.86 && adjLPratio <=  1.88) {
                        per_rank4 = 44;
                    }
                    else if (adjLPratio >  1.88 && adjLPratio <=  1.90) {
                        per_rank4 = 45;
                    }
                    else if (adjLPratio >  1.90 && adjLPratio <=  1.92) {
                        per_rank4 = 46;
                    }
                    else if (adjLPratio >  1.92 && adjLPratio <=  1.94) {
                        per_rank4 = 47;
                    }
                    else if (adjLPratio >  1.94 && adjLPratio <=  1.96) {
                        per_rank4 = 48;
                    }
                    else if (adjLPratio >  1.96 && adjLPratio <=  1.98) {
                        per_rank4 = 49;
                    }
                    else if (adjLPratio >  1.98 && adjLPratio <=  2.00) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjLPratio <=  1.80  && adjLPratio >  1.60){
                    
                    if (adjLPratio >  1.60 && adjLPratio <=1.62) {
                        per_rank4 = 31;
                    }
                    else if (adjLPratio >  1.62 && adjLPratio <=  1.64) {
                        per_rank4 = 32;
                    }
                    else if (adjLPratio >  1.64 && adjLPratio <=  1.66) {
                        per_rank4 = 33;
                    }
                    else if (adjLPratio >  1.66 && adjLPratio <=  1.68) {
                        per_rank4 = 34;
                    }
                    else if (adjLPratio >  1.68 && adjLPratio <=  1.70) {
                        per_rank4 = 35;
                    }
                    else if (adjLPratio >  1.70 && adjLPratio <=  1.72) {
                        per_rank4 = 36;
                    }
                    else if (adjLPratio >  1.72 && adjLPratio <=  1.74) {
                        per_rank4 = 37;
                    }
                    else if (adjLPratio >  1.74 && adjLPratio <=  1.76) {
                        per_rank4 = 38;
                    }
                    else if (adjLPratio >  1.76 && adjLPratio <=  1.78) {
                        per_rank4 = 39;
                    }
                    else if (adjLPratio >  1.78 && adjLPratio <=  1.80) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjLPratio <=  1.60  && adjLPratio >  1.30){
                    
                    if (adjLPratio >  1.30 && adjLPratio <=  1.33) {
                        per_rank4 = 21;
                    }
                    else if (adjLPratio >  1.33 && adjLPratio <=  1.36) {
                        per_rank4 = 22;
                    }
                    else if (adjLPratio >  1.36 && adjLPratio <=  1.39) {
                        per_rank4 = 23;
                    }
                    else if (adjLPratio >  1.39 && adjLPratio <=  1.42) {
                        per_rank4 = 24;
                    }
                    else if (adjLPratio >  1.42 && adjLPratio <=  1.45) {
                        per_rank4 = 25;
                    }
                    else if (adjLPratio >  1.45 && adjLPratio <=  1.48) {
                        per_rank4 = 26;
                    }
                    else if (adjLPratio >  1.48 && adjLPratio <=  1.51) {
                        per_rank4 = 27;
                    }
                    else if (adjLPratio >  1.51 && adjLPratio <=  1.54) {
                        per_rank4 = 28;
                    }
                    else if (adjLPratio >  1.54 && adjLPratio <=  1.57) {
                        per_rank4 = 29;
                    }
                    else if (adjLPratio >  1.57 && adjLPratio <=  1.60) {
                        per_rank4 = 30;
                    }
                    else  {
                        per_rank4 = 30;
                    }
                }
                else if(adjLPratio <=  1.30  && adjLPratio >  0.90){
                    
                    if (adjLPratio >  0.90 && adjLPratio <=  0.94) {
                        per_rank4 = 11;
                    }
                    else if (adjLPratio >  0.94 && adjLPratio <=  0.98) {
                        per_rank4 = 12;
                    }
                    else if (adjLPratio >  0.98 && adjLPratio <=  1.02) {
                        per_rank4 = 13;
                    }
                    else if (adjLPratio >  1.02 && adjLPratio <=  1.06) {
                        per_rank4 = 14;
                    }
                    else if (adjLPratio >  1.06 && adjLPratio <=  1.10) {
                        per_rank4 = 15;
                    }
                    else if (adjLPratio >  1.10 && adjLPratio <=  1.14) {
                        per_rank4 = 16;
                    }
                    else if (adjLPratio >  1.14 && adjLPratio <=  1.18) {
                        per_rank4 = 17;
                    }
                    else if (adjLPratio >  1.18 && adjLPratio <=  1.22) {
                        per_rank4 = 18;
                    }
                    else if (adjLPratio >  1.22 && adjLPratio <=  1.26) {
                        per_rank4 = 19;
                    }
                    else if (adjLPratio >  1.26 && adjLPratio <=  1.30) {
                        per_rank4 = 20;
                    }
                    else  {
                        per_rank4 = 20;
                    }
                }
                else if(adjLPratio <=  0.90  && adjLPratio >  0.70){
                    
                    if (adjLPratio >  0.70 && adjLPratio <=  0.72) {
                        per_rank4 = 1;
                    }
                    else if (adjLPratio >  0.72 && adjLPratio <=  0.74) {
                        per_rank4 = 2;
                    }
                    else if (adjLPratio >  0.74 && adjLPratio <=  0.76) {
                        per_rank4 = 3;
                    }
                    else if (adjLPratio >  0.76 && adjLPratio <=  0.78) {
                        per_rank4 = 4;
                    }
                    else if (adjLPratio >  0.78 && adjLPratio <=  0.80) {
                        per_rank4 = 5;
                    }
                    else if (adjLPratio >  0.80 && adjLPratio <=  0.82) {
                        per_rank4 = 6;
                    }
                    else if (adjLPratio >  0.82 && adjLPratio <=  0.84) {
                        per_rank4 = 7;
                    }
                    else if (adjLPratio >  0.84 && adjLPratio <=  0.86) {
                        per_rank4 = 8;
                    }
                    else if (adjLPratio >  0.86 && adjLPratio <=  0.88) {
                        per_rank4 = 9;
                    }
                    else if (adjLPratio >  0.88 && adjLPratio <=  0.90) {
                        per_rank4 = 10;
                    }
                }
                else if(adjLPratio <=  0.70){
                    per_rank4 = 0;
                }
                else if(adjLPratio >  3.10){
                    per_rank4 = 100;
                }
                 if(per_rank4 == 2222){
                    var lpRank  =  "--" ;                         
                    document.getElementById("lPressRank").value = lpRank ;  // Rank %
                }
                else{                    
                    var lpRank = parseInt(per_rank4);                        
                    document.getElementById("lPressRank").value = parseInt(lpRank) ;  // Rank %
                }
            }
             else{
                    per_rank4 = 2222;
                    var lpRank  =  "--" ;                         
                    document.getElementById("lPressRank").value = lpRank ;  // Rank %
            }         

        }
      

        
       }
      
      
        document.getElementById("per_rank4").value = parseInt(per_rank4) ;  // Rank4 %
        avgPerRank() ;
    }
    
    
 //get Leg Extension calculations 
    function legExtension() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var leKG = document.getElementById("lExtKG").value ;        
        var leResp = document.getElementById("lExtRM").value ; 
        
       if(parseInt(leKG) > 0 && parseInt(leResp) > 0)  
       {
         var lerm = parseInt(leKG)*2.2/(1.0278-(parseInt(leResp)*0.0278)) ;
         var leRMkg =  parseInt(lerm)/2.2 ;
         document.getElementById("lExtPred").value = Math.floor(parseFloat(leRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var LEratio  =  parseFloat(leRMkg)/parseInt(weight);
        var adjLEratio  =  ageCorrection * parseFloat(LEratio);      
        //alert(adjBPratio); die;
        document.getElementById("lExtSW").value = Math.floor(parseFloat(adjLEratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank5 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
       if(adjLEratio > 0){
                
                
                if(adjLEratio <=  1.350 && adjLEratio >   1.100){
                    
                    if (adjLEratio >  1.100 && adjLEratio <=  1.125) {
                        per_rank5 = 91;
                    }
                    else if (adjLEratio >  1.125 && adjLEratio <=  1.150) {
                        per_rank5 = 92;
                    }
                    else if (adjLEratio >  1.150 && adjLEratio <=  1.175) {
                        per_rank5 = 93;
                    }
                    else if (adjLEratio >  1.175 && adjLEratio <=  1.200) {
                        per_rank5 = 94;
                    }
                    else if (adjLEratio >  1.200 && adjLEratio <=  1.225) {
                        per_rank5 = 95;
                    }
                    else if (adjLEratio >  1.225 && adjLEratio <=  1.250) {
                        per_rank5 = 96;
                    }
                    else if (adjLEratio >  1.250 && adjLEratio <=  1.275) {
                        per_rank5 = 97;
                    }
                    else if (adjLEratio >  1.275 && adjLEratio <=  1.300) {
                        per_rank5 = 98;
                    }
                    else if (adjLEratio >  1.300 && adjLEratio <=  1.325) {
                        per_rank5 = 99;
                    }
                    else  {
                        per_rank5 = 100;
                    }
                    
                }
                else if(adjLEratio <=  1.10 && adjLEratio >   0.90){
                    
                    if (adjLEratio >  0.90 && adjLEratio <=  0.92) {
                        per_rank5 = 81;
                    }
                    else if (adjLEratio >  0.92 && adjLEratio <=  0.94) {
                        per_rank5 = 82;
                    }
                    else if (adjLEratio >  0.94 && adjLEratio <=  0.96) {
                        per_rank5 = 83;
                    }
                    else if (adjLEratio >  0.96 && adjLEratio <=  0.98) {
                        per_rank5 = 84;
                    }
                    else if (adjLEratio >  0.98 && adjLEratio <=  1.00) {
                        per_rank5 = 85;
                    }
                    else if (adjLEratio >  1.00 && adjLEratio <=  1.02) {
                        per_rank5 = 86;
                    }
                    else if (adjLEratio >  1.02 && adjLEratio <=  1.04) {
                        per_rank5 = 87;
                    }
                    else if (adjLEratio >  1.04 && adjLEratio <=  1.06) {
                        per_rank5 = 88;
                    }
                    else if (adjLEratio >  1.06 && adjLEratio <=  1.08) {
                        per_rank5 = 89;
                    }
                    else  {
                        per_rank5 = 90;
                    }
                    
                    
                }
                else if(adjLEratio <=  0.90 && adjLEratio >   0.80){
                    
                    if (adjLEratio >  0.80 && adjLEratio <=  0.81) {
                        per_rank5 = 71;
                    }
                    else if (adjLEratio >  0.81 && adjLEratio <=  0.82) {
                        per_rank5 = 72;
                    }
                    else if (adjLEratio >  0.82 && adjLEratio <=  0.83) {
                        per_rank5 = 73;
                    }
                    else if (adjLEratio >  0.83 && adjLEratio <=  0.84) {
                        per_rank5 = 74;
                    }
                    else if (adjLEratio >  0.84 && adjLEratio <=  0.85) {
                        per_rank5 = 75;
                    }
                    else if (adjLEratio >  0.85 && adjLEratio <=  0.86) {
                        per_rank5 = 76;
                    }
                    else if (adjLEratio >  0.86 && adjLEratio <=  0.87) {
                        per_rank5 = 77;
                    }
                    else if (adjLEratio >  0.87 && adjLEratio <=  0.88) {
                        per_rank5 = 78;
                    }
                    else if (adjLEratio >  0.88 && adjLEratio <=  0.89) {
                        per_rank5 = 79;
                    }
                    else  {
                        per_rank5 = 80;
                    }
                    
                    
                }//
                else if(adjLEratio <=  0.80  && adjLEratio >   0.70){
                    
                    
                    if (adjLEratio >  0.70 && adjLEratio <=  0.71) {
                        per_rank5 = 61;
                    }
                    else if (adjLEratio >  0.71 && adjLEratio <=  0.72) {
                        per_rank5 = 62;
                    }
                    else if (adjLEratio >  0.72 && adjLEratio <=  0.73) {
                        per_rank5 = 63;
                    }
                    else if (adjLEratio >  0.73 && adjLEratio <=  0.74) {
                        per_rank5 = 64;
                    }
                    else if (adjLEratio >  0.74 && adjLEratio <=  0.75) {
                        per_rank5 = 65;
                    }
                    else if (adjLEratio >  0.75 && adjLEratio <=  0.76) {
                        per_rank5 = 66;
                    }
                    else if (adjLEratio >  0.76 && adjLEratio <=  0.77) {
                        per_rank5 = 67;
                    }
                    else if (adjLEratio >  0.77 && adjLEratio <=  0.78) {
                        per_rank5 = 68;
                    }
                    else if (adjLEratio >  0.78 && adjLEratio <=  0.79) {
                        per_rank5 = 69;
                    }
                    else  {
                        per_rank5 = 70;
                    }
                    
                    
                }//
                else if(adjLEratio <=  0.700 && adjLEratio >   0.650){
                    
                    if (adjLEratio >  0.650 && adjLEratio <=  0.655) {
                        per_rank5 = 51;
                    }
                    else if (adjLEratio >  0.655 && adjLEratio <=  0.660) {
                        per_rank5 = 52;
                    }
                    else if (adjLEratio >  0.660 && adjLEratio <=  0.665) {
                        per_rank5 = 53;
                    }
                    else if (adjLEratio >  0.665 && adjLEratio <=  0.670) {
                        per_rank5 = 54;
                    }
                    else if (adjLEratio >  0.670 && adjLEratio <=  0.675) {
                        per_rank5 = 55;
                    }
                    else if (adjLEratio >  0.675 && adjLEratio <=  0.680) {
                        per_rank5 = 56;
                    }
                    else if (adjLEratio >  0.680 && adjLEratio <=  0.685) {
                        per_rank5 = 57;
                    }
                    else if (adjLEratio >  0.685 && adjLEratio <=  0.690) {
                        per_rank5 = 58;
                    }
                    else if (adjLEratio >  0.690 && adjLEratio <=  0.695) {
                        per_rank5 = 59;
                    }
                    else  {
                        per_rank5 = 60;
                    }
                    
                    
                }//
                else if(adjLEratio <=  0.650 && adjLEratio >   0.600){
                    
                    if (adjLEratio >  0.600 && adjLEratio <=  0.605) {
                        per_rank5 = 41;
                    }
                    else if (adjLEratio >  0.605 && adjLEratio <=  0.610) {
                        per_rank5 = 42;
                    }
                    else if (adjLEratio >  0.610 && adjLEratio <=  0.615) {
                        per_rank5 = 43;
                    }
                    else if (adjLEratio >  0.615 && adjLEratio <=  0.620) {
                        per_rank5 = 44;
                    }
                    else if (adjLEratio >  0.620 && adjLEratio <=  0.625) {
                        per_rank5 = 45;
                    }
                    else if (adjLEratio >  0.625 && adjLEratio <=  0.630) {
                        per_rank5 = 46;
                    }
                    else if (adjLEratio >  0.630 && adjLEratio <=  0.635) {
                        per_rank5 = 47;
                    }
                    else if (adjLEratio >  0.635 && adjLEratio <=  0.640) {
                        per_rank5 = 48;
                    }
                    else if (adjLEratio >  0.640 && adjLEratio <=  0.645) {
                        per_rank5 = 49;
                    }
                    else  {
                        per_rank5 = 50;
                    }
                    
                }
                else if(adjLEratio <=  0.600 && adjLEratio >   0.550){
                    
                    if (adjLEratio >  0.550 && adjLEratio <=  0.555) {
                        per_rank5 = 31;
                    }
                    else if (adjLEratio >  0.555 && adjLEratio <=  0.560) {
                        per_rank5 = 32;
                    }
                    else if (adjLEratio >  0.560 && adjLEratio <=  0.565) {
                        per_rank5 = 33;
                    }
                    else if (adjLEratio >  0.565 && adjLEratio <=  0.570) {
                        per_rank5 = 34;
                    }
                    else if (adjLEratio >  0.570 && adjLEratio <=  0.575) {
                        per_rank5 = 35;
                    }
                    else if (adjLEratio >  0.575 && adjLEratio <=  0.580) {
                        per_rank5 = 36;
                    }
                    else if (adjLEratio >  0.580 && adjLEratio <=  0.585) {
                        per_rank5 = 37;
                    }
                    else if (adjLEratio >  0.585 && adjLEratio <=  0.590) {
                        per_rank5 = 38;
                    }
                    else if (adjLEratio >  0.590 && adjLEratio <=  0.595) {
                        per_rank5 = 39;
                    }
                    else  {
                        per_rank5 = 40;
                    }
                    
                    
                }
                else if(adjLEratio <=  0.55 && adjLEratio >   0.45){
                    
                    if (adjLEratio >  0.45 && adjLEratio <=  0.46) {
                        per_rank5 = 21;
                    }
                    else if (adjLEratio >  0.46 && adjLEratio <=  0.47) {
                        per_rank5 = 22;
                    }
                    else if (adjLEratio >  0.47 && adjLEratio <=  0.48) {
                        per_rank5 = 23;
                    }
                    else if (adjLEratio >  0.48 && adjLEratio <=  0.49) {
                        per_rank5 = 24;
                    }
                    else if (adjLEratio >  0.49 && adjLEratio <=  0.50) {
                        per_rank5 = 25;
                    }
                    else if (adjLEratio >  0.50 && adjLEratio <=  0.51) {
                        per_rank5 = 26;
                    }
                    else if (adjLEratio >  0.51 && adjLEratio <=  0.52) {
                        per_rank5 = 27;
                    }
                    else if (adjLEratio >  0.52 && adjLEratio <=  0.53) {
                        per_rank5 = 28;
                    }
                    else if (adjLEratio >  0.53 && adjLEratio <=  0.54) {
                        per_rank5 = 29;
                    }
                    else  {
                        per_rank5 = 30;
                    }
                }
                else if(adjLEratio <=  0.450 && adjLEratio >   0.300){
                    
                    if (adjLEratio >  0.300 && adjLEratio<=  0.315) {
                        per_rank5 = 11;
                    }
                    else if (adjLEratio >  0.315 && adjLEratio <=  0.330) {
                        per_rank5 = 12;
                    }
                    else if (adjLEratio >  0.330 && adjLEratio <=  0.345) {
                        per_rank5 = 13;
                    }
                    else if (adjLEratio >  0.345 && adjLEratio <=  0.360) {
                        per_rank5 = 14;
                    }
                    else if (adjLEratio >  0.360 && adjLEratio <=  0.375) {
                        per_rank5 = 15;
                    }
                    else if (adjLEratio >  0.375 && adjLEratio <=  0.390) {
                        per_rank5 = 16;
                    }
                    else if (adjLEratio >  0.390 && adjLEratio <=  0.405) {
                        per_rank5 = 17;
                    }
                    else if (adjLEratio >  0.405 && adjLEratio <=  0.420) {
                        per_rank5 = 18;
                    }
                    else if (adjLEratio >  0.420 && adjLEratio <=  0.435) {
                        per_rank5 = 19;
                    }
                    else  {
                        per_rank5 = 20;
                    }
                }
                else if(adjLEratio <=  0.30  && adjLEratio >  0.10){
                    
                    
                    if (adjLEratio >  0.10 && adjLEratio <=  0.12) {
                        per_rank5 = 1;
                    }
                    else if (adjLEratio >  0.12 && adjLEratio <=  0.14) {
                        per_rank5 = 2;
                    }
                    else if (adjLEratio >  0.14 && adjLEratio <=  0.16) {
                        per_rank5 = 3;
                    }
                    else if (adjLEratio >  0.16 && adjLEratio <=  0.18) {
                        per_rank5 = 4;
                    }
                    else if (adjLEratio >  0.18 && adjLEratio <=  0.20) {
                        per_rank5 = 5;
                    }
                    else if (adjLEratio >  0.20 && adjLEratio <=  0.22) {
                        per_rank5 = 6;
                    }
                    else if (adjLEratio >  0.22 && adjLEratio <=  0.24) {
                        per_rank5 = 7;
                    }
                    else if (adjLEratio >  0.24 && adjLEratio <=  0.26) {
                        per_rank5 = 8;
                    }
                    else if (adjLEratio >  0.26 && adjLEratio <=  0.28) {
                        per_rank5 = 9;
                    }
                    else  {
                        per_rank5 = 10;
                    }

                }
                else if(adjLEratio <=   0.10 ){
                    per_rank5 = 0;
                }
                else if(adjLEratio >   1.350){
                    per_rank5 = 100;
                }
                
                  if(per_rank5 == 2222){
                    var leRank  =  "--" ;                         
                    document.getElementById("lExtRank").value = leRank ;  // Rank %
                }
                else{                    
                    var leRank = parseInt(per_rank5);                        
                    document.getElementById("lExtRank").value = parseInt(leRank) ;  // Rank %
                }
            }
             else{
                    per_rank5 = 2222;
                    var leRank  =  "--" ;                         
                    document.getElementById("lExtRank").value = leRank ;  // Rank %
            }
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

              if(adjLEratio > 0){
                
                if(adjLEratio <=  1.200 && adjLEratio >   0.950){
                    
                    
                    if (adjLEratio >  0.950 && adjLEratio <=  0.975) {
                        per_rank5 = 91;
                    }
                    else if (adjLEratio >  0.975 && adjLEratio <=  1.000) {
                        per_rank5 = 92;
                    }
                    else if (adjLEratio >  1.000 && adjLEratio <=  1.025) {
                        per_rank5 = 93;
                    }
                    else if (adjLEratio >  1.025 && adjLEratio <=  1.050) {
                        per_rank5 = 94;
                    }
                    else if (adjLEratio >  1.050 && adjLEratio <=  1.075) {
                        per_rank5 = 95;
                    }
                    else if (adjLEratio >  1.075 && adjLEratio <=  1.100) {
                        per_rank5 = 96;
                    }
                    else if (adjLEratio >  1.100 && adjLEratio <=  1.125) {
                        per_rank5 = 97;
                    }
                    else if (adjLEratio >  1.125 && adjLEratio <=  1.150) {
                        per_rank5 = 98;
                    }
                    else if (adjLEratio >  1.150 && adjLEratio <=  1.175) {
                        per_rank5 = 99;
                    }
                    else  {
                        per_rank5 = 100;
                    }
                    
                }
                else if(adjLEratio <=  0.950 && adjLEratio >   0.800){
                    
                    if (adjLEratio >  0.800 && adjLEratio <=  0.815) {
                        per_rank5 = 81;
                    }
                    else if (adjLEratio >  0.815 && adjLEratio <=  0.830) {
                        per_rank5 = 82;
                    }
                    else if (adjLEratio >  0.830 && adjLEratio <=  0.845) {
                        per_rank5 = 83;
                    }
                    else if (adjLEratio >  0.845 && adjLEratio <=  0.860) {
                        per_rank5 = 84;
                    }
                    else if (adjLEratio >  0.860 && adjLEratio <=  0.875) {
                        per_rank5 = 85;
                    }
                    else if (adjLEratio >  0.875 && adjLEratio <=  0.890) {
                        per_rank5 = 86;
                    }
                    else if (adjLEratio >  0.890 && adjLEratio <=  0.905) {
                        per_rank5 = 87;
                    }
                    else if (adjLEratio >  0.905 && adjLEratio <=  0.920) {
                        per_rank5 = 88;
                    }
                    else if (adjLEratio >  0.920 && adjLEratio <=  0.935) {
                        per_rank5 = 89;
                    }
                    else  {
                        per_rank5 = 90;
                    }
                    
                    
                }
                
                else if(adjLEratio <= 0.80  && adjLEratio > 0.70){
                    
                    if (adjLEratio >  0.70 && adjLEratio <=  0.71) {
                        per_rank5 = 71;
                    }
                    else if (adjLEratio >  0.71 && adjLEratio <=  0.72) {
                        per_rank5 = 72;
                    }
                    else if (adjLEratio >  0.72 && adjLEratio <=  0.73) {
                        per_rank5 = 73;
                    }
                    else if (adjLEratio >  0.73 && adjLEratio <=  0.74) {
                        per_rank5 = 74;
                    }
                    else if (adjLEratio >  0.74 && adjLEratio <=  0.75) {
                        per_rank5 = 75;
                    }
                    else if (adjLEratio >  0.75 && adjLEratio <=  0.76) {
                        per_rank5 = 76;
                    }
                    else if (adjLEratio >  0.76 && adjLEratio <=  0.77) {
                        per_rank5 = 77;
                    }
                    else if (adjLEratio >  0.77 && adjLEratio <=  0.78) {
                        per_rank5 = 78;
                    }
                    else if (adjLEratio >  0.78 && adjLEratio <=  0.79) {
                        per_rank5 = 79;
                    }
                    else  {
                        per_rank5 = 80;
                    }
                    
                }
                else if(adjLEratio <=  0.70  && adjLEratio >   0.60){
                    
                    
                    if (adjLEratio >  0.60 && adjLEratio <=  0.61) {
                        per_rank5 = 61;
                    }
                    else if (adjLEratio >  0.61 && adjLEratio <=  0.62) {
                        per_rank5 = 62;
                    }
                    else if (adjLEratio >  0.62 && adjLEratio <=  0.63) {
                        per_rank5 = 63;
                    }
                    else if (adjLEratio >  0.63 && adjLEratio <=  0.64) {
                        per_rank5 = 64;
                    }
                    else if (adjLEratio >  0.64 && adjLEratio <=  0.65) {
                        per_rank5 = 65;
                    }
                    else if (adjLEratio >  0.65 && adjLEratio <=  0.66) {
                        per_rank5 = 66;
                    }
                    else if (adjLEratio >  0.66 && adjLEratio <=  0.67) {
                        per_rank5 = 67;
                    }
                    else if (adjLEratio >  0.67 && adjLEratio <=  0.68) {
                        per_rank5 = 68;
                    }
                    else if (adjLEratio >  0.68 && adjLEratio <=  0.69) {
                        per_rank5 = 69;
                    }
                    else  {
                        per_rank5 = 70;
                    }
                    
                    
                }//
                else if(adjLEratio <=  0.60 && adjLEratio >   0.50){
                    
                    if (adjLEratio >  0.50 && adjLEratio <=  0.51) {
                        per_rank5 = 51;
                    }
                    else if (adjLEratio >  0.51 && adjLEratio <=  0.52) {
                        per_rank5 = 52;
                    }
                    else if (adjLEratio >  0.52 && adjLEratio <=  0.53) {
                        per_rank5 = 53;
                    }
                    else if (adjLEratio >  0.53 && adjLEratio <=  0.54) {
                        per_rank5 = 54;
                    }
                    else if (adjLEratio >  0.54 && adjLEratio <=  0.55) {
                        per_rank5 = 55;
                    }
                    else if (adjLEratio >  0.55 && adjLEratio <=  0.56) {
                        per_rank5 = 56;
                    }
                    else if (adjLEratio >  0.56 && adjLEratio <=  0.57) {
                        per_rank5 = 57;
                    }
                    else if (adjLEratio >  0.57 && adjLEratio <=  0.58) {
                        per_rank5 = 58;
                    }
                    else if (adjLEratio >  0.58 && adjLEratio <=  0.59) {
                        per_rank5 = 59;
                    }
                    else  {
                        per_rank5 = 60;
                    }
                    
                    
                }//
                else if(adjLEratio <=  0.50 && adjLEratio >   0.45){
                    
                    if (adjLEratio >  0.450 && adjLEratio <=  0.455) {
                        per_rank5 = 41;
                    }
                    else if (adjLEratio >  0.455 && adjLEratio <=  0.460) {
                        per_rank5 = 42;
                    }
                    else if (adjLEratio >  0.460 && adjLEratio <=  0.465) {
                        per_rank5 = 43;
                    }
                    else if (adjLEratio >  0.465 && adjLEratio <=  0.470) {
                        per_rank5 = 44;
                    }
                    else if (adjLEratio >  0.470 && adjLEratio <=  0.475) {
                        per_rank5 = 45;
                    }
                    else if (adjLEratio >  0.475 && adjLEratio <=  0.480) {
                        per_rank5 = 46;
                    }
                    else if (adjLEratio >  0.480 && adjLEratio <=  0.485) {
                        per_rank5 = 47;
                    }
                    else if (adjLEratio >  0.485 && adjLEratio <=  0.490) {
                        per_rank5 = 48;
                    }
                    else if (adjLEratio >  0.490 && adjLEratio <=  0.495) {
                        per_rank5 = 49;
                    }
                    else  {
                        per_rank5 = 50;
                    }
                    
                }
                else if(adjLEratio <=  0.45  && adjLEratio >  0.40){
                    
                    
                    if (adjLEratio >  0.400 && adjLEratio <=  0.405) {
                        per_rank5 = 31;
                    }
                    else if (adjLEratio >  0.405 && adjLEratio <=  0.410) {
                        per_rank5 = 32;
                    }
                    else if (adjLEratio >  0.410 && adjLEratio <=  0.415) {
                        per_rank5 = 33;
                    }
                    else if (adjLEratio >  0.415 && adjLEratio <=  0.420) {
                        per_rank5 = 34;
                    }
                    else if (adjLEratio >  0.420 && adjLEratio <=  0.425) {
                        per_rank5 = 35;
                    }
                    else if (adjLEratio >  0.425 && adjLEratio <=  0.430) {
                        per_rank5 = 36;
                    }
                    else if (adjLEratio >  0.430 && adjLEratio <=  0.435) {
                        per_rank5 = 37;
                    }
                    else if (adjLEratio >  0.435 && adjLEratio <=  0.440) {
                        per_rank5 = 38;
                    }
                    else if (adjLEratio >  0.440 && adjLEratio <=  0.445) {
                        per_rank5 = 39;
                    }
                    else  {
                        per_rank5 = 40;
                    }

                }
                else if(adjLEratio <=  0.40  && adjLEratio >  0.35){
                    
                    if (adjLEratio >  0.350 && adjLEratio <=  0.355) {
                        per_rank5 = 21;
                    }
                    else if (adjLEratio >  0.355 && adjLEratio <=  0.360) {
                        per_rank5 = 22;
                    }
                    else if (adjLEratio >  0.360 && adjLEratio <=  0.365) {
                        per_rank5 = 23;
                    }
                    else if (adjLEratio >  0.365 && adjLEratio <=  0.370) {
                        per_rank5 = 24;
                    }
                    else if (adjLEratio >  0.370 && adjLEratio <=  0.375) {
                        per_rank5 = 25;
                    }
                    else if (adjLEratio >  0.375 && adjLEratio <=  0.380) {
                        per_rank5 = 26;
                    }
                    else if (adjLEratio >  0.380 && adjLEratio <=  0.385) {
                        per_rank5 = 27;
                    }
                    else if (adjLEratio >  0.385 && adjLEratio <=  0.390) {
                        per_rank5 = 28;
                    }
                    else if (adjLEratio >  0.390 && adjLEratio <=  0.395) {
                        per_rank5 = 29;
                    }
                    else  {
                        per_rank5 = 30;
                    }
                    
                }
                else if(adjLEratio <=  0.35  && adjLEratio >  0.25){
                    
                    if (adjLEratio >  0.50 && adjLEratio <=  0.51) {
                        per_rank5 = 11;
                    }
                    else if (adjLEratio >  0.51 && adjLEratio <=  0.52) {
                        per_rank5 = 12;
                    }
                    else if (adjLEratio >  0.52 && adjLEratio <=  0.53) {
                        per_rank5 = 13;
                    }
                    else if (adjLEratio >  0.53 && adjLEratio <=  0.54) {
                        per_rank5 = 14;
                    }
                    else if (adjLEratio >  0.54 && adjLEratio <=  0.55) {
                        per_rank5 = 15;
                    }
                    else if (adjLEratio >  0.55 && adjLEratio <=  0.56) {
                        per_rank5 = 16;
                    }
                    else if (adjLEratio >  0.56 && adjLEratio <=  0.57) {
                        per_rank5 = 17;
                    }
                    else if (adjLEratio >  0.57 && adjLEratio <=  0.58) {
                        per_rank5 = 18;
                    }
                    else if (adjLEratio >  0.58 && adjLEratio <=  0.59) {
                        per_rank5 = 19;
                    }
                    else  {
                        per_rank5 = 20;
                    }
                    
                }
                
                else if(adjLEratio <=  0.25  && adjLEratio >  0.20){
                    
                    if (adjLEratio >  0.200 && adjLEratio <=  0.205) {
                        per_rank5 = 1;
                    }
                    else if (adjLEratio >  0.205 && adjLEratio <=  0.210) {
                        per_rank5 = 2;
                    }
                    else if (adjLEratio >  0.210 && adjLEratio <=  0.215) {
                        per_rank5 = 3;
                    }
                    else if (adjLEratio >  0.215 && adjLEratio <=  0.220) {
                        per_rank5 = 4;
                    }
                    else if (adjLEratio >  0.220 && adjLEratio <=  0.225) {
                        per_rank5 = 5;
                    }
                    else if (adjLEratio >  0.225 && adjLEratio <=  0.230) {
                        per_rank5 = 6;
                    }
                    else if (adjLEratio >  0.230 && adjLEratio <=  0.235) {
                        per_rank5 = 7;
                    }
                    else if (adjLEratio >  0.235 && adjLEratio <=  0.240) {
                        per_rank5 = 8;
                    }
                    else if (adjLEratio >  0.240 && adjLEratio <=  0.245) {
                        per_rank5 = 9;
                    }
                    else  {
                        per_rank5 = 10;
                    }

                }
                else if(adjLEratio <=  0.20){
                    per_rank5 = 0;
                }
                else if(adjLEratio >   1.200){
                    per_rank5 = 100;
                }
                
                 if(per_rank5 == 2222){
                    var leRank  =  "--" ;                         
                    document.getElementById("lExtRank").value = leRank ;  // Rank %
                }
                else{                    
                    var leRank = parseInt(per_rank5);                        
                    document.getElementById("lExtRank").value = parseInt(leRank) ;  // Rank %
                }
            }
             else{
                    per_rank5 = 2222;
                    var leRank  =  "--" ;                         
                    document.getElementById("lExtRank").value = leRank ;  // Rank %
            }      

        }
      

        
       }      
       
        document.getElementById("per_rank5").value = parseInt(per_rank5) ;  // Rank5 %
        avgPerRank() ;
      
    }
    
    
    //get Leg Curl calculations 
    function legCurl() 
    {
        var ageValue = document.getElementById("age").value ;
        var weight = document.getElementById("weight").value ; 
        var lcKG = document.getElementById("lCurlKG").value ;        
        var lcResp = document.getElementById("lCurlRM").value ; 
        
       if(parseInt(lcKG) > 0 && parseInt(lcResp) > 0)  
       {
         var lcrm = parseInt(lcKG)*2.2/(1.0278-(parseInt(lcResp)*0.0278)) ;
         var lcRMkg =  parseInt(lcrm)/2.2 ;
         document.getElementById("lCurlPred").value = Math.floor(parseFloat(lcRMkg) * 10) / 10 ;    // Pred 1RM
         
         var ageCorrection = 0.0;
        if(parseInt(ageValue) > 25){
            ageCorrection = ((parseInt(ageValue)-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        //  alert(ageCorrection); die;
       
        var LCratio  =  parseFloat(lcRMkg)/parseInt(weight);
        var adjLCratio  =  ageCorrection * parseFloat(LCratio);      
        //alert(adjBPratio); die;
        document.getElementById("lCurlSW").value = Math.floor(parseFloat(adjLCratio) * 1000) / 1000 ;    // SW Ratio
        
        var per_rank6 = 2222 ;
        
       // if client is Male
        if(document.getElementById('gender_Male').checked) {
                
                if(adjLCratio > 0){
                
                
                if(adjLCratio <=  0.80  && adjLCratio >   0.70){
                    
                    
                    if (adjLCratio >  0.70 && adjLCratio <=  0.71) {
                        per_rank6 = 91;
                    }
                    else if (adjLCratio >  0.71 && adjLCratio <=  0.72) {
                        per_rank6 = 92;
                    }
                    else if (adjLCratio >  0.72 && adjLCratio <=  0.73) {
                        per_rank6 = 93;
                    }
                    else if (adjLCratio >  0.73 && adjLCratio <=  0.74) {
                        per_rank6 = 94;
                    }
                    else if (adjLCratio >  0.74 && adjLCratio <=  0.75) {
                        per_rank6 = 95;
                    }
                    else if (adjLCratio >  0.75 && adjLCratio <=  0.76) {
                        per_rank6 = 96;
                    }
                    else if (adjLCratio >  0.76 && adjLCratio <=  0.77) {
                        per_rank6 = 97;
                    }
                    else if (adjLCratio >  0.77 && adjLCratio <=  0.78) {
                        per_rank6 = 98;
                    }
                    else if (adjLCratio >  0.78 && adjLCratio <=  0.79) {
                        per_rank6 = 99;
                    }
                    else  {
                        per_rank6 = 100;
                    }
                }
                else if(adjLCratio <=  0.700 && adjLCratio >   0.650){
                    
                    
                    if (adjLCratio >  0.650 && adjLCratio <=  0.655) {
                        per_rank6 = 81;
                    }
                    else if (adjLCratio >  0.655 && adjLCratio <=  0.660) {
                        per_rank6 = 82;
                    }
                    else if (adjLCratio >  0.660 && adjLCratio <=  0.665) {
                        per_rank6 = 83;
                    }
                    else if (adjLCratio >  0.665 && adjLCratio <=  0.670) {
                        per_rank6 = 84;
                    }
                    else if (adjLCratio >  0.670 && adjLCratio <=  0.675) {
                        per_rank6 = 85;
                    }
                    else if (adjLCratio >  0.675 && adjLCratio <=  0.680) {
                        per_rank6 = 86;
                    }
                    else if (adjLCratio >  0.680 && adjLCratio <=  0.685) {
                        per_rank6 = 87;
                    }
                    else if (adjLCratio >  0.685 && adjLCratio <=  0.690) {
                        per_rank6 = 88;
                    }
                    else if (adjLCratio >  0.690 && adjLCratio <=  0.695) {
                        per_rank6 = 89;
                    }
                    else  {
                        per_rank6 = 90;
                    }

                }
                else if(adjLCratio <=  0.65  && adjLCratio >   0.60){
                    
                    
                    if (adjLCratio >  0.600 && adjLCratio <=  0.605) {
                        per_rank6 = 71;
                    }
                    else if (adjLCratio >  0.605 && adjLCratio <=  0.610) {
                        per_rank6 = 72;
                    }
                    else if (adjLCratio >  0.610 && adjLCratio <=  0.615) {
                        per_rank6 = 73;
                    }
                    else if (adjLCratio >  0.615 && adjLCratio <=  0.620) {
                        per_rank6 = 74;
                    }
                    else if (adjLCratio >  0.620 && adjLCratio <=  0.625) {
                        per_rank6 = 75;
                    }
                    else if (adjLCratio >  0.625 && adjLCratio <=  0.630) {
                        per_rank6 = 76;
                    }
                    else if (adjLCratio >  0.630 && adjLCratio <=  0.635) {
                        per_rank6 = 77;
                    }
                    else if (adjLCratio >  0.635 && adjLCratio <=  0.640) {
                        per_rank6 = 78;
                    }
                    else if (adjLCratio >  0.640 && adjLCratio <=  0.645) {
                        per_rank6 = 79;
                    }
                    else  {
                        per_rank6 = 80;
                    }
                }
                else if(adjLCratio <=  0.60 && adjLCratio >   0.55){
                    
                    if (adjLCratio >  0.550 && adjLCratio <=  0.555) {
                        per_rank6 = 61;
                    }
                    else if (adjLCratio >  0.555 && adjLCratio <=  0.560) {
                        per_rank6 = 62;
                    }
                    else if (adjLCratio >  0.560 && adjLCratio <=  0.565) {
                        per_rank6 = 63;
                    }
                    else if (adjLCratio >  0.565 && adjLCratio <=  0.570) {
                        per_rank6 = 64;
                    }
                    else if (adjLCratio >  0.570 && adjLCratio <=  0.575) {
                        per_rank6 = 65;
                    }
                    else if (adjLCratio >  0.575 && adjLCratio <=  0.580) {
                        per_rank6 = 66;
                    }
                    else if (adjLCratio >  0.580 && adjLCratio <=  0.585) {
                        per_rank6 = 67;
                    }
                    else if (adjLCratio >  0.585 && adjLCratio <=  0.590) {
                        per_rank6 = 68;
                    }
                    else if (adjLCratio >  0.590 && adjLCratio <=  0.595) {
                        per_rank6 = 69;
                    }
                    else  {
                        per_rank6 = 70;
                    }
                }
                else if(adjLCratio <=  0.55 && adjLCratio >   0.50){
                    
                    if (adjLCratio >  0.500 && adjLCratio <=  0.505) {
                        per_rank6 = 51;
                    }
                    else if (adjLCratio >  0.505 && adjLCratio <=  0.510) {
                        per_rank6 = 52;
                    }
                    else if (adjLCratio >  0.510 && adjLCratio <=  0.515) {
                        per_rank6 = 53;
                    }
                    else if (adjLCratio >  0.515 && adjLCratio <=  0.520) {
                        per_rank6 = 54;
                    }
                    else if (adjLCratio >  0.520 && adjLCratio <=  0.525) {
                        per_rank6 = 55;
                    }
                    else if (adjLCratio >  0.525 && adjLCratio <=  0.530) {
                        per_rank6 = 56;
                    }
                    else if (adjLCratio >  0.530 && adjLCratio <=  0.535) {
                        per_rank6 = 57;
                    }
                    else if (adjLCratio >  0.535 && adjLCratio <=  0.540) {
                        per_rank6 = 58;
                    }
                    else if (adjLCratio >  0.540 && adjLCratio <=  0.545) {
                        per_rank6 = 59;
                    }
                    else  {
                        per_rank6 = 60;
                    }

                }
                else if(adjLCratio <=  0.50 && adjLCratio >   0.45){
                    
                    if (adjLCratio >  0.450 && adjLCratio <=  0.455) {
                        per_rank6 = 41;
                    }
                    else if (adjLCratio >  0.455 && adjLCratio <=  0.460) {
                        per_rank6 = 42;
                    }
                    else if (adjLCratio >  0.460 && adjLCratio <=  0.465) {
                        per_rank6 = 43;
                    }
                    else if (adjLCratio >  0.465 && adjLCratio <=  0.470) {
                        per_rank6 = 44;
                    }
                    else if (adjLCratio >  0.470 && adjLCratio <=  0.475) {
                        per_rank6 = 45;
                    }
                    else if (adjLCratio >  0.475 && adjLCratio <=  0.480) {
                        per_rank6 = 46;
                    }
                    else if (adjLCratio >  0.480 && adjLCratio <=  0.485) {
                        per_rank6 = 47;
                    }
                    else if (adjLCratio >  0.485 && adjLCratio <=  0.490) {
                        per_rank6 = 48;
                    }
                    else if (adjLCratio >  0.490 && adjLCratio <=  0.495) {
                        per_rank6 = 49;
                    }
                    else  {
                        per_rank6 = 50;
                    }
                }
                else if(adjLCratio <=  0.45  && adjLCratio >  0.40){
                    
                    
                    if (adjLCratio >  0.400 && adjLCratio <=  0.405) {
                        per_rank6 = 31;
                    }
                    else if (adjLCratio >  0.405 && adjLCratio <=  0.410) {
                        per_rank6 = 32;
                    }
                    else if (adjLCratio >  0.410 && adjLCratio <=  0.415) {
                        per_rank6 = 33;
                    }
                    else if (adjLCratio >  0.415 && adjLCratio <=  0.420) {
                        per_rank6 = 34;
                    }
                    else if (adjLCratio >  0.420 && adjLCratio <=  0.425) {
                        per_rank6 = 35;
                    }
                    else if (adjLCratio >  0.425 && adjLCratio <=  0.430) {
                        per_rank6 = 36;
                    }
                    else if (adjLCratio >  0.430 && adjLCratio <=  0.435) {
                        per_rank6 = 37;
                    }
                    else if (adjLCratio >  0.435 && adjLCratio <=  0.440) {
                        per_rank6 = 38;
                    }
                    else if (adjLCratio >  0.440 && adjLCratio <=  0.445) {
                        per_rank6 = 39;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                }
                else if(adjLCratio <=  0.40  && adjLCratio >   0.35){
                    
                    if (adjLCratio >  0.350 && adjLCratio <=  0.355) {
                        per_rank6 = 21;
                    }
                    else if (adjLCratio >  0.355 && adjLCratio <=  0.360) {
                        per_rank6 = 22;
                    }
                    else if (adjLCratio >  0.360 && adjLCratio <=  0.365) {
                        per_rank6 = 23;
                    }
                    else if (adjLCratio >  0.365 && adjLCratio <=  0.370) {
                        per_rank6 = 24;
                    }
                    else if (adjLCratio >  0.370 && adjLCratio <=  0.375) {
                        per_rank6 = 25;
                    }
                    else if (adjLCratio >  0.375 && adjLCratio <=  0.380) {
                        per_rank6 = 26;
                    }
                    else if (adjLCratio >  0.380 && adjLCratio <=  0.385) {
                        per_rank6 = 27;
                    }
                    else if (adjLCratio >  0.385 && adjLCratio <=  0.390) {
                        per_rank6 = 28;
                    }
                    else if (adjLCratio >  0.390 && adjLCratio <=  0.395) {
                        per_rank6 = 29;
                    }
                    else  {
                        per_rank6 = 30;
                    }
                }
                else if(adjLCratio <=  0.35  && adjLCratio >  0.30){
                    
                    
                    if (adjLCratio >  0.300 && adjLCratio <=  0.305) {
                        per_rank6 = 11;
                    }
                    else if (adjLCratio >  0.305 && adjLCratio <=  0.310) {
                        per_rank6 = 12;
                    }
                    else if (adjLCratio >  0.310 && adjLCratio <=  0.315) {
                        per_rank6 = 13;
                    }
                    else if (adjLCratio >  0.315 && adjLCratio <=  0.320) {
                        per_rank6 = 14;
                    }
                    else if (adjLCratio >  0.320 && adjLCratio <=  0.325) {
                        per_rank6 = 15;
                    }
                    else if (adjLCratio >  0.325 && adjLCratio <=  0.330) {
                        per_rank6 = 16;
                    }
                    else if (adjLCratio >  0.330 && adjLCratio <=  0.335) {
                        per_rank6 = 17;
                    }
                    else if (adjLCratio >  0.335 && adjLCratio <=  0.340) {
                        per_rank6 = 18;
                    }
                    else if (adjLCratio >  0.340 && adjLCratio <=  0.345) {
                        per_rank6 = 19;
                    }
                    else  {
                        per_rank6 = 20;
                    }

                }
                else if(adjLCratio <=  0.30  && adjLCratio >  0.25){
                    
                    
                    if (adjLCratio >  0.250 && adjLCratio <=  0.255) {
                        per_rank6 = 1;
                    }
                    else if (adjLCratio >  0.255 && adjLCratio <=  0.260) {
                        per_rank6 = 2;
                    }
                    else if (adjLCratio >  0.260 && adjLCratio <=  0.265) {
                        per_rank6 = 3;
                    }
                    else if (adjLCratio >  0.265 && adjLCratio <=  0.270) {
                        per_rank6 = 4;
                    }
                    else if (adjLCratio >  0.270 && adjLCratio <=  0.275) {
                        per_rank6 = 5;
                    }
                    else if (adjLCratio >  0.275 && adjLCratio <=  0.280) {
                        per_rank6 = 6;
                    }
                    else if (adjLCratio >  0.280 && adjLCratio <=  0.285) {
                        per_rank6 = 7;
                    }
                    else if (adjLCratio >  0.285 && adjLCratio <=  0.290) {
                        per_rank6 = 8;
                    }
                    else if (adjLCratio >  0.290 && adjLCratio <=  0.295) {
                        per_rank6 = 9;
                    }
                    else  {
                        per_rank6 = 10;
                    }
                }
                else if(adjLCratio <=  0.25){
                    per_rank6 = 0;
                }
                else if(adjLCratio >   0.800){
                    per_rank6 = 100;
                }
                
                 if(per_rank6 == 2222){
                    var lcRank  =  "--" ;                         
                    document.getElementById("lCurlRank").value = lcRank ;  // Rank %
                }
                else{                    
                    var lcRank = parseInt(per_rank6);                        
                    document.getElementById("lCurlRank").value = parseInt(lcRank) ;  // Rank %
                }
            }
             else{
                    per_rank6 = 2222;
                    var lcRank  =  "--" ;                         
                    document.getElementById("lCurlRank").value = lcRank ;  // Rank %
            }
        
      }
      // if client is Female
        else if(document.getElementById('gender_Female').checked) {

          if(adjLCratio > 0){
                
                
                if(adjLCratio <=  0.75  && adjLCratio >   0.65){
                    
                    if (adjLCratio >  0.65 && adjLCratio <=  0.66) {
                        per_rank6 = 91;
                    }
                    else if (adjLCratio >  0.66 && adjLCratio <=  0.67) {
                        per_rank6 = 92;
                    }
                    else if (adjLCratio >  0.67 && adjLCratio <=  0.68) {
                        per_rank6 = 93;
                    }
                    else if (adjLCratio >  0.68 && adjLCratio <=  0.69) {
                        per_rank6 = 94;
                    }
                    else if (adjLCratio >  0.69 && adjLCratio <=  0.70) {
                        per_rank6 = 95;
                    }
                    else if (adjLCratio >  0.70 && adjLCratio <=  0.71) {
                        per_rank6 = 96;
                    }
                    else if (adjLCratio >  0.71 && adjLCratio <=  0.72) {
                        per_rank6 = 97;
                    }
                    else if (adjLCratio >  0.72 && adjLCratio <=  0.73) {
                        per_rank6 = 98;
                    }
                    else if (adjLCratio >  0.73 && adjLCratio <=  0.74) {
                        per_rank6 = 99;
                    }
                    else  {
                        per_rank6 = 100;
                    }
                }
                if(adjLCratio <=  0.65  && adjLCratio >   0.60){
                    
                    if (adjLCratio >  0.60 && adjLCratio <=  0.605) {
                        per_rank6 = 81;
                    }
                    else if (adjLCratio >  0.605 && adjLCratio <=  0.610) {
                        per_rank6 = 82;
                    }
                    else if (adjLCratio >  0.610 && adjLCratio <=  0.615) {
                        per_rank6 = 83;
                    }
                    else if (adjLCratio >  0.615 && adjLCratio <=  0.620) {
                        per_rank6 = 84;
                    }
                    else if (adjLCratio >  0.620 && adjLCratio <=  0.625) {
                        per_rank6 = 85;
                    }
                    else if (adjLCratio >  0.625 && adjLCratio <=  0.630) {
                        per_rank6 = 96;
                    }
                    else if (adjLCratio >  0.630 && adjLCratio <=  0.635) {
                        per_rank6 = 87;
                    }
                    else if (adjLCratio >  0.635 && adjLCratio <=  0.640) {
                        per_rank6 = 88;
                    }
                    else if (adjLCratio >  0.640 && adjLCratio <=  0.645) {
                        per_rank6 = 89;
                    }
                    else  {
                        per_rank6 = 90;
                    }
                }
                if(adjLCratio <=  0.60  && adjLCratio >   0.55){
                    
                    if (adjLCratio >  0.550 && adjLCratio <=  0.555) {
                        per_rank6 = 71;
                    }
                    else if (adjLCratio >  0.555 && adjLCratio <=  0.560) {
                        per_rank6 = 72;
                    }
                    else if (adjLCratio >  0.560 && adjLCratio <=  0.565) {
                        per_rank6 = 73;
                    }
                    else if (adjLCratio >  0.565 && adjLCratio <=  0.570) {
                        per_rank6 = 74;
                    }
                    else if (adjLCratio >  0.570 && adjLCratio <=  0.575) {
                        per_rank6 = 75;
                    }
                    else if (adjLCratio >  0.575 && adjLCratio <=  0.580) {
                        per_rank6 = 76;
                    }
                    else if (adjLCratio >  0.580 && adjLCratio <=  0.585) {
                        per_rank6 = 77;
                    }
                    else if (adjLCratio >  0.585 && adjLCratio <=  0.590) {
                        per_rank6 = 78;
                    }
                    else if (adjLCratio >  0.590 && adjLCratio <=  0.595) {
                        per_rank6 = 79;
                    }
                    else  {
                        per_rank6 = 80;
                    }
                }
                else if(adjLCratio <=  0.55  && adjLCratio >   0.50){
                    
                    
                    if (adjLCratio >  0.50 && adjLCratio <=  0.505) {
                        per_rank6 = 61;
                    }
                    else if (adjLCratio >  0.505 && adjLCratio <=  0.510) {
                        per_rank6 = 62;
                    }
                    else if (adjLCratio >  0.510 && adjLCratio <=  0.515) {
                        per_rank6 = 63;
                    }
                    else if (adjLCratio >  0.515 && adjLCratio <=  0.520) {
                        per_rank6 = 64;
                    }
                    else if (adjLCratio >  0.520 && adjLCratio <=  0.525) {
                        per_rank6 = 65;
                    }
                    else if (adjLCratio >  0.525 && adjLCratio <=  0.530) {
                        per_rank6 = 66;
                    }
                    else if (adjLCratio >  0.530 && adjLCratio <=  0.535) {
                        per_rank6 = 67;
                    }
                    else if (adjLCratio >  0.535 && adjLCratio <=  0.540) {
                        per_rank6 = 68;
                    }
                    else if (adjLCratio >  0.540 && adjLCratio <=  0.545) {
                        per_rank6 = 69;
                    }
                    else  {
                        per_rank6 = 70;
                    }
                }
                else if(adjLCratio <=  0.50 && adjLCratio >   0.45){
                    
                    if (adjLCratio >  0.450 && adjLCratio <=  0.455) {
                        per_rank6 = 51;
                    }
                    else if (adjLCratio >  0.455 && adjLCratio <=  0.460) {
                        per_rank6 = 52;
                    }
                    else if (adjLCratio >  0.460 && adjLCratio <=  0.465) {
                        per_rank6 = 53;
                    }
                    else if (adjLCratio >  0.465 && adjLCratio <=  0.470) {
                        per_rank6 = 54;
                    }
                    else if (adjLCratio >  0.470 && adjLCratio <=  0.475) {
                        per_rank6 = 55;
                    }
                    else if (adjLCratio >  0.475 && adjLCratio <=  0.480) {
                        per_rank6 = 56;
                    }
                    else if (adjLCratio >  0.480 && adjLCratio <=  0.485) {
                        per_rank6 = 57;
                    }
                    else if (adjLCratio >  0.485 && adjLCratio <=  0.490) {
                        per_rank6 = 58;
                    }
                    else if (adjLCratio >  0.490 && adjLCratio <=  0.495) {
                        per_rank6 = 59;
                    }
                    else  {
                        per_rank6 = 60;
                    }
                }
                else if(adjLCratio <=  0.45 && adjLCratio >   0.40){
                    
                    if (adjLCratio >  0.40 && adjLCratio <=  0.405) {
                        per_rank6 = 41;
                    }
                    else if (adjLCratio >  0.405 && adjLCratio <=  0.410) {
                        per_rank6 = 42;
                    }
                    else if (adjLCratio >  0.410 && adjLCratio <=  0.415) {
                        per_rank6 = 43;
                    }
                    else if (adjLCratio >  0.415 && adjLCratio <=  0.420) {
                        per_rank6 = 44;
                    }
                    else if (adjLCratio >  0.420 && adjLCratio <=  0.425) {
                        per_rank6 = 45;
                    }
                    else if (adjLCratio >  0.425 && adjLCratio <=  0.430) {
                        per_rank6 = 46;
                    }
                    else if (adjLCratio >  0.430 && adjLCratio <=  0.435) {
                        per_rank6 = 47;
                    }
                    else if (adjLCratio >  0.435 && adjLCratio <=  0.440) {
                        per_rank6 = 48;
                    }
                    else if (adjLCratio >  0.440 && adjLCratio <=  0.445) {
                        per_rank6 = 49;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                }
                else if(adjLCratio <=  0.40 && adjLCratio >   0.35){
                    
                    if (adjLCratio >  0.35 && adjLCratio <=  0.355) {
                        per_rank6 = 31;
                    }
                    else if (adjLCratio >  0.355 && adjLCratio <=  0.360) {
                        per_rank6 = 32;
                    }
                    else if (adjLCratio >  0.360 && adjLCratio <=  0.365) {
                        per_rank6 = 33;
                    }
                    else if (adjLCratio >  0.365 && adjLCratio <=  0.370) {
                        per_rank6 = 34;
                    }
                    else if (adjLCratio >  0.370 && adjLCratio <=  0.375) {
                        per_rank6 = 35;
                    }
                    else if (adjLCratio >  0.375 && adjLCratio <=  0.380) {
                        per_rank6 = 36;
                    }
                    else if (adjLCratio >  0.380 && adjLCratio <=  0.385) {
                        per_rank6 = 37;
                    }
                    else if (adjLCratio >  0.385 && adjLCratio <=  0.390) {
                        per_rank6 = 38;
                    }
                    else if (adjLCratio >  0.390 && adjLCratio <=  0.395) {
                        per_rank6 = 39;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                    
                }
                else if(adjLCratio <=  0.35 && adjLCratio >   0.25){
                    
                    if (adjLCratio >  0.25 && adjLCratio <=  0.26) {
                        per_rank6 = 21;
                    }
                    else if (adjLCratio >  0.26 && adjLCratio <=  0.27) {
                        per_rank6 = 22;
                    }
                    else if (adjLCratio >  0.27 && adjLCratio <=  0.28) {
                        per_rank6 = 23;
                    }
                    else if (adjLCratio >  0.28 && adjLCratio <=  0.29) {
                        per_rank6 = 24;
                    }
                    else if (adjLCratio >  0.29 && adjLCratio <=  0.30) {
                        per_rank6 = 25;
                    }
                    else if (adjLCratio >  0.30 && adjLCratio <=  0.31) {
                        per_rank6 = 26;
                    }
                    else if (adjLCratio >  0.31 && adjLCratio <=  0.32) {
                        per_rank6 = 27;
                    }
                    else if (adjLCratio >  0.32 && adjLCratio <=  0.33) {
                        per_rank6 = 28;
                    }
                    else if (adjLCratio >  0.33 && adjLCratio <=  0.34) {
                        per_rank6 = 29;
                    }
                    else  {
                        per_rank6 = 30;
                    }
                    
                }
                else if(adjLCratio <=  0.25 && adjLCratio >   0.15){
                    
                    if (adjLCratio >  0.15 && adjLCratio <=  0.16) {
                        per_rank6 = 11;
                    }
                    else if (adjLCratio >  0.16 && adjLCratio <=  0.17) {
                        per_rank6 = 12;
                    }
                    else if (adjLCratio >  0.17 && adjLCratio <=  0.18) {
                        per_rank6 = 13;
                    }
                    else if (adjLCratio >  0.18 && adjLCratio <=  0.19) {
                        per_rank6 = 14;
                    }
                    else if (adjLCratio >  0.19 && adjLCratio <=  0.20) {
                        per_rank6 = 15;
                    }
                    else if (adjLCratio >  0.20 && adjLCratio <=  0.21) {
                        per_rank6 = 16;
                    }
                    else if (adjLCratio >  0.21 && adjLCratio <=  0.22) {
                        per_rank6 = 17;
                    }
                    else if (adjLCratio >  0.22 && adjLCratio <=  0.23) {
                        per_rank6 = 18;
                    }
                    else if (adjLCratio >  0.23 && adjLCratio <=  0.24) {
                        per_rank6 = 19;
                    }
                    else  {
                        per_rank6 = 20;
                    }
                    
                }
                else if(adjLCratio <=  0.15 && adjLCratio >   0.0){
                    
                    if (adjLCratio >  0.0 && adjLCratio <=  0.015) {
                        per_rank6 = 1;
                    }
                    else if (adjLCratio >  0.015 && adjLCratio <=  0.030) {
                        per_rank6 = 2;
                    }
                    else if (adjLCratio >  0.030 && adjLCratio <=  0.045) {
                        per_rank6 = 3;
                    }
                    else if (adjLCratio >  0.045 && adjLCratio <=  0.060) {
                        per_rank6 = 4;
                    }
                    else if (adjLCratio >  0.060 && adjLCratio <=  0.075) {
                        per_rank6 = 5;
                    }
                    else if (adjLCratio >  0.075 && adjLCratio <=  0.090) {
                        per_rank6 = 6;
                    }
                    else if (adjLCratio >  0.090 && adjLCratio <=  0.105) {
                        per_rank6 = 7;
                    }
                    else if (adjLCratio >  0.105 && adjLCratio <=  0.120) {
                        per_rank6 = 8;
                    }
                    else if (adjLCratio >  0.120 && adjLCratio <=  0.135) {
                        per_rank6 = 9;
                    }
                    else  {
                        per_rank6 = 10;
                    }
                    
                }
                else if(adjLCratio <=  0.0){
                    per_rank6 = 0;
                }
                else if(adjLCratio >   0.750){
                    per_rank6 = 100;
                }
                 if(per_rank6 == 2222){
                    var lcRank  =  "--" ;                         
                    document.getElementById("lCurlRank").value = lcRank ;  // Rank %
                }
                else{                    
                    var lcRank = parseInt(per_rank6);                        
                    document.getElementById("lCurlRank").value = parseInt(lcRank) ;  // Rank %
                }
            }
             else{
                    per_rank6 = 2222;
                    var lcRank  =  "--" ;                         
                    document.getElementById("lCurlRank").value = lcRank ;  // Rank %
            } 

        }
        
       }
       
            document.getElementById("per_rank6").value = parseInt(per_rank6) ;  // Rank6 %
            avgPerRank() ;
      
    }    
    
    function avgPerRank() 
    {        
        // get total rank %   
        var rank1 = document.getElementById("per_rank1").value ;  
        var rank2 = document.getElementById("per_rank2").value ;  
        var rank3 = document.getElementById("per_rank3").value ;  
        var rank4 = document.getElementById("per_rank4").value ;  
        var rank5 = document.getElementById("per_rank5").value ;  
        var rank6 = document.getElementById("per_rank6").value ;  
        var rank7 = document.getElementById("gripRank").value ; 
        
      //  alert(parseInt(rank1)); die;
        
        var count = 0;
        var s1 = 0;
        var s2 = 0;
        var s3 = 0;
        var s4 = 0;
        var s5 = 0;
        var s6 = 0;
        var s7 = 0;
                 
        if((parseFloat(rank1) >= 0) && parseFloat(rank1) != 2222 && (document.getElementById("bpRank").value) != "--" && (document.getElementById("bpRank").value) != "" ){           
            count = count + 1;
            s1 = rank1 ;
        }
        if((parseFloat(rank2) >= 0) && (parseFloat(rank2) != 2222) && (document.getElementById("acRank").value) != "--" && (document.getElementById("acRank").value) != "" ){
            count = count + 1;
            s2 = rank2 ;
        }
        if((parseFloat(rank3) >= 0) && (parseFloat(rank3) != 2222) && (document.getElementById("lPullRank").value) != "--" && (document.getElementById("lPullRank").value) != "" ){
            count = count + 1;
            s3 = rank3 ;
        }
        if((parseFloat(rank4) >= 0) && (parseFloat(rank4) != 2222) && (document.getElementById("lPressRank").value) != "--" && (document.getElementById("lPressRank").value) != "" ){
            count = count + 1;
            s4 = rank4 ;
        }
        if((parseFloat(rank5) >= 0) && (parseFloat(rank5) != 2222) && (document.getElementById("lExtRank").value) != "--" && (document.getElementById("lExtRank").value) != "" ){
            count = count + 1;
            s5 = rank5 ;
        }
        if((parseFloat(rank6) >= 0) && (parseFloat(rank6) != 2222) && (document.getElementById("lCurlRank").value) != "--" && (document.getElementById("lCurlRank").value) != "" ){
            count = count + 1;
            s6 = rank6 ;
        }
        if((rank7 >= 0) && (rank7 != 2222)){
            count = count + 1;
            s7 = rank7 ;
        }
        if(count == 0){
            count = 1;
        }        
         
         var perc = (parseFloat(s1) + parseFloat(s2) + parseFloat(s3) + parseFloat(s4) + parseFloat(s5) + parseFloat(s6) + parseFloat(s7)) / parseInt(count) ;
         document.getElementById("avgPerRank").value =  Math.floor(parseFloat(perc) * 10000) / 10000 ;   
        
    }


  function clear_fetch() 
  {   
     getLeftAverage();
     getRightAverage();
     benchPress();
     armCurl();
     leteralPulldown();
     legPress();
     legExtension();
     legCurl() ;
     avgPerRank() ;   
    
  }  
    
    
</script> 
</body>  
</html>
