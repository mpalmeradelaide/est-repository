<?php 
/* print_r($_SESSION['VO2max']);
echo $_SESSION['VO2rest'];
echo $_SESSION['VO2maxLM']; */

$VO2max=$_SESSION['VO2max'];
//$VO2max=40;
$gender=$_SESSION['user_gender'];
if(isset($_COOKIE['max_wl'])) { 
        //echo $_COOKIE['max_wl']; 
		$_SESSION['max_wl']=$_COOKIE['max_wl'];
    }


        	//To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
            $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
            $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
            $bodyfatdiv="block";    
            }
            else
            {
             $bodyfatdiv="none";    
            }
			$is_dynamic=$_SESSION['is_virtual']; 
			
  ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<script type="text/javascript">
	$(document).ready(function() {
		
		var gender='<?php echo $_SESSION['user_gender'];?>';
		
		
       /*  var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		  var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
		 $(".v_person").fadeTo( "slow" , 1, function() {}); 
		 $(".v_detail").toggle();
		} */
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	$(document).on('click','.menu_btn', function(){
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
		$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
    $(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
/*	 $(document).on('click','.vitual_btn', function(){
                  $("#vp_measured_vo2max").attr("action", "<?php echo base_url(); ?>index.php/Fitness/FitnessVirtualpersonGeneration");     
                  $("#vp_measured_vo2max").submit();       
         $(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
                return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();  
	});  */
	
	
	function clearclass()
	{
		//alert("aaaaaaaa");
		$("#con_protocol").removeClass("active");
		$("#conlabel").removeClass("active");
		$("#dis_protocol").removeClass("active");
		$("#dislabel").removeClass("active"); 
		$("#ergoclass").removeClass("active");
		$('#vo2maxvalue').html('');
		$('#speed').val('');
		$('#speedhid').val('');
		$('#workload').val(''); 
		
		$('#contprotocolspeed').val(''); 
		$('#contprotocolvo2').val(''); 
		$('#discontprotocolspeed').val(''); 
		$('#discontprotocolvo2').val(''); 
		$('#ergometer').val('');  
	}
	
  
	
	function edValueKeyPress()
    {
        var edValue = document.getElementById("speed");
        var s = edValue.value;
		        $('#workload').val(''); 
				$("#speedhid").val(s); 
				//$('#contprotocolspeed').val(s);  // hidden speed
				var protocolname=$("input[name='con_protocol']:checked"). val();
				if(protocolname == "contprotocol")
				{
					on_typevalue(protocolname,s);
				}
				if(protocolname == "discontprotocol")
				{
					on_typevalue(protocolname,s);
				}
    }
		
	
	
	
	function on_typevalue(protocolname, value)
	{
		var gender=$("input[name='gend']:checked"). val();		// get gender
		var workload=$('#workload').val();
		var weight='<?php echo $_SESSION['MASS'];?>';
		var vp_age='<?php echo $_SESSION['vp_age'];?>';
			//var spd= $('#speed').val();  
		if(protocolname == "contprotocol")
		{
			var spd = value;
			var vo2_adj= parseFloat( 15.754 - 2.3080 * parseFloat(spd) + 0.084821 * ( parseFloat(spd) * parseFloat(spd) ) );
			var vo2max_treadmill= parseFloat ( 1.5323 + 2.8453 * parseFloat(spd) + 0.019298 * Math.pow(parseFloat(spd),2) + parseFloat(vo2_adj) );
			vo2max_treadmill=parseFloat(vo2max_treadmill.toFixed(1));
			console.log(vo2max_treadmill);
			//$('#contprotocolvo2').val(vo2max_treadmill);     // hidden VO2 max
			$('#vo2maxvalue').html(vo2max_treadmill);
		}
		
		if(protocolname == "discontprotocol")
		{
			var spd = value;
			var vo2_adj= parseFloat( 15.754 - 2.3080 * parseFloat(spd) + 0.084821 * ( parseFloat(spd) * parseFloat(spd) ) );
			var vo2max_distreadmill= parseFloat( 1.5323 + 2.8453 * parseFloat(spd) + 0.019298 *  Math.pow(parseFloat(spd),2) - parseFloat(vo2_adj)  );
			vo2max_distreadmill=parseFloat(vo2max_distreadmill.toFixed(1));
			console.log(vo2max_distreadmill);
			//$('#discontprotocolvo2').val(vo2max_distreadmill);
			$('#vo2maxvalue').html(vo2max_distreadmill);
		}
	}
	
	
	
	
	
	
	
	function clickergometerButton()
	{
		 $('#speed').val('');
		 $('#speedhid').val('');
		 $("input:radio").parent().removeClass("active"); 
		 $("input:radio:checked").parent().addClass("active"); 
		 var VO2max_ergo="";
		 // calculate Predicted watts
		 //Predicted [max] WATTS = (VO2maxLM – VO2rest)/ 0.012 
		 var vo2rest='<?php echo $_SESSION['VO2rest'];?>';  
		 var vo2maxlm='<?php echo $_SESSION['VO2maxLM'];?>';
		 var weight='<?php echo $_SESSION['MASS'];?>';
		 var vp_age='<?php echo $_SESSION['vp_age'];?>';
		 var predicted_watts = parseFloat( (vo2maxlm - vo2rest) / 0.012 );
		 var gender=$("input[name='gend']:checked"). val();		// get gender
		 
		 
		 var workload_generated = $('#ergometer').val();
		 
		 if(workload_generated != "")
		 {
			 $('#workload').val(workload_generated);			
		 }
		 else{		 
				 if(gender == 'M' || gender == 'Male')
				 {
				  // Males FINAL Max Watts = Predicted WATTS * [(100+(0.1234*AGE - 4.072)) / 100]		  
				  var maxwatts = (predicted_watts) * [(100 + (0.1234 * vp_age - 4.072 ))/100] ;       // Adjustment workload
				  $('#workload').val(maxwatts.toFixed(0));
				  var workload = $('#workload').val();
				  $('#ergometer').val(workload);     
				  var VO2max_ergo= ((( parseFloat(workload)* 10.51 ) + ( parseFloat(weight) * 6.35 ) - ( vp_age * 10.49 ) + 519.3 ) / parseFloat(weight)) ;
				 }
				 
				  if(gender == 'F' || gender == 'Female')
				 {
				  // Females FINAL Max Watts = Predicted WATTS * [(100+(0.1045*AGE + 2.4167)) / 100]  
				  var maxwatts = parseFloat ( parseFloat(predicted_watts) * [ (100 + (0.1045 * vp_age + 2.4167)) / 100 ] );   // Adjustment workload
				  $('#workload').val(maxwatts.toFixed(0));				  
				  var workload = $('#workload').val();
				  $('#ergometer').val(workload);     
				  var VO2max_ergo= (( ( parseFloat(workload)* 9.39 ) + ( parseFloat(weight) * 7.7 ) - ( vp_age * 5.88 ) + 136.7 ) / parseFloat(weight));
				 }
				 
				 VO2max_ergo=parseFloat(VO2max_ergo.toFixed(1));
				 console.log(VO2max_ergo);
				  // store generated workload
				 $('#vo2maxvalue').html(VO2max_ergo);
		    }
	}
	
	
/* $(document).on('mouseenter','.check_tab label', function(){
	$(this).addClass("active");
});	
$(document).on('mouseleave','.check_tab label', function(){
	$(this).removeClass("active");
}); */	


function clicktredmillcontButton()
	{
		 $('#workload').val(''); 
		 $("input:radio").parent().removeClass("active"); 
		 $("input:radio:checked").parent().addClass("active"); 
		 // Random Speed Calculate
		 var vo2max = '<?php echo $VO2max; ?>';
		 var inputted_speed=$("#speedhid").val();
		 var spd=$('#speed').val();
		 var generated_speed = $('#contprotocolspeed').val();
		 
		 if( generated_speed != "" )
		 {
			 var speed=$('#contprotocolspeed').val(); 
			 var contvo2max= $('#contprotocolvo2').val();
			 
			 $('#speed').val(speed); 
			 $('#vo2maxvalue').html(contvo2max); 
		 }
		 else
		 {
		 //alert('autogenerated speed alert'); 
		 var spd=$('#speed').val();
		 var is_virtual='<?php echo $is_dynamic; ?>';		
			 $.ajax({
			        type: "POST",
					url: '<?php echo base_url(); ?>index.php/Fitness/calcu_biketest',
					data:{'vo2max': vo2max },
					success: function(data){
						        var vo2_adj=data;
								console.log("vo2_adj"+vo2_adj);
								
								// Final VO2max after adjustment
								console.log("Vo2adj"+vo2_adj);
								
								var vo2max_final = parseFloat(vo2max) + parseFloat(vo2_adj);			
								console.log("vo2max_final"+vo2max_final);
								
								var TREADMILLSPEEDcont = parseFloat ( -0.0021 * Math.pow(vo2max_final,2) + (0.4875 * vo2max_final) - 3.9025 );
								TREADMILLSPEEDcont = TREADMILLSPEEDcont.toFixed(1); 
								console.log("Continous Speed"+TREADMILLSPEEDcont); 
								$('#contprotocolspeed').val(TREADMILLSPEEDcont);  // hidden speed
								$('#speed').val(TREADMILLSPEEDcont);  //  speed
								
								 var gender=$("input[name='gend']:checked"). val();		// get gender     
								 var workload=$('#workload').val();
								 var weight='<?php echo $_SESSION['MASS'];?>';
								 var vp_age='<?php echo $_SESSION['vp_age'];?>';
								 //var spd= $('#speed').val();  
								 var spd = TREADMILLSPEEDcont;
								 console.log("Speed"+spd);
								 
								 var vo2_adj= parseFloat( 15.754 - 2.3080 * parseFloat(spd) + 0.084821 * ( parseFloat(spd) * parseFloat(spd) ) );
								 var vo2max_treadmill= parseFloat ( 1.5323 + 2.8453 * parseFloat(spd) + 0.019298 * Math.pow(parseFloat(spd),2) + parseFloat(vo2_adj) );
								 
								 vo2max_treadmill=parseFloat(vo2max_treadmill.toFixed(1));
								 console.log("Max vo2"+vo2max_treadmill);
								 $('#contprotocolvo2').val(vo2max_treadmill);     // hidden VO2 max
								 $('#vo2maxvalue').html(vo2max_treadmill);
					}
			 });		    
	      }
		
}
	
	
	
	function clicktredmilldiscontButton()
	{
		 $('#workload').val(''); 
		 var vo2max = '<?php echo $VO2max; ?>';
		 var inputted_speed=$("#speedhid").val();       // Get inputted value 
		 $("input:radio").parent().removeClass("active"); 
		 $("input:radio:checked").parent().addClass("active"); 
		 
		 var spd=$('#speed').val();
		 var generated_speed = $('#discontprotocolspeed').val();
		 if( generated_speed != "" )
		 {
			 var speed=$('#discontprotocolspeed').val(); 
			 var discontvo2max= $('#discontprotocolvo2').val();
			 
			 $('#speed').val(speed); 
			 $('#vo2maxvalue').html(discontvo2max); 
		 }
		
		else
		{
		 $.ajax({
			type: "POST",
			url: '<?php echo base_url(); ?>index.php/Fitness/calcu_biketest',
			data:{'vo2max': vo2max },
			success: function(data){
				                 console.log(data);
				                 var vo2_adj=data;
								// Final VO2max after adjustment
								var vo2max_final = parseFloat(vo2max) + parseFloat(vo2_adj);
								 //TREADMILLSPEEDcont =  0.00124* VO2max^2  + 0.20892*VO2max + 2.06706
								var TREADMILLSPEEDdistcont = parseFloat( 0.00124 * Math.pow(vo2max_final,2)  + (0.20892 * vo2max_final) + 2.06706 );
								console.log(TREADMILLSPEEDdistcont); 
								TREADMILLSPEEDdistcont = TREADMILLSPEEDdistcont.toFixed(1);        
                                
								$('#discontprotocolspeed').val(TREADMILLSPEEDdistcont);  // hidden speed
								$('#speed').val(TREADMILLSPEEDdistcont);  // speed
								
								 var spd = TREADMILLSPEEDdistcont;
								 if( spd == "" || spd == undefined )
								 {
									 alert("Please Enter Speed For Treadmill Dis-Continous Protocol");
									 return false;
								 }
								 else
								 {
									 var gender=$("input[name='gend']:checked"). val();		// get gender
									 //alert(gender);
									 var workload=$('#workload').val();
									 var weight='<?php echo $_SESSION['MASS'];?>';
									 var vp_age='<?php echo $_SESSION['vp_age'];?>';
									 var vo2_adj= parseFloat( 15.754 - 2.3080 * parseFloat(spd) + 0.084821 * ( parseFloat(spd) * parseFloat(spd) ) );
									 var vo2max_distreadmill= parseFloat( 1.5323 + 2.8453 * parseFloat(spd) + 0.019298 *  Math.pow(parseFloat(spd),2) - parseFloat(vo2_adj)  );
									 
									 vo2max_distreadmill=parseFloat(vo2max_distreadmill.toFixed(1));
									 console.log(vo2max_distreadmill);
									 $('#discontprotocolvo2').val(vo2max_distreadmill);
									 $('#vo2maxvalue').html(vo2max_distreadmill);
								 }
			           }
		 });	
	}
}	
	 
	
	
	
	
	
</script>
<body>
 
 <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['vp_age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],2);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
   
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container green_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit"><img src="<?php echo "$base/assets/images/"?>back_green.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Fitness Testing</h3>
            <p>Predicting VO<sub>2max</sub>using maximal-effort treadmill or bike tests</p>
        </div>
        
        
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_green.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_green.jpg"></a>
        </div>
		 <!--div class="orng_box_btn f_right">
                <form name="vpform" id="vp_measured_vo2max" method="post"> 
                    <a href="#" class="vitual_btn"><img src="<?php echo "$base/assets/images/" ?>virtual_icon.png" style="margin-top: 12px;"></a>
                    <input type="hidden" name="vptype" value="vp_measured_vo2max">
                </form>
            </div-->

            <div class="vitual_div">
            </div>    
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
        	<div class="info_block_head">Predicting VO2max using maximal-effort treadmill or bike tests</div>
            <h3>Maximal CYCLE ERGOMETER test for predicting aerobic power (VO2max)</h3>
			<br>
			<p><strong>Information:</strong> The protocol begins with 100 W and then every minute the resistance goes up by 15 W [note: athletes can begin a little higher and increase by 15 W] until the person cannot complete the final minute. The final (or highest if not the final) completed workload (must be a full 1-minute duration) is entered into the 'Final workload' field on the screen.
</p>
 <br>
 <h3>Maximal TREADMILL test for predicting aerobic power (VO2max)</h3>
<br>
<p><strong>Information:</strong> There are 2 variations of the tests using a treadmill to predict VO2max. Both variations of the treadmill test consist of 1-minute stages of running at 3% incline. The 3% incline remains for the entire test under both protocols. The treadmill speed usually begins at between about 8-12 km/hr (depending on the expected level of fitness) and is increased in 1 km/hr increments up to the maximal level where the person cannot make a complete minute. Completing 30 seconds on the last stage means you add 0.5 km/hr to the previously completed stage. For example, completing 30 seconds at 17 km/hr = final speed 16.5 km/hr. Enter this value into the 'Final speed' field on the screen.
 
The variations are either (1) a CONTINUOUS test where there is no break and the runner continuous to run until volitional fatigue, or (2) a DISCONTINUOUS test [best for field sport athletes] where there are scheduled breaks as follows: there is a 1-minute rest after the first 3 stages and then again after each additional 2 stages. For example, run for 1 minute at each of these speeds 10, 11, 12 km/hr, then rest 1 minute, then run for 1 minute at each of these speeds 13, 14, then rest 1 minute, then run for 1 minute at each of these speeds 15, 16, then rest 1 minute etc until volitional fatigue.​ Enter the final speed into the 'Final speed' field on the screen.​</p>    
            <div class="info_block_foot">
                <a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                  
        </div> 
    
    </div>
</div>

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
            	<li><a href="#" id="anaerobic_strength"><img src="<?php echo "$base/assets/images/"?>icon_anaerobic.png"> Anaerobic strength and power</a>
                	<ul class="sub_menu">
                    	<li><a href="<?php echo site_url('Fitness/vertical_jump'); ?>">vertical jump</a></li>
                        <li><a href="<?php echo site_url('Fitness/flight_time'); ?>">flight:contact time</a></li>
                        <li><a href="<?php echo site_url('Fitness/peak_power'); ?>">peak power [W]</a></li>
                        <li><a href="<?php echo site_url('Fitness/strength'); ?>">strength tests</a></li>
                    </ul>
                </li>
                <li><a href="#" id="anaerobic_capacity"><img src="<?php echo "$base/assets/images/"?>icon_capacity.png"> Anaerobic capacity</a>
                	<ul class="sub_menu">
                      <li><a href="<?php echo site_url('Fitness/total_work_done_30s'); ?>">30 s total work [kJ]</a></li>
                    </ul>
                </li>
                <li><a href="#" id="aerobic_fitness"><img src="<?php echo "$base/assets/images/"?>icon_fitness.png"> Aerobic fitness</a>
                	<ul class="sub_menu">
                    	<li><a href="#" id="VO2max">VO<sub>2max</sub> [mL/kg/min]</a>
                        	<ul class="inner_sub_menu">
                                <li><a href="<?php echo site_url('Fitness/submaximal_test'); ?>">submaximal tests</a></li>
                                <li><a href="#" id="max-txt">maximal tests</a>
									<ul class="inner_sub_menu2">
										<li><a href="<?php echo site_url('Fitness/shuttle_test'); ?>">20 m shuttle test</a></li>
										 <li><a href="<?php echo site_url('Fitness/bike_test'); ?>">Predicting VO2max using maximal-effort treadmill or bike tests</a></li>
									</ul>
								</li>
								
                                <li><a href="<?php echo site_url('Fitness/v02max_test'); ?>">measured VO<sub>2max</sub></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('Fitness/lactate_threshold'); ?>">lactate threshold</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Predicting VO2max using maximal-effort treadmill or bike tests</div>
        	<div class="field_row verticle_field_row"> 
                <div class="field_50">
                	<div class="field_24">
                		<label>Gender</label>
					</div>
                	<div class="field_24">
                		            <?php $radio_is_checked = ( ($gender === 'M' || $gender === 'Male' ) ) ? "checked" : "N";
                                    echo form_radio(array("name" => "gend", "id" => "option_M", "value" => "M", "class" => "gender", 'checked' => ($radio_is_checked === "N") ? "" : "checked" ,'onclick'=>'clearclass()'));
                                    ?> 
									<label for="option_M"><span style="margin-right: 5px;"></span>Male</label>
                                    <?php echo form_radio(array("name" => "gend", "id" => "option_F", "value" => "F", "class" => "gender", 'checked' => ($radio_is_checked === "N") ? "checked" : "" ,'onclick'=>'clearclass()')); ?> 
									<label for="option_F"><span style="margin-right: 5px;"></span>Female</label>  
					</div>
                </div>
             </div>
       
       		<div class="field_row verticle_field_row">
			  <div class="field_24">
				<label>Body mass [kg]</label>
				<input type="text" name="weight" id="weight" value="<?php echo $_SESSION['MASS'];?> ">
			  </div>
			  <div class="field_24">
				<label>Age [yr]</label>
				<input type="text" name="age" id="age" value="<?php echo $_SESSION['vp_age']; ?>">
			  </div>
			  
			  <input type="hidden" name="contprotocolspeed" id="contprotocolspeed">
			  <input type="hidden" name="contprotocolvo2" id="contprotocolvo2">
			  
			  
			  <input type="hidden" name="discontprotocolspeed" id="discontprotocolspeed">
			  <input type="hidden" name="discontprotocolvo2" id="discontprotocolvo2">
			  
			   <input type="hidden" name="ergometer" id="ergometer">
			  
			</div>
        
        	<div class="field_row verticle_field_row">
        		<div class="field_50 check_tab position_r fld50_ln">
					<div class="check_head">Treadmill</div><br>
					
					<label for="con_protocol" id="conlabel"><input type="radio" name="con_protocol" id="con_protocol" onclick="clicktredmillcontButton()" value="contprotocol">Continuous protocol</label>
					<label for="dis_protocol" id="dislabel"><input type="radio" name="con_protocol" id="dis_protocol" onclick="clicktredmilldiscontButton()" value="discontprotocol">Discontinuous protocol</label>
				</div>
				<div class="field_50 check_tab position_r">
					<div class="check_head">Cycle ergometer</div><br>
					
					<label for="dis_protocol_cycle" id="ergoclass"><input type="radio" name="con_protocol" id="dis_protocol_cycle" onclick="clickergometerButton()" >Continuous protocol</label>
				</div>
			</div>
        
        	<div class="field_row verticle_field_row" style="border: 0;">
			  <div class="field_24" style="text-align: center; margin-left: 14%;">                          	
				<label>Final speed [km/hr]</label>
				<input type="text" name="speed" id="speed" value="" onchange='edValueKeyPress()'>
				<input type="hidden" name="speedhid" id="speedhid" value="">
			  </div>
			  <div class="field_24 f_right" style="text-align: center; margin-right: 12%;">
				<label>Final workload [W]</label>
				<input type="text" name="workload" id="workload"  value="">
			  </div>
			</div>
        
			<div class="health_tag" style="width: 340px; font-size: 16px;">Predicted VO<sub>2max</sub> = <span id="vo2maxvalue"></span> (mL/kg/min)</div>

        </div>
    </div>
    <?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	
	window.onload = function() {
	     var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
		$("#vo2Max_value").val(<?php echo $_SESSION['VO2max'];?>);  
		}
	  
	};	

	$(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
    });	
	
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script> 

</body>
</html>
