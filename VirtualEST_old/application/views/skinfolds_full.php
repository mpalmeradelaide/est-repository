<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });	
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>
<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Restricted Profile / Skinfold analysis / Skinfold Map</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>    
    </div>
</div>

<div class="wrapper">
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	
    <div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
     <!--Start right --> 
        <div class="right-section right-section_new">
            <div class="right-head" style="margin-bottom:0;">Skinfold analysis</div> 
            <table class="graph_table" width="100%">
              <tbody>
                
                     <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>">
                     <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                     <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){echo "Male" ;}elseif($_SESSION['user_gender'] == "F"){echo "Female" ;}?>"> 
                    <tr>                
                    <td colspan="3" align="center" style="position:relative;"> 
                        <table width="100%">
                          <tbody> 
                            <tr> 
                              <td valign="top" width="30%">
                                    <div class="skinfold_head">sum of skinfolds</div>
                                    
                                    <table width="100%" class="tabel_bord tabel_bord_skinfold" cellspacing="0">
                                        <thead>
                                        <tr>	
                                          <td colspan="2">Skinfolds</td>
                                          <td>mm</td>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "tricep_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "tricep_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["triceps"] ; ?>">&nbsp;</div> </td> 	
                                          <td>Triceps</td>
                                          <td><input type="text" id="triceps" name="triceps" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["triceps"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "subscapular_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "subscapular_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["subscapular"] ; ?>">&nbsp;</div></td> 	
                                          <td>Subscapular</td>
                                          <td><input type="text" id="subscapular" name="subscapular" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["subscapular"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "biceps_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "biceps_female" ;}?>" onclick="changeState(this)"  value="<?php echo $skinfolds_Values["biceps"] ; ?>">&nbsp;</div></td> 	
                                          <td>Biceps</td>
                                          <td><input type="text" id="biceps" name="biceps" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["biceps"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "iliac_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "iliac_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["iliac_crest"] ; ?>">&nbsp;</div></td> 	
                                          <td>Iliac crest</td>
                                          <td><input type="text" id="iliac_crest" name="iliac_crest" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["iliac_crest"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "supraspinale_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "supraspinale_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["supraspinale"] ; ?>">&nbsp;</div></td> 	
                                          <td>Supraspinale</td>
                                          <td><input type="text" id="supraspinale" name="supraspinale" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["supraspinale"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "abdominal_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "abdominal_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["abdominal"] ; ?>">&nbsp;</div></td> 	
                                          <td>Abdominal</td>
                                          <td><input type="text" id="abdominal" name="abdominal" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["abdominal"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "thigh_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "thigh_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["thigh"] ; ?>">&nbsp;</div></td> 	
                                          <td>Front thigh</td>
                                          <td><input type="text" id="thigh" name="thigh" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["thigh"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="green_circle" id="<?php if($_SESSION['user_gender'] == "M"){echo "calf_male" ;}elseif($_SESSION['user_gender'] == "F"){echo "calf_female" ;}?>" onclick="changeState(this)" value="<?php echo $skinfolds_Values["calf"] ; ?>">&nbsp;</div></td> 	
                                          <td>Medial calf</td>
                                          <td><input type="text" id="calf" name="calf" class="cus-input width_60" value="<?php echo $skinfolds_Values["calf"] ; ?>"> </td>
                                        </tr>
                                        <tr>
                                          <td><div class="red_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["mid_axilla"] ; ?>">&nbsp;</div></td> 	
                                          <td>Mid-axilla</td>
                                          <td><input type="text" id="mid_axilla" name="mid_axilla" class="cus-input width_60" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value="<?php echo $skinfolds_Values["mid_axilla"] ; ?>"> </td>
                                        </tr>                                
                                      </tbody>
                                </table>
                              </td> 
                              <td valign="top">
                                    <div class="skinfold_head">population norms</div> 
                                    <div class="diag_chart">
                                        <div class="graph">
                                          
                                          <ul class="bottom_values">
                                            <li><span>1</span></li>
                                            <li><span>5</span></li>
                                            <li><span>25</span></li>
                                            <li><span>50</span></li>
                                            <li><span>75</span></li>
                                            <li><span>90</span></li>
                                            <li><span>95</span></li>
                                            <li style="float:right; width:auto;"><span><img src="<?php echo "$base/assets/images/"?>right_arrow.png"> &nbsp; Percentile</span></li>
                                          </ul>
                                                
                                          <ul class="bottom_values bottom_values_points">
                                            <li>
                                              <div class="blue_dot green_circle" id="plot1" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot2" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot3" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot4" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot5" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot6" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot7" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot8" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot9" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot10" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot11" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot12" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot13" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot14" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot15" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot16" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot17" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot18" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot19" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot20" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot21" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot22" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot23" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot24" style="visibility:hidden">&nbsp;</div>
                                              <div class="blue_dot green_circle" id="plot25" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                            <li>
                                              
                                            </li>
                                            <li>
                                              <div class="blue_dot green_circle" id="plot25" style="visibility:hidden">&nbsp;</div>
                                            </li>
                                          </ul>      
                                          
                                          <div class="graph_pulse_box"><img src="http://115.112.118.252/healthscreen//assets/images//pulse2.png" alt=""></div>
                                          <div class="percentile_box">
                                            <input type="text" id="percentile" name="percentile" class="cus-input width_60"> %'ile
                                          </div>
                                        </div>
                                    
                                    </div>
                                    
                                    <button id="plot" name="plot" class="lite_btn f_right btn_vio" style="position: absolute; bottom: 0; right: 0;">Skinfold Map</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </td>
                </tr>
                
                <tr>
                  <td align="left">
                    <table class="sigma_table">
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="three_Sites"><span><div id="sum3" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a></td>
                            <td align="left" style="width:95px;">&Sigma; 3 skinfolds</td> <input type="hidden" id="three_last" name="three_last" value="">
                            <td style="width: 100px;"><input type="text" id="three_Sum" name="three_Sum" class="cus-input" value="" readonly> <label>mm</label></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="four_Sites"><span><div id="sum4" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a></td>
                            <td align="left">&Sigma; 4 skinfolds</td> <input type="hidden" id="four_last" name="four_last" value="">
                            <td><input type="text" id="four_Sum" name="four_Sum" class="cus-input" value="" readonly></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="five_Sites"><span><div id="sum5" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                            <td align="left">&Sigma; 5 skinfolds</td> <input type="hidden" id="five_last" name="five_last" value="">
                            <td><input type="text" id="five_Sum" name="five_Sum" class="cus-input" value="" readonly></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="six_Sites"><span><div id="sum6" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                            <td align="left">&Sigma; 6 skinfolds</td> <input type="hidden" id="six_last" name="six_last" value="">
                            <td><input type="text" id="six_Sum" name="six_Sum" class="cus-input" value="" readonly></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="seven_Sites"><span><div id="sum7" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                            <td align="left">&Sigma; 7 skinfolds</td> <input type="hidden" id="seven_last" name="seven_last" value="">
                            <td><input type="text" id="seven_Sum" name="seven_Sum" class="cus-input" value="" readonly></td>
                        </tr>
                        <tr>
                            <td><a href="#" class="rest_bot_btns" id="eight_Sites"><span><div id="sum8"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                            <td align="left">&Sigma; 8 skinfolds</td> <input type="hidden" id="eight_last" name="eight_last" value="">
                            <td><input type="text" id="eight_Sum" name="eight_Sum" class="cus-input" value="" readonly></td>
                        </tr>
                    </table>
                  </td>  
                  
                  <td align="right" colspan="2" valign="bottom">      
                     
                  </td>        
                </tr>
                <tr>
                  <td align="left" colspan="3">&nbsp;</td>        
                </tr>
              </tbody>
            </table>
        </div>
	</div>
    
    	<div style="display:none;">
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
        </div>
             <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">  
    
   if(document.getElementById("gender").value == "Male")
        { 
            if(document.getElementById("age_range").value == "18-29")
            {             
              var tricep_male = ['3.96','4.23','4.77','5.39','6.49','7.66','8.78','9.98','11.35','13.03','15.41','19.46','23.55','30.26','33.29','33.29'] ;
              var biceps_male = ['2.37','2.44','2.57','2.76','3.04','3.48','3.84','4.12','4.77','5.69','6.65','10.79','11.37','13.28','21.23','21.23'] ;
              var subscapular_male = ['6.32','6.77','7.44','8.24','9.47','10.81','12.33','13.94','16.24','18.76','22.09','26.94','30.72','34.13','36.8','36.8'] ;
              var abdominal_male = ['4.86','4.92','6.02','6.61','8.40','11.14','14.11','18.60','23.06','28.54','31.65','37.28','39.15','41.22','41.53','41.53'] ;
              var calf_male = ['2.99','3.11','3.91','4.22','5.30','6.47','7.14','7.88','9.01','11.45','14.27','15.87','17.83','32.15','37.40','37.40'] ;
              var supraspinale_male = ['3.54','3.86','3.98','4.19','4.74','5.32','6.24','6.96','8.47','10.14','12.27','16.32','22.88','27.32','35.63','35.63'] ;
              var thigh_male = ['4.11','4.49','5.06','5.72','6.98','8.02','9.11','10.49','12.16','13.92','16.27','20.33','25.18','30.18','34.04','34.04'] ;     
              var iliac_male = ['4.33','4.73','5.46','6.31','8.23','10.9','13.73','17.13','20.96','25.51','29.93','34.76','38.02','40.46','41.87','41.87'] ;
            }
            else if(document.getElementById("age_range").value == "30-39")
            {              
              var tricep_male = ['3.72','4.16','4.87','5.79','7.36','8.51','9.8','10.83','12.24','14.11','16.05','20.28','24.91','31.03','34.65','34.65'] ;
              var biceps_male = ['2.21','2.45','2.72','3.01','3.87','4.22','4.75','5.23','5.71','6.38','7.15','9.98','11.30','12.85','19.25','19.25'] ;
              var subscapular_male = ['6.81','7.14','8.07','9.22','11.65','13.64','15.67','17.96','19.94','22.38','25.22','29.48','32.71','37.88','40.9','40.9'] ;
              var abdominal_male = ['6.08','6.63','7.46','10.91','18.14','20.39','23.70','26.29','29.48','33.27','35.70','38.44','39.06','40.25','41.08','41.08'] ;
              var calf_male = ['3.86','4.08','4.46','5.23','6.26','6.99','7.83','8.66','9.98','11.82','13.03','14.91','16.80','22.90','28.74','28.74'] ;
              var supraspinale_male = ['3.35','3.73','4.01','5.21','6.55','7.28','8.49','9.80','11.01','12.18','14.46','17.30','20.85','25.65','33.36','33.36'] ;
              var thigh_male = ['3.73','4.25','5.25','6.17','7.66','8.84','10.13','11.57','13.13','14.65','17.58','22.06','26.73','32.21','36.37','36.37'] ; 
              var iliac_male = ['4.23','4.74','5.72','5.78','11.41','15.59','19.41','22.44','25.64','28.74','32.28','36.04','38.45','41.09','43.74','43.74'] ;
            }
            else if(document.getElementById("age_range").value == "40-49")
            {              
              var tricep_male = ['3.85','4.21','5.27','6.46','7.7','7.9','10.25','11.47','12.76','14.29','16.63','21.33','24.42','28.98','31.64','31.64'] ;
              var biceps_male = ['2.47','2.73','3.16','3.64','4.28','4.81','5.71','5.80','6.45','7.13','8.42','10.28','12.58','17.14','19.80','19.80'] ;
              var subscapular_male = ['6.72','7.4','8.58','10.52','13.59','15.46','17.56','19.22','21.34','23.41','26.37','30.75','34.09','37.01','39.06','39.06'] ;
              var abdominal_male = ['6.19','7.40','11.19','13.30','18.62','22.72','25.84','27.54','31.58','34.56','36.78','39.26','40.90','41.79','42.15','42.15'] ;
              var calf_male = ['4.02','4.39','5.01','5.55','7.04','8.06','8.99','9.77','11.27','12.44','13.79','17.14','19.62','24.44','28.56','28.56'] ;
              var supraspinale_male = ['3.62','4.23','5.42','5.81','7.19','7.93','8.97','10.00','11.38','12.93','15.14','19.13','25.47','29.58','31.33','31.33'] ;
              var thigh_male = ['3.98','4.35','5.34','6.54','7.91','9.18','10.43','11.71','13.19','14.79','17.33','21.95','27.35','34.12','38.03','38.03'] ;              
              var iliac_male = ['4.44','5.01','7.26','9.75','13.98','17.17','20.32','23.16','26.1','28.79','32.22','36.26','38.75','40.94','42.56','42.56'] ;
            }
            else if(document.getElementById("age_range").value == "50-59")
            {               
              var tricep_male = ['4.42','4.87','5.88','6.69','8.13','9.32','10.39','11.74','12.96','14.43','16.8','21.23','25.39','31.99','35.13','35.13'] ;
              var biceps_male = ['2.20','2.40','3.04','3.65','4.32','4.86','5.25','6.08','6.69','7.29','8.19','9.20','11.35','12.40','16.84','16.84'] ;
              var subscapular_male = ['6.87','7.84','9.43','11.36','13.61','16.22','18.07','19.78','21.77','24.02','26.79','30.62','33.42','37.51','39.89','39.89'] ;
              var abdominal_male = ['7.43','8.23','10.62','14.46','17.15','20.58','22.42','26.80','29.97','33.45','35.47','38.28','40.09','41.07','42.09','42.09'] ;
              var calf_male = ['3.89','4.13','4.88','5.74','6.54','7.28','8.44','9.75','10.90','11.82','12.91','15.05','17.49','27.40','35.47','35.47'] ;
              var supraspinale_male = ['4.53','4.60','5.00','5.73','7.00','7.63','8.59','9.57','10.79','12.64','14.63','18.33','20.80','23.29','30.17','30.17'] ;
              var thigh_male = ['4.06','4.75','5.57','6.6','8.16','9.28','10.37','11.67','13.27','15.24','17.96','23.56','28.98','33.86','37.77','37.77'] ;   
              var iliac_male = ['5.14','6.08','7.51','10.18','13.86','17.37','19.67','22.1','24.39','27.41','30.32','34.22','36.8','40.29','41.94','41.94'] ;
            }
            else if(document.getElementById("age_range").value == "60-69")
            {              
              var tricep_male = ['4.38','4.91','5.87','7.05','8.63','9.83','10.89','11.82','12.98','14.68','16.91','21.95','25.45','30.21','32.78','32.78'] ;
              var biceps_male = ['2.76','2.82','3.26','3.66','4.44','5.03','5.63','6.07','6.58','7.33','7.91','9.95','13.10','18.33','20.85','20.85'] ;
              var subscapular_male = ['6.68','7.57','9.58','11.63','14.33','16.48','18.28','20.31','22.27','24.44','27.46','31.36','35.1','38.02','39.81','39.81'] ;
              var abdominal_male = ['7.17','8.02','13.17','15.89','19.40','20.75','22.92','24.79','27.46','30.51','33.54','38.05','39.61','40.58','41.52','41.52'] ;
              var calf_male = ['3.59','3.79','4.49','5.63','6.70','7.73','8.66','9.27','10.07','11.18','12.74','15.09','18.24','23.42','28.00','28.00'] ;
              var supraspinale_male = ['3.59','4.04','4.78','6.13','7.54','8.45','9.17','10.08','11.94','13.08','14.51','16.98','21.19','25.67','26.88','26.88'] ;
              var thigh_male = ['4.44','5.11','5.83','6.62','7.91','9.12','10.34','11.74','13.06','15.14','18.08','22.71','27.76','34.61','38.22','38.22'] ; 
              var iliac_male = ['4.74','5.45','7.95','9.96','13.33','15.74','18.19','20.71','23.5','25.65','29.05','33.56','36.67','39.4','41.39','41.39'] ;
            }
            else if(document.getElementById("age_range").value == "70+")
            {              
              var tricep_male = ['4.26','4.76','5.57','6.69','8.09','9.13','10.42','11.46','12.47','13.77','15.79','19.39','23.36','28.68','31.46','31.46'] ;
              var biceps_male = ['2.23','2.28','2.76','3.11','3.88','4.20','4.61','5.26','6.18','6.76','7.14','8.07','9.22','10.80','10.86','10.86'] ;
              var subscapular_male = ['6.12','7.1','8.07','9.64','12.19','14.27','16','17.72','19.46','21.4','23.71','27.35','30.82','34.59','36.77','36.77'] ;
              var abdominal_male = ['7.59','7.90','9.06','9.88','13.91','18.35','20.59','21.86','23.04','25.19','27.26','29.74','31.92','39.32','43.46','43.46'] ;
              var calf_male = ['3.82','3.88','4.21','4.76','5.56','6.57','6.78','7.68','8.47','9.58','11.70','14.31','16.52','17.87','17.95','17.95'] ;
              var supraspinale_male = ['3.91','4.16','4.85','5.04','5.93','7.32','7.73','8.35','9.27','10.32','11.61','13.81','14.82','18.15','20.64','20.64'] ;
              var thigh_male = ['4.34','4.68','5.46','6.52','7.96','9.21','10.52','11.95','13.58','16.02','18.91','23.99','28.77','34.3','37.24','37.24'] ;               
              var iliac_male = ['4.14','4.69','6.05','7.38','9.98','12.13','14.23','16.12','18.14','20.5','23.64','27.84','31.79','36.48','40.27','40.27'] ;
            }
        }
       else if(document.getElementById("gender").value == "Female")
        { 
             
            if(document.getElementById("age_range").value == "18-29")
            {              
              var tricep_female =['7.54','8.79','9.97','11.85','14.12','16.49','18.75','20.94','23.48','26.64','30.58','35.79','38.99','41.69','43.83','43.83'] ;
              var biceps_female =['2.60','2.80','4.18','4.65','6.27','7.36','8.45','9.14','9.99','11.00','14.13','21.81','25.10','29.86','30.96','30.96'] ;
              var subscapular_female = ['7.07','7.92','9.86','11.42','15.39','19.62','23.52','26.68','31.27','35.36','40.11','44.91','49.32','53.24','55.43','55.43'] ;
              var abdominal_female = ['9.07','9.99','10.81','13.97','17.41','20.65','22.87','26.31','29.36','34.62','38.48','39.89','41.13','41.65','42.08','42.08'] ;
              var calf_female = ['7.94','9.50','10.73','11.92','14.95','16.78','18.27','20.92','23.21','25.39','29','36.15','40.42','41.24','41.93','41.93'] ;
              var supraspinale_female = ['3.48','3.90','5.60','6.00','7.89','8.64','9.38','11.20','13.37','14.65','15.66','20.14','21.50','32.70','33.93','33.93'] ;
              var thigh_female = ['10.23','11.44','13.33','15.40','18.89','21.41','23.74','26.38','29.41','32.81','36.74','40.25','43.25','45.60','46.82','46.82'] ;     
              var iliac_female = ['4.92','5.56','6.51','7.86','10.19','12.74','15.97','19.03','22.93','26.95','30.89','36.75','39.71','42.2','43.9','43.9'] ;
            }
            else if(document.getElementById("age_range").value == "30-39")
            {              
              var tricep_female = ['7.67','8.74','11.08','13.21','16.61','19.84','22.54','25.38','28.82','32.27','35.62','39.61','42.26','44.67','46.17','46.17'] ;
              var biceps_female = ['2.52','2.97','3.91','4.12','5.50','6.60','7.44','8.46','10.06','12.14','14.83','22.37','26.58','29.48','32.37','32.37'] ;
              var subscapular_female = ['8.61','9.60','11.69','14.88','19.75','23.21','26.49','29.37','32.89','36.63','40.51','45.40','49.00','53.55','56.00','56.00'] ;
              var abdominal_female = ['6.97','7.31','10.23','12.19','16.81','20.72','23.83','25.82','29.45','32.70','39.97','41.74','42.98','44.14','45.75','45.75'] ;
              var calf_female = ['6.59','7.71','8.13','9.27','11.66','14.05','16.63','19.63','21.64','26.00','30.60','38.29','41.23','41.71','42.31','42.31'] ;
              var supraspinale_female = ['3.66','4.09','4.75','5.42','6.58','8.20','9.45','10.69','12.12','14.90','18.74','24.14','28.75','31.27','32.92','32.92'] ;
              var thigh_female = ['9.36','11.7','14.14','17.26','21.36','24.67','27.25','30.23','33.31','36.72','39.35','42.68','44.7','46.54','47.7','47.7'] ; 
              var iliac_female = ['4.92','5.56','6.51','7.86','10.19','12.74','15.97','19.03','22.93','26.95','30.89','36.75','39.71','42.2','43.9','43.9'] ;
            }
            else if(document.getElementById("age_range").value == "40-49")
            {              
              var tricep_female = ['8.97','10.64','13.26','16.57','20.49','23.31','25.62','27.82','30.66','33.34','35.88','39.16','41.67','43.80','45.93','45.93'] ;
              var biceps_female = ['3.65','3.93','4.97','6.03','7.01','8.30','9.99','11.88','14.49','17.34','21.17','25.54','31.93','36.13','37.50','37.50'] ;
              var subscapular_female = ['6.68','8.84','11.80','15.05','19.49','23.49','27.20','30.01','33.25','37.07','40.69','45.90','48.77','53.9','57.51','57.51'] ;
              var abdominal_female = ['7.02','8.08','12.50','15.70','21.90','27.97','32.01','35.96','39.36','40.94','41.74','43.07','43.76','45.23','45.91','45.91'] ;
              var calf_female = ['7.89','9.37','10.94','14.29','16.62','18.64','21.66','25.11','29.08','32.67','35.17','40.93','42.31','43.74','44.37','44.37'] ;
              var supraspinale_female = ['4.71','4.81','5.58','6.64','8.73','10.66','13.8','16.3','19.72','21.86','24.41','29.14','34.38','37.76','39.51','39.51'] ;
              var thigh_female = ['9.28','12.04','14.70','19.42','24.08','28.04','31.47','34.26','36.87','39.33','41.34','43.69','45.34','47.53','48.40','48.40'] ;              
              var iliac_female = ['4.9','5.52','7.62','10.11','15.5','20.32','23.97','27.06','29.99','33.24','36.73','40.02','42.08','44.87','45.91','45.91'] ;
            }
            else if(document.getElementById("age_range").value == "50-59")
            {               
              var tricep_female = ['8.09','11.19','14.22','16.7','20.68','23.43','26.19','28.63','31.23','33.41','36.15','39.61','42.56','44.81','46.09','46.09'] ;
              var biceps_female = ['3.48','4.72','5.4','6.35','7.68','9.70','11.10','13.06','15.07','17.86','21.46','25.02','30.70','32.91','37.95','37.95'] ;
              var subscapular_female = ['6.64','8.89','11.30','14.49','18.77','22.50','25.88','29.06','32.06','35.58','39.65','44.73','48.92','54.37','57.10','57.10'] ;
              var abdominal_female = ['8.21','8.81','12.80','17.01','25.90','30.22','36.74','39.36','41.11','42.08','42.67','43.37','44.77','45.43','46.11','46.11'] ;
              var calf_female = ['9.52','9.92','11.40','12.82','15.24','18.62','20.40','23.91','26.67','28.26','32.02','39.58','42.66','43.06','44.82','44.82'] ;
              var supraspinale_female = ['4.42','4.67','5.66','7.28','11.24','13.28','15.98','18.86','20.96','23.72','25.75','30.56','32.76','37.08','37.85','37.85'] ;
              var thigh_female = ['9.21','10.42','13.38','18.19','23.29','27.69','30.93','33.49','36.39','38.49','41.18','43.85','45.26','47.17','48.45','48.45'] ;   
              var iliac_female = ['4.65','5.74','8.09','11.08','20.68','23.43','26.19','28.63','31.23','33.41','36.15','39.61','42.56','44.81','46.09','46.09'] ;
            }
            else if(document.getElementById("age_range").value == "60-69")
            {              
              var tricep_female = ['8.26','10.47','13.22','16.07','19.04','21.51','23.99','26.27','28.46','31.13','34.32','37.61','40.46','43.74','45.81','45.81'] ;
              var biceps_female = ['3.81','4.73','5.99','6.91','8.80','10.17','11.26','12.19','12.72','14.76','17.85','22.47','24.72','29.09','38.45','38.45'] ;
              var subscapular_female = ['6.25','6.75','8.81','10.71','13.86','17.13','20.07','23.11','26.3','29.46','33.37','39.62','44.41','49.74','52.32','52.32'] ;
              var abdominal_female = ['8.4','9.96','15.16','21.96','30.15','33.98','36.62','39.37','40.85','41.71','43.03','44.21','45.91','46.38','46.96','46.96'] ;
              var calf_female = ['6.03','8.42','10.47','12.51','16.04','20.26','23.92','25.12','28.18','30.99','34.01','37.28','42.58','44.09','44.63','44.63'];
              var supraspinale_female = ['5.69','6.67','7.25','10.09','12.32','14.66','16.71','18.73','20.21','22.19','27.31','34.10','36.22','41.97','43.96','43.96'] ;
              var thigh_female = ['7.37','9.15','12.78','16.30','21.64','25.63','29.71','32.41','34.88','37.85','40.42','43.9','45.67','48.03','48.92','48.92'] ; 
              var iliac_female = ['4.67','6.02','8.01','10.78','15.07','18.89','22.38','25.54','28.21','31.02','34.49','38.22','41.51','44.02','46.23','46.23'] ;
            }
            else if(document.getElementById("age_range").value == "70+")
            {              
              var tricep_female = ['7.56','8.22','9.35','10.97','13.09','15.52','18.13','21.19','24.98','29.05','33.74','40.32','44.86','49.35','54.38','54.38'] ;
              var biceps_female = ['4.97','5.14','5.92','6.74','8.12','9.91','10.75','11.33','12.44','13.45','15.52','20.64','24.75','30.88','32.26','32.26'] ;
              var subscapular_female = ['6.13','7.61','9.76','12.03','14.92','17.40','19.72','21.88','24.34','27.01','29.99','33.84','37.19','40.96','42.67','42.67'] ;
              var abdominal_female = ['11.46','12.50','17.29','21.81','28.34','32.04','35.84','39.84','41.79','42.72','43.17','43.52','44.25','46.45','46.99','46.99'] ;
              var calf_female = ['11.55','11.81','13.49','16.16','17.61','20.66','21.77','26.7','28.79','32.69','35.37','42.52','43.83','44.46','44.54','44.54'] ;
              var supraspinale_female = ['7.03','7.33','8.54','9.01','11.31','12.09','14.7','16.05','19.64','22.91','25.79','29.84','31.01','32.94','33.39','33.39'] ;
              var thigh_female = ['7.8','10.19','13.37','16.60','21.04','24.51','28.17','31.04','33.77','36.23','39.53','42.81','44.55','46.95','48.24','48.24'] ;               
              var iliac_female = ['4.55','5.01','6.05','7.79','10.63','12.97','15.91','18.77','21.87','25.27','29.1','33.57','37.03','40.8','42.65','42.65'] ;
            }
        }
   
   
    $(window).bind("load", function() {
        
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        var mid_axilla = document.getElementById("mid_axilla").value ;          
       
        var sum = parseFloat(triceps) + parseFloat(subscapular) + parseFloat(biceps) + parseFloat(iliac) + parseFloat(supraspinale) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf) ;        
        document.getElementById("eight_Sum").value =  Math.round(parseFloat(sum) * 10) / 10 ; 


        if(document.getElementById("gender").value == "Male")
        {                  
          var sum_of_8 = new Array() ;  
             //assign sum values to array of sum of 8 skinfolds  
          for(var i=0; i<16; i++)
            { 
              var sum_skins = parseFloat(tricep_male[i]) + parseFloat(biceps_male[i]) + parseFloat(subscapular_male[i]) + parseFloat(abdominal_male[i]) + parseFloat(calf_male[i]) + parseFloat(supraspinale_male[i]) + parseFloat(thigh_male[i]) + parseFloat(iliac_male[i]) ;             
              sum_skins = Math.round(sum_skins * 100)/100 ;
              sum_of_8.push(sum_skins) ;             
            }
            
          for(var i=0; i<15; i++)
            {          
              if(parseFloat(sum) >= parseFloat(sum_of_8[i]) && parseFloat(sum) < parseFloat(sum_of_8[i+1]))
              {  
                if(i == 0)
                {  
                   var lower_rank = 0 ; // 0%
                   var upper_rank = 1 ; // 1%                  
                } 
                else if(i == 1)
                {  
                   var lower_rank = 1 ; // 1%
                   var upper_rank = 2 ; // 2%                   
                } 
                else if(i == 2)
                {  
                   var lower_rank = 2 ; // 2%
                   var upper_rank = 5 ; // 5%                  
                }
                else if(i == 3)
                {  
                   var lower_rank = 5 ; // 5%
                   var upper_rank = 10 ; // 10%                  
                }
                else if(i == 4)
                {  
                   var lower_rank = 10 ; // 10%
                   var upper_rank = 20 ; // 20%                   
                }
                else if(i == 5)
                {  
                   var lower_rank = 20 ; // 20%
                   var upper_rank = 30 ; // 30%                   
                }
                else if(i == 6)
                {  
                   var lower_rank = 30 ; // 30%
                   var upper_rank = 40 ; // 40%                   
                }
                else if(i == 7)
                {  
                   var lower_rank = 40 ; // 40%
                   var upper_rank = 50 ; // 50%                   
                }
                else if(i == 8)
                {  
                   var lower_rank = 50 ; // 50%
                   var upper_rank = 60 ; // 60%                   
                }
                else if(i == 9)
                {  
                   var lower_rank = 60 ; // 60%
                   var upper_rank = 70 ; // 70%                   
                }    
                else if(i == 10)
                {  
                   var lower_rank = 70 ; // 70%
                   var upper_rank = 80 ; // 80%                   
                }
                else if(i == 11)
                {  
                   var lower_rank = 80 ; // 80%
                   var upper_rank = 90 ; // 90%                   
                }
                else if(i == 12)
                {  
                   var lower_rank = 90 ; // 90%
                   var upper_rank = 95 ; // 95%                   
                }
                else if(i == 13)
                {  
                   var lower_rank = 95 ; // 95%
                   var upper_rank = 98 ; // 98%                   
                }
                else if(i == 14)
                {  
                   var lower_rank = 98 ; // 95%
                   var upper_rank = 99 ; // 98%                   
                }
                var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                var percent_point = (parseFloat(sum_of_8[i+1]) - parseFloat(sum_of_8[i])) / range_difference ; // each % point
                var difference = parseFloat(sum) - parseFloat(sum_of_8[i]) ; // sum greater than lower% value
                var rank_difference = difference / percent_point ;
                var rank = lower_rank + rank_difference ;     
                
                document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                break;
              } 
            }
        } 
        
        if(document.getElementById("gender").value == "Female")
        {
            
        var sum_of_8 = new Array() ;  
        //assign sum values to array of sum of 8 skinfolds  
          for(var i=0; i<16; i++)
            { 
              var sum_skins = parseFloat(tricep_female[i]) + parseFloat(biceps_female[i]) + parseFloat(subscapular_female[i]) + parseFloat(abdominal_female[i]) + parseFloat(calf_female[i]) + parseFloat(supraspinale_female[i]) + parseFloat(thigh_female[i]) + parseFloat(iliac_female[i]) ;             
              sum_skins = Math.round(sum_skins * 100)/100 ;
              sum_of_8.push(sum_skins) ;             
            }
            
          for(var i=0; i<15; i++)
            {          
              if(parseFloat(sum) >= parseFloat(sum_of_8[i]) && parseFloat(sum) < parseFloat(sum_of_8[i+1]))
              {  
                if(i == 0)
                {  
                   var lower_rank = 0 ; // 0%
                   var upper_rank = 1 ; // 1%                  
                } 
                else if(i == 1)
                {  
                   var lower_rank = 1 ; // 1%
                   var upper_rank = 2 ; // 2%                   
                } 
                else if(i == 2)
                {  
                   var lower_rank = 2 ; // 2%
                   var upper_rank = 5 ; // 5%                  
                }
                else if(i == 3)
                {  
                   var lower_rank = 5 ; // 5%
                   var upper_rank = 10 ; // 10%                  
                }
                else if(i == 4)
                {  
                   var lower_rank = 10 ; // 10%
                   var upper_rank = 20 ; // 20%                   
                }
                else if(i == 5)
                {  
                   var lower_rank = 20 ; // 20%
                   var upper_rank = 30 ; // 30%                   
                }
                else if(i == 6)
                {  
                   var lower_rank = 30 ; // 30%
                   var upper_rank = 40 ; // 40%                   
                }
                else if(i == 7)
                {  
                   var lower_rank = 40 ; // 40%
                   var upper_rank = 50 ; // 50%                   
                }
                else if(i == 8)
                {  
                   var lower_rank = 50 ; // 50%
                   var upper_rank = 60 ; // 60%                   
                }
                else if(i == 9)
                {  
                   var lower_rank = 60 ; // 60%
                   var upper_rank = 70 ; // 70%                   
                }    
                else if(i == 10)
                {  
                   var lower_rank = 70 ; // 70%
                   var upper_rank = 80 ; // 80%                   
                }
                else if(i == 11)
                {  
                   var lower_rank = 80 ; // 80%
                   var upper_rank = 90 ; // 90%                   
                }
                else if(i == 12)
                {  
                   var lower_rank = 90 ; // 90%
                   var upper_rank = 95 ; // 95%                   
                }
                else if(i == 13)
                {  
                   var lower_rank = 95 ; // 95%
                   var upper_rank = 98 ; // 98%                   
                }
                else if(i == 14)
                {  
                   var lower_rank = 98 ; // 95%
                   var upper_rank = 99 ; // 98%                   
                }
                var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                var percent_point = (parseFloat(sum_of_8[i+1]) - parseFloat(sum_of_8[i])) / range_difference ; // each % point
                var difference = parseFloat(sum) - parseFloat(sum_of_8[i]) ; // sum greater than lower% value
                var rank_difference = difference / percent_point ;
                var rank = lower_rank + rank_difference ;     
                
                document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                break;
              } 
            }
        }       
       
           
        
        // Plot range percentile
            document.getElementById("plot1").setAttribute('style', "visibility:hidden");
            document.getElementById("plot2").setAttribute('style', "visibility:hidden");
            document.getElementById("plot3").setAttribute('style', "visibility:hidden");
            document.getElementById("plot4").setAttribute('style', "visibility:hidden");
            document.getElementById("plot5").setAttribute('style', "visibility:hidden");
            document.getElementById("plot6").setAttribute('style', "visibility:hidden");
            document.getElementById("plot7").setAttribute('style', "visibility:hidden");
            document.getElementById("plot8").setAttribute('style', "visibility:hidden");
            document.getElementById("plot9").setAttribute('style', "visibility:hidden");
            document.getElementById("plot10").setAttribute('style', "visibility:hidden");
            document.getElementById("plot11").setAttribute('style', "visibility:hidden");
            document.getElementById("plot12").setAttribute('style', "visibility:hidden");
            document.getElementById("plot13").setAttribute('style', "visibility:hidden");
            document.getElementById("plot14").setAttribute('style', "visibility:hidden");
            document.getElementById("plot15").setAttribute('style', "visibility:hidden");
            document.getElementById("plot16").setAttribute('style', "visibility:hidden");
            document.getElementById("plot17").setAttribute('style', "visibility:hidden");
            document.getElementById("plot18").setAttribute('style', "visibility:hidden");
            document.getElementById("plot19").setAttribute('style', "visibility:hidden");
            document.getElementById("plot20").setAttribute('style', "visibility:hidden");
            document.getElementById("plot21").setAttribute('style', "visibility:hidden");
            document.getElementById("plot22").setAttribute('style', "visibility:hidden");
            document.getElementById("plot23").setAttribute('style', "visibility:hidden");
            document.getElementById("plot24").setAttribute('style', "visibility:hidden");
            document.getElementById("plot25").setAttribute('style', "visibility:hidden");
            
            
            if(document.getElementById("percentile").value > 0 && document.getElementById("percentile").value <= 1)
                {              
                  document.getElementById('plot1').removeAttribute('style');                     
                }
            else if(document.getElementById("percentile").value > 1 && document.getElementById("percentile").value < 5)
                {              
                  document.getElementById('plot2').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value == 5)
                {              
                  document.getElementById('plot3').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 5 && document.getElementById("percentile").value < 25)
                {              
                  document.getElementById('plot4').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 25)
                {              
                  document.getElementById('plot5').removeAttribute('style'); 
                }                   
            else if(document.getElementById("percentile").value > 25 && document.getElementById("percentile").value <= 38)
                {              
                  document.getElementById('plot6').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 38 && document.getElementById("percentile").value < 50)
                {              
                  document.getElementById('plot7').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 50)
                {              
                  document.getElementById('plot8').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 50 && document.getElementById("percentile").value <= 63)
                {              
                  document.getElementById('plot9').removeAttribute('style'); 
                }                
            else if(document.getElementById("percentile").value > 63 && document.getElementById("percentile").value < 75)
                {              
                  document.getElementById('plot10').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 75)
                {              
                  document.getElementById('plot11').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 75 && document.getElementById("percentile").value <= 79)
                {              
                  document.getElementById('plot12').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 79 && document.getElementById("percentile").value <= 83)
                {              
                  document.getElementById('plot13').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 83 && document.getElementById("percentile").value <= 87)
                {              
                  document.getElementById('plot14').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 87 && document.getElementById("percentile").value < 90)
                {              
                  document.getElementById('plot15').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 90)
                {              
                  document.getElementById('plot16').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 90 && document.getElementById("percentile").value <= 91)
                {              
                  document.getElementById('plot17').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 91 && document.getElementById("percentile").value <= 92)
                {              
                  document.getElementById('plot18').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 92 && document.getElementById("percentile").value <= 93)
                {              
                  document.getElementById('plot19').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 93 && document.getElementById("percentile").value < 94)
                {              
                  document.getElementById('plot20').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 94)
                {              
                  document.getElementById('plot21').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 94 && document.getElementById("percentile").value < 95)
                {              
                  document.getElementById('plot22').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 95)
                {              
                  document.getElementById('plot23').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 95 && document.getElementById("percentile").value <= 97)
                {              
                  document.getElementById('plot24').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 97 && document.getElementById("percentile").value <= 100)
                {              
                  document.getElementById('plot25').removeAttribute('style'); 
                }    
         
       }); 
       
         $(document).on('click','#three_Sites', function(){
                
                    var last3 = document.getElementById("three_last").value ;
                    
                    if(last3 !== "")
                    {    
                        document.getElementById("three_Sum").value = parseFloat(last3) ; 
                    }   
                        document.getElementById("sumSites").value = 3 ;                    
                        document.getElementById("sum3").removeAttribute('style'); 
                    
                         document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
               
        });
       
        
         $(document).on('click','#four_Sites', function(){
                
                    var last4 = document.getElementById("four_last").value ;
                    
                    if(last4 !== "")
                    {    
                        document.getElementById("four_Sum").value = parseFloat(last4) ; 
                    }    
                        document.getElementById("sumSites").value = 4 ;                    
                        document.getElementById("sum4").removeAttribute('style');
                        
                        document.getElementById("sum3").setAttribute('style', "display:none");
                        document.getElementById("three_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
               
        });
        
        $(document).on('click','#five_Sites', function(){
            
                    var last5 = document.getElementById("five_last").value ;
                    
                    if(last5 !== "")
                    {    
                        document.getElementById("five_Sum").value = parseFloat(last5) ; 
                    }               
                        document.getElementById("sumSites").value = 5 ;                    
                        document.getElementById("sum5").removeAttribute('style'); 
                        
                        document.getElementById("sum3").setAttribute('style', "display:none");
                        document.getElementById("three_Sum").value = "" ;

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
              
        });
        
        $(document).on('click','#six_Sites', function(){
            
                     var last6 = document.getElementById("six_last").value ;
                    
                     if(last6 !== "")
                     {    
                        document.getElementById("six_Sum").value = parseFloat(last6) ;
                     } 
                     
                        document.getElementById("sumSites").value = 6 ;                    
                        document.getElementById("sum6").removeAttribute('style'); 
                        
                        document.getElementById("sum3").setAttribute('style', "display:none");
                        document.getElementById("three_Sum").value = "" ;

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
                       
        });
        
        $(document).on('click','#seven_Sites', function(){
                              
                     var last7 = document.getElementById("seven_last").value ;         
                     
                     if(last7 !== "")
                     {    
                        document.getElementById("seven_Sum").value = parseFloat(last7) ; 
                     } 
                    
                        document.getElementById("sumSites").value = 7 ;                    
                        document.getElementById("sum7").removeAttribute('style'); 
                        
                        document.getElementById("sum3").setAttribute('style', "display:none");
                        document.getElementById("three_Sum").value = "" ;

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");

                        getSumSkinfolds() ; 
             
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = 8 ; 
            document.getElementById("sum8").removeAttribute('style');
            
            document.getElementById("sum3").setAttribute('style', "display:none");
                        document.getElementById("three_Sum").value = "" ;
            
            document.getElementById("sum4").setAttribute('style', "display:none"); 
            document.getElementById("four_Sum").value = "" ;
            
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("five_Sum").value = "" ;
            
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("six_Sum").value = "" ;
            
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("seven_Sum").value = "" ;
            
            getSumSkinfolds() ;
        });   
        
        $(document).on('click','#plot', function(){
             location.href = '<?php echo site_url();?>/Body/skinfold_Map?profile=restricted&triceps=<?php echo $skinfolds_Values["triceps"];?>&subscapular=<?php echo $skinfolds_Values["subscapular"];?>&illiac=<?php echo $skinfolds_Values["iliac_crest"];?>&thigh=<?php echo $skinfolds_Values["thigh"];?>&supraspinale=<?php echo $skinfolds_Values["supraspinale"];?>&biceps=<?php echo $skinfolds_Values["biceps"];?>&abdominal=<?php echo $skinfolds_Values["abdominal"];?>&calf=<?php echo $skinfolds_Values["calf"];?>&height=<?php echo $skinfolds_Values["height"];?>&body_mass=<?php echo $skinfolds_Values["body_mass"];?>' ;
        });
        
	$(document).on('click','.info_icon_btn', function(){
	    $(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
        
        function changeState(e)
            {  
               if(e.className == 'red_circle') {
                    e.className = 'green_circle';
                } else {
                    e.className = 'red_circle';
                }
                getSumSkinfolds() ;
            }
        
  

    function getSumSkinfolds()
    { 
        if(document.getElementById("gender").value == "Male")
        { 
            if(document.getElementById("age_range").value == "18-29")
            {             
              var tricep_male = ['3.96','4.23','4.77','5.39','6.49','7.66','8.78','9.98','11.35','13.03','15.41','19.46','23.55','30.26','33.29','33.29'] ;
              var biceps_male = ['2.37','2.44','2.57','2.76','3.04','3.48','3.84','4.12','4.77','5.69','6.65','10.79','11.37','13.28','21.23','21.23'] ;
              var subscapular_male = ['6.32','6.77','7.44','8.24','9.47','10.81','12.33','13.94','16.24','18.76','22.09','26.94','30.72','34.13','36.8','36.8'] ;
              var abdominal_male = ['4.86','4.92','6.02','6.61','8.40','11.14','14.11','18.60','23.06','28.54','31.65','37.28','39.15','41.22','41.53','41.53'] ;
              var calf_male = ['2.99','3.11','3.91','4.22','5.30','6.47','7.14','7.88','9.01','11.45','14.27','15.87','17.83','32.15','37.40','37.40'] ;
              var supraspinale_male = ['3.54','3.86','3.98','4.19','4.74','5.32','6.24','6.96','8.47','10.14','12.27','16.32','22.88','27.32','35.63','35.63'] ;
              var thigh_male = ['4.11','4.49','5.06','5.72','6.98','8.02','9.11','10.49','12.16','13.92','16.27','20.33','25.18','30.18','34.04','34.04'] ;     
              var iliac_male = ['4.33','4.73','5.46','6.31','8.23','10.9','13.73','17.13','20.96','25.51','29.93','34.76','38.02','40.46','41.87','41.87'] ;
            }
            else if(document.getElementById("age_range").value == "30-39")
            {              
              var tricep_male = ['3.72','4.16','4.87','5.79','7.36','8.51','9.8','10.83','12.24','14.11','16.05','20.28','24.91','31.03','34.65','34.65'] ;
              var biceps_male = ['2.21','2.45','2.72','3.01','3.87','4.22','4.75','5.23','5.71','6.38','7.15','9.98','11.30','12.85','19.25','19.25'] ;
              var subscapular_male = ['6.81','7.14','8.07','9.22','11.65','13.64','15.67','17.96','19.94','22.38','25.22','29.48','32.71','37.88','40.9','40.9'] ;
              var abdominal_male = ['6.08','6.63','7.46','10.91','18.14','20.39','23.70','26.29','29.48','33.27','35.70','38.44','39.06','40.25','41.08','41.08'] ;
              var calf_male = ['3.86','4.08','4.46','5.23','6.26','6.99','7.83','8.66','9.98','11.82','13.03','14.91','16.80','22.90','28.74','28.74'] ;
              var supraspinale_male = ['3.35','3.73','4.01','5.21','6.55','7.28','8.49','9.80','11.01','12.18','14.46','17.30','20.85','25.65','33.36','33.36'] ;
              var thigh_male = ['3.73','4.25','5.25','6.17','7.66','8.84','10.13','11.57','13.13','14.65','17.58','22.06','26.73','32.21','36.37','36.37'] ; 
              var iliac_male = ['4.23','4.74','5.72','5.78','11.41','15.59','19.41','22.44','25.64','28.74','32.28','36.04','38.45','41.09','43.74','43.74'] ;
            }
            else if(document.getElementById("age_range").value == "40-49")
            {              
              var tricep_male = ['3.85','4.21','5.27','6.46','7.7','7.9','10.25','11.47','12.76','14.29','16.63','21.33','24.42','28.98','31.64','31.64'] ;
              var biceps_male = ['2.47','2.73','3.16','3.64','4.28','4.81','5.71','5.80','6.45','7.13','8.42','10.28','12.58','17.14','19.80','19.80'] ;
              var subscapular_male = ['6.72','7.4','8.58','10.52','13.59','15.46','17.56','19.22','21.34','23.41','26.37','30.75','34.09','37.01','39.06','39.06'] ;
              var abdominal_male = ['6.19','7.40','11.19','13.30','18.62','22.72','25.84','27.54','31.58','34.56','36.78','39.26','40.90','41.79','42.15','42.15'] ;
              var calf_male = ['4.02','4.39','5.01','5.55','7.04','8.06','8.99','9.77','11.27','12.44','13.79','17.14','19.62','24.44','28.56','28.56'] ;
              var supraspinale_male = ['3.62','4.23','5.42','5.81','7.19','7.93','8.97','10.00','11.38','12.93','15.14','19.13','25.47','29.58','31.33','31.33'] ;
              var thigh_male = ['3.98','4.35','5.34','6.54','7.91','9.18','10.43','11.71','13.19','14.79','17.33','21.95','27.35','34.12','38.03','38.03'] ;              
              var iliac_male = ['4.44','5.01','7.26','9.75','13.98','17.17','20.32','23.16','26.1','28.79','32.22','36.26','38.75','40.94','42.56','42.56'] ;
            }
            else if(document.getElementById("age_range").value == "50-59")
            {               
              var tricep_male = ['4.42','4.87','5.88','6.69','8.13','9.32','10.39','11.74','12.96','14.43','16.8','21.23','25.39','31.99','35.13','35.13'] ;
              var biceps_male = ['2.20','2.40','3.04','3.65','4.32','4.86','5.25','6.08','6.69','7.29','8.19','9.20','11.35','12.40','16.84','16.84'] ;
              var subscapular_male = ['6.87','7.84','9.43','11.36','13.61','16.22','18.07','19.78','21.77','24.02','26.79','30.62','33.42','37.51','39.89','39.89'] ;
              var abdominal_male = ['7.43','8.23','10.62','14.46','17.15','20.58','22.42','26.80','29.97','33.45','35.47','38.28','40.09','41.07','42.09','42.09'] ;
              var calf_male = ['3.89','4.13','4.88','5.74','6.54','7.28','8.44','9.75','10.90','11.82','12.91','15.05','17.49','27.40','35.47','35.47'] ;
              var supraspinale_male = ['4.53','4.60','5.00','5.73','7.00','7.63','8.59','9.57','10.79','12.64','14.63','18.33','20.80','23.29','30.17','30.17'] ;
              var thigh_male = ['4.06','4.75','5.57','6.6','8.16','9.28','10.37','11.67','13.27','15.24','17.96','23.56','28.98','33.86','37.77','37.77'] ;   
              var iliac_male = ['5.14','6.08','7.51','10.18','13.86','17.37','19.67','22.1','24.39','27.41','30.32','34.22','36.8','40.29','41.94','41.94'] ;
            }
            else if(document.getElementById("age_range").value == "60-69")
            {              
              var tricep_male = ['4.38','4.91','5.87','7.05','8.63','9.83','10.89','11.82','12.98','14.68','16.91','21.95','25.45','30.21','32.78','32.78'] ;
              var biceps_male = ['2.76','2.82','3.26','3.66','4.44','5.03','5.63','6.07','6.58','7.33','7.91','9.95','13.10','18.33','20.85','20.85'] ;
              var subscapular_male = ['6.68','7.57','9.58','11.63','14.33','16.48','18.28','20.31','22.27','24.44','27.46','31.36','35.1','38.02','39.81','39.81'] ;
              var abdominal_male = ['7.17','8.02','13.17','15.89','19.40','20.75','22.92','24.79','27.46','30.51','33.54','38.05','39.61','40.58','41.52','41.52'] ;
              var calf_male = ['3.59','3.79','4.49','5.63','6.70','7.73','8.66','9.27','10.07','11.18','12.74','15.09','18.24','23.42','28.00','28.00'] ;
              var supraspinale_male = ['3.59','4.04','4.78','6.13','7.54','8.45','9.17','10.08','11.94','13.08','14.51','16.98','21.19','25.67','26.88','26.88'] ;
              var thigh_male = ['4.44','5.11','5.83','6.62','7.91','9.12','10.34','11.74','13.06','15.14','18.08','22.71','27.76','34.61','38.22','38.22'] ; 
              var iliac_male = ['4.74','5.45','7.95','9.96','13.33','15.74','18.19','20.71','23.5','25.65','29.05','33.56','36.67','39.4','41.39','41.39'] ;
            }
            else if(document.getElementById("age_range").value == "70+")
            {              
              var tricep_male = ['4.26','4.76','5.57','6.69','8.09','9.13','10.42','11.46','12.47','13.77','15.79','19.39','23.36','28.68','31.46','31.46'] ;
              var biceps_male = ['2.23','2.28','2.76','3.11','3.88','4.20','4.61','5.26','6.18','6.76','7.14','8.07','9.22','10.80','10.86','10.86'] ;
              var subscapular_male = ['6.12','7.1','8.07','9.64','12.19','14.27','16','17.72','19.46','21.4','23.71','27.35','30.82','34.59','36.77','36.77'] ;
              var abdominal_male = ['7.59','7.90','9.06','9.88','13.91','18.35','20.59','21.86','23.04','25.19','27.26','29.74','31.92','39.32','43.46','43.46'] ;
              var calf_male = ['3.82','3.88','4.21','4.76','5.56','6.57','6.78','7.68','8.47','9.58','11.70','14.31','16.52','17.87','17.95','17.95'] ;
              var supraspinale_male = ['3.91','4.16','4.85','5.04','5.93','7.32','7.73','8.35','9.27','10.32','11.61','13.81','14.82','18.15','20.64','20.64'] ;
              var thigh_male = ['4.34','4.68','5.46','6.52','7.96','9.21','10.52','11.95','13.58','16.02','18.91','23.99','28.77','34.3','37.24','37.24'] ;               
              var iliac_male = ['4.14','4.69','6.05','7.38','9.98','12.13','14.23','16.12','18.14','20.5','23.64','27.84','31.79','36.48','40.27','40.27'] ;
            }
        }
        else if(document.getElementById("gender").value == "Female")
        { 
             
            if(document.getElementById("age_range").value == "18-29")
            {              
              var tricep_female =['7.54','8.79','9.97','11.85','14.12','16.49','18.75','20.94','23.48','26.64','30.58','35.79','38.99','41.69','43.83','43.83'] ;
              var biceps_female =['2.60','2.80','4.18','4.65','6.27','7.36','8.45','9.14','9.99','11.00','14.13','21.81','25.10','29.86','30.96','30.96'] ;
              var subscapular_female = ['7.07','7.92','9.86','11.42','15.39','19.62','23.52','26.68','31.27','35.36','40.11','44.91','49.32','53.24','55.43','55.43'] ;
              var abdominal_female = ['9.07','9.99','10.81','13.97','17.41','20.65','22.87','26.31','29.36','34.62','38.48','39.89','41.13','41.65','42.08','42.08'] ;
              var calf_female = ['7.94','9.50','10.73','11.92','14.95','16.78','18.27','20.92','23.21','25.39','29','36.15','40.42','41.24','41.93','41.93'] ;
              var supraspinale_female = ['3.48','3.90','5.60','6.00','7.89','8.64','9.38','11.20','13.37','14.65','15.66','20.14','21.50','32.70','33.93','33.93'] ;
              var thigh_female = ['10.23','11.44','13.33','15.40','18.89','21.41','23.74','26.38','29.41','32.81','36.74','40.25','43.25','45.60','46.82','46.82'] ;     
              var iliac_female = ['4.92','5.56','6.51','7.86','10.19','12.74','15.97','19.03','22.93','26.95','30.89','36.75','39.71','42.2','43.9','43.9'] ;
            }
            else if(document.getElementById("age_range").value == "30-39")
            {              
              var tricep_female = ['7.67','8.74','11.08','13.21','16.61','19.84','22.54','25.38','28.82','32.27','35.62','39.61','42.26','44.67','46.17','46.17'] ;
              var biceps_female = ['2.52','2.97','3.91','4.12','5.50','6.60','7.44','8.46','10.06','12.14','14.83','22.37','26.58','29.48','32.37','32.37'] ;
              var subscapular_female = ['8.61','9.60','11.69','14.88','19.75','23.21','26.49','29.37','32.89','36.63','40.51','45.40','49.00','53.55','56.00','56.00'] ;
              var abdominal_female = ['6.97','7.31','10.23','12.19','16.81','20.72','23.83','25.82','29.45','32.70','39.97','41.74','42.98','44.14','45.75','45.75'] ;
              var calf_female = ['6.59','7.71','8.13','9.27','11.66','14.05','16.63','19.63','21.64','26.00','30.60','38.29','41.23','41.71','42.31','42.31'] ;
              var supraspinale_female = ['3.66','4.09','4.75','5.42','6.58','8.20','9.45','10.69','12.12','14.90','18.74','24.14','28.75','31.27','32.92','32.92'] ;
              var thigh_female = ['9.36','11.7','14.14','17.26','21.36','24.67','27.25','30.23','33.31','36.72','39.35','42.68','44.7','46.54','47.7','47.7'] ; 
              var iliac_female = ['4.92','5.56','6.51','7.86','10.19','12.74','15.97','19.03','22.93','26.95','30.89','36.75','39.71','42.2','43.9','43.9'] ;
            }
            else if(document.getElementById("age_range").value == "40-49")
            {              
              var tricep_female = ['8.97','10.64','13.26','16.57','20.49','23.31','25.62','27.82','30.66','33.34','35.88','39.16','41.67','43.80','45.93','45.93'] ;
              var biceps_female = ['3.65','3.93','4.97','6.03','7.01','8.30','9.99','11.88','14.49','17.34','21.17','25.54','31.93','36.13','37.50','37.50'] ;
              var subscapular_female = ['6.68','8.84','11.80','15.05','19.49','23.49','27.20','30.01','33.25','37.07','40.69','45.90','48.77','53.9','57.51','57.51'] ;
              var abdominal_female = ['7.02','8.08','12.50','15.70','21.90','27.97','32.01','35.96','39.36','40.94','41.74','43.07','43.76','45.23','45.91','45.91'] ;
              var calf_female = ['7.89','9.37','10.94','14.29','16.62','18.64','21.66','25.11','29.08','32.67','35.17','40.93','42.31','43.74','44.37','44.37'] ;
              var supraspinale_female = ['4.71','4.81','5.58','6.64','8.73','10.66','13.8','16.3','19.72','21.86','24.41','29.14','34.38','37.76','39.51','39.51'] ;
              var thigh_female = ['9.28','12.04','14.70','19.42','24.08','28.04','31.47','34.26','36.87','39.33','41.34','43.69','45.34','47.53','48.40','48.40'] ;              
              var iliac_female = ['4.9','5.52','7.62','10.11','15.5','20.32','23.97','27.06','29.99','33.24','36.73','40.02','42.08','44.87','45.91','45.91'] ;
            }
            else if(document.getElementById("age_range").value == "50-59")
            {               
              var tricep_female = ['8.09','11.19','14.22','16.7','20.68','23.43','26.19','28.63','31.23','33.41','36.15','39.61','42.56','44.81','46.09','46.09'] ;
              var biceps_female = ['3.48','4.72','5.4','6.35','7.68','9.70','11.10','13.06','15.07','17.86','21.46','25.02','30.70','32.91','37.95','37.95'] ;
              var subscapular_female = ['6.64','8.89','11.30','14.49','18.77','22.50','25.88','29.06','32.06','35.58','39.65','44.73','48.92','54.37','57.10','57.10'] ;
              var abdominal_female = ['8.21','8.81','12.80','17.01','25.90','30.22','36.74','39.36','41.11','42.08','42.67','43.37','44.77','45.43','46.11','46.11'] ;
              var calf_female = ['9.52','9.92','11.40','12.82','15.24','18.62','20.40','23.91','26.67','28.26','32.02','39.58','42.66','43.06','44.82','44.82'] ;
              var supraspinale_female = ['4.42','4.67','5.66','7.28','11.24','13.28','15.98','18.86','20.96','23.72','25.75','30.56','32.76','37.08','37.85','37.85'] ;
              var thigh_female = ['9.21','10.42','13.38','18.19','23.29','27.69','30.93','33.49','36.39','38.49','41.18','43.85','45.26','47.17','48.45','48.45'] ;   
              var iliac_female = ['4.65','5.74','8.09','11.08','20.68','23.43','26.19','28.63','31.23','33.41','36.15','39.61','42.56','44.81','46.09','46.09'] ;
            }
            else if(document.getElementById("age_range").value == "60-69")
            {              
              var tricep_female = ['8.26','10.47','13.22','16.07','19.04','21.51','23.99','26.27','28.46','31.13','34.32','37.61','40.46','43.74','45.81','45.81'] ;
              var biceps_female = ['3.81','4.73','5.99','6.91','8.80','10.17','11.26','12.19','12.72','14.76','17.85','22.47','24.72','29.09','38.45','38.45'] ;
              var subscapular_female = ['6.25','6.75','8.81','10.71','13.86','17.13','20.07','23.11','26.3','29.46','33.37','39.62','44.41','49.74','52.32','52.32'] ;
              var abdominal_female = ['8.4','9.96','15.16','21.96','30.15','33.98','36.62','39.37','40.85','41.71','43.03','44.21','45.91','46.38','46.96','46.96'] ;
              var calf_female = ['6.03','8.42','10.47','12.51','16.04','20.26','23.92','25.12','28.18','30.99','34.01','37.28','42.58','44.09','44.63','44.63'];
              var supraspinale_female = ['5.69','6.67','7.25','10.09','12.32','14.66','16.71','18.73','20.21','22.19','27.31','34.10','36.22','41.97','43.96','43.96'] ;
              var thigh_female = ['7.37','9.15','12.78','16.30','21.64','25.63','29.71','32.41','34.88','37.85','40.42','43.9','45.67','48.03','48.92','48.92'] ; 
              var iliac_female = ['4.67','6.02','8.01','10.78','15.07','18.89','22.38','25.54','28.21','31.02','34.49','38.22','41.51','44.02','46.23','46.23'] ;
            }
            else if(document.getElementById("age_range").value == "70+")
            {              
              var tricep_female = ['7.56','8.22','9.35','10.97','13.09','15.52','18.13','21.19','24.98','29.05','33.74','40.32','44.86','49.35','54.38','54.38'] ;
              var biceps_female = ['4.97','5.14','5.92','6.74','8.12','9.91','10.75','11.33','12.44','13.45','15.52','20.64','24.75','30.88','32.26','32.26'] ;
              var subscapular_female = ['6.13','7.61','9.76','12.03','14.92','17.40','19.72','21.88','24.34','27.01','29.99','33.84','37.19','40.96','42.67','42.67'] ;
              var abdominal_female = ['11.46','12.50','17.29','21.81','28.34','32.04','35.84','39.84','41.79','42.72','43.17','43.52','44.25','46.45','46.99','46.99'] ;
              var calf_female = ['11.55','11.81','13.49','16.16','17.61','20.66','21.77','26.7','28.79','32.69','35.37','42.52','43.83','44.46','44.54','44.54'] ;
              var supraspinale_female = ['7.03','7.33','8.54','9.01','11.31','12.09','14.7','16.05','19.64','22.91','25.79','29.84','31.01','32.94','33.39','33.39'] ;
              var thigh_female = ['7.8','10.19','13.37','16.60','21.04','24.51','28.17','31.04','33.77','36.23','39.53','42.81','44.55','46.95','48.24','48.24'] ;               
              var iliac_female = ['4.55','5.01','6.05','7.79','10.63','12.97','15.91','18.77','21.87','25.27','29.1','33.57','37.03','40.8','42.65','42.65'] ;
            }
        }      
        
        var ageValue = document.getElementById("age").value;      
        
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        var mid_axilla = document.getElementById("mid_axilla").value ;
        
        
        var sumSites = document.getElementById("sumSites").value ;
        var skins_selected = [] ; //store names of selected skinfolds  
        var sum_skins = 0 ;
        var sites = [] ;
        $('.green_circle').filter(function(){                   
           if(!($(this).attr("value") == undefined))
            {               
             sites.push($(this).attr("value")) ; 
             //skins_selected.push($(this).attr("id")) ;
             if($(this).attr("id") == "tricep_male")
             {
              skins_selected.push(tricep_male) ;                
             }
             else if($(this).attr("id") == "biceps_male")
             {
              skins_selected.push(biceps_male) ;                
             }
             else if($(this).attr("id") == "subscapular_male")
             {
              skins_selected.push(subscapular_male) ;                
             }
             else if($(this).attr("id") == "abdominal_male")
             {
              skins_selected.push(abdominal_male) ;                
             }
             else if($(this).attr("id") == "calf_male")
             {
              skins_selected.push(calf_male) ;                
             }
             else if($(this).attr("id") == "supraspinale_male")
             {
              skins_selected.push(supraspinale_male) ;                
             }
             else if($(this).attr("id") == "thigh_male")
             {
              skins_selected.push(thigh_male) ;                
             }
             else if($(this).attr("id") == "iliac_male")
             {
              skins_selected.push(iliac_male) ;                
             }
             else if($(this).attr("id") == "tricep_female")
             {
              skins_selected.push(tricep_female) ;                
             }
             else if($(this).attr("id") == "biceps_female")
             {
              skins_selected.push(biceps_female) ;                
             }
             else if($(this).attr("id") == "subscapular_female")
             {
              skins_selected.push(subscapular_female) ;                
             }
             else if($(this).attr("id") == "abdominal_female")
             {
              skins_selected.push(abdominal_female) ;                
             }
             else if($(this).attr("id") == "calf_female")
             {
              skins_selected.push(calf_female) ;                
             }
             else if($(this).attr("id") == "supraspinale_female")
             {
              skins_selected.push(supraspinale_female) ;                
             }
             else if($(this).attr("id") == "thigh_female")
             {
              skins_selected.push(thigh_female) ;                
             }
             else if($(this).attr("id") == "iliac_female")
             {
              skins_selected.push(iliac_female) ;                
             }
            }                        
          });            
            
             if(sumSites == 3)
            {       
                if(sites.length == 3)
                {          
                  var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2];   
                  var sum3Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ;
                 
                  document.getElementById("three_Sum").value = Math.round(parseFloat(sum3Skinfold) * 10) / 10 ; // display Sum total of selected sites    
                  document.getElementById("three_last").value = Math.round(parseFloat(sum3Skinfold) * 10) / 10 ;
                  
                   var sum_of_3 = new Array() ;  
                   //assign sum values to array of sum of 3 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_3.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum3Skinfold) >= parseFloat(sum_of_3[i]) && parseFloat(sum3Skinfold) < parseFloat(sum_of_3[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_3[i+1]) - parseFloat(sum_of_3[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum3Skinfold) - parseFloat(sum_of_3[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }                 
                }                  
            }            
            else if(sumSites == 4)
            {       
                if(sites.length == 4)
                {          
                  var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3];   
                  var sum4Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ;
                 
                  document.getElementById("four_Sum").value = Math.round(parseFloat(sum4Skinfold) * 10) / 10 ; // display Sum total of selected sites    
                  document.getElementById("four_last").value = Math.round(parseFloat(sum4Skinfold) * 10) / 10 ;
                  
                   var sum_of_4 = new Array() ;  
                   //assign sum values to array of sum of 4 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) + parseFloat(skins_selected[3][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_4.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum4Skinfold) >= parseFloat(sum_of_4[i]) && parseFloat(sum4Skinfold) < parseFloat(sum_of_4[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_4[i+1]) - parseFloat(sum_of_4[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum4Skinfold) - parseFloat(sum_of_4[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }                 
                }                  
            }
            else if(sumSites == 5)
            {   
                if(sites.length == 5)
                {
                  var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4];   
                  var sum5Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) ;
        
                  document.getElementById("five_Sum").value = Math.round(sum5Skinfold * 10) / 10 ;  // display Sum total of selected sites
                  document.getElementById("five_last").value = Math.round(sum5Skinfold * 10) / 10 ;
                  
                  var sum_of_5 = new Array() ;  
                  //assign sum values to array of sum of 5 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) + parseFloat(skins_selected[3][i]) + parseFloat(skins_selected[4][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_5.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum5Skinfold) >= parseFloat(sum_of_5[i]) && parseFloat(sum5Skinfold) < parseFloat(sum_of_5[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_5[i+1]) - parseFloat(sum_of_5[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum5Skinfold) - parseFloat(sum_of_5[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }
                }          
            }
            else if(sumSites == 6)
            {   
                if(sites.length == 6)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5];   
                 var sum6Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) ;
        
                 document.getElementById("six_Sum").value = Math.round(sum6Skinfold * 10) / 10 ; // display Sum total of selected sites 
                 document.getElementById("six_last").value = Math.round(sum6Skinfold * 10) / 10 ;
                 
                      var sum_of_6 = new Array() ;  
                  //assign sum values to array of sum of 6 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) + parseFloat(skins_selected[3][i]) + parseFloat(skins_selected[4][i]) + parseFloat(skins_selected[5][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_6.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum6Skinfold) >= parseFloat(sum_of_6[i]) && parseFloat(sum6Skinfold) < parseFloat(sum_of_6[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_6[i+1]) - parseFloat(sum_of_6[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum6Skinfold) - parseFloat(sum_of_6[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }
                } 
            }
            else if(sumSites == 7)
            {   
                if(sites.length == 7)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5]; var x7 = sites[6];   
                 var sum7Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) + parseFloat(x7) ;
        
                 document.getElementById("seven_Sum").value = Math.round(sum7Skinfold * 10) / 10 ;  // display Sum total of selected sites 
                 document.getElementById("seven_last").value = Math.round(sum7Skinfold * 10) / 10 ; 
                 
                      var sum_of_7 = new Array() ;  
                  //assign sum values to array of sum of 7 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) + parseFloat(skins_selected[3][i]) + parseFloat(skins_selected[4][i]) + parseFloat(skins_selected[5][i]) + parseFloat(skins_selected[6][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_7.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum7Skinfold) >= parseFloat(sum_of_7[i]) && parseFloat(sum7Skinfold) < parseFloat(sum_of_7[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_7[i+1]) - parseFloat(sum_of_7[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum7Skinfold) - parseFloat(sum_of_7[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }
                } 
            }
            else if(sumSites == 8)
            {    
                if(sites.length == 8)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5]; var x7 = sites[6]; var x8 = sites[7] ;   
                 var sum8Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) + parseFloat(x7) + parseFloat(x8) ;
        
                 document.getElementById("eight_Sum").value = Math.round(sum8Skinfold * 10) / 10 ; // display Sum total of selected sites 
                 document.getElementById("eight_last").value = Math.round(sum8Skinfold * 10) / 10 ;
                 
                 var sum_of_8 = new Array() ;  
                  //assign sum values to array of sum of 8 skinfolds  
                      for(var i=0; i<16; i++)
                        {                          
                          var sum_skins = parseFloat(skins_selected[0][i]) + parseFloat(skins_selected[1][i]) + parseFloat(skins_selected[2][i]) + parseFloat(skins_selected[3][i]) + parseFloat(skins_selected[4][i]) + parseFloat(skins_selected[5][i]) + parseFloat(skins_selected[6][i]) + parseFloat(skins_selected[7][i]) ;   
                          sum_skins = Math.round(sum_skins * 100)/100 ;
                          sum_of_8.push(sum_skins) ;             
                        }
                      
                      for(var i=0; i<15; i++)
                        {          
                          if(parseFloat(sum8Skinfold) >= parseFloat(sum_of_8[i]) && parseFloat(sum8Skinfold) < parseFloat(sum_of_8[i+1]))
                          {  
                            if(i == 0)
                            {  
                               var lower_rank = 0 ; // 0%
                               var upper_rank = 1 ; // 1%                  
                            } 
                            else if(i == 1)
                            {  
                               var lower_rank = 1 ; // 1%
                               var upper_rank = 2 ; // 2%                   
                            } 
                            else if(i == 2)
                            {  
                               var lower_rank = 2 ; // 2%
                               var upper_rank = 5 ; // 5%                  
                            }
                            else if(i == 3)
                            {  
                               var lower_rank = 5 ; // 5%
                               var upper_rank = 10 ; // 10%                  
                            }
                            else if(i == 4)
                            {  
                               var lower_rank = 10 ; // 10%
                               var upper_rank = 20 ; // 20%                   
                            }
                            else if(i == 5)
                            {  
                               var lower_rank = 20 ; // 20%
                               var upper_rank = 30 ; // 30%                   
                            }
                            else if(i == 6)
                            {  
                               var lower_rank = 30 ; // 30%
                               var upper_rank = 40 ; // 40%                   
                            }
                            else if(i == 7)
                            {  
                               var lower_rank = 40 ; // 40%
                               var upper_rank = 50 ; // 50%                   
                            }
                            else if(i == 8)
                            {  
                               var lower_rank = 50 ; // 50%
                               var upper_rank = 60 ; // 60%                   
                            }
                            else if(i == 9)
                            {  
                               var lower_rank = 60 ; // 60%
                               var upper_rank = 70 ; // 70%                   
                            }    
                            else if(i == 10)
                            {  
                               var lower_rank = 70 ; // 70%
                               var upper_rank = 80 ; // 80%                   
                            }
                            else if(i == 11)
                            {  
                               var lower_rank = 80 ; // 80%
                               var upper_rank = 90 ; // 90%                   
                            }
                            else if(i == 12)
                            {  
                               var lower_rank = 90 ; // 90%
                               var upper_rank = 95 ; // 95%                   
                            }
                            else if(i == 13)
                            {  
                               var lower_rank = 95 ; // 95%
                               var upper_rank = 98 ; // 98%                   
                            }
                            else if(i == 14)
                            {  
                               var lower_rank = 98 ; // 95%
                               var upper_rank = 99 ; // 98%                   
                            }
                            var range_difference = upper_rank - lower_rank  ; // difference between lower% and upper%
                            var percent_point = (parseFloat(sum_of_8[i+1]) - parseFloat(sum_of_8[i])) / range_difference ; // each % point
                            var difference = parseFloat(sum8Skinfold) - parseFloat(sum_of_8[i]) ; // sum greater than lower% value
                            var rank_difference = difference / percent_point ;
                            var rank = lower_rank + rank_difference ;     

                            document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
                            break;
                          } 
                        }
                }
            }
            
            
           // Plot range percentile
            document.getElementById("plot1").setAttribute('style', "visibility:hidden");
            document.getElementById("plot2").setAttribute('style', "visibility:hidden");
            document.getElementById("plot3").setAttribute('style', "visibility:hidden");
            document.getElementById("plot4").setAttribute('style', "visibility:hidden");
            document.getElementById("plot5").setAttribute('style', "visibility:hidden");
            document.getElementById("plot6").setAttribute('style', "visibility:hidden");
            document.getElementById("plot7").setAttribute('style', "visibility:hidden");
            document.getElementById("plot8").setAttribute('style', "visibility:hidden");
            document.getElementById("plot9").setAttribute('style', "visibility:hidden");
            document.getElementById("plot10").setAttribute('style', "visibility:hidden");
            document.getElementById("plot11").setAttribute('style', "visibility:hidden");
            document.getElementById("plot12").setAttribute('style', "visibility:hidden");
            document.getElementById("plot13").setAttribute('style', "visibility:hidden");
            document.getElementById("plot14").setAttribute('style', "visibility:hidden");
            document.getElementById("plot15").setAttribute('style', "visibility:hidden");
            document.getElementById("plot16").setAttribute('style', "visibility:hidden");
            document.getElementById("plot17").setAttribute('style', "visibility:hidden");
            document.getElementById("plot18").setAttribute('style', "visibility:hidden");
            document.getElementById("plot19").setAttribute('style', "visibility:hidden");
            document.getElementById("plot20").setAttribute('style', "visibility:hidden");
            document.getElementById("plot21").setAttribute('style', "visibility:hidden");
            document.getElementById("plot22").setAttribute('style', "visibility:hidden");
            document.getElementById("plot23").setAttribute('style', "visibility:hidden");
            document.getElementById("plot24").setAttribute('style', "visibility:hidden");
            document.getElementById("plot25").setAttribute('style', "visibility:hidden");
            
            
            if(document.getElementById("percentile").value > 0 && document.getElementById("percentile").value <= 1)
                {              
                  document.getElementById('plot1').removeAttribute('style');                     
                }
            else if(document.getElementById("percentile").value > 1 && document.getElementById("percentile").value < 5)
                {              
                  document.getElementById('plot2').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value == 5)
                {              
                  document.getElementById('plot3').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 5 && document.getElementById("percentile").value < 25)
                {              
                  document.getElementById('plot4').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 25)
                {              
                  document.getElementById('plot5').removeAttribute('style'); 
                }                   
            else if(document.getElementById("percentile").value > 25 && document.getElementById("percentile").value <= 38)
                {              
                  document.getElementById('plot6').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 38 && document.getElementById("percentile").value < 50)
                {              
                  document.getElementById('plot7').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 50)
                {              
                  document.getElementById('plot8').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 50 && document.getElementById("percentile").value <= 63)
                {              
                  document.getElementById('plot9').removeAttribute('style'); 
                }                
            else if(document.getElementById("percentile").value > 63 && document.getElementById("percentile").value < 75)
                {              
                  document.getElementById('plot10').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 75)
                {              
                  document.getElementById('plot11').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 75 && document.getElementById("percentile").value <= 79)
                {              
                  document.getElementById('plot12').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 79 && document.getElementById("percentile").value <= 83)
                {              
                  document.getElementById('plot13').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 83 && document.getElementById("percentile").value <= 87)
                {              
                  document.getElementById('plot14').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 87 && document.getElementById("percentile").value < 90)
                {              
                  document.getElementById('plot15').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 90)
                {              
                  document.getElementById('plot16').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 90 && document.getElementById("percentile").value <= 91)
                {              
                  document.getElementById('plot17').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 91 && document.getElementById("percentile").value <= 92)
                {              
                  document.getElementById('plot18').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 92 && document.getElementById("percentile").value <= 93)
                {              
                  document.getElementById('plot19').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 93 && document.getElementById("percentile").value < 94)
                {              
                  document.getElementById('plot20').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 94)
                {              
                  document.getElementById('plot21').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 94 && document.getElementById("percentile").value < 95)
                {              
                  document.getElementById('plot22').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 95)
                {              
                  document.getElementById('plot23').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 95 && document.getElementById("percentile").value <= 97)
                {              
                  document.getElementById('plot24').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 97 && document.getElementById("percentile").value <= 100)
                {              
                  document.getElementById('plot25').removeAttribute('style'); 
                }
               
        }
        
</script>
    
</body>
</html>
