<?php
session_start();

$def_prob=$_SESSION['probper'];
$def_prob=round($def_prob,0);
$gender=$_SESSION['gender'];
   if($fieldData['smoking2'] == 'Y' || $fieldData['smoking'] == 'Y')
    {
        $smoke='Y';
    }
    else
    {
        $smoke='N';
    }

$arr = explode(",",$fieldData['diab']);
$diabetes='N';
for($i=0;$i<sizeof($arr);$i++)
{
    if(strtolower($arr[$i]) == "diabetes")
    {
        $diabetes='Y';
    }
}
if($fieldData['ecgstroke'] == 'Y')
{
    $ecg='Y';
}
else
 {
    $ecg='N';
 }
?> 
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medication</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">

<style>
.range_div{ display: inline-block; width:180px; margin: 12px 0;height: 346px; position: relative;} 	
.rangeslider{background:#e6e6e6 !important; box-shadow: none;}
.rangeslider__fill{background:#b3b3b3 !important;box-shadow: none;}
.rangeslider__handle{ background:#4d4d4d !important; width: 20px !important; height: 20px !important; z-index:99;}
.rangeslider--vertical { width:8px !important; height: 100%; left: 50%; margin-left: -4px;}
.rangeslider--vertical .rangeslider__handle { left: -6px !important; border: 0;}
.rangeslider__handle:after{display: none;}	
.vio_range_div .rangeslider__fill{background:#c7b5e7 !important;}
.vio_range_div .rangeslider__handle{ background:#906bd0 !important;}
ul.range_numbers {list-style: none; margin: 0; padding: 0; position: absolute; left: 0; top:0; bottom: 0; z-index: -1;} 
ul.range_numbers li {color: #808080; font-size: 13px; width:74px; text-align: right; position:relative; margin-top:14.5px;}
ul.range_numbers li:before,ul.range_limits li:before{content: ''; width:20px; height: 1px; position: absolute; background:#e9e9e9; right:-35%; top: 8px;}
ul.range_numbers li:first-child{margin-top:2px;}
	
.range_div2 ul.range_numbers li{margin-top:21px;}
.range_div2 ul.range_numbers li:first-child{margin-top:3px;}
	
.range_div ul.range_numbers2 li{margin-top:32px;}
.range_div ul.range_numbers2 li:first-child{margin-top:5px;}
	
.range_div ul.range_numbers3 li{margin-top:8.5px;}
.range_div ul.range_numbers3 li:first-child{margin-top:3px;}	
</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>
<!--style>
#extra{ display: none; }
</style-->
<script type="text/javascript">
    
$(document).ready(function() {
	   // set probability percent from server on page load
     $def_prob='<?php echo $def_prob; ?>';    
     $("#prob").html($def_prob+"%");
    
	
	 var diabetes='<?php echo $diabetes; ?>'  ;
       $('input[type=radio]').change(function() 
        {
            cvd_probability();
        });
    
               if(diabetes == 'Y')
		{
			$('#w_diabetes').show();
			$('#wo_diabetes').hide();
                        diabetes=1;
	        }
		else 
		{
			$('#wo_diabetes').show();
			$('#w_diabetes').hide();
                         diabetes=0;
	        }
	  
        });
    
    //Calculate CVD Risk Probability
    function cvd_probability()
    {
        var smoking=$("input[name='smoker']:checked"). val();
        var colestrol=$("#chol").val();  
        var hdl=$("#hdl").val();// Here it is HDL RATIO
        var ecgstroke=$("input[name='ECG']:checked"). val();
        var diabetes=$("input[name='Diabetes']:checked"). val();
        var SBP=$("#sbp").val();
        //var gender='<?php echo $_SESSION['gender'];?>';
		
		// For gender
        var gender=$("input[name='gend']:checked"). val();
		
        var vp_age='<?php echo $_SESSION['vp_age']?>'; 
	console.log(smoking);
        console.log(colestrol);
        console.log(hdl);
        console.log(ecgstroke);
        console.log(diabetes);
         console.log(SBP);
         console.log(gender);
         console.log(vp_age);
        //Calculations Start
        var years;
        	if(diabetes == 'Y')
		{
			$('#w_diabetes').show();
			$('#wo_diabetes').hide();
                        diabetes=1;
		}
		else 
		{
			$('#wo_diabetes').show();
			$('#w_diabetes').hide();
                         diabetes=0;
		}
	     if(ecgstroke=='Y')
             {
             ecgstroke=1;    
             }   
             else
             {
             ecgstroke=0;       
             }
             if(smoking=='Y')
             {
             smoking=1;    
             }
             else
             {
             smoking=0;    
             }
        
        var years = $("input[type=radio][name='option_1']:checked").val();
       //a = 11.1122-0.9119*ln(SBP)-0.2767*smoking - 0.7181*ln(cholesterol/HDL ratio)-0.5865 * ECG LVH  
        var a=11.1122-0.9119*Math.log(SBP)- 0.2767 *smoking - 0.7181 * Math.log(colestrol/hdl)-0.5865 * ecgstroke;
         a=parseFloat((a).toFixed(9));
       
        /*Step 2	IF GENDER = MALE:
        m = a-1.4792*ln(age)-0.1759*diabetes*/
        if(gender =='M' || gender=='Male')
        {
            var m = a - 1.4792*Math.log(vp_age)- 0.1759 * diabetes;
             m=parseFloat((m).toFixed(9));
        }
        //m = a-5.8549 + 1.8549 + 1.8515 * (ln(age/74)) -3.758*diabetes
        if(gender =='F' || gender=='Female') 
        {
            //var m = a - 5.8549 + 1.8549 + 1.8515 * (Math.log(vp_age/74))- 3.758 * diabetes;   // Original
             var m = a - 5.8549 + 1.8515 * Math.pow(Math.log(vp_age/74),2)- 0.3758 * diabetes; 
        }
         //Step 3	μ = 4.4181+m  
        var mu=4.4181 + m;
         //Step 4	σ = EXP(-0.3155-0.2784*m) 
        var sigma= Math.exp(-0.3155-0.2784 * m);
        sigma=parseFloat((sigma).toFixed(9));
       // Step 5	υ = (ln(t) - μ)/σ    where t= years for risk probability and is typically 5 or 10 years  
        var u = (Math.log(years) - mu)/ sigma;
        u=parseFloat((u).toFixed(9));
    //Step 6	ρ = 1-EXP(-EXP(υ))      
        var p = 1 - Math.exp(- Math.exp(u)) ;  
         p=parseFloat((p).toFixed(6));
        //probability % = ρ * 100
        var probper=p*100; 
        probper=Math.round(probper);
        
        $("#prob").html(probper+"%");
    }
    
</script>    

<style>
.big{height:1190px !important;}
.small{height:800px !important;}
/*.dot-red{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#00ff00;}*/

.dot-red{position:relative;width:21px;height:21px;border-radius:15px;background:none;border-color:#000;border-width:2px;border-style:solid}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
</style>
</head>
<script type="text/javascript">
        $(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
	
	


 $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
      var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
             $text_box.val(this.value); 
             cvd_probability();
        //calculate_oz_rating();
	});
   $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
 
    });
      $text_box.val(this.value);   
	});
}); 


	
	
</script>
<body>
 <?php echo form_open('welcome/',''); ?> 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedication" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Medications & Conditions</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Medications & Conditions</div>
            <p>Select any diagnosed medical conditions you have and also any regular medications that you have been prescribed. 
               Only major categories of medications are listed. More than one can be selected.
               The final question is on bone and joint pain that you experience regularly and that is made worse by exercise. 
               If you answer �YES� then a figure appears that allows you to click on to highlight which areas of the body you experience these problems. 
               More than one site can be highlighted.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>       
    
    </div>
</div>

<!--Start Wrapper --> 
<div class="wrapper">

<!--Start login --> 
<div class="login-cont" style="display:none;">
     
<!--	<form action="<?php //echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
<!--    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_first_name') ;?>"  disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required  value="<?php //echo $this->session->userdata('user_last_name') ;?>"  disabled="disabled"><input name="submitMedication" type="submit" value="" title="edit client details" /></span>
     </div>  -->
   <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required><input name="submitMedication" type="submit" value="" title="edit client details"/></span>
     </div>
<!--	</form> -->
</div><!--End login --> 

<!--Start contain --> 
<div class="contain medi-contain-new">
   
   <!--Start left --> 
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn active" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
     
       </div>
   <!--End Left --> 
 
   <!--Start right --> 
    <?php //print_r($screening); ?>
   <div class="right">
       
   		<div class="right-head" style="margin-bottom:0;">Absolute CVD Risk</div>
   		<div class="right-section page_medi">
        <!--<div class="field_row checkbox_title_row" style="border:0;">
      		<div class="field_85">&nbsp;</div>
            <div class="field_15">
            	<span>5 Year</span>
            	<span>10 Year</span>                
            </div>        
      </div>-->
      
        <div class="field_row"> 
            <div class="field_85" style="padding-right: 0; width: 70%;">This is an estimated risk [% chance] of having a CVD event or heart attack in the next 5 years</div>
            <div class="field_15" style="width: 28%;">
			
			    <input type="radio" name="option_1" value="5" checked="checked" id="options_1Y" class="gender"> <label for="options_1Y"><span style="margin-right: 5px;"></span>5 Years</label> 
				<input type="radio" name="option_1" value="10" id="options_1N" class="gender"> <label for="options_1N"><span style="margin-right: 5px;"></span> 10 Years</label>   
			
			
            	
            </div>
        </div>
        
	   <p style="font-size:18px; margin-bottom:15px;">'What-if' analysis</p>
     		<!--div class="field_row checkbox_title_row" style="border:0;">
					<div class="field_50">&nbsp;</div>
					<div class="field_50">
						<span>Male</span>
						<span>Female</span>                
					</div>        
			  </div-->
			  
			  
			  
			 <div class="field_row checkbox_title_row" style="border:0;">
            <div class="field_50">Gender</div>
				<div class="field_50">
					 <?php     $radio_is_checked = ( ($gender === 'M' || $gender === 'Male' ) )?"checked":"N";
                        echo form_radio(array("name"=>"gend","id"=>"option_5Y","value"=>"M", "class"=>"gender",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_5Y"><span style="margin-right: 5px;"></span>Male</label>
                        <?php 
                        echo form_radio(array("name"=>"gend","id"=>"option_5N","value"=>"F", "class"=>"gender",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_5N"><span style="margin-right: 5px;"></span>Female</label>             
				</div>
            </div> 
			  
			  
			  
			  
			  
			  
      		<div class="field_row">
				<div class="field_50">Smoker</div>
				<div class="field_50">
					 <?php     $radio_is_checked = ( ($fieldData['smoking2'] === 'Y' || $fieldData['smoking'] === 'Y' ) )?"checked":"N";
                        echo form_radio(array("name"=>"smoker","id"=>"option_2Y","value"=>"Y", "class"=>"gender",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_2Y"><span style="margin-right: 5px;"></span>Yes</label>
                        <?php 
                        echo form_radio(array("name"=>"smoker","id"=>"option_2N","value"=>"N", "class"=>"gender",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_2N"><span style="margin-right: 5px;"></span>No</label>             
				</div>
				<div class="clearfix">&nbsp;</div>
				<div class="field_50">Diabetes</div>
				<div class="field_50 f_right">
					<?php     $radio_is_checked = ( ($diabetes === 'Y') )?"checked":"N";
                        echo form_radio(array("name"=>"Diabetes","id"=>"option_3Y","value"=>"Y", "class"=>"gender",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_3Y"><span style="margin-right: 5px;"></span>Yes</label>
                        <?php 
                        echo form_radio(array("name"=>"Diabetes","id"=>"option_3N","value"=>"N", "class"=>"gender",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_3N"><span style="margin-right: 5px;"></span>No</label>               
				</div>
				<div class="clearfix">&nbsp;</div>
				<div class="field_50">ECG LVH</div>
				<div class="field_50 f_right">
					<?php     $radio_is_checked = ( ($ecg === 'Y') )?"checked":"N";
                        echo form_radio(array("name"=>"ECG","id"=>"option_4Y","value"=>"Y", "class"=>"gender",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_4Y"><span style="margin-right: 5px;"></span>Yes</label>
                        <?php 
                        echo form_radio(array("name"=>"ECG","id"=>"option_4N","value"=>"N", "class"=>"gender",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_4N"><span style="margin-right: 5px;"></span>No</label>              
				</div>
			</div> 
     
						<div class="field_row cvd_row">
				<div class="field_25">
					
					<input type="text" id="sbp" name="sbp" value="<?php echo $_SESSION['SBP'];?>" style="width:80px;">
					<label>SBP</label>
					
					<div class="range_div">
					<input name="sbp_slider" type="range" value="<?php echo (isset($_SESSION['SBP'])?$_SESSION['SBP']:80);?>" min="80" max="200" step="0.1" data-orientation="vertical" link-to="sbp" data-rangeslider >
					<ul class="range_numbers range_numbers2">
						<li>200</li>
						<li>180</li>
						<li>160</li>
						<li>140</li>
						<li>120</li>
						<li>100</li>
						<li>80</li>
					</ul>
					</div> 
					<label>mmHg</label>
				</div>
				<div class="field_25">
					
					<input type="text" id="waist" name="waist" value="<?php if(isset($_SESSION['waist']))
                                                                    {
                                                                      echo $_SESSION['waist'];
                                                                    }else{
                                                                        echo "";
                                                                    }
                                                                   ?>" style="width:80px;">
					<label>Waist</label>
					
					<div class="range_div">
					<input name="waist_slider" type="range" value="<?php if(isset($_SESSION['waist']))
                                                                    {
                                                                      echo $_SESSION['waist'];
                                                                    }else{
                                                                        echo "50";
                                                                    }
                                                                   ?>" min="50" max="140" step="0.1" data-orientation="vertical" link-to="waist" data-rangeslider >
					<ul class="range_numbers">
						<li>140</li>
						<li>130</li>
						<li>120</li>
						<li>110</li>
						<li>100</li>
						<li>90</li>
						<li>80</li>
						<li>70</li>
						<li>60</li>
						<li>50</li>
					</ul>
					</div> 
					<label>cm</label>
				</div>
				<div class="field_25">
					
					<input type="text" id="chol" name="chol" value="<?php if(isset($_SESSION['colestrol']))
                                                                    {
                                                                      echo $_SESSION['colestrol'];
                                                                    }else{
                                                                        echo "";
                                                                    }
                                                                   ?>" style="width:80px;">
					<label>Cholesterol</label>
					
					<div class="range_div">
					<input name="chol_slider" type="range" value="<?php if(isset($_SESSION['colestrol']))
                                                                    {
                                                                      echo $_SESSION['colestrol'];
                                                                    }else{
                                                                        echo "3";
                                                                    }
                                                                   ?>" min="3" max="12" step="0.1" data-orientation="vertical" link-to="chol" data-rangeslider >
					<ul class="range_numbers">
						<li>12</li>
						<li>11</li>
						<li>10</li>
						<li>9</li>
						<li>8</li>
						<li>7</li>
						<li>6</li>
						<li>5</li>
						<li>4</li>
						<li>3</li>
					</ul>
					</div> 
					<label>mM</label>
				</div>
				<div class="field_25">
					
					<input type="text" id="hdl" name="hdl" value="<?php echo $_SESSION['hdl'];?>" style="width:80px;">
					<label>HDL</label>
					
					<div class="range_div">
					<input name="mass_slider" type="range" value="<?php echo (isset($_SESSION['hdl'])?$_SESSION['hdl']:0.6);?>" min="0.6" max="2.8" step="0.1" data-orientation="vertical" link-to="hdl" data-rangeslider >
					<ul class="range_numbers range_numbers3">
						<li>2.8</li>
						<li>2.6</li>
						<li>2.4</li>
						<li>2.2</li>
						<li>2.0</li>
						<li>1.8</li> 
						<li>1.6</li>
						<li>1.4</li>
						<li>1.2</li>
						<li>1.0</li>
						<li>0.8</li>
						<li>0.6</li>
					</ul>
					</div> 
					<label>mM</label>
				</div>
			</div>
    		
    		
           <div class="ab_tag">Absolute CVD risk <span id="prob">19%</span></div>
    
    		<div class="graph_box" id="wo_diabetes">	
				<h1>People Without Diabetes</h1>
				<div class="graph_head">
					<div class="head">Women</div>
					<div class="head">Men</div>
				</div>			
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="block-head" style="text-indent: 27px;">Non-smoker</div>
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="block-head">Smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>								
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block_age" style="margin-top: 40px;">Age<br>65-74</div>
					<div class="graph_block mr_22">
						<div class="block-head">Non-smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>			
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="block-head" style="text-indent: -38px;">Smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>		
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="yellow">&nbsp;</li>		
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="yellow">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>		
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block_age">Age<br>55-64</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="yellow">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block_age">Age<br>45-54</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>	
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>								
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>	
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>	
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block_age">Age<br>35-44</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>				
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="light_green">&nbsp;</li>		
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>		
								<li class="light_green">&nbsp;</li>		
								<li class="light_green">&nbsp;</li>	
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>	
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="graph_foot">
					<div class="g_foot">Total cholesterol:HDL ratio</div>
					<div class="g_foot">Total cholesterol:HDL ratio</div>
				</div>
			</div>
    		
    		<div class="graph_box" id="w_diabetes">	
				<h1>People With Diabetes</h1>
				<div class="graph_head">
					<div class="head">Women</div>
					<div class="head">Men</div>
				</div>			
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="block-head" style="text-indent: 27px;">Non-smoker</div>
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="block-head">Smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block_age" style="margin-top: 40px;">Age<br>65-74</div>
					<div class="graph_block mr_22">
						<div class="block-head">Non-smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="block-head" style="text-indent: -38px;">Smoker</div>
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>		
								<li class="orange_dark">&nbsp;</li>		
								<li class="orange_dark">&nbsp;</li>		
								<li class="red">&nbsp;</li>		
								<li class="blue">&nbsp;</li>	
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>			
								<li class="green">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>			
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>		
								<li class="orange_dark">&nbsp;</li>		
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="red">&nbsp;</li>
								<li class="blue">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>		
								<li class="orange_light">&nbsp;</li>		
							</ul>							
						</div>
					</div>
					<div class="graph_block_age">Age<br>55-64</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="yellow">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="blue">&nbsp;</li>		
								<li class="yellow">&nbsp;</li>	
								<li class="yellow">&nbsp;</li>	
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>	
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block_age">Age<br>45-54</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>	
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="orange_dark">&nbsp;</li>
								<li class="red">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
					</div>
				</div>
				<div class="graph_row">
					<div class="graph_block mr_22" style="width: 145px">
						<div class="graph_val">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="blue">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>	
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>							
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block">
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block_age">Age<br>35-44</div>
					<div class="graph_block mr_22">
						<div class="graph_grid">
							<ul>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>														
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="blue">&nbsp;</li>								
								<li class="light_green">&nbsp;</li>
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="green">&nbsp;</li>								
								<li class="green">&nbsp;</li>	
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>
								<li class="light_green">&nbsp;</li>							
								<li class="light_green">&nbsp;</li>		
								<li class="green">&nbsp;</li>			
							</ul>							
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
					<div class="graph_block" style="width: 145px">
						<div class="graph_grid">
							<ul>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="orange_light">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="yellow">&nbsp;</li>
								<li class="yellow">&nbsp;</li>								
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="blue">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="green">&nbsp;</li>
								<li class="blue">&nbsp;</li>	
							</ul>							
						</div>
						<div class="graph_val" style="margin-right: 0; margin-left: 2px;">
							<ul>
								<li>179*</li>
								<li>160</li>
								<li>140</li>
								<li>120</li>
							</ul>
						</div>
						<div class="graph_bottom_val">
							<ul>
								<li>4</li>
								<li>5</li>
								<li>6</li>
								<li>7</li>
								<li>8</li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="graph_foot">
					<div class="g_foot">Total cholesterol:HDL ratio</div>
					<div class="g_foot">Total cholesterol:HDL ratio</div>
				</div>
			</div>
     
			<div class="bottom_table">
				<p>Risk Level for cardiovascular (CVD) risk</p>
				<div class="bottom_col">
					<h3>High Risk</h3>
					<ul>
						<li class="red"></li>
						<li class="orange_dark"></li>
						<li class="orange_light"></li>
						<li class="yellow"></li>
					</ul>
				</div>
				<div class="bottom_col">
					<h3>Moderate Risk</h3>
					<ul>
						<li class="blue"></li>
					</ul>
				</div>
				<div class="bottom_col">
					<h3>Low Risk</h3>
					<ul>
						<li class="green"></li>
						<li class="light_green"></li>
					</ul>
				</div>
				
			</div>
     
			<div class="clearfix">&nbsp;</div>
      <?php echo form_submit('mysubmit4','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform4'");?>
      <?php echo form_submit('mysubmit6','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform6'");?>
       
      <?php echo form_close(); ?>
       
      </div><!--End right section--> 
   </div><!--End right --> 
</div><!--End contain -->

</div><!--End Wrapper --> 
<script>
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
});  
</script>
</body>
</html>
