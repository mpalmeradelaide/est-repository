<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>
<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Error Analysis / 95% CI around a single measure</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>    
    </div>
</div>


<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/saveClientErrorConfidenceInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Error Analysis</div>
            
            <div class="field_row verticle_field_row"> 
                <div class="half_container f_left">
                	<p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis'); ?>';" id="tem" name="error_radio" value="tem">
                    <label for="tem"> <span style="margin-right:8px;"></span>Calculate TEM from raw data</label></p>
                    <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis_confidence'); ?>';" id="single_measure" name="error_radio" value="single_measure" checked="checked"> 
                    <label for="single_measure"> <span style="margin-right:8px;"></span>95% CI around a single measure</label></p><!--<img src="<?php echo "$base/$image"?>/red_dot.png">-->
                    <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_real_change'); ?>';" id="real_change" name="error_radio" value="real_change"> 
                    <label for="real_change"> <span style="margin-right:8px;"></span>Has a real change occurred?</label></p>
                </div>
                <div class="half_container f_right">
                	<div class="note_text">
                        <p>Enter a single raw measurement and a %TEM value for that measure [or type of measure] to see the 95% confidence interval for that measurement.</p>
                    </div>
                </div>
            </div>
            
            <div class="field_row verticle_field_row"> 
            	<div class="field_24">
                	<label>Enter a single raw score</label>
                    <input type="text" id="single_raw" name="single_raw"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->single_raw)?$fieldData[0]->single_raw:""; ?>" autofocus>
                </div>
                <div class="field_24">
                	<label>Enter your %TEM</label>
                    <input type="text" id="tem_percent" name="tem_percent"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->tem_percent)?$fieldData[0]->tem_percent:""; ?>" tabindex="0">
                </div>
                <div class="field_24">
                	<button id="compute" name="compute" class="lite_btn f_left btn_vio" style="margin-top:30px;">Compute</button>
                </div>               
            </div>
            
            <div class="field_row verticle_field_row" style="border:0;"> 
            	<div class="field_24">
                	<label>The 95% CI for the measure is</label>
                    <input type="text" id="interval" name="interval" value="<?php echo isset($fieldData[0]->interval)?$fieldData[0]->interval:"****_****"; ?>" readonly>
                </div>
                <div class="side_75_per">
                	 <span id="answer" style="color: #ef3832; width: 50%; margin-top: 30px; margin-left: 0;">&nbsp;</span>
                </div>              
            </div>
            
            
        </div>   
    </div>

         <input type="hidden" id="exit_key" name="exit_key" value="">

     <?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

       $(document).on('click','#exit', function(){     
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#tem', function(){
          document.getElementById("exit_key").value = "tem" ;
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#single_measure', function(){ 
          document.getElementById("exit_key").value = "single_measure" ;  
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#real_change', function(){ 
          document.getElementById("exit_key").value = "real_change" ;  
          document.forms["myform"].submit();
        //return false;
        });
        
         document.getElementById("compute").addEventListener("click", function(event){
            event.preventDefault() ;
        });  
	
			
		$(document).on('click','.print_icon_toggle_btn', function(){
			$(".print_icon_toggle").toggle();
		});
			
               
// Check for Single raw value & %TEM

        $(document).on('click','#compute', function(){
            
             if(document.getElementById("single_raw").value == "" || document.getElementById("tem_percent").value == "")
               {
                  if(document.getElementById("single_raw").value == "")
                  {
                      alert("Please enter a single raw score") ;
                  }
                  else if(document.getElementById("tem_percent").value == "")
                  {
                      alert("Please enter your %TEM") ;
                  }
                  
               }
             else
               {
                  get_CI() ;
               }
            
        });


  // TEM calculation with 2 trial Columns
    function get_CI()
    {
       var raw_value = document.getElementById("single_raw").value ;
       var tem_percent = document.getElementById("tem_percent").value ;
       
       var Upper_confidence = (parseFloat(raw_value) + 2 * parseFloat(tem_percent) / 100 * parseFloat(raw_value)) ;
       var Lower_confidence = (parseFloat(raw_value) - 2 * parseFloat(tem_percent) / 100 * parseFloat(raw_value)) ;

       var upper = Math.round(Upper_confidence * 10) / 10 ; 
       var lower = Math.round(Lower_confidence * 10) / 10 ; 
       
       document.getElementById("interval").value = lower+" _ "+upper ; 
       document.getElementById("answer").innerHTML = "When the measured value is "+raw_value+", and the %TEM is "+tem_percent+", the 95% confidence interval is from "+lower+" to "+upper ;
       
       
	
    }
        
</script>    
</body>
</html>
