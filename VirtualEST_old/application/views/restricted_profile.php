
<?php 
		//To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
              $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
              $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
              $bodyfatdiv="block";    
            }
            else
            {
              $bodyfatdiv="none";    
            }
			
			
			$isvp=$_SESSION['is_virtual'];
			 
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>

<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		
		var isvp='<?php echo $isvp;?>';
        if(isvp == 1)
        {
		  $(".v_person").fadeTo( "slow" , 1, function() {});
		  $(".v_detail").toggle();
		}  
		
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});
 $(document).on('click', '.info_block_new_btn', function () {
                var info_id = this.id;
                 $.ajax({
                  url:'<?php echo site_url('Body/get_info_details');?>',  
                   type:'POST',
                   data:{info_id:info_id},
                   dataType:'json',
                   success:function(data)
                    {
                    $(".info_block_head").text(data.heading);
                    $("#headings").text(data.heading);       
                    $("#content").html(data.head_content);
                    $("#method").text(data.method_content);
                    var info_images=data.images; 
		    var ajax_image = "<img src='http://115.112.118.252/VirtualEST/assets/images/est_anthrop/"+info_images+"'/>";
                    $(".info_img").html(ajax_image);
                    }
                 })   
                $(".info_block_new").toggle();
                $(".overlay").toggle();
            });
	$(document).on('click','.close_new', function(){
		$(".info_block_new").toggle();
		$(".overlay").toggle();
	}); 	
</script>
    

    
<script>
$(document).on('click','#restricted_profile', function(){
        $("#restricted_profile").attr("href", "<?php echo site_url('Body/restricted_profile'); ?>");
        document.forms["myform"].submit();
	});  
    
    
$(document).on('click','#full_profile', function(){
         //alert('done doen');
        $("#full_profile").attr("href", "<?php echo site_url('Body/full_profile'); ?>");
		document.forms["myform"].submit();	  
	});  
    
$(document).on('click','#error_analysis', function(){
		document.forms["myform"].submit();	  
        $("#error_analysis").attr("href", "<?php echo site_url('Body/error_analysis'); ?>");
	});  
</script>  
        
    
    
    
    
    
</head>
<body>
   <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],1);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="<?php echo $_SESSION['bodyfat']; ?>">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Restricted Profile</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<form name="vpform" id="vprestrictedgen" method="post"> 
                <a href="#" class="virtual_btnrestricted"><img src="<?php echo "$base/assets/images/"?>virtual_icon.png" style="margin-top: 12px;"></a>
                <input type="hidden" name="vptype" value="restricted">
            </form> 
        </div>
		
		<div class="vitual_div">
			
		</div>
		<div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Restricted Profile</div>
            <p>The Restricted Profile allows the user to enter up to 9 skinfolds, 5 girths and 2 bone breadths plus height and body mass and calculate a range of variables including:
			phantom z-scores for each anthropometry variable, % body fat using a range of prediction equations, skinfold sums and skinfold maps, somatotype, and plot variables against 
			national age- and sex-based norms. Click the ‘Virtual Profile’ button to  generate a ‘virtual person’ where plausible values are generated in the program to be used in the
			various analyses. These can also be changed as the user explores the various analyses and outputs.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/restricted_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php //echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php //echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php //echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Restricted Profile</div>
            
            <div class="field_row verticle_field_row" style="border-bottom:0; padding-bottom:0;"> 
                <div class="field_24">
                	<label>Gender</label>
                	<!--input type="text" id="gender" name="gender" value="<?php //if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>"--> 
                	<input type="text" id="gender" name="gender" value="<?php if(isset($_SESSION['user_gender']))
                                                                              {
                                                                                if($_SESSION['user_gender'] === "M")
                                                                                 { echo "Male" ;}else{ echo "Female" ;}
                                                                              }
                                                                              else
                                                                              {
                                                                                if($fieldData[0]->gender == "Male"){ echo "Male" ;}else{ echo "Female" ;}
                                                                              }
                                                                        ?>"> 
                </div>
                <div class="field_24">
                	<label>Age [yr]</label>
                	<input type="text" id="age" name="age" value="<?php if(!empty($fieldData[0]->age)){echo $fieldData[0]->age;}else{echo $_SESSION['age'] ;}?>">
                </div>
                <div class="field_24">
                	<label>Height [cm]</label>
                    <input type="text" id="height" name="height" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo round($_SESSION['HEIGHT'],1); ?>" style="width:180px;">                         
                </div>
                <div class="field_24">
                	<label>Body mass [kg]</label>
                    <input type="text" id="body_mass" name="body_mass" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo $_SESSION['MASS']; ?>">
                </div>
             </div>
             
             <div class="field_row verticle_field_row"> 
                 <div class="field_24">
                    <label>Create virtual profile</label>
                    <button id="virtual_profile" name="virtual_profile" class="lite_btn btn_virtual">Virtual Profile</button>
                    <button id="clear_profile" name="clear_profile" class="lite_btn btn_virtual" style="display:none;">Clear</button>
                </div>
            </div>
           
            <div class="field_row verticle_field_row" style="border:0;"> 
                <div class="field_24">
                	<strong class="color_vio">Skinfolds (mm)</strong>
                    
                    <div class="resp_field">
                    	<label>Triceps<a href="#" class="info_block_new_btn" id="1"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="triceps" name="triceps" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                           if(isset($fieldData[0]->triceps))
                           {
                               echo $fieldData[0]->triceps;
                           }
                            else
                            {
                                echo $_SESSION['triceps']; 
                            }
                      
                        ?>">
                    </div>  
                     <div class="resp_field">
                    	<label>Subscapular<a href="#" class="info_block_new_btn" id="2"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="subscapular" name="subscapular" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                           if(isset($fieldData[0]->subscapular))
                           {
                               echo $fieldData[0]->subscapular;
                           }
                            else
                            {
                                echo $_SESSION['subscapular']; 
                            }
                        
                        ?>"> 
                     </div>   
                    <div class="resp_field">
                    	<label>Biceps<a href="#" class="info_block_new_btn" id="3"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="biceps" name="biceps" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->biceps))
                           {
                               echo $fieldData[0]->biceps;
                           }
                            else
                            {
                                echo $_SESSION['biceps']; 
                            }
                         ?>">
                    </div>   
                    <div class="resp_field">
                    	<label>Iliac crest<a href="#" class="info_block_new_btn" id="4"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="iliac_crest" name="iliac_crest" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->iliac_crest))
                           {
                               echo $fieldData[0]->iliac_crest;
                           }
                            else
                            {
                                echo $_SESSION['iliac_crest']; 
                            }
                        ?>">
                    </div>  
                    <div class="resp_field">
                    	<label>Supraspinale<a href="#" class="info_block_new_btn" id="5"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="supraspinale" name="supraspinale" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                           if(isset($fieldData[0]->supraspinale))
                           {
                               echo $fieldData[0]->supraspinale;
                           }
                            else
                            {
                                echo $_SESSION['supraspinale']; 
                            }
                        ?>">
                    </div>  
                    <div class="resp_field">
                    	<label>Abdominal<a href="#" class="info_block_new_btn" id="6"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="abdominal" name="abdominal" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                        
                        if(isset($fieldData[0]->abdominal))
                           {
                               echo $fieldData[0]->abdominal;
                           }
                            else
                            {
                                echo $_SESSION['abdominal']; 
                            }
                        ?>">
                    </div>  
                    <div class="resp_field">
                    	<label>Front thigh<a href="#" class="info_block_new_btn" id="7"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="thigh" name="thigh" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                        
                         if(isset($fieldData[0]->thigh))
                           {
                               echo $fieldData[0]->thigh;
                           }
                            else
                            {
                                echo $_SESSION['thigh']; 
                            }
                        ?>">
                    </div>  
                    <div class="resp_field">
                    	<label>Medial calf<a href="#" class="info_block_new_btn" id="8"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="calf" name="calf" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                        
                         if(isset($fieldData[0]->calf))
                           {
                               echo $fieldData[0]->calf;
                           }
                            else
                            {
                                echo $_SESSION['calf']; 
                            }
                      ?>">
                    </div>  
                    <div class="resp_field">
                    	<label>Mid-axilla<a href="#" class="info_block_new_btn" id="9"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="mid_axilla" name="mid_axilla" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                        
                         if(isset($fieldData[0]->mid_axilla))
                           {
                               echo $fieldData[0]->mid_axilla;
                           }
                            else
                            {
                                echo $_SESSION['mid_axilla']; 
                            }
                         ?>">
                    </div>                    
                </div>
                <div class="field_24">
                	<strong class="color_vio">Girths (cm)</strong>
                    
                    <div class="resp_field">
                    	<label>Arm (relaxed)<a href="#" class="info_block_new_btn" id="12"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="relArmG" name="relArmG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->relArmG))
                           {
                               echo $fieldData[0]->relArmG;
                           }
                            else
                            {
                                echo $_SESSION['relArmG']; 
                            }
                        ?>">
                    </div>
                    <div class="resp_field">
                    	<label>Arm (flexed)<a href="#" class="info_block_new_btn" id="13"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="flexArmG" name="flexArmG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->flexArmG))
                           {
                               echo $fieldData[0]->flexArmG;
                           }
                            else
                            {
                                echo $_SESSION['flexArmG']; 
                            }
                         ?>">
                    </div>
                    <div class="resp_field">
                    	<label>Waist (minimum)<a href="#" class="info_block_new_btn" id="17"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="waistG" name="waistG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->waistG))
                           {
                               echo $fieldData[0]->waistG;
                           }
                            else
                            {
                                echo $_SESSION['waistG']; 
                            }
                        ?>">
                    </div>
                    <div class="resp_field">
                    	<label>Gluteal (hips)<a href="#" class="info_block_new_btn" id="18"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="hipG" name="hipG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->hipG))
                           {
                               echo $fieldData[0]->hipG;
                           }
                            else
                            {
                                echo $_SESSION['hipG']; 
                            }
                        
                       ?>">
                    </div>
                    <div class="resp_field">
                    	<label>Calf (maximum)<a href="#" class="info_block_new_btn" id="21"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="calfG" name="calfG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                          if(isset($fieldData[0]->calfG))
                           {
                               echo $fieldData[0]->calfG;
                           }
                            else
                            {
                                echo $_SESSION['calfG']; 
                            }
                        ?>">
                    </div>
                </div>
                <div class="field_24">
                	<strong class="color_vio">Breadths (cm)</strong>
                    
                    <div class="resp_field">
                    	<label>Humerus<a href="#" class="info_block_new_btn" id="40"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="humerus" name="humerus" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                          if(isset($fieldData[0]->humerus))
                           {
                               echo $fieldData[0]->humerus;
                           }
                            else
                            {
                                echo $_SESSION['humerus']; 
                            }
                          ?>"> 
                    </div>
                    <div class="resp_field">
                    	<label>Femur<a href="#" class="info_block_new_btn" id="41"><img src="<?php echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="femur" name="femur" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php 
                         if(isset($fieldData[0]->femur))
                           {
                               echo $fieldData[0]->femur;
                           }
                            else
                            {
                                echo $_SESSION['femur']; 
                            }
                        ?>">
                    </div>
                </div>
             </div> 
              <!-- Block for Information Popup-->
            <div class="info_block_new">
                        <div class="info_block_head" style="background: #906bd0;">Full Profile</div>
                        <div class="info_content">
                            <div class="info_img">
                               
                            </div>
                            <div class="info_txt">
                                <h3 id="headings"></h3>	
                                <p id="content"></p>
                                <div class="clearfix">&nbsp;</div>
                                <h3>Method</h3>
                                <p id="method"></p>
                            </div>
                        </div>
                        <div class="info_block_foot">
                            <a href="#" class="lite_btn grey_btn f_right close_new">Close</a>
                        </div>                
                    </div> 
             <div class="bottom_tabs">
             	<div class="lite_btn" id="phantom" name="phantom" onclick="getPhantomZscore()">Phantom</div>
                 <div class="lite_btn" id="body_fat" name="body_fat" onclick="getBodyFat()">% Body fat</div>
                 <div class="lite_btn" id="skinfold" name="skinfold" onclick="getSkinfolds()">Skinfolds</div>
                 <div class="lite_btn" id="somatotype" name="somatotype" onclick="getSomatotype()">Somatotype</div>
                 <div class="lite_btn" id="anthropometry" name="anthropometry" onclick="getAnthropometry()">Norms</div>
             <!--<div class="lite_btn" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';">Fractionation</div>-->
             </div>
             
             <input type="hidden" id="zscore_Triceps" name="zscore_Triceps">
            <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular">
            <input type="hidden" id="zscore_Biceps" name="zscore_Biceps">
            <input type="hidden" id="zscore_Iliac" name="zscore_Iliac">
            <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale">
            <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal">
            <input type="hidden" id="zscore_Thigh" name="zscore_Thigh">
            <input type="hidden" id="zscore_Calf" name="zscore_Calf">
            <input type="hidden" id="zscore_RelArmG" name="zscore_RelArmG">
            <input type="hidden" id="zscore_FlexArmG" name="zscore_FlexArmG">
            <input type="hidden" id="zscore_WaistG" name="zscore_WaistG">
            <input type="hidden" id="zscore_HipG" name="zscore_HipG">
            <input type="hidden" id="zscore_CalfG" name="zscore_CalfG">
            <input type="hidden" id="zscore_Humerus" name="zscore_Humerus">
            <input type="hidden" id="zscore_Femur" name="zscore_Femur">     
        </div>
    </div>


    <input type="hidden" id="exit_key" name="exit_key" value="">
    <input type="hidden" id="action_key" name="action_key" value="">
    <input type="hidden" id="profile" name="profile" value="restricted">
        
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
    
         $(window).bind("load", function() {
             
          if(document.getElementById("height").value !== "" && 
          document.getElementById("body_mass").value !== "" &&           
          document.getElementById("triceps").value !== "" &&    
          document.getElementById("subscapular").value !== "" &&    
          document.getElementById("biceps").value !== "" &&    
          document.getElementById("iliac_crest").value !== "" &&    
          document.getElementById("supraspinale").value !== "" &&    
          document.getElementById("abdominal").value !== "" &&    
          document.getElementById("thigh").value !== "" &&
          document.getElementById("calf").value !== "" &&    
          document.getElementById("mid_axilla").value !== "" &&    
          document.getElementById("relArmG").value !== "" &&    
          document.getElementById("flexArmG").value !== "" &&    
          document.getElementById("waistG").value !== "" &&    
          document.getElementById("hipG").value !== "" &&    
          document.getElementById("calfG").value !== "" &&    
          document.getElementById("humerus").value !== "" &&    
          document.getElementById("femur").value !== "")
          {      
           document.getElementById('clear_profile').style.display = "" ;
           document.getElementById('virtual_profile').style.display = "none" ;
          }          
          
         }); 

    	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
        });	

        document.getElementById("phantom").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("body_fat").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("skinfold").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("virtual_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("clear_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
     	
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','#virtual_profile', function(){ 
          
          document.getElementById("height").value = 179.3 ; 
          document.getElementById("body_mass").value = 74.84 ;             
          document.getElementById("triceps").value = 5 ;    
          document.getElementById("subscapular").value = 8.6 ;    
          document.getElementById("biceps").value = 3.5 ;    
          document.getElementById("iliac_crest").value = 6.5 ;    
          document.getElementById("supraspinale").value = 3.4 ;    
          document.getElementById("abdominal").value = 4.7 ;    
          document.getElementById("thigh").value = 9.2 ;
          document.getElementById("calf").value = 5.4 ;    
          document.getElementById("mid_axilla").value = 4.5 ;    
          document.getElementById("relArmG").value = 33.1 ;    
          document.getElementById("flexArmG").value = 35.5 ;    
          document.getElementById("waistG").value = 75.2 ;    
          document.getElementById("hipG").value = 98 ;    
          document.getElementById("calfG").value = 42 ;    
          document.getElementById("humerus").value = 7.43 ;    
          document.getElementById("femur").value = 10.97 ;    
          
          document.getElementById('clear_profile').style.display = "" ;
          document.getElementById('virtual_profile').style.display = "none" ; 
         
        });
        
        $(document).on('click','#clear_profile', function(){ 
          
          document.getElementById("height").value = "" ; 
          document.getElementById("body_mass").value = "" ;             
          document.getElementById("triceps").value = "" ;    
          document.getElementById("subscapular").value = "" ;    
          document.getElementById("biceps").value = "" ;    
          document.getElementById("iliac_crest").value = "" ;    
          document.getElementById("supraspinale").value = "" ;    
          document.getElementById("abdominal").value = "" ;    
          document.getElementById("thigh").value = "" ;
          document.getElementById("calf").value = "" ;    
          document.getElementById("mid_axilla").value = "" ;    
          document.getElementById("relArmG").value = "" ;    
          document.getElementById("flexArmG").value = "" ;    
          document.getElementById("waistG").value = "" ;    
          document.getElementById("hipG").value = "" ;    
          document.getElementById("calfG").value = "" ;    
          document.getElementById("humerus").value = "" ;    
          document.getElementById("femur").value = "" ;    
         
          document.getElementById('virtual_profile').style.display = "" ;
          document.getElementById('clear_profile').style.display = "none" ; 
        });
        
</script>  

<script>   
   //calculate phantom z-score value
    function getPhantomZscore()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

           if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || relArmG == "" || flexArmG == "" || waistG == "" || hipG == "" || calfG == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 1 ;
            document.forms["myform"].submit();	  
           }  
          else
          { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 1 ;
            document.forms["myform"].submit();	  
          }   
        }
    }
 
 
   //calculate Body Fat value
    function getBodyFat()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || relArmG == "" || flexArmG == "" || waistG == "" || hipG == "" || calfG == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 2 ;
            document.forms["myform"].submit();
           }  
          else
          { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 2 ;
            document.forms["myform"].submit();
          }  

        }
    }
 

 //calculate skinfolds value
    function getSkinfolds()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       
            var mid_axilla = document.getElementById("mid_axilla").value ;            
            
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 
            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || mid_axilla == "")
            {
               alert ("All Skinfolds values should be entered");
               //return false;
            }  
            else
            {
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur); 

               document.getElementById("exit_key").value = 0 ;
               document.getElementById("action_key").value = 3 ;
               document.forms["myform"].submit();              
            }        
        }
    }
    


 //calculate somatotype value
    function getSomatotype()
    {          
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       
            var mid_axilla = document.getElementById("mid_axilla").value ;            
            
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 
            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || supraspinale == "" || calf == "" || flexArmG == "" || calfG == "" || humerus == "" || femur == "")
            {
             alert ("Some values are missing");          
            }  
            else
            {
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur); 

               document.getElementById("exit_key").value = 0 ;
               document.getElementById("action_key").value = 4 ;
               document.forms["myform"].submit();              
            }        
        }
    } 
    
   
//calculate anthropometry value
    function getAnthropometry()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

           if (triceps == "" || subscapular == "" || thigh == "" || relArmG == "" || waistG == "" || hipG == "")
           {
             alert ("Some values are missing");          
           }  
           else
           { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 5 ;
            document.forms["myform"].submit();	  
           }   
        }
    }
    
   
     function phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur)
     {
            ht = Math.round(parseFloat(ht) * 10) / 10 ;  //upto 2 decimal places  
            mass = Math.round(parseFloat(mass) * 100) / 100 ;  //upto 2 decimal places
              
            triceps = Math.round(parseFloat(triceps) * 10) / 10 ;
            subscapular = Math.round(parseFloat(subscapular) * 10) / 10 ;
            biceps = Math.round(parseFloat(biceps) * 10) / 10 ;
            iliac = Math.round(parseFloat(iliac) * 10) / 10 ;
            supraspinale = Math.round(parseFloat(supraspinale) * 10) / 10 ;
            abdominal = Math.round(parseFloat(abdominal) * 10) / 10 ;
            thigh = Math.round(parseFloat(thigh) * 10) / 10 ;
            calf = Math.round(parseFloat(calf) * 10) / 10 ;
            relArmG = Math.round(parseFloat(relArmG) * 10) / 10 ;
            flexArmG = Math.round(parseFloat(flexArmG) * 10) / 10 ;
            waistG = Math.round(parseFloat(waistG) * 10) / 10 ;
            hipG = Math.round(parseFloat(hipG) * 10) / 10 ;
            calfG = Math.round(parseFloat(calfG) * 10) / 10 ;
            humerus = Math.round(parseFloat(humerus) * 100) / 100 ; //upto 2 decimal places
            femur = Math.round(parseFloat(femur) * 100) / 100 ; //upto 2 decimal places
            
            
        // Skinfolds Phantom Zscores    
            var zscore_Triceps = Math.round([(parseFloat(triceps) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
            var zscore_Subscapular = Math.round([(parseFloat(subscapular) * (170.18 / parseFloat(ht)) - 17.2) / 5.07] * 10) / 10 ;
            var zscore_Biceps= Math.round([(parseFloat(biceps) * (170.18 / parseFloat(ht)) - 8) / 2] * 10) / 10 ;
            var zscore_Iliac = Math.round([(parseFloat(iliac) * (170.18 / parseFloat(ht)) - 22.4) / 6.8] * 10) / 10 ;
            var zscore_Supspinale = Math.round([(parseFloat(supraspinale) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
            var zscore_Abdominal = Math.round([(parseFloat(abdominal) * (170.18 / parseFloat(ht)) - 25.4) / 7.78] * 10) / 10 ;
            var zscore_Thigh = Math.round([(parseFloat(thigh) * (170.18 / parseFloat(ht)) - 27) / 8.33] * 10) / 10 ;
            var zscore_Calf = Math.round([(parseFloat(calf) * (170.18 / parseFloat(ht)) - 16) / 4.67] * 10) / 10 ;

        // Girths Phantom Zscores    
            var zscore_RelArmG = Math.round([(parseFloat(relArmG) * (170.18 / parseFloat(ht)) - 26.89) / 2.33] * 10) / 10 ;
            var zscore_FlexArmG = Math.round([(parseFloat(flexArmG) * (170.18 / parseFloat(ht)) - 29.41) / 2.37] * 10) / 10 ;
            var zscore_WaistG = Math.round([(parseFloat(waistG) * (170.18 / parseFloat(ht)) - 71.91) / 4.45] * 10) / 10 ;
            var zscore_HipG = Math.round([(parseFloat(hipG) * (170.18 / parseFloat(ht)) - 94.67) / 5.58] * 10) / 10 ;
            var zscore_CalfG = Math.round([(parseFloat(calfG) * (170.18 / parseFloat(ht)) - 35.25) / 2.3] * 10) / 10 ;

        // Breadths Phantom Zscores
            var zscore_Humerus = Math.round([(parseFloat(humerus) * (170.18 / parseFloat(ht)) - 6.48) / 0.35] * 10) / 10 ;
            var zscore_Femur = Math.round([(parseFloat(femur) * (170.18 / parseFloat(ht)) - 9.52) / 0.48] * 10) / 10 ;	  

            document.getElementById("zscore_Triceps").value = parseFloat(zscore_Triceps) ;
            document.getElementById("zscore_Subscapular").value = parseFloat(zscore_Subscapular) ;
            document.getElementById("zscore_Biceps").value = parseFloat(zscore_Biceps) ;
            document.getElementById("zscore_Iliac").value = parseFloat(zscore_Iliac) ;
            document.getElementById("zscore_Supspinale").value = parseFloat(zscore_Supspinale) ;
            document.getElementById("zscore_Abdominal").value = parseFloat(zscore_Abdominal) ;
            document.getElementById("zscore_Thigh").value = parseFloat(zscore_Thigh) ;
            document.getElementById("zscore_Calf").value = parseFloat(zscore_Calf) ;
            document.getElementById("zscore_RelArmG").value = parseFloat(zscore_RelArmG) ;
            document.getElementById("zscore_FlexArmG").value = parseFloat(zscore_FlexArmG) ;
            document.getElementById("zscore_WaistG").value = parseFloat(zscore_WaistG) ;
            document.getElementById("zscore_HipG").value = parseFloat(zscore_HipG) ;
            document.getElementById("zscore_CalfG").value = parseFloat(zscore_CalfG) ;
            document.getElementById("zscore_Humerus").value = parseFloat(zscore_Humerus) ;
            document.getElementById("zscore_Femur").value = parseFloat(zscore_Femur) ;
     }    
    $(document).on('click','.virtual_btnrestricted', function(){
    $("#vprestrictedgen").attr("action", "<?php echo base_url(); ?>index.php/Body/BCVirtualpersonGeneration");     
        $("#vprestrictedgen").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
       $(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
		
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 
</script>
    
</body>
</html>
