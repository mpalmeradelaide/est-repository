<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Risk Factors</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script type="text/javascript">
 
 
  function compareValue() {
   
 val7 = parseInt($('#options_7').val());
  val8 =parseInt($('#options_8').val());
  
  
  if(val7=='')val7=0;
  if(val8=='')val8=0;
  
  console.log(val7);
  console.log(val8);
 if(val8 > val7){
    alert("Please check your blood pressure values");
 $('#options_8').val("");
 }
  }   


  function handleChangeSbp(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 240;
 compareValue()
  }  
    
  function handleChangeDbp(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 140;
    compareValue()
  }  
  function handleChangeAge(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 100;
  }  
    
  $(function() {
    $('.two-decimal').keyup(function(){
   if($(this).val().indexOf('.')!=-1){         
       if($(this).val().split(".")[1].length > 2){                
           if( isNaN( parseFloat( this.value ) ) ) return;
           this.value = parseFloat(this.value).toFixed(2);
       }  
    }            
    return this; //for chaining
 });  
 
  $('.one-decimal').keyup(function(){
   if($(this).val().indexOf('.')!=-1){         
       if($(this).val().split(".")[1].length > 1){                
           if( isNaN( parseFloat( this.value ) ) ) return;
           this.value = parseFloat(this.value).toFixed(1);
       }  
    }            
    return this; //for chaining
 }); 
   <?php if($fieldData[0]->option_gender != 'F'){ ?>
      $("#option_male").attr('checked', 'checked');
 <?php } ?>    
          
      
   
   var age ='<?php  echo $fieldData[0]->option_1 !=''?$fieldData[0]->option_1:"blank";?>';   
   var smoke ='<?php  echo $fieldData[0]->option_2 !=''?$fieldData[0]->option_2:"blank";?>';   
   var smoke_6 ='<?php  echo $fieldData[0]->option_3 !=''?$fieldData[0]->option_3:"blank";?>';   
   //console.log(age);
   if(age != 'Y')
       { 
    $("#content").hide();
       }
   if(smoke != 'Y'){
    $("#content_option").hide();
     }
    if(smoke_6 != 'Y'){ 
    $("#content_option2").hide();
    }
	
	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
			 if ($(window).scrollTop() > 100) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});  
	
});  
function toggleDiv(divId) {

  if($('[name=options_1]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}

function toggleDiv2(divId) {

  if($('[name=options_2]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}


function toggleDiv3(divId) {

  if($('[name=options_3]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}


     var specialKeys = new Array();
      specialKeys.push(8); //Backspace
        function IsNumeric(e) {
        /*   
        var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        if(!ret)  { 
        alert('Only Numbers');
        }
            return ret;
            */
          
                var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0)){
                e.preventDefault();
           // alert('only Numbers');
            //CUSTOM_ALERT.alert('','Number Only');
            }
      // theNum = document.getElementById().value;
          
 
       
        }
       
        
      
   function scrollWin()
    {
    window.scrollBy(0,300);
    }    

        
</script>

</head>
<script type="text/javascript">
$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
<!--<body onLoad="scrollWin()">-->
<body>
<?php
      $hidden = array('userid' => $id, 'novalidate');
      echo form_open('welcome/saveClientRiskFactorInfo','',$hidden); ?>  
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitRiskFactor" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg" title="Back to Home Screen"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Risk Factors</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Risk Factors</div>
            <p>The RISK FACTORS listed on this screen cover items that are important for your health. Please click a response for each question.
               A family history of heart disease may be a risk for you through a genetic or familial association. Smoking is a well-established 
               risk factor and proportional to the number of cigarettes smoked per day. Answer the questions on blood pressure, cholesterol and 
               blood sugar [glucose] as best you can by recalling conversations you may have had with your medical practitioner or by entering actual 
               values for these risk factor variables in the fields provided. Most people don't have immediate access to these measures or can't recall them, 
               so leave them blank if this is the case.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>   
    
    </div>
</div>

<!--Start Wrapper --> 
<div class="wrapper">

<div id="CustomAlert">
 <div id="CustomAlertMessage"></div>
 <input type="button" value="OK" onClick="CUSTOM_ALERT.hide();" id="CustomAlertSampleokButton">
</div>

<!--Start login --> 
<div class="login-cont" style="display:none;">
    <?php   echo form_open('welcome/saveClientRiskFactorInfo','',$hidden); ?>  
<!--	<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
  <!-- <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitRiskFactor" type="submit" value=""  title="edit client details"/></span>
     </div> -->
   <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required><input name="submitRiskFactor" type="submit" value="" title="edit client details"/></span>
     </div>
<!--	</form> -->
</div>
<!--End login --> 

 <!--Start Mid --> 
 
   
<!--Start contain --> 
<div class="contain">
   
   <!--Start left --> 
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn active" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			<div class="btn">
            <button type="submit" name="mysubmit7" class="left_panel_btn" id="myform7"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Absolute CVD risk</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			
			<div class="btn">
            <button type="submit" name="mysubmit8" class="left_panel_btn active" id="myform8"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Life Expectancy</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
			
       </div>
   <!--End Left --> 
 
 <?php //print_r($fieldData); ?>
 
   <!--Start right --> 
   <div class="right">
   		<div class="right-head" style="margin-bottom:0;">Risk Factors</div>
   		<div class="right-section page_medi">
       	<div class="field_row checkbox_title_row" style="border:0;">
      		<div class="field_85">&nbsp;</div>
            <div class="field_15">
            	<span>Yes</span>
            	<span>No</span>                
            </div>        
      </div>
      
       <p style="font-size:18px; margin-bottom:15px;">Family History of Heart Disease</p>
       <div class="field_row">
              <div class="field_85">Have you ever had a male relative [father, brother, son] or a female relative [mother, sister, daughter] who has had a myocardial infarction or bypass operation, or died suddenly of a heart attack before the age of 65?</div>
              <div class="field_15">
			<?php $radio_is_checked = ($fieldData[0]->option_1 === 'Y')?"checked":"";
            echo form_radio(array("name"=>"options_1","id"=>"options_1Y","value"=>"Y","class"=>"gender" ,"onClick"=>"toggleDiv('content')" ,"checked"=>$radio_is_checked)); ?>  <label for="options_1Y"><span></span></label>            
			  <?php  $radio_is_checked = ($fieldData[0]->option_1 === 'N')?"checked":"";
          echo form_radio(array("name"=>"options_1","id"=>"options_1N","value"=>"N","class"=>"gender","onClick"=>"toggleDiv('content')" ,"checked"=>$radio_is_checked)); ?> <label for="options_1N"><span></span></label>
            </div>
            <div class="clearfix">&nbsp;</div>
             <div id="content" style="margin-top:30px;"> 
              <div class="half_container f_left">
            	<div class="field_50">Gender of relative</div>
                <div class="field_50">
                	 <?php $radio_is_checked = ($fieldData[0]->option_gender === 'M')?"checked":""; ?>
                    <input type="radio" name="option_gender" id="male" value="M" <?php echo $radio_is_checked; ?>> <label for="male" style="display:inline-block; margin-right:10px;"><span style="margin-right:8px;"></span>Male</label>     
                    <?php $radio_is_checked = ($fieldData[0]->option_gender === 'F')?"checked":""; ?>
                    <input type="radio" name="option_gender" id="female" value="F" <?php echo $radio_is_checked; ?>> <label for="female" style="display:inline-block;"><span style="margin-right:8px;"></span>Female</label>     
                </div>
            </div>  
            
            <div class="half_container f_right">
            	<div class="field_50">At what age (yr)</div>
                <div class="field_50">
					<?php   $option_age = $fieldData[0]->option_age==''?'':$fieldData[0]->option_age; ?>
                    <input type="number" name="option_age" value="<?php echo $option_age; ?>" onchange="handleChangeAge(this)">
                </div>
            </div>
                       
            </div>
            
      	</div>
        
       
   
  
  
   <p style="font-size:18px; margin-bottom:15px;">Smoking</p>

      <div class="field_row">
              <div class="field_85">Do you smoke?</div>
              <div class="field_15"> 
			<?php $radio_is_checked = ($fieldData[0]->option_2 === 'Y')?"checked":"";
            echo form_radio(array("name"=>"options_2","id"=>"options_2Y","value"=>"Y",'class'=>'gender',"onClick"=>"toggleDiv2('content_option')" ,"checked"=>$radio_is_checked )); ?><label for="options_2Y"><span></span></label>  
            <?php $radio_is_checked = ($fieldData[0]->option_2 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_2","id"=>"options_2N","value"=>"N",'class'=>'gender',"onClick"=>"toggleDiv2('content_option')" ,"checked"=>$radio_is_checked)); ?><label for="options_2N"><span></span></label> 
			</div>
      	
		<div class="clearfix">&nbsp;</div>
      <div id="content_option">
        <div class="field_85">How many do you smoke per day? (/day)</div>
         <div class="field_15" style="margin-bottom:20px;">
			 <?php   $option_smoke = $fieldData[0]->option_smoke==''?'':$fieldData[0]->option_smoke; ?>
            <input type="number" name="option_smoke" value="<?php echo $option_smoke; ?>">
        </div>
     </div> 

		<div class="clearfix">&nbsp;</div>
      <div class="field_85">Have you quit smoking in the last 6 months?</div>
      <div class="field_15">
      <?php $radio_is_checked = ($fieldData[0]->option_3 === 'Y')?"checked":""; 
        echo form_radio(array("name"=>"options_3","id"=>"options_3Y","value"=>"Y",'class'=>'gender',"onClick"=>"toggleDiv3('content_option2')","checked"=>$radio_is_checked)); ?> <label for="options_3Y"><span></span></label> 
	  <?php $radio_is_checked = ($fieldData[0]->option_3 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_3","id"=>"options_3N","value"=>"N",'class'=>'gender',"onClick"=>"toggleDiv3('content_option2')" ,"checked"=>$radio_is_checked)); ?> <label for="options_3N"><span></span></label> 
		</div>
        <div class="clearfix">&nbsp;</div>
        <div id="content_option2">
        	<div class="field_85" style="margin-top:10px;">How many did you smoke per day? (/day)</div>
            <div class="field_15" style="margin-top:10px;"><?php   $option_smoke_6 = $fieldData[0]->option_smoke_6==''?'':$fieldData[0]->option_smoke_6; ?>
            <input type="number" name="option_smoke_6" value="<?php echo $option_smoke_6; ?>">
            </div>
        </div> 
        
      </div>
       
       
      
      <p style="font-size:18px; margin-bottom:15px;">Self-Report</p>
      <div class="field_row">
            <div class="field_85">Have you been told you have high blood pressure?</div>
            <div class="field_15">
            	<?php $radio_is_checked = ($fieldData[0]->option_4 === 'Y')?"checked":""; 
                echo form_radio(array("name"=>"options_4","id"=>"options_4Y","value"=>"Y",'class'=>'gender',"checked"=>$radio_is_checked )); ?> <label for="options_4Y"><span></span></label> 
                <?php $radio_is_checked = ($fieldData[0]->option_4 === 'N')?"checked":""; 
                echo form_radio(array("name"=>"options_4","id"=>"options_4N","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?> <label for="options_4N"><span></span></label>                 
            </div>
      		<div class="clearfix">&nbsp;</div>
      		<div class="field_85">Have you been told you have high cholesterol?</div>
            <div class="field_15">
            <?php $radio_is_checked = ($fieldData[0]->option_5 === 'Y')?"checked":""; 
            echo form_radio(array("name"=>"options_5","id"=>"options_5Y","value"=>"Y",'class'=>'gender' ,"checked"=>$radio_is_checked)); ?> <label for="options_5Y"><span></span></label> 
			<?php $radio_is_checked = ($fieldData[0]->option_5 === 'N')?"checked":""; 
            echo form_radio(array("name"=>"options_5","id"=>"options_5N","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?> <label for="options_5N"><span></span></label> 
            </div>
      		<div class="clearfix">&nbsp;</div>
      		<div class="field_85">Have you been told you have high blood sugar?</div>
            <div class="field_15">
            <?php $radio_is_checked = ($fieldData[0]->option_6 === 'Y')?"checked":""; 
            echo form_radio(array("name"=>"options_6","id"=>"options_6Y","value"=>"Y",'class'=>'gender',"checked"=>$radio_is_checked )); ?> <label for="options_6Y"><span></span></label> 
			<?php $radio_is_checked = ($fieldData[0]->option_6 === 'N')?"checked":""; 
            echo form_radio(array("name"=>"options_6","id"=>"options_6N","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?> <label for="options_6N"><span></span></label> 
            </div>
      </div>  
   
       <p style="font-size:18px; margin-bottom:15px;">Measured</p>
      		<div class="field_row"> 
            	<div class="half_container f_left">
                    <?php
                     $option_7 = $fieldData[0]->option_7==''?'':$fieldData[0]->option_7;
                     $attrib = array(
                        'name'        => 'options_7',
                        'id'          => 'options_7',
                        //'class'       => 'physi-text ',
                         'size'=>'20',
                         'value'=>$option_7,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onkeyup'=>'return onkeyup(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'type'=>'number',
                         'min'=>'0',						 
						 'onchange'=>'handleChangeSbp(this)'
                      );
                    
                    
                    ?>
                    
               
               <div class="field_65">SBP (mmHg)</div>			  
			   <div class="field_35"><?php echo form_input($attrib); ?></div>
              <div class="clearfix">&nbsp;</div>
              
               <?php
                $option_8 = $fieldData[0]->option_8==''?'':$fieldData[0]->option_8;
                $attrib = array(
                        'name'        => 'options_8',
                        'id'          => 'options_8',
                        //'class'       => 'physi-text  ',
                         'size'=>'20',
                         'value'=>$option_8,                        
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0',
						// 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        //'ondrop'=>'return false',
						 'onchange'=>'handleChangeDbp(this)'              

                      );
                    
                    
                    ?>
                    
                    <div class="field_65">DBP (mmHg)</div>
                    <div class="field_35"><?php echo form_input($attrib); ?></div>
              		<div class="clearfix">&nbsp;</div>
               <?php 
                $option_12 = $fieldData[0]->option_12==''?'':$fieldData[0]->option_12;
                $attrib = array(
                        'name'        => 'options_12',
                        'id'          => 'options_12',
                        //'class'       => 'physi-text two-decimal ',
                         'size'=>'20',
                         'value'=>$option_12,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0',
                       //  'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                      //  'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              
              <div class="field_65">Fasting blood glucose (mM)</div>
              <div class="field_35"><?php echo form_input($attrib); ?></div>  
             	<div class="clearfix">&nbsp;</div>
                <?php 
                 $option_11 = $fieldData[0]->option_11==''?'':$fieldData[0]->option_11;
                $attrib = array(
                        'name'        => 'options_11',
                        'id'          => 'options_11',
                        //'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_11,
                        // 'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                         
                      );
                    
                    ?>
                    
              <div class="field_65">Total blood cholesterol (mM)</div>
			  <div class="field_35"><?php echo form_input($attrib); ?></div>
              
              </div>
              
              <div class="half_container f_right">
               <?php
                $option_9 = $fieldData[0]->option_9==''?'':$fieldData[0]->option_9;
               $attrib = array(
                        'name'        => 'options_9',
                        'id'          => 'options_9',
                        //'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_9,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                        // 'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                         //'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
                    
                    
              
			  <div class="field_35"><?php echo form_input($attrib); ?></div>
              <div class="field_65 f_right field_65_r">HDL (mM)</div>
              <div class="clearfix">&nbsp;</div>
               <?php  
                $option_10 = $fieldData[0]->option_10==''?'':$fieldData[0]->option_10;
               $attrib = array(
                        'name'        => 'options_10',
                        'id'          => 'options_10',
                        //'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_10,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                         //'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              
			  <div class="field_35"><?php echo form_input($attrib); ?></div> 
              <div class="field_65 f_right field_65_r">LDL (mM)</div>
              	<div class="clearfix">&nbsp;</div>
               <?php
                $option_13 = $fieldData[0]->option_13==''?'':$fieldData[0]->option_13;
               $attrib = array(
                        'name'        => 'options_13',
                        'id'          => 'options_13',
                        //'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_13,
                         'type'=>'number',
                         'step'=>'any'
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              
			  <div class="field_35"><?php echo form_input($attrib); ?></div>
              <div class="field_65 f_right field_65_r">Triglycerides (mM)</div>
 
  		</div>
        </div>
        
      <?php echo form_submit('mysubmit2','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform2'");?>
      <?php echo form_submit('mysubmit4','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform4'");?>
       
      <?php echo form_close(); ?>
      
   </div><!--End right --> 
   
   
</div><!--End contain -->
<!--
<div class="footer risk-footer">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Wrapper --> 
 <div id="confirmBox">
    <div class="message"></div>
    <span class="button yes">New Client</span>
    <span class="button no">Cancel</span>
     </div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>	
</body>
</html>
