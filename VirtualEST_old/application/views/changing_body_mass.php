
<?php 
		//To show Hide Additional Info
	  if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
              $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
              $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
              $bodyfatdiv="block";    
            }
            else
            {
              $bodyfatdiv="none";    
            }
			$isvp=$_SESSION['is_virtual'];
			 
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">

<style>
.range_div{ display: inline-block; width:205px; margin: 12px 0;height: 346px; position: relative;} 	
.rangeslider{background:#e6e6e6 !important; box-shadow: none;}
.rangeslider__fill{background:#b3b3b3 !important;box-shadow: none;}
.rangeslider__handle{ background:#4d4d4d !important; width: 20px !important; height: 20px !important; z-index:99;}
.rangeslider--vertical { width:8px !important; height: 100%; left: 50%; margin-left: -4px;}
.rangeslider--vertical .rangeslider__handle { left: -6px !important; border: 0;}
.rangeslider__handle:after{display: none;}
	
.vio_range_div .rangeslider__fill{background:#c7b5e7 !important;}
.vio_range_div .rangeslider__handle{ background:#906bd0 !important;}
ul.range_numbers {list-style: none; margin: 0; padding: 0; position: absolute; left: 0; top:0; bottom: 0; z-index: -1;} 
ul.range_numbers li {color: #808080; font-size: 13px; width:78px; text-align: right; position:relative; margin-top:14.5px;}
ul.range_numbers li:before,ul.range_limits li:before{content: ''; width:20px; height: 1px; position: absolute; background:#e9e9e9; right:-35%; top: 8px;}
ul.range_numbers li:first-child{margin-top:2px;}
.range_div2 ul.range_numbers li{margin-top:21px;}
.range_div2 ul.range_numbers li:first-child{margin-top:3px;}
	
.range_div ul.range_numbers2 li{margin-top:21px;}
.range_div ul.range_numbers2 li:first-child{margin-top:3px;}
	
.range_div ul.range_numbers3 li{margin-top:17.5px;}
.range_div ul.range_numbers3 li:first-child{margin-top:3px;}	
	
	 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
	input[type="text"]{border-radius: 3px;}
</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		 var isvp='<?php echo $isvp;?>';
        if(isvp == 1)
        {
		  $(".v_person").fadeTo( "slow" , 1, function() {});
	      $(".v_detail").toggle();
		}
		$("#default").hide();
               
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 

$(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
     
    var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
	$text_box.val(this.value);
	calculate_currentFat($text_box);	
        });
  $text_box.on("change", function() {
  calculate_currentFat($text_box);
   $slider.val($text_box.val()).change();
});
      $text_box.val(this.value);   
	});
});
//Calculate Target Body Mass and how much client needs to Loss.
//Calculated on Range Button changes and Textbox value change
function calculate_currentFat()
 {
 $('#range_from').val('');
 $('#range_to').val('');
 $("#gain_loss_from").val(''); 
 $("#gain_loss_to").val('');  
 $("#gain_loss").val('');
        
 var current_mass =$("#body_mass").val(); 
 var current_fat_per =$("#body_fat_per").val(); 
 var cur_fat_mass =Math.round(current_mass*current_fat_per)/100;
 var desired_BF_per=$("#desired_body_fat").val(); 
 var per_mass_change=$("#per_mass_change").val();       
 var BF_per=Math.round(desired_BF_per)/100;      
 var FatRaio=per_mass_change/100;

 //=(cur_fat_mass-current_mass*BF_per)/(BF_per-FatRaio)
 var change_in_mass= parseFloat(((cur_fat_mass-current_mass*BF_per)/(BF_per-FatRaio)).toFixed(9));
 var target_body_mass=parseFloat(current_mass)+parseFloat(change_in_mass);
 var target_mass=parseFloat(target_body_mass).toFixed(1);       
 var DeltaMass=parseFloat(change_in_mass).toFixed(1); 
 var UpperDeltaMass=parseFloat(((current_mass*current_fat_per/100-BF_per * current_mass -0.84)/( BF_per -1 + 0.302)).toFixed(1));
 var LowerDeltaMass=parseFloat(((current_mass*current_fat_per/100-BF_per * current_mass -0.84)/( BF_per -1 + 0.398)).toFixed(1));
 var range_from=parseFloat(current_mass)+parseFloat(LowerDeltaMass);
 var range_start=parseFloat(range_from).toFixed(1); 
 var range_to=parseFloat(current_mass)+parseFloat(UpperDeltaMass);
 var range_end=parseFloat(range_to).toFixed(1);
 
    var textbind="(Mean body mass change "+DeltaMass+" kg)";
    $("#target_mass").val(target_mass); 
    $("#gain_loss").html(textbind);
    $("#default").hide();
    $("#body_fat_per_txt").html(current_fat_per);
 }
//
function calc_defaultValue()
 {
 $('input[name="slider_4"]').val('73.4').change();
 var current_mass =$("#body_mass").val(); 
 var current_fat_per =$("#body_fat_per").val(); 
 var cur_fat_mass =Math.round(current_mass*current_fat_per)/100;
 var desired_BF_per=$("#desired_body_fat").val(); 
 var per_mass_change=$("#per_mass_change").val();       
 var BF_per=Math.round(desired_BF_per)/100;      
 var FatRaio=per_mass_change/100;

 //(MASS*%body fat /100-%BF * MASS -0.84)/( %BF -1+0.330)= DeltaMass
 var change_in_mass=parseFloat((current_mass*current_fat_per/100-BF_per * current_mass -0.84)/( BF_per -1 + 0.330).toFixed(9));
 var DeltaMass=parseFloat(change_in_mass).toFixed(1);      
 var UpperDeltaMass=parseFloat(((current_mass*current_fat_per/100-BF_per * current_mass -0.84)/( BF_per -1 + 0.302)).toFixed(1));
 var LowerDeltaMass=parseFloat(((current_mass*current_fat_per/100-BF_per * current_mass -0.84)/( BF_per -1 + 0.398)).toFixed(1));

 // var change_in_mass= parseFloat(((cur_fat_mass-current_mass*BF_per)/(BF_per-FatRaio)).toFixed(9));
 var target_body_mass=parseFloat(current_mass)+parseFloat(change_in_mass);
 var target_mass=parseFloat(target_body_mass).toFixed(1);       
 var range_from=parseFloat(current_mass)+parseFloat(LowerDeltaMass);
 var range_start=parseFloat(range_from).toFixed(1); 
 var range_to=parseFloat(current_mass)+parseFloat(UpperDeltaMass);
 var range_end=parseFloat(range_to).toFixed(1);
    $("#default").show();
    $("#target_mass").val(target_mass); 
    $('#range_from').val(range_start);
    $('#range_to').val(range_end);
    $("#gain_loss_from").val(UpperDeltaMass); 
    $("#gain_loss_to").val(LowerDeltaMass); 
     var textbind="(Mean body mass change "+DeltaMass+" kg)";
    $("#gain_loss").html(textbind);
 }

</script>
    

    
<script>
$(document).on('click','#restricted_profile', function(){
        $("#restricted_profile").attr("href", "<?php echo site_url('Body/restricted_profile'); ?>");
        document.forms["myform"].submit();
	});  
    
    
$(document).on('click','#full_profile', function(){
         //alert('done doen');
        $("#full_profile").attr("href", "<?php echo site_url('Body/full_profile'); ?>");
		document.forms["myform"].submit();	  
	});  
    
$(document).on('click','#error_analysis', function(){
		document.forms["myform"].submit();	  
        $("#error_analysis").attr("href", "<?php echo site_url('Body/error_analysis'); ?>");
	});  
</script>  
         
</head>
<body>
   <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],1);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="<?php echo $_SESSION['bodyfat']; ?>">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Changing Body Mass</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<form name="vpform" id="vp_changemass" method="post"> 
                <a href="#" class="virtual_change_mass"><img src="<?php echo "$base/assets/images/"?>virtual_icon.png" style="margin-top: 12px;"></a>
                <input type="hidden" name="vptype" value="vp_changemass">
            </form> 
        </div>
		
		<div class="vitual_div">
			
		</div>
		<div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Changing body mass screen</div>
            <p>This screen allows the user to calculate the change in body mass (kg) required when a change in % body fat is chosen. For example, when a person loses weight it is extremely unlikely that the weight loss will comprise 100% body fat, even with significant levels of exercise. The user can enter their current body mass and % body fat values and select the desired % body fat level. They must then decide what percentage of the weight gain/loss will be fat ? They either use the slider to indicate the % of weight loss that will be fat or click the ‘Use default value’ button which is based on the published literature and has a mean value of ~73.4%. In other words, on average, when people lose weight about 73.4% of the weight lost is fat and 26.6% is lean tissue. Using the default value will also result in a range of weight loss values because research has shown a range in typical values, often depending on the combination and type of exercise and dietary interventions.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/go_on_menu', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php //echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php //echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php //echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
                <li><a href="<?php //echo site_url('Body/changing_body_mass'); ?>" id="changing_body_mass"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Changing Body Mass</a></li>
            </ul>
        </div>
        </div>

        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Changing Body Mass</div>
                <p>The current %BF of the client is <span style="float:none;" id="body_fat_per_txt"><?php echo $_SESSION['bodyfat'];?></span>%.</p>
           <p>To estimate the required body mass change, move the sliders on the <br>right side and choose the %BF goal and fat proportion.</p>
            
            
			<div class="half_container f_left">
				<div class="change_head">Current</div>

				<div class="field_row" style="border-bottom:0; padding-bottom:0;"> 
					<div class="field_50">
						<label>Body mass (kg)</label><input   type="text" id="body_mass" name="body_mass" value="" style="width:180px;"> 
					</div>
					<div class="field_50">
						<label>% body fat</label>
						<input type="text" name="body_fat_per" id="body_fat_per" value="">
					</div>
				 </div>
	
				<div class="range_div">
					<input name="mass_slider" type="range" value="<?php if(isset($_SESSION['MASS']))
                                                                        {
                                                                          echo $_SESSION['MASS'];  
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "40";
                                                                        }
                                                                  
                                                                  
                                                                  ?>" min="40" max="150" step="0.1" data-orientation="vertical" link-to="body_mass" data-rangeslider >
					<ul class="range_numbers">
						<li>150</li>
						<li>140</li>
						<li>130</li>
						<li>120</li>
						<li>110</li>
						<li>100</li>
						<li>90</li>
						<li>80</li>
						<li>70</li>
						<li>60</li>
						<li>50</li>
						<li>40</li>
					</ul>
				</div> 
				
				<div class="range_div range_div2">
					<input name="bodyfat_slider" type="range" value="<?php 
                                                                     if(isset($_SESSION['bodyfat']))
                                                                        {
                                                                          echo $_SESSION['bodyfat'];  
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "5";
                                                                        }
                                                                  ?>" min="5" max="50" step="0.01" data-orientation="vertical" link-to="body_fat_per" data-rangeslider >
					<ul class="range_numbers">
						<li>50</li>
						<li>45</li>
						<li>40</li>
						<li>35</li>
						<li>30</li>
						<li>25</li>
						<li>20</li>
						<li>15</li>
						<li>10</li>
						<li>5</li>
					</ul>
				</div>
				 
			</div>  
			<div class="half_container f_right">
				<div class="change_head">Goal</div>
				
				<div class="field_row" style="border-bottom:0; padding-bottom:0;"> 
					<div class="field_50">
						<label>Desired % body fat</label>
						<input type="text" id="desired_body_fat" name="desired_body_fat" value="" style="width:180px;">                         
					</div>
					<div class="field_50">
						<label>% mass change as fat</label>
						<input type="text" id="per_mass_change" name="per_mass_change" value="">
					</div>
				 </div>
				
				<div class="range_div vio_range_div">
					<input name="slider_3" type="range" value="3" min="3" max="30" step="0.5" data-orientation="vertical" link-to="desired_body_fat" data-rangeslider >
					<ul class="range_numbers range_numbers2">
						
						<li>30</li>
						<li>27</li>
						<li>24</li>
						<li>21</li>
						<li>18</li>
						<li>15</li>
						<li>12</li>
						<li>9</li>
						<li>6</li>
						<li>3</li>
					</ul>
				</div> 
				
				<div class="range_div vio_range_div">
					<input name="slider_4" type="range" value="73.4" min="0" max="100" step="0.01" data-orientation="vertical" link-to="per_mass_change" data-rangeslider>
					<ul class="range_numbers range_numbers3">
						<li>100</li>
						<li>90</li>
						<li>80</li>
						<li>70</li>
						<li>60</li>
						<li>50</li>
						<li>40</li>
						<li>30</li>
						<li>20</li>
						<li>10</li>
						<li>0</li>
					</ul>
				</div>
				
				<button type="button" class="lite_btn f_right" onclick="calc_defaultValue();" style="margin-right: 26px;">Use default value</button>
			</div>  
           
			
			<div class="field_row verticle_field_row" style="border-top: 1px solid #e6e6e6; border-bottom: 0; margin-top: 20px; padding-top: 20px;">
				
			  <p id="default">Using default (typical) value published in the literature:</p>	<br>
			  <div class="field_24" style="width: 17%;">
				<label>Target body mass is</label>
				<input type="text" id="target_mass" name="target_mass" value="" style="width:72px;"> kg
			  </div>
			  <div class="field_24" style="width: 26%;">
				<label>The range is between</label>
				<input type="text" id="range_from" name="range" value="" style="width:72px;"> to  &nbsp;
				<input type="text" id="range_to" name="range2" value="" style="width:72px;"> kg
			  </div>
			  <div class="field_24" style="width: 50%;">
                              <label>The client needs to lose (or gain) <div id="gain_loss" class="f_right"></div></label>
				
                              <input type="text" id="gain_loss_from" name="gain_loss_from" value="" style="width:72px;"> to   &nbsp;
				<input type="text" id="gain_loss_to" name="gain_loss_to" value="" style="width:72px;"> kg
			  </div>
			</div>
                 
        </div>
    </div>

        
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php echo "$base/assets/images/"?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>		
<script>
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
});        

$(document).on('click','.virtual_change_mass', function(){
    $("#vp_changemass").attr("action", "<?php echo base_url(); ?>index.php/Body/BCVirtualpersonGeneration");     
        $("#vp_changemass").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 

</script>
   
</body>
</html>
