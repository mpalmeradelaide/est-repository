<?php
class client_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          session_start();
    }
           
    public function save()
      {
        //$originalDate = $this->input->post('dob');
        //$newDate = date("d/m/Y", strtotime($originalDate));
          // print_r($this->input->post());
        $dob = $this->input->post('daydropdown')."/".$this->input->post('monthdropdown')."/".$this->input->post('yeardropdown');
            $code = array('first_name' => $this->input->post('first_name'),'last_name' => $this->input->post('last_name'),'email' => $this->input->post('email') ,'p_name' => $this->input->post('p_name'),'p_desc' => $this->input->post('p_desc'),'gender' => $this->input->post('gender'),'dob' => $dob,'country' => $this->input->post('country'),'occupation' => $this->input->post('occupation'));
			 //print_r($code);die;
            //$this->input->post('first_name')
         //   if($this->isExist( 'client_info',  $this->session->userdata('userid') )){
            if($this->isExist( 'client_info',  $_SESSION['userid'] )){
			
                   // $this->db->where('id', $this->session->userdata('userid'));
                    $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_info', $code); 
                    // return  $this->session->userdata('userid');
                     return $_SESSION['userid'];
                }else{
					
                      $this->db->insert('client_info', $code); 
                     return $this->db->insert_id();
      }
                
        
         
        
      }
      
    public function saveMedicalInfo()
      {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'notes' => $this->input->post('notes'),
              'option_8' => $this->input->post('option_8'));
                ///print_r($code ); die;
                //if($this->isExist( 'client_medical_info',  $this->session->userdata('userid') )){
                if($this->isExist( 'client_medical_info',  $_SESSION['userid'] )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medical_info', $code); 
                }else{

                    return $this->db->insert('client_medical_info', $code); 
                }               
          
      }
      
      
       public function savePhysicalActivityInfo()
      {
         $code = array(
             
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'));
        
          
           //if($this->isExist( 'client_physical_activity_info',  $this->session->userdata('userid') )){
           if($this->isExist( 'client_physical_activity_info', $_SESSION['userid'] )){
                    $this->db->where('c_id', $_SESSION['userid']);
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    return  $this->db->update('client_physical_activity_info', $code); 
                }else{

                    return  $this->db->insert('client_physical_activity_info', $code); 
                }                    
         
        }
      
      public function saveRiskFactorInfo()
      {
           
        
          $code = array(
            //  'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'),
              'option_10' => $this->input->post('options_10'),
              'option_11' => $this->input->post('options_11'),
              'option_12' => $this->input->post('options_12'),
              'option_13' => $this->input->post('options_13'),
              'option_gender' => $this->input->post('option_gender'),
              'option_age' => $this->input->post('option_age'),
              'option_smoke' => $this->input->post('option_smoke'),
              'option_smoke_6' => $this->input->post('option_smoke_6'));
            
          
            if($this->isExist( 'client_risk_factor_info',  $_SESSION['userid'] )){
            //if($this->isExist( 'client_risk_factor_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id',$_SESSION['userid']);
                    return  $this->db->update('client_risk_factor_info', $code); 
                }else{

                    return  $this->db->insert('client_risk_factor_info', $code); 
                }   
          
          //return  $this->db->insert('client_risk_factor_info', $code); 
      }
      
      
       public function saveBodyCompositionInfo()
      {          
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_height' => $this->input->post('options_height'),
              'option_weight' => $this->input->post('options_weight'),
              'option_height_measured' => $this->input->post('options_height_measured'),
              'option_weight_measured' => $this->input->post('options_weight_measured'),
              'option_bmi' => round($this->input->post('bmi'), 1),
              'option_waist' => $this->input->post('waist'),
              'option_hip' => $this->input->post('hip'),
              'option_whr' => round($this->input->post('whr'), 2),
              'option_triceps' => $this->input->post('triceps'),
              'option_biceps' => $this->input->post('biceps'),
              'option_subscapular' => $this->input->post('subscapular'),
              'option_sos' => round($this->input->post('sos'), 1));
             
          
             if($this->isExist( 'client_body_composition_info',  $_SESSION['userid'])){
           //  if($this->isExist( 'client_body_composition_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id',  $_SESSION['userid']);
                    return  $this->db->update('client_body_composition_info', $code); 
                }else{

                    return  $this->db->insert('client_body_composition_info', $code); 
                } 
                        
         // return  $this->db->insert('client_body_composition_info', $code); 
      }
      
      
       public function saveMeditationInfo()
      {
           
        
          $code = array(
             // 'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('option_1'),
              'medical_conditions' => $this->input->post('medicalConditions'),
              'medical_regular' => $this->input->post('medicalRegular'),
              'option_4' => $this->input->post('option_4'),
              'notes' => $this->input->post('notes'),
              'option_pregnant' => $this->input->post('option_pregnant'));
             
         // print_r($code)  ;
          
        //  if($this->isExist( 'client_medication_info',  $this->session->userdata('userid') )){
          if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
                   // $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medication_info', $code); 
                }else{

                    return  $this->db->insert('client_medication_info', $code); 
                } 
          
         // return  $this->db->insert('client_medication_info', $code); 
      }
      
       public function fetchScreeningInfo()
      {
           $this->fetchScreeningStatusInfo();
     //   $user_id=$this->session->userdata('userid');   
        $user_id=$_SESSION['userid'];   
        
       $q="SELECT ci.id,ci.p_desc as client_info, (pai.option_2 + 2*pai.option_4 + pai.option_6) as physical_activity ,rfi.option_1 as heart_disease,rfi.option_2 as smoking ,rfi.option_7 as hbp,rfi.option_8 as dbp ,rfi.option_11 as colestrol,rfi.option_9 as hdl,rfi.option_10 as ldl,rfi.option_13 as triglycerides,rfi.option_12 as hbg ,bci.option_bmi as bmi ,bci.option_waist as waist ,bci.option_hip as hip,ci.gender as gender ,ci.dob ,mi.option_8 as proceed ,rfi.option_gender,rfi.option_gender,rfi.option_age,rfi.option_3 as smoking2 ,rfi.option_4 as high_blood_pressure,rfi.option_5 as high_colestrol,rfi.option_6 as high_sugar , medi.medical_regular as llm , mi.notes as medical_notes ,medi.notes as medication_notes,medi.option_4 as medication_pic,medi.pt1 as pt1,medi.pt2 as pt2,medi.pt3 as pt3,medi.pt4 as pt4,medi.pt5 as pt5,medi.pt6 as pt6,medi.pt7 as pt7,medi.pt8 as pt8,medi.pt9 as pt9,medi.pt10 as pt10,medi.pt11 as pt11,medi.pt12 as pt12,medi.pt13 as pt13,medi.pt14 as pt14,medi.pt15 as pt15 ,bci.option_height as self_height ,bci.option_weight as self_weight  ,bci.option_height_measured as measured_height ,bci.option_weight_measured as measured_weight , pai.option_2 as pa2 , pai.option_6 as pa6, pai.option_4 as pa4 , pai.option_7 as pa7 , pai.option_8 as pa8,pai.option_9 as pa9, rfi.option_1 as family_history , rfi.option_2 as rf4 , rfi.option_smoke as rf5 , rfi.option_7 as rf_systolic , rfi.option_8 as rf_diastolic , rfi.option_smoke_6 as rf7 , rfi.option_4 as rf8, rfi.option_12 as rf_fbg , rfi.option_6 as rf10 , rfi.option_5 as rf9 ,bci.option_triceps,bci.option_biceps,option_subscapular,bci.option_sos FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id  where  ci.id= $user_id ";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
           
            /*** a date before 1970 ***/
        
            $date = str_replace('/', '-', $row['dob']);
                      
           $dateOfBirth=   date("Y-m-d", strtotime( $date) );
          
           $dob = strtotime($dateOfBirth);

            /*** another date ***/
            $tdate = strtotime(date('Y-m-d'));
          
            array_push($row,array('age'=>$this->getAge($dob, $tdate)));
            array_push($row,array('status'=>$this->fetchScreeningStatusInfo()));
            return $row;
           
      }
      
      
      function getAge( $dob,$tdate)
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
        public function fetchScreeningStatusInfo()
      {
// var_dump($_SESSION);

    //  $user_id=$this->session->userdata('userid');   
      $user_id=$_SESSION['userid'];   
      //  echo "aaaaa";
    //  die(__FILE__);
        $q="SELECT * FROM `client_medical_info` where  c_id= $user_id";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
          // print_r($row);
           if($row['option_1']=='N' && $row['option_2']=='N' && $row['option_3']=='N' && $row['option_4']=='N' && $row['option_5']=='N' && $row['option_6']=='N' && $row['option_7']=='N' ){
           return 'N';
          }else{
               return 'Y';
           }
           
           // return $row;
           
      } 

  public function updateRiskFactorInfo($riskFacor,$variable)
      {
           
     

          $code = array(
             // 'id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'risk_factor' => $riskFacor,
              'screening_decision_category' => $variable);
          
         
                 //   $this->db->where('id', $this->session->userdata('userid'));
                    $this->db->where('id', $_SESSION['userid']);
                      $this->db->update('client_info', $code); 
                
          
      }  

  public function updateMedicationInfo($column,$status)
      {
           
       if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
             echo  $sql = "UPDATE client_medication_info SET ". $column ." = ".$status." WHERE c_id =".$_SESSION['userid'];
		}else{
		echo $sql = "insert into client_medication_info (". $column .",c_id) values ( ".$status.",".$_SESSION['userid'].")";
		}	   
$this->db->query($sql);
          
      } 	  
      
      function getAllProjects()
    {
       $query = $this->db->query('SELECT * FROM project_info');
        $return = array();
        if($query->num_rows() > 0) {
        foreach($query->result_array() as $row) {
        //array_push($return,  $row['p_name']);
         $return [$row['id']] = $row['p_name'];
        }

    }else{
        $return[$row['0']] = "select";   
         }
         return $return;
    }
   
    
    //GET RANDOM VAULES 
    function generateRandomValues()
    {
       
       //FOR MALE FEMALE 
       $genderarray = array( '1'  => 'Male','2'  => 'Female');  
       $k = array_rand($genderarray);
       $v = $genderarray[$k]; 
       $selectedgender=array('gendervalue'=>$k,'gendertext'=>$v);
        //FOR MALE FEMALE
       if($k=='1')
       {
       $columnname="boys_fname";    
       }
       else
       {
        $columnname="girls_fname";   
       }
       //GENERATE RANDOM NAMES 
       $randomnamesQry="SELECT $columnname as firstname FROM random_names ORDER BY RAND() LIMIT 1";
       $query = $this->db->query($randomnamesQry);
       if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $firstname=$result['firstname'];
       $randomlastQry="SELECT last_name FROM random_names ORDER BY RAND() LIMIT 1";
       $queryrandom = $this->db->query($randomlastQry);
       if($queryrandom->num_rows() > 0) {
         $resultlastname=$queryrandom->row_array();
        }
       $lastname=$resultlastname['last_name'];
       $selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
       //END GENERATE RANDOM NAMES 
       
       //RANDOM DATE OF BIRTH GENERATION
        
       $dates = array(
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
       
                            $month = array(
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
       
       
       $years = array('1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927', '1926'=>'1926', '1925'=>'1925', '1924'=>'1924', '1923'=>'1923', '1922'=>'1922', '1921'=>'1921', '1920'=>'1920', '1919'=>'1919', '1918'=>'1918', '1917'=>'1917', '1916'=>'1916', '1915'=>'1915', '1914'=>'1914', '1913'=>'1913', '1912'=>'1912', '1911'=>'1911', '1910'=>'1910', '1910'=>'1910');		
       /*Day Selection*/
       $daykey = array_rand($dates);
       $valuedate = $dates[$daykey]; 
       $selecteddates=array('datekey'=>$daykey,'datevalue'=>$valuedate);
        /*Month Selection*/
       $monthkey = array_rand($month);
       $valuemonth = $month[$monthkey]; 
       $selectedmonth=array('monthkey'=>$monthkey,'monthvalue'=>$valuemonth);
        /*Month Selection*/
        
       /*Year Selection*/
       $yearskey = array_rand($years);
       $valueyears = $years[$yearskey]; 
       $selectedyears=array('yearskey'=>$yearskey,'yearsvalue'=>$valueyears);
        /*Year Selection*/
       
       /*COUNTRY PROFILE*/
        $min=1;
        $max=100;
        $randomcountryval=rand($min,$max);
        if($randomcountryval<90)
        {
         $selectedyears=array('countrykey'=>'AU','countryvalue'=>'Australia');   
        }
       else
       {
        $randomctryQry="SELECT country_code,country_name FROM country ORDER BY RAND() LIMIT 1";
       $queryrandomctry = $this->db->query($randomctryQry);
       if($queryrandomctry->num_rows() > 0) {
         $resultctry=$queryrandomctry->row_array();
        }    
       $selectedyears=array('countrykey'=>$resultctry['country_code'],'countryvalue'=>$resultctry['country_name']);    
       } 
      /*END COUNTRY PROFILE*/
      /*OCCUPATION PROFILE*/
     
        $minoccu=1;
        $maxoccu=30;
        $randomoccuval=rand($minoccu,$maxoccu);
       
       $occupationQry="SELECT `occupation` FROM occupation where id=$randomoccuval";
       $queryrandomoccu = $this->db->query($occupationQry);
       if($queryrandomoccu->num_rows() > 0) {
         $resultoccupation=$queryrandomoccu->row_array();
        }    
       $selectedoccupation=array('occupationkey'=>$resultoccupation['occupation'],'occupationval'=>$resultoccupation['occupation']);
       $occupation=$resultoccupation['occupation']; 
       /*END OCCUPATION PROFILE*/ 
       $dateofBirth=$valueyears.'-'.$valuemonth.'-'.$valuedate;
       $datebrth= date("Y-m-d",strtotime($dateofBirth));
       $dob = strtotime($datebrth);
       $tdate = strtotime(date('Y-m-d'));
       $vp_age=$this->getAge($dob, $tdate);
       $agerange=$this->getAgeRange($vp_age);
      //SUB-POPULATION PROFILE CONDITION
       $subpopulation="";
       //FOR MALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 6)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 6 && $RN< 20)
            {
            $subpopulation="General";    
            }
             else if($RN >=20  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
      
      //FOR FEMALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 8)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 8 && $RN< 24)
            {
            $subpopulation="General";    
            }
             else if($RN >=24  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
        //FOR MALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 9)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 9 && $RN< 30)
            {
            $subpopulation="General";    
            }
             else if($RN >=30  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
      
      //FOR FEMALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
       //FOR Male 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //FOR Female 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 14)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 14 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //ANother Condition Based on Subpopulation and Age..
       if($subpopulation=='Athlete' && $vp_age>40)
       {
        $subpopulation="Active";    
       }
       //FOR MALE 45-54 yr
       if(($vp_age >=45 && $vp_age<=54) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 15)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 15 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 45-54 
       if(($vp_age >=45 && $vp_age<=54) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 18)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 18 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR MALE 55-64 yr
       if(($vp_age >=55 && $vp_age<=64) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 16)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 16 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 55-64 
       if(($vp_age >=55 && $vp_age<=64) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       
        //FOR MALE 65-74  yr
       if(($vp_age >=65 && $vp_age<=74) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 65-74 
       if(($vp_age >=65 && $vp_age<=74) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 24)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 24 && $RN< 52)
            {
            $subpopulation="General";    
            }
             else if($RN >=52)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Male 75+ 
       if($vp_age>= 75 && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 31)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 31 && $RN< 62)
            {
            $subpopulation="General";    
            }
             else if($RN >=62)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Female 75+ 
       if($vp_age>= 75 && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 36)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 36 && $RN< 68)
            {
            $subpopulation="General";    
            }
             else if($RN >=68)
            {
            $subpopulation="Active";    
            }
       }
       $selectedgender=array('subpopulationvalue'=>$subpopulation,'gendertext'=>$v);
    
      $randomData=array('firstname'=>$firstname,'lastname'=>$lastname,'gender'=>$v,'daydropdown'=>$valuedate,'monthdropdown'=>$monthkey,'yeardropdown'=>$valueyears,'age_category'=>$agerange,'occupation'=>$occupation,'sub_population'=>$subpopulation,'country'=>$selectedyears); 
       return $randomData; 

//  echo $vp_age.'-'.$subpopulation.'-'.$agerange.'-'.$v.'-random-'.$RN;die;
       
       /******************MASS, BMI AND HEIGHT PROFILE***********************/
       
       
       
        /******************END MASS, BMI AND HEIGHT PROFILE***********************/
       }//END OF FUNCTION GENERATE RANDOM
    
    
       //GET AGE RANGE
       function getAgeRange($age)
       {
       	
			 if($age >= 18 && $age <=29)
			 {
				$agerange = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $agerange = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				 $agerange = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				$agerange = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				 $agerange = '60-69' ;
			 }
			 elseif($age >=70 && $age <=79)
			 {
				$agerange = '70-79' ;
			 }
                          elseif($age >=80 && $age <=84)
			 {
				$agerange = '80-84' ;
			 }
                          elseif($age >84)
			 {
				$agerange = '84+' ;
			 }
                    
                         return $agerange;     
                         
                   }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    
    
    
      
}
?>