<?php
class body_model extends CI_Model
{
    function __construct()
    {
        parent::__construct(); 
        session_start();
    }

	
	
	
  		//Fetch Age Range and Population 
  public function fetch_blood_norms(){
    $this->db->select('age');
    $this->db->group_by('age');
    $query=$this->db->get('blood_norms_all_ctry');
    $age_range=$query->result();   
    //Country
    $this->db->select('country');
    $this->db->group_by('country');
    $query_ctry=$this->db->get('blood_norms_all_ctry');
    $ctry=$query_ctry->result();
     
    $age_ctry=array('age_range'=>$age_range,'country'=>$ctry);
	return $age_ctry;
  }
		
	
    public function getnormdata($gender,$age,$population)
   {
	    $this->db->select("*");	     
		$this->db->from('blood_norms_all_ctry');
	    $this->db->where('age', $age);
		$this->db->where('gender', $gender);  
		$this->db->where('country',$population); 
		$query = $this->db->get();
		return $query->result();
			
   }
		

	
	
	
	
   public function all_sports_male(){
    
    $this->db->select("sport");	     
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%m%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  
   public function all_sports_female(){
    
    $this->db->select("*");      
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%f%')"); 
	$this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
   
   public function fetch_anthropometetry($gender){
    $this->db->select("*");	     
    $this->db->from('sport_match');
    $this->db->where("( gender like '$gender%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  
  
  public function fetch_sportslist($gender){
    $this->db->select("sport,endomorph,mesomorph,ectomorph");	     
    $this->db->from('sport_match');
    $this->db->where("( gender like '$gender%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  }     
    
  
  
   public function all_ages(){
    
    $this->db->select("*");	     
    $this->db->from('age');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  
  public function all_tests(){
    
    $this->db->select("*");	     
    $this->db->from('tests');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  public function all_ranges(){
    
    $this->db->select("*");	     
    $this->db->from('probability');    
    $query = $this->db->get(); 
    return $query->result();
  }
  //Get Info Data 
  public function fetch_info_data($info_id){
   
      $this->db->select("*");
        $this->db->from('body_comp_info');
        $this->db->where('id', $info_id);
        $query = $this->db->get();
        return $query->result();
    }
  
  function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
  
public function saveRestrictedInfo()
 {
    //var_dump($this->session->all_userdata());         
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'height' => $this->input->post('height'),
              'body_mass' => $this->input->post('body_mass'),
              'triceps' => $this->input->post('triceps'),
              'subscapular' => $this->input->post('subscapular'),
              'biceps' => $this->input->post('biceps'),
              'iliac_crest' => $this->input->post('iliac_crest'),
              'supraspinale' => $this->input->post('supraspinale'),
              'abdominal' => $this->input->post('abdominal'),
              'thigh' => $this->input->post('thigh'),
              'calf' => $this->input->post('calf'),
              'mid_axilla' => $this->input->post('mid_axilla'),
              'relArmG' => $this->input->post('relArmG'),
              'flexArmG' => $this->input->post('flexArmG'),
              'waistG' => $this->input->post('waistG'),
              'hipG' => $this->input->post('hipG'),
              'calfG' => $this->input->post('calfG'),
              'humerus' => $this->input->post('humerus'),
              'femur' => $this->input->post('femur')
              );
               
            $query = $this->db->query("SELECT c_id FROM restricted_profile
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('restricted_profile', $code); 
                }
                else
                {
                    return $this->db->insert('restricted_profile', $code); 
                }               
          
      }
      

public function saveFullInfo()
 {
    //var_dump($this->session->all_userdata());         
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'height' => $this->input->post('height'),
              'body_mass' => $this->input->post('body_mass'),
              'triceps' => $this->input->post('triceps'),
              'subscapular' => $this->input->post('subscapular'),
              'biceps' => $this->input->post('biceps'),
              'iliac_crest' => $this->input->post('iliac_crest'),
              'supraspinale' => $this->input->post('supraspinale'),
              'abdominal' => $this->input->post('abdominal'),
              'thigh' => $this->input->post('thigh'),
              'calf' => $this->input->post('calf'),
              'mid_axilla' => $this->input->post('mid_axilla'),              
              'headG' => $this->input->post('headG'),
              'neckG' => $this->input->post('neckG'),
              'relArmG' => $this->input->post('relArmG'),
              'flexArmG' => $this->input->post('flexArmG'),
              'forearmG' => $this->input->post('forearmG'),
              'wristG' => $this->input->post('wristG'),
              'chestG' => $this->input->post('chestG'),
              'waistG' => $this->input->post('waistG'),
              'hipG' => $this->input->post('hipG'),
              'thighG' => $this->input->post('thighG'),
              'midThighG' => $this->input->post('midThighG'),
              'calfG' => $this->input->post('calfG'),              
              'ankleG' => $this->input->post('ankleG'),
              'acRad' => $this->input->post('acRad'),
              'radStyl' => $this->input->post('radStyl'),
              'midStylDact' => $this->input->post('midStylDact'),
              'iliospinale' => $this->input->post('iliospinale'),
              'troch' => $this->input->post('troch'),
              'trochTib' => $this->input->post('trochTib'),
              'tibLat' => $this->input->post('tibLat'),
              'tibMed' => $this->input->post('tibMed'),              
              'biac' => $this->input->post('biac'),
              'bideltoid' => $this->input->post('bideltoid'),
              'billio' => $this->input->post('billio'),
              'bitrochanteric' => $this->input->post('bitrochanteric'),
              'foot' => $this->input->post('foot'),
              'sitting' => $this->input->post('sitting'),
              'trChest' => $this->input->post('trChest'),
              'apChest' => $this->input->post('apChest'),
              'armSpan' => $this->input->post('armSpan'),       
              'humerus' => $this->input->post('humerus'),
              'femur' => $this->input->post('femur')
              );
               
            $query = $this->db->query("SELECT c_id FROM full_profile
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('full_profile', $code); 
                }
                else
                {
                    return $this->db->insert('full_profile', $code); 
                }               
          
      }
      
      
  public function saveErrorConfidenceInfo()
  {
    //var_dump($this->session->all_userdata());         
          
          $code = array(             
              'c_id' => $_SESSION['userid'],              
              'single_raw' => $this->input->post('single_raw'),
              'tem_percent' => $this->input->post('tem_percent'),
              'interval' => $this->input->post('interval')
              );
                            
            $query = $this->db->query("SELECT c_id FROM error_confidence
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('error_confidence', $code); 
                }
                else
                {
                    return $this->db->insert('error_confidence', $code); 
                }               
          
      }
      
  public function savetErrorRealChangeInfo()
  {
    //var_dump($this->session->all_userdata());         
          
          $code = array(             
              'c_id' => $_SESSION['userid'],              
              'first_score' => $this->input->post('first_score'),
              'second_score' => $this->input->post('second_score'),
              'tem_percent' => $this->input->post('tem_percent'),
              'interval' => $this->input->post('interval')
              );
                            
            $query = $this->db->query("SELECT c_id FROM error_real_change
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('error_real_change', $code); 
                }
                else
                {
                    return $this->db->insert('error_real_change', $code); 
                }               
          
      }      
  
}


?>