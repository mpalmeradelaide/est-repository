<?php

class virtual_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          
    }
	  //fetch list of test variables  
 public function gettest_variables() 
      { 
    $query = $this->db->get('test_variables'); 
    return($query->result());
   
   } 		
		//add_VP_Data  
      public function add_VP_Data($data) 
      { 
              $this->db->insert('virtual_person_info', $data); 
                     return $this->db->insert_id();
      } 

    
    
// kritika Code
    // Kritika     
public function body_composition_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation)
{
    $object1 = new PHPExcel_Calculation_Statistical();
    $fieldData=array();
    $meanwaist=0.0;     $waist=0.0;     $bodycompprob=0.0;    $hip=0.0;     $waistnorm=0.0;     $hipnorm=0.0;   $meantri=0.0;   $tricepsnorm=0.0; $triceps=0.0; $options_height=0.0;  $options_weight=0.0;
    
    $measuredheight=$height;
    $measuredmass=$mass;
    $measuredBMI=$bmi;
    $measuredpopulation=$user_subpopulation;
    $gender=$user_gender;
    $measuredage=$age;
  

// SELF REPORT 
/*SRH = RANDBETWEEN(1,100) 
If SRH < 50 then: [else there will be no value entered in the self-report fields for HEIGHT and MASS]
SRHEIGHT = (NORM.INV(RAND(),HEIGHT,0.5)+0.5

Enter SRHEIGHT in the ‘Self report HEIGHT’ field on screen 4

SRMASS = (NORM.INV(RAND(),MASS,0.8)-1.2

Enter SRMASS in the ‘Self report MASS’ field on screen 4
*/
       $srh=rand(1,100);
       if($srh <= 50)
       {
       //SRHEIGHT = (NORM.INV(RAND(),HEIGHT,0.5)+0.5
       $srhprob = $this->frand(0,1);
       $srhprob = round($srhprob, 9);
       $srh=$object1->NORMINV($srhprob,$measuredheight,0.5)+0.5;
       
           
       $options_height=round($srh,2);
           
           
       $massprob = $this->frand(0,1);
       $massprob = round($massprob, 9);
       $srmass=$object1->NORMINV($massprob,$measuredmass,0.8)-1.2;
       $options_weight=round($srmass,2);
        
       }
       $options_height = $options_height; 
       $options_weight = $options_weight; 
      
    if($gender == 'M')      // MALE 
    {
        /*MeanWaist = 2.639*(BMI) + 21.959 
        WAISTNORM = (NORM.INV(RAND(),MeanWaist,4.5)*/
        //Calculate for Male
        $object1 = new PHPExcel_Calculation_Statistical();
        $meanwaist= round(2.639*($measuredBMI) + 21.959);
        $bodycompprob=$this->frand(0,1);
        $bodycompprob=round($bodycompprob,9); 
        $waistnorm = $object1->NORMINV($bodycompprob,$meanwaist,4.5);
       // echo $waistnorm; die;
        
        // HIP CALCULATION
        /*MeanHip = 1.659*(BMI) + 59.1 
        HIPNORM = (NORM.INV(RAND(),MeanHip,4.3)*/
        $meanhip = 1.659*($measuredBMI) + 59.1 ;
        $hipprob=$this->frand(0,1);
        $hipprob=round($hipprob,9); 
        //$hipnorm = $object1->NORMINV($hipprob,$meanhip,4.3);
        
        /*HIPNORM = (NORM.INV(RAND(),MeanHip,3.3)*/       
        $hipnorm = $object1->NORMINV($hipprob,$meanhip,3.3);  // new
       
       if($measuredpopulation == "Sedentary")
        {
            //$waist = $waistnorm * 1.08; //OLD
            $waist = $waistnorm * 1.1;  // NEW
            //$hip = $hipnorm * 1.08;
            $hip = $hipnorm * 1.1;       // new
        }
        elseif($measuredpopulation == "General")
        {
            $waist = $waistnorm * 1.04;
            $hip = $hipnorm * 1.04;
        }
        elseif($measuredpopulation == "Active")
        {
            $waist = $waistnorm * 0.98;
            $hip = $hipnorm * 0.98;
        }
        elseif($measuredpopulation == "Athlete")
        {
            $waist = $waistnorm * 0.94;
            $hip = $hipnorm * 0.94;
        }
		
		
		
// WAIST ADJUSTMENT 12 oct
/*
 PLEASE ADD Adjustment because the values are causing problems:

MALE WAIST adjustment = -0.0018* AGE^2 + 0.2964*AGE - 6 
eg., a 40 yr old adjustment = -0.0018*40^2 + 0.2964*40 - 6 = 2.98 cm
Therefore, 2.98 cm would be ADDED to the WAIST value to give the FINAL [adjusted] WAIST value  [used from now on]
*/
    
        $m_waist_adj = - 0.0018* pow($age,2) + 0.2964 * $age - 6 ;
        $waist = $waist +  $m_waist_adj;
		
    }
    
    else     // FEMALE
    {
        /*MeanWaist = 2.293*(BMI) + 21.950 
        WAISTNORM = (NORM.INV(RAND(),MeanWaist,4.9)*/
        //Calculate for FeMale
        $object1 = new PHPExcel_Calculation_Statistical();
        $meanwaist= round(2.293*($measuredBMI) + 21.950);
        $bodycompprob=$this->frand(0,1);
        $bodycompprob=round($bodycompprob,9); 
        $waistnorm = $object1->NORMINV($bodycompprob,$meanwaist,4.9);
       // echo $waistnorm; die;
        
        
         // HIP CALCULATION
        /*If FEMALE then
			MeanHip = 1.903*(BMI) + 55.97 
			HIPNORM = (NORM.INV(RAND(),MeanHip,4.3)*/
        $meanhip = 1.903*($measuredBMI) + 55.97 ;
        $hipprob=$this->frand(0,1);
        $hipprob=round($hipprob,9); 
          //$hipnorm = $object1->NORMINV($hipprob,$meanhip,4.3);
        
        /// HIPNORM = (NORM.INV(RAND(),MeanHip,3.6)              //NEW        
        $hipnorm = $object1->NORMINV($hipprob,$meanhip,3.6);     // new
 
        if($measuredpopulation == "Sedentary")
        {
           //$waist = $waistnorm * 1.08; // Old
            $waist = $waistnorm * 1.1; // New
           // $hip = $hipnorm * 1.08;
            $hip = $hipnorm * 1.1;      // new
        }
        elseif($measuredpopulation == "General")
        {
            $waist = $waistnorm * 1.04;
            $hip = $hipnorm * 1.04;
        }
        elseif($measuredpopulation == "Active")
        {
            $waist = $waistnorm * 0.98;
            $hip = $hipnorm * 0.98;
        }
        elseif($measuredpopulation == "Athlete")
        {
            $waist = $waistnorm * 0.94;
            $hip = $hipnorm * 0.94;
        }
    }
    
    
    
    $getdata= $this->fetch_physical_activity_details();
    $total_PA=$getdata[0]->total_PA;
    $vig_Total=$getdata[0]->vig_total;
    
    if($gender  == 'M')
    {
        // -0.0594*(Age) + 0.8699*(BMI) – 0.00191*(TOTAL PA) – 7.5 (OLD)

        /* New Formula
        If MALE then
            MeanTri = -0.0594*(Age) + 0.76*(BMI) – 0.00191*(TOTAL PA) – 7.5 
            TRICEPSNORM = (NORM.INV(RAND(),MeanTri,3.3)
        */		
            $meantri  =-0.0594 * ($measuredage) + 0.76 * ($measuredBMI) - 0.00191 * ($total_PA) - 7.5;
            $prob = $this->frand(0, 1);
            $prob = round($prob, 9);
            $x = $object1->NORMINV($prob, $meantri, 3.3);
            $tricepsnorm =$x; 
        
        
        
           // Biceps MAle
           /*
           If MALE then
            MeanBi = 0.637*(BMI) - 0.002*(2*VIG TOTAL) – 10.1 
            BICEPSNORM = (NORM.INV(RAND(),MeanBi,2.8)
           */
            $meanbiceps = 0.637 * ($measuredBMI) - 0.002 * (2 * $vig_Total) - 10.1 ;
            $prb = $this->frand(0, 1);
            $prb = round($prb, 9);
            $bnorm = $object1->NORMINV($prb, $meanbiceps, 2.8);
            $bicepsnorm =$bnorm; 
        
        
          // subscaluar
          /*
          If MALE then
            MeanSub = 0.931*(BMI) + 0.219*(WAIST) – 0.004*(TOTAL PA) – 23.3
            SUBSCAPULARNORM = (NORM.INV(RAND(),MeanSub,5.9)
          */
            $meansubs  =  0.931 * ($measuredBMI) + 0.219 * ($waist) - 0.004 * ($total_PA) - 23.3;
            $pb = $this->frand(0, 1);
            $pb = round($pb, 9);
            $subs = $object1->NORMINV($pb, $meansubs,5.9);
            $subcabularnorm  = $subs; 
        
        
        
     }
    if($gender  ==  'F')
    {
              //MeanTri = 0.0366*(Age) + 1.0898*(BMI) – 0.0027*(TOTAL PA) – 5.3
			  
			/*(New)
        If FEMALE then
            MeanTri = 0.0366*(Age) + 0.898*(BMI) – 0.0027*(TOTAL PA) – 5.3
            TRICEPSNORM = (NORM.INV(RAND(),MeanTri,4.8) 
        */			 
			  
            $meantri = 0.0366 * ($measuredage) + 0.898 * ($measuredBMI) - 0.0027 * ($total_PA) - 5.3;
            $prob = $this->frand(0, 1);
            $prob = round($prob, 9);
            $x = $object1->NORMINV($prob, $meantri, 4.8);
            $tricepsnorm=$x; 
        
        
        
           // female Biceps
           /*
           If FEMALE then
            MeanBi = 0.71*(BMI) + 0.118*(WAIST) – 0.105*(HIP) – 0.001*(TOTAL PA) – 5.6
            BICEPSNORM = (NORM.INV(RAND(),MeanBi,3.6)
           */
        
            $meanbiceps = 0.71 * ($measuredBMI)+ 0.118 * ($waist) - 0.105 * ($hip) - 0.001 * ($total_PA) - 5.6;
            $prb = $this->frand(0, 1);
            $prb = round($prb, 9);
            $bnorm = $object1->NORMINV($prb, $meanbiceps,3.6);
            $bicepsnorm =$bnorm; 
        
        
         //Female subscapular
           /*(NEW)
          If FEMALE then
                MeanSub = 0.81*(BMI) + 0.495*(WAIST) – 0.42*(HIP) – 0.005*(2*VIG TOTAL) – 1.4
                SUBSCAPULARNORM = (NORM.INV(RAND(),MeanSub,6.5)      
          */
            //$meansubs = 1.181 * ($measuredBMI)+ 0.495 * ($waist) - 0.42 * ($hip) - 0.005 * (2 * $vig_Total) - 1.4;
			
			$meansubs = 0.81 * ($measuredBMI)+ 0.495 * ($waist) - 0.42 * ($hip) - 0.005 * (2 * $vig_Total) - 1.4;
			
            $pb = $this->frand(0, 1);
            $pb = round($pb, 9);
            $subs = $object1->NORMINV($pb, $meansubs,6.5);
            $subcabularnorm  = $subs; 
        
        
        
     }
      if($measuredpopulation=='Sedentary')
      {
         $triceps =$tricepsnorm * 1.2;   // Altered 
         $biceps =$bicepsnorm * 1.2;     // ALtered
         $subscapular  =$subcabularnorm * 1.2;  // ALtered
      }
     if($measuredpopulation=='General')
      {
         $triceps =$tricepsnorm * 1.1;   // Altered
         $biceps =$bicepsnorm * 1.1;    // Altered
         $subscapular =$subcabularnorm * 1.11;  // ALtered   
      }
      if($measuredpopulation=='Active')
      {
          $triceps=$tricepsnorm * 0.9; 
          $biceps=$bicepsnorm * 0.9; 
          $subscapular=$subcabularnorm * 0.9; 
      }
      if($measuredpopulation=='Athlete')
      {
         $triceps=$tricepsnorm * 0.7; 
         $biceps=$bicepsnorm * 0.7; 
         $subscapular=$subcabularnorm * 0.7; 
      }
       
       if($triceps < 4)
       {
        $triceps=4;   
       } 
    
       if($biceps < 3)
       {
        $biceps=3;   
       } 
    
       if($subscapular < 5)
       {
        $subscapular=5;   
       }
    
    // Skinfolds Calculate
    $skinfold_summ=$triceps+$biceps+$subscapular;
    $fieldData[0]->skinfold_summ=round($skinfold_summ,2);

    // waist hip ratio   
    $whr=abs($waist/$hip);
  
    
    
    // kritika
    // ANOTHER BODY COMPOSITION MODULE VARIABLES
    //ILIAC CREST skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.46,2.5)
   if($gender  == 'M')
    {
      $object1 = new PHPExcel_Calculation_Statistical(); 
      // Iliac Crest
      $iliaccrest=$this->frand(0,1);
      $iliaccrest=round($iliaccrest,9);
      $Iliac_crest_skinfold = $object1->NORMINV($iliaccrest,$skinfold_summ*0.46,2.5);
      if($Iliac_crest_skinfold < 2.9)
      {
          $Iliac_crest_skinfold=2.9;
      }
        
      // SUPRASPINALE skinfold   
        //SUPRASPINALE skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.30,2)
      $supraspinale=$this->frand(0,1);
      $supraspinale=round($supraspinale,9);
      $Supraspinale_skinfold=  $object1->NORMINV($supraspinale,$skinfold_summ*0.30,2);
        if($Supraspinale_skinfold < 2.2)
        {
            $Supraspinale_skinfold=2.2;
        }
                       
    //Abdominal Skinfold
        //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.50,2.5)
		
		/*New
        ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.50,2.5) +0.05*age       
        */
      $abdominal=$this->frand(0,1);
      $abdominal=round($abdominal,9);   
      $Abdominal_skinfold = $object1->NORMINV($abdominal,$skinfold_summ*0.50,2.5)+0.05 * $measuredage;        // ALtered 
        if($Abdominal_skinfold < 4)
        {
            $Abdominal_skinfold=4;
        }
            
    //Front THIGH
        //FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.4,2.5)
		
		// //FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.45,2.3)      // NEW
		
		
      $front_thigh=$this->frand(0,1);
      $front_thigh=round($front_thigh,9);                  
      //$Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.4,2.5);
      $Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.45,2.3);   // New
        if($Front_thigh_skinfold  < 3.2)
        {
            $Front_thigh_skinfold=3.2;
        }
                              
   // MEdialCalf Skinfold
        //MEDIAL CALF skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $medial_calf=$this->frand(0,1);
      $medial_calf=round($medial_calf,9);                        
      $Medial_calf_skinfold=  $object1->NORMINV($medial_calf,$skinfold_summ*0.25,2); 
         if($Medial_calf_skinfold < 2.9)
         {
             $Medial_calf_skinfold=2.9;
         }
                       
  // Mid-Axilla Skinfold  
        //MID-AXILLA skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $midaxilla=$this->frand(0,1);
      $midaxilla=round($midaxilla,9);                        
      $Midaxilla_skinfold=  $object1->NORMINV($midaxilla,$skinfold_summ*0.25,2);
        if($Midaxilla_skinfold < 2.6)
        {
            $Midaxilla_skinfold=2.6;
        }
    }
    
 else // Female Case
 {
      $object1 = new PHPExcel_Calculation_Statistical(); 
      // Iliac Crest
     // ILIAC CREST skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.40,3)
      $iliaccrest=$this->frand(0,1);
      $iliaccrest=round($iliaccrest,9);
      $Iliac_crest_skinfold = $object1->NORMINV($iliaccrest,$skinfold_summ*0.40,3);
      if($Iliac_crest_skinfold < 4)
      {
          $Iliac_crest_skinfold=4;
      }
        
      // SUPRASPINALE skinfold   
        //SUPRASPINALE skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.28,3)
      $supraspinale=$this->frand(0,1);
      $supraspinale=round($supraspinale,9);
      $Supraspinale_skinfold=  $object1->NORMINV($supraspinale,$skinfold_summ*0.28,3);
        if($Supraspinale_skinfold < 3.2)
        {
            $Supraspinale_skinfold=3.2;
        }
                       
    //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.43,3.5)     (OLD)
     
     //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.43,3.5)+0.05*age  (NEW)
      $abdominal=$this->frand(0,1);
      $abdominal=round($abdominal,9);   
      $Abdominal_skinfold = $object1->NORMINV($abdominal,$skinfold_summ*0.43,3.5) +0.05 * $measuredage; 
        if($Abdominal_skinfold < 5)
        {
            $Abdominal_skinfold=5;
        }
            
     //Front THIGH
     // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.8,3.5)     (OLD)
     
     // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.65,3)  (NEW)
     
	 // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.7,2.5)  // new
      $front_thigh=$this->frand(0,1);
      $front_thigh=round($front_thigh,9);                  
       //$Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.65,3);
      $Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.7,2.5);       //new
        if($Front_thigh_skinfold  < 5.2)
        {
            $Front_thigh_skinfold=5.2;
        }
                              
   // MEdialCalf Skinfold
     // MEDIAL CALF skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.45,3)
      $medial_calf=$this->frand(0,1);
      $medial_calf=round($medial_calf,9);                        
      $Medial_calf_skinfold=  $object1->NORMINV($medial_calf,$skinfold_summ*0.45,3); 
         if($Medial_calf_skinfold < 3.9)
         {
             $Medial_calf_skinfold=3.9;
         }
                       
  // Mid-Axilla Skinfold   
     //  MID-AXILLA skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $midaxilla=$this->frand(0,1);
      $midaxilla=round($midaxilla,9);                        
      $Midaxilla_skinfold=  $object1->NORMINV($midaxilla,$skinfold_summ*0.25,2);
        if($Midaxilla_skinfold < 3.6)
        {
            $Midaxilla_skinfold=3.6;
        }
    
    
    
 }
    // HEAD CALCULATION
/*    EAD
If MALE then:
HEADMEAN = 50.02 + 0.087 * MASS
HEAD = (NORM.INV(RAND(),HEADMEAN,0.7)

*/
    
if($gender  == 'M')
{
         $headmean= 50.02 + 0.087 * $mass;
         $headprob=$this->frand(0,1);
         $headprob=round($headprob,9); 
         $Head = $object1->NORMINV($headprob,$headmean,0.7);
      
   	    /*  NECK
            If MALE then:
            NECKMEAN = 22.09 + 0.201 * MASS
            NECK = (NORM.INV(RAND(),NECKMEAN,0.9)
        */
         $neckmean= 22.09 + 0.201 * $mass;
         $neckprob=$this->frand(0,1);
         $neckprob=round($neckprob,9);
         $neck = $object1->NORMINV($neckprob,$neckmean,0.9);
        
        
        
        
        /*
ARM [relaxed]
If MALE then:
ARMMEAN = 1.121 * BMI + 2.8
ARMR [relaxed] = (NORM.INV(RAND(),ARMMEAN,2.0)

*/        
        $armmean=1.121 * $bmi + 2.8;
        $armprob=$this->frand(0,1);
        $armprob=round($armprob,9);
        $relaxed_arm=$object1->NORMINV($armprob,$armmean,2.0);
         
/*    ARM [flexed]
If MALE then:
ARMFMEAN = 1.041 * ARMR [relaxed] + 0.897
ARMF [flexed] = (NORM.INV(RAND(),ARMFMEAN,1.1)
*/        
        $armmean_fl=1.041 * $relaxed_arm + 0.897;
        $armprob_fl=$this->frand(0,1);
        $armprob_fl=round($armprob_fl,9);
        $relaxed_arm_fl=$object1->NORMINV($armprob_fl,$armmean_fl,1.1);
        
/*
FOREARM [max]
If MALE then:
FOREARMMEAN = 14.28 + 0.182 * MASS
FOREARM [max] = (NORM.INV(RAND(),FOREARMMEAN,0.7)
*/        
        
        $forearmmean = 14.28 + 0.182 * $mass;
        $forearmprob=$this->frand(0,1);
        $forearmprob=round($forearmprob,9);
        $forearm_max = $object1->NORMINV($forearmprob,$forearmmean,0.7);        

        
/*    WRIST [distal styloids]
If MALE then:
WRISTMEAN = 11.256 + 0.083 * MASS
WRIST = (NORM.INV(RAND(),WRISTMEAN,0.5)
*/        
 
        $wristmean=11.256 + 0.083 * $mass;
        $wristprob=$this->frand(0,1);
        $wristprob=round($wristprob,9);
        $wrist = $object1->NORMINV($wristprob,$wristmean,0.5);
 /*CHEST
If MALE then:
CHESTMEAN = 3.091 * BMI + 19.194
CHEST = (NORM.INV(RAND(),CHESTMEAN,3.5)
*/       
        $chestmean=3.091 * $bmi + 19.194;
        $chestprob=$this->frand(0,1);
        $chestprob=round($chestprob,9);
        $chest = $object1->NORMINV($chestprob,$chestmean,3.5);        

/*    THIGH [1 cm distal]
If MALE then:
THIGHMEAN = 28.993 + 0.38 * MASS
THIGH = (NORM.INV(RAND(),THIGHMEAN,1.8)
*/

/*
THIGHMEAN = 31.6+0.3452*MASS
*/

        //$thighmean_distal = 28.993 + 0.38 * $mass;
        $thighmean_distal = 31.6 + 0.3452 * $mass;            // new
		
        $thighprob_distal=$this->frand(0,1);
        $thighprob_distal=round($thighprob_distal,9);
        $thigh_distal = $object1->NORMINV($thighprob_distal,$thighmean_distal,1.8);
 
        
/*  THIGH [mid]
If MALE then:
THIGHMMEAN = 3.57 + 0.839 * THIGH
THIGH mid = (NORM.INV(RAND(),THIGHMMEAN,1.5)
 */ 
        
        
        $thighmean_mid = 3.57 + 0.839 * $thigh_distal;
        $thighprob_mid=$this->frand(0,1);
        $thighprob_mid=round($thighprob_mid,9);
        $thigh_mid = $object1->NORMINV($thighprob_mid,$thighmean_mid,1.5);      
        
 /*    CALF
If MALE then:
CALFMEAN = 21.138 + 0.22 * MASS
CALF = (NORM.INV(RAND(),CALFMEAN,1.0)
*/
        
        
    $calfmean=21.138 + 0.22 * $mass;
    $calfprob=$this->frand(0,1);
    $calfprob=round($calfprob,9);
    $calf = $object1->NORMINV($calfprob,$calfmean,1.0);    
        
              
/*ANKLE
If MALE then:
ANKLEMEAN = 15.11 + 0.11 * MASS
ANKLE = (NORM.INV(RAND(),ANKLEMEAN,0.8)
*/    
        
    $anklemean= 15.11 + 0.11 * $mass;
    $ankleprob=$this->frand(0,1);
    $ankleprob=round($ankleprob,9);
    $ankle=$object1->NORMINV($ankleprob,$anklemean,0.8);
        
              

// Lengths and breadths
/*   SITTING HEIGHT
If MALE then:
SITHEIGHT% = -0.0008*HEIGHT + 0.6758
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.006)
If sub-population = ‘Athlete’ then:
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.006,0.003)

CHECK:
If MALE then
If SITTING HEIGHT / HEIGHT < 0.48 then SITTING HEIGHT = HEIGHT * 0.48


If FEMALE then:
SITHEIGHT% = -0.0010*HEIGHT + 0.7263
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.005)
If sub-population = ‘Athlete’ then:
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.004,0.002)


CHECK:
If FEMALE then
If SITTING HEIGHT / HEIGHT < 0.49 then SITTING HEIGHT = HEIGHT * 0.49*/ 
    
    
        $sitheight_per = - 0.0008 * $height + 0.6758;
        $sitprob=$this->frand(0,1);
        $sitprob=round($sitprob,9);
        $sittingheight=$object1->NORMINV($sitprob,$sitheight_per,0.006);
        
        if($measuredpopulation == 'Athlete')
        {
            $sittingheight=$object1->NORMINV($sitprob,$sitheight_per - 0.006,0.003);
            
        }
       if(($sittingheight / $height) < 0.48)
            {
                $sittingheight =  $height * 0.48;
            } 
        
 /*    
ACROMIALE-RADIALE length
If MALE then:
ARmean = 0.17 * HEIGHT + 0.021 * MASS + 1.625
AR = (NORM.INV(RAND(),ARmean,0.9)
*/           
        
        $acromialemean= 0.17 * $height + 0.021 * $mass + 1.625;
        $acroprob=$this->frand(0,1);
        $acroprob=round($acroprob,9);
        $AR = $object1->NORMINV($acroprob,$acromialemean,0.9);
       
        
  /*RADIALE-STYL length
If MALE then:
RSmean = 0.2156 * AR - 0.0721 * AGE + 0.0819 * HEIGHT + 0.0329 * MASS + 3.415
RS = (NORM.INV(RAND(),RSmean,0.8)
 */       
        
         $rsmean = 0.2156 * $AR - 0.0721 * $age + 0.0819 * $height + 0.0329 * $mass + 3.415;
        $rsprob=$this->frand(0,1);
        $rsprob=round($rsprob,9);
        $RS = $object1->NORMINV($rsprob,$rsmean,0.8);
        
        
 /*    
MIDSTYL-DACTY length
If MALE then:
MDmean = 0.1515 * AR + 0.01294 * RS + 0.0471 * HEIGHT + 0.0304 * AGE + 1.463
MD = (NORM.INV(RAND(),MDmean,0.65)
 */ 
        $MDmean=0.1515 * $AR + 0.01294 * $RS + 0.0471 * $height + 0.0304 * $age + 1.463;
        $mdprob=$this->frand(0,1);
        $mdprob=round($mdprob,9);
        $MD = $object1->NORMINV($mdprob,$MDmean,0.65);
  
}

    
    
    
//if($gender == 'F')
else   // in case of female    
{
/*
If FEMALE then:
HEADMEAN = 50.22 + 0.066 * MASS
HEAD = (NORM.INV(RAND(),HEADMEAN,0.7)
*/
         $headmean= 50.22 + 0.066 * $mass;
         $headprob=$this->frand(0,1);
         $headprob=round($headprob,9); 
         $Head = $object1->NORMINV($headprob,$headmean,0.7);
        
/*
If FEMALE then:
NECKMEAN = 23.26 + 0.146 * MASS
NECK = (NORM.INV(RAND(),NECKMEAN,0.75)
*/
        
         $neckmean=  23.26 + 0.146 * $mass;
         $neckprob=$this->frand(0,1);
         $neckprob=round($neckprob,9);
         $neck = $object1->NORMINV($neckprob,$neckmean,0.75);
               
        
/* 
If FEMALE then:
ARMMEAN = 6.684 * BMI + 0.917
ARMR [relaxed] = (NORM.INV(RAND(),ARMMEAN,1.3)


Relaxed arm (New Formula)
-6.97 + (2.208*BMI) - (0.025*BMI^2)
*/

//$armmean=6.684 * $bmi + 0.917;
         $armmean=  -6.97 + (2.208 *$bmi) - (0.025 * pow($bmi,2) ) ;
         $armprob=$this->frand(0,1);
         $armprob=round($armprob,9);
         $relaxed_arm=$object1->NORMINV($armprob,$armmean,1.3);  
        
        
/*If FEMALE then:
ARMFMEAN = 0.967 * ARMR [relaxed] + 2.034
ARMF [flexed] = (NORM.INV(RAND(),ARMFMEAN,0.8)*/
    
        $armmean_fl=0.967 * $relaxed_arm + 2.034;
        $armprob_fl=$this->frand(0,1);
        $armprob_fl=round($armprob_fl,9);
        $relaxed_arm_fl=$object1->NORMINV($armprob_fl,$armmean_fl,0.8);
        
		/*If FEMALE then:
FOREARMMEAN = 15.03 + 0.153 * MASS
FOREARM [max] = (NORM.INV(RAND(),FOREARMMEAN,0.6) */
        
        $forearmmean = 15.03 + 0.153 * $mass;
        $forearmprob=$this->frand(0,1);
        $forearmprob=round($forearmprob,9);
        $forearm_max = $object1->NORMINV($forearmprob,$forearmmean,0.6);
        
		
/*If FEMALE then:
WRISTMEAN = 11.043 + 0.077 * MASS
WRIST = (NORM.INV(RAND(),WRISTMEAN,0.4)		*/
		
        $wristmean=11.043 + 0.077 * $mass;
        $wristprob=$this->frand(0,1);
        $wristprob=round($wristprob,9);
        $wrist = $object1->NORMINV($wristprob,$wristmean,0.4);
       
 /*If FEMALE then:
CHESTMEAN = 2.354 * BMI + 35.484
CHEST = (NORM.INV(RAND(),CHESTMEAN,3)*/       
        
        $chestmean=2.354 * $bmi + 35.484;
        $chestprob=$this->frand(0,1);
        $chestprob=round($chestprob,9);
        $chest = $object1->NORMINV($chestprob,$chestmean,3);
        
        
/*THIGHMEAN = 26.643 + 0.5 * MASS
THIGH = (NORM.INV(RAND(),THIGHMEAN,1.6)*/

/*  (NEW)
THIGHMEAN = 28.3 + 0.4735*MASS
*/
		 
       // $thighmean_distal = 26.643 + 0.5 * $mass;
        $thighmean_distal = 28.3 + 0.4735 * $mass;       // NEW
        $thighprob_distal=$this->frand(0,1);
        $thighprob_distal=round($thighprob_distal,9);
        $thigh_distal = $object1->NORMINV($thighprob_distal,$thighmean_distal,1.6);
        
        /*
         THIGHMMEAN = 2.89 + 0.835 * THIGH
         THIGH mid = (NORM.INV(RAND(),THIGHMMEAN,1.4) 
        */
        $thighmean_mid = 2.89 + 0.835 * $thigh_distal;
        $thighprob_mid=$this->frand(0,1);
        $thighprob_mid=round($thighprob_mid,9);
        $thigh_mid = $object1->NORMINV($thighprob_mid,$thighmean_mid,1.4);
        
        
/*  If FEMALE then:
CALFMEAN = 19.66 + 0.263 * MASS
CALF = (NORM.INV(RAND(),CALFMEAN,0.9)*/
        $calfmean=19.66 + 0.263 * $mass;
        $calfprob=$this->frand(0,1);
        $calfprob=round($calfprob,9);
        $calf = $object1->NORMINV($calfprob,$calfmean,0.9);
        
 /*
If FEMALE then:
ANKLEMEAN = 14.27 + 0.123 * MASS
ANKLE = (NORM.INV(RAND(),ANKLEMEAN,0.65)
*/ 
        
        $anklemean= 14.27 + 0.123 * $mass;
        $ankleprob=$this->frand(0,1);
        $ankleprob=round($ankleprob,9);
        $ankle=$object1->NORMINV($ankleprob,$anklemean,0.65);
        
        
        
        /*
        
        If FEMALE then:
        SITHEIGHT% = -0.0010*HEIGHT + 0.7263
        SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.005)
        If sub-population = ‘Athlete’ then:
        SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.004,0.002)


        CHECK:
        If FEMALE then
        If SITTING HEIGHT / HEIGHT < 0.49 then SITTING HEIGHT = HEIGHT * 0.49
        */
        $sitheight_per = -0.0010 * $height + 0.7263;
        $sitprob=$this->frand(0,1);
        $sitprob=round($sitprob,9);
        $sittingheight=$object1->NORMINV($sitprob,$sitheight_per,0.005);
        
                if($measuredpopulation == 'Athlete')
                {
                    $sittingheight=$object1->NORMINV($sitprob,$sitheight_per - 0.004,0.002);
                    
                }
         if(($sittingheight / $height) < 0.49)
                    {
                        $sittingheight =  $height * 0.49;
                    }
        
        /*
        If FEMALE then:
            ARmean = 0.181 * HEIGHT + 0.071 * BMI - 0.63
            AR = (NORM.INV(RAND(),ARmean,0.8)
        */
        
        $acromialemean= 0.181 * $height + 0.071 * $bmi - 0.63;
        $acroprob=$this->frand(0,1);
        $acroprob=round($acroprob,9);
        $AR = $object1->NORMINV($acroprob,$acromialemean,0.8);
        
/*   If FEMALE then:
RSmean = 0.327 * AR - 0.0752 * AGE + 0.835 * HEIGHT + 1.3472
RS = (NORM.INV(RAND(),RSmean,0.9)*/
 
 /*(NEW)
If FEMALE then:
RSmean = 0.327 * AR - 0.0752 * AGE + 0.0835 * HEIGHT + 1.3472
*/
		
        $rsmean = 0.327 * $AR - 0.0752 * $age + 0.0835 * $height + 1.3472;
        $rsprob=$this->frand(0,1);
        $rsprob=round($rsprob,9);
        $RS = $object1->NORMINV($rsprob,$rsmean,0.9); 
		
		
        
 /*If FEMALE then:
MDmean = 0.231 * AR + 0.0544 * HEIGHT + 0.0295 * AGE + 0.8369 
MD = (NORM.INV(RAND(),MDmean,0.6)*/       
        $MDmean=0.231 * $AR + 0.0544 * $height + 0.0295 * $age + 0.8369; 
        $mdprob=$this->frand(0,1);
        $mdprob=round($mdprob,9);
        $MD = $object1->NORMINV($mdprob,$MDmean,0.6); 
        
        
    }


    
/*        [both MALE and FEMALE]:
ARM LENGTH = AR + RS + MD

If ARM LENGTH / HEIGHT < 0.41 then 
AR = AR * 1.03
RS = RS * 1.03
MD = MD * 1.03

If ARM LENGTH / HEIGHT > 0.47 then 
AR = AR * 0.98
RS = RS * 0.98
MD = MD * 0.98*/
        
    $armlength=$AR+$RS+$MD;
    if(($armlength/$height) < 0.41)
    {
        $AR = $AR * 1.03;
        $RS = $RS * 1.03;
        $MD = $MD * 1.03;
    }
    
    if(($armlength/$height)  > 0.47){
        $AR = $AR * 0.98;
        $RS = $RS * 0.98;
        $MD = $MD * 0.98;
    }
       
   
    
    
/*    BRACHIAL INDEX = RS / AR
If MALE

If BRACHIAL INDEX < 0.7 then 
AR = RS * 1.429

If BRACHIAL INDEX > 0.8 then 
AR = RS * 1.25

If FEMALE

If BRACHIAL INDEX < 0.71 then 
AR = RS * 1.429

If BRACHIAL INDEX > 0.78 then 
AR = RS * 1.25*/
    
  $brachial_index=$RS/$AR;
  if($gender == 'M' )
  {
     if($brachial_index < 0.7)
     {
        $AR = $RS * 1.429;
     }
     if($brachial_index > 0.8) 
     {
         $AR = $RS * 1.25;
     }
  }
 if($gender == 'F' )
 {
         if($brachial_index < 0.71)
         {
            $AR = $RS * 1.429;
         }
         if($brachial_index > 0.78) 
         {
             $AR = $RS * 1.25;
         } 
 }
    
    $fieldData[0]->radStyl=round($RS,2);
    $radStyl=round($RS,2);
    $fieldData[0]->midStylDact=round($MD,2);    
    $midStylDact=round($MD,2);    
    
	$fieldData[0]->acRad=round($AR,2);    
    $acRad=round($AR,2);    
    
    
    
    
/*
ILIOSPINALE height
If MALE then:
ILmean = -1.184 * AR - 0.648 * SITTING HEIGHT + 1.095 * HEIGHT - 2.316 
IL = (NORM.INV(RAND(),ILmean,6)
*/

// MALE CASE


    if( $gender == 'M')
    {
    $ILmean= -1.184 * $AR - 0.648 * $sittingheight + 1.095 * $height - 2.316;   
    $ILprob=$this->frand(0,1);
    $ILprob=round($ILprob,9);    
    $IL = $object1->NORMINV($ILprob,$ILmean,6);
        
/*If MALE then:
TROCHmean = 0.883 * IL + 2.211 
TROCH = (NORM.INV(RAND(),TROCHmean,3.3)
 */           
  
        $TROCHmean = 0.883 * $IL + 2.211;
        $TROCHprob=$this->frand(0,1);
        $TROCHprob=round($TROCHprob,9); 
        $TROCH = $object1->NORMINV($TROCHprob,$TROCHmean,3.3);
 
//TROCHANT-TIBIALE height
/*If MALE then:
TTmean = 0.149 * RS + 0.212 * HEIGHT + 2.6 
TT = (NORM.INV(RAND(),TTmean,1.4)
 */  
        
        $TTmean=0.149 * $RS + 0.212 * $height + 2.6 ; 
        $TTprob=$this->frand(0,1);
        $TTprob=round($TTprob,9);
        $TT = $object1->NORMINV($TTprob,$TTmean,1.4);        
    }
    else // FEMALE CASE
    {
		 /* If FEMALE then:
ILmean = -0.063 * AR - 0.021 * SITTING HEIGHT + 0.029 * AGE + 0.071 
IL = (NORM.INV(RAND(),ILmean,4)*/
	    /*
        (NEW)
        If FEMALE then:
            ILmean = -21.96 – 0.381 * MASS + 0.786 * HEIGHT
            IL = (NORM.INV(RAND(),ILmean,4)
        */
        //$ILmean= -0.063 * $AR - 0.021 * $sittingheight + 0.029 * $age + 0.071 ;   (OLD)  
       
        $ILmean= -21.96 - 0.381 * $measuredmass + 0.786 * $height;  //(ALtered)	
        $ILprob=$this->frand(0,1);
        $ILprob=round($ILprob,9);    
        $IL = $object1->NORMINV($ILprob,$ILmean,4);
        
        /* If FEMALE then:
TROCHmean = 0.874 * IL + 4.252 
TROCH = (NORM.INV(RAND(),TROCHmean,3.8) */
        $TROCHmean = 0.874 * $IL + 4.252;
        $TROCHprob=$this->frand(0,1);
        $TROCHprob=round($TROCHprob,9); 
        $TROCH = $object1->NORMINV($TROCHprob,$TROCHmean,3.8);
        
   /* If FEMALE then:
TTmean = 0.2741 * HEIGHT – 3.3626 
TT = (NORM.INV(RAND(),TTmean,1.8) */     
        $TTmean= 0.2741 * $height - 3.3626;
        $TTprob=$this->frand(0,1);
        $TTprob=round($TTprob,9);
        $TT = $object1->NORMINV($TTprob,$TTmean,1.8);
    }
    
    $fieldData[0]->iliospinale=round($IL,2); 
    
    $fieldData[0]->troch=round($TROCH,2); 

    $fieldData[0]->trochTib=round($TT,2);    

    
 //TIBIALE LATERALE height
/*If MALE then:
TLmean = 0.2166 * RS + 0.2982 * AR + 0.1995 * HEIGHT – 0.1167 * AGE – 1.13 
TL = (NORM.INV(RAND(),TLmean,1.5)

If FEMALE then:
TLmean = 0.2927 * RS + 0.331 * AR + 0.1675 * HEIGHT – 0.0884 * AGE – 0.19 
TL = (NORM.INV(RAND(),TLmean,1.4) */
    
   /* echo $AR; echo "<br>";
    echo $height; echo "<br>";
    echo $age; echo "<br>";*/
    
    if($gender == 'M')
    {
        $TLmean = 0.2166 * $RS + 0.2982 * $AR + 0.1995 * $height - 0.1167 * $age - 1.13 ;
        $TLprob=$this->frand(0,1);
        $TLprob=round($TLprob,9);
        $TL = $object1->NORMINV($TLprob,$TLmean,1.5);
    }
    if($gender == 'F')
    {
        $TLmean = 0.2927 * $RS + 0.331 * $AR + 0.1675 * $height - 0.0884 * $age - 0.19 ;
        $TLprob=$this->frand(0,1);
        $TLprob=round($TLprob,9);
        $TL = $object1->NORMINV($TLprob,$TLmean,1.4); 
    }
 
    $fieldData[0]->tibLat=round($TL,2);   

	
//TIB MED-SPHY TIB height
/*If MALE then:
TMSTmean = 0.1009 * TL + 0.1146 * TT + 0.2942 * RS + 0.1536 * AR – 0.0625 * AGE + 0.1129 * HEIGHT – 1.55 
TMST = (NORM.INV(RAND(),TMSTmean,1.1)

If FEMALE then:
TMSTmean = 0.1933 * TL + 0.0821 * TT + 0.1322 * RS + 0.3407 * AR + 0.0773 * HEIGHT – 1.679 
TMST = (NORM.INV(RAND(),TMSTmean,1.15) */   
    
    
    if($gender == 'M')
    {
        $TMSTmean = 0.1009 * $TL + 0.1146 * $TT + 0.2942 * $RS + 0.1536 * $AR - 0.0625 * $age + 0.1129 * $height - 1.55;
        $TMSprob=$this->frand(0,1);
        $TMSprob=round($TMSprob,9);
        $TMST = $object1->NORMINV($TMSprob,$TMSTmean,1.1);
    }
     if($gender == 'F')
    {
        $TMSTmean = 0.1933 * $TL + 0.0821 * $TT + 0.1322 * $RS + 0.3407 * $AR + 0.0773 * $height - 1.679 ;
        $TMSprob=$this->frand(0,1);
        $TMSprob=round($TMSprob,9);
        $TMST = $object1->NORMINV($TMSprob,$TMSTmean,1.15);
    }
    
 
    $fieldData[0]->tibMed=round($TMST,2);   

        
//BIACROMIAL breadth
/*If MALE then:
BIAmean = 0.1671 * RS + 0.0475 * AGE + 0.0905 * HEIGHT + 0.1137 * MASS + 9.55 
BIA = (NORM.INV(RAND(),BIAmean,1.0)

If FEMALE then:
BIAmean = 0.1335 * SITTING HEIGHT + 0.1548 * RS + 0.1815 * AR + 0.1202 * AGE + 0.1031 * MASS + 6.67 
BIA = (NORM.INV(RAND(),BIAmean,1.1)*/  
       
    if($gender == 'M')
    {
       $BIAmean= 0.1671 * $RS + 0.0475 * $age + 0.0905 * $height + 0.1137 * $mass + 9.55;
       $BIAprob=$this->frand(0,1);
       $BIAprob=round($BIAprob,9);  
       $BIA = $object1->NORMINV($BIAprob,$BIAmean,1.0);    
    }
     if($gender == 'F')
    {
       $BIAmean= 0.1335 * $sittingheight + 0.1548 * $RS + 0.1815 * $AR + 0.1202 * $age + 0.1031 * $mass + 6.67;
       $BIAprob=$this->frand(0,1);
       $BIAprob=round($BIAprob,9);  
       $BIA = $object1->NORMINV($BIAprob,$BIAmean,1.1);  
    }
    

    $fieldData[0]->biac=round($BIA,2); 
    $biac=round($BIA,2); 
    
    
    
/* BIDELTOID breadth
If MALE then:
BIDEL = (NORM.INV(RAND(),BIA + 4,1.0)

If FEMALE then:
BIDEL = (NORM.INV(RAND(),BIA + 3,0.8)

CHECK :

If BIDEL < BIA + 0.6 then
BIDEL = BIA + 0.6  */ 
    
    if($gender == 'M')
    {
        $BIDELprob=$this->frand(0,1);
        $BIDELprob=round($BIDELprob,9);
        $BIDEL = $object1->NORMINV($BIDELprob,$BIA + 4,1.0);
    }
     if($gender == 'F')
    {
        $BIDELprob=$this->frand(0,1);
        $BIDELprob=round($BIDELprob,9);
        $BIDEL = $object1->NORMINV($BIDELprob,$BIA + 3,0.8);
    }
    if($BIDEL < $BIA + 0.6)
    {
        $BIDEL =$BIA + 0.6;
    }
    

    $fieldData[0]->bideltoid=round($BIDEL,2);   
    
 /*
BIILIOCRISTAL breadth
If MALE then:
BIILmean = 0.2053 * BIA + 0.0929 * RS + 0.1067 * AR - 0.0368 * HEIGHT + 0.114 * MASS + 12 
BIIL = (NORM.INV(RAND(),BIILmean,0.85)

If FEMALE then:
BIILmean = 0.1102 * BMI + 0.3379 * BIA + 0.04 * CHEST + 0.0512 * HEIGHT + 0.0621 * AGE – 0.71
BIIL = (NORM.INV(RAND(),BIILmean,0.85)*/
    
    if($gender == 'M')
    {
        $BIILmean=0.2053 * $BIA + 0.0929 * $RS + 0.1067 * $AR - 0.0368 * $height + 0.114 * $mass + 12;
        $BIILprob=$this->frand(0,1);
        $BIILprob=round($BIILprob,9);
        $BIIL = $object1->NORMINV($BIILprob,$BIILmean,0.85);
    }
     if($gender == 'F')
    {
        $BIILmean=0.1102 * $bmi + 0.3379 * $BIA + 0.04 * $chest + 0.0512 * $height + 0.0621 * $age - 0.71;
        $BIILprob=$this->frand(0,1);
        $BIILprob=round($BIILprob,9);
        $BIIL = $object1->NORMINV($BIILprob,$BIILmean,0.85);
    }

    $fieldData[0]->billio=round($BIIL,2);   
    $billio=round($BIIL,2);   
  
    
/*    BITROCHANTERIC breadth
If MALE then:
BITROCH = (NORM.INV(RAND(),BIIL * 1.08,1.2)

If FEMALE then:
BITROCH = (NORM.INV(RAND(),BIIL * 1.1,2)*/
    
    if($gender == 'M')
    {
        $BITROCHprob=$this->frand(0,1);
        $BITROCHprob=round($BITROCHprob,9);
        $BITROCH = $object1->NORMINV($BITROCHprob,$BIIL * 1.08,1.2);
    }
     if($gender == 'F')
    {
        $BITROCHprob=$this->frand(0,1);
        $BITROCHprob=round($BITROCHprob,9);
        $BITROCH = $object1->NORMINV($BITROCHprob,$BIIL * 1.1,2);
    }
    

     $fieldData[0]->bitrochanteric=round($BITROCH,2); 

    
/*FOOT LENGTH
If MALE then:
FOOTmean = 0.058 * BIIL + 0.0574 * BIA + 0.1653 * AR + 0.0668 * HEIGHT + 5.34
FOOT = (NORM.INV(RAND(),FOOTmean,0.7)

If FEMALE then:
FOOTmean = -0.0684 * BMI + 0.0485 * SITTING HEIGHT + 0.1241 * BIIL + 0.1151 * AR + 0.0965 * RS + 6.12
FOOT = (NORM.INV(RAND(),FOOTmean,0.85)    */
    
    
    if($gender == 'M')
    {
        $FOOTmean = 0.058 * $BIIL + 0.0574 * $BIA + 0.1653 * $AR + 0.0668 * $height + 5.34;
        $FOOTprob=$this->frand(0,1);
        $FOOTprob=round($FOOTprob,9);
        $FOOT = $object1->NORMINV($FOOTprob,$FOOTmean,0.7);
    }
    if($gender == 'F')
    {
        $FOOTmean = -0.0684 * $bmi + 0.0485 * $sittingheight + 0.1241 * $BIIL + 0.1151 * $AR + 0.0965 * $RS + 6.12;
        $FOOTprob=$this->frand(0,1);
        $FOOTprob=round($FOOTprob,9);
        $FOOT = $object1->NORMINV($FOOTprob,$FOOTmean,0.7);
    }

    $fieldData[0]->foot=round($FOOT,2); 
    
/*
    TRANSVERSE CHEST
If MALE then:
TCmean = 0.0291 * SITTING HEIGHT + 0.3399 * BIIL + 0.2428 * BIA + 0.0953 * CHEST + 0.0295 * AGE – 1.94
TC = (NORM.INV(RAND(),TCmean,0.8)

If FEMALE then:
TCmean = 0.2651 * BIIL + 0.4214 * BIA + 0.0508 * WAIST + 0.19
TC = (NORM.INV(RAND(),TCmean,1.1)

*/
    
    
if($gender == 'M')    
{
    $TCmean = 0.0291 * $sittingheight + 0.3399 * $BIIL + 0.2428 * $BIA + 0.0953 * $chest + 0.0295 * $age - 1.94;
    $TCprob=$this->frand(0,1);
    $TCprob=round($TCprob,9);
    $TC = $object1->NORMINV($TCprob,$TCmean,0.8);
}
else
 {
    $TCmean = 0.2651 * $BIIL + 0.4214 * $BIA + 0.0508 * $waist + 0.19;
    $TCprob=$this->frand(0,1);
    $TCprob=round($TCprob,9);
    $TC = $object1->NORMINV($TCprob,$TCmean,1.1);
 }
    

    $fieldData[0]->trChest=round($TC,2); 
    $trChest=round($TC,2); 
     
/*    A-P CHEST
If MALE then:
A-Pmean = -0.0611 * SITTING HEIGHT - 0.026 * WAIST + 0.145 * MASS + 16.5351 
A-P = (NORM.INV(RAND(),A-Pmean,0.9)

If FEMALE then:
A-Pmean = 0.1691 * BMI + 0.1096 * TC + 0.0991 * AR + 0.0393 * CHEST + 4.39
A-P = (NORM.INV(RAND(),A-Pmean,0.8)
    */
    
    
if($gender == 'M')
{
    $APmean = - 0.0611 * $sittingheight - 0.026 * $waist + 0.145 * $mass + 16.5351; 
    $APprob=$this->frand(0,1);
    $APprob=round($APprob,9);
    $AP = $object1->NORMINV($APprob,$APmean,0.9);    
}
else
{
    $APmean = 0.1691 * $bmi + 0.1096 * $TC + 0.0991 * $AR + 0.0393 * $chest + 4.39; 
    $APprob=$this->frand(0,1);
    $APprob=round($APprob,9);
    $AP = $object1->NORMINV($APprob,$APmean,0.8);    
}
    
    $fieldData[0]->apChest=round($AP,2);     
    $apChest=round($AP,2);     

    
/*ARM SPAN breadth
[both MALE and FEMALE]:
ALR [arm length ratio] = (AR + RS + MD)/HEIGHT [same as equation generated above]
ARMSP% = -11.9048* ALR^2 + 12.1905 * ALR - 2.0286
ARMCM = ARMSP% * HEIGHT
ARM SPAN = (NORM.INV(RAND(),ARMCM,1.0)

CHECK :

If ARM SPAN < HEIGHT * 0.90 then
ARM SPAN = HEIGHT * 0.90

If ARM SPAN > HEIGHT * 1.13 then
ARM SPAN = HEIGHT * 1.13  */  
    
    
    $alr=($AR+$RS+$MD)/$height;
    $armsp_per= - 11.9048 * pow($alr,2) + 12.1905 * $alr - 2.0286;
    $ARMCM = $armsp_per * $height;
    $ARMprob=$this->frand(0,1);
    $ARMprob=round($ARMprob,9);
    $ARMSPAN = $object1->NORMINV($ARMprob,$ARMCM,1.0);
     
    if($ARMSPAN  < $height * 0.90 )
    {
        $ARMSPAN = $height * 0.90;
    }
    else
    if($ARMSPAN  > $height * 1.13 )
    {
        $ARMSPAN = $height * 1.13;
    }
    
 
    $fieldData[0]->armSpan=round($ARMSPAN,2);     

/*HUMERUS (bi-epi)
If MALE then:
BIHUMmean = 0.0191 * BIIL + 0.0644 * FOOT + 0.161 * MASS + 0.0091 * HEIGHT + 2.25 
BIHUM= (NORM.INV(RAND(),BIHUMmean,0.2)

(NEW)
If MALE then:
NEW one is: BIHUMmean = 0.01929 * BIIL + 0.0639 * FOOT + 0.0161 * MASS + 0.00922 * HEIGHT + 2.252


If FEMALE then:
BIHUMmean = 0.0465 * FOOT + 0.0455 * BMI + 0.0225 * HEIGHT + 0.63
BIHUM= (NORM.INV(RAND(),BIHUMmean,0.2)*/    
    
    if($gender =='M')
    {
        $BIHUMmean = 0.01929 * $BIIL + 0.0639 * $FOOT + 0.0161 * $mass + 0.00922 * $height + 2.252 ;  //(Altered)
        $BIHUprob=$this->frand(0,1);
        $BIHUprob=round($BIHUprob,9);
        $BIHUM= $object1->NORMINV($BIHUprob,$BIHUMmean,0.2);
    }
    if($gender == 'F')
    {
        $BIHUMmean = 0.0465 * $FOOT + 0.0455 * $bmi + 0.0225 * $height + 0.63;
        $BIHUprob=$this->frand(0,1);
        $BIHUprob=round($BIHUprob,9);
        $BIHUM= $object1->NORMINV($BIHUprob,$BIHUMmean,0.2);
    }
    
    
    
    
/*FEMUR (bi-epi)
If MALE then:
BIFEMmean = 0.094 * BIHUM + 0.08999 * FOOT - 0.0119 * AGE + 0.0257 * MASS + 5.254 
BIFEM= (NORM.INV(RAND(),BIFEMmean,0.25)

If FEMALE then:
BIFEMmean = 0.0332 * FOOT + 0.0273 * BIIL + 0.0324 * MASS + 5.537 
BIFEM= (NORM.INV(RAND(),BIFEMmean,0.22) */ 
    
    
    if($gender == 'M')
    {
        $BIFEMmean = 0.094 * $BIHUM + 0.08999 * $FOOT - 0.0119 * $age + 0.0257 * $mass + 5.254 ;
        $BIFEprob=$this->frand(0,1);
        $BIFEprob=round($BIFEprob,9);
        $BIFE= $object1->NORMINV($BIFEprob,$BIFEMmean,0.25);
    }
    if($gender == 'F')
    {
        $BIFEMmean = 0.0332 * $FOOT + 0.0273 * $BIIL + 0.0324 * $mass + 5.537  ;
        $BIFEprob=$this->frand(0,1);
        $BIFEprob=round($BIFEprob,9);
        $BIFE= $object1->NORMINV($BIFEprob,$BIFEMmean,0.22);
    }
    
    
    
// ANOTHER BODY COMPOSITION MODULE VARIABLES */
    
    $fieldData[0]->option_height_measured=round($measuredheight,1);
    $fieldData[0]->option_weight_measured=round($measuredmass,2);
    $fieldData[0]->option_bmi=round($measuredBMI,2);
    $fieldData[0]->option_measuredpopulation=$measuredpopulation;    
    $fieldData[0]->option_waist=round($waist,2);
    $fieldData[0]->option_hip=round($hip,2);

    
    $fieldData[0]->option_whr=round($whr,2);
    $fieldData[0]->option_triceps=round($triceps,1);
    $fieldData[0]->option_biceps=round($biceps,1);
    $fieldData[0]->option_subscapular=round($subscapular,1);
   
    
    $fieldData[0]->option_sos=round($skinfold_summ,1);
    $fieldData[0]->option_height=round($options_height,1);
    $fieldData[0]->option_weight=round($options_weight,2);
       // Other factors
    $fieldData[0]->triceps=round($triceps,1);
    $triceps=round($triceps,1);
    
    $fieldData[0]->biceps=round($biceps,1);
    $biceps=round($biceps,1);
    
    $fieldData[0]->subscapular=round($subscapular,1);
    $subscapular=round($subscapular,1);
    
    $fieldData[0]->iliac_crest=round($Iliac_crest_skinfold,1);
    $iliac=round($Iliac_crest_skinfold,1);
    
    $fieldData[0]->supraspinale=round($Supraspinale_skinfold,1);
    $supraspinale=round($Supraspinale_skinfold,1);
    
    
    $fieldData[0]->abdominal=round($Abdominal_skinfold,1);
    $abdominal=round($Abdominal_skinfold,1);
    
    $fieldData[0]->calf=round($Medial_calf_skinfold,1);
    $calf=round($Medial_calf_skinfold,1);
    
    $fieldData[0]->mid_axilla=round($Midaxilla_skinfold,1);
    $mid_axilla=round($Midaxilla_skinfold,1);
    
    $fieldData[0]->thigh=round($Front_thigh_skinfold,1);
    $thigh=round($Front_thigh_skinfold,1);
    
    $fieldData[0]->relArmG=round($relaxed_arm,2);
    $relArmG=round($relaxed_arm,2);
    
    $fieldData[0]->flexArmG=round($relaxed_arm_fl,2);
    $flexArmG=round($relaxed_arm_fl,2);
    
    $fieldData[0]->waistG=round($waist,2);
    $waistG=round($waist,2);
    
    $fieldData[0]->hipG=round($hip,2);
    $hipG=round($hip,2);
    
    $fieldData[0]->calfG=round($calf,2);
    $calfG=round($calf,2);
    
    // Lengths and breadhs
    $fieldData[0]->humerus=round($BIHUM,2);
    $humerus=round($BIHUM,2);
    
    $fieldData[0]->femur=round($BIFE,2);
    $femur=round($BIFE,2);
    
    // heigth and body mass
    $fieldData[0]->height=round($measuredheight,1);
    $ht=round($measuredheight,1);
    
    $fieldData[0]->body_mass=round($measuredmass,2);
    $body_mass=round($measuredmass,2);
 
     
    $fieldData[0]->headG=round($Head,2);
    $headG=round($Head,2);
    
    $fieldData[0]->neckG=round($neck,2);
    $neckG=round($neck,2);
    
    $fieldData[0]->relArmG=round($relaxed_arm,2);
    $relArmG=round($relaxed_arm,2);
    
    $fieldData[0]->flexArmG=round($relaxed_arm_fl,2);
    $flexArmG=round($relaxed_arm_fl,2);    
    
    $fieldData[0]->forearmG=round($forearm_max,2);
    $forearmG=round($forearm_max,2);
    
    $fieldData[0]->wristG=round($wrist,2);
    $wristG=round($wrist,2);
    
    $fieldData[0]->chestG=round($chest,2);
    $chestG=round($chest,2);
       
    $fieldData[0]->thighG=round($thigh_distal,2);
    $thighG=round($thigh_distal,2);
    
    $fieldData[0]->midThighG=round($thigh_mid,2);
    $midThighG=round($thigh_mid,2);
    
    $fieldData[0]->calfG=round($calf,2);
    $calfG=round($calf,2);
    
    $fieldData[0]->ankleG=round($ankle,2);
    $ankleG=round($ankle,2);
    
    $fieldData[0]->sitting=round($sittingheight,2);
    $sitting=round($sittingheight,2);
    
    
    
    // 138171213
    
    // BODY FAT MEAN Variables Calculation
    // 10 Oct Start
    if($gender == 'M')
     {       
             
              $n = 0 ; // Total number of %BF  
              $totalBF = 0 ; // Total %BF
              $sumSquares = 0 ; // Sum of squares of %BF
             // Body Fat Mean    

              if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $supraspinale !== "" && $abdominal !== "" && $thigh !== "" && $calf !== "") 
              {   
               $BDa = 1.0988 - 0.0004 * ($triceps + $subscapular + $biceps + $supraspinale + $abdominal + $thigh + $calf)  ;
               $per_BFa = 495 / $BDa - 450 ; // %Body Fat 1 
               $lower_BFa = (($per_BFa - 4.8) * 10) / 10 ; // Lower Limit 1  
               $upper_BFa = (($per_BFa + 4.8) * 10) / 10 ; // Upper Limit 1  
               $totalBF = $totalBF + $per_BFa ;
               $sumSquares = $sumSquares + $per_BFa * $per_BFa ;
               $n = $n+1 ;
              } 

              if($abdominal !== "" && $thigh !== "") 
              {     
               $BDb = 1.08543 - 0.000886 * $abdominal - 0.0004 * $thigh ;
               $per_BFb = 495 / $BDb - 450 ; // %Body Fat 2 
               $lower_BFb = (($per_BFb - 6.6) * 10) / 10 ; // Lower Limit 2  
               $upper_BFb = (($per_BFb + 6.6) * 10) / 10 ; // Upper Limit 2  
               $totalBF = $totalBF + $per_BFb ;
               $sumSquares = $sumSquares + $per_BFb * $per_BFb ;
               $n = $n+1 ;
              }

              if($subscapular !== "" && $thigh !== "") 
              {
               $BDc = 1.1043 - 0.001327 * $thigh - 0.00131 * $subscapular ;
               $per_BFc = 495 / $BDc - 450 ; // %Body Fat 3  
               $lower_BFc = (($per_BFc - 5.1) * 10) / 10 ; // Lower Limit 3  
               $upper_BFc = (($per_BFc + 5.1) * 10) / 10 ; // Upper Limit 3
               $totalBF = $totalBF + $per_BFc ;
               $sumSquares = $sumSquares + $per_BFc * $per_BFc;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $abdominal !== "") 
              { 
               $BDd = 1.09665 - 0.00103 * $triceps - 0.00056 * $subscapular - 0.00054 * $abdominal ;
               $per_BFd = 495 / $BDd - 450 ; // %Body Fat 4
               $lower_BFd = (($per_BFd - 6.2) * 10) / 10 ; // Lower Limit 4  
               $upper_BFd = (($per_BFd + 6.2) * 10) / 10 ; // Upper Limit 4
               $totalBF = $totalBF + $per_BFd ;
               $sumSquares = $sumSquares + ($per_BFd * $per_BFd) ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $iliac !== "") 
              { 
               $BDe = 1.1765 - 0.0744 * log10($triceps + $biceps + $subscapular + $iliac) ;
               $per_BFe = 495 / $BDe - 450 ; // %Body Fat 5
               $lower_BFe = (($per_BFe - 9.1) * 10) / 10 ; // Lower Limit 5  
               $upper_BFe = (($per_BFe + 9.1) * 10) / 10 ; // Upper Limit 5 
               $totalBF = $totalBF + $per_BFe ;
               $sumSquares = $sumSquares + $per_BFe * $per_BFe ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $iliac !== "" && $abdominal !== "" && $thigh !== "" && $calf !== "" && $mid_axilla !== "") 
              { 
               $BDf = 1.1091 - 0.00052 * ($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf) + 0.00000032 *  (($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf ) * ($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf ) )  ;  // edited bracket [=>()]
               $per_BFf = 495 / $BDf - 450 ; // %Body Fat 6
               $lower_BFf = (($per_BFf - 4.6) * 10) / 10 ; // Lower Limit 6  
               $upper_BFf = (($per_BFf + 4.6) * 10) / 10 ; // Upper Limit 6  
               $totalBF = $totalBF + $per_BFf ;
               $sumSquares = $sumSquares + ($per_BFf * $per_BFf) ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $abdominal !== "" && $mid_axilla !== "") 
              { 
               $BDg = 1.10647 - 0.00162 * $subscapular - 0.00144 * $abdominal - 0.00077 * $triceps + 0.00071 * $mid_axilla ;
               $per_BFg = 495 / $BDg - 450 ; // %Body Fat 7
               $lower_BFg = (($per_BFg - 5) * 10) / 10 ; // Lower Limit 7  
               $upper_BFg = (($per_BFg + 5) * 10) / 10 ; // Upper Limit 7 
               $totalBF = $totalBF + $per_BFg ;
               $sumSquares = $sumSquares + ($per_BFg * $per_BFg) ;
               $n = $n+1 ;
              } 
    
         /*        
         echo $totalBF;
         echo "<br>";
         echo $n;
         */
     }
    
     elseif($gender == 'F')
     {
            
                 $n = 0 ; // Total number of %BF  
                 $totalBF = 0 ; // Total %BF
                 $sumSquares = 0 ; // Sum of squares of %BF  

                  if($triceps !== "" && $subscapular !== "" && $supraspinale !== "" && $abdominal !== "" && $thigh !== "" && $calf !== "") 
                  {  
                   $BDa = 1.20953 - 0.08294 * log10($triceps + $subscapular + $supraspinale + $abdominal + $thigh + $calf )  ;
                   $per_BFa = 495 / $BDa - 450 ; // %Body Fat 1
                   $lower_BFa = (($per_BFa - 5.6) * 10) / 10 ; // Lower Limit 1  
                   $upper_BFa = (($per_BFa + 5.6) * 10) / 10 ; // Upper Limit 1 
                   $totalBF = $totalBF + $per_BFa ;
                   $sumSquares = $sumSquares + ($per_BFa * $per_BFa) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $supraspinale !== "" && $calf !== "") 
                  { 
                   $BDb = 1.17484 - 0.07229 * log10($triceps + $subscapular + $supraspinale + $calf ) ;
                   $per_BFb = 495 / $BDb - 450 ; // %Body Fat 2 
                   $lower_BFb = (($per_BFb - 5.6) * 10) / 10 ; // Lower Limit 2  
                   $upper_BFb = (($per_BFb + 5.6) * 10) / 10 ; // Upper Limit 2 
                   $totalBF = $totalBF + $per_BFb ;
                   $sumSquares = $sumSquares + ($per_BFb * $per_BFb) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $thigh !== "") 
                  { 
                   $BDc = 1.06234 - 0.000688 * $subscapular - 0.00039 * $triceps - 0.00025 * $thigh ;
                   $per_BFc = 495 / $BDc - 450 ; // %Body Fat 3
                   $lower_BFc = (($per_BFc - 5.6) * 10) / 10 ; // Lower Limit 3  
                   $upper_BFc = (($per_BFc + 5.6) * 10) / 10 ; // Upper Limit 3
                   $totalBF = $totalBF + $per_BFc ;
                   $sumSquares = $sumSquares + ($per_BFc * $per_BFc) ;
                   $n = $n+1 ;
                  } 

                  if($triceps !== "" && $subscapular !== "" && $iliac !== "") 
                  { 
                   $BDd =1.0987 - 0.00122 * ($triceps + $subscapular + $iliac) + 0.00000263 *  ($triceps + $subscapular + $iliac) * ($triceps + $subscapular + $iliac )  ;
                   $per_BFd = 495 / $BDd - 450 ; // %Body Fat 4
                   $lower_BFd = (($per_BFd - 5.2) * 10) / 10 ; // Lower Limit 4  
                   $upper_BFd = (($per_BFd + 5.2) * 10) / 10 ; // Upper Limit 4  
                   $totalBF = $totalBF + $per_BFd ;
                   $sumSquares = $sumSquares + ($per_BFd * $per_BFd) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $iliac !== "") 
                  {  
                   $BDe = 1.0764 - 0.00081 * $iliac - 0.00088 * $triceps ;
                   $per_BFe = 495 / $BDe - 450 ; // %Body Fat 5
                   $lower_BFe = (($per_BFe - 7.2) * 10) / 10 ; // Lower Limit 5  
                   $upper_BFe = (($per_BFe + 7.2) * 10) / 10 ; // Upper Limit 5  
                   $totalBF = $totalBF + $per_BFe ;
                   $sumSquares = $sumSquares + ($per_BFe * $per_BFe) ;
                   $n = $n+1 ;       
                  }

                  if($triceps !== "" && $abdominal !== "" && $thigh !== "" && $iliac !== "" && $hipG !== "") 
                  { 
                   $BDf = 1.24374 - 0.03162 * log( $triceps + $abdominal + $thigh + $iliac ) - 0.00066 * $hipG;
                   $per_BFf = 495 / $BDf - 450 ; // %Body Fat 6
                   $lower_BFf = (($per_BFf - 7.2) * 10) / 10 ; // Lower Limit 6  
                   $upper_BFf = (($per_BFf + 7.2) * 10) / 10 ; // Upper Limit 6  
                   $totalBF = $totalBF + $per_BFf ;
                   $sumSquares = $sumSquares + ($per_BFf * $per_BFf) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $thigh !== "" && $iliac !== "") 
                  { 
                   $BDg = 1.21389 - 0.04057 * log($triceps + $thigh + $iliac) - 0.00016 * $age ;
                   $per_BFg = 495 / $BDg - 450 ; // %Body Fat 7
                   $lower_BFg = (($per_BFg - 7.8) * 10) / 10 ; // Lower Limit 7  
                   $upper_BFg = (($per_BFg + 7.8) * 10) / 10 ; // Upper Limit 7  
                   $totalBF = $totalBF + $per_BFg ;
                   $sumSquares = $sumSquares + ($per_BFg * $per_BFg) ;
                   $n = $n+1 ;
                  } 

                  if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $iliac !== "") 
                  {
                   $BDh = 1.1567 - 0.0717 * log10( $triceps+ $biceps + $subscapular + $iliac ) ;
                   $per_BFh = 495 / $BDh - 450 ; // %Body Fat 8
                   $lower_BFh = (($per_BFh - 10.8) * 10) / 10 ; // Lower Limit 8  
                   $upper_BFh = (($per_BFh + 10.8) * 10) / 10 ; // Upper Limit 8 
                   $totalBF = $totalBF + $per_BFh ;
                   $sumSquares = $sumSquares + ($per_BFh * $per_BFh) ;
                   $n = $n+1 ;
                  }

                  if($thigh !== "" && $iliac !== "") 
                  {
                   $BDi = 1.0852 - 0.0008 * $iliac - 0.0011 * $thigh ;
                   $per_BFi = 495 / $BDi - 450 ; // %Body Fat 9
                   $lower_BFi = (($per_BFi - 8.2) * 10) / 10 ; // Lower Limit 9  
                   $upper_BFi = (($per_BFi + 8.2) * 10) / 10 ; // Upper Limit 9
                   $totalBF = $totalBF + $per_BFi ;
                   $sumSquares = $sumSquares + ($per_BFi * $per_BFi) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $flexArmG !== "" && $subscapular !== "" && $hipG !== "") 
                  { 
                   $BDj = 1.12569 - 0.001835 * $triceps - 0.002779 / 2.54 * $hipG + 0.005419 / 2.54 * $flexArmG - 0.0007167 * $subscapular  ;
                   $per_BFj = 495 / $BDj - 450 ; // %Body Fat 10
                   $lower_BFj = (($per_BFj - 7.2) * 10) / 10 ; // Lower Limit 10  
                   $upper_BFj = (($per_BFj + 7.2) * 10) / 10 ; // Upper Limit 10
                   $totalBF = $totalBF + $per_BFj ;
                   $sumSquares = $sumSquares + ($per_BFj * $per_BFj) ;
                   $n = n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $relArmG !== "") 
                  { 
                   //$BDk = 0.97845 - 0.0002 * $triceps + 0.00088 * $ht - 0.00122 * $subscapular - 0.00234 * $relArmG  ;
                   $BDk = 0.97845 - 0.0002 * $triceps + 0.00088 * $ht - 0.00122 * $subscapular - 0.00234 * $relArmG  ;
                   $per_BFk = 495 / $BDk - 450 ; // %Body Fat 11
                   $lower_BFk = (($per_BFk - 7) * 10) / 10 ; // Lower Limit 11  
                   $upper_BFk = (($per_BFk + 7) * 10) / 10 ; // Upper Limit 11    
                   $totalBF = $totalBF + $per_BFk ;
                   $sumSquares = $sumSquares + ($per_BFk * $per_BFk) ;
                   $n = $n+1 ;
                  }
     }
    // BODY FAT MEAN Variables Calculation
    // 10 Oct End
    
    
// Fractionasation

    $pi = 3.1415962 ;    
     
    $meanFatZ = ($calf + $thigh + $abdominal + $triceps + $subscapular + $supraspinale)/6 ; 
    $fat_mass = ($meanFatZ * 3.25 + 12.13)/((170.18/$ht) * (170.18/$ht) * (170.18/$ht));    
    //document.getElementById("fat_mass").innerHTML = Math.floor(parseFloat(fat_mass) * 10)/10 ; // Fat Mass
    $fat_mass= (floor($fat_mass * 10))/10 ;
    
    
    $ZCorrChestG = (($chestG - $subscapular * $pi / 10) * 170.18 / $ht - 82.46) / 4.86 ; 
    $ZCorrRelArmG = (($relArmG - $triceps * $pi / 10) * 170.18 / $ht - 22.05) / 1.91 ; 
    $ZCorrThighG = (($thighG - $thigh * $pi / 10) * 170.18 / $ht - 47.34) / 3.59 ; 
    $ZCorrCalfG = (($calfG - $calf * $pi / 10) * 170.18 / $ht - 30.22) / 1.97 ;    // Calfg= Girth , Calf=Medial Calf 
    //var meanLeanZ = (ZCorrCalfG + ZCorrThighG + ZCorrRelArmG + ZCorrChestG + zscore_ForearmG) / 5 ; 
	
	// Kritika Code Start
       $grades = array();
       $grades[0]=$ZCorrCalfG;
       $grades[1]=$ZCorrThighG;
       $grades[2]=$ZCorrRelArmG;
       $grades[3]=$ZCorrChestG;
       $grades[4]=$zscore_ForearmG;
    
       $meanLeanZ = $this->getAvg($grades) ;   
       $meanLeanZ = $meanLeanZ ;
    
	// Kritika Code End
	
    $muscle_mass = ($meanLeanZ * 2.99 + 25.55)/ ( (170.18/$ht) * (170.18/$ht) * (170.18/$ht) ) ;    
    //document.getElementById("muscle_mass").innerHTML = Math.floor(parseFloat(muscle_mass) * 10)/10 ; // Muscle Mass
    $muscle_mass = (floor($muscle_mass) * 10)/10 ;
      
    $meanSkeletalZ = ($femur + $humerus + $ankleG + $wristG) / 4 ; 
    $bone_mass = ($meanSkeletalZ * 1.57 + 10.49)/ ( (170.18/$ht) * (170.18/$ht) * (170.18/$ht) );    
    //document.getElementById("bone_mass").innerHTML = Math.floor(parseFloat(bone_mass) * 10)/10 ; // Bone Mass
    $bone_mass= (floor($bone_mass) * 10)/10 ;
    
    
    $MeanResidualZ = ($biac + $trChest + $billio + $apChest) / 4 ; 
    $residual_mass = ($MeanResidualZ * 1.9 + 16.41)/ ( (170.18/$ht) * (170.18/$ht) * (170.18/$ht) );    
    //document.getElementById("residual_mass").innerHTML = Math.floor(parseFloat(residual_mass) * 10)/10 ; // Residual Mass
    $residual_mass =  (floor($residual_mass) * 10)/10 ;                                                     
    

    
    
    
    
    //Somatotype Variables
 
    $total = $triceps + $supraspinale + $subscapular ;
    
    $endomorph_score = (-0.7182 + 0.1451 * ($total) - 0.00068 * ($total * $total) + 0.0000014 * ($total * $total * $total)) ;  
    $mesomorph_score = 0.858 * $humerus + 0.601 * $femur + 0.188 * ( $flexArmG - ($triceps / 10)) + 0.161 * ( $calfG - ($calf / 10)) - 0.131 * $ht + 4.5 ;
           
    // Additional Calculation  , Dt: 11 oct 17
    //if HWR > 38.25 and HWR <40.75 then ECTOMORPH score = .463* HWR -17.63   
       
    //$hwr = $ht / (Math.cbrt(parseFloat(body_mass))) ; 
    $hwr = $ht / (pow($body_mass,1/3)) ; 
   // echo $hwr;
	
   
    if($hwr >= 40.75)
    {
     $ectomorph_score = 0.732 * $hwr - 28.58 ;  
    }
    else if($hwr < 38.25)
    {
     $ectomorph_score = 0.1 ;       
    }     
    else if($hwr > 38.25 && $hwr < 40.75 )  // added condition    
    {
     $ectomorph_score = .463 * $hwr - 17.63 ; 
    }
       
    //document.getElementById("somato").innerHTML = (Math.round(endomorph_score * 100) / 100)+", "+(Math.round(mesomorph_score * 100) / 100)+", "+(Math.round(ectomorph_score * 100) / 100) ;  
    $endomorph_score=   ($endomorph_score * 100) / 100;
    $mesomorph_score=   ($mesomorph_score * 100) / 100; 
    $ectomorph_score=   ($ectomorph_score * 100) / 100; 
    
        
    // BODY FATMean Calculation
    $meanBF = $totalBF / $n ; // Mean value 
    
    
    
    $fieldData[0]->meanbodyfat=round($meanBF,2);
    // Fractionation Variables                                                  
    $fieldData[0]->fat_mass=round($fat_mass,1);
    $fieldData[0]->muscle_mass=round($muscle_mass,1);
    $fieldData[0]->bone_mass=round($bone_mass,1);
    $fieldData[0]->residual_mass=round($residual_mass,1);
    // Somatotype Variables
    $fieldData[0]->ectomorph_score=round($ectomorph_score);
    $fieldData[0]->mesomorph_score=round($mesomorph_score);
    $fieldData[0]->endomorph_score=round($endomorph_score);
   
   
   return $fieldData;
    
	
	
}


public function getAvg ($grades) {
    $fLen = sizeof($grades);
    $sum=0;
    for ($i = 0; $i < $fLen; $i++) {
            $sum += $grades[$i];
     }
    return ($sum/$fLen);
}





 // Get Vo2Max value
         //Predicted VO2max and PWC
  public function get_VO2max($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange)
     {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
     /*  $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
      $mass = $_SESSION['MASS']; */
	  
	  $age = $age;
      $subpopulation = $user_subpopulation;
      $age_range = $agerange;
      $gender = $user_gender;
      $mass = $mass;
	  
      if($gender=='M')
      {
       //HRmax = (NORM.INV(RAND(),209-0.69*AGE, 10) [no decimal places]
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $x=$object1->NORMINV($probability,209-0.69*$age,10);
      $HRmax=round($x);  
      }
      if($gender=='F')
      {
       //HRmax = (NORM.INV(RAND(),209-0.69*AGE, 10) [no decimal places]
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $x=$object1->NORMINV($probability,205-0.75*$age,10);
      $HRmax=round($x);  
      }
    if($HRmax < 135)
    {
     $HRmax=135;   
    }
    if($HRmax > 220)
    {
     $HRmax=220;   
    }  
    
    //PREDICTED Resting HR [HRrest]
    //HRrest = (NORM.INV(RAND(),0.2*AGE+55, 2) [no decimal places]
      $prob_hrrest=$this->frand(0,1);
      $prob_hrrest=round($prob_hrrest,9); 
      $HRrest=$object1->NORMINV($prob_hrrest,0.2*$age+55,2); 
    if($subpopulation=='Athlete')
    {
      $prob_hrrest=$this->frand(0,1);
      $prob_hrrest=round($prob_hrrest,9); 
      $HRrest=$object1->NORMINV($prob_hrrest,0.2*$age+50,2);     
    }
   
    $HRrest=round($HRrest);
   
// Predicted HRmax can be entered in to the field on the screen in the ‘Known HRmax [BPM] field 
    //Calculate the HR reserve [HRreserve] :
    //HRreserve = HRmax – HRrest  
    $HRreserve=$HRmax - $HRrest;
    //Calculate the HR for each of the three stages on the screen :
    //STAGE 1 HR = (NORM.INV(RAND(),0.5, 0.015)* HRreserve + HRrest    [no decimal places]
      $prob_stage1=$this->frand(0,1);
      $prob_stage1=round($prob_stage1,9); 
      $stage1HR=$object1->NORMINV($prob_stage1,0.5,0.015) * $HRreserve + $HRrest; 
      $stage1HR=round($stage1HR);
    //STAGE 2 HR = (NORM.INV(RAND(),0.65, 0.015)* HRreserve + HRrest [no decimal places]
      $prob_stage2=$this->frand(0,1);
      $prob_stage2=round($prob_stage2,9); 
      $stage2HR=$object1->NORMINV($prob_stage2,0.65, 0.015) * $HRreserve + $HRrest; 
      $stage2HR=round($stage2HR);
      //STAGE 3 HR = (NORM.INV(RAND(),0.8, 0.015)* HRreserve + HRrest [no decimal places]
      $prob_stage3=$this->frand(0,1);
      $prob_stage3=round($prob_stage3,9); 
      $stage3HR=$object1->NORMINV($prob_stage3,0.8, 0.015) * $HRreserve + $HRrest; 
      $stage3HR=round($stage3HR);
      //If STAGE 2 HR – STAGE 1 HR < 8 then STAGE 2 HR = STAGE 1 HR + 8
      if(($stage2HR-$stage1HR) < 8)
      {
      $stage2HR=$stage1HR +8;    
      }  
    //If STAGE 3 HR – STAGE 2 HR < 8 then STAGE 3 HR = STAGE 2 HR + 8
      if(($stage3HR-$stage2HR) < 8)
      {
      $stage3HR=$stage2HR +8;    
      }  
      //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Sedentary')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 7.5)*34/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,7.5) * 34 / 38.3;     
      if($VO2max < 10)
      {
       $VO2max=10;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
       //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='General')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 8) )*37/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,8) * 37 / 38.3;     
      if($VO2max < 12)
      {
       $VO2max=12;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
        //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Active')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 7) )*39/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,7) * 39 / 38.3;     
      if($VO2max < 15)
      {
       $VO2max=15;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
         //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Athlete')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 56, 5) )*45/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+56,5) * 45 / 38.3;     
      if($VO2max < 40)
      {
       $VO2max=40;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
     
       //PREDICTED VO2MAX Female
      if($gender == 'F' && $subpopulation=='Sedentary')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 42, 7.5)*27/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+42,7.5) * 27/31.1;     
      if($VO2max < 10)
      {
       $VO2max=10;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
       //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='General')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 44, 8) )*30/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+44,8) * 30/31.1;     
      if($VO2max < 12)
      {
       $VO2max=12;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
        //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='Active')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 44, 7) )*32/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+44,7) *32/31.1;     
      if($VO2max < 15)
      {
       $VO2max=15;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
         //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='Athlete')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 46, 5) )*38/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+46,5) * 38 / 31.1;     
      if($VO2max < 32)
      {
       $VO2max=32;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
      
     //PREDICTED VO2rest  
      if($gender=='M')
      {
      //VO2rest = 0.005*MASS + 0.1	[ in litres per minute]
       $VO2rest= 0.005 * $mass + 0.1;  
      }
      if($gender=='F')
      {
      //VO2rest = 0.005*MASS 	[ in litres per minute]
       $VO2rest= 0.005 * $mass;  
      }
      //NOW generate a regression equation predicting VO2 from HR
      //SLOPEVO2 = (HRmax – HRrest) / (VO2maxLM – VO2rest) (changed according to new document.)
      $SLOPEVO2= ($VO2maxLM - $VO2rest)/($HRmax - $HRrest);
	  $Intercept=$VO2rest-($SLOPEVO2 * $HRrest);	
	//The equation = VO2 = SLOPEVO2 * HR + VO2rest
      $stage1VO2 =$SLOPEVO2 * $stage1HR + $Intercept;
      $stage2VO2 =$SLOPEVO2 * $stage2HR + $Intercept;
      $stage3VO2 =$SLOPEVO2 * $stage3HR + $Intercept;
      //Converting VO2 to Watts :
      //WATTS = (VO2 – VO2rest) / (NORM.INV(RAND(),0.01197,0.001))  [this is a random adjustment for differences in mechanical efficiency]
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
	  $useforwatt=$object1->NORMINV($prob_watt,0.01197,0.001);
      $stage1WATT=($stage1VO2 - $VO2rest)/ $useforwatt;
        //Stage 2 WATT
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
      $stage2WATT=($stage2VO2 - $VO2rest)/$useforwatt;
      //Stage 3 WATT
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
      $stage3WATT=($stage3VO2 - $VO2rest)/$useforwatt;
      
      //PREDICTED RPE at rest [RPErest]
      //RPEgradient = (NORM.INV(RAND(),0.2,0.007)) 
      $prob_RPE=$this->frand(0,1);
      $prob_RPE=round($prob_RPE,9); 
      $RPEgradient=$object1->NORMINV($prob_RPE,0.2,0.007);
      
    //RPEintercept = (NORM.INV(RAND(),0,0.5)) 
       $prob_intercept=$this->frand(0,1);
      $prob_intercept=round($prob_intercept,9); 
      $RPEintercept=$object1->NORMINV($prob_intercept,0,0.5);
      //RPE [at each stage] = RPEgradient * [100 - (((HRmax-STAGE 1 HR)/(HRmax – HRrest))*100)] + RPEintercept  [round to nearest 0.5 RPE unit]
     // [example 1: STAGE 1 HR = 110 BPM and HRmax = 185 BPM and HRrest = 60 BMP. 
   //Therefore,  RPE at this stage = 100 – [(185 – 110) / (185 – 60) * 100] + RPEintercept  
  //= 100 – (75/125 *100) = 100 - 60  = 40% [HRreserve]. This means RPE = 0.2  * 40 = 8. [assume RPEgradient = 0.2 and RPEintercept = 0]  
      $stage1RPE=$RPEgradient*(100 - (($HRmax - $stage1HR)/($HRmax-$HRrest))*100)+$RPEintercept; 
      if($stage1RPE <=8)
      {
       $stage1RPE=8;   
      }
      if($stage1RPE >20)
      {
       $stage1RPE=20;   
      }
       //Stage 2 RPE
      $stage2RPE=$RPEgradient*(100 - (($HRmax - $stage2HR)/($HRmax-$HRrest))*100)+$RPEintercept;
    if($stage2RPE <=8)
      {
       $stage2RPE=8;   
      }
      if($stage2RPE >20)
      {
       $stage2RPE=20;   
      }  
    //Stage 3 RPE
      $stage3RPE=$RPEgradient*(100 - (($HRmax - $stage3HR)/($HRmax-$HRrest))*100)+$RPEintercept;
      if($stage3RPE <=8)
      {
       $stage3RPE=8;   
      }
      if($stage3RPE >20)
      {
       $stage3RPE=20;   
      }  
	  
	  
	//$vo2max_norms = array('vo2max'=>round($VO2max,1) , 'vo2maxLM'=>round($VO2maxLM,2) , 'vo2rest' => round($VO2rest,2) );
  
  
       $vo2max_norms = array('vo2max'=>round($VO2max,1) , 'vo2maxLM'=>round($VO2maxLM,2) , 'vo2rest' => round($VO2rest,2) ,
						  'stage1HR'=>$stage1HR , 'stage2HR'=>$stage2HR , 'stage3HR' => $stage3HR ,						  
						  'stage1WATT'=>round($stage1WATT) , 'stage2WATT'=>round($stage2WATT) , 'stage3WATT' => round($stage3WATT) ,
						  'HRmax'=>$HRmax,
						  'stage1RPE'=>round($stage1RPE) , 'stage2RPE'=>round($stage2RPE) , 'stage3RPE' => round($stage3RPE) 
	                      );
						  
	return $vo2max_norms;
	  
	  /* 
      $_SESSION['HRmax']=$HRmax;
      $_SESSION['stage1HR']=$stage1HR;
      $_SESSION['stage2HR']=$stage2HR;
      $_SESSION['stage3HR']=$stage3HR;
      $_SESSION['VO2max']=round($VO2max,1);
	  $_SESSION['VO2maxLM']=round($VO2maxLM,2);
	   $_SESSION['VO2rest']=round($VO2rest,2);
      $_SESSION['HRrest']= round($HRrest);
      $_SESSION['stage1WATT']=round($stage1WATT);
      $_SESSION['stage2WATT']=round($stage2WATT);
      $_SESSION['stage3WATT']=round($stage3WATT);
      $_SESSION['stage1RPE']= round($stage1RPE);
      $_SESSION['stage2RPE']= round($stage2RPE);
      $_SESSION['stage3RPE']= round($stage3RPE);
	  $_SESSION['useforwatt']=$useforwatt; 
       */
      //return;
  } 


   public function calculate_PWC($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$pwc_vo2max,$pwc_stage1HR,$pwc_stage2HR,$pwc_stage3HR,$pwc_stage1WATT,$pwc_stage2WATT,$pwc_stage3WATT,$pwc_HRmax,$pwc_stage1RPE,$pwc_stage2RPE,$pwc_stage3RPE)
  {

	  $age=$age;
	  $weight=$mass;
	  
	  // Workload $iables
	  $x1 = $pwc_stage1WATT;
      $x2 = $pwc_stage2WATT;
      $x3 = $pwc_stage3WATT;
	  
	  // HR values   
      $y1 = $pwc_stage1HR;
      $y2 = $pwc_stage2HR;
      $y3 = $pwc_stage3HR;
	  
	  $x1y1 = $x1 * $y1 ; //product
      $x2y2 = $x2 * $y2 ; //product
      $x3y3 = $x3 * $y3 ; //product
        
      $sumxy = $x1y1 + $x2y2 + $x3y3 ; // sum of products
      $sumx = $x1 + $x2 + $x3 ; // sum of x
      $sumy = $y1 + $y2 + $y3 ; // sum of y

      $xsquared = ($x1 * $x1) + ($x2 * $x2) +($x3 * $x3) ;
      $sumxsquared = ($sumx * $sumx)  ;
      
      $slope = (3 * $sumxy - ($sumx * $sumy)) / (3 * $xsquared - $sumxsquared) ;
      $intercept = ($sumy - ($slope * $sumx)) / 3  ; 
	  
	  //var y = document.getElementById("max_hr").value ;  
	  $y = (208 - (0.7 * $age)) ;        

	  $x = ($y - $intercept) / $slope ;
	  $x = floor($x * 10) /10;
  
      //document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
	  $max_wl_regheartrate = $x; 
			
			
	  $code=$max_wl_regheartrate;
	  $vo1  =  (($x / 9.81) * 60) * 2 + (3.5 * $weight) ;
	  $predicted_VO  =  $vo1 / $weight ;
	  $predicted_VO_regheartrate  =  $predicted_VO   ;
	  	
     

      $PWC_norms = array('max_wl_regheartrate'=>round($max_wl_regheartrate) , 
	                     'predicted_VO_regheartrate'=>round($predicted_VO_regheartrate) 
						);
  
      return $PWC_norms;   
      
  }
  
  
  
   public function calculate_PWC_RPE($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$pwc_vo2max,$pwc_stage1WATT,$pwc_stage2WATT,$pwc_stage3WATT,$pwc_HRmax,$pwc_stage1RPE,$pwc_stage2RPE,$pwc_stage3RPE)
  {

     
     $weight =$mass;
	 
	 $y1 = $pwc_stage1RPE ;
     $y2 = $pwc_stage2RPE ;
     $y3 = $pwc_stage3RPE;
     $x1 = $pwc_stage1WATT;
     $x2 = $pwc_stage2WATT;
     $x3 = $pwc_stage3WATT;
	 
	  $x1y1 = $x1 * $y1 ; //product
      $x2y2 = $x2 * $y2 ; //product
      $x3y3 = $x3 * $y3 ; //product
        
      $sumxy = $x1y1 + $x2y2 + $x3y3 ; // sum of products
      $sumx = $x1 + $x2 + $x3 ; // sum of x
      $sumy = $y1 + $y2 + $y3 ; // sum of y

      $xsquared = ($x1 * $x1) + ($x2 * $x2) +($x3 * $x3) ;
      $sumxsquared = ($sumx * $sumx)  ;
      
      $slope = (3 * $sumxy - ($sumx * $sumy)) / (3 * $xsquared - $sumxsquared) ;
      $intercept = ($sumy - ($slope * $sumx)) / 3  ; 
	  
	  
	  $pwc_maxRPE=20;
	  $y = $pwc_maxRPE ;  

	  $x = ($y - $intercept) / $slope ;
	  $x = floor($x * 10) /10;
 
	  $max_wl_regrperate = $x; 
			
			
	  
	  $vo1  =  (($x / 9.81) * 60) * 2 + (3.5 * $weight) ;
	  $predicted_VO  =  $vo1 / $weight ;
	  $predicted_VO_regrperate  =  $predicted_VO   ;

      
      $PWC_norms = array('Max_Power_RPE'=>round($max_wl_regrperate) , 
	                     'VO2max_RPE'=>round($predicted_VO_regrperate) 
						 
	                    );
  
      return $PWC_norms;   
      
  }
  
  
  
  
  
  
  
  
  


// BIKE TESTS
  public function calculate_biketest($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$biketest_vo2max,$biketest_vo2maxLM,$biketest_vo2rest)
  {
	   // need mass, age , speed
	   $vo2max = $biketest_vo2max;
	   $vo2maxlm=$biketest_vo2maxLM;
	   $vo2rest=$biketest_vo2rest;
	   $vp_age=$age;
	   $weight=$mass;
	   
	   
	   // Calculate Adjustment using vo2max
	   $object1 = new PHPExcel_Calculation_Statistical(); 
	   $prob_biketest=$this->frand(0,1);
       $prob_biketest=round($prob_biketest,9);
	    //(=NORM.INV(RAND(),0,0.004*VO2max)	
       $bikeres=$object1->NORMINV($prob_biketest,0,0.004*$vo2max);
	   $vo2_adj=$bikeres;  //Adjustment
	   
       $vo2max_final = $vo2max + $vo2_adj;
 
         // Treadmill Continous
         $TREADMILLSPEEDcont = -0.0021 * pow($vo2max_final,2) + (0.4875 * $vo2max_final) - 3.9025 ;
		 $TREADMILLSPEEDcont = round($TREADMILLSPEEDcont,1);               // treadmill speed		
         $spd = $TREADMILLSPEEDcont;
		 $vo2_adj= 15.754 - 2.3080 * $spd + 0.084821 * ( ($spd) * ($spd) );
		 $vo2max_treadmill= ( 1.5323 + 2.8453 * ($spd) + 0.019298 * pow($spd,2) + $vo2_adj );
		 $vo2max_treadmill=round($vo2max_treadmill,1);
		 
		 $cont_vo2max_treadmill =  $vo2max_treadmill;             // Continous Treadmill VO2max
		 
		 // Treadmill Discontinous 
		 $TREADMILLSPEEDdistcont = 0.00124 * pow($vo2max_final,2)  + (0.20892 * $vo2max_final) + 2.06706 ;
	 	 $TREADMILLSPEEDdistcont = round($TREADMILLSPEEDdistcont,1);
		 $spd_disct = $TREADMILLSPEEDdistcont;
		 //$vo2_adj= parseFloat( 15.754 - 2.3080 * parseFloat(spd) + 0.084821 * ( parseFloat(spd) * parseFloat(spd) ) );
		 $vo2max_distreadmill= 1.5323 + 2.8453 * ($spd_disct) + 0.019298 *  pow($spd_disct,2) - ($vo2_adj) ;
		 $vo2max_distreadmill=round($vo2max_distreadmill,1); 
		 
         $discont_vo2max_distreadmill = $vo2max_distreadmill;  // Discontinous Treadmill Vo2max
		 
		   
		   
		   
		//Ergometer  Algorithm 
		
		$predicted_watts = ( ($vo2maxlm - $vo2rest) / 0.012 );
		if($user_gender == 'M' || $user_gender == 'Male')
				 {
				  // Males FINAL Max Watts = Predicted WATTS * [(100+(0.1234*AGE - 4.072)) / 100]		  
				  $maxwatts = ($predicted_watts) * (100 + (0.1234 * $vp_age - 4.072 ))/100 ;       // Adjustment workload
				  $maxwatts = round($maxwatts,0);  
				  $worload  =  $maxwatts;   // Here acts as a workload
				  $VO2max_ergo= ((( $workload * 10.51 ) + ( ($weight) * 6.35 ) - ( $vp_age * 10.49 ) + 519.3 ) / ($weight)) ;
				 }
				 
				 if($user_gender == 'F' || $user_gender == 'Female')
				 {
				  // Females FINAL Max Watts = Predicted WATTS * [(100+(0.1045*AGE + 2.4167)) / 100]  
				  $maxwatts = ( ($predicted_watts) *  (100 + (0.1045 * $vp_age + 2.4167)) / 100 );   // Adjustment workload
				  $maxwatts = round($maxwatts,0);  
				  $worload  =  $maxwatts;   // Here acts as a workload
				  
				  $VO2max_ergo= (( ( ($workload)* 9.39 ) + ( ($weight) * 7.7 ) - ( $vp_age * 5.88 ) + 136.7 ) / ($weight));
				 }
				 
		// Erogometer VO2max
         $ergo_vo2max  = 	$VO2max_ergo;
		   
		 $vo2max_biketest = array('Continous_vo2max_treadmill'=>round($cont_vo2max_treadmill,1) , 'Discontinous_vo2max_distreadmill'=>round($discont_vo2max_distreadmill,2) , 'Ergometer_Continous_vo2max' => round($ergo_vo2max,2) );  
         return $vo2max_biketest;  
		   
  }


//      //30s Total Work [kJ]
  public function total_work($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange)
     {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $age;
      $subpopulation = $user_subpopulation;
      $age_range = $agerange;
      $gender = $user_gender;
	  $weight =$mass;
	  
	  
    $fitness_array=$this->fitness_module_norms($gender,$age_range);
    $MeanTW=$fitness_array['MeanTW'];
    $SDTW=$fitness_array['SDTW'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanTW =$MeanTW - (1.5 *  $SDTW);   
    $SDTW= 0.65 *  $SDTW;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanTW = $MeanTW - (0.5 * $SDTW);   
    $SDTW= 0.75 * $SDTW;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanTW = $MeanTW + (0.5 * $SDTW);   
    $SDTW= 0.75 * $SDTW;
  }  
  
//For Athlete
  if($subpopulation=='Athlete')
  {
   $MeanTW = $MeanTW + (1.5 * $SDTW);   
    $SDTW= 0.5 *$SDTW;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanTW,$SDTW);
      $randomprob=round($x,9);   
      $TW =$randomprob;
      
    //If TW < -0.00121*(Age) + 0.13765 then TW = -0.00121*(Age) + 0.13765
      if($gender=='M' && ($TW < (-0.00121 * ($age) + 0.13765 )))
      {
      $TW = -0.00121 * ($age) + 0.13765; 
      }
      //If TW > -0.00451*(Age) + 0.54384 then TW = -0.00451*(Age) + 0.54384
      if($gender=='M' && ($TW > (-0.00451 * ($age) + 0.54384)))
      {
      $TW = -0.00451 * ($age) + 0.54384 ;  
      }
//      FOR FEMALE
//    If TW < -0.00099*(Age) + 0.11769 then TW = -0.00099*(Age) + 0.11769  
      if($gender=='F' && ($TW < (-0.00099 * ($age) + 0.11769)))
      {
      $TW = -0.00099 * ($age) + 0.11769;  
      }
    // If TW > -0.00388*(Age) + 0.48017 then TW = -0.00388*(Age) + 0.48017
      if($gender=='F' && ($TW > (-0.00388 *($age) +  0.48017)))
      {
      $TW = -0.00388 *($age) +  0.48017 ;  
      }
      
     $TW=round($TW,4);
    // $_SESSION['TW']=$TW;
    // $weight=$_SESSION['MASS'];
     $valuekj=$TW*$weight;
    // $_SESSION['valuekj']=round($valuekj,1);
 
     $total_work = array('total_work_done_in30s_kJ'=>round($valuekj,1));  
     return $total_work;  
  
  }
  
  
 // Peak Power

 //Flight : Contact time ratio
public function peak_power_test($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange)
{
     // $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $age;
      $subpopulation = $user_subpopulation;
      $age_range = $agerange;
      $gender = $user_gender;
    $fitness_array=$this->fitness_module_norms($gender,$age_range);
    $MeanPPWKg=$fitness_array['MeanPPWKg'];
    $SDPPWKg=$fitness_array['SDPPWKg'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanPPWKg =$MeanPPWKg - (1.5 *  $SDPPWKg);   
    $SDFTCT= 0.65 *  $SDPPWKg;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanPPWKg = $MeanPPWKg - (0.5 * $SDPPWKg);   
    $SDPPWKg= 0.75 * $SDPPWKg;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanPPWKg = $MeanPPWKg + (0.5 * $SDPPWKg);   
    $SDFTCT= 0.75 * $SDPPWKg;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
   $MeanPPWKg = $MeanPPWKg + (1.5 * $SDPPWKg);   
    $SDPPWKg= 0.5 *$SDPPWKg;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanPPWKg,$SDPPWKg);
      $randomprob=round($x,9);   
      $PPWKg =$randomprob;
      
    //If FTCT < -0.0138*(Age) + 1.62 then FTCT = -0.0138*(Age) + 1.62
      if($gender=='M' && ($PPWKg < (-0.0763 * ($age) + 9.3 )))
      {
      $PPWKg = -0.0763 * ($age) + 9.3 ;  
      }
      //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
      if($gender=='M' && ($PPWKg > (-0.2425 * ($age) + 31)))
      {
      $PPWKg = -0.2425 * ($age) + 31 ;  
      }
//      FOR FEMALE
//      
      if($gender=='F' && ($PPWKg < (-0.0577 * ($age) + 6.6)))
      {
      $PPWKg = -0.0577 * ($age) + 6.6 ;  
      }
    // If PPWKg  > -0.1994*(Age) + 24.2 then PPWKg = -0.1994*(Age) + 24.2 
    if($gender=='F' && ($PPWKg > (-0.1994 *($age) +  24.2)))
      {
      $PPWKg = -0.1994 *($age) +  24.2 ;  
      }
      
      $PPWKg=round($PPWKg,2);
  //    $_SESSION['PPWKg']=$PPWKg;
    //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_pickpower($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($PPWKg - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    //$weight=$_SESSION['MASS'];
    $weight=$mass;
    $pickpower_vo2Max = $PPWKg / $weight ;
    $pickpower_vo2Max=round($pickpower_vo2Max,2);
            
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    $watts_val=$PPWKg * $weight;
    /* $_SESSION['pickpower_percent']=$perform_percent;
    $_SESSION['pickpower_z_score']=$z_score;
    $_SESSION['pickpower_vo2Max']=$pickpower_vo2Max;
	$_SESSION['watts_val']=round($watts_val,1);  */
	
	
	
	$vo2max_peakpower = array('watts_val'=>round($watts_val,1) , 'PPWKg'=>round($PPWKg,2) );  
    return $vo2max_peakpower;  	
  
  }
  
 function getzscore_pickpower($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 15.86 ;
            $sd = 1.59 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 14.38 ;
            $sd = 1.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 13.24 ;
           $sd = 1.05 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 11.65  ;
           $sd = 0.86 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 10.03 ;
           $sd = 0.70 ;
        
      }else if($age_range == "70+") {
        
           $mean =8.53 ;
           $sd = 0.58 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 13.89 ;
            $sd = 1.39 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 12.65 ;
           $sd = 1.10 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 11.45 ;
           $sd =  0.90;
        
      }else if($age_range == "50-59") {
        
           $mean = 10.00 ;
           $sd = 0.74;
        
      }else if($age_range == "60-69") {
        
           $mean =8.47 ;
           $sd = 0.59 ;
        
      }else if($age_range == "70+") {
        
           $mean = 7.34 ;
           $sd = 0.50 ;        
      }                  
   }
      
   $meansd_pickpower=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_pickpower;
      }  
  
  
  
   // 20 Shuttle test
  // 20 M shuttle TEST
 public function twenty_shuttle_test($age,$mass,$height,$bmi,$user_gender,$user_subpopulation,$agerange,$shuttletest_vo2max)
  {
      //$setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $age;
      $subpopulation = $user_subpopulation;
      $age_range = $agerange;
      $gender = $user_gender;
      $mass = $mass;    
      $VO2max = $shuttletest_vo2max;
 $lap=(0.01664 * pow($VO2max,2)) + (1.68560 * $VO2max)  - 32.36922;
	 $lap=round($lap);
      if($lap > 171)
     {
     $lap = 171;    
     }   
   if($VO2max >= 20)
    {
     $shuttle_level=$this->get_shuttle_level($lap);  
    }
    else
    {
	  $shuttle_level=""; 	
		
     			
      $prob_meterwalked=$this->frand(0,1);
      $prob_meterwalked=round($prob_meterwalked,9); 
      $walked_meter=($VO2max - 4.948) / 0.023 * ($prob_meterwalked * (1.1-0.9)+0.9);
      }
	
	
	$vo2max_twenty_test = array( 
	                      'shuttle_level'=>$shuttle_level , 
						  'shuttle_VO2max'=>round($VO2max,2) 
						  );  
   
  
   return $vo2max_twenty_test;  
	
  
  } 
  
  
  
  	// Get Shuttle Level
  function get_shuttle_level($lap)
  {
        $qry = $this->db->select('shuttle_level')->from('shuttle_level')->where('lap', $lap)->get();
        $res = $qry->result();
        if (count($res) > 0) {
            $shuttle_level = $res[0]->shuttle_level;
            return $shuttle_level;
        } else {
            $shuttle_level = "";
            return $shuttle_level;
        }
  }    
  

  
  
/* function fitness_module_norms($gender,$age_range)
 { 
		   if($gender=='M' && $age_range=='18-29')
		   {
			  $qry="SELECT * FROM fitness_norm where age='18-29' and gender='M'";
		   }
           if($gender=='M' && $age_range=='30-39')
		   {
			   $qry="SELECT * FROM fitness_norm where age='30-39' and gender='M'";
		   }
		   if($gender=='M' && $age_range=='40-49')
			{
				$qry="SELECT * FROM fitness_norm where age='40-49' and gender='M'";
			}
          if($gender=='M' && $age_range=='50-59')
			{
			   $qry="SELECT * FROM fitness_norm where age='50-59' and gender='M'";
		    }
		  if($gender=='M' && $age_range=='60-69')
			{
			   $qry="SELECT * FROM fitness_norm where age='60-69' and gender='M'";
		    }
		  if($gender=='M' && $age_range=='70+')
			{
			   $qry="SELECT * FROM fitness_norm where age='70+' and gender='M'";
		    }
		   
		   
		   
		  
		    if($gender=='F' && $age_range=='18-29')
		   {
			  $qry="SELECT * FROM fitness_norm where age='18-29' and gender='F'";
		   }
           if($gender=='F' && $age_range=='30-39')
		   {
			   $qry="SELECT * FROM fitness_norm where age='30-39' and gender='F'";
		   }
		   if($gender=='F' && $age_range=='40-49')
			{
				$qry="SELECT * FROM fitness_norm where age='40-49' and gender='F'";
			}
          if($gender=='F' && $age_range=='50-59')
			{
			   $qry="SELECT * FROM fitness_norm where age='50-59' and gender='F'";
		    }
		  if($gender=='F' && $age_range=='60-69')
			{
			   $qry="SELECT * FROM fitness_norm where age='60-69' and gender='F'";
		    }
		  if($gender=='F' && $age_range=='70+')
			{
			   $qry="SELECT * FROM fitness_norm where age='70+' and gender='F'";
		    }
		   
		   
		  // echo $qry;
		   
		   
         $query = $this->db->query($qry); 
		 if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $MeanVJ=$result['MeanVJ'];   
       $SDVJ=$result['SDVJ'];   
       $MeanFTCT=$result['MeanFTCT'];   
       $SDFTCT=$result['SDFTCT'];   
       $MeanPPWKg=$result['MeanPPWKg'];   
       $SDPPWKg=$result['SDPPWKg'];   
       $MeanTW=$result['MeanTW'];   
       $SDTW=$result['SDTW'];   
       $endurance_VO2max=$result['endurance_VO2max'];   
       $SD_endurance_VO2max=$result['SD_endurance_VO2max'];   
       $Lactate_threshold_w=$result['Lactate_threshold_w'];   
       $SD_Lactate_threshold_w=$result['SD_Lactate_threshold_w'];   
       $Lactate_threshold_km=$result['Lactate_threshold_km'];   
       $SD_Lactate_threshold_km=$result['SD_Lactate_threshold_km'];   
       $MeanGS=$result['MeanGS'];   
       $SDGS=$result['SDGS'];   
   
   $fitness_norms =array('MeanVJ'=>$MeanVJ,
    'SDVJ'=>$SDVJ,
    'MeanFTCT'=>$MeanFTCT,
    'SDFTCT'=>$SDFTCT,
    'MeanPPWKg'=>$MeanPPWKg,
    'SDPPWKg'=>$SDPPWKg,
    'MeanTW'=>$MeanTW,
    'SDTW'=>$SDTW,        
    'endurance_VO2max'=>$endurance_VO2max,
    'SD_endurance_VO2max'=>$SD_endurance_VO2max,
    'Lactate_threshold_w'=>$Lactate_threshold_w,
    'SD_Lactate_threshold_w'=>$SD_Lactate_threshold_w,  
    'Lactate_threshold_km'=>$Lactate_threshold_km,
    'SD_Lactate_threshold_km'=>$SD_Lactate_threshold_km,
    'MeanGS' =>$MeanGS,
    'SDGS' =>$SDGS);
  
   //  print_r($fitness_norms); die;
  
   return $fitness_norms;
   
   }
 */




   // MBJ Sites FOR VP Generator
public function mbjsites_data()
{
     //MBJ Code
              $medi_data=array();
              $MBJ_site=array();
              $MBJPain=rand(1,10);
 
              if($MBJPain <= 3){
                  $option_MBJ="Y"; }
              else{
                  $option_MBJ="N"; }
              
			  //$finalarr["MBJoption"]=$option_MBJ;
			  $medi_data["MBJoption"]=$option_MBJ;
			  
              if($option_MBJ == "Y")
              {
                  $MBJ_number=rand(1,5);
                  if($MBJ_number == 1)
                  {
                      for($i=0;$i<1;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 2)
                  {
                      for($i=0;$i<2;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 3)
                  {
                      for($i=0;$i<3;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 4)
                  {
                      for($i=0;$i<4;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  elseif($MBJ_number == 5)
                  {
                      for($i=0;$i<5;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  else
                  {
                      $MBJ_site[0] = "No value exists";
                  }
              }
              else
              {
                  $MBJ_site = 0;
              }
              $medi_data["mbjsite"]=$MBJ_site;
         return $medi_data;
}
    



// Kritika Code end
    
    
    
    
        //GET PHYSICAL ACTIVITY DETAILS  
    public function fetch_physical_activity_details($age,$mass,$height,$bmi,$user_gender,$user_subpopulation)
      {
       $fieldData=array();
       $subpopulation=$user_subpopulation;
       $gender=$user_gender;
       $age=$age;
       $BMI=$bmi;
       if($subpopulation=='Sedentary')
       {
        $walk_freq='0';
        $walk_tot_min='0';
        $vig_freq='0';
        $vig_freq_tot_min='0';
        $other_mod_freq='0';
        $other_mod_tot_min='0';
       
		if($age < 60)
       {
        $SedTV = (rand(-1,7)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }
      //IF AGE > 60
       if($age >= 60)
       {
        $SedTV = (rand(0-1,11)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }  
       // Driving or being driven
        //IF AGE < 60
       if($age < 65)
       {
        $DriveTime = (rand(-1,5)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 0.5;   
       } 
      }  
          //IF AGE < 60
       if($age >= 65)
       {
        $DriveTime = (rand(-1,4)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 2;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 1;   
       } 
      } 
       
      //Job requires sitting?
        //IF AGE < 60
       if($age < 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 10)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }  
         //IF AGE < 60
       if($age >= 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 8)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }
       }
           



//GENERAL MALE
       if($subpopulation=='General' && $gender=='M')
       {
        $minoccu=-1;
        $maxoccu=4;
        $walk_freq=rand($minoccu,$maxoccu)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=6;
                $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
           
           
           
           
           
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }                
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      //FOR FEMALE 
          if($subpopulation=='General' && $gender=='F')
       {
        $minoccu=-1;
        $maxoccu=6;
        $walk_freq=rand($minoccu,$maxoccu)+1;
        $walk_duration=rand(2,5)*5;   
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='10';   
        }
        $other_mod_tot_min=$other_mod_freq * 10;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
       
      //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='M')
       {
        $walk_freq=rand(-1,10)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=10;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,4)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur=rand(2,10) * 5;       
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='25';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
       //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='F')
       {
        $walk_freq=rand(-1,12)+1;
        $walk_duration=rand(2,10)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
         $vigdur=rand(2,10) * 5;       
      
         $vig_freq_tot_min=$vig_freq * 10;
        
        $other_mod_freq=rand(-1,2)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='20';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
        //Athlete SUB WITH MALE 
      if($subpopulation=='Athlete')
       {
        $walk_freq=rand(1,5)+1;
        $walk_duration=rand(2,60)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(3,7)+1 ;
         $vigdur=rand(4,12) * 5;       
      
         $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,12) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
      
      } 
     
      $fieldData[0]->option_1=$walk_freq;
      $fieldData[0]->option_2=$walk_tot_min;
      $fieldData[0]->option_3=$vig_freq;
      $fieldData[0]->option_4=$vig_freq_tot_min;
      $fieldData[0]->option_5=$other_mod_freq;
      $fieldData[0]->option_6=$other_mod_tot_min;
      $fieldData[0]->option_7=$SedTV;
      $fieldData[0]->option_8=$DriveTime;
      $fieldData[0]->option_9=$jobsitopt;
      //$fieldData[0]->total_PA=$total_PA;
	  
	  $fieldData[0]->total_PA=$total_PA;
      $fieldData[0]->vig_total=$vig_freq_tot_min;
      
     // $resultPA=array('total_PA'=>$total_PA,'walk_tot_min'=>$walk_tot_min,'vig_freq_tot_min'=>$vig_freq_tot_min,'other_mod_tot_min'=>$other_mod_tot_min,'walk_freq'=>$walk_freq,'vig_freq'=>$vig_freq,'other_mod_freq'=>$other_mod_freq,'SedTV'=>$SedTV,'DriveTime'=>$DriveTime,'jobsitopt'=>$jobsitopt); 
          
    // print_r($fieldData); die;
          
          
     return ($fieldData);
     }
    
       
// Kritika Code    
     
    //Get the Random Value In Pre-exercise Form
        public function fetch_preexercise_details($age,$mass,$height,$bmi,$gender,$subpopulation)
      {
         $fieldData=array();
        $fieldData[0] = new StdClass;
        $vp_age=$age;
        $MASS=$mass;
        $BMI=$bmi;
        $HEIGHT=$height;
        $sub_population=$subpopulation;
        $gender=$gender;
              //HEART QUESTION
        //calculate Heart Probability
      //FOR MALE HEART QUES
       if($gender=='M')
       {
        $HEARTPROB=1+(0.00000161*pow($vp_age,3.7892425)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';
        
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       }  
       
//FOR FEMALE HEART QUES
       if($gender=='F')
       {
        $HEARTPROB=1+(0.0000113*pow($vp_age,3.1941547)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';    
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       
      }
          
//END HEART QUESTION
  
      //FOR MALE CHEST PAIN
       if($gender=='M')
       {
        $CHESTPROB=1+(0.00105*pow($vp_age,2)+0.08262*$vp_age-3.15); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       }   
      
//FOR FEMALE CHEST PAIN QUES
       if($gender=='F')
       {
        $CHESTPROB=1+(0.00223*pow($vp_age,2)-0.06514*($vp_age)+0.56); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       } 
      // FEEL FAINT QUES
         //FOR MALE FAINT
       if($gender=='M')
       {
        $FAINTPROB=(0.00208*pow($vp_age,2)-0.09524*($vp_age)+1.7); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       }
        
       
         //FOR FEMALE FAINT
       if($gender=='F')
       {
        $FAINTPROB=(0.00262*pow($vp_age,2)-0.10952*($vp_age)+2.3); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       } 
       //ASTHMA ATTACK QUES
       
         //FOR MALE ASTHMA ATTACK
       if($gender=='M')
       {
        $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
         //FOR FEMALE ASTHMA ATTACK
       if($gender=='F')
       {
           $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
        
          //FOR MALE DIABETES
       if($gender=='M')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(5 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=3;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
         //FOR FEMALE DIABETES
       if($gender=='F')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(2 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=2;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
          //FOR MALE BONE OR MUSCLE PROBLEM
       if($gender=='M' || $gender=='F')
       {
         $BONEPROB =0.002321* pow($vp_age,2) - 0.152143*($vp_age)+ 2.8; 
        /*Random No*/
        $min=1;
        $max=100;
        $bone=rand($min,$max);
       
        // 1 Question yes no
        if($bone<=$BONEPROB)
        {
        $fieldData[0]->option_6='Y';    
        }
        else
        {
         $fieldData[0]->option_6='N';      
        }
       } 
     
    //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=45 && $gender=='M')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
      
       //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=55 && $gender=='F')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
     
      if(!isset($fieldData[0]->option_7))
      {
       $fieldData[0]->option_7='N';  
      }
       return $fieldData;      
    
       
         }//end of pre exercising Option
      
    //medication_condition
     public function medication_condition($medical_history,$gender)
     {
        
         if(count($medical_history)>0)
        {
    
    $medical_conditions=array();
    $medication=array();
    $HEART=($medical_history[0]->option_1 === 'Y')?"Y":"N";
      
//For Heart Condition
        if($HEART=='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $heartcondition=rand($min,$max);  
            if($heartcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($heartcondition >= 8)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $heartmeds=rand($min,$max);  
        if($heartmeds <= 8)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($heartmeds <= 4)
        {
         $medication[]='cardiovascular';  
        }
        if($heartmeds >= 6)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($heartmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='musculo-skeletal';   
            break;
         case 5:
        $medication[]='psychiatric';   
            break;
         case 6:
        $medication[]='renal medication';   
            break; 
        case 7:
        $medication[]='respiratory';   
            break;
         case 8:
        $medication[]='other';   
            break;
        }
          
        }
        $CHESTPAIN =($medical_history[0]->option_2 === 'Y')?"Y":"N";
        
 
       //For CHESTPAIN Condition
        if($CHESTPAIN =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $chestcondition=rand($min,$max);  
            if($chestcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($chestcondition >= 9)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $chestmeds=rand($min,$max);  
        if($chestmeds <= 9)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($chestmeds <= 6)
        {
         $medication[]='cardiovascular';  
        }
        if($chestmeds >= 3)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($chestmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='other';   
            break;
        
        }
          
        }
       
      $FAINT  =($medical_history[0]->option_3 === 'Y')?"Y":"N";
         
//For FAINT Condition
        if($FAINT =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $faintcondition=rand($min,$max);  
            if($faintcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        switch($faintcondition)
        {
        case 7:  
        $medical_conditions[]='thyroid disease'; 
        break;    
        case 8:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 9:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 10:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        }
        $min_other=1;
        $max_other=10;
        $othercondition=rand($min_other,$max_other);  
        if($othercondition>=7)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $faintmeds=rand($min,$max);  
        if($faintmeds <= 3)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($faintmeds <= 5)
        {
         $medication[]='cardiovascular';  
        }
        if($faintmeds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($faintmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        
        $ASTHMA  =($medical_history[0]->option_4 === 'Y')?"Y":"N";

       //For ASTHMA Condition
        if($ASTHMA =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $asthmacondition=rand($min,$max);  
            if($asthmacondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($asthmacondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='cerebrovascular';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($asthmacondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $asthmameds=rand($min,$max);  
        if($asthmameds >= 2)
        {
         $medication[]='respiratory';  
        }
        if($asthmameds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($asthmameds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($asthmameds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        //For DIABETES Condition
         $DIABETES   =($medical_history[0]->option_5 === 'Y')?"Y":"N";
        if($DIABETES =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $diabetescondition=rand($min,$max);  
            if($diabetescondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($diabetescondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='arthritis or osteoporosis';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($diabetescondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $diabetesmeds=rand($min,$max);  
        if($diabetesmeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($diabetesmeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($diabetesmeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($diabetesmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
        
        }
        if($diabetesmeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        }
       
         //For BONE Condition
         $BONE =($medical_history[0]->option_6 === 'Y')?"Y":"N";
        if($BONE =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $bonecondition=rand($min,$max);  
            if($bonecondition <= 8)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($bonecondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='diabetes';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($bonecondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $bonemeds=rand($min,$max);  
        if($bonemeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($bonemeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($bonemeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($bonemeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($bonemeds >= 5 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($bonemeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        
        }

        
       //For OTHERMED  Condition
         $OTHERMED =($medical_history[0]->option_7 === 'Y')?"Y":"N";
        if($OTHERMED =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $othercondition=rand($min,$max);  
            if($othercondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($othercondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='psychiatric illness';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($othercondition >= 6)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $lastmeds=rand($min,$max);  
        if($lastmeds >= 8)
        {
         $medication[]='psychiatric illness';  
        }
        if($lastmeds <= 2)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($lastmeds >= 6)
        {
         $medication[]='other';  
        }
        switch($lastmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='metabolic';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($lastmeds >= 9 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($lastmeds >= 7) 
        {
         $medication[]='psychiatric';     
        }
       }
         //##################IF ALL NO ANSWER SELECTED#######################################      
     if($HEART=='N' && $CHESTPAIN=='N' && $FAINT=='N' && $ASTHMA=='N' && $DIABETES=='N' && $BONE=='N' && $OTHERMED=='N')
     {
        
         $min=1;
        $max=100;
        $medcondition=rand($min,$max); 
       
        if($medcondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';  
          $medication[]='other';
        }
        switch($medcondition)
        {
        case 3:
        $medical_conditions[]='cardiovascular';   
        $medication[]='LIPID-lowering medication';  
            break;
         case 4:
        $medical_conditions[]='cerebrovascular';
        $medication[]='other';       
            break;
        case 5:
            $medical_conditions[]='diabetes';
            $medication[]='diabetic';   
            break;
         case 6:
             $medical_conditions[]='kidney disease';
             $medication[]='BLOOD PRESSURE-lowering medication';   
            break;
         case 7:
         $medical_conditions[]='liver or metabolic disorder';
         $medication[]='metabolic';      
            break;
         case 8:
         $medical_conditions[]='psychiatric illness';
         $medication[]='psychiatric';      
            break;
        case 9:
         $medical_conditions[]='respiratory disease';
         $medication[]='respiratory';      
            break;
         case 10:
         $medical_conditions[]='thyroid disease';
         $medication[]='metabolic';      
            break;
         }
     if($medcondition >= 98)
     {
         $medical_conditions[]='other';
         $medication[]='other';         
     }
   }
                
        $medication_selected=implode(',',array_unique($medication));
        $medical_cond_selected=implode(',',array_unique($medical_conditions));
            
            
// kritika Code

//End Of Medications code
          

              //MBJ Code

              $MBJ_site=array();
              $MBJPain=rand(1,10);
 
              if($MBJPain <= 3){
                  $option_MBJ="Y"; }
              else{
                  $option_MBJ="N"; }
              
			  $finalarr["MBJoption"]=$option_MBJ;
			  
              if($option_MBJ == "Y")
              {
                  $MBJ_number=rand(1,5);
                  if($MBJ_number == 1)
                  {
                      for($i=0;$i<1;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 2)
                  {
                      for($i=0;$i<2;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 3)
                  {
                      for($i=0;$i<3;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 4)
                  {
                      for($i=0;$i<4;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  elseif($MBJ_number == 5)
                  {
                      for($i=0;$i<5;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  else
                  {
                      $MBJ_site[0] = "No value exists";
                  }
              }
              else
              {
                  $MBJ_site = "MBJ Site NO";
              }
              $finalarr["mbjsite"]=$MBJ_site;

              $data['MBJdata'] = $finalarr;  

              //print_r($data['MBJdata']); 
                $checkedval="";
                $i=0;
                $sitearr_values = array();
                foreach($data['MBJdata'] as $key=>$val)
                {               
                    if($key == "mbjsite")
                    {
                        foreach($val as $key)
                        {
                            $sitearr_values[$i]=$key;
                            $i++;
                        }
                    }	

                    if($key == "MBJoption")
                    {
                        $checkedval=$val;
                    }

                }
            
                $sitevalue=implode(" ,", $sitearray_values);

                //print_r($sitearr_values);
                //print_r($data['MBJdata']);


/* function frand($min, $max ,$decimals = 2) {
      $scale = pow(10, $decimals);
      return mt_rand($min * $scale, $max * $scale) / $scale;
  }*/
// kritika Code    
            
          
        return(array('medication_selected'=>$medication_selected,'medical_cond_selected'=>$medical_cond_selected));
        //print_r((array('medication_selected'=>$medication_selected,'medical_cond_selected'=>$medical_cond_selected,'medicationarray'=>$data ,'sitevalues'=>$sitearr_values)));
       // return(array('medication_selected'=>$medication_selected,'medical_cond_selected'=>$medical_cond_selected,'sitevalues'=>$sitearr_values));
        //die('aaaaaaaaaaaaaaaaaaaa');
            
            
            
            
       } 
     }
 
     // VP_physical_activity
     public function VP_physical_activity($age,$mass,$height,$bmi,$gender,$subpopulation)
      {
       $fieldData=array();
       $fieldData[0] = new StdClass; 
       $subpopulation=$subpopulation;
       $gender=$gender;
       $age=$age;
       $BMI=$bmi;
       $SedTV="";
       $DriveTime="";
       $jobsitopt="";
       $total_PA="";
       if($subpopulation=='Sedentary')
       {
        $walk_freq='0';
        $walk_tot_min='0';
        $vig_freq='0';
        $vig_freq_tot_min='0';
        $other_mod_freq='0';
        $other_mod_tot_min='0';
       }
 //GENERAL MALE
       if($subpopulation=='General' && $gender=='M')
       {
        $minoccu=-1;
        $maxoccu=4;
        $walk_freq=rand($minoccu,$maxoccu)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=6;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      //FOR FEMALE 
          if($subpopulation=='General' && $gender=='F')
       {
        $minoccu=-1;
        $maxoccu=6;
        $walk_freq=rand($minoccu,$maxoccu)+1;
        $walk_duration=rand(2,5)*5;   
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='10';   
        }
        $other_mod_tot_min=$other_mod_freq * 10;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
       
      //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='M')
       {
        $walk_freq=rand(-1,10)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=10;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,4)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur=rand(2,10) * 5;       
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA < 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='25';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
       //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='F')
       {
        $walk_freq=rand(-1,12)+1;
        $walk_duration=rand(2,10)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
         $vigdur=rand(2,10) * 5;       
      
         $vig_freq_tot_min=$vig_freq * 10;
        
        $other_mod_freq=rand(-1,2)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA < 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='20';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
        //Athlete SUB WITH MALE 
      if($subpopulation=='Athlete')
       {
        $walk_freq=rand(1,5)+1;
        $walk_duration=rand(2,60)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(3,7)+1 ;
         $vigdur=rand(4,12) * 5;       
      
         $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,12) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
      
      } 
    
         if($age < 60)
       {
        $SedTV = (rand(-1,7)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }
      //IF AGE > 60
       if($age >= 60)
       {
        $SedTV = (rand(0-1,11)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }  
      
  // Driving or being driven
        //IF AGE < 60
       if($age < 65)
       {
        $DriveTime = (rand(-1,5)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 0.5;   
       } 
      }  
          //IF AGE < 60
       if($age >= 65)
       {
        $DriveTime = (rand(-1,4)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 2;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 1;   
       } 
      } 
       
      //Job requires sitting?
        //IF AGE < 60
       if($age < 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 10)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }  
         //IF AGE < 60
       if($age >= 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 8)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }
      $fieldData[0]->option_1=$walk_freq;
      $fieldData[0]->option_2=$walk_tot_min;
      $fieldData[0]->option_3=$vig_freq;
      $fieldData[0]->option_4=$vig_freq_tot_min;
      $fieldData[0]->option_5=$other_mod_freq;
      $fieldData[0]->option_6=$other_mod_tot_min;
      $fieldData[0]->option_7=$SedTV;
      $fieldData[0]->option_8=$DriveTime;
      $fieldData[0]->option_9=$jobsitopt;
      $fieldData[0]->total_PA=$total_PA;
      
     // $resultPA=array('total_PA'=>$total_PA,'walk_tot_min'=>$walk_tot_min,'vig_freq_tot_min'=>$vig_freq_tot_min,'other_mod_tot_min'=>$other_mod_tot_min,'walk_freq'=>$walk_freq,'vig_freq'=>$vig_freq,'other_mod_freq'=>$other_mod_freq,'SedTV'=>$SedTV,'DriveTime'=>$DriveTime,'jobsitopt'=>$jobsitopt); 
     return ($fieldData);
     }
    
		//GET RISK FACTOR
     public function fetch_risk_factors_details($age,$mass,$height,$bmi,$gender,$subpopulation)
     {
       $fieldData=array();
       $fieldData[0] = new StdClass; 
       $subpopulation=$subpopulation;
       $gender=$gender;
       $age=$age;
       $BMI=$bmi;   
       $age_of_ralative="";
       $family_gen="";
       $smoke_perday="";
       $total_smoke_perday="";
       $month_six_smoke="";
       $family_his_num=rand(1,10); 
       if($family_his_num=='1')
       {
       $family_his='Y';    
       }
       else
       {
       $family_his='N';    
       }
     if($family_his=='Y')
     {
      $family_gen_num=rand(1,2);    
    
      switch($family_gen_num)
      {
        case 1:
        $family_gen='M';
           break;
       case 2:
        $family_gen='F';
           break;
      }
     $age_of_ralative=rand(1,30)+40;
     }
      
//     SMOKING PROFILE 
//    [MALES] – current smoker
     if($gender=='M' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)   = -0.00004155*(Age)^4 + 0.00803066*(Age)^3 - 0.57352160*(Age)^2 + 17.52605448*(Age) - 156.13741177
        $smoke_prob_per= -0.00004155 * pow($age,4) +  0.00803066 * pow($age,3) - 0.57352160 * pow($age,2) + 17.52605448 * $age - 156.13741177;    
        }
     }
     
    //General  
     if($gender=='M' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002770*(Age)^4 + 0.00535377*(Age)^x3 - 0.38234773*(Age)^2 + 11.68403632*(Age) - 104.09160785
        $smoke_prob_per= -0.00002770 * pow($age,4) +  0.00535377 * pow($age,3) -0.38234773 * pow($age,2) + 11.68403632 * $age - 104.09160785;    
        }
     }
     //Active   
     if($gender=='M' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%) = -0.00000692*(Age)^4 + 0.00133844*(Age)^3 - 0.09558693*(Age)^2 + 2.92100908*(Age) - 26.02290196
        $smoke_prob_per= -0.00000692 * pow($age,4) +  0.00133844 * pow($age,3) - 0.09558693 * pow($age,2) + 2.92100908 * $age - 26.02290196;    
        }
     }
     // FEMALE SMOKEING PROFILE
     if($gender=='F' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //= -0.00003046*(Age)^4 + 0.00578225*(Age)^3 - 0.40274444*(Age)^2 + 11.95970651*(Age) - 102.32997609
        $smoke_prob_per= -0.00003046 * pow($age,4) +   0.00578225 * pow($age,3) - 0.40274444 * pow($age,2) +  11.95970651 * $age - 102.32997609;    
        }
     }
     
    //General  
     if($gender=='F' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002030*(Age)^4 + 0.00385484*(Age)^3 - 0.26849630*(Age)^2 + 7.97313767*(Age) - 68.21998406
        $smoke_prob_per= -0.00002030 * pow($age,4) + 0.00385484 * pow($age,3) - 0.26849630 * pow($age,2) + 7.97313767 * $age - 68.21998406;    
        }
     }
     //Active   
     if($gender=='F' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00000508*(Age)^4 + 0.00096371*(Age)^3 - 0.06712407*(Age)^2 + 1.99328442*(Age) - 17.05499601
        $smoke_prob_per= -0.00000508 * pow($age,4) + 0.00096371 * pow($age,3) - 0.06712407 * pow($age,2) + 1.99328442 * $age - 17.05499601;    
        }
     }
     
     //Male and Female smokers: 
     $random_num=rand(1,100);
     if($random_num > $smoke_prob_per)
     {
      $smoking='N';      
     }
     else
     {
       $smoking='Y';   
     }
     
     if($smoking=='Y')
     {
     $smoke_perday=rand(1,4)*5;      
     }
     
     if($subpopulation=='Athlete')
     {
      $smoking='N';  
        }
    //Have you quit smoking in the last 6 months?
     if($smoking=='N')
     {
       if($subpopulation=='Sedentary' || $subpopulation=='General')
       {
        $randomval=rand(1,15);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
     } 
     //Active
      if($subpopulation=='Active')
       {
        $randomval=rand(1,40);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
      } 
      if($subpopulation=='Athlete')
      {
      $month_six_smoke='N';  
      }
     }
   //Have you been told you have high blood pressure?
   $RNBP= rand(1,100); 
   $SRBPprob = 0.00464 * pow($age,2) - 0.26429 * ($age) + 5.06250;  
   
   If($SRBPprob >= $RNBP)
     {
        $high_bp='Y'; 
     }
     else
     {
      $high_bp='N';    
     }
     //Have you been told you have high lipids? 
     $RNLIPID=rand(1,100); 
     $SRLIPIDprob = 0.00464 * pow($age,2) - 0.26429*($age) + 5.06250;
     If($SRLIPIDprob >= $RNLIPID)
     {
        $high_cols='Y'; 
     }
     else
     {
      $high_cols='N';    
     }
     //Have you been told you have high blood sugar?
     $RNSUGAR=rand(1,100); 
     $SRSUGARprob = 0.0019 * pow($age,2) - 0.0881 * ($age) + 2.1071;
     //If SRSUGARprob < RNSUGAR then ‘YES’ else
     if($SRSUGARprob > $RNSUGAR)
     {
      $blood_sugar='Y';   
     }
    else{
      $blood_sugar='N';   
    }
 $getbloodvalue=$this->Blood_values_norms($age,$gender);    
//MEASURED BLOOD PRESSURE
    //SBP and DBP
  // Random SBP =ROUND(NORM.INV(RAND(), mean SBP, SD SBP))
 $object1 = new PHPExcel_Calculation_Statistical();
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_sbp=$getbloodvalue['mean_sbp'];
 $sd_sbp=$getbloodvalue['sd_sbp'];
 $x=$object1->NORMINV($probability,$mean_sbp,$sd_sbp);
  $randomsbp=round($x);   
  //Generate a random DBP using the normal distribution function :
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_dbp=$getbloodvalue['mean_dbp'];
 $sd_dbp=$getbloodvalue['sd_dbp'];
 $get_dbp=$object1->NORMINV($probability,$mean_dbp,$sd_dbp);
  $randomdbp=round($get_dbp);
//Adjustment 
  if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 0.94;
     $DBP_adjustment = 0.97;
    }
    else
    {
     $SBP_adjustment = 0.93;
     $DBP_adjustment = 0.96;    
    }
  }
    if($subpopulation=='Sedentary' || $subpopulation=='General')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;
    }
    else
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;    
    }
  }
  //Therefore, for all virtual subjects:
  //SBP = round(random SBP * SBP adjustment) 
  //DBP = round(random DBP * DBP adjustment) 
  $SBP=round($randomsbp * $SBP_adjustment);
  $DBP=round($randomdbp * $DBP_adjustment);
  //[adjustment for overweight /obese people]   
  if($BMI >= 35)
  {
   $SBP = $SBP + 9;
   $DBP = $DBP + 6;
  }
   if($BMI >= 30 && $BMI<35)
  {
   $SBP = $SBP + 5;
   $DBP = $DBP + 4;
  }
  if($BMI >= 28 && $BMI<30)
  {
   $SBP = $SBP + 2;
   $DBP = $DBP + 2;
  }
  //[check to ensure no unrealistic lower values]  
  if($gender=='M' && $SBP<90)
  {
   $SBP =90;   
  }
   //[check to ensure no unrealistic lower values]  
  if($gender=='F' && $SBP < 85)
  {
   $SBP =85;   
  }
   if($gender=='M' && $DBP < 52)
  {
   $DBP =52;   
  }
   if($gender=='F' && $DBP < 50)
  {
   $DBP =50;   
  }
 // [check to prevent to SBP and DBP from being unrealistically close by this algorithm]:
  //if SBP-DBP < 22 then SBP = DBP+22 
  if(($SBP - $DBP) < 22)
  {
   $SBP = $DBP + 22;   
  }
 
  //TOTAL BLOOD CHOLESTEROL [CHOL]
  $bloodprob=$this->frand(0,1);
  $bloodprob=round($bloodprob,9); 
  $mean_cols=$getbloodvalue['mean_cols'];
  $sd_cols=$getbloodvalue['sd_cols'];
  $cols_random=$object1->NORMINV($bloodprob,$mean_cols,$sd_cols);
  $cols_random=round($cols_random,2);   
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $cols_adjustment = 0.97;
    }
    else
    {
     $cols_adjustment = 0.96; 
    }
  $CHOL=($cols_random * $cols_adjustment);
    
    }
    else
    {
    $CHOL=$cols_random;    
    }
  //[If Male and CHOL <2.5 then CHOL = 2.5 ; If Female and CHOL <2.5 then CHOL = 2.5]
  if($CHOL < 2.5)
  {
   $CHOL = 2.5;   
  }
 //HIGH-DENSITY LIPOPROTEIN [HDL]
   $proprob=$this->frand(0,1);
  $proprob=round($proprob,9); 
  $mean_ln_hdl=$getbloodvalue['mean_ln_hdl'];
  $sd_ln_hdl=$getbloodvalue['sd_ln_hdl'];
  $pro_random=$object1->NORMINV($proprob,$mean_ln_hdl,$sd_ln_hdl);
  $pro_random=round($pro_random,2);   
  $random_hdl=exp($pro_random);
  $random_hdl=round($pro_random,2);
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $hdl_adjustment = 1.15;
    }
    else
    {
     $hdl_adjustment = 1.05; 
    }
  $HDL=($random_hdl * $hdl_adjustment);
    
    }
    else
    {
    $HDL=$random_hdl;    
    }
  if($gender=='M' && $HDL < 0.6)
  {
   $HDL = 0.6;   
  }
  if($gender=='F' && $HDL < 0.75)
  {
   $HDL = 0.75;   
  }
 
  //LOW-DENSITY LIPOPROTEIN [LDL]
  $ldlprob=$this->frand(0,1);
  $ldlprob=round($ldlprob,9); 
  $mean_ldl=$getbloodvalue['mean_ldl'];
  $sd_ldl=$getbloodvalue['sd_ldl'];
  $ldl_random=$object1->NORMINV($ldlprob,$mean_ldl,$sd_ldl);
  $ldl_random=round($ldl_random,2);   
  $random_ldl=$ldl_random;
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $ldl_adjustment = 0.94;
    }
    else
    {
     $ldl_adjustment = 0.95; 
    }
  $LDL=($random_ldl * $ldl_adjustment);
    
    }
    else
    {
    $LDL=$random_ldl;    
    }
  
 if($gender=='M' && $LDL < 1.3)
  {
   $LDL = 1.3 ;   
  }
  if($gender=='F' && $LDL < 1.2)
  {
   $LDL = 1.2;   
  }
    
 //TRIGLYCERIDES [TRIG]  
   $triprob=$this->frand(0,1);
  $triprob=round($triprob,9); 
  $mean_ln_trig=$getbloodvalue['mean_ln_trig'];
  $sd_ln_trig=$getbloodvalue['sd_ln_trig'];
  $tri_random=$object1->NORMINV($triprob,$mean_ln_trig,$sd_ln_trig);
  $tri_random=round($tri_random,2);   
  $random_tri=exp($tri_random);
  $random_tri=round($random_tri,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $trig_adjustment = 0.92;
    }
    else
    {
     $trig_adjustment = 0.93; 
    }
  $TRIG=($random_tri * $trig_adjustment);
    
    }
    else
    {
    $TRIG=$random_tri;    
    }
  
 if($gender=='M' && $TRIG < 0.45)
  {
   $TRIG = 0.45 ;   
  }
  if($gender=='F' && $TRIG < 0.4)
  {
   $TRIG = 0.4;   
  } 
    //TRIGLYCERIDES [TRIG]   
    
  //FASTING BLOOD GLUCOSE [GLU] 
 
   $gluprob=$this->frand(0,1);
  $gluprob=round($gluprob,9); 
  $mean_ln_glu=$getbloodvalue['mean_ln_glu'];
  $sd_ln_glu=$getbloodvalue['sd_ln_glu'];
  $glu_random=$object1->NORMINV($gluprob,$mean_ln_glu,$sd_ln_glu);
  $glu_random=round($glu_random,2);   
  $random_glu=exp($glu_random);
  $random_glu=round($random_glu,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $glu_adjustment = 1.05;
    }
    else
    {
     $glu_adjustment = 1.04; 
    }
  $GLU=($random_glu * $glu_adjustment);
    
    }
    else
    {
    $GLU=$random_glu;    
    }
  
 if($gender=='M' && $GLU < 3.9)
  {
   $GLU = 3.9 ;   
  }
  if($gender=='F' && $GLU < 3.7)
  {
   $GLU = 3.7;   
  }
  //other BLOOD PARAMETER PROFILE [Hb, Hct,] [not inserted into any fields at this stage in the EST program]
  if($gender=='M')
  {
  $Hbgen =  -0.00087 * pow($age,2) + 0.04542 * ($age) + 14.32083;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.36);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
  if($gender=='F')
  {
  //Hb = -0.00000132*(Age)^4 + 0.0002767*(Age)^3 - 0.02091004*(Age)^2 + 0.66274621*(Age) + 5.7875
      $Hbgen =  -0.00000132 * pow($age,4) + 0.0002767 * pow($age,3)- 0.02091004 * pow($age,2)+ 0.66274621 * ($age) +  5.7875;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.24);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
 
  //Haematocrit [Hct] [%]
  $Hct_gen = 2.63 * $Hb + 5.83;
  $hctmprob=$this->frand(0,1);
  $hctmprob=round($hctmprob,9); 
  $hct_random=$object1->NORMINV($hctmprob,$Hct_gen,1.22);
  $Random_Male_Hct= round($hct_random,2);
  $hctfprob=$this->frand(0,1);
  $hctfprob=round($hctfprob,9); 
  $hctf_random=$object1->NORMINV($hctfprob,$Hct_gen,1.16);
  $Random_Female_Hct= round($hctf_random,2); 
   $fieldData[0]->option_1=$family_his;
  $fieldData[0]->option_gender=$family_gen;
  $fieldData[0]->option_age=$age_of_ralative;
  $fieldData[0]->option_2=$smoking;
  $fieldData[0]->option_smoke=$smoke_perday;
  $fieldData[0]->option_3=$month_six_smoke;
  $fieldData[0]->option_smoke_6=$total_smoke_perday;
  $fieldData[0]->option_4 =$high_bp;
  $fieldData[0]->option_5 =$high_cols; 
  $fieldData[0]->option_6 = $blood_sugar;
  $fieldData[0]->option_7 = $SBP;
  $fieldData[0]->option_8 = $DBP;
  $fieldData[0]->option_9 = round($HDL,1);
  $fieldData[0]->option_10 = round($LDL,1);
  $fieldData[0]->option_11 = round($CHOL,1);
  $fieldData[0]->option_12 = round($GLU,1);
  $fieldData[0]->option_13 = round($TRIG,1);
  
 return ($fieldData);
  }
     
	//PUT MASS and BMI 
       function Blood_values_norms($vp_age,$v)
       {
            if(($vp_age >=18 && $vp_age<=24) && $v=='M')
           {
      //MASS AND BMI VALUE
        $mean_sbp='118';    
        $sd_sbp='12.2';
        $mean_dbp='71';
        $sd_dbp='9.3';
        $mean_cols='4.62';
        $sd_cols='0.87'; 
        $mean_ln_hdl='0.1172';
        $sd_ln_hdl='0.219';
        $mean_ln_trig='0.12295';
        $sd_ln_trig='0.3869';
        $mean_ldl='2.9';
        $sd_ldl='0.8';
        $mean_ln_glu='1.569';
        $sd_ln_glu='0.14';
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=34) && $v=='M')
       {
        //MASS AND BMI VALUE
       $mean_sbp='120';    
        $sd_sbp='11.5';
        $mean_dbp='74';
        $sd_dbp='8.4';
        $mean_cols='5.32';
        $sd_cols='1.15'; 
        $mean_ln_hdl='0.1151';
        $sd_ln_hdl='0.2258';
        $mean_ln_trig='0.40576';
        $sd_ln_trig='0.593';
        $mean_ldl='3.4';
        $sd_ldl='0.8';
        $mean_ln_glu='1.609';
        $sd_ln_glu='0.15';
       }
       //FOR MALE 35-54
       if(($vp_age >=35 && $vp_age<=44) && $v=='M')
       {
      
//MASS AND BMI VALUE
        $mean_sbp='122';    
        $sd_sbp='12.1';
        $mean_dbp='77';
        $sd_dbp='9.8';
        $mean_cols='5.5';
        $sd_cols='1.08'; 
        $mean_ln_hdl='0.1222';
        $sd_ln_hdl='0.2194';
        $mean_ln_trig='0.48708';
        $sd_ln_trig='0.5128';
        $mean_ldl='3.5';
        $sd_ldl='0.9';
        $mean_ln_glu='1.649';
        $sd_ln_glu='0.16';
       }

//FOR MALE 35-39
       if(($vp_age >=45 && $vp_age<=54) && $v=='M')
       {
        //MASS AND BMI VALUE
        $mean_sbp='126';    
        $sd_sbp='11.8';
        $mean_dbp='80';
        $sd_dbp='9.6';
        $mean_cols='5.35';
        $sd_cols='1.09'; 
        $mean_ln_hdl='0.1092';
        $sd_ln_hdl='0.2661';
        $mean_ln_trig='0.49794';
        $sd_ln_trig='0.5421';
        $mean_ldl='3.3';
        $sd_ldl='0.9';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.17';
       }
      
       //FOR MALE 55-64
       if(($vp_age >=55 && $vp_age<=64) && $v=='M')
       {
        $mean_sbp='133';    
        $sd_sbp='14.1';
        $mean_dbp='81';
        $sd_dbp='8.8';
        $mean_cols='5.33';
        $sd_cols='0.96'; 
        $mean_ln_hdl='0.1077';
        $sd_ln_hdl='0.2319';
        $mean_ln_trig='0.39828';
        $sd_ln_trig='0.42942';
        $mean_ldl='3.1';
        $sd_ldl='0.9';
        $mean_ln_glu='1.740';
        $sd_ln_glu='0.165';
       }
 
//FOR MALE 65-74
      if(($vp_age >=65 && $vp_age<=74) && $v=='M')
       {
    
        $mean_sbp='136';    
        $sd_sbp='14.6';
        $mean_dbp='78';
        $sd_dbp='9.1';
        $mean_cols='5.14';
        $sd_cols='0.91'; 
        $mean_ln_hdl='0.1058';
        $sd_ln_hdl='0.2111';
        $mean_ln_trig='0.31785';
        $sd_ln_trig='0.42675';
        $mean_ldl='2.8';
        $sd_ldl='0.8';
        $mean_ln_glu='1.758';
        $sd_ln_glu='0.19';
       }
 
//FOR MALE 80+
       if(($vp_age >=75) && $v=='M')
       {
      $mean_sbp='138';    
        $sd_sbp='15.3';
        $mean_dbp='75';
        $sd_dbp='8.2';
        $mean_cols='5';
        $sd_cols='1'; 
        $mean_ln_hdl='0.1018';
        $sd_ln_hdl='0.2011';
        $mean_ln_trig='0.3085';
        $sd_ln_trig='0.40675';
        $mean_ldl='2.7';
        $sd_ldl='0.9';
        $mean_ln_glu='1.723';
        $sd_ln_glu='0.17';
       }
      
       
//FOR FEMALE 
        if(($vp_age >=18 && $vp_age<=24) && $v=='F')
           {
        //MASS AND BMI VALUE
        $mean_sbp='109';    
        $sd_sbp='12.4';
        $mean_dbp='72';
        $sd_dbp='8.9';
        $mean_cols='4.6';
        $sd_cols='0.92'; 
        $mean_ln_hdl='0.2781';
        $sd_ln_hdl='0.1985';
        $mean_ln_trig='-0.00734';
        $sd_ln_trig='0.34472';
        $mean_ldl='2.7';
        $sd_ldl='0.7';
        $mean_ln_glu='1.526';
        $sd_ln_glu='0.145';
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=34) && $v=='F')
       {
    //MASS AND BMI VALUE
       $mean_sbp='109';    
        $sd_sbp='12.9';
        $mean_dbp='74';
        $sd_dbp='9.4';
        $mean_cols='4.75';
        $sd_cols='0.95'; 
        $mean_ln_hdl='0.3187';
        $sd_ln_hdl='0.216';
        $mean_ln_trig='0.12792';
        $sd_ln_trig='0.35516';
        $mean_ldl='3';
        $sd_ldl='0.8';
        $mean_ln_glu='1.569';
        $sd_ln_glu='0.142';
       }
       //FOR MALE 35-54
       if(($vp_age >=35 && $vp_age<=44) && $v=='F')
       {
   
//MASS AND BMI VALUE
        $mean_sbp='114';    
        $sd_sbp='13.5';
        $mean_dbp='76';
        $sd_dbp='8.9';
        $mean_cols='5';
        $sd_cols='0.94'; 
        $mean_ln_hdl='0.2893';
        $sd_ln_hdl='0.2281';
        $mean_ln_trig='0.1318';
        $sd_ln_trig='0.51077';
        $mean_ldl='3.2';
        $sd_ldl='0.7';
        $mean_ln_glu='1.609';
        $sd_ln_glu='0.16';
       }

//FOR F 45-54
       if(($vp_age >=45 && $vp_age<=54) && $v=='F')
       {
        
//MASS AND BMI VALUE
        $mean_sbp='121';    
        $sd_sbp='14.7';
        $mean_dbp='79';
        $sd_dbp='9.2';
        $mean_cols='5.25';
        $sd_cols='1.02'; 
        $mean_ln_hdl='0.311';
        $sd_ln_hdl='0.2467';
        $mean_ln_trig='0.195';
        $sd_ln_trig='0.49851';
        $mean_ldl='3.5';
        $sd_ldl='0.9';
        $mean_ln_glu='1.629';
        $sd_ln_glu='0.157';
       }
      
       //FOR FEMALE 55-64
       if(($vp_age >=55 && $vp_age<=64) && $v=='F')
       {
        $mean_sbp='128';    
        $sd_sbp='14.3';
        $mean_dbp='80';
        $sd_dbp='9';
        $mean_cols='5.68';
        $sd_cols='1.11'; 
        $mean_ln_hdl='0.394';
        $sd_ln_hdl='0.2411';
        $mean_ln_trig='0.35695';
        $sd_ln_trig='0.47582';
        $mean_ldl='3.3';
        $sd_ldl='0.9';
        $mean_ln_glu='1.668';
        $sd_ln_glu='0.162';
       }
 
//FOR MALE 65-74
      if(($vp_age >=65 && $vp_age<=74) && $v=='F')
       {  
        $mean_sbp='136';    
        $sd_sbp='13.2';
        $mean_dbp='78';
        $sd_dbp='8.9';
        $mean_cols='5.57';
        $sd_cols='0.98'; 
        $mean_ln_hdl='0.2952';
        $sd_ln_hdl='0.245';
        $mean_ln_trig='0.38101';
        $sd_ln_trig='0.44512';
        $mean_ldl='2.9';
        $sd_ldl='0.9';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.17';
       }
 
//FOR MALE 80+
       if(($vp_age >=75) && $v=='F')
       {
        $mean_sbp='141';    
        $sd_sbp='12.6';
        $mean_dbp='76';
        $sd_dbp='9.5';
        $mean_cols='5.15';
        $sd_cols='1'; 
        $mean_ln_hdl='0.2752';
        $sd_ln_hdl='0.225';
        $mean_ln_trig='0.37101';
        $sd_ln_trig='0.4312';
        $mean_ldl='2.8';
        $sd_ldl='0.8';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.155';
       }
      
     $data=array('mean_sbp'=>$mean_sbp
             ,'sd_sbp'=>$sd_sbp
             ,'mean_dbp'=>$mean_dbp
             ,'sd_dbp'=>$sd_dbp
             ,'mean_cols'=>$mean_cols
             ,'sd_cols'=>$sd_cols
             ,'mean_ln_hdl'=>$mean_ln_hdl
             ,'sd_ln_hdl'=>$sd_ln_hdl
              ,'mean_ln_trig'=>$mean_ln_trig
              ,'sd_ln_trig'=>$sd_ln_trig
              ,'mean_ldl'=>$mean_ldl
              ,'sd_ldl'=>$sd_ldl
              ,'mean_ln_glu'=>$mean_ln_glu
              ,'sd_ln_glu'=>$sd_ln_glu
             );
      return $data;
      
       }

	//Flight : Contact time ratio
  public function get_Flight_ratio($age,$subpopulation,$gender){
      $setagerange=$this->getagerangeforvp($age);
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $age;
      $subpopulation = $subpopulation;
      $age_range = $setagerange;
      $gender =$gender;
    $fitness_array=$this->fintness_module_norms($gender,$age_range);
    $MeanFTCT=$fitness_array['MeanFTCT'];
    $SDFTCT=$fitness_array['SDFTCT'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanFTCT =$MeanFTCT - (1.5 * $SDFTCT);   
    $SDFTCT= 0.65 * $SDFTCT;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanFTCT = $MeanFTCT - (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanFTCT = $MeanFTCT + (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
   $MeanFTCT = $MeanFTCT + (1.5 * $SDFTCT);   
    $SDFTCT= 0.5 *$SDFTCT;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanFTCT,$SDFTCT);
      $randomprob=round($x,9);   
      $FTCT =$randomprob;
      
    //If FTCT < -0.0138*(Age) + 1.62 then FTCT = -0.0138*(Age) + 1.62
      if($gender=='M' && ($FTCT < (-0.0138*($age) + 1.62)))
      {
      $FTCT = -0.0138*($age) + 1.62 ;  
      }
      //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
      if($gender=='M' && ($FTCT > (-0.0403*($age) + 5.1)))
      {
      $FTCT = -0.0403*($age) + 5.1 ;  
      }
//      FOR FEMALE
//       If FTCT < -0.0094*(Age) + 1.16 then FTCT = -0.0094*(Age) + 1.16
      if($gender=='F' && ($FTCT < (-0.0094*($age) +1.16)))
      {
      $FTCT = -0.0094*($age) + 1.16 ;  
      }
    //  If FTCT > -0.0295*(Age) + 3.8107 then FTCT = -0.0295*(Age) + 3.8
    if($gender=='F' && ($FTCT > (-0.0295*($age) + 3.8107)))
      {
      $FTCT = -0.0295*($age) + 3.8 ;  
      }
      
    $FTCT=round($FTCT,2);
    //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_flight($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($FTCT - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    
    $flight_array=array('FTCT'=>$FTCT,'flight_percent'=>$perform_percent,'flight_z_score'=>$z_score);
   
    return ($flight_array);
  }
    
    
//STRENGTH TESTS
  public function strength_test($age,$mass,$height,$bmi,$gender,$subpopulation)
     {
      $setagerange=$this->getagerangeforvp($age);
    $object1 = new PHPExcel_Calculation_Statistical(); 
      $age_range = $setagerange;
      $gender = $gender;
      $mass = $mass;
      $fieldData=array();
       $fieldData[0] = new StdClass;
        $BMI=round($bmi,2);
         $strenght_array = $this->fitness_module_norms($gender,$age_range);
        $MeanGS = $strenght_array['MeanGS'];
        $SDGS = $strenght_array['SDGS'];
      if($gender=='M')
      {
          if ($subpopulation == 'Sedentary') {
            $MeanGS = $MeanGS + 0.2 * $BMI - (1.5 * $SDGS);
            $SDGS = 0.65 * $SDGS;
        }

        //For General
        if ($subpopulation == 'General') {
           $MeanGS = $MeanGS + 0.2 * $BMI - (0.5 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
        //For Active
        if ($subpopulation == 'Active') {
            $MeanGS = $MeanGS + 0.2 * $BMI + (0.3 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }

//For Active
        if ($subpopulation == 'Athlete') {
           //If sub-population = ‘Athlete’ then MeanGS = (MeanGS+0.2*BMI + (0.5* SDGS))
           //If sub-population = ‘Athlete’ then SDGS = (0.5* SDGS)
             $MeanGS = $MeanGS + 0.2 * $BMI + (0.2 * $SDGS);
            $SDGS = 0.5 * $SDGS;
        }
      }  
      //FOR FEMALE 
      if($gender=='F')
      {
        if ($subpopulation == 'Sedentary') {
            //If sub-population = ‘Sedentary’ then MeanGS = (MeanGS+0.06*BMI – (1.5* SDGS))
            //If sub-population = ‘Sedentary’ then SDGS = (0.65* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI - (1.5 * $SDGS);
            $SDGS = 0.65 * $SDGS;
        }

        //For General
        if ($subpopulation == 'General') {
           //If sub-population = ‘General’ then MeanGS = (MeanGS+0.06*BMI – (0.5* SDGS))
           //If sub-population = ‘General’ then SDGS = (0.75* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI - (0.5 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
        //For Active
        if ($subpopulation == 'Active') {
            //If sub-population = ‘Active’ then MeanGS = (MeanGS+0.06*BMI + (0.3* SDGS))
           //If sub-population = ‘Active’ then SDGS = (0.75* SDGS)
           $MeanGS = $MeanGS + 0.06 * $BMI + (0.3 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
    //For Active
        if ($subpopulation == 'Athlete') {
           //If sub-population = ‘Athlete’ then MeanGS = (MeanGS+0.06*BMI + (0.5* SDGS))
           //If sub-population = ‘Athlete’ then SDGS = (0.5* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI + (0.5 * $SDGS);
            $SDGS = 0.5 * $SDGS;
        }
      } 
        $probability = $this->frand(0, 1);
        $probability = round($probability, 9);

        $leftgsval = $object1->NORMINV($probability, $MeanGS, $SDGS);
        $GSLEFT = round($leftgsval, 1);

        
        if ($gender == 'M' && ($GSLEFT < (-0.1778 * ($age) + 29.7))) {
            $GSLEFT = -0.1778 * ($age) + 29.7;
        }
        //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
        if ($gender == 'M' && ($GSLEFT > (-0.5132 * ($age) + 101.4))) {
            $GSLEFT = -0.5132 * ($age) + 101.4;
        }
//      FOR FEMALE
//      
        if ($gender == 'F' && ($GSLEFT < (-0.0857 * ($age) + 16.7))) {
            $GSLEFT = -0.0857 * ($age) + 16.7;
        }
        // If PPWKg  > -0.1994*(Age) + 24.2 then PPWKg = -0.1994*(Age) + 24.2 
        if ($gender == 'F' && ($PPWKg > (-0.2706 * ($age) + 59.5))) {
            $GSLEFT = -0.2706 * ($age) + 59.5;
        }

        $GSLEFT = round($GSLEFT, 1);
        //RIGHT GS
        //RIGHT GS = (NORM.INV(RAND(),LEFT GS, 1.5),   [round to 1 decimal place]
        $probabilityright = $this->frand(0, 1);
        $probabilityright = round($probabilityright, 9);
        $rightgsval = $object1->NORMINV($probabilityright, $GSLEFT, 1.5);
        $GSRIGHT = round($rightgsval, 1);
        $total = $GSLEFT + $GSREGHT;
        
//Get Mead and SD for Zscore
        $get_meansd = $this->getzscore_strength($age_range, $gender);
        $mean = $get_meansd['mean'];
        $sd = $get_meansd['sd'];
       
        $grip_score = ($mean - $total) / $sd;
        $z_score = round($grip_score, 3);

        $perform_percent = $object1->NORMSDIST($z_score);
        
        $perform_percent = $perform_percent * 100;
        $perform_percent = 100 - $perform_percent;
        $perform_percent = round($perform_percent, 3);
        //BENCH PRESS
    //strength testing calculations
        if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench * (1.5-0.6) + 0.6;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(1.0-0.6)+0.6;
        } 
          if ($subpopulation == 'Athlete') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(1.5-1.2)+1.2;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench * (0.9 - 0.45)+0.45;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(0.6-0.45)+0.45;
        } 
          if ($subpopulation == 'Athlete') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(0.9-0.7)+0.7;
        } 
      }
      //[ie., ADJS:W = S : W * (((25-AGE)*0.01025)+1)]  
      if($age>=25)
        {
         $ADJSW = $SWratio * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $SWratio;  
        }
      $BPadjMAX=$mass * $ADJSW;  
      $BPadjMAX=round($BPadjMAX,1);
      $BPreps = rand(5,15);
      //(BPadjMAX)*(1.0278-(BPreps*0.0278)) 
      $BPmass =$BPadjMAX *(1.0278-($BPreps*0.0278));
      $BPmass=round($BPmass,1);
      if($BPmass<10)
      {
       $BPmass=10;   
      }
        
//Arm curl	
//ACmass = arm curl weight lifted 			
//ACreps = number of arm curl reps			
      if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl * (0.7 - 0.25) + 0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.5 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(0.7-0.55)+0.55
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.7-0.55)+0.55;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl * ( 0.5 - 0.14)+0.14;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.25-0.14)+0.14;
        } 
          if ($subpopulation == 'Athlete') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.5-0.35)+0.35;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $AC_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $AC_SW;  
        }
      $ACadjMAX=$mass * $ADJSW;  
      $ACadjMAX=round($ACadjMAX,1);
      $ACreps = rand(5,15);
      //(BPadjMAX)*(1.0278-(BPreps*0.0278)) 
      $ACmass =$ACadjMAX *(1.0278-($ACreps*0.0278));
      $ACmass=round($ACmass,1);
      if($ACmass<4)
      {
       $ACmass=4;   
      } 
      
     //LATERAL PULLDOWN
      //LPDmass = lat pulldown weight lifted		
      //LPDreps = number of lat pulldown reps	
     if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //[ie., in excel this is done using  LPDS:W = RAND( )*( 1.2 - 0.75)+0.75
            $LPD_SW =$rand_lateral * (1.2 - 0.75) + 0.75;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
           // LPDS : W = RAND( )*(0.9-0.75)+0.75
            $LPD_SW =$rand_lateral *(0.9 - 0.75) + 0.75;
        } 
          if ($subpopulation == 'Athlete') {
            //LPDS : W = RAND( )*(1.2-1.0)+1.0
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
             $LPD_SW =$rand_lateral *(1.2 - 1.0) + 1.0;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //LPDS:W = RAND( )*( 0.85 - 0.4)+0.4
            $LPD_SW =$rand_lateral * (0.85 - 0.4) + 0.4;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //RAND( )*(0.55-0.4)+0.4 
            $LPD_SW =$rand_lateral *(0.55 - 0.4) + 0.4;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //RAND( )*(0.85-0.7)+0.7
             $LPD_SW =$rand_lateral *(0.85 - 0.7) + 0.7;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LPD_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LPD_SW;  
        }
      $LPDadjMAX =$mass * $ADJSW;  
      $LPDadjMAX=round($LPDadjMAX,1);
      $LPDreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LPDmass =$LPDadjMAX *(1.0278-($LPDreps*0.0278));
      $LPDmass=round($LPDmass,1);
      if($LPDmass<8)
      {
       $LPDmass=8;   
      } 
     
      
        //LEG PRESS
       //LPmass = leg press weight lifted		
      //LPreps = number of leg press reps		
 if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //RAND( )*( 3.0 - 1.2)+1.2
            $LP_SW =$rand_legpress * ( 3.0 - 1.2) + 1.2;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
           // LPS : W = RAND( )*(1.9-1.2)+1.2
            $LP_SW =$rand_legpress *(1.9 - 1.2) + 1.2;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(3.0-2.3)+2.3
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
             $LP_SW =$rand_legpress *(3.0-2.3) + 2.3;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //LPS:W = RAND( )*( 2.7 - 0.9)+0.9
            $LP_SW =$rand_legpress * (2.7 - 0.9) + 0.9;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //RAND( )*(1.6-0.9)+0.9
            $LP_SW =$rand_legpress *(1.6-0.9) + 0.9;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legpress = $this->frand(0, 1);
            $rand_lateral = round($rand_legpress, 9);
            //RAND( )*(2.7-2.1)+2.1
             $LP_SW =$rand_legpress *(2.7 - 2.1) + 2.1;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LP_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LP_SW;  
        }
      $LPadjMAX  =$mass * $ADJSW;  
      $LPadjMAX=round($LPadjMAX,1);
      $LPreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LPmass =$LPadjMAX *(1.0278-($LPreps*0.0278));
      $LPmass=round($LPmass,1);
      if($LPmass < 15)
      {
       $LPmass=15;   
      } 
       
      
      //LEG EXTENSION
      //LEmass  = leg extension weight lifted
      //LEreps = number of leg extension reps
      if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //RAND( )*( 0.8 – 0.35)+0.35
            $LE_SW =$rand_legext *(0.8 - 0.35 ) + 0.35;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
           // LES : W = RAND( )*(0.5-0.35)+0.35
            $LE_SW =$rand_legext *(0.5 - 0.35) + 0.35;
        } 
          if ($subpopulation == 'Athlete') {
            //LES : W = RAND( )*(0.8-0.6)+0.6
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            $LE_SW =$rand_legext *(0.8 - 0.6) + 0.6;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES:W = RAND( )*( 0.7 - 0.25)+0.25
            $LE_SW =$rand_legext * ( 0.7 - 0.25) + 0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES : W = RAND( )*(0.45-0.25)+0.25
            $LE_SW =$rand_legext *(0.45 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES : W = RAND( )*(0.7-0.5)+0.5
             $LE_SW =$rand_legext * (0.7 - 0.5) + 0.5;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LE_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LE_SW;  
        }
      $LEadjMAX   =$mass * $ADJSW;  
      $LEadjMAX=round($LEadjMAX,1);
      $LEreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LEmass =$LEadjMAX *(1.0278-($LEreps*0.0278));
      $LEmass=round($LEmass,1);
      if($LEmass < 8)
      {
       $LEmass=8;   
      } 
     

    //LEG CURL
     //LCmass = leg curl weight lifted 
     //LCreps = number of leg curl reps 
         if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS:W = RAND( )*( 0.7 – 0.25)+0.25
            $LC_SW =$rand_legcurl *( 0.7 - 0.25)+0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
           // RAND( )*(0.45-0.25)+0.25
            $LC_SW =$rand_legcurl *(0.45 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(0.7-0.5)+0.5
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            $LC_SW =$rand_legcurl *(0.7 - 0.5) + 0.5;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //RAND( )*( 0.6 - 0.15)+0.15
            $LC_SW =$rand_legcurl * ( 0.6 - 0.15) + 0.15;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS : W = RAND( )*(0.35-0.15)+0.15
            $LC_SW =$rand_legcurl *(0.35 - 0.15) + 0.15;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS : W = RAND( )*(0.6-0.4)+0.4
             $LC_SW =$rand_legcurl * (0.6 - 0.4) + 0.4;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LC_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LC_SW;  
        }
      $LCadjMAX   =$mass * $ADJSW;  
      $LCadjMAX=round($LCadjMAX,1);
      $LCreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LCmass =$LCadjMAX *(1.0278-($LCreps*0.0278));
      $LCmass=round($LCmass,1);
      if($LCmass < 4)
      {
       $LCmass=4;   
      } 
     
$strength_array=array('GSLEFT'=>$GSLEFT,
        'GSRIGHT'=>$GSRIGHT,
        'avgGrip'=>$total,
        'gripRank'=>$perform_percent,
        'strength_z_score'=>$z_score,
        'BPmass'=>$BPmass,
        'BPreps'=>$BPreps,
        'SWratio'=>$SWratio,
        'ACmass'=>$ACmass,
        'ACreps'=>$ACreps,
        'LPDmass'=>$LPDmass,
        'LPDreps'=>$LPDreps,
        'LPmass'=>$LPmass,
        'LPreps'=>$LPreps,
        'LEmass'=>$LEmass,
        'LEreps'=>$LEreps,
        'LCmass'=>$LCmass,
        'LCreps'=>$LCreps
    );
    return($strength_array);
      
      }
     
    //Fitness Module Norms Data  
function fitness_module_norms($gender,$age_range)
 { 
     if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
    $MeanFTCT='2.48';
    $SDFTCT='0.471';
    $MeanPPWKg='14.86';
    $SDPPWKg='2.59';
    $MeanTW='0.21605';
    $SDTW='0.04';        
    $endurance_VO2max='44.7';
    $SD_endurance_VO2max='8.9';
    $Lactate_threshold_w='148';
    $SD_Lactate_threshold_w='28';  
    $Lactate_threshold_km='10.9';
    $SD_Lactate_threshold_km='1.8';
    $MeanGS ='46.6';
    $SDGS ='8.3';        
   }
   if($gender=='M' && $age_range=='30-39')
   {
    
    $MeanVJ='45.6';
    $SDVJ='7.91';
    $MeanFTCT='2.36';
    $SDFTCT='0.424';
    $MeanPPWKg='13.38';
    $SDPPWKg='2.25';
    $MeanTW='0.19375';
    $SDTW='0.036425';        
    $endurance_VO2max='39.4';
    $SD_endurance_VO2max='8.55';
    $Lactate_threshold_w='133';
    $SD_Lactate_threshold_w='25';  
    $Lactate_threshold_km='9.7';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='49.8';
    $SDGS ='8.5';        
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
    $MeanFTCT='2.17';
    $SDFTCT='0.325';
    $MeanPPWKg='12.24';
    $SDPPWKg='2.05';
    $MeanTW='0.17648';
    $SDTW='0.032648042';        
    $endurance_VO2max='37.3';
    $SD_endurance_VO2max='8.54';
    $Lactate_threshold_w='117';
    $SD_Lactate_threshold_w='22';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='48.5';
    $SDGS ='8.2';        
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
    $MeanFTCT='1.89';
    $SDFTCT='0.283';
    $MeanPPWKg='11.15';
    $SDPPWKg='1.86';
    $MeanTW='0.15353';
    $SDTW='0.027943195';        
    $endurance_VO2max='32.95';
    $SD_endurance_VO2max='8.57';
    $Lactate_threshold_w='100';
    $SD_Lactate_threshold_w='19';  
    $Lactate_threshold_km='8.3';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='46.25';
    $SDGS ='7.2';        
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
    $MeanFTCT='1.60';
    $SDFTCT='0.240';
    $MeanPPWKg='10.03';
    $SDPPWKg='1.70';
    $MeanTW='0.13210';
    $SDTW='0.02404137';        
    $endurance_VO2max='30.6';
    $SD_endurance_VO2max='8.33';
    $Lactate_threshold_w='82';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='40.3';
    $SDGS ='7.1';        
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
    $MeanFTCT='1.33';
    $SDFTCT='0.213';
    $MeanPPWKg='8.13';
    $SDPPWKg='1.58';
    $MeanTW='0.11103';
    $SDTW='0.020206772';        
    $endurance_VO2max='29.5';
    $SD_endurance_VO2max='8.31';
    $Lactate_threshold_w='65';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='35.5';
    $SDGS ='7';        
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
    $MeanFTCT='1.90';
    $SDFTCT='0.361';
    $MeanPPWKg='10.69';
    $SDPPWKg='2.09';
    $MeanTW='0.19683';
    $SDTW='0.034';        
    $endurance_VO2max='36';
    $SD_endurance_VO2max='10.5';
    $Lactate_threshold_w='96';
    $SD_Lactate_threshold_w='18';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='29.1';
    $SDGS ='4.95';        
   }
   if($gender=='F' && $age_range=='30-39')
   {
    
    $MeanVJ='33.1';
    $SDVJ='5.77';
    $MeanFTCT='1.81';
    $SDFTCT='0.325';
    $MeanPPWKg='9.85';
    $SDPPWKg='1.80';
    $MeanTW='0.17742';
    $SDTW='0.030160615';        
    $endurance_VO2max='31.6';
    $SD_endurance_VO2max='9.92';
    $Lactate_threshold_w='85';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='8.6';
    $SD_Lactate_threshold_km='1.5';
    $MeanGS ='29.96';
    $SDGS ='5.25';        
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
    $MeanFTCT='1.66';
    $SDFTCT='0.266';
    $MeanPPWKg='9.05';
    $SDPPWKg='1.60';
    $MeanTW='0.15967';
    $SDTW='0.025547815';        
    $endurance_VO2max='30.5';
    $SD_endurance_VO2max='10.1';
    $Lactate_threshold_w='73';
    $SD_Lactate_threshold_w='14';  
    $Lactate_threshold_km='8.2';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='29.5';
    $SDGS ='4.9';        
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
    $MeanFTCT='1.44';
    $SDFTCT='0.217';
    $MeanPPWKg='8.00';
    $SDPPWKg='1.54';
    $MeanTW='0.14109';
    $SDTW='0.023984758';        
    $endurance_VO2max='29.6';
    $SD_endurance_VO2max='9.88';
    $Lactate_threshold_w='61';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='27.45';
    $SDGS ='5.05';        
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
    $MeanFTCT='1.23';
    $SDFTCT='0.190';
    $MeanPPWKg='6.27';
    $SDPPWKg='1.59';
    $MeanTW='0.11992';
    $SDTW='0.020387044';        
    $endurance_VO2max='25.7';
    $SD_endurance_VO2max='9.79';
    $Lactate_threshold_w='49';
    $SD_Lactate_threshold_w='9';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='25.4';
    $SDGS ='4.56';        
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
    $MeanFTCT='1.02';
    $SDFTCT='0.163';
    $MeanPPWKg='5.14';
    $SDPPWKg='1.50';
    $MeanTW='0.10151';
    $SDTW='0.017256647';        
    $endurance_VO2max='23.1';
    $SD_endurance_VO2max='8.7';
    $Lactate_threshold_w='38';
    $SD_Lactate_threshold_w='7';  
    $Lactate_threshold_km='7.4';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='22.3';
    $SDGS ='5.2';        
   }
   
   $fitness_norms =array('MeanVJ'=>$MeanVJ,
    'SDVJ'=>$SDVJ,
    'MeanFTCT'=>$MeanFTCT,
    'SDFTCT'=>$SDFTCT,
    'MeanPPWKg'=>$MeanPPWKg,
    'SDPPWKg'=>$SDPPWKg,
    'MeanTW'=>$MeanTW,
    'SDTW'=>$SDTW,        
    'endurance_VO2max'=>$endurance_VO2max,
    'SD_endurance_VO2max'=>$SD_endurance_VO2max,
    'Lactate_threshold_w'=>$Lactate_threshold_w,
    'SD_Lactate_threshold_w'=>$SD_Lactate_threshold_w,  
    'Lactate_threshold_km'=>$Lactate_threshold_km,
    'SD_Lactate_threshold_km'=>$SD_Lactate_threshold_km,
    'MeanGS' =>$MeanGS,
    'SDGS' =>$SDGS);
  
   return $fitness_norms;
   
   } 
        
        
	//Get zscore Mean and SD
  function getzscore_strength($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.6 ;
            $sd = 8.3 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 49.8 ;
            $sd = 8.5 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 48.5 ;
           $sd = 8.2 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 46.25  ;
           $sd = 7.2 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 40.3 ;
           $sd = 7.1 ;
        
      }else if($age_range == "70+") {
        
           $mean =35.5 ;
           $sd = 7 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 29.1 ;
            $sd = 4.95 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.96 ;
           $sd = 5.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 29.5 ;
           $sd =  4.9;
        
      }else if($age_range == "50-59") {
        
           $mean = 27.45 ;
           $sd = 5.05;
        
      }else if($age_range == "60-69") {
        
           $mean =25.4 ;
           $sd = 4.56 ;
        
      }else if($age_range == "70+") {
        
           $mean = 22.3;
           $sd = 5.2;        
      }                  
   }
      
   $meansd_strength=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_strength;
      }


	//Fitness Module Norms Data  
 function fintness_module_norms($gender,$age_range)
 { 
     if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
    $MeanFTCT='2.48';
    $SDFTCT='0.471';
    $MeanPPWKg='14.86';
    $SDPPWKg='2.59';
    $MeanTW='0.21605';
    $SDTW='0.04';        
    $endurance_VO2max='44.7';
    $SD_endurance_VO2max='8.9';
    $Lactate_threshold_w='148';
    $SD_Lactate_threshold_w='28';  
    $Lactate_threshold_km='10.9';
    $SD_Lactate_threshold_km='1.8';
    $MeanGS ='46.6';
    $SDGS ='8.3';        
   }
   if($gender=='M' && $age_range=='30-39')
   {
    
    $MeanVJ='45.6';
    $SDVJ='7.91';
    $MeanFTCT='2.36';
    $SDFTCT='0.424';
    $MeanPPWKg='13.38';
    $SDPPWKg='2.25';
    $MeanTW='0.19375';
    $SDTW='0.036425';        
    $endurance_VO2max='39.4';
    $SD_endurance_VO2max='8.55';
    $Lactate_threshold_w='133';
    $SD_Lactate_threshold_w='25';  
    $Lactate_threshold_km='9.7';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='49.8';
    $SDGS ='8.5';        
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
    $MeanFTCT='2.17';
    $SDFTCT='0.325';
    $MeanPPWKg='12.24';
    $SDPPWKg='2.05';
    $MeanTW='0.17648';
    $SDTW='0.032648042';        
    $endurance_VO2max='37.3';
    $SD_endurance_VO2max='8.54';
    $Lactate_threshold_w='117';
    $SD_Lactate_threshold_w='22';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='48.5';
    $SDGS ='8.2';        
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
    $MeanFTCT='1.89';
    $SDFTCT='0.283';
    $MeanPPWKg='11.15';
    $SDPPWKg='1.86';
    $MeanTW='0.15353';
    $SDTW='0.027943195';        
    $endurance_VO2max='32.95';
    $SD_endurance_VO2max='8.57';
    $Lactate_threshold_w='100';
    $SD_Lactate_threshold_w='19';  
    $Lactate_threshold_km='8.3';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='46.25';
    $SDGS ='7.2';        
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
    $MeanFTCT='1.60';
    $SDFTCT='0.240';
    $MeanPPWKg='10.03';
    $SDPPWKg='1.70';
    $MeanTW='0.13210';
    $SDTW='0.02404137';        
    $endurance_VO2max='30.6';
    $SD_endurance_VO2max='8.33';
    $Lactate_threshold_w='82';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='40.3';
    $SDGS ='7.1';        
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
    $MeanFTCT='1.33';
    $SDFTCT='0.213';
    $MeanPPWKg='8.13';
    $SDPPWKg='1.58';
    $MeanTW='0.11103';
    $SDTW='0.020206772';        
    $endurance_VO2max='29.5';
    $SD_endurance_VO2max='8.31';
    $Lactate_threshold_w='65';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='35.5';
    $SDGS ='7';        
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
    $MeanFTCT='1.90';
    $SDFTCT='0.361';
    $MeanPPWKg='10.69';
    $SDPPWKg='2.09';
    $MeanTW='0.19683';
    $SDTW='0.034';        
    $endurance_VO2max='36';
    $SD_endurance_VO2max='10.5';
    $Lactate_threshold_w='96';
    $SD_Lactate_threshold_w='18';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='29.1';
    $SDGS ='4.95';        
   }
   if($gender=='F' && $age_range=='30-39')
   {
    
    $MeanVJ='33.1';
    $SDVJ='5.77';
    $MeanFTCT='1.81';
    $SDFTCT='0.325';
    $MeanPPWKg='9.85';
    $SDPPWKg='1.80';
    $MeanTW='0.17742';
    $SDTW='0.030160615';        
    $endurance_VO2max='31.6';
    $SD_endurance_VO2max='9.92';
    $Lactate_threshold_w='85';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='8.6';
    $SD_Lactate_threshold_km='1.5';
    $MeanGS ='29.96';
    $SDGS ='5.25';        
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
    $MeanFTCT='1.66';
    $SDFTCT='0.266';
    $MeanPPWKg='9.05';
    $SDPPWKg='1.60';
    $MeanTW='0.15967';
    $SDTW='0.025547815';        
    $endurance_VO2max='30.5';
    $SD_endurance_VO2max='10.1';
    $Lactate_threshold_w='73';
    $SD_Lactate_threshold_w='14';  
    $Lactate_threshold_km='8.2';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='29.5';
    $SDGS ='4.9';        
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
    $MeanFTCT='1.44';
    $SDFTCT='0.217';
    $MeanPPWKg='8.00';
    $SDPPWKg='1.54';
    $MeanTW='0.14109';
    $SDTW='0.023984758';        
    $endurance_VO2max='29.6';
    $SD_endurance_VO2max='9.88';
    $Lactate_threshold_w='61';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='27.45';
    $SDGS ='5.05';        
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
    $MeanFTCT='1.23';
    $SDFTCT='0.190';
    $MeanPPWKg='6.27';
    $SDPPWKg='1.59';
    $MeanTW='0.11992';
    $SDTW='0.020387044';        
    $endurance_VO2max='25.7';
    $SD_endurance_VO2max='9.79';
    $Lactate_threshold_w='49';
    $SD_Lactate_threshold_w='9';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='25.4';
    $SDGS ='4.56';        
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
    $MeanFTCT='1.02';
    $SDFTCT='0.163';
    $MeanPPWKg='5.14';
    $SDPPWKg='1.50';
    $MeanTW='0.10151';
    $SDTW='0.017256647';        
    $endurance_VO2max='23.1';
    $SD_endurance_VO2max='8.7';
    $Lactate_threshold_w='38';
    $SD_Lactate_threshold_w='7';  
    $Lactate_threshold_km='7.4';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='22.3';
    $SDGS ='5.2';        
   }
   
   $fitness_norms =array('MeanVJ'=>$MeanVJ,
    'SDVJ'=>$SDVJ,
    'MeanFTCT'=>$MeanFTCT,
    'SDFTCT'=>$SDFTCT,
    'MeanPPWKg'=>$MeanPPWKg,
    'SDPPWKg'=>$SDPPWKg,
    'MeanTW'=>$MeanTW,
    'SDTW'=>$SDTW,        
    'endurance_VO2max'=>$endurance_VO2max,
    'SD_endurance_VO2max'=>$SD_endurance_VO2max,
    'Lactate_threshold_w'=>$Lactate_threshold_w,
    'SD_Lactate_threshold_w'=>$SD_Lactate_threshold_w,  
    'Lactate_threshold_km'=>$Lactate_threshold_km,
    'SD_Lactate_threshold_km'=>$SD_Lactate_threshold_km,
    'MeanGS' =>$MeanGS,
    'SDGS' =>$SDGS);
  
   return $fitness_norms;
   
   }
         
//Get zscore Mean and SD
  function getzscore_flight($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 2.28 ;
            $sd = 0.319 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 2.17 ;
            $sd = 0.303 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.99 ;
           $sd = 0.279 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.73 ;
           $sd = 0.243 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 1.47 ;
           $sd = 0.221 ;
        
      }else if($age_range == "70+") {
        
           $mean =1.22 ;
           $sd = 0.196 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 1.90 ;
            $sd = 0.266 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 1.81 ;
           $sd = 0.253 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.66 ;
           $sd =  0.241 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.44 ;
           $sd = 0.217 ;
        
      }else if($age_range == "60-69") {
        
           $mean =1.23 ;
           $sd = 0.190 ;
        
      }else if($age_range == "70+") {
        
           $mean = 1.02 ;
           $sd = 0.163 ;        
      }                  
   }
      
   $meansd_flight=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_flight;
      }



//GET AGE RANGE
  public function getagerangeforvp($age)
  { 
      if($age >= 18 && $age <=29)
			 {
				 $age_range = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $age_range = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				$age_range = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				 $age_range = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				$age_range = '60-69' ;
			 }
			 elseif($age >=70)
			 {
				 $age_range = '70+' ;
			 }
  
                         return $age_range;
                         }
    
     

 //Get zscore Mean and SD
  function getzscore_meansd($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.0 ;
            $sd = 8.50 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 42.8 ;
            $sd = 8.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 36.1 ;
           $sd = 7.35 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 31.0 ;
           $sd = 6.84 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 26.9 ;
           $sd = 6.36 ;
        
      }else if($age_range == "70+") {
        
           $mean = 23.6 ;
           $sd = 5.91 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 33.5 ;
            $sd = 6.20 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.1 ;
           $sd = 5.77 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 25.4 ;
           $sd = 5.36 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 22.1 ;
           $sd = 4.99 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 19.2 ;
           $sd = 4.64 ;
        
      }else if($age_range == "70+") {
        
           $mean = 16.7 ;
           $sd = 4.31 ;        
      }                  
   }
      
   $meansd_array=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_array;
      }
   
//GET VERTICAL JUMP
  public function get_Vertical_jump($age,$subpopulation,$gender){
      $setagerange=$this->getagerangeforvp($age);
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age =$age;
      $subpopulation = $subpopulation;
        $age_range = $setagerange;
        $gender = $gender;
    if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
   }
   if($gender=='M' && $age_range=='30-39')
   {
    $MeanVJ='45.6';
    $SDVJ='7.91';
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
   }
   if($gender=='F' && $age_range=='30-39')
   {
    $MeanVJ='33.1';
    $SDVJ='5.77';
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
   } 
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
   //For Subpopulation
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
  //For General
  if($subpopulation=='General')
  {
      $MeanVJ = $MeanVJ - (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanVJ = $MeanVJ + (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
    $MeanVJ = $MeanVJ + (1.5 * $SDVJ);   
    $SDVJ= 0.5 * $SDVJ;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanVJ,$SDVJ);
      $randomprob=round($x,9);   
      $vj=$randomprob;
      //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_meansd($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($vj - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    $vj=round($vj,1);
    $vj = floor($vj*2)/2;
   
    $arrayvj=array('vj'=>$vj,'perform_percent'=>$perform_percent,'z_score'=>$z_score);

  return ($arrayvj);
  }


//GET AGE   
     function getAge( $dob,$tdate)
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
     
    
    //GET RANDOM VAULES 
    function generateRandomValues($gender="",$population="")
    {
       
       //FOR MALE FEMALE 
       if($gender=="")
       {   
       $genderarray = array( '1'  => 'Male','2'  => 'Female');  
       $k = array_rand($genderarray);
       $v = $genderarray[$k]; 
       $selectedgender=array('gendervalue'=>$k,'gendertext'=>$v);
        //FOR MALE FEMALE
       }
    else{
        if($gender=='M')
        {
        $k='1';    
       $v="Male";
        }
        if($gender=='F')
        {
        $k='2';    
        $v="Female";
        }
        }
       
       if($k=='1')
       {
       $columnname="boys_fname";    
       }
      if($k=='2')
       {
        $columnname="girls_fname";   
       }
       
       //GENERATE RANDOM NAMES 
       $randomnamesQry="SELECT $columnname as firstname FROM random_names where $columnname!='' ORDER BY RAND() LIMIT 1";
       $query = $this->db->query($randomnamesQry);
       if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $firstname=$result['firstname'];
       $randomlastQry="SELECT last_name FROM random_names where last_name!='' ORDER BY RAND() LIMIT 1";
       $queryrandom = $this->db->query($randomlastQry);
       if($queryrandom->num_rows() > 0) {
         $resultlastname=$queryrandom->row_array();
        }
       $lastname=$resultlastname['last_name'];
       $selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
     
       //END GENERATE RANDOM NAMES 
       
       //RANDOM DATE OF BIRTH GENERATION
        
       $dates = array(
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
       
                            $month = array(
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
       
       
       $years = array('1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927');		
       /*Day Selection*/
       $daykey = array_rand($dates);
       $valuedate = $dates[$daykey]; 
       $selecteddates=array('datekey'=>$daykey,'datevalue'=>$valuedate);
        /*Month Selection*/
       $monthkey = array_rand($month);
       $valuemonth = $month[$monthkey]; 
       $selectedmonth=array('monthkey'=>$monthkey,'monthvalue'=>$valuemonth);
        /*Month Selection*/
        
       /*Year Selection*/
       $yearskey = array_rand($years);
       $valueyears = $years[$yearskey]; 
       $selectedyears=array('yearskey'=>$yearskey,'yearsvalue'=>$valueyears);
        /*Year Selection*/
       
       /*COUNTRY PROFILE*/
        $min=1;
        $max=100;
        $randomcountryval=rand($min,$max);
        if($randomcountryval<90)
        {
         $selectedyears=array('countrykey'=>'AU','countryvalue'=>'Australia');   
        }
       else
       {
        $randomctryQry="SELECT country_code,country_name FROM country ORDER BY RAND() LIMIT 1";
       $queryrandomctry = $this->db->query($randomctryQry);
       if($queryrandomctry->num_rows() > 0) {
         $resultctry=$queryrandomctry->row_array();
        }    
       $selectedyears=array('countrykey'=>$resultctry['country_code'],'countryvalue'=>$resultctry['country_name']);    
       } 
      /*END COUNTRY PROFILE*/
      
		$minoccu=1;
        $maxoccu=30;
        $randomoccuval=rand($minoccu,$maxoccu);
       if(strtolower($population) == 'athlete')   // Condition for athlete
	   {
		   $occupation="athlete/coach";
	   }
	   else{	   
			   $occupationQry="SELECT `occupation` FROM occupation where id=$randomoccuval";
			   $queryrandomoccu = $this->db->query($occupationQry);
			   if($queryrandomoccu->num_rows() > 0) {
				 $resultoccupation=$queryrandomoccu->row_array();
				}    
			   $selectedoccupation=array('occupationkey'=>$resultoccupation['occupation'],'occupationval'=>$resultoccupation['occupation']);
			   $occupation=$resultoccupation['occupation']; 	   
	   }
       
	   /*END OCCUPATION PROFILE*/ 
      
	   if(strtolower($population) == 'athlete')  // Condition for athlete
	   {
		$today_date=date('Y-m-d');
		$start = strtotime("$today_date -40 year");
		$end= strtotime("$today_date -18 year");
		$int= mt_rand($start,$end);
		$between_date=date('Y-m-d',$int);
		$get_date_values=explode("-",$between_date)	;
		$valueyears=$get_date_values[0];
		$valuemonth=$get_date_values[1];
		$valuedate=$get_date_values[2];
		$dateofBirth = $valueyears.'-'.$valuemonth.'-'.$valuedate;
			$datebrth= date("Y-m-d",strtotime($dateofBirth));
			$dob = strtotime($datebrth);
		    $tdate = strtotime(date('Y-m-d'));
		    $vp_age=$this->getAge($dob, $tdate);
		    $agerange=$this->getAgeRange($vp_age);	
	   }
	   
	   else{
		 
		   $dateofBirth=$valueyears.'-'.$valuemonth.'-'.$valuedate;
		   $datebrth= date("Y-m-d",strtotime($dateofBirth));
		   $dob = strtotime($datebrth);
		   $tdate = strtotime(date('Y-m-d'));
		   $vp_age=$this->getAge($dob, $tdate);
		   $agerange=$this->getAgeRange($vp_age);
	   }
	  
	   if($vp_age>70)
       {
        $occupation='retired';   
       }
	  //SUB-POPULATION PROFILE CONDITION
       $subpopulation="";
      
       if($population=="")
       {
        //FOR MALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 6)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 6 && $RN< 20)
            {
            $subpopulation="General";    
            }
             else if($RN >=20  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
     
          }
      
      //FOR FEMALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 8)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 8 && $RN< 24)
            {
            $subpopulation="General";    
            }
             else if($RN >=24  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
        
            }
       
        //FOR MALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 9)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 9 && $RN< 30)
            {
            $subpopulation="General";    
            }
             else if($RN >=30  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
            }
      
      //FOR FEMALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
       //FOR Male 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //FOR Female 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 14)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 14 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //ANother Condition Based on Subpopulation and Age..
       if($subpopulation=='Athlete' && $vp_age>40)
       {
        $subpopulation="Active";    
       }
       //FOR MALE 45-54 yr
       if(($vp_age >=45 && $vp_age<=54) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 15)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 15 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 45-54 
       if(($vp_age >=45 && $vp_age<=54) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 18)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 18 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR MALE 55-64 yr
       if(($vp_age >=55 && $vp_age<=64) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 16)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 16 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 55-64 
       if(($vp_age >=55 && $vp_age<=64) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       
        //FOR MALE 65-74  yr
       if(($vp_age >=65 && $vp_age<=74) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 65-74 
       if(($vp_age >=65 && $vp_age<=74) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 24)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 24 && $RN< 52)
            {
            $subpopulation="General";    
            }
             else if($RN >=52)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Male 75+ 
       if($vp_age>= 75 && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 31)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 31 && $RN< 62)
            {
            $subpopulation="General";    
            }
             else if($RN >=62)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Female 75+ 
       if($vp_age>= 75 && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 36)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 36 && $RN< 68)
            {
            $subpopulation="General";    
            }
             else if($RN >=68)
            {
            $subpopulation="Active";    
            }
       }
    }//If Sub-population is not taken firstly then it generates
     else
     {
     $subpopulation=$population;    
     }
    
     $randomgenbmi=$this->mass_and_bmi($vp_age,$v); 
      
     $object1 = new PHPExcel_Calculation_Statistical();
      $mean_log_Mass=$randomgenbmi['mean_log_Mass'];
      $sd_log_Mass=$randomgenbmi['sd_log_Mass'];
       $m_BMI=$randomgenbmi['m_BMI'];
       $b_BMI=$randomgenbmi['b_BMI'];
       $s_BMI=$randomgenbmi['s_BMI'];
        
     
//for male and Sedentary
       if($v=='Male' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.007';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Sedentary
       if($v=='Female' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.038';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.75),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     
     //for Male and General
       if($v=='Male' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.25),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and General
       if($v=='Female' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
      //for Male and Athlete or active
       if($v=='Male' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.975';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Athlete or active
       if($v=='Female' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.962';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
 $randomData=array(
     'firstname'=>$firstname,
     'lastname'=>$lastname,
     'gender'=>$v,
     'daydropdown'=>$valuedate,
     'monthdropdown'=>$monthkey,
     'yeardropdown'=>$valueyears,
     'age_category'=>$agerange,
     'occupation'=>$occupation,
     'sub_population'=>$subpopulation,
     'country'=>$selectedyears,
     'Random_generated_log_BMI'=>$randomprob,
     'MASS'=>round($MASS,2),
     'BMI'=>round($randombmi,1),
     'HEIGHT'=>round($HEIGHT,1),
     'probability_LOG_BMI'=>$probability,
      'probability_random_BMI'=>$probabilitybmi,
     'm_BMI'=>$m_BMI,
     'b_BMI'=>$b_BMI,
     's_BMI'=>$s_BMI,
     'mean_log_Mass'=>$mean_log_Mass,
     'sd_log_Mass'=>$sd_log_Mass,
     'vp_age'=>$vp_age
     ); 
 return $randomData; 
       }//END OF FUNCTION GENERATE RANDOM
     
     //fraction random number.
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }
       //PUT MASS and BMI 
       function mass_and_bmi($vp_age,$v)
       {
            if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.38';    
        $sd_log_Mass='0.157';
        $m_BMI='0.2305';
        $b_BMI='6.44';
        $s_BMI='1.543';
            
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.385';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2325';
        $b_BMI='6.44';
        $s_BMI='1.532';
       }
       //FOR MALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.46';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2395';
        $b_BMI='6.44';
        $s_BMI='1.52';
       }

//FOR MALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.475';    
        $sd_log_Mass='0.165';
        $m_BMI='0.2435';
        $b_BMI='6.44';
        $s_BMI='1.506';
       }
      
       //FOR MALE 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.483';    
        $sd_log_Mass='0.1653';
        $m_BMI='0.2545';
        $b_BMI='5.77';
        $s_BMI='1.998';
       }
 
//FOR MALE 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.484';    
        $sd_log_Mass='0.176';
        $m_BMI='0.2567';
        $b_BMI='5.77';
        $s_BMI='1.994';
       }
 
//FOR MALE 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.494';    
        $sd_log_Mass='0.1725';
        $m_BMI='0.2842';
        $b_BMI='3.42';
        $s_BMI='1.685';
       }
       

//FOR MALE 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.486';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2868';
        $b_BMI='3.22';
        $s_BMI='1.683';
       }

//FOR MALE 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.481';    
        $sd_log_Mass='0.151';
        $m_BMI='0.2892';
        $b_BMI='3.7';
        $s_BMI='1.85';
       }
	
//FOR MALE 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.467';    
        $sd_log_Mass='0.15';
        $m_BMI='0.2892';
        $b_BMI='3.2';
        $s_BMI='1.799';
       }
      //FOR MALE 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.452';    
        $sd_log_Mass='0.148';
        $m_BMI='0.3211';
        $b_BMI='1.74';
        $s_BMI='1.87';
       }
      
       //FOR MALE 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.362';    
        $sd_log_Mass='0.143';
        $m_BMI='0.2677';
        $b_BMI='6.04';
        $s_BMI='1.85';
       }
       
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.339';    
        $sd_log_Mass='0.1375';
        $m_BMI='0.267';
        $b_BMI='6.11';
        $s_BMI='1.529';
       }
      
       
//FOR FEMALE 
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.1835';    
        $sd_log_Mass='0.1785';
        $m_BMI='0.2655';
        $b_BMI='6.24';
        $s_BMI='1.456';
            
           } 
        //FOR FEMALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.205';    
        $sd_log_Mass='0.167';
        $m_BMI='0.2686';
        $b_BMI='6.44';
        $s_BMI='1.56';
       }
      //FOR FEMALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.2442';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2685';
        $b_BMI='6.8';
        $s_BMI='1.466';
       }

//FOR FEMALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.262';    
        $sd_log_Mass='0.165';
        $m_BMI='0.279';
        $b_BMI='6.44';
        $s_BMI='1.456';
       }
      
      //FOR Female 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.293';    
        $sd_log_Mass='0.1724';
        $m_BMI='0.3018';
        $b_BMI='5.15';
        $s_BMI='1.8022';
       }
 
//FOR Female 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.176';
        $m_BMI='0.3135';
        $b_BMI='4.45';
        $s_BMI='1.8022';
       }
 

//FOR Female 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.309';    
        $sd_log_Mass='0.1844';
        $m_BMI='0.323';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }
       
//FOR Female 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.169';
        $m_BMI='0.3248';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }

//FOR Female 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.302';    
        $sd_log_Mass='0.1715';
        $m_BMI='0.3312';
        $b_BMI='3.88';
        $s_BMI='1.821';
       }

//FOR Female 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.3';    
        $sd_log_Mass='0.15';
        $m_BMI='0.3481';
        $b_BMI='2.88';
        $s_BMI='1.821';
       }
//FOR Female 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.292';    
        $sd_log_Mass='0.1575';
        $m_BMI='0.362';
        $b_BMI='3.11';
        $s_BMI='1.845';
       }
      
       //FOR Female 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.229';    
        $sd_log_Mass='0.143';
        $m_BMI='0.323';
        $b_BMI='5.11';
        $s_BMI='1.845';
       }
  
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.192';    
        $sd_log_Mass='0.143';
        $m_BMI='0.355';
        $b_BMI='3.65';
        $s_BMI='1.658';
       }
      
     $data=array('mean_log_Mass'=>$mean_log_Mass,'sd_log_Mass'=>$sd_log_Mass,'m_BMI'=>$m_BMI,'b_BMI'=>$b_BMI,'s_BMI'=>$s_BMI);
      return $data;
      
       }
    
    

//GET AGE RANGE
       function getAgeRange($age)
       {
       	
			 if($age >= 18 && $age <=29)
			 {
				$agerange = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $agerange = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				 $agerange = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				$agerange = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				 $agerange = '60-69' ;
			 }
			 elseif($age >=70 && $age <=79)
			 {
				$agerange = '70-79' ;
			 }
                          elseif($age >=80 && $age <=84)
			 {
				$agerange = '80-84' ;
			 }
                          elseif($age >84)
			 {
				$agerange = '84+' ;
			 }
                    
                         return $agerange;     
                         
                   }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    
    

    
      
}
?>