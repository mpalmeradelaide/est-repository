
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Screening</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<style>
.big{height:1190px !important;}
.small{height:800px !important;}
/*.dot-red{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#00ff00;}*/

.dot-red{position:relative;width:21px;height:21px;border-radius:15px;background:none;border-color:#000;border-width:2px;border-style:solid}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script>
    
function doConfirm(msg, yesFn, noFn) {
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".yes,.no").unbind().click(function () {
        confirmBox.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}
    
    
  $(function() {
	 
	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
	   console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
			// console.log($('.left').hasClass('fixed');
		});
                
                
                    $("#form_new").submit(function (e) {
        e.preventDefault();
        var form = this;
        doConfirm("Warning: The current client data will be lost", function yes() {
            form.submit();
        }, function no() {
            // do nothing
        });
    });
	
}); 


</script>

</head>

<body>


<script>

</script>  
    <?php 
    
    //print_r($screening);
	//echo "</br>";
//var_dump($screening);

   function cumnormdist($x)
        {
          $b1 =  0.319381530;
          $b2 = -0.356563782;
          $b3 =  1.781477937;
          $b4 = -1.821255978;
          $b5 =  1.330274429;
          $p  =  0.2316419;
          $c  =  0.39894228;

          if($x >= 0.0) {
              $t = 1.0 / ( 1.0 + $p * $x );
              return (1.0 - $c * exp( -$x * $x / 2.0 ) * $t *
              ( $t *( $t * ( $t * ( $t * $b5 + $b4 ) + $b3 ) + $b2 ) + $b1 ));
          }
          else {
              $t = 1.0 / ( 1.0 - $p * $x );
              return ( $c * exp( -$x * $x / 2.0 ) * $t *
              ( $t *( $t * ( $t * ( $t * $b5 + $b4 ) + $b3 ) + $b2 ) + $b1 ));
            }
        }


		
  $pa2 = $screening['pa2'] ==''?0:$screening['pa2'];
  $pa4 = $screening['pa4'] ==''?0:$screening['pa4'];
  $pa6 = $screening['pa6'] ==''?0:$screening['pa6'];
  $pa7 = $screening['pa7'] ==''?0:$screening['pa7'];
  $pa8 = $screening['pa8'] ==''?0:$screening['pa8'];
  $pa9 = $screening['pa9'] ==''?0:$screening['pa9'];
  
  if($pa9=='Y')
   {
   $pa9=1;
   }else
   { $pa9=0;}
   
   //echo "isPhyData".
  $isPhyData = $screening['1']['status'];//echo "</br>";
  
  
  $healthAge = 0.0;
  $performanceAge = 0.0;
  $health_z_count = 0.0;
  $performance_z_count = 0.0;

  $performanceYearsAdded =0;
  $healthYearsAdded =0.0;
  //echo "</br>"."totalTimeExercising:".
  $totalTimeExercising = $pa2 + 2*$pa4 + $pa6;
  //echo "</br>"."totalTimeVigourous:".
  $totalTimeVigourous=$pa4;
  //echo "</br>"."familyHistory:".
  $familyHistory=$screening['family_history'];
  //echo "</br>"."clientCurrentAge:".
  $clientCurrentAge=$screening['0']['age'];
   
  $rf4=$screening['rf4'] ==''?0:$screening['rf4']."</br>";

  if (preg_match('/Y/',$rf4))
     $rf4=1;
  else
	 $rf4=0;
	 
  //echo "</br>"."rf4:".$rf4;

// echo "</br>"."rf5:".
 $rf5=$screening['rf5'] ==''?0:$screening['rf5']."</br>";
 
  
  if(($isPhyData == "Y") || $totalTimeExercising > 0){
            
            
            $performanceYearsAdded = $performanceYearsAdded - (-2 + (0.0125*$totalTimeExercising));
           // echo 'healthYearsAdded isPhyData1 :'.
		   $healthYearsAdded = $healthYearsAdded - (-2 + (0.0125*$totalTimeExercising));

            //echo "</br>";
	        $performanceYearsAdded = $performanceYearsAdded - (0.01*$totalTimeVigourous);
            //echo 'healthYearsAdded isPhyData2:'.
			$healthYearsAdded = $healthYearsAdded- (0.2 * (0.01*$totalTimeVigourous));
            //echo "</br>";
            //echo "<br>yrs_sendentary:".
	        $yrs_sendentary = $pa7+ $pa8;
            if($yrs_sendentary > 10){
                $yrs_sendentary = 10;
            }
			if($yrs_sendentary>0)
			{
            $performanceYearsAdded = $performanceYearsAdded + (-2 + (0.5*$yrs_sendentary));//echo "</br>";
            //echo 'healthYearsAdded isPhyData3:'.
			$healthYearsAdded = $healthYearsAdded + (1.25 * (-2 + (0.5 * $yrs_sendentary)));
			 }
           // echo "<br>yrs_sendentary:".$yrs_sendentary;
		   //echo "</br>";echo 'pa9:'.$pa9;echo "</br>";
           
		   if ( $pa9 == 1 ) {
                
                $performanceYearsAdded = $performanceYearsAdded + 0.5;//echo "</br>";
                 //echo 'healthYearsAdded isPhyData4:'.
				 $healthYearsAdded = $healthYearsAdded + 1;
                 //echo "</br>";
                
            }

//echo $familyHistory."</br>";
//echo"1".$healthYearsAdded."</br>" ;
	if($familyHistory =='Y'){
	
	//echo "</br>";
	//echo "clientCurrentAge:".$clientCurrentAge;echo "</br>";
	//echo "healthYearsAdded:".$healthYearsAdded;echo "</br>";
	// echo ' Family history healthYearsAdded isPhyData5:'.
    $healthYearsAdded = $healthYearsAdded + (-0.1 * $clientCurrentAge + 6.5);//echo "</br>";
    // echo"2".$healthYearsAdded."</br>" ;           
   }

//echo"3".$healthYearsAdded."</br>" ;


 	if($rf4 == 1){
                
                $performanceYearsAdded = $performanceYearsAdded + 5;


 	if(!$rf5){
              // echo "health smoke";echo "</br>";     //[healthZScoreList addObject:[NSString stringWithFormat:@"%f",2.0]];
                    //echo '1 health age :'.
                    $hlth_age = (cumnormdist(2.0))*100;//echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
                        $healthAge = $healthAge + (cumnormdist(2.0))*100;
                        $health_z_count = $health_z_count + 1;
                    }
                }
                else{
                    $tempZSCore = 0.50258 + (1.5108 * log($rf5));
                   // [healthZScoreList addObject:[NSString stringWithFormat:@"%f",tempZSCore]];
                    //echo '2 health age :'.
                      $hlth_age = $tempZSCore *100;//echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
                        $healthAge = $healthAge + cumnormdist($tempZSCore) *100;
                        $health_z_count = $health_z_count + 1;
                        
                        
                    }
                }

         
		}
		//echo"<br>healthAge----" .$healthAge;
		//echo"<br>----------------------------------------" ;
		
			//echo "<br>healthAge----hhhhh" .$healthAge;
				//echo "<br>health_z_count----" .$health_z_count;
		//echo "<br>healthYearsAdded----" .$healthYearsAdded;
		//echo "</br>";
		//echo"<br>----------------------------------------" ;
		}
		
		//echo "<br>rf_systolic----".
		$rf_systolic=$screening['rf_systolic'] ==''?0:$screening['rf_systolic'];
        //echo "<br>rf_diastolic----".
		$rf_diastolic=$screening['rf_diastolic'] ==''?0:$screening['rf_diastolic'];
        $clientGender=$screening['gender'] ==''?0:$screening['gender'];
				
		if (preg_match('/M/',$clientGender))
            $clientGender="M";
	     else
	        $clientGender="F";
					
		//echo "<br>clientGender----".$clientGender;			
		$mean_values_dict_male = array(
                    "SBP"=>128.4,
                    "DBP"=>83.22,
                    "chol"=>5.53,
                    "lnHDL"=>0.11,
                    "LDL"=>19.355,
                    "lntrig"=>0.138,
                    "glucose"=>5.901,
                    "lnBMI"=>3.229,
                    "WHR"=>0.92,
                    "lnSumSF"=>3.861,
                    "SRT"=>207.3,
                    "CRT"=>392.7,
                    "fev1"=>80.1,
                    "flex"=>4.53,
                    "grip"=>46.13,
                    "VO2max"=>36.6
                    );
                $sd_values_dict_male = array(
                    "SBP"=>19.1,
                    "DBP"=>10.45,
                    "chol"=>1.13,
                    "lnHDL"=>0.233,
                    "LDL"=>0,
                    "lntrig"=>0.501,
                    "glucose"=>1.354,
                    "lnBMI"=>0.129,
                    "WHR"=>0.066,
                    "lnSumSF"=>0.419,
                    "SRT"=>30,
                    "CRT"=>60,
                    "fev1"=>7.2,
                    "flex"=>10.08,
                    "grip"=>8.41,
                    "VO2max"=>9.74
                );
                $mean_values_dict_female = array(
                    "SBP"=>123.4,
                    "DBP"=>75.22,
                    "chol"=>5.54,
                    "lnHDL"=>0.109,
                    "LDL"=>19.355,
                    "lntrig"=>0.139,
                    "glucose"=>5.645,
                    "lnBMI"=>3.222,
                    "WHR"=>0.802,
                    "lnSumSF"=>3.981,
                    "SRT"=>218.2,
                    "CRT"=>409.1,
                    "fev1"=>80.3,
                    "flex"=>8.74,
                    "grip"=>28.15,
                    "VO2max"=>31.6
                );
                $sd_values_dict_female = array(
                    
                    "SBP"=>19.1,
                    "DBP"=>10.45,
                    "chol"=>1.12,
                    "lnHDL"=>0.235,
                    "LDL"=>0,
                    "lntrig"=>0.503,
                    "glucose"=>1.164,
                    "lnBMI"=>0.167,
                    "WHR"=>0.074,
                    "lnSumSF"=>0.416,
                    "SRT"=>32,
                    "CRT"=>64,
                    "fev1"=>7.3,
                    "flex"=>9.5,
                    "grip"=>5.25,
                    "VO2max"=>9.51
                    
                );

		$healthZScoreList=array();
                //echo "</br>"."rf8".$rf8=$screening['rf8'] ==''?0:$screening['rf8'];
                $rf8=$screening['rf8'] ==''?0:$screening['rf8'];
                if(($rf_systolic > 0) || ($rf_diastolic > 0)){
                //echo "SUCCESS";echo "</br>";
                if($clientGender == 'M'){
				//echo 'mean_SBP'.
                    $mean_SBP = $mean_values_dict_male["SBP"];//echo "</br>";
                    //echo 'sd_SBP'.
					$sd_SBP = $sd_values_dict_male["SBP"];//echo "</br>";
                    //echo 'zScore'.
                     $zScore = ($rf_systolic - $mean_SBP)/$sd_SBP;//echo "</br>";
                    //healthAge = healthAge + zScore;
                  //  [healthZScoreList addObject:[NSString stringWithFormat:@"%f",zScore]];
                   // array_push($zScore);
				   //  echo 'hlth_age'.
                  $hlth_age = cumnormdist($zScore) *100;//echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
					//  echo 'healthAge'.
                      $healthAge = $healthAge + cumnormdist($zScore)*100;//echo "</br>";
                        //echo 'health_z_count'.
						$health_z_count = $health_z_count + 1;//echo "</br>";
                       // NSLog(@" FINAL sbp healthAge :%f", healthAge);
                       // NSLog(@" sbp health_z_count:%f", health_z_count);
                        
                        
                    }
                    
//echo 'mean_DBP'.
                    $mean_DBP = $mean_values_dict_male["DBP"];//echo "</br>";
					//echo 'sd_DBP'.
                    $sd_DBP = $sd_values_dict_male["DBP"];//echo "</br>";
// echo 'zScore'.
                   $zScore = ($rf_diastolic - $mean_DBP)/$sd_DBP;//echo "</br>";
                    
                   //  echo 'hlth_agee'. 
                  $hlth_agee = cumnormdist($zScore) *100;//echo "</br>";
                    if(is_infinite($hlth_agee)){
                        
                    }
                    else{
					//echo 'healthAge'.
                        $healthAge = $healthAge + cumnormdist($zScore)*100;//echo "</br>";
                        //echo 'health_z_count'.
						$health_z_count = $health_z_count + 1;//echo "</br>";
                       // NSLog(@" FINAL dbp healthAge :%f", healthAge);
                        //NSLog(@" dbp health_z_count:%f", health_z_count);
                        
                        
                    }
                    
                    
                }
                else{
                    $mean_SBP = $mean_values_dict_female["SBP"];
                    $sd_SBP = $sd_values_dict_female["SBP"] ;

                    
                    $zScore = ($rf_systolic - $mean_SBP)/$sd_SBP;
                    $hlth_age = cumnormdist($zScore) *100;
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
                        $healthAge = $healthAge + cumnormdist($zScore)*100;
                        $health_z_count = $health_z_count + 1;
                    }
                    

                    $mean_DBP = $mean_values_dict_female["DBP"];
                    $sd_DBP = $mean_values_dict_female["DBP"];

                    $zScore = ($rf_diastolic - $mean_DBP)/$sd_DBP;
                    
                    
                    $hlth_agee = cumnormdist($zScore) *100;
                    if(is_infinite($hlth_agee)){
                        
                    }
                    else{
                        $healthAge = $healthAge + cumnormdist($zScore)*100;
                    }
                    
                    
                }
            }
            else
            {
			//echo "fail";echo "</br>";
                if($rf8 == 'Y'){
                    
                    $healthYearsAdded = $healthYearsAdded + 2;
                    
                    
                }
				//echo "<br>healthYearsAdded----" .$healthYearsAdded;
            }

            //rf_fbg
// echo "<br>rf_fbg:".
$rf_fbg=$screening['rf_fbg'] ==''?0:$screening['rf_fbg'];
            //  echo "<br>rf10:".
			$rf10= $screening['rf10'] ==''?0:$screening['rf10'];
   
            if($rf_fbg > 0){
                //echo 'FBG';echo "</br>";
				//echo 'FBGgggggggggggggg'.$clientGender;
              // if($clientGender == 'M'){
			  	if (preg_match('/M/',$clientGender)){
//echo 'mean_glucose'.
                    $mean_glucose = $mean_values_dict_male["glucose"];//echo "</br>";
                   // echo 'sd_glucose'.
				   $sd_glucose = $sd_values_dict_male["glucose"] ;//echo "</br>";
                   //echo 'glucose'.
				   $glucose = $rf_fbg;//echo "</br>";
                    //echo 'zScore'.
					$zScore =   ($glucose-$mean_glucose)/$sd_glucose;//echo "</br>";
//echo 'hlth_age'.
                    $hlth_age = cumnormdist($zScore) *100;//echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
					//echo 'healthAge'.
                        $healthAge = $healthAge + cumnormdist($zScore)*100;//echo "</br>";
                        //echo 'health_z_count'.
						$health_z_count = $health_z_count + 1;//echo "</br>";
                        
                    }
                   
                }
                else{
                    
                    $mean_glucose = $mean_values_dict_female["glucose"];
                    $sd_glucose = $sd_values_dict_female["glucose"];
                    $glucose = $rf_fbg;
                    $zScore =   ($glucose-$mean_glucose)/$sd_glucose;

                    $hlth_age = cumnormdist($zScore) *100;
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
                        $healthAge = $healthAge + cumnormdist($zScore)*100;
                        $health_z_count = $health_z_count + 1;
                    }
                    
                    
                }
            }
            else
            {
                if($rf10 == 'Y'){
                    
                    $healthYearsAdded = $healthYearsAdded + 2;
                    
                    
                }
                //echo "<br> rf_fbg healthYearsAdded----" .$healthYearsAdded;
            }
			
//echo"<br>rf_tbc:".			
$rf_tbc=$screening['colestrol'] ==''?0:$screening['colestrol'];
//echo"<br>rf_triglycerides:".
$rf_triglycerides=$screening['triglycerides'] ==''?0:$screening['triglycerides'];
 //echo "<br>rf9:".  
   $rf9= $screening['rf9'] ==''?0:$screening['rf9'];
if($rf_tbc != 0 || $rf_triglycerides != 0){

            $tbc_value = $rf_tbc;
          
            $trig_value = $rf_triglycerides;
                        
            if((preg_match('/M/',$clientGender)) && ($rf_tbc != '')){
                 $mean_Chol = $mean_values_dict_male["chol"];
				 $sd_Chol = $sd_values_dict_male["chol"];
               
                
                
                $zScore =   ($rf_tbc-$mean_Chol)/$sd_Chol;
                
                $hlth_age = cumnormdist($zScore) *100;
                if(is_infinite($hlth_age)){
                    
                }
                else{
				//echo"<br>healthAge:".
                    $healthAge = $healthAge + cumnormdist($zScore)*100;
                   // echo"<br>health_z_count:".
				   $health_z_count = $health_z_count + 1;
                }
               
                
            }  elseif (preg_match('/F/',$clientGender) && ($rf_tbc != '')){
                
                
               
                 $mean_Chol = $mean_values_dict_female["chol"];
                 $sd_Chol = $sd_values_dict_female["chol"];
                
                $zScore =   ($rf_tbc-$mean_Chol)/$sd_Chol;
                
                $hlth_age = cumnormdist($zScore) *100;
                if(is_infinite($hlth_age)){
                    
                }
                else{
                    $healthAge = $healthAge + cumnormdist($zScore) *100;
                    $health_z_count = $health_z_count + 1;
                    
                    
                }
              
            }
			if(preg_match('/M/',$clientGender) && ($rf_triglycerides != ''))
            {   
// echo 'trigLogV'.			
			$trigLogV = log($rf_triglycerides);
				 //  echo "</br>";
                    
                    $mean_lntrig = $mean_values_dict_male["lntrig"];
                    $sd_lntrig = $sd_values_dict_male["lntrig"];
                    //echo '</br>zScore'.
                    $zScore = ($trigLogV - $mean_lntrig)/$sd_lntrig;
						   //echo "</br>";
                    //echo '</br>hlth_ageggggg'.
					$hlth_age =  $healthAge +cumnormdist($zScore)*100;   //echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
					//echo "</br>healthAge:".
					$healthAge.":zScore:".cumnormdist($zScore)*100;
                        //echo 'healthAgemmmmmmm'.
						$healthAge = $healthAge + cumnormdist($zScore)*100;
						//echo "</br>";
						//echo 'health_z_count'.
                       $health_z_count = $health_z_count + 1;
                        
                    }
            }
            elseif(preg_match('/F/',$clientGender) && ($rf_triglycerides != '')){
                //echo 'trigLogV'.
                $trigLogV = log($rf_triglycerides);
				  // echo "</br>";
                    
                    $mean_lntrig = $mean_values_dict_female["lntrig"];
                    $sd_lntrig = $sd_values_dict_female["lntrig"];
                    //echo 'zScore'.
                    $zScore = ($trigLogV - $mean_lntrig)/$sd_lntrig;
						   //echo "</br>";
                    //echo 'hlth_age'.
					$hlth_age = $healthAge + cumnormdist($zScore)*100;   //echo "</br>";
                    if(is_infinite($hlth_age)){
                        
                    }
                    else{
					//echo 'healthAge'.
                        $healthAge = $healthAge + cumnormdist($zScore)*100;
						//echo "</br>";
                      // echo 'health_z_count'.
					  $health_z_count = $health_z_count + 1;
                        
                    }
                                
            }

}
else
    {
		 
            if($rf9 == 'Y'){
                $healthYearsAdded = $healthYearsAdded + 1;
                $healthAge = $healthAge + 2;
                
            }
			 
             //echo "<br>  healthYearsAdded----GGGGGGGGGG" .$healthYearsAdded;
			 //echo '<br>  healthAge'.$healthAge ;
        }
///////////////////////////////////////////tested /////////////////////////////////////////////////////////////////////////	
//echo "</br>rf_ldl:".
$rf_ldl=$screening['ldl'] ==''?0:$screening['ldl'];
if(($rf_ldl != 0)){
        if($clientGender == 'M'){
            
            $mean_LDL = $mean_values_dict_male["LDL"];
            
            $ldlPercent_zscore = (-30.129 + ($mean_LDL*$rf_ldl));

            $hlth_age = $ldlPercent_zscore;
            if(is_infinite($hlth_age)){
                
            }
            else{
                $healthAge = $healthAge + $hlth_age;
                $health_z_count = $health_z_count + 1.0;
                
            }
			//echo "<br>  healthAge----" .$healthAge;
			 //echo '<br>  health_z_count'.$health_z_count ;
         }
        else{
            $mean_LDL = $mean_values_dict_female["LDL"];
            
            $ldlPercent_zscore = (-30.129 + ($mean_LDL*$rf_ldl));
            
            $hlth_age = cumnormdist($ldlPercent_zscore)*100*1.5;
            if(is_infinite($hlth_age)){
                
            }
		    else{
                $healthAge = $healthAge + cumnormdist($ldlPercent_zscore)*100*1.5;
            }
            // rank
            $ranks = 0.0;
            if($ldlPercent_zscore > 0){
                $ranks = (100 - $ldlPercent_zscore)/100;
            }
            else{
                $ranks = $ldlPercent_zscore/100;
            }
            
            $hlth_agee = $ranks*100;
            if(is_infinite($hlth_agee)){
                
            }
            else{
                $healthAge = $healthAge + $ranks*100;
                $health_z_count = $health_z_count + 1;
            }
			
			//echo "<br>  healthAge----" .$healthAge;
			 //echo '<br>  health_z_count'.$health_z_count ;
           
            
        }
		}
 

///////////////////////////////////////////tested /////////////////////////////////////////////////////////////////////////	
 //echo "</br>rf_hdl:".
 $rf_hdl=$screening['hdl'] ==''?0:$screening['hdl'];
if(($rf_hdl != 0)){
        if($clientGender == 'M'){
            
             $mean_LDL = $mean_values_dict_male["LDL"];             
            $mean_lnHDL = $mean_values_dict_male["lnHDL"];
            $sd_lnHDL = $sd_values_dict_male["lnHDL"];
            
            $logV = log($rf_hdl);//also converted mg/dL to mm
            
            $zScore =   ($mean_lnHDL-$logV)/$sd_lnHDL;
            
         $hlth_age = cumnormdist($zScore)*100*1.5;
            if(is_infinite($hlth_age)){
                
            }
            else{
                
                $healthAge = $healthAge + cumnormdist($zScore)*100*1.5;
                $health_z_count = $health_z_count + 1.5;
                
            }
			//echo "<br>  healthAge----" .$healthAge;
			 //echo '<br>  health_z_count'.$health_z_count ;
            
        }
        else{
                       
            
            $mean_lnHDL = $mean_values_dict_female["lnHDL"];
            $sd_lnHDL  = $sd_values_dict_female["lnHDL"];
            $logV = log($rf_hdl);//also converted mg/dL to mm
            
            $zScore =   ($mean_lnHDL-$logV)/$sd_lnHDL;
            
            $hlth_age = cumnormdist($zScore)*100*1.5;
            if(is_infinite($hlth_age)){
                
            }
            else{
                $healthAge = $healthAge + cumnormdist($zScore)*100*1.5;
                $health_z_count = $health_z_count + 1.5;
                
            }
            //echo "<br>  healthAge----" .$healthAge;
			 //echo '<br>  health_z_count'.$health_z_count ;
                        
        }
        }

		if($rf_ldl > 0 && $rf_hdl > 0){
            $LHratio = ($rf_ldl)/($rf_hdl);
             //echo '<br>  LHratio'.$LHratio ;
            $LHratioAdjFinal = -15.286 + (11.683 *$LHratio)-(2.6667* $LHratio* $LHratio) + (0.22222 * $LHratio *$LHratio * $LHratio);

			 //echo '<br>  LHratioAdjFinal'.$LHratioAdjFinal ;
			 //echo '<br>  healthYearsAdded:'.$healthYearsAdded ;
            $healthYearsAdded = $healthYearsAdded + $LHratioAdjFinal;
            
            
         } 
		  //echo '<br>  healthYearsAdded'.$healthYearsAdded ;
///////////////////////////////////////////tested//////////////////////////////////////////////////////////////////	


// COMPOSITION INFO
      
        //self-report BMI  =  [weight [kg] / Height^2]  * 10,000
		/*
       echo "</br>"."self_weight".$self_weight=$screening['self_weight'] ==''?0:$screening['self_weight'];
       echo "</br>"."self_height".$self_height=$screening['self_height'] ==''?0:$screening['self_height'];
       echo "</br>"."m_weight".$m_weight=$screening['measured_weight'] ==''?0:$screening['measured_weight'];
       echo "</br>"."m_height".$h_weight=$screening['measured_height'] ==''?0:$screening['measured_height'];;
        */
	//echo"<br>------------BMIIIIII----------------------------" ;	
//echo "</br>self_weight:".		
		$self_weight=$screening['self_weight'] ==''?0:$screening['self_weight'];
        //echo "</br>self_height:".
		$self_height=$screening['self_height'] ==''?0:$screening['self_height'];
      // echo "</br>m_weight:".
	  $m_weight=$screening['measured_weight'] ==''?0:$screening['measured_weight'];
      //echo  "</br>h_weight:".
	  $m_height=$screening['measured_height'] ==''?0:$screening['measured_height'];
       $selfWeight = 0.0;
       $selfHeight = 0.0;
        
        if($self_weight > 0.0){
            $selfWeight = $self_weight;
            $clientWeight = $self_weight;
            $m_clientWeight = $self_weight;
            $bc_weight = $self_weight;
        }
        if($self_height > 0.0){
         $selfHeight = $self_height;
            $bc_Height = $self_height;
        }
        
        $selfReportBMI = 0.0;
        if(($self_weight > 0) && ($self_height > 0)){
		
            
            $selfHeight2 = $selfHeight*$selfHeight;
            $selfReportBMI = ($selfWeight/$selfHeight2)*10000;
          
           $bc_BMI = $selfReportBMI;
        }
      //  $mean_lnBMI = $mean_values_dict["lnBMI"];
       // $sd_lnBMI = $sd_values_dict["lnBMI"];
        
        
        
        
        if($selfReportBMI > 0.0){
            $bmi_value = $selfReportBMI;
        }
        if($self_height > 0.0){
            $height_value =$self_height;
        }
        if($self_weight > 0.0){
            $weight_value =$self_weight;
        }
        
         if($m_weight > 0 ||$m_height > 0){
           //echo"</br>measuredWeight:".
            $measuredWeight = $m_weight;
			//echo"</br>measuredHeight:".
            $measuredHeight = $m_height;
            //echo"</br>measuredHeight2:".
			$measuredHeight2 = $measuredHeight*$measuredHeight;
            $measuredReportBMI = ($measuredWeight/$measuredHeight2)*10000;
            
            
            $bc_BMI = $measuredReportBMI;
            // bc_BMI = measuredReportBMI;
            $clientWeight = $m_weight;
            $bc_weight = $m_weight;
            $bc_Height = $m_height;
            $m_clientWeight = $m_weight;
            
            if($measuredReportBMI > 0){
                $bmi_value = $measuredReportBMI;
            }
            if($m_height > 0){
                $height_value =$m_height;
            }
            if($m_weight > 0){
                $weight_value =$m_weight;
            }
            
          //  echo "</br>"."clientGenderrrrrrrrrrrrr".$clientGender =$screening['gender'] ==''?0:$screening['gender'];
            if($clientGender == 'M'){
               // echo "</br>BMI";
				//echo "</br>";
				//echo '</br>mean_lnBMI:'.
                $mean_lnBMI = $mean_values_dict_male["lnBMI"];
				//echo '</br>sd_lnBMI:'.
				$sd_lnBMI = $sd_values_dict_male["lnBMI"];
				//echo "</br>measuredReportBMI:".$measuredReportBMI;
                //echo '</br>zScore:'.
				$zScore = (log($measuredReportBMI) - $mean_lnBMI)/$sd_lnBMI;//1 july
					//echo "</br>";
               // echo '</br>hlth_age:'.
			   $hlth_age = cumnormdist($zScore)*100;
				//echo "</br>";
                if(is_infinite($hlth_age)){
                    
                }
                else{
                    //echo '</br>healthAge:'.
                    $healthAge = $healthAge + cumnormdist($zScore)*100;
						//echo "</br>";
						//echo '</br>health_z_count:'.
                    $health_z_count = $health_z_count + 1;
                    	//echo "</br>";
						//echo '</br>performanceAge:'.
                    $performanceAge = $performanceAge + cumnormdist($zScore)*100;
					//echo "</br>";
					//echo '</br>performance_z_count:'.
                    $performance_z_count = $performance_z_count + 1;
                }
				//echo '<br>  healthAge1:'.$healthAge ;
			 //echo '<br>  health_z_count1:'.$health_z_count ;
                
            }
            else{
                 $mean_lnBMI = $mean_values_dict_female["lnBMI"];
                $zScore =  (log($measuredReportBMI) - $mean_lnBMI)/$sd_lnBMI;
                
                $hlth_age = cumnormdist($zScore)*100;
                if(is_infinite($hlth_age)){
                    
                }
                else{
                   $healthAge = $healthAge + cumnormdist($zScore)*100;
                    $health_z_count = $health_z_count + 1;
                    
                    $performanceAge = $performanceAge + cumnormdist($zScore)*100;
                    $performance_z_count = $performance_z_count + 1;
                    
                }
				//echo '<br>  healthAge2'.$healthAge ;
			 //echo '<br>  health_z_count2:'.$health_z_count ;
            }
            
          $measuredHeight =$measuredWeight;
            
        }
        else{
           // $selfReportBMI="from database";
            //$clientGender="from database";
        //echo "<br>selfReportBMI".$selfReportBMI;
            if($selfReportBMI >= 30){
                $riskFactorNo = $riskFactorNo + 1;
            }
            if($clientGender == 'M'){
			//echo "BMI";echo "</br>";
                //echo 'mean_lnBMI'.
				$mean_lnBMI = $mean_values_dict_male["lnBMI"];//echo "</br>";
				//echo 'mean_lnBMI'.
				$sd_lnBMI = $sd_values_dict_male["lnBMI"];//echo "</br>";
                //zScore = ([self logx:selfReportBMI :10] - mean_lnBMI)/sd_lnBMI; //log issue
               // echo"<br>Z_score".$zScore = (log($selfReportBMI) - $mean_lnBMI)/$sd_lnBMI;
               //echo 'mean_lnBMI'. 
			   $zScore = (log($selfReportBMI) - $mean_lnBMI)/$sd_lnBMI;//echo "</br>";
              // echo 'mean_lnBMI'.
			  $hlth_age = cumnormdist($zScore)*100;//echo "</br>";
                if(is_infinite($hlth_age)){
                    
                }
                else{
                    // echo 'healthAge3:'.
                   $healthAge = $healthAge + cumnormdist($zScore)*100;//echo "</br>";
                   // echo 'health_z_count3:'.
				   $health_z_count = $health_z_count + 1;//echo "</br>";
                }
                
				//echo '<br>  healthAge'.$healthAge ;
			 //echo '<br>  health_z_count:'.$health_z_count ;
                $perf_age = cumnormdist($zScore)*100;
                if(is_infinite($perf_age)){
                    
                }
                else{
				//echo 'performanceAge'.
                    $performanceAge = $performanceAge + cumnormdist($zScore)*100;//echo "</br>";
// echo 'performance_z_count'.                   
				   $performance_z_count = $performance_z_count + 1;//echo "</br>";
                    
                }
                
            }
            else{
                $mean_lnBMI = $mean_values_dict_female["lnBMI"];
				$sd_lnBMI = $sd_values_dict_male["lnBMI"];
                $zScore = (log($selfReportBMI) - $mean_lnBMI)/$sd_lnBMI;
                
                $hlth_age = cumnormdist($zScore)*100;
                if(is_infinite($hlth_age)){
                    
                }
                else{
                    $healthAge = $healthAge + cumnormdist($zScore)*100;
                    $health_z_count = $health_z_count + 1;
                    
                    
                }
                //echo '</br>healthAge4:'.$healthAge + cumnormdist($zScore)*100;
                //echo '</br>health_z_count4:'.$health_z_count;
                
                $perf_age = cumnormdist($zScore)*100;
                if(is_infinite($perf_age)){
                    
                }
                else{
                    $performanceAge = $performanceAge + cumnormdist($zScore)*100;
                    $performance_z_count = $performance_z_count + 1;
                    
                }
                
            }
        }
         // echo"<br>healthAge----8---" .$healthAge;   
              //WAIST
       //  echo "</br>"."m_waist".$m_waist=$screening['waist'] ==''?0:$screening['waist'];
       // echo "</br>"."m_hip".$m_hip=$screening['waist'] ==''?0:$screening['waist'];
		// echo"</br>m_waist:".
		$m_waist=$screening['waist'] ==''?0:$screening['waist'];
         //echo"</br>m_hip:".
		 $m_hip=$screening['hip'] ==''?0:$screening['hip'];
       // $clientGender="from database";
       // $clientGender=$clientCurrentAge;
      //  $mean_WHR = $mean_values_dict["WHR"];
        //$sd_WHR = $sd_values_dict["WHR"];
        
        if(($m_waist) > 0.0){
            $waist_value = $m_waist;
        }
        
        if($m_waist > 0 && $m_hip > 0){
        
        if($clientGender == 'M'){
            /*
            if(($m_waist) > 94){
                $riskFactorNo = $riskFactorNo + 1;
            }
            */
            //Calculate the waist-to-hip ratio [WHR]
            //WHR = Waist [cm] / hip [cm]
            //echo"</br>measuredWHR:".
            $measuredWHR = (($m_waist))/(($m_hip));
			
           
            $bc_WHR = $measuredWHR;//measuredWHR;
            $bc_waist = $m_waist;
            //echo"</br>mean_WHR:".
			$mean_WHR = $mean_values_dict_male["WHR"];
            $sd_WHR = $sd_values_dict_male["WHR"];
			 //echo"</br>sd_WHR:".$sd_WHR ;
            //echo"</br>zScore:".
			$zScore = ($measuredWHR-$mean_WHR)/$sd_WHR;
            
            //echo"</br>hlth_age:".
			$hlth_age = cumnormdist($zScore)*100;
            if(is_infinite($hlth_age)){
                
            }
            else{
                
                $healthAge = $healthAge + cumnormdist($zScore)*100;
                $health_z_count = $health_z_count + 1;
                
            }
             //echo '</br>healthAge5:'.$healthAge + cumnormdist($zScore)*100;
                //echo '</br>health_z_count5:'.$health_z_count;
            $perf_age = cumnormdist($zScore)*100;
            if(is_infinite($perf_age)){
                
            }
            else{
                $performanceAge = $performanceAge + $perf_age;
                $performance_z_count = $performance_z_count + 1;
                
            }
            $m_waist =$m_hip;
        }
        else{
            /*
            if(($m_waist) > 80){
                $riskFactorNo = $riskFactorNo + 1;
            }
            */
           $measuredWHR = (($m_waist))/(($m_hip));
            //bc_WHR = measuredWHR;
            
            $bc_WHR = $measuredWHR;
            $bc_waist = $m_waist;
              $mean_WHR = $mean_values_dict_female["WHR"];
               $sd_WHR = $sd_values_dict_female["WHR"];
            $zScore = ($measuredWHR-$mean_WHR)/$sd_WHR;

            $hlth_age =cumnormdist($zScore)*100;
            if(is_infinite($hlth_age)){
                
            }
            else{
                $healthAge = $healthAge + cumnormdist($zScore)*100;
                $health_z_count = $health_z_count + 1;
                
                
            }
			//echo '<br>  healthAge6:'.$healthAge ;
			// echo '<br>  health_z_count6:'.$health_z_count ;
            $perf_age = cumnormdist($zScore)*100;
            if(is_infinite($perf_age)){
                
            }
            else{
                $performanceAge = $performanceAge + cumnormdist($zScore)*100;
                $performance_z_count = $performance_z_count + 1;
            }
            $m_waist =$m_hip;
            
        }
        }
		
		//echo"<br>healthAge----9---" .$healthAge; 
        /*
            echo "</br>"."m_biceps".$m_biceps=$screening['option_biceps'] ==''?0:$screening['option_biceps'];
            echo "</br>"."m_triceps".$m_triceps=$screening['option_triceps'] ==''?0:$screening['option_triceps'];
            echo "</br>"."m_subscapular".$m_subscapular=$screening['option_subscapular'] ==''?0:$screening['option_subscapular'];
			*/
			
			$m_biceps=$screening['option_biceps'] ==''?0:$screening['option_biceps'];
            $m_triceps=$screening['option_triceps'] ==''?0:$screening['option_triceps'];
            $m_subscapular=$screening['option_subscapular'] ==''?0:$screening['option_subscapular'];
       // $clientGender="from database";
        if(($m_biceps > 0.0) || ($m_triceps > 0.0) || ($m_subscapular > 0.0)){
            
            $mean_lnSumSF = $mean_values_dict["lnSumSF"];
            $sd_lnSumSF = $sd_values_dict["lnSumSF"];
            
            $sumSK = $m_biceps + $m_triceps + $m_subscapular;
         

            if($sumSK > 0.0){
                
            $bc_SOS = $sumSK;
            if($clientGender == 'M'){
			//echo "sumSK"; echo "</br>";
              //  echo 'mean_lnSumSF'.
			  $mean_lnSumSF = $mean_values_dict_male["lnSumSF"];//echo "</br>";
               // echo 'sd_lnSumSF'.
			   $sd_lnSumSF = $sd_values_dict_male["lnSumSF"];//echo "</br>";
               //  echo 'zScore'.
			   $zScore = (log($sumSK)-$mean_lnSumSF)/$sd_lnSumSF ;//echo "</br>";
                // echo 'hlth_age'.
                $hlth_age = cumnormdist($zScore)*100;//echo "</br>";
                if(is_infinite($hlth_age)){
                    
                }
                else{
				//echo 'healthAge'.
                     $healthAge = $healthAge + cumnormdist($zScore)*100;//echo "</br>";
                    // echo 'health_z_count'.
					$health_z_count = $health_z_count + 1;//echo "</br>";
                    
                }
				// echo '</br>healthAge6'.$healthAge + cumnormdist($zScore)*100;
                //echo '</br>health_z_count6'.$health_z_count;
                //echo 'perf_age'.
				$perf_age = cumnormdist($zScore)*100;//echo "</br>";
                if(is_infinite($perf_age)){
                    
                }
                else{
				//echo 'performanceAge'.
                     $performanceAge = $performanceAge + cumnormdist($zScore)*100;//echo "</br>";
                   // echo 'performance_z_count'.
				   $performance_z_count = performance_z_count + 1.5;//echo "</br>";
                    
                }
            
            }
            else{
                
                //zScore = ([self logx:sumSK :10]-mean_lnSumSF)/sd_lnSumSF;
                 $mean_lnSumSF = $mean_values_dict_female["lnSumSF"];
                $sd_lnSumSF = $sd_values_dict_female["lnSumSF"];
                $zScore = ($log($sumSK)-$mean_lnSumSF)/$sd_lnSumSF;
                
                $hlth_age = cumnormdist($zScore)*100;
                if(is_infinite($hlth_age)){
                    
                }
                else{
                    $healthAge = $healthAge + cumnormdist($zScore)*100;
                    $health_z_count = $health_z_count + 1;
                }
               $perf_age = cumnormdist($zScore)*100*1.5;
                if(is_infinite($perf_age)){
                    
                }
                else{
                    $performanceAge = $performanceAge + cumnormdist($zScore)*100*1.5;
                    $performance_z_count = $performance_z_count + 1.5;
                    
                    
                }
               // [self getSOSValue:dbMgrBody.m_biceps :dbMgrBody.m_triceps :dbMgrBody.m_subscapular];
                
            }
            }
        }
		//echo"<br>------------Body----------------------------" ;
		
			//echo "<br>healthAge----:" .$healthAge;
				//echo "<br>health_z_count----:" .$health_z_count;
		//echo "<br>healthYearsAdded----:" .$healthYearsAdded;
		//echo "</br>";
		//echo"<br>----------------------------------------" ;
//echo"<br>healthAge----10---" .$healthAge; 
    //echo"<br>currentAge".$clientCurrentAge= $screening['0']['age'];
        $finalHealthAge = 0.0;
    if($health_z_count == 0){
        $health_z_count = 1;
    }
	
	//echo"healthAge:".$healthAge."</br>";	
	//echo"healthYearsAdded:".$healthYearsAdded;
    $finalHealthAge = $healthAge / $health_z_count;
   // $healthAge = $finalHealthAge + $healthYearsAdded;
    //echo"healthAge FINAL 1 :".$healthAge;
    
    
//echo"healthAge2:".$healthAge."</br>";	
//	if($healthAge==0){
	//echo "fdsfds";
//	$healthAge=$clientCurrentAge;
	//}
	
	$finalAgeValue =0;
	if($finalHealthAge>0){
	 $finalAgeValue= ($clientCurrentAge+$finalHealthAge)/2+$healthYearsAdded;
	    if($clientCurrentAge < 26) {
         $finalAgeValue = $finalAgeValue + (-12 + (0.45*$clientCurrentAge));
         }
        if ($clientCurrentAge > 55 ) {

         $finalAgeValue= $finalAgeValue + (-8 + (0.155*$clientCurrentAge));
        
        
        }
	
	}else{
	$finalAgeValue=$clientCurrentAge;
	}
	$healthAge=$finalAgeValue;
	if($healthAge <18){
	$healthAge=18.0;
	}
	
	if($healthAge >80){
	$healthAge=80.0;
	}
	//echo"<br>$finalAgeValue:" .$finalAgeValue;
	
//echo"<br>finalHealthAge----11---" .$finalHealthAge; 		 
			//echo"<br>healthAge----3---" .$healthAge;			
            
      // echo"<br>healthAgeggggggg----7---" .$healthAge;
				
		//die;
  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        
    $risk_factor =0;
    if( $screening['physical_activity'] !='' || $screening['physical_activity'] !=0){
    if( $screening['physical_activity'] >=150){
        $risk_factor = $risk_factor-1;
       //echo $risk_factor.'physical_activity'."</br>";
        }else{
            if($screening['physical_activity'] >0){
                $risk_factor = $risk_factor+1;
                //echo $risk_factor.'physical_activity'."</br>";
            }
      }
    }
      $heart_disease_value ='No';  
	 // echo $screening['heart_disease']; 
     if($screening['heart_disease'] =='Y' || $screening['heart_disease'] =='N'){  
      
            if($screening['heart_disease'] =='Y'){
              
                
                if($screening['option_gender']=='M' || $screening['option_gender']=='F' ){
               //echo"ssss".$screening['option_age'];
                    if($screening['option_gender']=='M' && $screening['option_age'] < 55 && $screening['option_age'] != '' ){
                        $heart_disease_value ='Yes';
                        $risk_factor = $risk_factor+1;
                        //echo $risk_factor.'heart_disease'."</br>";
                }else{
                     if($screening['option_gender']=='F' && $screening['option_age'] < 65 && $screening['option_age'] != '' ){
                        $heart_disease_value ='Yes'; 
                        $risk_factor = $risk_factor+1;
                        //echo $risk_factor.'heart_disease'."</br>";
                    }
                }
               }
            } 
     }
       

if($screening['smoking'] =='Y' || $screening['smoking2'] == 'Y'){
$risk_factor = $risk_factor+1;
//echo $risk_factor.'smoking'."</br>";
}
$isHighBp="false";
$isHighColestrol="false";
$isHighGlucose="false";

if($screening['hbp']!="" && $screening['dbp']!=""){
if($screening['hbp'] >=140){
$risk_factor = $risk_factor+1;
$isHighBp ="true";
//echo $risk_factor.'hbp'."</br>";
}elseif($screening['dbp']>90 && $screening['hbp']<140){
$risk_factor = $risk_factor+1;
$isHighBp ="true";
//echo $risk_factor.'hbp2'."</br>";
}
}
/*else if($screening['hbp']!=""){
$risk_factor++;
}
 * */
 
elseif($screening['high_blood_pressure'] =='Y'){
$risk_factor = $risk_factor+1;
$isHighBp ="true";

//echo $risk_factor.'high_blood_pressure'."</br>";
}

if($screening['hbg']!=""){
if($screening['hbg']>=5.5){
$risk_factor = $risk_factor+1;
$isHighGlucose="true";
//echo $risk_factor.'hbg'."</br>";
}
}elseif($screening['high_sugar']=='Y'){
    $risk_factor = $risk_factor+1;
    $isHighGlucose="true";
//echo $risk_factor.'high_sugar'."</br>";
}

$lipid_factor =0;

if($screening['colestrol'] != "" && $screening['triglycerides'] !=""){
if($screening['colestrol']>=5.2){
 $lipid_factor= $lipid_factor +1;
 $isHighColestrol="true";
 //echo $lipid_factor.'colestrol'."</br>";
}
elseif($screening['triglycerides'] >=1.7){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'triglycerides'."</br>";
}
}
elseif($screening['colestrol']!=""){
if($screening['colestrol']>=5.2){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'colestrol'."</br>";
}
}
elseif($screening['triglycerides']!=""){
if($screening['triglycerides'] >=1.7){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'triglycerides'."</br>";
}
}
elseif($screening['high_colestrol']=='Y'){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'high_colestrol'."</br>";
}
$llm_arr =  explode(',', $screening['llm']);
//print_r($llm_arr);

//in_array('LIPID-lowering medication', $llm_arr)?'true':'false';
 $lipid ='false';
 $bloodPressure ='false';
 $glucose ='false';


if(in_array('LIPID-lowering medication', $llm_arr)){
    $lipid='true';
    if($isHighColestrol != "true")
    $lipid_factor= $lipid_factor +1;
    //echo $lipid_factor."LIPID-lowering medication";
}
if(in_array('BLOOD PRESSURE-lowering medication', $llm_arr)){
    $bloodPressure='true';
  if($isHighBp != "true")
     $risk_factor = $risk_factor+1;
}

if( $screening['dbp'] > 90 && $screening['hbp'] < 140 ){
      $bloodPressure='true';
}

if( $screening['hbp'] >=140 ){
      $bloodPressure='true';
}


if(in_array('GLUCOSE-lowering medication', $llm_arr)){
  $glucose='true';
  if($isHighGlucose != "true")
  $risk_factor = $risk_factor+1;
}
//1-left
$lipid  ."</br>";
//echo $bloodPressure ."</br>";
//echo $glucose ."</br>";

if($screening['hdl'] !="" && $screening['hdl'] !=0){
if($screening['hdl']<1){
$lipid_factor= $lipid_factor +1;
//$risk_factor = $risk_factor +1;
//echo $lipid_factor.'hdllll'."</br>";
}elseif($screening['hdl']>1.55){
    //$lipid_factor= $lipid_factor -1;
    $risk_factor = $risk_factor -1;
    //echo $lipid_factor.'hdldddd'."</br>";

}
}

if($screening['ldl'] !="" && $screening['ldl'] !=0){
if($screening['ldl']>=3.4){
    $lipid_factor= $lipid_factor +1;
    //echo $lipid_factor.'ldl'."</br>";

}
}


if($lipid_factor>1){
$risk_factor = $risk_factor + 1;
 //echo 'lipidfactor :'.$lipid_factor.'risk factor :'.$risk_factor."</br>";
}else{
$risk_factor = $risk_factor + $lipid_factor;
 //echo 'lipidfactorvalue :'.$lipid_factor.'risk factor :'.$risk_factor."</br>";
}

if($screening['bmi']>=30){
$risk_factor = $risk_factor+1;
 $risk_factor.'bmi'."</br>";
}
if($screening['gender'] =='M'){
if($screening['waist'] >94){
$risk_factor = $risk_factor+1;
 $risk_factor.'waist'."</br>";
}
if($screening['0']['age']>=45){
$risk_factor = $risk_factor+1;
 $risk_factor.'age'."</br>";
}
}else{
if($screening['waist'] >80){
$risk_factor = $risk_factor+1;
 $risk_factor.'waist female'."</br>";
}
if($screening['0']['age']>=55){
$risk_factor = $risk_factor+1;
 $risk_factor.'age female'."</br>";
}

}  

//$myClass->updateClientInfo($risk_factor);
   

$variable='' ;
$class='';
$text ='';
if( $screening['1']['status'] == 'Y'){
        $variable = "HIGH";
        $class = 'red-text';
          $text = " risk for beginning or upgrading an exercise program. Your total Risk Factor Score [max = 9 ; min = -2] and Health Age are also displayed below. We recommend you consult with your GP or allied health professional prior to commencing any new exercise program.";
    }
    else{
        
        if($risk_factor  >= 2){
           $variable = "Moderate";
            $class = 'orange-text';
            
            $text = " risk for beginning or upgrading an exercise program. Your total Risk factor score [max = 9 ; min = -2] and Health Age are also displayed below. While you may begin or continue with moderate-intensity exercise, we recommend you consult with your GP or allied health professional prior to commencing any vigorous activity.";
        }
        else if($risk_factor < 2){
             $variable = "Low";
            $class = 'green-text';
           
            $text = "risk for beginning or upgrading an exercise program. Your total Risk factor score [max = 9 ; min = -2] and Health Age are also displayed below.";          }
 }
    
  $myClass->updateClientInfo($risk_factor ,$variable);   
    
    
    
    
    
    
    ?>
<?php //print_r($screening); 
      //echo $healthAge;
      ?>
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo"  id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
    <?php  echo form_open('welcome/fetchScreenInfo','',$hidden); ?>     
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
  <!--  <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitScreening" type="submit" value="" title="edit client details"  id="edit-btn"/></span>
     </div> -->
    
     <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required>
			<input name="submitScreening" type="submit" value="" title="edit client details" class="edit_btn" />
		</span>
     </div>
<!--	</form>  -->
</div>
<!--End login --> 




 <!--Start Mid --> 
  <div class="mid3 screen-contain">
 
   
<!--Start contain --> 
<div class="contain">
 <?php
      $hidden = array('userid' => $id);
      echo form_open('welcome/fetchScreenInfo','',$hidden); ?>    
   <!--Start left --> 
   <div class="left" style="" >
   		<div class="btn" >
        <?php echo form_submit('mysubmit1','',"class='client_submit_form1' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form66' , 'id' = 'myform6'");  ?>
        </div>
       <?php echo form_close(); ?>   
   
   </div>
   <!--End Left --> 
   <!--Start right --> 
   <div class="right">
   		<div class="right-head">Screening Summary</div>
   		<div class="right-section">

       	<div class="screen-para" style="font-size:14px">You have been screened as  <b class="<?php echo $class ;  ?>"> <?php echo $variable; ?> </b> <?php echo $text; ?></div>
       
         <?php if($screening['proceed'] =='proceed' &&$screening['1']['status']=='Y'){  ?>
        <div class="screen-para-proceed">A professional decision has been made to allow the client to proceed.</div>
        <?php } ?>
        
               
        <div class="screen-data">
        
        <div class="screen-left">
        
        <?php if($screening['gender'] =='M'){  ?>
        <div class="screen-content-box <?php echo $screening['0']['age'] >= '45'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Age <b class="small">[yr]</b></div>
        	<div class="screen-value"><?php echo $screening['0']['age']; ?></div>
        </div>
        <?php }else{ ?> 
        <div class="screen-content-box <?php echo $screening['0']['age'] >= '55'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Age <b class="small">[yr]</b></div>
        	<div class="screen-value"><?php echo number_format($screening['0']['age'], 1); ?></div>
        </div>
        <?php } ?>
            
        
          <?php if($screening['physical_activity'] == '' || $screening['physical_activity'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Physical activity * <b class="small">[min/wk]</b></div>
        	<div class="screen-value"></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo $screening['physical_activity'] >= '150'?'green-text':'red-text'; ?>">
        	<div class="screen-head">Physical activity * <b class="small">[min/wk]</b></div>
        	<div class="screen-value"><?php echo $screening['physical_activity']; ?></div>
        </div>
          <?php } ?>   
            
        <?php if($screening['heart_disease'] == '' || $screening['heart_disease'] == '0'){ ?> 
         <div class="screen-content-box margin-family">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"></div>
             </div>
          <?php }else{ ?>
           
            <div class="screen-content-box margin-family <?php echo $heart_disease_value=='Yes'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"><?php echo $heart_disease_value;  ?></div>
             </div>
        <?php } ?>     
            
    <!--        
       
        <?php if($screening['heart_disease'] == '' || $screening['heart_disease'] == '0'){ ?> 
         <div class="screen-content-box margin-family">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"></div>
             </div>
          <?php }else{ ?>
           <?php if($screening['heart_disease'] == 'N'){ ?>
            <div class="screen-content-box margin-family green-text">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"><?php echo $screening['heart_disease'] == 'Y' ?'Yes':'No'; ?></div>
             </div>
        <?php }else{ ?>
            
                        <?php if($screening['option_gender'] =='M'){  ?>
                       <div class="screen-content-box margin-family <?php echo $screening['option_age'] >= '55'?'green-text':'red-text'; ?>">
                              <div class="screen-head">Family history of heart disease</div>
                              <div class="screen-value"><?php echo $screening['heart_disease'] == 'Y'  && $screening['option_age'] < '55'?'Yes':'No'; ?></div>
                      </div>
                           <?php }else{ ?> 
                       <div class="screen-content-box margin-family <?php echo $screening['option_age'] >= '65'?'green-text':'red-text'; ?>">
                              <div class="screen-head">Family history of heart disease</div>
                              <div class="screen-value"><?php echo $screening['heart_disease'] == 'Y' && $screening['option_age'] < '65'?'Yes':'No'; ?></div>
                      </div>
                     <?php } ?>
          <?php } ?>  
          <?php } ?>     
            
            
         --> 
          
          <?php if($screening['smoking'] == '' || $screening['smoking'] == '0'){ ?> 
         <div class="screen-content-box" >
        	<div class="screen-head">Smoking</div>
        	<div class="screen-value"></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo ($screening['smoking'] == 'Y' || $screening['smoking2'] == 'Y')?'red-text':'green-text'; ?>">
        	<div class="screen-head">Smoking</div>
        	<div class="screen-value"><?php echo ($screening['smoking'] == 'Y' || $screening['smoking2'] == 'Y')?'Yes':'No'; ?></div>
        </div>
          <?php } ?>    
            
        
        
         <?php if($screening['dbp'] != '' || $screening['dbp'] != 0){ 
                if($screening['hbp'] < 140 && $screening['dbp'] > 90){
        ?> 
                    <div class="screen-content-box red-text">
                                   <div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
                                   <div class="screen-value"><?php echo $screening['hbp']."/"; ?><?php echo $screening['dbp']; ?></div></div>
               <?php }else{ 
if($screening['hbp'] != '' || $screening['hbp'] != 0){ 
?>
                        <div class="screen-content-box <?php echo $bloodPressure=='true'?'red-text':'green-text';?>">
                                        <div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
                                        <div class="screen-value"><?php echo $screening['hbp']."/"; ?><?php echo $screening['dbp']; ?></div></div>
<?php }


}
}else{
 if($screening['hbp'] != '' || $screening['hbp'] != 0){   
   if($screening['hbp'] >= 140){
 ?>
 <div class="screen-content-box red-text">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"><?php echo $screening['hbp']; ?></div></div>
<?php }else{ ?>
<div  class="screen-content-box <?php echo $bloodPressure=='true'?'red-text':'green-text';?>">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"><?php echo $screening['hbp']; ?></div></div>
<?php } }else{
    if($screening['high_blood_pressure']=='Y' || $bloodPressure=='true'){

    ?>
      <div class="screen-content-box red-text">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
       </div> 	 
    
<?php }else{ ?>
    <div class="screen-content-box ">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"></div></div>
    
<?php }}}?> 
<!--
<?php if(($screening['dbp'] === '' || $screening['dbp'] === 0) && ($screening['hbp'] === '' || $screening['hbp'] === 0)){ ?>
<div class="screen-content-box ">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"></div></div>

<?php }?>   

-->        
        
        
         <?php if($screening['colestrol'] == '' || $screening['colestrol'] == 0){
             ?> 
        <div class="screen-content-box  <?php echo ($lipid=='true' || $screening['high_colestrol']=='Y') ?'red-text':'';?>">
        	<div class="screen-head">Total cholesterol <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['colestrol']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo ($screening['colestrol'] >= '5.2'|| $lipid=='true' )?'red-text':'green-text'; ?>">
        	<div class="screen-head">Total cholesterol <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['colestrol']; ?></div>
        </div>
          <?php } ?>
         
        <?php if($screening['hdl'] == '' || $screening['hdl'] == 0){ ?> 
        <div class="screen-content-box ">
        	<div class="screen-head">HDL * <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hdl']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo $screening['hdl'] <= '1'?'red-text':'green-text'; ?>">
        	<div class="screen-head">HDL * <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hdl']; ?></div>
        </div>
          <?php } ?>       
        
        <?php if($screening['ldl'] == '' || $screening['ldl'] == 0){ ?> 
        <div class="screen-content-box ">
        	<div class="screen-head ">LDL <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['ldl']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo $screening['ldl'] >= '3.4'?'red-text':'green-text'; ?>">
        	<div class="screen-head ">LDL <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['ldl']; ?></div>
        </div>
          <?php } ?>
        
        <?php if($screening['triglycerides'] == '' || $screening['triglycerides'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Triglycerides <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['triglycerides']; ?></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo $screening['triglycerides'] >= '1.7'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Triglycerides <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['triglycerides']; ?></div>
        </div>
          <?php } ?>
        
         
        
             
             <?php if($screening['hbg'] == '' || $screening['hbg'] == 0){ ?> 
         <div class="screen-content-box <?php echo ($glucose =='true' || $screening['high_sugar']=='Y')?'red-text':'';?> ">
        	<div class="screen-head">Blood glucose <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hbg']; ?></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo ($screening['hbg'] >= '5.5' || $glucose =='true')?'red-text':'green-text'; ?>">
        	<div class="screen-head">Blood glucose <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hbg']; ?></div>
        </div>
          <?php } ?>
         <?php if($screening['bmi'] == '' || $screening['bmi'] == 0){ ?> 
         <div class="screen-content-box">
        	<div class="screen-head">BMI</div>
        	<div class="screen-value"><?php echo $screening['bmi']!=0?$screening['bmi']:""; ?></div>
        </div>
          <?php }else{ ?>
           <div class="screen-content-box <?php echo $screening['bmi'] >= '30'?'red-text':'green-text'; ?>">
        	<div class="screen-head">BMI</div>
        	<div class="screen-value"><?php echo $screening['bmi']; ?></div>
        </div>
          <?php } ?>
         
         
         <?php if($screening['waist'] == '' || $screening['waist'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
          <?php }else{ ?>
          <?php if($screening['gender'] =='M'){  ?>
         <div class="screen-content-box <?php echo $screening['waist'] > '94'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
           <?php }else{ ?> 
         <div class="screen-content-box <?php echo $screening['waist'] > '80'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
        <?php } ?>
          <?php } ?> 
        <div class="screen-content-box">
        <div class="riskfactor red-text">risk factor</div>
        <div class="riskfactor green-text">* can be a positive or negative risk factor</div>
        </div>
        </div><!--End screen left--> 
        
        <div class="screen-right">
			<div class="score-head">Risk factor score</div>
                        <div class="score"><?php echo $risk_factor>9 ?9:$risk_factor;  ?></div>
        </div>
        
        <div class="screen-right1">
			<div class="score-head">Health Age</div>
                        <div class="score"><?php echo round($healthAge,1);  ?></div>
        </div>
        
		</div><!--End screen data--> 
       
      </div><!--End right section--> 
     <form class="form_new" id="form_new" action="<?php echo site_url(); ?>">
   <input type="submit" value="Finished" class="new-test" size="100"  />
   </form>
   <form class="form_print"><input type="button" value="Print report" onclick="window.print();return false;" /></form>
   
      <div id="confirmBox">
    <div class="message"></div>
    <span class="button yes">New Client</span>
    <span class="button no">Cancel</span>
     </div>
   



   <!--<div class="medical_top"></div>-->
 <?php if($screening['medical_notes'] != "" && $screening['medical_notes'] !=""){   ?>
   <div class="disc" style="padding-top:50px;"> 
       <div class="disc-head" style="color:#fff;">.</div>   
       <div class="disc-head">Medical Notes:</div>
       <div class="disc-text"><?php echo $screening['medical_notes'];?></div>
       </div>
 <?php } ?>
  <?php if($screening['medication_notes'] != "" && $screening['medication_notes'] !=""){   ?>    
  <div class="disc" style="margin-top:20px;">  
       <div class="disc-head" style="width:300px !important;">Medications and Conditions:</div>
       <div class="disc-text"><?php echo $screening['medication_notes'];?></div>
   </div>
      <?php } ?>
	  
	  <div class="disc" style="margin-top:20px;">  
       <div class="disc-head" style="width:300px !important;">Client Details:</div>
       <div class="disc-text"><?php echo $screening['client_info'];?></div>
   </div>
	  
    </div><!--End right --> 
   
</div>
   <!--End contain -->
<!--
<div class="footer screen-footer">&copy; Copyrights Reserved by Health Screen Pro</div>
-->

</div><!--End Mid --> 



	  
	  




    <div class="main-sign" style="margin-top:30px;">
       <div class="tester-sign">Client's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
	</div>
	
    <div class="main-sign" style="margin-top:10px;">	
       <div class="tester-sign">Tester's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
    </div>
   

   


</div><!--End Wrapper --> 


 <div class="disc-head" style="color:#fff;height:20px;">.</div> 
      <div class="disc" style="margin-top:300px;margin-left:50px;">        
       <div class="disc-head tp_mrgn">Disclaimer statement:</div>
       <div class="disc-text">This web-based screening tool assists the pre-exercise screening process using a national standard assessment tool. The tool was jointly developed by three professional organisations [Fitness Australia, Exercise and Sports Science Australia and Sports Medicine Australia] as a recommended pre-exercise screening system. However, the screening tool used as part of the screening system is not intended to provide advice on a particular matter, medical condition or health issue, nor does it substitute for professional advice from appropriately qualified medical or other health practitioners. No warranty of
safety should result from its use. The screening system in no way guarantees against injury or death. No responsibility or liability whatsoever can be accepted by Fitness Australia, Exercise and Sports Science Australia or Sports Medicine Australia  for any loss, damage or injury that may arise from any person acting on any statement or information contained in this software program or from the use of the pre-exercise screening tool.
</div>
   </div>
 
<?php if($screening['medication_pic']=='Y'){ ?>

<!--Page 2 start--> 
  <div class="page2"> 
  
    <div class="logo"  id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>

  <!--Start login --> 
<div class="login-cont">
    <?php  echo form_open('welcome/fetchScreenInfo','',$hidden); ?>     
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
  <!--  <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitScreening" type="submit" value="" title="edit client details"  id="edit-btn"/></span>
     </div> -->
    
     <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required>
			<input name="submitScreening" type="submit" value="" title="edit client details" class="edit_btn" />
		</span>
     </div>
<!--	</form>  -->
</div>
<!--End login --> 

<div class="right-head">Screening Summary</div>
  
  <div class="screen-data2">
  <div id="extra2" class="extra">
	   <!--back-Left arm -->
	   <div id="pt1" class="<?php echo $screening['pt1']==0?'dot-red':'dot-green';?>" style="left:72px;top:83px"></div>
	   <div id="pt2" class="<?php echo $screening['pt2']==0?'dot-red':'dot-green';?>" style="left:45px;top:121px"></div>
	   <div id="pt3" class="<?php echo $screening['pt3']==0?'dot-red':'dot-green';?>" style="left:30px;top:141px"></div>
	   
	   <!--back-Back -->
	   <div id="pt4" class="<?php echo $screening['pt4']==0?'dot-red':'dot-green';?>" style="left:149px;top:-25px"></div>
	   <div id="pt5" class="<?php echo $screening['pt5']==0?'dot-red':'dot-green';?>" style="left:149px;top:9px"></div>
	   <div id="pt6" class="<?php echo $screening['pt6']==0?'dot-red':'dot-green';?>" style="left:149px;top:40px"></div>
	   
	   <!--back-Right arm -->
	   <div id="pt7" class="<?php echo $screening['pt7']==0?'dot-red':'dot-green';?>" style="left:222px;top:-67px"></div>
	   <div id="pt8" class="<?php echo $screening['pt8']==0?'dot-red':'dot-green';?>" style="left:247px;top:-29px"></div>
	   <div id="pt9" class="<?php echo $screening['pt9']==0?'dot-red':'dot-green';?>" style="left:267px;top:-10px"></div>
	   
	   <!--back-left leg -->
	   <div id="pt10" class="<?php echo $screening['pt10']==0?'dot-red':'dot-green';?>" style="left:103px;top:-25px"></div>
	   <div id="pt11" class="<?php echo $screening['pt11']==0?'dot-red':'dot-green';?>" style="left:104px;top:60px"></div>
	   <div id="pt12" class="<?php echo $screening['pt12']==0?'dot-red':'dot-green';?>" style="left:92px;top:138px"></div>
	   
	   <!--back-Right leg -->
	   <div id="pt13" class="<?php echo $screening['pt13']==0?'dot-red':'dot-green';?>" style="left:192px;top:-100px"></div>
	   <div id="pt14" class="<?php echo $screening['pt14']==0?'dot-red':'dot-green';?>" style="left:190px;top:-14px"></div>
	   <div id="pt15" class="<?php echo $screening['pt15']==0?'dot-red':'dot-green';?>" style="left:201px;top:63px"></div>
	   
	   
	   <!----------------------------------------------------------------------------->
	    
	   </div>
	   </div>

    <div class="main-sign" style="margin-top:270px;">
       <div class="tester-sign">Client's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
	</div>
	
    <div class="main-sign" style="margin-top:10px;">	
       <div class="tester-sign">Tester's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
    </div>
	
	 <div class="disc-head" style="color:#fff;height:20px;">.</div> 
     <div class="disc" style="margin-top:350px;">        
		<div class="disc-head tp_mrgn">Disclaimer statement:</div>
		<div class="disc-text">This web-based screening tool assists the pre-exercise screening process using a national standard assessment tool. The tool was jointly developed by three professional organisations [Fitness Australia, Exercise and Sports Science Australia and Sports Medicine Australia] as a recommended pre-exercise screening system. However, the screening tool used as part of the screening system is not intended to provide advice on a particular matter, medical condition or health issue, nor does it substitute for professional advice from appropriately qualified medical or other health practitioners. No warranty ofsafety should result from its use. The screening system in no way guarantees against injury or death. No responsibility or liability whatsoever can be accepted by Fitness Australia, Exercise and Sports Science Australia or Sports Medicine Australia  for any loss, damage or injury that may arise from any person acting on any statement or information contained in this software program or from the use of the pre-exercise screening tool.
		</div>
	</div>
   <?php } ?>
</div> 
<!--Page 2 End--> 
	

</body>
</html>
