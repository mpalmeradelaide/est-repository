<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

function message($type,$msg,$content="")
{  $str="";
  	if($type=="success")
	{ $str.='<div class="isa_success">
     <i class="fa fa-check"></i>
     '.$msg.'
</div>';
	}
	elseif($type=="error")
	{
		$str.='<div class="isa_error">
   <i class="fa fa-times-circle"></i>
   '.$msg.'
</div>';
	}
	elseif($type=="warning")
	{
		$str.='<div class="isa_warning">
     <i class="fa fa-warning"></i>
     '.$msg.'
</div>';
	}
 
echo $str;
	   
}

function mystripslashes($object)
{
	if(is_array($object))
	{
		foreach ($object as &$row)
		{
			// if (!is_object($row) && !is_array($row))
			// {
				// foreach ($object as $key => $val)
					// {
						// $a = stripslashes($a);
						// $array[$key] = $a;
					// }
				// return ($array);
			// }	
			
			if (is_object($row))
			{
				foreach ($row as $key => &$val)
				{
					$row->$key=stripslashes($val);				
				}            
			}
		}
	return $object;
	}
}

  function paging($url,$query_string ='',$total_records,$per_page,$start_index){  
    $ci=& get_instance();
    $ci->load->library('pagination');
    $config['base_url'] = $url;
      
    $config['total_rows'] = $total_records;
    /* if($first_url == ''){
      $first_url  = $url;
    } */
      
    if($query_string != ''){
      $config['first_url']   =  $url.'?'.$query_string;
      $config['suffix']      = '?'.$query_string;
    }/*else {
      $config['first_url']  = $first_url;
    } */
      
    $config['per_page']      = $per_page;
    $config['uri_segment']   = $start_index;
    $config['full_tag_open'] = '<p>';
    $config['full_tag_close'] = '</p>';
    $ci->pagination->initialize($config);
    return  $ci->pagination->create_links();    
  }
  
  function qry_str($arr, $skip = ''){
	$s = "?";
	$i = 0;
	foreach($arr as    $key =>    $value){
	  if ($key !=    $skip){
        if (is_array($value)){
          foreach($value as $value2){
            if ($i == 0) {
              $s .= $key . '[]=' . $value2;
              $i = 1;
            }else {
              $s .= '&' .    $key . '[]=' . $value2;
            }
          }
        }else{
          if ($i == 0){
            $s .= "$key=$value";
            $i = 1;
          }else{
            $s .= "&$key=$value";
          }
        }
      }
    }
    return $s;
  }
  
  function get_pagesize_dropdown($url,$query_string ='',$qry_str='',$per_page=''){
    /* $pagesize = 4;
    //if(trim($per_page) != ''){*/
	  $pagesize = $per_page;
    //}
       
    if($qry_str == ''){
      $new_url = $url.'?per_page=';
    }else {
      // $q_arr = explode('=',$qry_str);
      // $pos = array_search('per_page',$q_arr);
           
      unset($query_string['per_page']);
      $q_str = qry_str($query_string);
      $new_url= $url.$q_str.'&per_page=';
    }  
       
    $str='Listing Per Page'; 
       
    $str.='<select name="listingpage" onchange="pagesize_dropdown(\''.$new_url.'\',this.value)">
                   <option value="15"  ';
    if($pagesize=="15"){ $str.=' selected="selected"';}
    $str.=' >15</option>';
    $str.='<option value="25"';
    if($pagesize=="25"){ $str.=' selected="selected"';}
	$str.='>25</option>';
	$str.=' <option value="50" ';
    if($pagesize=="50"){ $str.= ' selected="selected"';}
    $str.='>50</option>';
	$str.=' <option value="100" ';
    if($pagesize=="100"){ $str.= 'selected="selected"';}
    $str.='>100</option>
    </select>';
    return  $str;  
  }
  
  function get_value_by_id($field_name, $table_name, $where_condition){
    $ci=& get_instance();
	$ci->load->database();
	$ci->db->select($field_name);
	$query = $ci->db->get_where($table_name,$where_condition);
    if($query->num_rows() >=1){
      return $query->row(); 
    }else{
      return '0';
    }
  }
   
  function get_single_value($table_name,$field_name,$where_condition='',$idField="")
   {
     $ci=& get_instance();
     $ci->load->database();   
     $ci->db->select($field_name,false);
     if(!empty($idField))
     $ci->db->order_by($idField,'desc');
    
     if( $where_condition !='' ){
        $query = $ci->db->get_where($table_name,$where_condition);
     }
     else {
        $query =  $ci->db->get($table_name);
        
     }
     if($query->num_rows() >=1)
     {
         #echo $ci->db->last_query();
         return $query->row()->$field_name; 
     } 
     else
     {
         return '';
     }
    
   }
  
   function fetch_single_details($table='',$id_field='',$id_value='')
  {
      
    $ci=& get_instance();
    $ci->load->database(); 
    if($id_field!="" && $id_value!="")
    {
     $ci->db->where($id_field,$id_value);
    }
    $query = $ci->db->get($table);
    return $query->row();
    
    
  }

  
  function select($table='',$cond='',$flds='*',$fetch_type='single')
  {
    $ci=& get_instance();
    $ci->load->database(); 
    $ci->db->select($flds,false);
    ($cond!="")?$ci->db->where($cond):""; 
    $query=$ci->db->get($table);
    #echo $ci->db->last_query();
    if($query->num_rows() >=1)
     {
         if($fetch_type=="single")
        {
           return $query->row();    
        } 
        elseif($fetch_type=="multi")
        {
           return $query->result();
        }
        elseif($fetch_type=="count")
        {
           return $query->num_rows();
        }  
     }
     else
     {
         return "";
     }
  }
  
  function check_subscribed_unsubscribed_spots_hubs($uid='',$subId='',$type='')
  {
       return $status=select('ss_posts_subscribed','(sub_id="'.$subId.'" AND uid="'.$uid.'" AND user_type="user" AND subscriber_type="'.$type.'" AND sub_status="Active")','id','count');  
  }
  
    function get_total_result($where_field="",$id="",$tablename="")
   {
     $ci=& get_instance();
     $ci->load->database();   
     $ci->db->select('count(*) as count');
     if(!empty($where_field) && !empty($id))
     {
      $ci->db->where($where_field,$id);
     }
     $query = $ci->db->get($tablename);
     if($query->num_rows()>0)
     {
        return $query->row()->count; 
     } 
     else
     {
         return 0;
     }  
       
   }
  
  function fetch_total_value($table='',$id_field='',$id_value='')
  {
    $ci=& get_instance();
    $ci->load->database(); 
    if($id_field!="" && $id_value!="")
    {
     $ci->db->where($id_field,$id_value);
    }
    $query = $ci->db->get($table);
    return $query->result();
    
      
  }
  
  function fetch_single_value_through_join($select='',$from='',$joinTable='',$joinCondition='',$where='',$id)
  {
    $ci=& get_instance();
    $ci->load->database(); 
    $ci->db->select($select); 
    $ci->db->from($from);
    $ci->db->join($joinTable,$joinCondition);
    $ci->db->where($where,$id);
    $query = $ci->db->get();
    //print $ci->db->last_query();
    return $query->row();
      
  }
  
   function send_mail($to_email,$from_name,$from_email,$email_subject,$message,$cc='',$bcc='')
  {
      $ci=& get_instance();
      $config['mailtype'] = 'html';
      $ci->load->library('email',$config);
      $ci->email->from($from_email, $from_name);
      $ci->email->to($to_email);
      if($cc != ''){
          $ci->email->cc($cc); 
      }
      if($bcc != ''){
          $ci->email->bcc($bcc); 
      }
      $ci->email->subject($email_subject);
      $ci->email->message($message);
      $ci->email->send();
   }
   
   function hotness_meter_percentage($rating_id='',$rating_on='ss_posts',$type='hub')
   {
     
     $total_posts=select($rating_on,'(post="" AND wall_user_type="'.$type.'" AND post_status="Active")','post_id','count'); 
     $ratng_posts=select($rating_on,'(post="" AND wall_user_type="'.$type.'" AND wall_user_id="'.$rating_id.'" AND post_status="Active")','post_id','count');
     if(isset($ratng_posts) && !empty($ratng_posts))
        $rating_posts=$ratng_posts;
      else
        $rating_posts=0; 
     if(isset($total_posts) && !empty($total_posts))
     {
        $total_posts=$total_posts;
        $rating_percentage=(($rating_posts*100)/$total_posts); 
     }
     else
     {
         $rating_percentage=0;
     } 
     
    
     #echo $rating_percentage;
     return $result=array($rating_posts,$rating_percentage);
   }
  
   function hotness_meter_image($rating_id='')
   {
       $rating=hotness_meter_percentage($rating_id);
       $image=NULL;
       
       if($rating[1]>=0 && $rating[1]<=10)
       {
         $image='rating_10';  
       }
       elseif($rating[1]>=11 && $rating[1]<=20)
       {
        $image='rating_20';   
       }
       elseif($rating[1]>=21 && $rating[1]<=30)
       {
        $image='rating_30';   
       }
       elseif($rating[1]>=31 && $rating[1]<=40)
       {
         $image='rating_40';  
       }
       elseif($rating[1]>=41 && $rating[1]<=50)
       {
         $image='rating_50';  
       }
       elseif($rating[1]>=51 && $rating[1]<=60)
       {
         $image='rating_60';  
       }
       elseif($rating[1]>=61 && $rating[1]<=70)
       {
          $image='rating_70'; 
       }
       elseif($rating[1]>=71 && $rating[1]<=80)
       {
          $image='rating_80'; 
       }
       elseif($rating[1]>=81 && $rating[1]<=90)
       {
         $image='rating_90';  
       }
       elseif($rating[1]>=91 && $rating[1]<=100)
       {
         $image='rating_100';  
       }
       return $result=array($rating[0],$image);
   }

  function get_user_frnd_request($uid='',$type='count',$select='',$where='')
  {
     $ci=& get_instance();
     $ci->load->database(); 
     if($type=='count')
     {  
      $ci->db->select('count(*) as count');
     }
     elseif($type=='result')
     {
      $ci->db->select($select,false);
     }
     
     if($where=="")
     {
         $where="(ss_friend_req.status='Active' AND (ss_friend_req.inv_uid='".$uid."' OR ss_friend_req.uid='".$uid."'))";
     }
     $ci->db->join('ss_users','ss_users.uid=ss_friend_req.inv_uid','inner');
     $ci->db->join('ss_login','ss_login.uid=ss_friend_req.inv_uid AND ss_login.status="Active"','inner');
     $ci->db->order_by('id','desc');
     $query=$ci->db->get_where('ss_friend_req',$where);
     #echo $ci->db->last_query();
     if($type=='count') { return $query->row()->count; }elseif($type=='result') { return $query->result();  }
    
  }
    function check_is_stalking($uid='',$frid='')
    {
     $ci=& get_instance();
     $ci->load->database(); 
     $ci->db->where('( uid IN ("'.$frid.'","'.$uid.'") and frid IN ("'.$frid.'","'.$uid.'"))');
     $query=$ci->db->get('ss_friend_map'); 
     return $query->num_rows();   
    } 
    
    function check_is_accept($uid='',$frid='')
    {
     $ci=& get_instance();
     $ci->load->database(); 
     $where=array('uid'=>$uid,'frid'=>$frid);
     $query=$ci->db->get_where('ss_friend_map',$where); 
     return $query->num_rows();   
        
    }
   
   function fetch_stalkers_count($user,$type,$status="Active")  // function to fetch all stalkers related to that user
      {
           $ci=& get_instance();
           $ci->load->database(); 
          switch($type)
          {
              case "stalkingme" :
              {
                  return $ci->db->select('count(ss_friend_map.id) as count')
                                  ->join("ss_login","ss_login.uid=ss_friend_map.uid AND ss_login.status='".$status."'","inner")
                                  ->join("ss_users","ss_login.uid=ss_users.uid","inner")->where('(`ss_friend_map`.`frid` = '.$user.' AND `ss_friend_map`.`uid` NOT IN (SELECT `frid` FROM `ss_friend_map` WHERE `ss_friend_map`.`uid` = '.$user.'))')
                                  ->get("ss_friend_map")->row()->count;
                  break;
              }

              case "iamstalking" :
              {
                  return $ci->db->select('count(ss_friend_map.id) as count')
                  ->join("ss_login","ss_login.uid=ss_friend_map.frid AND ss_login.status='".$status."'","inner")
                  ->join("ss_users","ss_login.uid=ss_users.uid","inner")
                  ->where(array("ss_friend_map.uid"=>$user))->get("ss_friend_map")->row()->count;
                  break;
              }
          }
      }
      
    
   function get_user_frnd_request_for_notification($uid='',$type='count',$select='',$where='')
  {
     $ci=& get_instance();
     $ci->load->database(); 
     if($type=='count')
     {  
      $ci->db->select('count(*) as count');
     }
     elseif($type=='result')
     {
      $ci->db->select($select,false);
     }
     
     if($where=="")
     {
         $where="(ss_friend_req.status='Active' AND ss_friend_req.inv_uid='".$uid."' AND request_status='waiting')";
     }
     $ci->db->join('ss_users','ss_users.uid=ss_friend_req.inv_uid','inner');
     $ci->db->join('ss_login','ss_login.uid=ss_friend_req.inv_uid AND ss_login.status="Active"','inner');
     $ci->db->order_by('id','desc');
     $query=$ci->db->get_where('ss_friend_req',$where);
     #echo $ci->db->last_query();
     if($type=='count') { return $query->row()->count; }elseif($type=='result') { return $query->result();  }
    
  }
  
  function update_user_logout_time($uid='',$role='')
  {
     $ci=& get_instance();
     $ci->load->database();
     $ci->db->where(array('uid'=>$uid,'role'=>$role));
     $ci->db->update('ss_login',array('logout_timed'=>date('Y-m-d H:i:s')));
  } 
  
  function fetch_indiviual_posts($user,$type="spot",$select="*",$result='result',$status="Active",$notification_status='Opened')  // function to fetch posts of a perticular user
      {
          $ci=& get_instance();
          $ci->load->database();
            
          $subscribers = $ci->db->select("sub_id")->where(array("uid"=>$user,'subscriber_type'=>$type,'sub_status'=>'Active'))->get("ss_posts_subscribed")->result();  
        
         foreach($subscribers as $subs){$sub[] = $subs->sub_id;}
         if(isset($sub) && is_array($sub))
         {
           $subscriber=implode(',',$sub);
         
             $login_details = $ci->db->select("login_timed,logout_timed")->where(array("uid"=>$user,'role'=>'user'))->get("ss_login")->row();
             #echo $login_details->login_timed;
             #echo $login_details->logout_timed; 
             
             if($result=="result" || $result=="row" ){ $ci->db->select($select,false);}
             else{ $ci->db->select('count(post_id) as count'); }
             
             if($type=="hub"){ $ci->db->join('ss_hubs_branches as hub','hub.hub_branch_id=wall_user_id OR hub.hub_branch_id=post_user_id'); }
             elseif($type=='spot') { $ci->db->join('ss_outlets as spot','spot.outlet_id=wall_user_id OR spot.outlet_id=post_user_id');  }
             else { $ci->db->join('ss_hubs_branches as hub','hub.hub_branch_id=wall_user_id OR hub.hub_branch_id=post_user_id','left');
                    $ci->db->join('ss_outlets as spot','spot.outlet_id=wall_user_id OR spot.outlet_id=post_user_id','left'); }
             
             if($result=="count")
             {
              $date_time_condition='AND post_date BETWEEN CAST("' . $login_details->login_timed . '" AS DATETIME) AND CAST("' .date('Y-m-d H:i:s'). '" AS DATETIME)';
             }
             else
             {
               $date_time_condition="";  
             }
             if($type=="spot")  // in spot fetch only tapping
             { $ci->db->where('( notification_status="'.$notification_status.'" AND post_status="Active" and wall_user_type="'.$type.'" AND post_user_type="user" AND wall_user_id in ('.$subscriber.') AND post="" '.$date_time_condition.'  )'); } 
             elseif($type=="hub") // in hub fetch all spending and also posting
             { $ci->db->where('( notification_status="'.$notification_status.'" AND post_status="Active" and (wall_user_type="'.$type.'" OR post_user_type="'.$type.'") AND wall_user_id in ('.$subscriber.')  '.$date_time_condition.' )'); } 
              
             
             $query=$ci->db->get('ss_posts');
            # echo $ci->db->last_query();
             
             if($result=='result') { return $query->result(); }
             elseif($result=='row'){ return $query->row();  }
             else { return $query->row()->count; }
        }
         
      }
    function CalculateAge($date)
    {
       $age=NULL;
       if(!empty($date))
       {
        $y = date('Y');
        $m = date('n');
        $d = date('j');
        list($day,$mo,$yr) = explode('/',$date);
        $now = ($y*10000+$m*100+$d);
        $past = ($yr*10000+$mo*100+$day);
        $diff = ($past-$now);
        if ($diff>0) { $age = 0 ; }
        else
        {
        $age = (($y-$yr)-1);
        if (($m>$mo) || (($m>=$mo) && ($d>=$day))) { $age++; }
        }
       }
        return $age;
    }

  function get_album_title_by_id($album_id){
    $ci=& get_instance();
    $ci->load->database();
	$ci->db->select("album_title");
	$ci->db->where("album_id",$album_id);
	$query = $ci->db->get('tj_photo_album');
	return $query->row();
  }
  
    function statistics(){
    $ci=& get_instance();
    $ci->load->database();
    
    $ci->db->select("uid");
    $ci->db->where("status",'Active');
    $ci->db->where("role",'user');
    $activeusers = $ci->db->count_all_results('ss_login');
    
    $ci->db->select("uid");
    $ci->db->where("status",'Inactive');
    $ci->db->where("role",'user');
    $Inactiveusers = $ci->db->count_all_results('ss_login');
    
    $ci->db->select("uid");
    $ci->db->where("status",'Active');
    $ci->db->where("role",'vendor');
    $activebrands = $ci->db->count_all_results('ss_login');
    
    $ci->db->select("uid");
    $ci->db->where("status",'Inactive');
    $ci->db->where("role",'vendor');
    $Inactivebrands = $ci->db->count_all_results('ss_login');

        
    
    $data= array(
      'activeusers' => $activeusers,
      'Inactiveusers' => $Inactiveusers,
      'activebrands' => $activebrands,
      'Inactivebrands' => $Inactivebrands,
    );
    return $data;
  }

  function get_country_dropdown($country_id=''){
    return $output = get_dropdown('tj_country',"country_status = 'Active'",'country_name','asc',$country_id,'name="country_id" id="country_id" onchange="change_states(this.value)"','country_id','country_name');
}
  function get_state_dropdown($country_id,$state_id=''){
	return $output = get_dropdown('tj_state',"state_status = 'Active' and country_id=$country_id",'state_name','asc',$state_id,'name="state_id" id="state_id" onchange="change_city(this.value)"','state_id','state_name');
}
  function get_city_dropdown($state_id,$city_id=''){
    return $output = get_dropdown('tj_city',"city_status = 'Active' and state_id=$state_id",'city_name','asc',$city_id,'name="city_id" id="city_id" ','city_id','city_name');
}
  function get_dropdown($table_name,$where_condition,$order_by='',$order,$sel_value,$extra,$field_name1,$field_name2,$additional_fields='',$separator=' ')
   {
        $ci=& get_instance();
        $ci->load->database(); 
        $str = '';
        if($additional_fields == ''){
            $ci->db->select("$field_name1,$field_name2");
        }
        else {
            $ci->db->select("$field_name1,CONCAT_WS('".$separator."',$field_name2,$additional_fields) as $field_name2",false);
        }
        $ci->db->where($where_condition,'',false);
        $ci->db->order_by($order_by,$order);
        $query   = $ci->db->get($table_name);
         $ci->db->last_query();
        
        
        $qry_res = $query->result();
       
        if(is_array($qry_res))
        {
            $str   =  "<select ".$extra ." >";
            $str  .=  "<option value=''>Select</option>";
            foreach($qry_res as $key => $value)
            {
               $str.="<option value='".$value->$field_name1."' ";
               if(is_array($sel_value)){
                    if(in_array($value->$field_name1, $sel_value) ){
                        $str.=" selected ='selected' ";
                    }
               }
               else if($value->$field_name1 == $sel_value){
                   $str.= "selected = 'selected'";
               }
               $str.= " >".$value->$field_name2."</option>";
             }     
               $str.="</select>";
        }
        return $str;
  }
  
  
  function get_mydropdown($qry_res,$field_name1,$field_name2,$sel_value='',$extra=' ')
   {
        $ci=& get_instance();
        $ci->load->database(); 
        $str = '';
       
        if(is_array($qry_res))
        {
            $str   =  "<select ".$extra ." >";
            $str  .=  "<option value=''>Select</option>";
            foreach($qry_res as $key => $value)
            {
               $str.="<option value='".$value->$field_name1."' ";
               if(is_array($sel_value)){
                    if(in_array($value->$field_name1, $sel_value) ){
                        $str.=" selected ='selected' ";
                    }
               }
               else if($value->$field_name1 == $sel_value){
                   $str.= "selected = 'selected'";
               }
               $str.= " >".$value->$field_name2."</option>";
             }     
               $str.="</select>";
        }
        return $str;
  }
  
  
  function get_formated_date($date,$dateonly=false){
    $date = date_create($date, timezone_open('Asia/Kolkata')); 
    if($dateonly)
      $date = date_format($date, 'j M Y');
    else
      $date = date_format($date, 'j M Y G:i:s');   
    return $date;
  }
  

  
  function get_content($table_name,$where_condition)
  {
     $ci=& get_instance();
     $ci->load->database();
     $query = $ci->db->get_where($table_name,$where_condition); 
     return $query->row();
  }
  
  function count_gallery_images_by_id($album_id){
    $ci=& get_instance();
    $ci->load->database();
	$ci->db->select('count(*) as count');
	$where = 'album_id = '. $album_id . ' AND gallery_status="Active" ';
	$query = $ci->db->get_where('tj_photo_gallery',$where);
	return $query->row()->count;
  }
  
  function check_is_valid_agency($user_id,$user_type){
    
    if($user_id == '' || $user_type != 'agency'){
        redirect('login/agency');
    }
  }
  function day_options($day=''){
	//Day 
	for($x=1;$x<=31;$x++){
		$x =$x<10?"0".$x:$x;
		$sel = $day==$x?"selected":"";
		echo '<option value="'.$x.'" '.$sel.'>'.$x.'</option>';
	}
	//End day
}
  function month_options($mon=''){
	//Month 
	for($x=1;$x<=12;$x++){
		$x =$x<10?"0".$x:$x;
		$sel = $mon==$x?"selected":"";
		echo '<option value="'.$x.'" '.$sel.'>'.$x.'</option>';
		
	}
	//End Month
}

function year_options($year=''){
	//Year 
	$sYear =1940;
	$cuYear =2005;
	for($x=$sYear;$x<=$cuYear;$x++){
		$sel = $year==$x?"selected":"";
		echo '<option value="'.$x.'" '.$sel.'>'.$x.'</option>';
	}
	//End Year
}

function word_wrap($string,$limit)
{
    $temp_data = explode(" ",$string);
    $str = "";
    $char_count = 0;
    if(strlen($string)>$limit)
    {
        foreach($temp_data as $data)
        {
            $char_count = $char_count+strlen($data);
            if($char_count<=($limit-3)){$str .= $data." "; } 
        }
        return $str."...";
    }
    else
    {
        return $string;
    }
}

function image_resize($source,$destination,$width,$height,$object)
{
    $ci=& get_instance();
    $config['image_library'] = 'gd2';
    $config['source_image']    = UPLOADS.$source;
    $config['create_thumb'] = TRUE;
    $config['maintain_ratio'] = FALSE;
    $config['new_image'] = UPLOADS.$destination;
    $config['thumb_marker']     = '';
    $config['width']     = $width;
    $config['height']    = $height;
    $ci->load->library('image_lib', $config, $object);
    $ci->$object->resize();
}

function check_exist($table,$cond="",$joins="")
{
    $ci=& get_instance();
    $ci->load->database();
    if($joins!="")
    {
        foreach($joins as $join)
        {
            $ci->db->join($join->table,$join->cond,$join->type);
        }
    }
    ($cond!="")?$ci->db->where($cond):"";
    if($ci->db->count_all_results($table)>0){return TRUE;}else{return FALSE;}
}

function calculate_long_lati($add)
{
   $address = str_replace(" ", "+", $add);
   $json = file_get_contents("http://maps.google.com/maps/api/geocode/json?address=$address&sensor=false&region=");
   $json = json_decode($json);
   $lat =  isset($json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'})?$json->{'results'}[0]->{'geometry'}->{'location'}->{'lat'}:"28.668299";
   $long = isset($json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'})?$json->{'results'}[0]->{'geometry'}->{'location'}->{'lng'}:"77.101364";
   return $result=array($long,$lat,$add);
}
