<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script src="https://raw.githubusercontent.com/caleb531/jcanvas/master/jcanvas.js"></script>

</head>

<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php 
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                  <a onclick="window.location.href = 'http://localhost/healthscreen/index.php/Body/restricted_profile';" class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>                     
                <h1 class="page_head">Skinfold analysis</h1>        
              </td>
            </tr>                   
            
            <tr>
              <td colspan="3">	
              	    <div class="skinfold_container">
                    	<div class="skmap_left">skinfold map</div>
                        <div class="skmap_right">
                            <div class="skmap_rtxt1"><img src="<?php echo "$base/$image"?>/bold_line.jpg" alt=""> <?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</div>
                            <div class="skmap_rtxt2"><img src="<?php echo "$base/$image"?>/lite_line.jpg" alt=""> population, <?php echo $_SESSION['age_range'] ;?> yr<br>[10, 50, 90 %'ile]</div>
                        </div>
                    </div>
                  
                            <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>">
                            <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                            <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){echo "Male" ;}elseif($_SESSION['user_gender'] == "F"){echo "Female" ;}?>">
                            <input type="hidden" id="height" name="height" value="<?php echo $skinfolds["height"];?>">
                            <input type="hidden" id="body_mass" name="body_mass" value="<?php echo $skinfolds["body_mass"];?>">
                            
              		<div class="lrg_circle">
                    	<div class="lrg_circle_in">
                        	<div class="lrg_circle_inner">
                            	<div class="lrg_circle_under">&nbsp;</div>
                            </div>
                        </div>
                        
                        <div class="sk_txt1">triceps</div>
                        <div class="sk_txt2">subscapular</div>
                        <div class="sk_txt3">front thigh</div>
                        <div class="sk_txt4">iliac crest</div>
                        
                        <input type="hidden" id="triceps" name="triceps" value="<?php echo $skinfolds["triceps"] ; ?>">
                        <input type="hidden" id="subscapular" name="subscapular" value="<?php echo $skinfolds["subscapular"] ; ?>">
                        <input type="hidden" id="illiac" name="illiac" value="<?php echo $skinfolds["illiac"] ; ?>">
                        <input type="hidden" id="thigh" name="thigh" value="<?php echo $skinfolds["thigh"] ; ?>">                        
                        
                        <div class="hori_content">
                            <ul id="horizontal_scale">
                            	<li id="hor_scale1"></li>
                                <li id="hor_scale2"></li>
                                <li id="hor_scale3"></li>
                                <li id="hor_scale4"></li>
                                <li id="hor_scale5"></li>
                                <li id="hor_scale6"></li>
                                <li id="hor_scale7"></li>
                                <li id="hor_scale8"></li> 
                            </ul>
                        </div>
                        
                        <div class="vert_content">
                            <ul id="vertical_scale">
                            	<li id="ver_scale1"></li>
                                <li id="ver_scale2"></li>
                                <li id="ver_scale3"></li>
                                <li id="ver_scale4"></li>
                                <li id="ver_scale5"></li>
                                <li id="ver_scale6"></li>
                                <li id="ver_scale7"></li>
                                <li id="ver_scale8"></li>
                            </ul>
                        </div>
                        
                        <canvas id="myCanvas" width="640" height="640" style="position: absolute; top: 0; left:0;">&nbsp;</canvas>
                    </div>
                    
              </td>        
            </tr>             	
            
            <tr>
              <td align="left">&nbsp;</td> 
			  
              <td align="right" colspan="2" valign="bottom">      
              	 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <!--<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>-->
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>

             <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>

<script type="text/javascript">    
    
    $(window).bind("load", function() {       
       
        var triceps = document.getElementById("triceps").value ;        
        var subscapular = document.getElementById("subscapular").value ;         
        var illiac = document.getElementById("illiac").value ;          
        var thigh = document.getElementById("thigh").value ;  
        
        var height = document.getElementById("height").value ;  
        var body_mass = document.getElementById("body_mass").value ;  
        
        var skinfolds_arr = new Array();
        
        if(document.getElementById('gender').value == "Male")
        {
          var tricepSF = (triceps - 1.28) * 170.18 / height + 1.28 ;
          var subscapularSF = (subscapular - 2.07) * 170.18 / height + 2.07 ;
          var illiacSF = (illiac - 1.27) * 170.18 / height + 1.27 ;
          var thighSF = (thigh - 1.49) * 170.18 / height + 1.49 ;  
                    
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 6.49 ;
            var subscap10 = 9.47 ;
            var illiac10 = 8.23 ;
            var thigh10 = 6.98 ;
            
            var tricep50 = 11.35 ;
            var subscap50 = 16.24 ;
            var illiac50 = 20.96 ;
            var thigh50 = 12.16 ;
            
            var tricep90 = 23.55 ;
            var subscap90 = 30.72 ;
            var illiac90 = 38.02 ;
            var thigh90 = 25.18 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 7.36 ;
            var subscap10 = 11.65 ;
            var illiac10 = 11.41 ;
            var thigh10 = 7.66 ;
            
            var tricep50 = 12.24 ;
            var subscap50 = 19.94 ;
            var illiac50 = 25.64 ;
            var thigh50 = 13.13 ;
            
            var tricep90 = 24.91 ;
            var subscap90 = 32.71 ;
            var illiac90 = 38.45 ;
            var thigh90 = 26.73 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 7.7 ;
            var subscap10 = 13.59 ;
            var illiac10 = 13.98 ;
            var thigh10 = 7.91 ;
            
            var tricep50 = 12.76 ;
            var subscap50 = 21.34 ;
            var illiac50 = 26.1 ;
            var thigh50 = 13.19 ;
            
            var tricep90 = 24.42 ;
            var subscap90 = 34.09 ;
            var illiac90 = 38.75 ;
            var thigh90 = 27.35 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 8.13 ;
            var subscap10 = 13.61 ;
            var illiac10 = 13.86 ;
            var thigh10 = 8.16 ;
            
            var tricep50 =  12.96 ;
            var subscap50 = 21.77 ;
            var illiac50 = 24.39 ;
            var thigh50 = 13.27 ;
            
            var tricep90 = 25.39 ;
            var subscap90 = 33.42 ;
            var illiac90 = 36.8 ;
            var thigh90 = 28.98 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 8.63 ;
            var subscap10 = 14.33 ;
            var illiac10 = 15.74 ;
            var thigh10 = 9.12 ;
            
            var tricep50 = 12.98 ;
            var subscap50 = 22.27 ;
            var illiac50 = 23.5 ;
            var thigh50 = 13.06 ;
            
            var tricep90 = 25.45 ;
            var subscap90 = 35.1 ;
            var illiac90 = 36.67 ;
            var thigh90 = 27.76 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 8.09 ;
            var subscap10 = 12.19 ;
            var illiac10 = 9.98 ;
            var thigh10 = 7.96 ;
            
            var tricep50 = 12.47 ;
            var subscap50 = 19.46 ;
            var illiac50 = 18.14 ;
            var thigh50 = 13.58 ;
            
            var tricep90 = 23.36 ;
            var subscap90 = 30.82 ;
            var illiac90 = 31.79 ;
            var thigh90 = 28.77 ;
          }
        }
        else if(document.getElementById('gender').value == "Female")
        {
          var tricepSF = (triceps - 1.1) * 170.18 / height + 1.1 ;
          var subscapularSF = (subscapular - 1.74) * 170.18 / height + 1.74 ;
          var illiacSF = (illiac - 0.92) * 170.18 / height + 0.92 ;
          var thighSF = (thigh - 1.04) * 170.18 / height + 1.04 ;  
          
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 14.2 ;
            var subscap10 = 15.39 ;
            var illiac10 = 10.19 ;
            var thigh10 = 18.89 ;
            
            var tricep50 = 23.48 ;
            var subscap50 = 31.27 ;
            var illiac50 = 22.93 ;
            var thigh50 = 29.41 ;
            
            var tricep90 = 38.99 ;
            var subscap90 = 49.32 ;
            var illiac90 = 39.71 ;
            var thigh90 = 43.25 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 16.61 ;
            var subscap10 = 19.75 ;
            var illiac10 = 11.56 ;
            var thigh10 = 21.36 ;
            
            var tricep50 = 28.82 ;
            var subscap50 = 32.89 ;
            var illiac50 = 27.91 ;
            var thigh50 = 33.31 ;
            
            var tricep90 = 42.26 ;
            var subscap90 = 49.00 ;
            var illiac90 = 42.1 ;
            var thigh90 = 44.70 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 20.49 ;
            var subscap10 = 19.49 ;
            var illiac10 = 15.5 ;
            var thigh10 = 24.08 ;
            
            var tricep50 = 30.66 ;
            var subscap50 = 33.25 ;
            var illiac50 = 29.99 ;
            var thigh50 = 36.87 ;
            
            var tricep90 = 41.67 ;
            var subscap90 = 48.77 ;
            var illiac90 = 42.08 ;
            var thigh90 = 45.34 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 20.68 ;
            var subscap10 = 18.77 ;
            var illiac10 = 20.68 ;
            var thigh10 = 23.29 ;
            
            var tricep50 = 31.23 ;
            var subscap50 = 32.06 ;
            var illiac50 = 31.23 ;
            var thigh50 = 36.39 ;
            
            var tricep90 = 42.56 ;
            var subscap90 = 48.92 ;
            var illiac90 = 42.56 ;
            var thigh90 = 45.26 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 19.04 ;
            var subscap10 = 13.86 ;
            var illiac10 = 15.07 ;
            var thigh10 = 21.64 ;
            
            var tricep50 = 28.46 ;
            var subscap50 = 26.3 ;
            var illiac50 = 28.21 ;
            var thigh50 = 34.88 ;
            
            var tricep90 = 40.46 ;
            var subscap90 = 44.41 ;
            var illiac90 = 41.51 ;
            var thigh90 = 45.67 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 13.09 ;
            var subscap10 = 14.92 ;
            var illiac10 = 10.63 ;
            var thigh10 = 21.04 ;
            
            var tricep50 = 24.98 ;
            var subscap50 = 24.34 ;
            var illiac50 = 21.87 ;
            var thigh50 = 33.77 ;
            
            var tricep90 = 44.86 ;
            var subscap90 = 37.19 ;
            var illiac90 = 37.03 ;
            var thigh90 = 44.55 ;
          }
        }
    
        skinfolds_arr.push(tricepSF,subscapularSF,illiacSF,thighSF,tricep10,subscap10,illiac10,thigh10,tricep50,subscap50,illiac50,thigh50,tricep90,subscap90,illiac90,thigh90) ;
        
        var maxSkinfold = Math.max.apply(null, skinfolds_arr) ;
        
        
        document.getElementById("hor_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("hor_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("hor_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("hor_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("hor_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("hor_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("hor_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ; 
        document.getElementById("hor_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        
        document.getElementById("ver_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("ver_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("ver_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("ver_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("ver_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("ver_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("ver_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("ver_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ; 
                
        plot(maxSkinfold,tricep90,subscap90,thigh90,illiac90,tricep50,tricep10,subscap10,thigh10,illiac10,subscap50,thigh50,illiac50,tricepSF,subscapularSF,thighSF,illiacSF);  
        
    });
    
</script>        
    
<script src="<?php echo "$base/assets/js/"?>jCanvas.js"></script>

<script>

function plot(maxSkinfold,tricep90,subscap90,thigh90,illiac90,tricep50,tricep10,subscap10,thigh10,illiac10,subscap50,thigh50,illiac50,tricepSF,subscapularSF,thighSF,illiacSF){   
    
 var max1 = Math.round(maxSkinfold * 10) / 10 ;
 var max2 = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
 var max3 = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
 var max4 = Math.round((maxSkinfold * 0.25) * 10) / 10 ;

 // Plot tricep90       
 var x1_90 ; var y1_90 ;
 if(tricep90 >= max1)
 {
     x1_90 = 50 ; 
     y1_90 = 150 ;
 }
 else if(tricep90 < max1 && tricep90 > ((max1 + max2)/2))
 {
     x1_90 = 60 ; 
     y1_90 = 153 ;
 }
 else if(tricep90 == ((max1 + max2)/2))
 {
     x1_90 = 85 ; 
     y1_90 = 170 ;
 }
 else if(tricep90 < ((max1 + max2)/2) && tricep90 > max2)
 {
     x1_90 = 110 ; 
     y1_90 = 185 ;
 }
 else if(tricep90 == max2)
 {
     x1_90 = 115 ; 
     y1_90 = 190 ;
 }
 else if(tricep90 < max2 && tricep90 > ((max2 + max3)/2))
 {
     x1_90 = 130 ; 
     y1_90 = 200 ;
 }
 else if(tricep90 == ((max2 + max3)/2))
 {
     x1_90 = 155 ; 
     y1_90 = 220 ;
 }
 else if(tricep90 < ((max2 + max3)/2) && tricep90 > max3)
 {
     x1_90 = 175 ; 
     y1_90 = 230 ;
 }
 else if(tricep90 == max3)
 {
     x1_90 = 185 ; 
     y1_90 = 235 ;
 }
 else if(tricep90 < max3 && tricep90 > ((max3 + max4)/2))
 {
     x1_90 = 200 ; 
     y1_90 = 245 ;
 }
 else if(tricep90 == ((max3 + max4)/2))
 {
     x1_90 = 225 ; 
     y1_90 = 260 ;
 }
 else if(tricep90 < ((max3 + max4)/2) && tricep90 > max4)
 {
     x1_90 = 245 ; 
     y1_90 = 273 ;
 }
 else if(tricep90 == max4)
 {
     x1_90 = 250 ; 
     y1_90 = 275 ;
     
 }
 else if(tricep90 < max4 && tricep90 > (max4 / 2))
 {
     x1_90 = 255 ; 
     y1_90 = 282 ;
 }
 else if(tricep90 <= (max4 / 2))
 {
     x1_90 = 265 ; 
     y1_90 = 285 ;
 }


 // Plot subscap90       
 var x2_90 ; var y2_90 ;
 if(subscap90 >= max1)
 {
     x2_90 = 583 ; 
     y2_90 = 143 ;
 }
 else if(subscap90 < max1 && subscap90 > ((max1 + max2)/2))
 {
     x2_90 = 570 ; 
     y2_90 = 150 ;
 }
 else if(subscap90 == ((max1 + max2)/2))
 {
     x2_90 = 555 ; 
     y2_90 = 160 ;
 }
 else if(subscap90 < ((max1 + max2)/2) && subscap90 > max2)
 {
     x2_90 = 530 ; 
     y2_90 = 178 ;
 }
 else if(subscap90 == max2)
 {
     x2_90 = 520 ; 
     y2_90 = 186 ;
 }
 else if(subscap90 < max2 && subscap90 > ((max2 + max3)/2))
 {
     x2_90 = 508 ; 
     y2_90 = 193 ;
 }
 else if(subscap90 == ((max2 + max3)/2))
 {
     x2_90 = 485 ; 
     y2_90 = 210 ;
 }
 else if(subscap90 < ((max2 + max3)/2) && subscap90 > max3)
 {
     x2_90 = 465 ; 
     y2_90 = 223 ;
 }
 else if(subscap90 == max3)
 {
     x2_90 = 453 ; 
     y2_90 = 232 ;
 }
 else if(subscap90 < max3 && subscap90 > ((max3 + max4)/2))
 {
     x2_90 = 440 ; 
     y2_90 = 240 ;
 }
 else if(subscap90 == ((max3 + max4)/2))
 {
     x2_90 = 415 ; 
     y2_90 = 255 ;
 }
 else if(subscap90 < ((max3 + max4)/2) && subscap90 > max4)
 {
     x2_90 = 395 ; 
     y2_90 = 270 ;
 }
 else if(subscap90 == max4)
 {
     x2_90 = 385 ; 
     y2_90 = 278 ;
     
 }
 else if(subscap90 < max4 && subscap90 > (max4 / 2))
 {
     x2_90 = 380 ; 
     y2_90 = 280 ;
 }
 else if(subscap90 <= (max4 / 2))
 {
     x2_90 = 375 ; 
     y2_90 = 285 ;
 }
 
 // Plot thigh90      
 var x3_90 ; var y3_90 ;
 if(thigh90 >= max1)
 {
     x3_90 = 580 ; 
     y3_90 = 495 ;
 }
 else if(thigh90 < max1 && thigh90 > ((max1 + max2)/2))
 {
     x3_90 = 573 ; 
     y3_90 = 492 ;
 }
 else if(thigh90 == ((max1 + max2)/2))
 {
     x3_90 = 555 ; 
     y3_90 = 478 ;
 }
 else if(thigh90 < ((max1 + max2)/2) && thigh90 > max2)
 {
     x3_90 = 530 ; 
     y3_90 = 465 ;
 }
 else if(thigh90 == max2)
 {
     x3_90 = 515 ; 
     y3_90 = 453 ;
 }
 else if(thigh90 < max2 && thigh90 > ((max2 + max3)/2))
 {
     x3_90 = 505 ; 
     y3_90 = 446 ;
 }
 else if(thigh90 == ((max2 + max3)/2))
 {
     x3_90 = 485 ; 
     y3_90 = 432 ;
 }
 else if(thigh90 < ((max2 + max3)/2) && thigh90 > max3)
 {
     x3_90 = 463 ; 
     y3_90 = 416 ;
 }
 else if(thigh90 == max3)
 {
     x3_90 = 450 ; 
     y3_90 = 405 ;
 }
 else if(thigh90 < max3 && thigh90 > ((max3 + max4)/2))
 {
     x3_90 = 440 ; 
     y3_90 = 398 ;
 }
 else if(thigh90 == ((max3 + max4)/2))
 {
     x3_90 = 420 ; 
     y3_90 = 383 ;
 }
 else if(thigh90 < ((max3 + max4)/2) && thigh90 > max4)
 {
     x3_90 = 395 ; 
     y3_90 = 370 ;
 }
 else if(thigh90 == max4)
 {
     x3_90 = 385 ; 
     y3_90 = 363 ;
     
 }
 else if(thigh90 < max4 && thigh90 > (max4 / 2))
 {
     x3_90 = 380 ; 
     y3_90 = 360 ;
 }
 else if(thigh90 <= (max4 / 2))
 {
     x3_90 = 370 ; 
     y3_90 = 355 ;
 }
 
 
 // Plot illiac90      
 var x4_90 ; var y4_90 ;
 if(illiac90 >= max1)
 {
     x4_90 = 63 ; 
     y4_90 = 500 ;
 }
 else if(illiac90 < max1 && illiac90 > ((max1 + max2)/2))
 {
     x4_90 = 68 ; 
     y4_90 = 495 ;
 }
 else if(illiac90 == ((max1 + max2)/2))
 {
     x4_90 = 90 ; 
     y4_90 = 480 ;
 }
 else if(illiac90 < ((max1 + max2)/2) && illiac90 > max2)
 {
     x4_90 = 113 ; 
     y4_90 = 463 ;
 }
 else if(illiac90 == max2)
 {
     x4_90 = 128 ; 
     y4_90 = 453 ;
 }
 else if(illiac90 < max2 && illiac90 > ((max2 + max3)/2))
 {
     x4_90 = 133 ; 
     y4_90 = 450 ;
 }
 else if(illiac90 == ((max2 + max3)/2))
 {
     x4_90 = 155 ; 
     y4_90 = 435 ;
 }
 else if(illiac90 < ((max2 + max3)/2) && illiac90 > max3)
 {
     x4_90 = 180 ; 
     y4_90 = 418 ;
 }
 else if(illiac90 == max3)
 {
     x4_90 = 192 ; 
     y4_90 = 408 ;
 }
 else if(illiac90 < max3 && illiac90 > ((max3 + max4)/2))
 {
     x4_90 = 203 ; 
     y4_90 = 400 ;
 }
 else if(illiac90 == ((max3 + max4)/2))
 {
     x4_90 = 225 ; 
     y4_90 = 385 ;
 }
 else if(illiac90 < ((max3 + max4)/2) && illiac90 > max4)
 {
     x4_90 = 245 ; 
     y4_90 = 372 ;
 }
 else if(illiac90 == max4)
 {
     x4_90 = 255 ; 
     y4_90 = 365 ;
     
 }
 else if(illiac90 < max4 && illiac90 > (max4 / 2))
 {
     x4_90 = 260 ; 
     y4_90 = 360 ;
 }
 else if(illiac90 <= (max4 / 2))
 {
     x4_90 = 368 ; 
     y4_90 = 355 ;
 }
 
 

 // Plot tricep50       
 var x1_50 ; var y1_50 ;
 if(tricep50 >= max1)
 {
     x1_50 = 50 ; 
     y1_50 = 150 ;
 }
 else if(tricep50 < max1 && tricep50 > ((max1 + max2)/2))
 {
     x1_50 = 60 ; 
     y1_50 = 153 ;
 }
 else if(tricep50 == ((max1 + max2)/2))
 {
     x1_50 = 85 ; 
     y1_50 = 170 ;
 }
 else if(tricep50 < ((max1 + max2)/2) && tricep50 > max2)
 {
     x1_50 = 110 ; 
     y1_50 = 185 ;
 }
 else if(tricep50 == max2)
 {
     x1_50 = 115 ; 
     y1_50 = 190 ;
 }
 else if(tricep50 < max2 && tricep50 > ((max2 + max3)/2))
 {
     x1_50 = 130 ; 
     y1_50 = 200 ;
 }
 else if(tricep50 == ((max2 + max3)/2))
 {
     x1_50 = 155 ; 
     y1_50 = 220 ;
 }
 else if(tricep50 < ((max2 + max3)/2) && tricep50 > max3)
 {
     x1_50 = 175 ; 
     y1_50 = 230 ;
 }
 else if(tricep50 == max3)
 {
     x1_50 = 185 ; 
     y1_50 = 235 ;
 }
 else if(tricep50 < max3 && tricep50 > ((max3 + max4)/2))
 {
     x1_50 = 200 ; 
     y1_50 = 245 ;
 }
 else if(tricep50 == ((max3 + max4)/2))
 {
     x1_50 = 225 ; 
     y1_50 = 260 ;
 }
 else if(tricep50 < ((max3 + max4)/2) && tricep50 > max4)
 {
     x1_50 = 245 ; 
     y1_50 = 273 ;
 }
 else if(tricep50 == max4)
 {
     x1_50 = 250 ; 
     y1_50 = 275 ;
     
 }
 else if(tricep50 < max4 && tricep50 > (max4 / 2))
 {
     x1_50 = 255 ; 
     y1_50 = 282 ;
 }
 else if(tricep50 <= (max4 / 2))
 {
     x1_50 = 265 ; 
     y1_50 = 285 ;
 }


 // Plot subscap50       
 var x2_50 ; var y2_50 ;
 if(subscap50 >= max1)
 {
     x2_50 = 583 ; 
     y2_50 = 143 ;
 }
 else if(subscap50 < max1 && subscap50 > ((max1 + max2)/2))
 {
     x2_50 = 570 ; 
     y2_50 = 150 ;
 }
 else if(subscap50 == ((max1 + max2)/2))
 {
     x2_50 = 555 ; 
     y2_50 = 160 ;
 }
 else if(subscap50 < ((max1 + max2)/2) && subscap50 > max2)
 {
     x2_50 = 530 ; 
     y2_50 = 178 ;
 }
 else if(subscap50 == max2)
 {
     x2_50 = 520 ; 
     y2_50 = 186 ;
 }
 else if(subscap50 < max2 && subscap50 > ((max2 + max3)/2))
 {
     x2_50 = 508 ; 
     y2_50 = 193 ;
 }
 else if(subscap50 == ((max2 + max3)/2))
 {
     x2_50 = 485 ; 
     y2_50 = 210 ;
 }
 else if(subscap50 < ((max2 + max3)/2) && subscap50 > max3)
 {
     x2_50 = 465 ; 
     y2_50 = 223 ;
 }
 else if(subscap50 == max3)
 {
     x2_50 = 453 ; 
     y2_50 = 232 ;
 }
 else if(subscap50 < max3 && subscap50 > ((max3 + max4)/2))
 {
     x2_50 = 440 ; 
     y2_50 = 240 ;
 }
 else if(subscap50 == ((max3 + max4)/2))
 {
     x2_50 = 415 ; 
     y2_50 = 255 ;
 }
 else if(subscap50 < ((max3 + max4)/2) && subscap50 > max4)
 {
     x2_50 = 395 ; 
     y2_50 = 270 ;
 }
 else if(subscap50 == max4)
 {
     x2_50 = 385 ; 
     y2_50 = 278 ;
     
 }
 else if(subscap50 < max4 && subscap50 > (max4 / 2))
 {
     x2_50 = 380 ; 
     y2_50 = 280 ;
 }
 else if(subscap50 <= (max4 / 2))
 {
     x2_50 = 375 ; 
     y2_50 = 285 ;
 }
 
 // Plot thigh50      
 var x3_50 ; var y3_50 ;
 if(thigh50 >= max1)
 {
     x3_50 = 580 ; 
     y3_50 = 495 ;
 }
 else if(thigh50 < max1 && thigh50 > ((max1 + max2)/2))
 {
     x3_50 = 573 ; 
     y3_50 = 492 ;
 }
 else if(thigh50 == ((max1 + max2)/2))
 {
     x3_50 = 555 ; 
     y3_50 = 478 ;
 }
 else if(thigh50 < ((max1 + max2)/2) && thigh50 > max2)
 {
     x3_50 = 530 ; 
     y3_50 = 465 ;
 }
 else if(thigh50 == max2)
 {
     x3_50 = 515 ; 
     y3_50 = 453 ;
 }
 else if(thigh50 < max2 && thigh50 > ((max2 + max3)/2))
 {
     x3_50 = 505 ; 
     y3_50 = 446 ;
 }
 else if(thigh50 == ((max2 + max3)/2))
 {
     x3_50 = 485 ; 
     y3_50 = 432 ;
 }
 else if(thigh50 < ((max2 + max3)/2) && thigh50 > max3)
 {
     x3_50 = 463 ; 
     y3_50 = 416 ;
 }
 else if(thigh50 == max3)
 {
     x3_50 = 450 ; 
     y3_50 = 405 ;
 }
 else if(thigh50 < max3 && thigh50 > ((max3 + max4)/2))
 {
     x3_50 = 440 ; 
     y3_50 = 398 ;
 }
 else if(thigh50 == ((max3 + max4)/2))
 {
     x3_50 = 420 ; 
     y3_50 = 383 ;
 }
 else if(thigh50 < ((max3 + max4)/2) && thigh50 > max4)
 {
     x3_50 = 395 ; 
     y3_50 = 370 ;
 }
 else if(thigh50 == max4)
 {
     x3_50 = 385 ; 
     y3_50 = 363 ;
     
 }
 else if(thigh50 < max4 && thigh50 > (max4 / 2))
 {
     x3_50 = 380 ; 
     y3_50 = 360 ;
 }
 else if(thigh50 <= (max4 / 2))
 {
     x3_50 = 370 ; 
     y3_50 = 355 ;
 }
 
 
 // Plot illiac50      
 var x4_50 ; var y4_50 ;
 if(illiac50 >= max1)
 {
     x4_50 = 63 ; 
     y4_50 = 500 ;
 }
 else if(illiac50 < max1 && illiac50 > ((max1 + max2)/2))
 {
     x4_50 = 68 ; 
     y4_50 = 495 ;
 }
 else if(illiac50 == ((max1 + max2)/2))
 {
     x4_50 = 90 ; 
     y4_50 = 480 ;
 }
 else if(illiac50 < ((max1 + max2)/2) && illiac50 > max2)
 {
     x4_50 = 113 ; 
     y4_50 = 463 ;
 }
 else if(illiac50 == max2)
 {
     x4_50 = 128 ; 
     y4_50 = 453 ;
 }
 else if(illiac50 < max2 && illiac50 > ((max2 + max3)/2))
 {
     x4_50 = 133 ; 
     y4_50 = 450 ;
 }
 else if(illiac50 == ((max2 + max3)/2))
 {
     x4_50 = 155 ; 
     y4_50 = 435 ;
 }
 else if(illiac50 < ((max2 + max3)/2) && illiac50 > max3)
 {
     x4_50 = 180 ; 
     y4_50 = 418 ;
 }
 else if(illiac50 == max3)
 {
     x4_50 = 192 ; 
     y4_50 = 408 ;
 }
 else if(illiac50 < max3 && illiac50 > ((max3 + max4)/2))
 {
     x4_50 = 203 ; 
     y4_50 = 400 ;
 }
 else if(illiac50 == ((max3 + max4)/2))
 {
     x4_50 = 225 ; 
     y4_50 = 385 ;
 }
 else if(illiac50 < ((max3 + max4)/2) && illiac50 > max4)
 {
     x4_50 = 245 ; 
     y4_50 = 372 ;
 }
 else if(illiac50 == max4)
 {
     x4_50 = 255 ; 
     y4_50 = 365 ;
     
 }
 else if(illiac50 < max4 && illiac50 > (max4 / 2))
 {
     x4_50 = 260 ; 
     y4_50 = 360 ;
 }
 else if(illiac50 <= (max4 / 2))
 {
     x4_50 = 368 ; 
     y4_50 = 355 ;
 }
 
 
 // Plot tricep10       
 var x1_10 ; var y1_10 ;
 if(tricep10 >= max1)
 {
     x1_10 = 50 ; 
     y1_10 = 150 ;
 }
 else if(tricep10 < max1 && tricep10 > ((max1 + max2)/2))
 {
     x1_10 = 60 ; 
     y1_10 = 153 ;
 }
 else if(tricep10 == ((max1 + max2)/2))
 {
     x1_10 = 85 ; 
     y1_10 = 170 ;
 }
 else if(tricep10 < ((max1 + max2)/2) && tricep10 > max2)
 {
     x1_10 = 110 ; 
     y1_10 = 185 ;
 }
 else if(tricep10 == max2)
 {
     x1_10 = 115 ; 
     y1_10 = 190 ;
 }
 else if(tricep10 < max2 && tricep10 > ((max2 + max3)/2))
 {
     x1_10 = 130 ; 
     y1_10 = 200 ;
 }
 else if(tricep10 == ((max2 + max3)/2))
 {
     x1_10 = 155 ; 
     y1_10 = 220 ;
 }
 else if(tricep10 < ((max2 + max3)/2) && tricep10 > max3)
 {
     x1_10 = 175 ; 
     y1_10 = 230 ;
 }
 else if(tricep10 == max3)
 {
     x1_10 = 185 ; 
     y1_10 = 235 ;
 }
 else if(tricep10 < max3 && tricep10 > ((max3 + max4)/2))
 {
     x1_10 = 200 ; 
     y1_10 = 245 ;
 }
 else if(tricep10 == ((max3 + max4)/2))
 {
     x1_10 = 225 ; 
     y1_10 = 260 ;
 }
 else if(tricep10 < ((max3 + max4)/2) && tricep10 > max4)
 {
     x1_10 = 245 ; 
     y1_10 = 273 ;
 }
 else if(tricep10 == max4)
 {
     x1_10 = 250 ; 
     y1_10 = 275 ;
     
 }
 else if(tricep10 < max4 && tricep10 > (max4 / 2))
 {
     x1_10 = 255 ; 
     y1_10 = 282 ;
 }
 else if(tricep10 <= (max4 / 2))
 {
     x1_10 = 265 ; 
     y1_10 = 285 ;
 }


 // Plot subscap10       
 var x2_10 ; var y2_10 ;
 if(subscap10 >= max1)
 {
     x2_10 = 583 ; 
     y2_10 = 143 ;
 }
 else if(subscap10 < max1 && subscap10 > ((max1 + max2)/2))
 {
     x2_10 = 570 ; 
     y2_10 = 150 ;
 }
 else if(subscap10 == ((max1 + max2)/2))
 {
     x2_10 = 555 ; 
     y2_10 = 160 ;
 }
 else if(subscap10 < ((max1 + max2)/2) && subscap10 > max2)
 {
     x2_10 = 530 ; 
     y2_10 = 178 ;
 }
 else if(subscap10 == max2)
 {
     x2_10 = 520 ; 
     y2_10 = 186 ;
 }
 else if(subscap10 < max2 && subscap10 > ((max2 + max3)/2))
 {
     x2_10 = 508 ; 
     y2_10 = 193 ;
 }
 else if(subscap10 == ((max2 + max3)/2))
 {
     x2_10 = 485 ; 
     y2_10 = 210 ;
 }
 else if(subscap10 < ((max2 + max3)/2) && subscap10 > max3)
 {
     x2_10 = 465 ; 
     y2_10 = 223 ;
 }
 else if(subscap10 == max3)
 {
     x2_10 = 453 ; 
     y2_10 = 232 ;
 }
 else if(subscap10 < max3 && subscap10 > ((max3 + max4)/2))
 {
     x2_10 = 440 ; 
     y2_10 = 240 ;
 }
 else if(subscap10 == ((max3 + max4)/2))
 {
     x2_10 = 415 ; 
     y2_10 = 255 ;
 }
 else if(subscap10 < ((max3 + max4)/2) && subscap10 > max4)
 {
     x2_10 = 395 ; 
     y2_10 = 270 ;
 }
 else if(subscap10 == max4)
 {
     x2_10 = 385 ; 
     y2_10 = 278 ;
     
 }
 else if(subscap10 < max4 && subscap10 > (max4 / 2))
 {
     x2_10 = 380 ; 
     y2_10 = 280 ;
 }
 else if(subscap10 <= (max4 / 2))
 {
     x2_10 = 375 ; 
     y2_10 = 285 ;
 }
 
 // Plot thigh10      
 var x3_10 ; var y3_10 ;
 if(thigh10 >= max1)
 {
     x3_10 = 580 ; 
     y3_10 = 495 ;
 }
 else if(thigh10 < max1 && thigh10 > ((max1 + max2)/2))
 {
     x3_10 = 573 ; 
     y3_10 = 492 ;
 }
 else if(thigh10 == ((max1 + max2)/2))
 {
     x3_10 = 555 ; 
     y3_10 = 478 ;
 }
 else if(thigh10 < ((max1 + max2)/2) && thigh10 > max2)
 {
     x3_10 = 530 ; 
     y3_10 = 465 ;
 }
 else if(thigh10 == max2)
 {
     x3_10 = 515 ; 
     y3_10 = 453 ;
 }
 else if(thigh10 < max2 && thigh10 > ((max2 + max3)/2))
 {
     x3_10 = 505 ; 
     y3_10 = 446 ;
 }
 else if(thigh10 == ((max2 + max3)/2))
 {
     x3_10 = 485 ; 
     y3_10 = 432 ;
 }
 else if(thigh10 < ((max2 + max3)/2) && thigh10 > max3)
 {
     x3_10 = 463 ; 
     y3_10 = 416 ;
 }
 else if(thigh10 == max3)
 {
     x3_10 = 450 ; 
     y3_10 = 405 ;
 }
 else if(thigh10 < max3 && thigh10 > ((max3 + max4)/2))
 {
     x3_10 = 440 ; 
     y3_10 = 398 ;
 }
 else if(thigh10 == ((max3 + max4)/2))
 {
     x3_10 = 420 ; 
     y3_10 = 383 ;
 }
 else if(thigh10 < ((max3 + max4)/2) && thigh10 > max4)
 {
     x3_10 = 395 ; 
     y3_10 = 370 ;
 }
 else if(thigh10 == max4)
 {
     x3_10 = 385 ; 
     y3_10 = 363 ;
     
 }
 else if(thigh10 < max4 && thigh10 > (max4 / 2))
 {
     x3_10 = 380 ; 
     y3_10 = 360 ;
 }
 else if(thigh10 <= (max4 / 2))
 {
     x3_10 = 370 ; 
     y3_10 = 355 ;
 }
 
 
 // Plot illiac10      
 var x4_10 ; var y4_10 ;
 if(illiac10 >= max1)
 {
     x4_10 = 63 ; 
     y4_10 = 500 ;
 }
 else if(illiac10 < max1 && illiac10 > ((max1 + max2)/2))
 {
     x4_10 = 68 ; 
     y4_10 = 495 ;
 }
 else if(illiac10 == ((max1 + max2)/2))
 {
     x4_10 = 90 ; 
     y4_10 = 480 ;
 }
 else if(illiac10 < ((max1 + max2)/2) && illiac10 > max2)
 {
     x4_10 = 113 ; 
     y4_10 = 463 ;
 }
 else if(illiac10 == max2)
 {
     x4_10 = 128 ; 
     y4_10 = 453 ;
 }
 else if(illiac10 < max2 && illiac10 > ((max2 + max3)/2))
 {
     x4_10 = 133 ; 
     y4_10 = 450 ;
 }
 else if(illiac10 == ((max2 + max3)/2))
 {
     x4_10 = 155 ; 
     y4_10 = 435 ;
 }
 else if(illiac10 < ((max2 + max3)/2) && illiac10 > max3)
 {
     x4_10 = 180 ; 
     y4_10 = 418 ;
 }
 else if(illiac10 == max3)
 {
     x4_10 = 192 ; 
     y4_10 = 408 ;
 }
 else if(illiac10 < max3 && illiac10 > ((max3 + max4)/2))
 {
     x4_10 = 203 ; 
     y4_10 = 400 ;
 }
 else if(illiac10 == ((max3 + max4)/2))
 {
     x4_10 = 225 ; 
     y4_10 = 385 ;
 }
 else if(illiac10 < ((max3 + max4)/2) && illiac10 > max4)
 {
     x4_10 = 245 ; 
     y4_10 = 372 ;
 }
 else if(illiac10 == max4)
 {
     x4_10 = 255 ; 
     y4_10 = 365 ;
     
 }
 else if(illiac10 < max4 && illiac10 > (max4 / 2))
 {
     x4_10 = 260 ; 
     y4_10 = 360 ;
 }
 else if(illiac10 <= (max4 / 2))
 {
     x4_10 = 368 ; 
     y4_10 = 355 ;
 } 
 
 
       
 // Plot tricepSF       
 var x1 ; var y1 ;
 if(tricepSF >= max1)
 {
     x1 = 50 ; 
     y1 = 150 ;
 }
 else if(tricepSF < max1 && tricepSF > ((max1 + max2)/2))
 {
     x1 = 60 ; 
     y1 = 153 ;
 }
 else if(tricepSF == ((max1 + max2)/2))
 {
     x1 = 85 ; 
     y1 = 170 ;
 }
 else if(tricepSF < ((max1 + max2)/2) && tricepSF > max2)
 {
     x1 = 110 ; 
     y1 = 185 ;
 }
 else if(tricepSF == max2)
 {
     x1 = 115 ; 
     y1 = 190 ;
 }
 else if(tricepSF < max2 && tricepSF > ((max2 + max3)/2))
 {
     x1 = 130 ; 
     y1 = 200 ;
 }
 else if(tricepSF == ((max2 + max3)/2))
 {
     x1 = 155 ; 
     y1 = 220 ;
 }
 else if(tricepSF < ((max2 + max3)/2) && tricepSF > max3)
 {
     x1 = 175 ; 
     y1 = 230 ;
 }
 else if(tricepSF == max3)
 {
     x1 = 185 ; 
     y1 = 235 ;
 }
 else if(tricepSF < max3 && tricepSF > ((max3 + max4)/2))
 {
     x1 = 200 ; 
     y1 = 245 ;
 }
 else if(tricepSF == ((max3 + max4)/2))
 {
     x1 = 225 ; 
     y1 = 260 ;
 }
 else if(tricepSF < ((max3 + max4)/2) && tricepSF > max4)
 {
     x1 = 245 ; 
     y1 = 273 ;
 }
 else if(tricepSF == max4)
 {
     x1 = 250 ; 
     y1 = 275 ;
     
 }
 else if(tricepSF < max4 && tricepSF > (max4 / 2))
 {
     x1 = 255 ; 
     y1 = 282 ;
 }
 else if(tricepSF <= (max4 / 2))
 {
     x1 = 265 ; 
     y1 = 285 ;
 }


 // Plot subscapularSF       
 var x2 ; var y2 ;
 if(subscapularSF >= max1)
 {
     x2 = 583 ; 
     y2 = 143 ;
 }
 else if(subscapularSF < max1 && subscapularSF > ((max1 + max2)/2))
 {
     x2 = 570 ; 
     y2 = 150 ;
 }
 else if(subscapularSF == ((max1 + max2)/2))
 {
     x2 = 555 ; 
     y2 = 160 ;
 }
 else if(subscapularSF < ((max1 + max2)/2) && subscapularSF > max2)
 {
     x2 = 530 ; 
     y2 = 178 ;
 }
 else if(subscapularSF == max2)
 {
     x2 = 520 ; 
     y2 = 186 ;
 }
 else if(subscapularSF < max2 && subscapularSF > ((max2 + max3)/2))
 {
     x2 = 508 ; 
     y2 = 193 ;
 }
 else if(subscapularSF == ((max2 + max3)/2))
 {
     x2 = 485 ; 
     y2 = 210 ;
 }
 else if(subscapularSF < ((max2 + max3)/2) && subscapularSF > max3)
 {
     x2 = 465 ; 
     y2 = 223 ;
 }
 else if(subscapularSF == max3)
 {
     x2 = 453 ; 
     y2 = 232 ;
 }
 else if(subscapularSF < max3 && subscapularSF > ((max3 + max4)/2))
 {
     x2 = 440 ; 
     y2 = 240 ;
 }
 else if(subscapularSF == ((max3 + max4)/2))
 {
     x2 = 415 ; 
     y2 = 255 ;
 }
 else if(subscapularSF < ((max3 + max4)/2) && subscapularSF > max4)
 {
     x2 = 395 ; 
     y2 = 270 ;
 }
 else if(subscapularSF == max4)
 {
     x2 = 385 ; 
     y2 = 278 ;
     
 }
 else if(subscapularSF < max4 && subscapularSF > (max4 / 2))
 {
     x2 = 380 ; 
     y2 = 280 ;
 }
 else if(subscapularSF <= (max4 / 2))
 {
     x2 = 375 ; 
     y2 = 285 ;
 }
 
 // Plot thighSF       
 var x3 ; var y3 ;
 if(thighSF >= max1)
 {
     x3 = 580 ; 
     y3 = 495 ;
 }
 else if(thighSF < max1 && thighSF > ((max1 + max2)/2))
 {
     x3 = 573 ; 
     y3 = 492 ;
 }
 else if(thighSF == ((max1 + max2)/2))
 {
     x3 = 555 ; 
     y3 = 478 ;
 }
 else if(thighSF < ((max1 + max2)/2) && thighSF > max2)
 {
     x3 = 530 ; 
     y3 = 465 ;
 }
 else if(thighSF == max2)
 {
     x3 = 515 ; 
     y3 = 453 ;
 }
 else if(thighSF < max2 && thighSF > ((max2 + max3)/2))
 {
     x3 = 505 ; 
     y3 = 446 ;
 }
 else if(thighSF == ((max2 + max3)/2))
 {
     x3 = 485 ; 
     y3 = 432 ;
 }
 else if(thighSF < ((max2 + max3)/2) && thighSF > max3)
 {
     x3 = 463 ; 
     y3 = 416 ;
 }
 else if(thighSF == max3)
 {
     x3 = 450 ; 
     y3 = 405 ;
 }
 else if(thighSF < max3 && thighSF > ((max3 + max4)/2))
 {
     x3 = 440 ; 
     y3 = 398 ;
 }
 else if(thighSF == ((max3 + max4)/2))
 {
     x3 = 420 ; 
     y3 = 383 ;
 }
 else if(thighSF < ((max3 + max4)/2) && thighSF > max4)
 {
     x3 = 395 ; 
     y3 = 370 ;
 }
 else if(thighSF == max4)
 {
     x3 = 385 ; 
     y3 = 363 ;
     
 }
 else if(thighSF < max4 && thighSF > (max4 / 2))
 {
     x3 = 380 ; 
     y3 = 360 ;
 }
 else if(thighSF <= (max4 / 2))
 {
     x3 = 370 ; 
     y3 = 355 ;
 }
 
 
 // Plot illiacSF       
 var x4 ; var y4 ;
 if(illiacSF >= max1)
 {
     x4 = 63 ; 
     y4 = 500 ;
 }
 else if(illiacSF < max1 && illiacSF > ((max1 + max2)/2))
 {
     x4 = 68 ; 
     y4 = 495 ;
 }
 else if(illiacSF == ((max1 + max2)/2))
 {
     x4 = 90 ; 
     y4 = 480 ;
 }
 else if(illiacSF < ((max1 + max2)/2) && illiacSF > max2)
 {
     x4 = 113 ; 
     y4 = 463 ;
 }
 else if(illiacSF == max2)
 {
     x4 = 128 ; 
     y4 = 453 ;
 }
 else if(illiacSF < max2 && illiacSF > ((max2 + max3)/2))
 {
     x4 = 133 ; 
     y4 = 450 ;
 }
 else if(illiacSF == ((max2 + max3)/2))
 {
     x4 = 155 ; 
     y4 = 435 ;
 }
 else if(illiacSF < ((max2 + max3)/2) && illiacSF > max3)
 {
     x4 = 180 ; 
     y4 = 418 ;
 }
 else if(illiacSF == max3)
 {
     x4 = 192 ; 
     y4 = 408 ;
 }
 else if(illiacSF < max3 && illiacSF > ((max3 + max4)/2))
 {
     x4 = 203 ; 
     y4 = 400 ;
 }
 else if(illiacSF == ((max3 + max4)/2))
 {
     x4 = 225 ; 
     y4 = 385 ;
 }
 else if(illiacSF < ((max3 + max4)/2) && illiacSF > max4)
 {
     x4 = 245 ; 
     y4 = 372 ;
 }
 else if(illiacSF == max4)
 {
     x4 = 255 ; 
     y4 = 365 ;
     
 }
 else if(illiacSF < max4 && illiacSF > (max4 / 2))
 {
     x4 = 260 ; 
     y4 = 360 ;
 }
 else if(illiacSF <= (max4 / 2))
 {
     x4 = 368 ; 
     y4 = 355 ;
 }


    $('#myCanvas').drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_90, y1: y1_90,
      x2: x2_90, y2: y2_90,
      x3: x3_90, y3: y3_90,
      x4: x4_90, y4: y4_90,
      x5: x1_90, y5: y1_90
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_50, y1: y1_50,
      x2: x2_50, y2: y2_50,
      x3: x3_50, y3: y3_50,
      x4: x4_50, y4: y4_50,
      x5: x1_50, y5: y1_50
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_10, y1: y1_10,
      x2: x2_10, y2: y2_10,
      x3: x3_10, y3: y3_10,
      x4: x4_10, y4: y4_10,
      x5: x1_10, y5: y1_10
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 4,   
      x1: x1, y1: y1, 
      x2: x2, y2: y2,
      x3: x3, y3: y3,
      x4: x4, y4: y4,
      x5: x1, y5: y1
    }); 
}

</script>
<script type="text/javascript">
        $(document).on('click','#exit', function(){
            document.forms["myform"].submit();
            //return false;
        });	
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ;   
        });
	
	$(document).on('click','.info_icon_btn', function(){
	    $(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
</script>  

</body>
</html>
