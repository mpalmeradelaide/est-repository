<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script src="https://raw.githubusercontent.com/caleb531/jcanvas/master/jcanvas.js"></script>

</head>

<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php 
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""></a>
                 <div class="print_icon">
                    <a class="strength_btns print_icon_toggle_btn"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""></a>
                    <ul class="print_icon_toggle">                                      
                        <li><form class="form_print"><a onclick="window.print(); return false;">Print Report</a></form></li>                        
                        <li><form class="form_print"><a href="#">E-mail Report</a></form></li>
                    </ul>
                </div>   
                <h1 class="page_head">Skinfold analysis</h1>        
              </td>
            </tr>                   
            
            <tr>
              <td colspan="3">	
              		<div class="skinfold_container">
                    	<div class="skmap_left">skinfold map</div>
                        <div class="skmap_right">
                        	<div class="skmap_rtxt1"><img src="<?php echo "$base/$image"?>/bold_line.jpg" alt=""> Lynda NORTON 34 yr</div>
                            <div class="skmap_rtxt2"><img src="<?php echo "$base/$image"?>/lite_line.jpg" alt=""> population, 30-39 yr<br>[10, 50,90 %'ile]</div>
                        </div>
                    </div>
              		<div class="lrg_circle">
                    	<div class="lrg_circle_in">
                        	<div class="lrg_circle_inner">
                            	<div class="lrg_circle_under">&nbsp;</div>
                            </div>
                        </div>
                        
                        <div class="sk_txt1">triceps</div>
                        <div class="sk_txt2">subscapular</div>
                        <div class="sk_txt3">front thigh</div>
                        <div class="sk_txt4">iliac crest</div>
                        
                        
                        <div class="hori_content">
                        	<ul>
                            	<li>44.9</li>
                                <li>33.7</li>
                                <li>22.5</li>
                                <li>11.6</li>
                                <li>11.6</li>
                                <li>22.5</li>
                                <li>33.7</li>
                                <li>44.9</li> 
                            </ul>
                        </div>
                        
                        <div class="vert_content">
                        	<ul>
                            	<li>44.9</li>
                                <li>33.7</li>
                                <li>22.5</li>
                                <li>11.6</li>
                                <li>11.6</li>
                                <li>22.5</li>
                                <li>33.7</li>
                                <li>44.9</li>
                            </ul>
                        </div>
                        
                        <canvas id="myCanvas" width="640" height="640" style="position: absolute; top: 0; left:0;">&nbsp;</canvas>
                    </div>
                    
              </td>        
            </tr>             	
            
            <tr>
              <td align="right" colspan="3" valign="bottom">
              	    <button class="number_btn">1</button>
                    <button class="number_btn">2</button>
                    <button class="number_btn">3</button>
                    <button class="number_btn">4</button>
                    <button class="number_btn">5</button>
                    <button class="number_btn">6</button>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>

             <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>

<script src="<?php echo "$base/assets/js/"?>jCanvas.js"></script>
<script>
$('#myCanvas').drawLine({
  strokeStyle: '#000',   
  strokeWidth: 1,
  x1: 80, y1: 160,
  x2: 90, y2: 480,
  x3: 570, y3: 495,
  x4: 580, y4: 140,
  x5: 80, y5: 160,
}).drawLine({
  strokeStyle: '#000',   
  strokeWidth: 1,
  x1: 160, y1: 220,
  x2: 175, y2: 420,
  x3: 500, y3: 440,
  x4: 480, y4: 200,
  x5: 160, y5: 220, 
}).drawLine({
  strokeStyle: '#000',   
  strokeWidth: 1,
  x1: 240, y1: 270,
  x2: 280, y2: 360,
  x3: 410, y3: 380,
  x4: 385, y4: 280, 
  x5: 240, y5: 270, 
}).drawLine({
  strokeStyle: '#000',   
  strokeWidth: 4,
  x1: 238, y1: 270,
  x2: 270, y2: 365,
  x3: 515, y3: 450,
  x4: 380, y4: 287, 
  x5: 240, y5: 270, 
}); 
</script>0
<script type="text/javascript">

        $(document).on('click','#exit', function(){
            document.forms["myform"].submit();
            //return false;
        });	
        
         $(document).on('click','#four_Sites', function(){
            document.getElementById("sumSites").value = "4" ; 
            document.getElementById("sum4").setAttribute('style', ""); 
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#five_Sites', function(){
            document.getElementById("sumSites").value = "5" ; 
            document.getElementById("sum5").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#six_Sites', function(){
            document.getElementById("sumSites").value = "6" ; 
            document.getElementById("sum6").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#seven_Sites', function(){
            document.getElementById("sumSites").value = "7" ; 
            document.getElementById("sum7").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ; 
            document.getElementById("sum8").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ;   
        });
	
	$(document).on('click','.info_icon_btn', function(){
	    $(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
</script>  



<script>   
//calculate %Body Fat value
    function getSumSkinfolds()
    { 
        var ageValue = document.getElementById("age").value;      
      
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        
        var sumSites = document.getElementById("sumSites").value ;
  
  
            if(parseInt(sumSites) == 4)
            {
                 var sum4Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ; 
                 var sumSquare4Skinfold = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;   

                 if(document.getElementById('gender').value == "Male")
                 {    
                   var percentFat = (0.29288 * sum4Skinfold) - (0.0005 * sumSquare4Skinfold) +  (0.15845 * (parseInt(ageValue))) + 5.76377 ;
                   percentFat = Math.round(parseFloat(percentFat) * 10) / 10 ;
                 }
                 else if(document.getElementById('gender').value == "Female") 
                 {
                   var percentFat = (0.29669 * sum4Skinfold) - (0.00043 * sumSquare4Skinfold) + (0.02963 * (parseInt(ageValue))) + 1.4072 ;                
                 }

                 percentFat = Math.round(percentFat * 10) / 10 ;
            }  
            
            document.getElementById("percentile").value = parseFloat(percentFat) ;   

    }	  
 
  

    
</script>
    
</body>
</html>
