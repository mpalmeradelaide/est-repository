<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
</head>

<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
    <div class="mid">       
        <a style="margin: 20px 0 0 20px;" onclick="window.location.href = '<?php  echo site_url('/'); ?>';" class="strength_btns" id="exit" style="margin-left: 17px; margin-top: 27px; font: 15px 'MYRIADPROREGULAR'; text-align: center;">
            <img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p>
        </a>
        <div class="drop_main">
        	<ul>
            	<li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile">Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile">Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis">Error Analysis</a></li>
            </ul>
        </div>
        <div class="clear">&nbsp;</div>
    </div>
</div>
</body>
</html>
