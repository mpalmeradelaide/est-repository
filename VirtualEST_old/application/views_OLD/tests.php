<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").slideUp();
		$(".inner_sub_menu").hide();
		$(this).next(".sub_menu").slideDown();		
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").slideDown();		
	});
</script>
</head>

<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$health_logo"?>" width="540" height="65" alt="Health Screen Logo"></div>
    <div class="mid">
	<a style="margin: 20px 0 0 20px;" href="<?php echo site_url('/');?>" class="strength_btns"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
        <div class="drop_main">
        	<ul>
            	<li><a href="#" id="anaerobic_strength">Anaerobic strength and power</a>
                	<ul class="sub_menu">
                    	<li><a href="<?php echo site_url('Fitness/vertical_jump'); ?>">vertical jump [cm]</a></li>
                        <li><a href="<?php echo site_url('Fitness/flight_time'); ?>">flight:contact time</a></li>
                        <li><a href="<?php echo site_url('Fitness/peak_power'); ?>">peak power [W]</a></li>
                        <li><a href="<?php echo site_url('Fitness/strength'); ?>">strength tests</a></li>
                    </ul>
                </li>
                <li><a href="#" id="anaerobic_capacity">Anaerobic capacity</a>
                	<ul class="sub_menu">
                    	  <li><a href="<?php echo site_url('Fitness/total_work_done_30s'); ?>">30 s total work [kJ]</a></li>
                        </ul>
                </li>
                <li><a href="#" id="aerobic_fitness">Aerobic fitness</a>
                	<ul class="sub_menu">
                    	<li><a href="#" id="VO2max">VO<sub>2max</sub><br/>[mL/kg/min]</a>
                        	<ul class="inner_sub_menu">
                                    <li><a href="<?php echo site_url('Fitness/submaximal_test'); ?>">3 x 3 min submaximal test</a></li>
                                    <li><a href="<?php echo site_url('Fitness/shuttle_test'); ?>">20 m shuttle test</a></li>
                                    <li><a href="<?php echo site_url('Fitness/v02max_test'); ?>">measured VO<sub>2max</sub></a></li>
                                </ul>
                        </li>
                        <li><a href="<?php echo site_url('Fitness/lactate_threshold'); ?>">Lactate threshold</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <div class="clear">&nbsp;</div>
    </div>
</div>
</body>
</html>
