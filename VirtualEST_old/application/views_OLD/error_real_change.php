<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/saveClientErrorRealChangeInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                  <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>                  
                <h1 class="page_head">ERROR ANALYSIS</h1> 
              </td>
              </tr>
            
            <tr>
            	<td width="29%" valign="top">
                    <div class="text_list">
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis'); ?>';" id="tem" name="error_radio" value="tem"> Calculate TEM from raw data</p>
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis_confidence'); ?>';" id="single_measure" name="error_radio" value="single_measure"> 95% CI around a single measure</p>
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_real_change'); ?>';" id="real_change" name="error_radio" value="real_change" checked="checked"> Has a real change ocurred?</p>
                    </div>
                    
                    <div class="note_text"> 
                    	<strong>notes</strong>
                        <p>Enter a first raw measurement, a second raw measurement and a %TEM value to see if a real change has occurred between the repeat measurements.</p>
                    </div>
                </td>
                <td width="46%" class="error_select">
                	<table width="100%" class="tabel_bord error_conf_table" cellpadding="0" cellspacing="0">
                      <tbody>
                        <tr>
                          <td width="70%">Enter the first trial score</td>
                          <td><input type="text" id="first_score" name="first_score" value="<?php echo isset($fieldData[0]->first_score)?$fieldData[0]->first_score:""; ?>" class="cus-input" autofocus></td>
                        </tr>
                        <tr>
                          <td height="80px">&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td width="70%">Enter the second trial score</td>
                          <td><input type="text" id="second_score" name="second_score" value="<?php echo isset($fieldData[0]->second_score)?$fieldData[0]->second_score:""; ?>" class="cus-input"  tabindex="0"></td>
                        </tr>
                        <tr>
                          <td height="80px">&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>Enter your %TEM</td>
                          <td><input type="text" id="tem_percent" name="tem_percent" value="<?php echo isset($fieldData[0]->tem_percent)?$fieldData[0]->tem_percent:""; ?>" class="cus-input"></td>
                        </tr>
                        <tr>
                          <td height="80px">&nbsp;</td>
                          <td>&nbsp;</td>
                        </tr>
                        <tr>
                          <td>The calculated 95% CI is</td>
                          <td><input type="text" id="interval" name="interval" value="<?php echo isset($fieldData[0]->interval)?$fieldData[0]->interval:"****_****"; ?>" readonly="readonly"></td>
                        </tr>
                      </tbody>
                    </table>
                  
                </td>
                <td width="25%" align="bottom">                	
                    <button id="compute" name="compute" class="plot_btn compute_btn" style="margin-top: 390px;">Compute</button>
                </td>
            </tr>            
            
            <tr>
              <td align="center" colspan="3">                  
                  <div id="answer" style="color:#da5456;">&nbsp;</div>
              </td>        
            </tr>
          </tbody>
        </table>
    </div>

       <input type="hidden" id="exit_key" name="exit_key" value="">

     <?php echo form_close(); ?>
<!-- Form ends -->    
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

        $(document).on('click','#exit', function(){     
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#tem', function(){
          document.getElementById("exit_key").value = "tem" ;
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#single_measure', function(){ 
          document.getElementById("exit_key").value = "single_measure" ;  
          document.forms["myform"].submit();
        //return false;
        });
        
        $(document).on('click','#real_change', function(){ 
          document.getElementById("exit_key").value = "real_change" ;  
          document.forms["myform"].submit();
        //return false;
        });
        
         document.getElementById("compute").addEventListener("click", function(event){
            event.preventDefault() ;
        });  
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
               
    // Check for Single raw value & %TEM

        $(document).on('click','#compute', function(){
            
             if(document.getElementById("first_score").value == "" || document.getElementById("second_score").value == "" || document.getElementById("tem_percent").value == "")
               {
                  if(document.getElementById("first_score").value == "")
                  {
                      alert("Please enter the first trial score") ;
                  }
                  else if(document.getElementById("second_score").value == "")
                  {
                      alert("Please enter the second trial score") ;
                  }
                  else if(document.getElementById("tem_percent").value == "")
                  {
                      alert("Please enter your %TEM") ;
                  }
                  
               }
             else
               {
                  get_CI() ;
               }
            
        });


  // TEM calculation with 2 trial Columns
    function get_CI()
    {
       var value1 = document.getElementById("first_score").value ;
       var value2 = document.getElementById("second_score").value ;
       var tem_percent = document.getElementById("tem_percent").value ;
       
       var difference = parseFloat(value1) - parseFloat(value2) ;       
       var SE = (parseFloat(tem_percent)) * (Math.sqrt(2)) ;
       
       var Upper_confidence = (difference + 2 * SE / 100 * (parseFloat(value1) + parseFloat(value2)) / 2) ;
       var Lower_confidence = (difference - 2 * SE / 100 * (parseFloat(value1) + parseFloat(value2)) / 2) ;

        
       var upper = Math.round(Upper_confidence * 10) / 10 ; 
       var lower = Math.round(Lower_confidence * 10) / 10 ; 
       
       if(lower <= 0 && upper >= 0)
       {
         document.getElementById("interval").value = lower+" _ "+upper ;                  
         document.getElementById("answer").innerHTML = "Since the confidence interval is between "+lower+" _ "+upper+" and includes zero then we cannot be sure a real change has occurred" ;   
       }
       else if((lower < 0 && upper < 0) || (lower > 0 && upper > 0))
       {
         document.getElementById("interval").value = lower+" _ "+upper ;  
         document.getElementById("answer").innerHTML = "Since the confidence interval is between "+lower+" _ "+upper+" and does not include zero then we are 95% sure a real change has occurred" ;     
       }       
          
    }
        
</script>    
</body>
</html>
