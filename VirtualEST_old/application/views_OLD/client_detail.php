<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>Client Details</title>
<link href='<?php echo "$base/$css" ;?>' rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<!--<link rel="stylesheet" type="text/css" href="http://www.vestaging.in/projects/health/assets/css/jquery.datetimepicker.css"/>-->
<!--<link rel="stylesheet" type="text/css" href="http://115.112.129.194:8083/~iphonedev/health/assets/css/jquery.datetimepicker.css"/> -->
<link rel="stylesheet" type="text/css" href="http://fitnessriskmanagement.com.au/screening-tool/assets/css/jquery.datetimepicker.css"/>
<script type="text/javascript" src="<?php echo base_url() . 'assets/'; ?>js/jquery.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url() . 'assets/'; ?>js/jquery.datetimepicker.js"></script>-->

<!-------------------------------------------------------------------------------DOB Start
<script type="text/javascript">
var monthtext=['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sept','Oct','Nov','Dec'];

function populatedropdown(dayfield, monthfield, yearfield){
var today=new Date()
var dayfield=document.getElementById(dayfield)
var monthfield=document.getElementById(monthfield)
var yearfield=document.getElementById(yearfield)
for (var i=1; i<=31; i++)
dayfield.options[i]=new Option(i, i+1)
dayfield.options[today.getDate()]=new Option(today.getDate(), today.getDate(), true, true) //select today's day
for (var m=0; m<12; m++)
monthfield.options[m]=new Option(monthtext[m], monthtext[m])
monthfield.options[today.getMonth()]=new Option(monthtext[today.getMonth()], monthtext[today.getMonth()], true, true) //select today's month
var thisyear=today.getFullYear()
for (var y=0; y<95; y++){
yearfield.options[y]=new Option(thisyear, thisyear)
thisyear-=1
}
yearfield.options[0]=new Option(today.getFullYear(), today.getFullYear(), true, true) //select today's year
}

</script>

<script type="text/javascript">

//populatedropdown(id_of_day_select, id_of_month_select, id_of_year_select)
window.onload=function(){
populatedropdown("daydropdown", "monthdropdown", "yeardropdown")
}
</script>-->
<!-------------------------------------------------------------------------------DOB End-->
<script>
  $(function(){ 
   
  
     gender = <?php $gender;  ?> 
            genderTest = gender.value
            
      if( genderTest == 'M' ){   
       $("#male").prop('checked', true);
      }
      
    });
</script>
<!--date picker start
 <link rel="stylesheet" href="/resources/demos/style.css">-->
 <!--date picker End--> 
 <style>
 .notenew{width:298px;height:100px;resize:none;font: 18px "MYRIADPROREGULAR" !important;}
 .mid2{width:100%;}
 .mid2_left{padding:4%; width:30%; background:none; height:auto;}
.mid2_right{width:52%;}
.buttons{width:100px; margin-left: 0; padding-top: 0; overflow:visible; display:inline-block; text-align:center;}
.buttons ul li{display:inline-block;}
.buttons ul li a{height: 48px; width: 48px; margin-right:0; margin-bottom:10px; transition:all .3s linear; background:#ff5a59; text-align:center; line-height:50px;}
.buttons ul li a:hover {box-shadow:inset 0 0 0 4px #C60003, 2px 2px 5px rgba(0, 0, 0, 0.37);}
.buttons ul li a img{max-width:90%;}
.mid2_left p {font: 17px "MYRIADPROREGULAR"; color: #8B8888; margin-top: 20px; line-height: 25px;}
.mid2_left h3{font: 20px "MYRIADPROREGULAR"; color: #898888;}
span input[type="submit"]{padding:0 11px; font-size:16px; width:auto; margin-right: 8px;}
span input[type="submit"].body_comp_btn{margin-right:0;}
.form-base{width:450px; height:auto; position:relative;}
.lsection{width:450px;}
.lsection span p{width:150px;}
.lsection span input[type="text"]{width:288px;}
.lsection span select{width:300px;}
input[type="reset"].btn_reset{position:absolute; top:-60px; right:0; background:#c60003 url(../../assets/images/reset_icon.png) center no-repeat !important; background-size: 42%!important; text-indent:-999px; height:35px; width:35px !important;}
.dob #daydropdown{width: 85px; background-color: #ddd !important;color:#000;}
.dob #monthdropdown{width: 95px; background-color: #ddd !important; color:#000;}
.dob #yeardropdown{width: 109px; background-color: #ddd !important;color:#000;}
.access_code_sec{width:100%; margin-top:8px;}
.access_code_sec span{width:270px; margin:auto;}
.access_code_sec span p{width:auto; margin-right:10px;}
.access_code_sec span input[type="text"]{width:120px; }
.mid_center{width:auto; display:block; padding: 4%}
.mid_center h3{font: 20px "MYRIADPROREGULAR"; color: #898888;}
.mid_center p{font: 17px "MYRIADPROREGULAR"; color: #8B8888; margin-top: 20px; line-height: 25px;}
.clearfix{clear:both;}
.footer{margin-top:10px;}
.fitness_btn{background: #c60003; height: 45px; border: none; display: block; cursor: pointer; margin: 40px 0 0 0; float: right; margin-left: 0px; font-family: "MYRIADPROREGULAR"; font-size: 24px; color: #FFF; text-align: center; text-decoration: none; line-height: 45px;}
 </style>
</head>

<body>
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>


 <!--Start Mid --> 
  <div class="mid2">
   <!--case form Start-->
  <div class="form-head">CLIENT DETAILS</div>
    <!--Start Buttons --> 
	<div class="mid2_left">
    <div class="buttons">
      <ul>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_1.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_2.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_3.PNG" width="140" height="140"></a></li>
      	<li><a href="#" class="last"><img src="<?php echo "$base/$image"?>/img_4.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_5.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_6.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_7.PNG" width="140" height="140"></a></li>
      	<li><a href="#" class="last"><img src="<?php echo "$base/$image"?>/img_8.PNG" width="140" height="140"></a></li>
      </ul>
    </div><!--End Buttons --> 
	<div style="margin: 0 auto; width:100%; text-align:center;">
		<!--<a href="#"><div class="quit-btn">Quit</div></a>-->			
				<div><?php echo $msg ;  ?></div>
				<div class="lsection access_code_sec" style="display:inline-block; width:auto; float:none; margin-top:20px;">
					<span style="margin:0;"><p>Access Code</p><input type="text" name="code" value="" style="width:150px;"></span>
				</div>	
	</div> <!--End Access Code box --> 
	
 </div>
	
    <div class="mid2_right">
   
  <div class="form-base">
      
      
   

     <?php 
  
     $attributes = array('id' => 'clientInfo');
       echo form_open('welcome/saveClientInfo',$attributes); ?>
      
      <div class="lsection">
      <div id="validate">
                <?php echo validation_errors(); ?>
      </div>
          
          <?php  $firstName =$firstName !='' ?$firstName:'';
        $lastName =$lastName===''?'':$lastName;
        $email =$email===''?'':$email;
        $p_desc =$p_desc===''?'':$p_desc;
        $gender =$gender==='M'?'M':$gender;
        $genderM =$gender==='M'?'Checked':'';
        $genderF =$gender==='F'?'Checked':'';
       
          $dobirth =$dob===''?'':$dob;
          $occupation =$occupation===''?'selectoccupation':$occupation;
          $country =$country===''?'select':$country;
          $p_name =$p_name===''?'select':$p_name;
          $daydropdown =$daydropdown===''?'select':$daydropdown;
          $monthdropdown =$monthdropdown===''?'select':$monthdropdown;
          $yeardropdown =$yeardropdown===''?'select':$yeardropdown;
         
                  
                  ?>
      <span><p>First name</p><?php echo form_input('first_name', $firstName,"class='cus-input'"); ?></span>
      <span><p>Last name</p><?php echo form_input('last_name', $lastName,"class='cus-input'"); ?></span>
      <span><p>Email</p><?php echo form_input('email', $email); ?></span>
      <span><p>Project</p><?php
               
           

              echo form_dropdown('p_name', $projects, $p_name);
      
        ?></span>
      <?php

        $dob = array(
                      'name'        => 'datetimepicker2',
       'id'        => 'datetimepicker2',
       'placeholder'=>"mm/dd/yyyy",
            'value'=>$dobirth, 
            'type'=>'numb',
            'class'=>'webDate'
            
                    );
      /* 
        $dob2 = array(
                      'name'        => 'datetimepicker2',
       'id'        => 'datetimepicker2',
       'placeholder'=>"mm/dd/yyyy",
            'value'=>$dobirth, 
           // 'type'=>'number',
            'class'=>'deviceDate'
                    );*/
       
        
         
       ?>
      <span><p class="gen">Gender</p><?php echo form_radio(array("name"=>"gender","id"=>"male","value"=>"M","checked"=> $genderM )); ?>&nbsp;Male&nbsp;&nbsp;&nbsp;<?php echo form_radio(array("name"=>"gender","id"=>"female","value"=>"F","checked"=> $genderF )); ?>&nbsp;Female</span>
   
      <span class="dob"><p>Date of birth</p>
      <span><?php
                $options = array(
                            'dd'  => 'DD',
							'01' => '01',  
							'02' => '02',  
							'03' => '03',  
							'04' => '04',  
							'05' => '05',  
							'06' => '06',  
							'07' => '07',  
							'08' => '08',  
							'09' => '09',  
							'10' => '10',  
							'11' => '11',  
							'12' => '12',  
							'13' => '13',  
							'14' => '14',  
							'15' => '15',  
							'16' => '16',  
							'17' => '17',  
							'18' => '18',  
							'19' => '19',  
							'20' => '20',  
							'21' => '21',  
							'22' => '22',  
							'23' => '23',  
							'24' => '24',  
							'25' => '25',  
							'26' => '26', 
							'27' => '27', 
							'28' => '28', 
							'29' => '29', 
							'30' => '30', 
							'31' => '31', 
							
							
                          );
						  $js = 'id="daydropdown" ';
              echo form_dropdown('daydropdown', $options, $daydropdown,'class="cus-input" id="daydropdown"', $js);
      
        ?></span>


<span><?php
                $options = array(
				'mm' => 'MM',
				'1' => 'JAN',
                '2' => 'FEB',
                '3' => 'MAR',
                '4' => 'APR',
                '5' => 'MAY',
                '6' => 'JUN',
                '7' => 'JUL',
                '8' => 'AUG',
                '9' => 'SEP',
                '10' => 'OCT',
                '11' => 'NOV',
                '12' => 'DEC');
				
				
			$js = 'id="monthdropdown" ';
              echo form_dropdown('monthdropdown', $options, $monthdropdown,'class="cus-input" id="monthdropdown"', $js);
      
        ?></span>
		

	
<span><?php
            //    $options = array('0' => 'YYYY','1910'=>'1910','1910'=>"1910", '1911'=>"1911", '1912'=>"1912", '1913'=>"1913", '1914'=>"1914", '1915'=>"1915", '1916'=>"1916", '1917'=>"1917", '1918'=>"1918", '1919'=>"1919", '1920'=>"1920", '1921'=>"1921", '1922'=>"1922", '1923'=>"1923", '1924'=>"1924", '1925'=>"1925", '1926'=>"1926", '1927'=>"1927", '1928'=>"1928", '1929'=>"1929", '1930'=>"1930", '1931'=>"1931", '1932'=>"1932", '1933'=>"1933", '1934'=>"1934", '1935'=>"1935", '1936'=>"1936", '1937'=>"1937", '1938'=>"1938", '1939'=>"1939", '1940'=>"1940", '1941'=>"1941", '1942'=>"1942", '1943'=>"1943", '1944'=>"1944", '1945'=>"1945", '1946'=>"1946", '1947'=>"1947", '1948'=>"1948", '1949'=>"1949", '1950'=>"1950", '1951'=>"1951", '1952'=>"1952", '1953'=>"1953", '1954'=>"1954", '1955'=>"1955", '1956'=>"1956", '1957'=>"1957", '1958'=>"1958", '1959'=>"1959", '1960'=>"1960", '1961'=>"1961", '1962'=>"1962", '1963'=>"1963", '1964'=>"1964", '1965'=>"1965", '1966'=>"1966", '1967'=>"1967", '1968'=>"1968", '1969'=>"1969", '1970'=>"1970", '1971'=>"1971", '1972'=>"1972", '1973'=>"1973", '1974'=>"1974", '1975'=>"1975", '1976'=>"1976", '1977'=>"1977", '1978'=>"1978", '1979'=>"1979", '1980'=>"1980", '1981'=>"1981", '1982'=>"1982", '1983'=>"1983", '1984'=>"1984", '1985'=>"1985", '1986'=>"1986", '1987'=>"1987", '1988'=>"1988", '1989'=>"1989", '1990'=>"1990", '1991'=>"1991", '1992'=>"1992", '1993'=>"1993", '1994'=>"1994", '1995'=>"1995", '1996'=>"1996", '1997'=>"1997", '1998'=>"1998", '1999'=>"1999", '2000'=>"2000", '2001'=>"2001", '2002'=>"2002", '2003'=>"2003", '2004'=>"2004", '2005'=>"2005", '2006'=>"2006", '2007'=>"2007", '2008'=>"2008", '2009'=>'2009', '2010'=>'2010', '2011'=>'2011', '2012'=>'2012', '2013'=>'2013', '2014'=>'2014');
				// rsort($options  , SORT_DESC);
		$options = array('0000' => 'YYYY','2010'=>'2010', '2009'=>'2009', '2008'=>'2008', '2007'=>'2007', '2006'=>'2006', '2005'=>'2005', '2004'=>'2004', '2003'=>'2003', '2002'=>'2002', '2001'=>'2001', '2000'=>'2000', '1999'=>'1999', '1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1963'=>'1963', '1964'=>'1964' , '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927', '1926'=>'1926', '1925'=>'1925', '1924'=>'1924', '1923'=>'1923', '1922'=>'1922', '1921'=>'1921', '1920'=>'1920', '1919'=>'1919', '1918'=>'1918', '1917'=>'1917', '1916'=>'1916', '1915'=>'1915', '1914'=>'1914', '1913'=>'1913', '1912'=>'1912', '1911'=>'1911', '1910'=>'1910', '1910'=>'1910');		
			$js = 'id="yeardropdown" ';
              echo form_dropdown('yeardropdown', $options, $yeardropdown,'class="cus-input" id="yeardropdown"', $js);
      
        ?></span>
      </span>
		
      
      
      <!--    
     <span class="dob"><p>Date of Birth</p>
     <select id="daydropdown"  name="daydropdown">
    		<option value='dd'>DD</option>
     		<option value='01'>01</option>
			<option value='02'>02</option>
			<option value='03'>03</option>
			<option value='04'>04</option>
			<option value='05'>05</option>
			<option value='06'>06</option>
			<option value='07'>07</option>
			<option value='08'>08</option>
			<option value='09'>09</option>
			<option value='10'>10</option>
			<option value='11'>11</option>
			<option value='12'>12</option>
			<option value='13'>13</option>
			<option value='14'>14</option>
			<option value='15'>15</option>
			<option value='16'>16</option>
			<option value='17'>17</option>
			<option value='18'>18</option>
			<option value='19'>19</option>
			<option value='20'>20</option>
			<option value='21'>21</option>
			<option value='22'>22</option>
			<option value='23'>23</option>
			<option value='24'>24</option>
			<option value='25'>25</option>
			<option value='26'>26</option>
			<option value='27'>27</option>
			<option value='28'>28</option>
			<option value='29'>29</option>
			<option value='30'>30</option>
			<option value='31'>31</option>
     </select> 
     
	 <select id="monthdropdown"  name="monthdropdown">
        <option value='mm'>MM</option>
     	<option value='01'>Jan</option>
		<option value='02'>Feb</option>
		<option value='03'>Mar</option>
		<option value='04'>Apr</option>
		<option value='05'>May</option>
		<option value='06'>Jun</option>
		<option value='07'>Jul</option>
		<option value='08'>Aug</option>
		<option value='09'>Sep</option>
		<option value='10'>Oct</option>
		<option value='11'>Nov</option>
		<option value='12'>Dec</option>
     </select> 
     
	 <select id="yeardropdown"  name="yeardropdown">
     <option value="yyyy">YYYY</option>
     <option value="2004">2004</option>
<option value="2003">2003</option>
<option value="2002">2002</option>
<option value="2001">2001</option>
<option value="2000">2000</option>
<option value="1999">1999</option>
<option value="1998">1998</option>
<option value="1997">1997</option>
<option value="1996">1996</option>
<option value="1995">1995</option>
<option value="1994">1994</option>
<option value="1993">1993</option>
<option value="1992">1992</option>
<option value="1991">1991</option>
<option value="1990">1990</option>
<option value="1989">1989</option>
<option value="1988">1988</option>
<option value="1987">1987</option>
<option value="1986">1986</option>
<option value="1985">1985</option>
<option value="1984">1984</option>
<option value="1983">1983</option>
<option value="1982">1982</option>
<option value="1981">1981</option>
<option value="1980">1980</option>
<option value="1979">1979</option>
<option value="1978">1978</option>
<option value="1977">1977</option>
<option value="1976">1976</option>
<option value="1975">1975</option>
<option value="1974">1974</option>
<option value="1973">1973</option>
<option value="1972">1972</option>
<option value="1971">1971</option>
<option value="1970">1970</option>
<option value="1969">1969</option>
<option value="1968">1968</option>
<option value="1967">1967</option>
<option value="1966">1966</option>
<option value="1965">1965</option>
<option value="1964">1964</option>
<option value="1963">1963</option>
<option value="1962">1962</option>
<option value="1961">1961</option>
<option value="1960">1960</option>
<option value="1959">1959</option>
<option value="1958">1958</option>
<option value="1957">1957</option>
<option value="1956">1956</option>
<option value="1955">1955</option>
<option value="1954">1954</option>
<option value="1953">1953</option>
<option value="1952">1952</option>
<option value="1951">1951</option>
<option value="1950">1950</option>
<option value="1949">1949</option>
<option value="1948">1948</option>
<option value="1947">1947</option>
<option value="1946">1946</option>
<option value="1945">1945</option>
<option value="1944">1944</option>
<option value="1943">1943</option>
<option value="1942">1942</option>
<option value="1941">1941</option>
<option value="1940">1940</option>
<option value="1939">1939</option>
<option value="1938">1938</option>
<option value="1937">1937</option>
<option value="1936">1936</option>
<option value="1935">1935</option>
<option value="1934">1934</option>
<option value="1933">1933</option>
<option value="1932">1932</option>
<option value="1931">1931</option>
<option value="1930">1930</option>
<option value="1929">1929</option>
<option value="1928">1928</option>
<option value="1927">1927</option>
<option value="1926">1926</option>
<option value="1925">1925</option>
<option value="1924">1924</option>
<option value="1923">1923</option>
<option value="1922">1922</option>
<option value="1921">1921</option>
<option value="1920">1920</option>
<option value="1919">1919</option>
<option value="1918">1918</option>
<option value="1917">1917</option>
<option value="1916">1916</option>
<option value="1915">1915</option>
<option value="1914">1914</option>
<option value="1913">1913</option>
<option value="1912">1912</option>
<option value="1911">1911</option>
<option value="1910">1910</option>
     </select> 
-->	 
	 <?php //echo form_input($dob); ?> <?php //echo form_input($dob2); ?></span> 
      
    <!--  <span><p>Date of birth</p><input type="text" id="datetimepicker2" placeholder="dd/mm/yyyy"/></span> -->
      <span></span>
      <span><p>Country</p><?php
               $options = array(
'selectcountry'  => 'Select',
'AF'=>'Afghanistan',
'AX'=>'Aland Islands',
'AL'=>'Albania',
'DZ'=>'Algeria',
'AS'=>'American Samoa',
'AD'=>'Andorra',
'AO'=>'Angola',
'AI'=>'Anguilla',
'AQ'=>'Antarctica',
'AG'=>'Antigua and Barbuda',
'AR'=>'Argentina',
'AM'=>'Armenia',
'AW'=>'Aruba',
'AU'=>'Australia',
'AT'=>'Austria',
'AZ'=>'Azerbaijan',
'BS'=>'Bahamas',
'BH'=>'Bahrain',
'BD'=>'Bangladesh',
'BB'=>'Barbados',
'BY'=>'Belarus',
'BE'=>'Belgium',
'BZ'=>'Belize',
'BJ'=>'Benin',
'BM'=>'Bermuda',
'BT'=>'Bhutan',
'BO'=>'Bolivia',
'BQ'=>'Bonaire, Sint Eustatius and Saba',
'BA'=>'Bosnia and Herzegovina',
'BW'=>'Botswana',
'BR'=>'Brazil',
'IO'=>'British Indian Ocean Territory',
'BN'=>'Brunei Darussalam',
'BG'=>'Bulgaria',
'BF'=>'Burkina Faso',
'BI'=>'Burundi',
'KH'=>'Cambodia',
'CM'=>'Cameroon',
'CA'=>'Canada',
'CV'=>'Cape Verde',
'KY'=>'Cayman Islands',
'CF'=>'Central African Republic',
'TD'=>'Chad',
'CL'=>'Chile',
'CN'=>'China',
'CX'=>'Christmas Island',
'CC'=>'Cocos (Keeling) Islands',
'CO'=>'Colombia',
'KM'=>'Comoros',
'CG'=>'Congo',
'CD'=>'Congo, The Democratic Republic of the',
'CK'=>'Cook Islands',
'CR'=>'Costa Rica',
'CI'=>'Cote d Ivoire',
'HR'=>'Croatia',
'CU'=>'Cuba',
'CW'=>'Curaçao',
'CY'=>'Cyprus',
'CZ'=>'Czech Republic',
'DK'=>'Denmark',
'DJ'=>'Djibouti',
'DM'=>'Dominica',
'DO'=>'Dominican Republic',
'EC'=>'Ecuador',
'EG'=>'Egypt',
'SV'=>'El Salvador',
'GQ'=>'Equatorial Guinea',
'ER'=>'Eritrea',
'EE'=>'Estonia',
'ET'=>'Ethiopia',
'FK'=>'Falkland Islands(Malvinas)',
'FO'=>'Faroe Islands',
'FJ'=>'Fiji',
'FI'=>'Finland',
'FR'=>'France',
'GF'=>'French Guiana',
'PF'=>'French Polynesia',
'TF'=>'French Southern Territories',
'GA'=>'Gabon',
'GM'=>'Gambia',
'GE'=>'Georgia',
'DE'=>'Germany',
'GH'=>'Ghana',
'GI'=>'Gibraltar',
'GR'=>'Greece',
'GL'=>'Greenland',
'GD'=>'Grenada',
'GP'=>'Guadeloupe',
'GU'=>'Guam',
'GT'=>'Guatemala',
'GG'=>'Guernsey',
'GN'=>'Guinea',
'GW'=>'Guinea-Bissau',
'GY'=>'Guyana',
'HT'=>'Haiti',
'HM'=>'Heard Island and McDonald Islands',
'VA'=>'Holy See (Vatican City State)',
'HN'=>'Honduras',
'HK'=>'Hong Kong',
'HU'=>'Hungary',
'IS'=>'Iceland',
'IN'=>'India',
'ID'=>'Indonesia',
'XZ'=>'Installations in International Waters',
'IR'=>'Iran, Islamic Republic of',
'IQ'=>'Iraq',
'IE'=>'Ireland',
'IM'=>'Isle of Man',
'IL'=>'Israel',
'IT'=>'Italy',
'JM'=>'Jamaica',
'JP'=>'Japan',
'JE'=>'Jersey',
'JO'=>'Jordan',
'KZ'=>'Kazakhstan',
'KE'=>'Kenya',
'KI'=>'Kiribati',
'KP'=>'Korea, Democratic People s Republic of',
'KR'=>'Korea, Republic of',
'KW'=>'Kuwait',
'KG'=>'Kyrgyzstan',
'LA'=>'Lao People s Democratic Republic',
'LV'=>'Latvia',
'LB'=>'Lebanon',
'LS'=>'Lesotho',
'LR'=>'Liberia',
'LY'=>'Libya',
'LI'=>'Liechtenstein',
'LT'=>'Lithuania',
'LU'=>'Luxembourg',
'MO'=>'Macao',
'MK'=>'Macedonia, The former Yugoslav Republic of',
'MG'=>'Madagascar',
'MW'=>'Malawi',
'MY'=>'Malaysia',
'MV'=>'Maldives',
'ML'=>'Mali',
'MT'=>'Malta',
'MH'=>'Marshall Islands',
'MQ'=>'Martinique',
'MR'=>'Mauritania',
'MU'=>'Mauritius',
'YT'=>'Mayotte',
'MX'=>'Mexico',
'FM'=>'Micronesia, Federated States of',
'MD'=>'Moldova, Republic of',
'MC'=>'Monaco',
'MN'=>'Mongolia',
'ME'=>'Montenegro',
'MS'=>'Montserrat',
'MA'=>'Morocco',
'MZ'=>'Mozambique',
'MM'=>'Myanmar',
'NA'=>'Namibia',
'NR'=>'Nauru',
'NP'=>'Nepal',
'NL'=>'Netherlands',
'NC'=>'New Caledonia',
'NZ'=>'New Zealand',
'NI'=>'Nicaragua',
'NE'=>'Niger',
'NG'=>'Nigeria',
'NU'=>'Niue',
'NF'=>'Norfolk Island',
'MP'=>'Northern Mariana Islands',
'NO'=>'Norway',
'OM'=>'Oman',
'PK'=>'Pakistan',
'PW'=>'Palau',
'PS'=>'Palestine, State of',
'PA'=>'Panama',
'PG'=>'Papua New Guinea',
'PY'=>'Paraguay',
'PE'=>'Peru',
'PH'=>'Philippines',
'PN'=>'Pitcairn',
'PL'=>'Poland',
'PT'=>'Portugal',
'PR'=>'Puerto Rico',
'QA'=>'Qatar',
'RE'=>'Reunion',
'RO'=>'Romania',
'RU'=>'Russian Federation',
'RW'=>'Rwanda',
'BL'=>'Saint Barthelemy',
'SH'=>'Saint Helena, Ascension and Tristan Da Cunha',
'KN'=>'Saint Kitts and Nevis',
'LC'=>'Saint Lucia',
'MF'=>'Saint Martin (French Part)',
'PM'=>'Saint Pierre and Miquelon',
'VC'=>'Saint Vincent and the Grenadines',
'WS'=>'Samoa',
'SM'=>'San Marino',
'ST'=>'Sao Tome and Principe',
'SA'=>'Saudi Arabia',
'SN'=>'Senegal',
'RS'=>'Serbia',
'SC'=>'Seychelles',
'SL'=>'Sierra Leone',
'SG'=>'Singapore',
'SX'=>'Sint Maarten (Dutch Part)',
'SK'=>'Slovakia',
'SI'=>'Slovenia',
'SB'=>'Solomon Islands',
'SO'=>'Somalia',
'ZA'=>'South Africa',
'GS'=>'South Georgia and the South Sandwich Islands',
'SS'=>'South Sudan',
'ES'=>'Spain',
'LK'=>'Sri Lanka',
'SD'=>'Sudan',
'SR'=>'Suriname',
'SJ'=>'Svalbard and Jan Mayen',
'SZ'=>'Swaziland',
'SE'=>'Sweden',
'CH'=>'Switzerland',
'SY'=>'Syrian Arab Republic',
'TW'=>'Taiwan, Province of China',
'TJ'=>'Tajikistan',
'TZ'=>'Tanzania, United Republic of',
'TH'=>'Thailand',
'TL'=>'Timor-Leste',
'TG'=>'Togo',
'TK'=>'Tokelau',
'TO'=>'Tonga',
'TT'=>'Trinidad and Tobago',
'TN'=>'Tunisia',
'TR'=>'Turkey',
'TM'=>'Turkmenistan',
'TC'=>'Turks and Caicos Islands',
'TV'=>'Tuvalu',
'UG'=>'Uganda',
'UA'=>'Ukraine',
'AE'=>'United Arab Emirates',
'GB'=>'United Kingdom',
'US'=>'United States',
'UM'=>'United States Minor Outlying Islands',
'UY'=>'Uruguay',
'UZ'=>'Uzbekistan',
'VU'=>'Vanuatu',
'VE'=>'Venezuela',
'VN'=>'Viet Nam',
'VG'=>'Virgin Islands, British',
'VI'=>'Virgin Islands, U.S.',
'WF'=>'Wallis and Futuna',
'EH'=>'Western Sahara',
'YE'=>'Yemen',
'ZM'=>'Zambia',
'ZW'=>'Zimbabwe');

//print_r($options);die;

           

              echo form_dropdown('country', $options, $country);
      
        ?></span>
                                    <span><p>Occupation</p><?php
                                     $options = array(
                                                        'selectoccupation'  => 'Select',
                                                        'Accounting'  => 'Accounting',
                                                        'Administrative/offic'  => 'Administrative/office',
                                                        'Advertising'   => 'Advertising',
							'Animal care'   => 'Animal care',
							'Architect'   => 'Architect',
							'Art-creative'   => 'Art-creative',
							'Athlete/coach'   => 'Athlete/coach',
							'Building/surveying'   => 'Building/surveying',
							'Customer service'   => 'Customer service',
							'Design'   => 'Design',
							'Emergency services'   => 'Emergency services',
							'Education'   => 'Education',
							'Engineering'   => 'Engineering',
							'Entertainer'   => 'Entertainer',
							'Farmer/primary production'   => 'Farmer/primary production',
							'Finance'   => 'Finance',
							'Garden care'   => 'Garden care',
							'General business'   => 'General business',
							'Health care'   => 'Health care',
							'Human resources'   => 'Human resources',
							'Information technology'   => 'Information technology',
							'Legal'   => 'Legal',			
                                                        'Management' => 'Management',
							'Manufacturing' => 'Manufacturing',
							'Manufacturing' => 'Manufacturing',
							'Medical/dental' => 'Medical/dental',
							'Nurse'   => 'Nurse',
							'Pilot'   => 'Pilot',
							'Production'   => 'Production',
							'Public service'   => 'Public service',
							'Research'   => 'Research',
                                 );  

              echo form_dropdown('occupation', $options, $occupation);?>
	   </span>
       <span><p>Notes</p><?php echo form_textarea('p_desc', $p_desc,"class='notenew'"); ?></span>
       <span>         
          <?php echo form_submit('mysubmit1','Pre-exercise screening');  ?> 
          <?php echo form_submit('mysubmit2','Fitness testing',"class='start-btn fitness_test_btn'"); ?>
          <?php echo form_submit('mysubmit3','Body composition',"class='start-btn body_comp_btn'"); ?>
       </span>

      </div>
      <br>
	<?php echo form_close(); ?>
</div>
</div>
<!--MID 3 Left end-->
<div class="clearfix"></div>
 <!-- <div class="mid_center">
	<h3>Please note:</h3>
	<p>Your information will be kept confidential, and your data will not be individually presented to your employer. Group data will be analysed and health initiatives will be developed according to the specific needs of the employees and the roles that are undertaken for the company.</p>
</div>

<div class="bottom-btn">
    <div class="quit-btn"><a href="#">Submit</a></div>
    <div class="start-btn"><a href="#">Reset</a></div>
</div>   End botton buttons 


<div class="footer">&copy; Copyrights Reserved by Health Screen Pro Pty Ltd</div>--> 


 
 <div class="footer"> <div>Developed by Professor Kevin Norton and Dr Lynda Norton </div> </div>
</div><!--End Mid --> 
</div><!--End Wrapper --> 

</body>
<script>
$('#default_datetimepicker').datetimepicker({
	formatTime:'H:i',
	formatDate:'d.m.Y',
	defaultDate:'01.01.2015', // it's my birthday
	defaultTime:'10:00'
});

$('#datetimepicker2').datetimepicker({
	yearOffset:0,
	lang:'en',
	timepicker:false,
	defaultDate:'01.01.2015',
	format:'d/m/Y',
	formatDate:'Y/m/d',

});

</script>
</html>
