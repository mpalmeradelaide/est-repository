<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Screening</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script>
    
function doConfirm(msg, yesFn, noFn) {
    var confirmBox = $("#confirmBox");
    confirmBox.find(".message").text(msg);
    confirmBox.find(".yes,.no").unbind().click(function () {
        confirmBox.hide();
    });
    confirmBox.find(".yes").click(yesFn);
    confirmBox.find(".no").click(noFn);
    confirmBox.show();
}
    
    
  $(function() {
	 
	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
	   console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
			// console.log($('.left').hasClass('fixed');
		});
                
                
                    $("#form_new").submit(function (e) {
        e.preventDefault();
        var form = this;
        doConfirm("Warning: The current client data will be lost", function yes() {
            form.submit();
        }, function no() {
            // do nothing
        });
    });
	
}); 


</script>

</head>

<body>


<script>

</script>  
    <?php 
    $risk_factor =0;
    if( $screening['physical_activity'] !='' || $screening['physical_activity'] !=0){
    if( $screening['physical_activity'] >=150){
        $risk_factor = $risk_factor-1;
       //echo $risk_factor.'physical_activity'."</br>";
        }else{
            if($screening['physical_activity'] >0){
                $risk_factor = $risk_factor+1;
                //echo $risk_factor.'physical_activity'."</br>";
            }
      }
    }
      $heart_disease_value ='No';  
     if($screening['heart_disease'] =='Y' || $screening['heart_disease'] =='N'){  
      
            if($screening['heart_disease'] =='Y'){
               
                
                if($screening['option_gender']=='M' || $screening['option_gender']=='F' ){
               
                    if($screening['option_gender']=='M' && $screening['option_age'] < 55 && $screening['option_age'] != '' ){
                        $heart_disease_value ='Yes';
                        $risk_factor = $risk_factor+1;
                        //echo $risk_factor.'heart_disease'."</br>";
                }else{
                     if($screening['option_gender']=='F' && $screening['option_age'] < 65 && $screening['option_age'] != '' ){
                        $heart_disease_value ='Yes'; 
                        $risk_factor = $risk_factor+1;
                        //echo $risk_factor.'heart_disease'."</br>";
                    }
                }
               }
            } 
     }
       

if($screening['smoking'] =='Y' || $screening['smoking2'] == 'Y'){
$risk_factor = $risk_factor+1;
//echo $risk_factor.'smoking'."</br>";
}
$isHighBp="false";
$isHighColestrol="false";
$isHighGlucose="false";

if($screening['hbp']!="" && $screening['dbp']!=""){
if($screening['hbp'] >=140){
$risk_factor = $risk_factor+1;
$isHighBp ="true";
//echo $risk_factor.'hbp'."</br>";
}elseif($screening['dbp']>90 && $screening['hbp']<140){
$risk_factor = $risk_factor+1;
$isHighBp ="true";
//echo $risk_factor.'hbp2'."</br>";
}
}
/*else if($screening['hbp']!=""){
$risk_factor++;
}
 * */
 
elseif($screening['high_blood_pressure'] =='Y'){
$risk_factor = $risk_factor+1;
$isHighBp ="true";

//echo $risk_factor.'high_blood_pressure'."</br>";
}

if($screening['hbg']!=""){
if($screening['hbg']>=5.5){
$risk_factor = $risk_factor+1;
$isHighGlucose="true";
//echo $risk_factor.'hbg'."</br>";
}
}elseif($screening['high_sugar']=='Y'){
    $risk_factor = $risk_factor+1;
    $isHighGlucose="true";
//echo $risk_factor.'high_sugar'."</br>";
}

$lipid_factor =0;

if($screening['colestrol'] != "" && $screening['triglycerides'] !=""){
if($screening['colestrol']>=5.2){
 $lipid_factor= $lipid_factor +1;
 $isHighColestrol="true";
 //echo $lipid_factor.'colestrol'."</br>";
}
elseif($screening['triglycerides'] >=1.7){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'triglycerides'."</br>";
}
}
elseif($screening['colestrol']!=""){
if($screening['colestrol']>=5.2){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'colestrol'."</br>";
}
}
elseif($screening['triglycerides']!=""){
if($screening['triglycerides'] >=1.7){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'triglycerides'."</br>";
}
}
elseif($screening['high_colestrol']=='Y'){
$lipid_factor= $lipid_factor +1;
$isHighColestrol="true";
//echo $lipid_factor.'high_colestrol'."</br>";
}
$llm_arr =  explode(',', $screening['llm']);
//print_r($llm_arr);

//in_array('LIPID-lowering medication', $llm_arr)?'true':'false';
 $lipid ='false';
 $bloodPressure ='false';
 $glucose ='false';


if(in_array('LIPID-lowering medication', $llm_arr)){
    $lipid='true';
    if($isHighColestrol != "true")
    $lipid_factor= $lipid_factor +1;
    //echo $lipid_factor."LIPID-lowering medication";
}
if(in_array('BLOOD PRESSURE-lowering medication', $llm_arr)){
    $bloodPressure='true';
  if($isHighBp != "true")
     $risk_factor = $risk_factor+1;
}

if( $screening['dbp'] > 90 && $screening['hbp'] < 140 ){
      $bloodPressure='true';
}

if( $screening['hbp'] >=140 ){
      $bloodPressure='true';
}


if(in_array('GLUCOSE-lowering medication', $llm_arr)){
  $glucose='true';
  if($isHighGlucose != "true")
  $risk_factor = $risk_factor+1;
}
//1-left
$lipid  ."</br>";
//echo $bloodPressure ."</br>";
//echo $glucose ."</br>";

if($screening['hdl'] !="" && $screening['hdl'] !=0){
if($screening['hdl']<1){
$lipid_factor= $lipid_factor +1;
//$risk_factor = $risk_factor +1;
//echo $lipid_factor.'hdllll'."</br>";
}elseif($screening['hdl']>1.55){
    //$lipid_factor= $lipid_factor -1;
    $risk_factor = $risk_factor -1;
    //echo $lipid_factor.'hdldddd'."</br>";

}
}

if($screening['ldl'] !="" && $screening['ldl'] !=0){
if($screening['ldl']>=3.4){
    $lipid_factor= $lipid_factor +1;
    //echo $lipid_factor.'ldl'."</br>";

}
}


if($lipid_factor>1){
$risk_factor = $risk_factor + 1;
 //echo 'lipidfactor :'.$lipid_factor.'risk factor :'.$risk_factor."</br>";
}else{
$risk_factor = $risk_factor + $lipid_factor;
 //echo 'lipidfactorvalue :'.$lipid_factor.'risk factor :'.$risk_factor."</br>";
}

if($screening['bmi']>=30){
$risk_factor = $risk_factor+1;
 $risk_factor.'bmi'."</br>";
}
if($screening['gender'] =='M'){
if($screening['waist'] >94){
$risk_factor = $risk_factor+1;
 $risk_factor.'waist'."</br>";
}
if($screening['0']['age']>=45){
$risk_factor = $risk_factor+1;
 $risk_factor.'age'."</br>";
}
}else{
if($screening['waist'] >80){
$risk_factor = $risk_factor+1;
 $risk_factor.'waist female'."</br>";
}
if($screening['0']['age']>=55){
$risk_factor = $risk_factor+1;
 $risk_factor.'age female'."</br>";
}

}  

$myClass->updateClientInfo($risk_factor);

$variable='' ;
$class='';
$text ='';
if( $screening['1']['status'] == 'Y'){
        $variable = "HIGH";
        $class = 'red-text';
          $text = " risk. It is recommended they seek advice from a medical practitioner or an appropriate allied health professional before undertaking an exercise program.";
    }
    else{
        
        if($risk_factor  >= 2){
           $variable = "Moderate";
            $class = 'orange-text';
            
            $text = " risk. If they have no other concerns about their health, they may proceed to undertake light-moderate intensity physical activity/exercise with appropriate supervision and progression.";
        }
        else if($risk_factor < 2){
             $variable = "Low";
            $class = 'green-text';
           
            $text = " risk. If they have no other concerns about their health, they may proceed to undertake aerobic physical activity/exercise up to a vigorous or high intensity with appropriate supervision and progression.";          }
 }
    
    
    
    
    
    
    
    
    ?>
<?php //print_r($screening); ?>
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo"  id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
    <?php  echo form_open('welcome/fetchScreenInfo','',$hidden); ?>     
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitScreening" type="submit" value="" title="edit client details"  id="edit-btn"/></span>
     </div>
<!--	</form>  -->
</div>
<!--End login --> 




 <!--Start Mid --> 
  <div class="mid3 screen-contain">
 
   
<!--Start contain --> 
<div class="contain">
 <?php
      $hidden = array('userid' => $id);
      echo form_open('welcome/fetchScreenInfo','',$hidden); ?>    
   <!--Start left --> 
   <div class="left">
   		<div class="btn">
        <?php echo form_submit('mysubmit1','',"class='client_submit_form1' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form66' , 'id' = 'myform6'");  ?>
        </div>
       <?php echo form_close(); ?>   
   
   </div>
   <!--End Left --> 
   <!--Start right --> 
   <div class="right">
   		<div class="right-head">Screening Summary</div>
   		<div class="right-section">

       	<div class="screen-para">The client has been screened as <b class="<?php echo $class ;  ?>"> <?php echo $variable; ?> </b> <?php echo $text; ?></div>
       
         <?php if($screening['proceed'] =='proceed' &&$screening['1']['status']=='Y'){  ?>
        <div class="screen-para-proceed">A professional decision has been made to allow the client to proceed.</div>
        <?php } ?>
        
               
        <div class="screen-data">
        
        <div class="screen-left">
        
        <?php if($screening['gender'] =='M'){  ?>
        <div class="screen-content-box <?php echo $screening['0']['age'] >= '45'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Age <b class="small">[yr]</b></div>
        	<div class="screen-value"><?php echo $screening['0']['age']; ?></div>
        </div>
        <?php }else{ ?> 
        <div class="screen-content-box <?php echo $screening['0']['age'] >= '55'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Age <b class="small">[yr]</b></div>
        	<div class="screen-value"><?php echo number_format($screening['0']['age'], 1); ?></div>
        </div>
        <?php } ?>
            
        
          <?php if($screening['physical_activity'] == '' || $screening['physical_activity'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Physical activity * <b class="small">[min/wk]</b></div>
        	<div class="screen-value"></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo $screening['physical_activity'] >= '150'?'green-text':'red-text'; ?>">
        	<div class="screen-head">Physical activity * <b class="small">[min/wk]</b></div>
        	<div class="screen-value"><?php echo $screening['physical_activity']; ?></div>
        </div>
          <?php } ?>   
            
        <?php if($screening['heart_disease'] == '' || $screening['heart_disease'] == '0'){ ?> 
         <div class="screen-content-box margin-family">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"></div>
             </div>
          <?php }else{ ?>
           
            <div class="screen-content-box margin-family <?php echo $heart_disease_value=='Yes'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"><?php echo $heart_disease_value;  ?></div>
             </div>
        <?php } ?>     
            
    <!--        
       
        <?php if($screening['heart_disease'] == '' || $screening['heart_disease'] == '0'){ ?> 
         <div class="screen-content-box margin-family">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"></div>
             </div>
          <?php }else{ ?>
           <?php if($screening['heart_disease'] == 'N'){ ?>
            <div class="screen-content-box margin-family green-text">
        	<div class="screen-head">Family history of heart disease</div>
        	<div class="screen-value"><?php echo $screening['heart_disease'] == 'Y' ?'Yes':'No'; ?></div>
             </div>
        <?php }else{ ?>
            
                        <?php if($screening['option_gender'] =='M'){  ?>
                       <div class="screen-content-box margin-family <?php echo $screening['option_age'] >= '55'?'green-text':'red-text'; ?>">
                              <div class="screen-head">Family history of heart disease</div>
                              <div class="screen-value"><?php echo $screening['heart_disease'] == 'Y'  && $screening['option_age'] < '55'?'Yes':'No'; ?></div>
                      </div>
                           <?php }else{ ?> 
                       <div class="screen-content-box margin-family <?php echo $screening['option_age'] >= '65'?'green-text':'red-text'; ?>">
                              <div class="screen-head">Family history of heart disease</div>
                              <div class="screen-value"><?php echo $screening['heart_disease'] == 'Y' && $screening['option_age'] < '65'?'Yes':'No'; ?></div>
                      </div>
                     <?php } ?>
          <?php } ?>  
          <?php } ?>     
            
            
         --> 
          
          <?php if($screening['smoking'] == '' || $screening['smoking'] == '0'){ ?> 
         <div class="screen-content-box" >
        	<div class="screen-head">Smoking</div>
        	<div class="screen-value"></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo ($screening['smoking'] == 'Y' || $screening['smoking2'] == 'Y')?'red-text':'green-text'; ?>">
        	<div class="screen-head">Smoking</div>
        	<div class="screen-value"><?php echo ($screening['smoking'] == 'Y' || $screening['smoking2'] == 'Y')?'Yes':'No'; ?></div>
        </div>
          <?php } ?>    
            
        
        
         <?php if($screening['dbp'] != '' || $screening['dbp'] != 0){ 
                if($screening['hbp'] < 140 && $screening['dbp'] > 90){
        ?> 
                    <div class="screen-content-box red-text">
                                   <div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
                                   <div class="screen-value"><?php echo $screening['hbp']."/"; ?><?php echo $screening['dbp']; ?></div></div>
               <?php }else{ 
if($screening['hbp'] != '' || $screening['hbp'] != 0){ 
?>
                        <div class="screen-content-box <?php echo $bloodPressure=='true'?'red-text':'green-text';?>">
                                        <div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
                                        <div class="screen-value"><?php echo $screening['hbp']."/"; ?><?php echo $screening['dbp']; ?></div></div>
<?php }


}
}else{
 if($screening['hbp'] != '' || $screening['hbp'] != 0){   
   if($screening['hbp'] >= 140){
 ?>
 <div class="screen-content-box red-text">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"><?php echo $screening['hbp']; ?></div></div>
<?php }else{ ?>
<div  class="screen-content-box <?php echo $bloodPressure=='true'?'red-text':'green-text';?>">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"><?php echo $screening['hbp']; ?></div></div>
<?php } }else{
    if($screening['high_blood_pressure']=='Y' || $bloodPressure=='true'){

    ?>
      <div class="screen-content-box red-text">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
       </div> 	 
    
<?php }else{ ?>
    <div class="screen-content-box ">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"></div></div>
    
<?php }}}?> 
<!--
<?php if(($screening['dbp'] === '' || $screening['dbp'] === 0) && ($screening['hbp'] === '' || $screening['hbp'] === 0)){ ?>
<div class="screen-content-box ">
        	<div class="screen-head">Blood pressure <b class="small">[mmHg]</b></div>
        	<div class="screen-value"></div></div>

<?php }?>   

-->        
        
        
         <?php if($screening['colestrol'] == '' || $screening['colestrol'] == 0){
             ?> 
        <div class="screen-content-box  <?php echo ($lipid=='true' || $screening['high_colestrol']=='Y') ?'red-text':'';?>">
        	<div class="screen-head">Total cholesterol <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['colestrol']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo ($screening['colestrol'] >= '5.2'|| $lipid=='true' )?'red-text':'green-text'; ?>">
        	<div class="screen-head">Total cholesterol <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['colestrol']; ?></div>
        </div>
          <?php } ?>
         
        <?php if($screening['hdl'] == '' || $screening['hdl'] == 0){ ?> 
        <div class="screen-content-box ">
        	<div class="screen-head">HDL * <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hdl']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo $screening['hdl'] <= '1'?'red-text':'green-text'; ?>">
        	<div class="screen-head">HDL * <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hdl']; ?></div>
        </div>
          <?php } ?>       
        
        <?php if($screening['ldl'] == '' || $screening['ldl'] == 0){ ?> 
        <div class="screen-content-box ">
        	<div class="screen-head ">LDL <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['ldl']; ?></div>
        </div>
          <?php }else{ ?>
         <div class="screen-content-box <?php echo $screening['ldl'] >= '3.4'?'red-text':'green-text'; ?>">
        	<div class="screen-head ">LDL <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['ldl']; ?></div>
        </div>
          <?php } ?>
        
        <?php if($screening['triglycerides'] == '' || $screening['triglycerides'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Triglycerides <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['triglycerides']; ?></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo $screening['triglycerides'] >= '1.7'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Triglycerides <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['triglycerides']; ?></div>
        </div>
          <?php } ?>
        
         
        
             
             <?php if($screening['hbg'] == '' || $screening['hbg'] == 0){ ?> 
         <div class="screen-content-box <?php echo ($glucose =='true' || $screening['high_sugar']=='Y')?'red-text':'';?> ">
        	<div class="screen-head">Blood glucose <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hbg']; ?></div>
        </div>
          <?php }else{ ?>
          <div class="screen-content-box <?php echo ($screening['hbg'] >= '5.5' || $glucose =='true')?'red-text':'green-text'; ?>">
        	<div class="screen-head">Blood glucose <b class="small">[mM]</b></div>
        	<div class="screen-value"><?php echo $screening['hbg']; ?></div>
        </div>
          <?php } ?>
         <?php if($screening['bmi'] == '' || $screening['bmi'] == 0){ ?> 
         <div class="screen-content-box">
        	<div class="screen-head">BMI</div>
        	<div class="screen-value"><?php echo $screening['bmi']; ?></div>
        </div>
          <?php }else{ ?>
           <div class="screen-content-box <?php echo $screening['bmi'] >= '30'?'red-text':'green-text'; ?>">
        	<div class="screen-head">BMI</div>
        	<div class="screen-value"><?php echo $screening['bmi']; ?></div>
        </div>
          <?php } ?>
         
         
         <?php if($screening['waist'] == '' || $screening['waist'] == 0){ ?> 
         <div class="screen-content-box ">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
          <?php }else{ ?>
          <?php if($screening['gender'] =='M'){  ?>
         <div class="screen-content-box <?php echo $screening['waist'] > '94'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
           <?php }else{ ?> 
         <div class="screen-content-box <?php echo $screening['waist'] > '80'?'red-text':'green-text'; ?>">
        	<div class="screen-head">Waist girth <b class="small">[cm]</b></div>
        	<div class="screen-value"><?php echo $screening['waist']; ?></div>
        </div>
        <?php } ?>
          <?php } ?> 
        <div class="screen-content-box">
        <div class="riskfactor red-text">risk factor</div>
        <div class="riskfactor green-text">* can be a positive or negative risk factor</div>
        </div>
        </div><!--End screen left--> 
        
        <div class="screen-right">
			<div class="score-head">Risk factor score</div>
            <div class="score"><?php echo $risk_factor>9 ?9:$risk_factor;  ?></div>
        </div>
		</div><!--End screen data--> 
       
      </div><!--End right section--> 
     <form class="form_new" id="form_new" action="<?php echo site_url('welcome/clientDetail'); ?>">
   <input type="submit" value="Start a new test" class="new-test" size="100"  />
   </form>
   <form class="form_print">
   <input type="button" value="" onclick="window.print();return false;" /></form>
   
      <div id="confirmBox">
    <div class="message"></div>
    <span class="button yes">New Client</span>
    <span class="button no">Cancel</span>
     </div>

   
 <?php if($screening['medical_notes'] != "" && $screening['medical_notes'] !=""){   ?>
   <div class="disc">   
       <div class="disc-head">Medical Notes:</div>
       <div class="disc-text"><?php echo $screening['medical_notes'];?></div>
       </div>
 <?php } ?>
  <?php if($screening['medication_notes'] != "" && $screening['medication_notes'] !=""){   ?>    
  <div class="disc">  
       <div class="disc-head">Medication Notes:</div>
       <div class="disc-text"><?php echo $screening['medication_notes'];?></div>
   </div>
      <?php } ?>
   
   </div><!--End right --> 
   
   
</div>
   <!--End contain -->
<!--
<div class="footer screen-footer">&copy; Copyrights Reserved by Health Screen Pro</div>
-->

</div><!--End Mid --> 
         <div class="main-sign"  style="margin-top:50px;">
       <div class="tester-sign">Client's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
      </div>
      
       <div class="main-sign">
       <div class="tester-sign">Tester's Signature :______________________________</div>
       <div class="date-sign">Date :_________________</div>
      </div>
   
   
   
      <div class="disc">        
       <div class="disc-head tp_mrgn">Disclaimer statement:</div>
       <div class="disc-text">This web-based screening tool assists the pre-exercise screening process using a national standard assessment tool. The tool was jointly developed by three professional organisations [Fitness Australia, Exercise and Sports Science Australia and Sports Medicine Australia] as a recommended pre-exercise screening system. However, the screening tool used as part of the screening system is not intended to provide advice on a particular matter, medical condition or health issue, nor does it substitute for professional advice from appropriately qualified medical or other health practitioners. No warranty of
safety should result from its use. The screening system in no way guarantees against injury or death. No responsibility or liability whatsoever can be accepted by Fitness Australia, Exercise and Sports Science Australia, Sports Medicine Australia or by any AFIRM project members for any loss, damage or injury that may arise from any person acting on any statement or information contained in this software program or from the use of the pre-exercise screening tool.
</div>
   </div>

</div><!--End Wrapper --> 

</body>
</html>
