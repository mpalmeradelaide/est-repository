<!DOCTYPE html>
<!--
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Risk Factor</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">

</head>
<body>

<div id="container"  style=" float:left ;width:90%">
    FAMILY HISTORY OF HEART DISEASE</br>
<?
//$hidden = array('id' => $id);

  
 echo form_open('welcome/saveClientRiskFactorInfo',''); ?>

<? echo "Have you ever had a male relative[father,brother,son] or a female relative[mother, sister, daughter] who has had a myocardial infraction or bypass operation or died suddenly of a heart attack before the age of 65 ?"; ?>:
<? echo form_radio(array("name"=>"options_1","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_1","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

SMOKING</br>
<? echo "Do you smoke?"; ?>:
<? echo form_radio(array("name"=>"options_2","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_2","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

<? echo "Have you quit smoking in last 6 month?"; ?>:
<? echo form_radio(array("name"=>"options_3","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_3","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

SELF REPORT</br>
<? echo "Have you been told you have high blood pressure?"; ?>:
<? echo form_radio(array("name"=>"options_4","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_4","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

<? echo "Have you been told you have high colesterol?"; ?>:
<? echo form_radio(array("name"=>"options_5","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_5","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

<? echo "Have you been told you have high blood sugar?"; ?>:
<? echo form_radio(array("name"=>"options_6","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_6","id"=>"yes","value"=>"yes" )); ?>:YES
</br>

MEASURED
</br>


<? echo "SBP"; ?>:
<? echo form_input('options_7'); ?>mmHg
</br>

<? echo "DBP"; ?>:
<? echo form_input('options_8'); ?>mmHg
</br>

<? echo "Fasting Blood Glucose"; ?>:
<? echo form_input('options_9'); ?>mM
</br>

<? echo "Total Blood Cholestrol"; ?>:
<? echo form_input('options_10'); ?>mM
</br>

<? echo "HDL"; ?>:
<? echo form_input('options_11'); ?>mM
</br>

<? echo "LDL"; ?>:
<? echo form_input('options_12'); ?>mM
</br>

<? echo "Triglycerides"; ?>:
<? echo form_input('options_12'); ?>mM
</br>



<? echo form_submit('mysubmit','Submit!');  ?>
<? echo form_close(); ?>



</div>
    <div style="float:left ; width:9%">
        <?php echo $menu; ?>
        
    </div>
</body>
</html>

-->

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Risk Factors</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">

<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">
 
 
  function compareValue() {
   
 val7 = parseInt($('#options_7').val());
  val8 =parseInt($('#options_8').val());
  
  
  if(val7=='')val7=0;
  if(val8=='')val8=0;
  
  console.log(val7);
  console.log(val8);
 if(val8 > val7){
    alert("Please check your blood pressure values");
 $('#options_8').val("");
 }
  }   


  function handleChangeSbp(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 240;
 compareValue()
  }  
    
  function handleChangeDbp(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 140;
    compareValue()
  }  
  function handleChangeAge(input) {
    if (input.value < 0) input.value = 0;
    if (input.value > 240) input.value = 100;
  }  
    
  $(function() {
    $('.two-decimal').keyup(function(){
   if($(this).val().indexOf('.')!=-1){         
       if($(this).val().split(".")[1].length > 2){                
           if( isNaN( parseFloat( this.value ) ) ) return;
           this.value = parseFloat(this.value).toFixed(2);
       }  
    }            
    return this; //for chaining
 });  
 
  $('.one-decimal').keyup(function(){
   if($(this).val().indexOf('.')!=-1){         
       if($(this).val().split(".")[1].length > 1){                
           if( isNaN( parseFloat( this.value ) ) ) return;
           this.value = parseFloat(this.value).toFixed(1);
       }  
    }            
    return this; //for chaining
 }); 
   <?php if($fieldData[0]->option_gender != 'F'){ ?>
      $("#option_male").attr('checked', 'checked');
 <?php } ?>    
          
      
   
   var age ='<?php  echo $fieldData[0]->option_1 !=''?$fieldData[0]->option_1:"blank";?>';   
   var smoke ='<?php  echo $fieldData[0]->option_2 !=''?$fieldData[0]->option_2:"blank";?>';   
   var smoke_6 ='<?php  echo $fieldData[0]->option_3 !=''?$fieldData[0]->option_3:"blank";?>';   
   //console.log(age);
   if(age != 'Y')
       { 
    $("#content").hide();
       }
   if(smoke != 'Y'){
    $("#content_option").hide();
     }
    if(smoke_6 != 'Y'){ 
    $("#content_option2").hide();
    }
	
	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
	   console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
			// console.log($('.left').hasClass('fixed');
		});
	
});  
function toggleDiv(divId) {

  if($('[name=options_1]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}

function toggleDiv2(divId) {

  if($('[name=options_2]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}


function toggleDiv3(divId) {

  if($('[name=options_3]:checked').val()=='Y') {
   $("#"+divId).show();
  }else{
     $("#"+divId).hide(); 
  }
  
  
}


     var specialKeys = new Array();
      specialKeys.push(8); //Backspace
        function IsNumeric(e) {
        /*   
        var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        if(!ret)  { 
        alert('Only Numbers');
        }
            return ret;
            */
          
                var a = [];
            var k = e.which;

            for (i = 48; i < 58; i++)
                a.push(i);

            if (!(a.indexOf(k)>=0)){
                e.preventDefault();
           // alert('only Numbers');
            //CUSTOM_ALERT.alert('','Number Only');
            }
      // theNum = document.getElementById().value;
          
 
       
        }
       
        
      
   function scrollWin()
    {
    window.scrollBy(0,300);
    }    
        




</script>

</head>

<body    onLoad="scrollWin()">
<!--Start Wrapper --> 
<div class="wrapper">

        <div id="CustomAlert">
 <div id="CustomAlertMessage"></div>
 <input type="button" value="OK" onClick="CUSTOM_ALERT.hide();" id="CustomAlertSampleokButton">
</div>

    
  <div class="logo" id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
    <?php   echo form_open('welcome/saveClientRiskFactorInfo','',$hidden); ?>  
<!--	<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitRiskFactor" type="submit" value=""  title="edit client details"/></span>
     </div>
<!--	</form> -->
</div><!--End login --> 




 <!--Start Mid --> 
  <div class="mid3 risk-mid">
 <?php
      $hidden = array('userid' => $id, 'novalidate');
      echo form_open('welcome/saveClientRiskFactorInfo','',$hidden); ?>  
   
<!--Start contain --> 
<div class="contain risk-contain">
   
   <!--Start left --> 
   <div class="left">
   		<div class="btn">
        <?php echo form_submit('mysubmit1','',"class='client_submit_form1' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form33' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
        </div>
   
   </div><!--End Left --> 
 
 <?php //print_r($fieldData); ?>
 
   <!--Start right --> 
   <div class="right-risk">
   		<div class="right-head">Risk Factors</div>
   		<div class="right-section">
     
        
       <span style="margin-top:0px !important">
       <p><strong>FAMILY HISTORY OF HEART DISEASE</strong></p>
       </span>

      <span style="margin-top:5px !important"><p class="gen">Have you ever had a male relative [father, brother, son] or a female relative [mother, sister, daughter] who has had a myocardial infarction or bypass operation, or died suddenly of a heart attack before the age of 65?</p>
          <?php  $radio_is_checked = ($fieldData[0]->option_1 === 'N')?"checked":"";
          echo form_radio(array("name"=>"options_1","id"=>"N","value"=>"N","class"=>"gender","onClick"=>"toggleDiv('content')" ,"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_1 === 'Y')?"checked":"";
echo form_radio(array("name"=>"options_1","id"=>"Y","value"=>"Y","class"=>"gender" ,"onClick"=>"toggleDiv('content')" ,"checked"=>$radio_is_checked)); ?>YES</span>
      
        <span>
       
<div id="content">
<span class="gen-width"><p class="gen-rel">Gender of relative</p>
    <?php $radio_is_checked = ($fieldData[0]->option_gender === 'M')?"checked":""; ?>
    <input type="radio" name="option_gender" value="M" <?php echo $radio_is_checked; ?>>&nbsp;Male&nbsp;&nbsp;
    <?php $radio_is_checked = ($fieldData[0]->option_gender === 'F')?"checked":""; ?>
    <input type="radio" name="option_gender" value="F" <?php echo $radio_is_checked; ?>>&nbsp;Female</span>

<span class="at-age gen-width"><p class="gen-age">At what age</p>
   <?php   $option_age = $fieldData[0]->option_age==''?'':$fieldData[0]->option_age; ?>
    <input type="number" class="physi-text-age one-decimal" name="option_age" value="<?php echo $option_age; ?>" onchange="handleChangeAge(this)"><div class="value-risk">&nbsp;yr</div></span>
</div> 
  </span>
   <span  style="margin-top:0px !important">
       <p><strong>SMOKING</strong></p>
       </span>

      <span style="margin-top:5px !important"><p class="gen">Do you smoke?</p>
          <?php $radio_is_checked = ($fieldData[0]->option_2 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_2","id"=>"no","value"=>"N",'class'=>'gender',"onClick"=>"toggleDiv2('content_option')" ,"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_2 === 'Y')?"checked":"";
echo form_radio(array("name"=>"options_2","id"=>"yes","value"=>"Y",'class'=>'gender',"onClick"=>"toggleDiv2('content_option')" ,"checked"=>$radio_is_checked )); ?>YES</span>

      <div id="content_option" style="background-color: #nnn; padding: 5px 10px;">
<span class="smoke-width"><p class="smoke-text">How many do you smoke per day?</p>
     <?php   $option_smoke = $fieldData[0]->option_smoke==''?'':$fieldData[0]->option_smoke; ?>
    <input type="number" class="physi-text-age" name="option_smoke" value="<?php echo $option_smoke; ?>"><div class="value-risk">&nbsp;/day</div></span>
</div> 

      <span style="margin-top:5px !important"><p class="gen">Have you quit smoking in the last 6 months?</p>
          <?php $radio_is_checked = ($fieldData[0]->option_3 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_3","id"=>"no","value"=>"N",'class'=>'gender',"onClick"=>"toggleDiv3('content_option2')" ,"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_3 === 'Y')?"checked":""; 
echo form_radio(array("name"=>"options_3","id"=>"yes","value"=>"Y",'class'=>'gender',"onClick"=>"toggleDiv3('content_option2')","checked"=>$radio_is_checked)); ?>YES</span>
      
      <div id="content_option2" style="background-color: #nnn; padding: 5px 10px;">
<span class="smoke-width"><p class="smoke-text">How many did you smoke per day?</p>
    <?php   $option_smoke_6 = $fieldData[0]->option_smoke_6==''?'':$fieldData[0]->option_smoke_6; ?>
    <input type="number" class="physi-text-age" name="option_smoke_6" value="<?php echo $option_smoke_6; ?>"><div class="value-risk">&nbsp;/day</div></span> 
</div>  
       <span style="margin-top:3px !important">
       <p><strong>SELF-REPORT</strong></p>
       </span>
      
      <span style="margin-top:5px !important"><p class="gen">Have you been told you have high blood pressure?</p>
          <?php $radio_is_checked = ($fieldData[0]->option_4 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_4","id"=>"no","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_4 === 'Y')?"checked":""; 
echo form_radio(array("name"=>"options_4","id"=>"yes","value"=>"Y",'class'=>'gender',"checked"=>$radio_is_checked )); ?>YES</span>
      
      <span style="margin-top:5px !important"><p class="gen">Have you been told you have high cholesterol?</p>
          <?php $radio_is_checked = ($fieldData[0]->option_5 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_5","id"=>"no","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_5 === 'Y')?"checked":""; 
echo form_radio(array("name"=>"options_5","id"=>"yes","value"=>"Y",'class'=>'gender' ,"checked"=>$radio_is_checked)); ?>YES</span>
      
      <span style="margin-top:5px !important"><p class="gen">Have you been told you have high blood sugar?</p>
          <?php $radio_is_checked = ($fieldData[0]->option_6 === 'N')?"checked":""; 
          echo form_radio(array("name"=>"options_6","id"=>"no","value"=>"N",'class'=>'gender',"checked"=>$radio_is_checked)); ?>NO
<?php $radio_is_checked = ($fieldData[0]->option_6 === 'Y')?"checked":""; 
echo form_radio(array("name"=>"options_6","id"=>"yes","value"=>"Y",'class'=>'gender',"checked"=>$radio_is_checked )); ?>YES</span>
   
       <span style="margin-top:3px !important">
       <p><strong>MEASURED</strong></p>
       </span>
       
       <div class="measured-cont">
       		<div class="measured-left">
                    <?php
                     $option_7 = $fieldData[0]->option_7==''?'':$fieldData[0]->option_7;
                     $attrib = array(
                        'name'        => 'options_7',
                        'id'          => 'options_7',
                        'class'       => 'physi-text ',
                         'size'=>'20',
                         'value'=>$option_7,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onkeyup'=>'return onkeyup(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                       //  'type'=>'number',
                         'min'=>'0','onchange'=>'handleChangeSbp(this)'
                      );
                    
                    
                    ?>
              <span><p class="gen">SBP</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mmHg</div></span>
              
              
               <?php
                $option_8 = $fieldData[0]->option_8==''?'':$fieldData[0]->option_8;
                     $attrib = array(
                        'name'        => 'options_8',
                        'id'          => 'options_8',
                        'class'       => 'physi-text  ',
                         'size'=>'20',
                         'value'=>$option_8,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                        'ondrop'=>'return false',
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                         ,'onchange'=>'handleChangeDbp(this)'
                         

                      );
                    
                    
                    ?>
              <span><p class="gen">DBP</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mmHg</div></span>
              
              
               <?php 
                $option_12 = $fieldData[0]->option_12==''?'':$fieldData[0]->option_12;
               $attrib = array(
                        'name'        => 'options_12',
                        'id'          => 'options_12',
                        'class'       => 'physi-text two-decimal ',
                         'size'=>'20',
                         'value'=>$option_12,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0',
                       //  'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                      //  'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              
              <span><p class="gen-new">Fasting blood glucose</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mM</div></span>
              
                              </div>
            
            <div class="measured-right">
               
                <?php 
                 $option_11 = $fieldData[0]->option_11==''?'':$fieldData[0]->option_11;
                $attrib = array(
                        'name'        => 'options_11',
                        'id'          => 'options_11',
                        'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_11,
                        // 'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                         

                      );
                    
                    
                    ?>
              <span><p class="gen-new">Total blood cholesterol</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mM</div></span>
              
               <?php
                $option_9 = $fieldData[0]->option_9==''?'':$fieldData[0]->option_9;
               $attrib = array(
                        'name'        => 'options_9',
                        'id'          => 'options_9',
                        'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_9,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                        // 'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                         //'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              <span><p class="gen">HDL</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mM</div></span>
               <?php  
                $option_10 = $fieldData[0]->option_10==''?'':$fieldData[0]->option_10;
               $attrib = array(
                        'name'        => 'options_10',
                        'id'          => 'options_10',
                        'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_10,
                         'type'=>'number',
                         'step'=>'any',
                         'min'=>'0'
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                         //'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              <span><p class="gen">LDL</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mM</div></span>

              
               <?php
                $option_13 = $fieldData[0]->option_13==''?'':$fieldData[0]->option_13;
               $attrib = array(
                        'name'        => 'options_13',
                        'id'          => 'options_13',
                        'class'       => 'physi-text two-decimal',
                         'size'=>'20',
                         'value'=>$option_13,
                         'type'=>'number',
                         'step'=>'any'
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                         

                      );
                    
                    
                    ?>
              <span><p class="gen">Triglycerides</p><?php echo form_input($attrib); ?><div class="value">&nbsp;mM</div></span>
            </div>
       </div>
      <!--
       <input type="submit" name="mysubmit" value="Submit!" class="client_submit">
      -->
  
      <?php echo form_close(); ?>
      
      </div><!--End right section--> 
   </div><!--End right --> 
   
   
</div><!--End contain -->
<!--
<div class="footer risk-footer">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Mid --> 
</div><!--End Wrapper --> 
 <div id="confirmBox">
    <div class="message"></div>
    <span class="button yes">New Client</span>
    <span class="button no">Cancel</span>
     </div>
</body>
</html>
