<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>   
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">              
                  <a onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';" class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a> 
                <h1 class="page_head">PHANTOM z-scores</h1>
              </td>
            </tr>
            <tr>
              <td align="center">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="cus-input" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
              </td> 
			  
              <td align="center">
                <label>Gender</label>
                <input type="text" id="gender" name="gender" class="cus-input" value="<?php if($_SESSION['user_gender'] == 'M'){ echo "Male" ;}else{ echo "Female" ;} ;?>"> 
              </td>             
              
              <td align="center">
                <label>Age (yr)</label>
                <input type="text" id="age" name="age" class="cus-input" value="<?php echo $_SESSION['age'] ;?>"> 
              </td>        
            </tr>
            
            <tr>
              <td colspan="3"> 
                <table>
                    <tr>
                      <td align="center" class="sml_text_field" width="336px">
                        <label>Height</label>
                        <input type="text" id="height" name="height" class="cus-input" value="<?php echo $phantom_Zscores["height"] ; ?>" style="width:180px;"> 
                        cm
                      </td>
                      
                      <td align="center" class="sml_text_field" width="324px">
                        <label>Body mass</label>
                        <input type="text" id="body_mass" name="body_mass" class="cus-input" value="<?php echo $phantom_Zscores["body_mass"] ; ?>"> 
                        kg
                      </td>                             
                    </tr>                    
                </table>
              </td>                             
            </tr>
            
            <tr>                
                <td colspan="3" align="center" style="padding:10px 0; padding-top: 30px;"> 
                 <table width="100%" style="padding:0;">
                      <tbody>
                        <tr>
                        	<td width="16%">&nbsp;</td>
                            <td valign="top" width="35%" style="padding:10px 0;">
                          	<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                    <thead>
                                    <tr>
                                      <td>Skinfolds</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Triceps <span id="zscore_Triceps"><?php if($phantom_Zscores["zscore_Triceps"] !== "NaN"){echo $phantom_Zscores["zscore_Triceps"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Subscapular <span id="zscore_Subscapular"><?php if($phantom_Zscores["zscore_Subscapular"] !== "NaN"){echo $phantom_Zscores["zscore_Subscapular"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                      <td>Biceps <span id="zscore_Biceps"><?php if($phantom_Zscores["zscore_Biceps"] !== "NaN"){echo $phantom_Zscores["zscore_Biceps"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Iliac crest <span id="zscore_Iliac"><?php if($phantom_Zscores["zscore_Iliac"] !== "NaN"){echo $phantom_Zscores["zscore_Iliac"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Supraspinale <span id="zscore_Supspinale"><?php if($phantom_Zscores["zscore_Supspinale"] !== "NaN"){echo $phantom_Zscores["zscore_Supspinale"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Abdominal <span id="zscore_Abdominal"><?php if($phantom_Zscores["zscore_Abdominal"] !== "NaN"){echo $phantom_Zscores["zscore_Abdominal"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Front thigh <span id="zscore_Thigh"><?php if($phantom_Zscores["zscore_Thigh"] !== "NaN"){echo $phantom_Zscores["zscore_Thigh"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Medial calf <span id="zscore_Calf"><?php if($phantom_Zscores["zscore_Calf"] !== "NaN"){echo $phantom_Zscores["zscore_Calf"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                      <td>Mid-axilla &nbsp;</td>
                                    </tr>                            
                                    </tbody>
                            </table> 
                            
                            <div class="chart_div chart_div_first">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                 <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="tricep_plot1" name="tricep_plot1" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot2" name="tricep_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot3" name="tricep_plot3" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot4" name="tricep_plot4" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot5" name="tricep_plot5" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot6" name="tricep_plot6" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot7" name="tricep_plot7" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot8" name="tricep_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot9" name="tricep_plot9" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot10" name="tricep_plot10" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot11" name="tricep_plot11" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot12" name="tricep_plot12" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot13" name="tricep_plot13" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot14" name="tricep_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot15" name="tricep_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="subscapular_plot1" name="subscapular_plot1" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot2" name="subscapular_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot3" name="subscapular_plot3" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot4" name="subscapular_plot4" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot5" name="subscapular_plot5" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot6" name="subscapular_plot6" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot7" name="subscapular_plot7" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot8" name="subscapular_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot9" name="subscapular_plot9" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot10" name="subscapular_plot10" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot11" name="subscapular_plot11" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot12" name="subscapular_plot12" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot13" name="subscapular_plot13" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot14" name="subscapular_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot15" name="subscapular_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="bicep_plot1" name="bicep_plot1" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot2" name="bicep_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot3" name="bicep_plot3" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot4" name="bicep_plot4" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot5" name="bicep_plot5" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot6" name="bicep_plot6" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot7" name="bicep_plot7" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot8" name="bicep_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot9" name="bicep_plot9" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot10" name="bicep_plot10" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot11" name="bicep_plot11" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot12" name="bicep_plot12" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot13" name="bicep_plot13" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot14" name="bicep_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot15" name="bicep_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="iliac_plot1" name="iliac_plot1" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot2" name="iliac_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot3" name="iliac_plot3" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot4" name="iliac_plot4" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot5" name="iliac_plot5" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot6" name="iliac_plot6" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot7" name="iliac_plot7" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot8" name="iliac_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot9" name="iliac_plot9" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot10" name="iliac_plot10" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot11" name="iliac_plot11" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot12" name="iliac_plot12" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot13" name="iliac_plot13" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot14" name="iliac_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot15" name="iliac_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="supspinale_plot1" name="supspinale_plot1" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot2" name="supspinale_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot3" name="supspinale_plot2" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot4" name="supspinale_plot4" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot5" name="supspinale_plot5" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot6" name="supspinale_plot6" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot7" name="supspinale_plot7" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot8" name="supspinale_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot9" name="supspinale_plot9" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot10" name="supspinale_plot10" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot11" name="supspinale_plot11" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot12" name="supspinale_plot12" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot13" name="supspinale_plot13" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot14" name="supspinale_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot15" name="supspinale_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="abdominal_plot1" name="abdominal_plot1" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot2" name="abdominal_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot3" name="abdominal_plot3" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot4" name="abdominal_plot4" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot5" name="abdominal_plot5" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot6" name="abdominal_plot6" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot7" name="abdominal_plot7" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot8" name="abdominal_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot9" name="abdominal_plot9" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot10" name="abdominal_plot10" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot11" name="abdominal_plot11" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot12" name="abdominal_plot12" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot13" name="abdominal_plot13" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot14" name="abdominal_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot15" name="abdominal_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="thigh_plot1" name="thigh_plot1" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot2" name="thigh_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot3" name="thigh_plot3" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot4" name="thigh_plot4" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot5" name="thigh_plot5" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot6" name="thigh_plot6" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot7" name="thigh_plot7" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot8" name="thigh_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot9" name="thigh_plot9" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot10" name="thigh_plot10" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot11" name="thigh_plot11" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot12" name="thigh_plot12" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot13" name="thigh_plot13" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot14" name="thigh_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot15" name="thigh_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="calf_plot1" name="calf_plot1" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot2" name="calf_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot3" name="calf_plot3" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot4" name="calf_plot4" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot5" name="calf_plot5" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot6" name="calf_plot6" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot7" name="calf_plot7" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot8" name="calf_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot9" name="calf_plot9" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot10" name="calf_plot10" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot11" name="calf_plot11" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot12" name="calf_plot12" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot13" name="calf_plot13" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot14" name="calf_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot15" name="calf_plot15" class="red_circule">&nbsp;</div>
                                        </div>                                     
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>                            
                          </td>
                          	<td width="4%">&nbsp;</td>
                          	<td valign="top" width="35%" style="padding:10px 0;">
                          		<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                  	<thead>                                  
                                    <tr> 
                                      <td>Girths</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Head <span id="zscore_HeadG" name="zscore_HeadG"><?php if($phantom_Zscores["zscore_HeadG"] !== "NaN"){echo $phantom_Zscores["zscore_HeadG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Neck <span id="zscore_NeckG" name="zscore_NeckG"><?php if($phantom_Zscores["zscore_NeckG"] !== "NaN"){echo $phantom_Zscores["zscore_NeckG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Arm (relaxed) <span id="zscore_RelArmG" name="zscore_RelArmG"><?php if($phantom_Zscores["zscore_RelArmG"] !== "NaN"){echo $phantom_Zscores["zscore_RelArmG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Arm (flexed) <span id="zscore_FlexArmG" name="zscore_FlexArmG"><?php if($phantom_Zscores["zscore_FlexArmG"] !== "NaN"){echo $phantom_Zscores["zscore_FlexArmG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Forearm <span id="zscore_ForearmG" name="zscore_ForearmG"><?php if($phantom_Zscores["zscore_ForearmG"] !== "NaN"){echo $phantom_Zscores["zscore_ForearmG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Wrist <span id="zscore_WristG" name="zscore_WristG"><?php if($phantom_Zscores["zscore_WristG"] !== "NaN"){echo $phantom_Zscores["zscore_WristG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Chest <span id="zscore_ChestG" name="zscore_ChestG"><?php if($phantom_Zscores["zscore_ChestG"] !== "NaN"){echo $phantom_Zscores["zscore_ChestG"] ;}?></span></td>
                                    </tr>                                    
                                    <tr>
                                        <td>Waist (min.) <span id="zscore_WaistG" name="zscore_WaistG"><?php if($phantom_Zscores["zscore_WaistG"] !== "NaN"){echo $phantom_Zscores["zscore_WaistG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Gluteal (hips) <span id="zscore_HipG" name="zscore_HipG"><?php if($phantom_Zscores["zscore_HipG"] !== "NaN"){echo $phantom_Zscores["zscore_HipG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Thigh <span id="zscore_ThighG" name="zscore_ThighG"><?php if($phantom_Zscores["zscore_ThighG"] !== "NaN"){echo $phantom_Zscores["zscore_ThighG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Thigh (mid) <span id="zscore_MidThighG" name="zscore_MidThighG"><?php if($phantom_Zscores["zscore_MidThighG"] !== "NaN"){echo $phantom_Zscores["zscore_MidThighG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Calf (max.) <span id="zscore_CalfG" name="zscore_CalfG"><?php if($phantom_Zscores["zscore_CalfG"] !== "NaN"){echo $phantom_Zscores["zscore_CalfG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Ankle (min.) <span id="zscore_AnkleG" name="zscore_AnkleG"><?php if($phantom_Zscores["zscore_AnkleG"] !== "NaN"){echo $phantom_Zscores["zscore_AnkleG"] ;}?></span></td>
                                    </tr>                                   
                                  </tbody>
                                </table>
                                
                                <div class="chart_div">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                        <li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                        <li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                  <div class="chart_circule_main">                                      
                                        <div class="chart_circule_row">
                                            <div id="headG_plot1" name="headG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot2" name="headG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="headG_plot3" name="headG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot4" name="headG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot5" name="headG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot6" name="headG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot7" name="headG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot8" name="headG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="headG_plot9" name="headG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot10" name="headG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot11" name="headG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot12" name="headG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot13" name="headG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="headG_plot14" name="headG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="headG_plot15" name="headG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="neckG_plot1" name="neckG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot2" name="neckG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="neckG_plot3" name="neckG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot4" name="neckG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot5" name="neckG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot6" name="neckG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot7" name="neckG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot8" name="neckG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="neckG_plot9" name="neckG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot10" name="neckG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot11" name="neckG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot12" name="neckG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot13" name="neckG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="neckG_plot14" name="neckG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="neckG_plot15" name="neckG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                    	<div class="chart_circule_row">
                                            <div id="relArmG_plot1" name="relArmG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot2" name="relArmG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot3" name="relArmG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot4" name="relArmG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot5" name="relArmG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot6" name="relArmG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot7" name="relArmG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot8" name="relArmG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot9" name="relArmG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot10" name="relArmG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot11" name="relArmG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot12" name="relArmG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot13" name="relArmG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot14" name="relArmG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot15" name="relArmG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="flexArmG_plot1" name="flexArmG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot2" name="flexArmG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot3" name="flexArmG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot4" name="flexArmG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot5" name="flexArmG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot6" name="flexArmG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot7" name="flexArmG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot8" name="flexArmG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot9" name="flexArmG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot10" name="flexArmG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot11" name="flexArmG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot12" name="flexArmG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot13" name="flexArmG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot14" name="flexArmG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot15" name="flexArmG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="forearmG_plot1" name="forearmG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot2" name="forearmG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="forearmG_plot3" name="forearmG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot4" name="forearmG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot5" name="forearmG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot6" name="forearmG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot7" name="forearmG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot8" name="forearmG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="forearmG_plot9" name="forearmG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot10" name="forearmG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot11" name="forearmG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot12" name="forearmG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot13" name="forearmG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="forearmG_plot14" name="forearmG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="forearmG_plot15" name="forearmG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="wristG_plot1" name="wristG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot2" name="wristG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="wristG_plot3" name="wristG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot4" name="wristG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot5" name="wristG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot6" name="wristG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot7" name="wristG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot8" name="wristG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="wristG_plot9" name="wristG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot10" name="wristG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot11" name="wristG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot12" name="wristG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot13" name="wristG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="wristG_plot14" name="wristG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="wristG_plot15" name="wristG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="chestG_plot1" name="chestG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot2" name="chestG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="chestG_plot3" name="chestG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot4" name="chestG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot5" name="chestG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot6" name="chestG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot7" name="chestG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot8" name="chestG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="chestG_plot9" name="chestG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot10" name="chestG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot11" name="chestG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot12" name="chestG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot13" name="chestG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="chestG_plot14" name="chestG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="chestG_plot15" name="chestG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="waistG_plot1" name="waistG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot2" name="waistG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot3" name="waistG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot4" name="waistG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot5" name="waistG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot6" name="waistG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot7" name="waistG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot8" name="waistG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot9" name="waistG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot10" name="waistG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot11" name="waistG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot12" name="waistG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot13" name="waistG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot14" name="waistG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot15" name="waistG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="hipG_plot1" name="hipG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot2" name="hipG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot3" name="hipG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot4" name="hipG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot5" name="hipG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot6" name="hipG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot7" name="hipG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot8" name="hipG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot9" name="hipG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot10" name="hipG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot11" name="hipG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot12" name="hipG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot13" name="hipG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot14" name="hipG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot15" name="hipG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="thighG_plot1" name="thighG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot2" name="thighG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="thighG_plot3" name="thighG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot4" name="thighG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot5" name="thighG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot6" name="thighG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot7" name="thighG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot8" name="thighG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="thighG_plot9" name="thighG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot10" name="thighG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot11" name="thighG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot12" name="thighG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot13" name="thighG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="thighG_plot14" name="thighG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="thighG_plot15" name="thighG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="midThighG_plot1" name="midThighG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot2" name="midThighG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="midThighG_plot3" name="midThighG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot4" name="midThighG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot5" name="midThighG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot6" name="midThighG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot7" name="midThighG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot8" name="midThighG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="midThighG_plot9" name="midThighG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot10" name="midThighG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot11" name="midThighG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot12" name="midThighG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot13" name="midThighG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="midThighG_plot14" name="midThighG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="midThighG_plot15" name="midThighG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="calfG_plot1" name="calfG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot2" name="calfG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot3" name="calfG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot4" name="calfG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot5" name="calfG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot6" name="calfG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot7" name="calfG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot8" name="calfG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot9" name="calfG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot10" name="calfG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot11" name="calfG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot12" name="calfG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot13" name="calfG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot14" name="calfG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot15" name="calfG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="ankleG_plot1" name="ankleG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot2" name="ankleG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="ankleG_plot3" name="ankleG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot4" name="ankleG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot5" name="ankleG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot6" name="ankleG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot7" name="ankleG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot8" name="ankleG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="ankleG_plot9" name="ankleG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot10" name="ankleG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot11" name="ankleG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot12" name="ankleG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot13" name="ankleG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="ankleG_plot14" name="ankleG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="ankleG_plot15" name="ankleG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>                              
                          </td>
                          	<td width="16%">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td colspan="5">&nbsp;</td>
                        </tr>
                        <tr>
                        	<td width="16%">&nbsp;</td>
                            <td valign="top" width="35%" style="padding:10px 0;">
                          	<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                    <thead>
                                    <tr>
                                      <td>Lengths</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Acromiale-radiale <span id="zscore_AcRad"><?php if($phantom_Zscores["zscore_AcRad"] !== "NaN"){echo $phantom_Zscores["zscore_AcRad"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Radiale-styl <span id="zscore_RadStyl"><?php if($phantom_Zscores["zscore_RadStyl"] !== "NaN"){echo $phantom_Zscores["zscore_RadStyl"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                      <td>Midstyl-dacty <span id="zscore_midStylDact"><?php if($phantom_Zscores["zscore_midStylDact"] !== "NaN"){echo $phantom_Zscores["zscore_midStylDact"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Iliospinale <span id="zscore_Iliospinale"><?php if($phantom_Zscores["zscore_Iliospinale"] !== "NaN"){echo $phantom_Zscores["zscore_Iliospinale"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Trochanterion <span id="zscore_Troch"><?php if($phantom_Zscores["zscore_Troch"] !== "NaN"){echo $phantom_Zscores["zscore_Troch"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Trochant-tibiale <span id="zscore_TrochTib"><?php if($phantom_Zscores["zscore_TrochTib"] !== "NaN"){echo $phantom_Zscores["zscore_TrochTib"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Tibiale laterale <span id="zscore_TibLat"><?php if($phantom_Zscores["zscore_TibLat"] !== "NaN"){echo $phantom_Zscores["zscore_TibLat"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Tib med-sphy tib <span id="zscore_TibMed"><?php if($phantom_Zscores["zscore_TibMed"] !== "NaN"){echo $phantom_Zscores["zscore_TibMed"] ;}?></span></td>
                                    </tr>                                                             
                                    </tbody>
                            </table> 
                            
                            <div class="chart_div chart_div_first">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                 <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="acRad_plot1" name="acRad_plot1" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot2" name="acRad_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="acRad_plot3" name="acRad_plot3" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot4" name="acRad_plot4" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot5" name="acRad_plot5" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot6" name="acRad_plot6" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot7" name="acRad_plot7" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot8" name="acRad_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="acRad_plot9" name="acRad_plot9" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot10" name="acRad_plot10" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot11" name="acRad_plot11" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot12" name="acRad_plot12" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot13" name="acRad_plot13" class="red_circule">&nbsp;</div>
                                            <div id="acRad_plot14" name="acRad_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="acRad_plot15" name="acRad_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="radStyl_plot1" name="radStyl_plot1" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot2" name="radStyl_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="radStyl_plot3" name="radStyl_plot3" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot4" name="radStyl_plot4" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot5" name="radStyl_plot5" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot6" name="radStyl_plot6" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot7" name="radStyl_plot7" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot8" name="radStyl_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="radStyl_plot9" name="radStyl_plot9" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot10" name="radStyl_plot10" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot11" name="radStyl_plot11" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot12" name="radStyl_plot12" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot13" name="radStyl_plot13" class="red_circule">&nbsp;</div>
                                            <div id="radStyl_plot14" name="radStyl_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="radStyl_plot15" name="radStyl_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="midStylDact_plot1" name="midStylDact_plot1" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot2" name="midStylDact_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="midStylDact_plot3" name="midStylDact_plot3" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot4" name="midStylDact_plot4" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot5" name="midStylDact_plot5" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot6" name="midStylDact_plot6" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot7" name="midStylDact_plot7" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot8" name="midStylDact_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="midStylDact_plot9" name="midStylDact_plot9" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot10" name="midStylDact_plot10" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot11" name="midStylDact_plot11" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot12" name="midStylDact_plot12" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot13" name="midStylDact_plot13" class="red_circule">&nbsp;</div>
                                            <div id="midStylDact_plot14" name="midStylDact_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="midStylDact_plot15" name="midStylDact_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="iliospinale_plot1" name="iliospinale_plot1" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot2" name="iliospinale_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="iliospinale_plot3" name="iliospinale_plot3" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot4" name="iliospinale_plot4" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot5" name="iliospinale_plot5" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot6" name="iliospinale_plot6" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot7" name="iliospinale_plot7" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot8" name="iliospinale_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="iliospinale_plot9" name="iliospinale_plot9" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot10" name="iliospinale_plot10" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot11" name="iliospinale_plot11" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot12" name="iliospinale_plot12" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot13" name="iliospinale_plot13" class="red_circule">&nbsp;</div>
                                            <div id="iliospinale_plot14" name="iliospinale_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="iliospinale_plot15" name="iliospinale_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="troch_plot1" name="troch_plot1" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot2" name="troch_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="troch_plot3" name="troch_plot3" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot4" name="troch_plot4" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot5" name="troch_plot5" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot6" name="troch_plot6" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot7" name="troch_plot7" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot8" name="troch_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="troch_plot9" name="troch_plot9" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot10" name="troch_plot10" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot11" name="troch_plot11" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot12" name="troch_plot12" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot13" name="troch_plot13" class="red_circule">&nbsp;</div>
                                            <div id="troch_plot14" name="troch_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="troch_plot15" name="troch_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="trochTib_plot1" name="trochTib_plot1" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot2" name="trochTib_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="trochTib_plot3" name="trochTib_plot3" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot4" name="trochTib_plot4" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot5" name="trochTib_plot5" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot6" name="trochTib_plot6" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot7" name="trochTib_plot7" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot8" name="trochTib_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="trochTib_plot9" name="trochTib_plot9" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot10" name="trochTib_plot10" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot11" name="trochTib_plot11" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot12" name="trochTib_plot12" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot13" name="trochTib_plot13" class="red_circule">&nbsp;</div>
                                            <div id="trochTib_plot14" name="trochTib_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="trochTib_plot15" name="trochTib_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="tibLat_plot1" name="tibLat_plot1" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot2" name="tibLat_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="tibLat_plot3" name="tibLat_plot3" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot4" name="tibLat_plot4" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot5" name="tibLat_plot5" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot6" name="tibLat_plot6" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot7" name="tibLat_plot7" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot8" name="tibLat_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="tibLat_plot9" name="tibLat_plot9" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot10" name="tibLat_plot10" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot11" name="tibLat_plot11" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot12" name="tibLat_plot12" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot13" name="tibLat_plot13" class="red_circule">&nbsp;</div>
                                            <div id="tibLat_plot14" name="tibLat_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="tibLat_plot15" name="tibLat_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="tibMed_plot1" name="tibMed_plot1" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot2" name="tibMed_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="tibMed_plot3" name="tibMed_plot3" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot4" name="tibMed_plot4" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot5" name="tibMed_plot5" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot6" name="tibMed_plot6" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot7" name="tibMed_plot7" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot8" name="tibMed_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="tibMed_plot9" name="tibMed_plot9" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot10" name="tibMed_plot10" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot11" name="tibMed_plot11" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot12" name="tibMed_plot12" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot13" name="tibMed_plot13" class="red_circule">&nbsp;</div>
                                            <div id="tibMed_plot14" name="tibMed_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="tibMed_plot15" name="tibMed_plot15" class="red_circule">&nbsp;</div>
                                        </div>                                     
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>                            
                          </td>
                          	<td width="4%">&nbsp;</td>
                          	<td valign="top" width="35%" style="padding:10px 0;">
                          		<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                  	<thead>                                  
                                    <tr> 
                                      <td>Girths</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Biacromial <span id="zscore_Biac"><?php if($phantom_Zscores["zscore_Biac"] !== "NaN"){echo $phantom_Zscores["zscore_Biac"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Bideltoid <span id="zscore_Bideltoid"><?php if($phantom_Zscores["zscore_Bideltoid"] !== "NaN"){echo $phantom_Zscores["zscore_Bideltoid"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                      <td>Biiliocristal <span id="zscore_Billio"><?php if($phantom_Zscores["zscore_Billio"] !== "NaN"){echo $phantom_Zscores["zscore_Billio"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Bitrochanteric <span id="zscore_Bitrochanteric"><?php if($phantom_Zscores["zscore_Bitrochanteric"] !== "NaN"){echo $phantom_Zscores["zscore_Bitrochanteric"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Foot length <span id="zscore_Foot"><?php if($phantom_Zscores["zscore_Foot"] !== "NaN"){echo $phantom_Zscores["zscore_Foot"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Sitting height <span id="zscore_Sitting"><?php if($phantom_Zscores["zscore_Sitting"] !== "NaN"){echo $phantom_Zscores["zscore_Sitting"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Transverse chest <span id="zscore_TrChest"><?php if($phantom_Zscores["zscore_TrChest"] !== "NaN"){echo $phantom_Zscores["zscore_TrChest"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>A-P chest <span id="zscore_ApChest"><?php if($phantom_Zscores["zscore_ApChest"] !== "NaN"){echo $phantom_Zscores["zscore_ApChest"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Arm span <span id="zscore_ArmSpan"><?php if($phantom_Zscores["zscore_ArmSpan"] !== "NaN"){echo $phantom_Zscores["zscore_ArmSpan"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Humerus (bi-epi) <span id="zscore_Humerus"><?php if($phantom_Zscores["zscore_Humerus"] !== "NaN"){echo $phantom_Zscores["zscore_Humerus"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                      <td>Femur (bi-epi) <span id="zscore_Femur"><?php if($phantom_Zscores["zscore_Femur"] !== "NaN"){echo $phantom_Zscores["zscore_Femur"] ;}?></span></td>
                                    </tr> 
                                  </tbody>
                                </table>
                                
                                <div class="chart_div">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                        <li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                  <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="biac_plot1" name="biac_plot1" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot2" name="biac_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="biac_plot3" name="biac_plot3" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot4" name="biac_plot4" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot5" name="biac_plot5" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot6" name="biac_plot6" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot7" name="biac_plot7" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot8" name="biac_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="biac_plot9" name="biac_plot9" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot10" name="biac_plot10" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot11" name="biac_plot11" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot12" name="biac_plot12" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot13" name="biac_plot13" class="red_circule">&nbsp;</div>
                                            <div id="biac_plot14" name="biac_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="biac_plot15" name="biac_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="bideltoid_plot1" name="bideltoid_plot1" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot2" name="bideltoid_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="bideltoid_plot3" name="bideltoid_plot3" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot4" name="bideltoid_plot4" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot5" name="bideltoid_plot5" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot6" name="bideltoid_plot6" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot7" name="bideltoid_plot7" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot8" name="bideltoid_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="bideltoid_plot9" name="bideltoid_plot9" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot10" name="bideltoid_plot10" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot11" name="bideltoid_plot11" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot12" name="bideltoid_plot12" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot13" name="bideltoid_plot13" class="red_circule">&nbsp;</div>
                                            <div id="bideltoid_plot14" name="bideltoid_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="bideltoid_plot15" name="bideltoid_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="billio_plot1" name="billio_plot1" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot2" name="billio_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="billio_plot3" name="billio_plot3" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot4" name="billio_plot4" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot5" name="billio_plot5" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot6" name="billio_plot6" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot7" name="billio_plot7" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot8" name="billio_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="billio_plot9" name="billio_plot9" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot10" name="billio_plot10" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot11" name="billio_plot11" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot12" name="billio_plot12" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot13" name="billio_plot13" class="red_circule">&nbsp;</div>
                                            <div id="billio_plot14" name="billio_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="billio_plot15" name="billio_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="bitrochanteric_plot1" name="bitrochanteric_plot1" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot2" name="bitrochanteric_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="bitrochanteric_plot3" name="bitrochanteric_plot3" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot4" name="bitrochanteric_plot4" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot5" name="bitrochanteric_plot5" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot6" name="bitrochanteric_plot6" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot7" name="bitrochanteric_plot7" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot8" name="bitrochanteric_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="bitrochanteric_plot9" name="bitrochanteric_plot9" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot10" name="bitrochanteric_plot10" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot11" name="bitrochanteric_plot11" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot12" name="bitrochanteric_plot12" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot13" name="bitrochanteric_plot13" class="red_circule">&nbsp;</div>
                                            <div id="bitrochanteric_plot14" name="bitrochanteric_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="bitrochanteric_plot15" name="bitrochanteric_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="foot_plot1" name="foot_plot1" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot2" name="foot_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="foot_plot3" name="foot_plot3" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot4" name="foot_plot4" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot5" name="foot_plot5" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot6" name="foot_plot6" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot7" name="foot_plot7" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot8" name="foot_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="foot_plot9" name="foot_plot9" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot10" name="foot_plot10" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot11" name="foot_plot11" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot12" name="foot_plot12" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot13" name="foot_plot13" class="red_circule">&nbsp;</div>
                                            <div id="foot_plot14" name="foot_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="foot_plot15" name="foot_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="sitting_plot1" name="sitting_plot1" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot2" name="sitting_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="sitting_plot3" name="sitting_plot3" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot4" name="sitting_plot4" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot5" name="sitting_plot5" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot6" name="sitting_plot6" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot7" name="sitting_plot7" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot8" name="sitting_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="sitting_plot9" name="sitting_plot9" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot10" name="sitting_plot10" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot11" name="sitting_plot11" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot12" name="sitting_plot12" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot13" name="sitting_plot13" class="red_circule">&nbsp;</div>
                                            <div id="sitting_plot14" name="sitting_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="sitting_plot15" name="sitting_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="trChest_plot1" name="trChest_plot1" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot2" name="trChest_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="trChest_plot3" name="trChest_plot3" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot4" name="trChest_plot4" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot5" name="trChest_plot5" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot6" name="trChest_plot6" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot7" name="trChest_plot7" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot8" name="trChest_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="trChest_plot9" name="trChest_plot9" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot10" name="trChest_plot10" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot11" name="trChest_plot11" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot12" name="trChest_plot12" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot13" name="trChest_plot13" class="red_circule">&nbsp;</div>
                                            <div id="trChest_plot14" name="trChest_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="trChest_plot15" name="trChest_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="apChest_plot1" name="apChest_plot1" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot2" name="apChest_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="apChest_plot3" name="apChest_plot3" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot4" name="apChest_plot4" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot5" name="apChest_plot5" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot6" name="apChest_plot6" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot7" name="apChest_plot7" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot8" name="apChest_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="apChest_plot9" name="apChest_plot9" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot10" name="apChest_plot10" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot11" name="apChest_plot11" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot12" name="apChest_plot12" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot13" name="apChest_plot13" class="red_circule">&nbsp;</div>
                                            <div id="apChest_plot14" name="apChest_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="apChest_plot15" name="apChest_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="armSpan_plot1" name="armSpan_plot1" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot2" name="armSpan_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="armSpan_plot3" name="armSpan_plot3" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot4" name="armSpan_plot4" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot5" name="armSpan_plot5" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot6" name="armSpan_plot6" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot7" name="armSpan_plot7" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot8" name="armSpan_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="armSpan_plot9" name="armSpan_plot9" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot10" name="armSpan_plot10" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot11" name="armSpan_plot11" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot12" name="armSpan_plot12" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot13" name="armSpan_plot13" class="red_circule">&nbsp;</div>
                                            <div id="armSpan_plot14" name="armSpan_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="armSpan_plot15" name="armSpan_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="humerus_plot1" name="humerus_plot1" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot2" name="humerus_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot3" name="humerus_plot3" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot4" name="humerus_plot4" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot5" name="humerus_plot5" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot6" name="humerus_plot6" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot7" name="humerus_plot7" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot8" name="humerus_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot9" name="humerus_plot9" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot10" name="humerus_plot10" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot11" name="humerus_plot11" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot12" name="humerus_plot12" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot13" name="humerus_plot13" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot14" name="humerus_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot15" name="humerus_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="femur_plot1" name="femur_plot1" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot2" name="femur_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot3" name="femur_plot3" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot4" name="femur_plot4" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot5" name="femur_plot5" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot6" name="femur_plot6" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot7" name="femur_plot7" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot8" name="femur_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot9" name="femur_plot9" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot10" name="femur_plot10" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot11" name="femur_plot11" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot12" name="femur_plot12" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot13" name="femur_plot13" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot14" name="femur_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot15" name="femur_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>                              
                          </td>
                          	<td width="16%">&nbsp;</td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
              <td align="left">&nbsp;</td> 
			  
              <td align="right" colspan="2">                  
	         <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">     
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script>   


<script>
 
 $(window).bind("load", function() {   
   
   
    var zscore_Triceps = document.getElementById("zscore_Triceps").innerHTML ;           
        
        if(zscore_Triceps !== "")
        {
          if(zscore_Triceps < -3)
          {    
            document.getElementById('tricep_plot1').style.opacity = "1" ;  
            var start1 = document.getElementById('tricep_plot1');
          }
          else if(zscore_Triceps == -3)
          {    
            document.getElementById('tricep_plot2').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot2');
          }
          else if(zscore_Triceps < -2 && zscore_Triceps > -3)
          {    
            document.getElementById('tricep_plot3').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot3');
          } 
          else if(zscore_Triceps == -2)
          {    
            document.getElementById('tricep_plot4').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot4');
          }
          else if(zscore_Triceps < -1 && zscore_Triceps > -2)
          {    
            document.getElementById('tricep_plot5').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot5');
          }
          else if(zscore_Triceps == -1)
          {    
            document.getElementById('tricep_plot6').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot6');
          }
          else if(zscore_Triceps < 0 && zscore_Triceps > -1)
          {    
            document.getElementById('tricep_plot7').style.opacity = "1" ;  
            var start1 = document.getElementById('tricep_plot7');
          } 
          else if(zscore_Triceps == 0)
          {    
            document.getElementById('tricep_plot8').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot8');
          }
          else if(zscore_Triceps < 1 && zscore_Triceps > 0)
          {    
            document.getElementById('tricep_plot9').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot9');
          }
          else if(zscore_Triceps == 1)
          {    
            document.getElementById('tricep_plot10').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot10');
          }
          else if(zscore_Triceps < 2 && zscore_Triceps > 1)
          {    
            document.getElementById('tricep_plot11').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot11');
          } 
          else if(zscore_Triceps == 2)
          {    
            document.getElementById('tricep_plot12').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot12');
          }
          else if(zscore_Triceps < 3 && zscore_Triceps > 2)
          {    
            document.getElementById('tricep_plot13').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot13');
          } 
          else if(zscore_Triceps == 3)
          {    
            document.getElementById('tricep_plot14').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot14');
          }
          else if(zscore_Triceps > 3)
          {    
            document.getElementById('tricep_plot15').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot15');
          } 
        } 
        
     
     var zscore_Subscapular = document.getElementById("zscore_Subscapular").innerHTML ;
     
        if(zscore_Subscapular !== "")
        {
          if(zscore_Subscapular < -3)
          {    
            document.getElementById('subscapular_plot1').style.opacity = "1" ;  
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot1'), "#ec6565", 1);  
            }        
            var start2 = document.getElementById('subscapular_plot1') ;
          }
          else if(zscore_Subscapular == -3)
          {    
            document.getElementById('subscapular_plot2').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot2'), "#ec6565", 1);  
            }               
            var start2 = document.getElementById('subscapular_plot2') ;
          }
          else if(zscore_Subscapular < -2 && zscore_Subscapular > -3)
          {    
            document.getElementById('subscapular_plot3').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot3'), "#ec6565", 1);  
            }            
            var start2 = document.getElementById('subscapular_plot3') ;
          } 
          else if(zscore_Subscapular == -2)
          {    
            document.getElementById('subscapular_plot4').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot4'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot4') ;
          }
          else if(zscore_Subscapular < -1 && zscore_Subscapular > -2)
          {    
            document.getElementById('subscapular_plot5').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot5'), "#ec6565", 1); 
            }            
            var start2 = document.getElementById('subscapular_plot5') ;
          }
          else if(zscore_Subscapular == -1)
          {    
            document.getElementById('subscapular_plot6').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot6'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot6') ;
          }
          else if(zscore_Subscapular < 0 && zscore_Subscapular > -1)
          {    
            document.getElementById('subscapular_plot7').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot7'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot7') ;
          } 
          else if(zscore_Subscapular == 0)
          {    
            document.getElementById('subscapular_plot8').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot8'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot8') ;
          }
          else if(zscore_Subscapular < 1 && zscore_Subscapular > 0)
          {    
            document.getElementById('subscapular_plot9').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot9'), "#ec6565", 1); 
            }            
            var start2 = document.getElementById('subscapular_plot9') ;
          }
          else if(zscore_Subscapular == 1)
          {    
            document.getElementById('subscapular_plot10').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot10'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot10') ;
          }
          else if(zscore_Subscapular < 2 && zscore_Subscapular > 1)
          {    
            document.getElementById('subscapular_plot11').style.opacity = "1" ;   
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot11'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot11') ;
          } 
          else if(zscore_Subscapular == 2)
          {    
            document.getElementById('subscapular_plot12').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot12'), "#ec6565", 1);  
            }            
            var start2 = document.getElementById('subscapular_plot12') ;
          }
          else if(zscore_Subscapular < 3 && zscore_Subscapular > 2)
          {    
            document.getElementById('subscapular_plot13').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot13'), "#ec6565", 1);   
            }             
            var start2 = document.getElementById('subscapular_plot13') ;
          } 
          else if(zscore_Subscapular == 3)
          {    
            document.getElementById('subscapular_plot14').style.opacity = "1" ;   
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot14'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot14') ;
          }
          else if(zscore_Subscapular > 3)
          {    
            document.getElementById('subscapular_plot15').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot15'), "#ec6565", 1); 
            }              
            var start2 = document.getElementById('subscapular_plot15') ;
          }          
         
        }          
         
         
      
     var zscore_Biceps = document.getElementById("zscore_Biceps").innerHTML ;    
     
        if(zscore_Biceps !== "")
        {            
          if(zscore_Biceps < -3)
          {    
            document.getElementById('bicep_plot1').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot1'), "#ec6565", 1);
            }            
            var start3 = document.getElementById('bicep_plot1') ;
          }
          else if(zscore_Biceps == -3)
          {    
            document.getElementById('bicep_plot2').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot2'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot2') ;
          }
          else if(zscore_Biceps < -2 && zscore_Biceps > -3)
          {    
            document.getElementById('bicep_plot3').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot3'), "#ec6565", 1);            
            } 
            var start3 = document.getElementById('bicep_plot3') ;
          } 
          else if(zscore_Biceps == -2)
          {    
            document.getElementById('bicep_plot4').style.opacity = "1" ;     
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot4'), "#ec6565", 1);            
            }             
            var start3 = document.getElementById('bicep_plot4') ;
          }
          else if(zscore_Biceps < -1 && zscore_Biceps > -2)
          {    
            document.getElementById('bicep_plot5').style.opacity = "1" ;  
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot5'), "#ec6565", 1);            
            }             
            var start3 = document.getElementById('bicep_plot5') ;
          }
          else if(zscore_Biceps == -1)
          {    
            document.getElementById('bicep_plot6').style.opacity = "1" ;   
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot6'), "#ec6565", 1);           
            }            
            var start3 = document.getElementById('bicep_plot6') ;
          }
          else if(zscore_Biceps < 0 && zscore_Biceps > -1)
          {    
            document.getElementById('bicep_plot7').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot7'), "#ec6565", 1);          
            }            
            var start3 = document.getElementById('bicep_plot7') ;
          } 
          else if(zscore_Biceps == 0)
          {    
            document.getElementById('bicep_plot8').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot8'), "#ec6565", 1);       
            }             
            var start3 = document.getElementById('bicep_plot8') ;
          }
          else if(zscore_Biceps < 1 && zscore_Biceps > 0)
          {    
            document.getElementById('bicep_plot9').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot9'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot9') ;
          }
          else if(zscore_Biceps == 1)
          {    
            document.getElementById('bicep_plot10').style.opacity = "1" ;          
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot10'), "#ec6565", 1);
            }            
            var start3 = document.getElementById('bicep_plot10') ;
          }
          else if(zscore_Biceps < 2 && zscore_Biceps > 1)
          {    
            document.getElementById('bicep_plot11').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot11'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot11') ;
          } 
          else if(zscore_Biceps == 2)
          {    
            document.getElementById('bicep_plot12').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot12'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot12') ;
          }
          else if(zscore_Biceps < 3 && zscore_Biceps > 2)
          {    
            document.getElementById('bicep_plot13').style.opacity = "1" ;          
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot13'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot13') ;
          } 
          else if(zscore_Biceps == 3)
          {    
            document.getElementById('bicep_plot14').style.opacity = "1" ;    
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot14'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot14') ;
          }
          else if(zscore_Biceps > 3)
          {    
            document.getElementById('bicep_plot15').style.opacity = "1" ;   
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot15'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot15') ;
          }         
        
        }        
       
       

     var zscore_Iliac = document.getElementById("zscore_Iliac").innerHTML ;
     
        if(zscore_Iliac !== "")
        {
          if(zscore_Iliac < -3)
          {    
            document.getElementById('iliac_plot1').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot1'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot1') ;
          }
          else if(zscore_Iliac == -3)
          {    
            document.getElementById('iliac_plot2').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot2'), "#ec6565", 1);
            }             
            var start4 = document.getElementById('iliac_plot2') ;
          }
          else if(zscore_Iliac < -2 && zscore_Iliac > -3)
          {    
            document.getElementById('iliac_plot3').style.opacity = "1" ;  
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot3'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot3') ;
          } 
          else if(zscore_Iliac == -2)
          {    
            document.getElementById('iliac_plot4').style.opacity = "1" ;   
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot4'), "#ec6565", 1);
            }             
            var start4 = document.getElementById('iliac_plot4') ;
          }
          else if(zscore_Iliac < -1 && zscore_Iliac > -2)
          {    
            document.getElementById('iliac_plot5').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot5'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot5') ;
          }
          else if(zscore_Iliac == -1)
          {    
            document.getElementById('iliac_plot6').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot6'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot6') ;
          }
          else if(zscore_Iliac < 0 && zscore_Iliac > -1)
          {    
            document.getElementById('iliac_plot7').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot7'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot7') ;
          } 
          else if(zscore_Iliac == 0)
          {    
            document.getElementById('iliac_plot8').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot8'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot8') ;
          }
          else if(zscore_Iliac < 1 && zscore_Iliac > 0)
          {    
            document.getElementById('iliac_plot9').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot9'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot9') ;
          }
          else if(zscore_Iliac == 1)
          {    
            document.getElementById('iliac_plot10').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot10'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot10') ;
          }
          else if(zscore_Iliac < 2 && zscore_Iliac > 1)
          {    
            document.getElementById('iliac_plot11').style.opacity = "1" ;   
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot11'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot11') ;
          } 
          else if(zscore_Iliac == 2)
          {    
            document.getElementById('iliac_plot12').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot12'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot12') ;
          }
          else if(zscore_Iliac < 3 && zscore_Iliac > 2)
          {    
            document.getElementById('iliac_plot13').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot13'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot13') ;
          } 
          else if(zscore_Iliac == 3)
          {    
            document.getElementById('iliac_plot14').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot14'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot14') ;
          }
          else if(zscore_Iliac > 3)
          {    
            document.getElementById('iliac_plot15').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot15'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot15') ;
          } 
          
        }
   
        

     var zscore_Supspinale = document.getElementById("zscore_Supspinale").innerHTML ;
     
        if(zscore_Supspinale !== "")
        {
          if(zscore_Supspinale < -3)
          {    
            document.getElementById('supspinale_plot1').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot1'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot1') ;
          }
          else if(zscore_Supspinale == -3)
          {    
            document.getElementById('supspinale_plot2').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot2'), "#ec6565", 1);
            }             
            var start5 = document.getElementById('supspinale_plot2') ;
          }
          else if(zscore_Supspinale < -2 && zscore_Supspinale > -3)
          {    
            document.getElementById('supspinale_plot3').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot3'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot3') ;
          } 
          else if(zscore_Supspinale == -2)
          {    
            document.getElementById('supspinale_plot4').style.opacity = "1" ;   
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot4'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot4') ;
          }
          else if(zscore_Supspinale < -1 && zscore_Supspinale > -2)
          {    
            document.getElementById('supspinale_plot5').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot5'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot5') ;
          }
          else if(zscore_Supspinale == -1)
          {    
            document.getElementById('supspinale_plot6').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot6'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot6') ;
          }
          else if(zscore_Supspinale < 0 && zscore_Supspinale > -1)
          {    
            document.getElementById('supspinale_plot7').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot7'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot7') ;
          } 
          else if(zscore_Supspinale == 0)
          {    
            document.getElementById('supspinale_plot8').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot8'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot8') ;
          }
          else if(zscore_Supspinale < 1 && zscore_Supspinale > 0)
          {    
            document.getElementById('supspinale_plot9').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot9'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot9') ;
          }
          else if(zscore_Supspinale == 1)
          {    
            document.getElementById('supspinale_plot10').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot10'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot10') ;
          }
          else if(zscore_Supspinale < 2 && zscore_Supspinale > 1)
          {    
            document.getElementById('supspinale_plot11').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot11'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot11') ;
          } 
          else if(zscore_Supspinale == 2)
          {    
            document.getElementById('supspinale_plot12').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot12'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot12') ;
          }
          else if(zscore_Supspinale < 3 && zscore_Supspinale > 2)
          {    
            document.getElementById('supspinale_plot13').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot13'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot13') ;
          } 
          else if(zscore_Supspinale == 3)
          {    
            document.getElementById('supspinale_plot14').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot14'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot14') ;
          }
          else if(zscore_Supspinale > 3)
          {    
            document.getElementById('supspinale_plot15').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot15'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot15') ;
          } 
          
        }
        

     var zscore_Abdominal = document.getElementById("zscore_Abdominal").innerHTML ;
     
        if(zscore_Abdominal !== "")
        {
          if(zscore_Abdominal < -3)
          {    
            document.getElementById('abdominal_plot1').style.opacity = "1" ; 
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot1'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot1') ;
          }
          else if(zscore_Abdominal == -3)
          {    
            document.getElementById('abdominal_plot2').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot2'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot2') ;
          }
          else if(zscore_Abdominal < -2 && zscore_Abdominal > -3)
          {    
            document.getElementById('abdominal_plot3').style.opacity = "1" ;  
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot3'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot3') ;
          } 
          else if(zscore_Abdominal == -2)
          {    
            document.getElementById('abdominal_plot4').style.opacity = "1" ;   
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot4'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot4') ;
          }
          else if(zscore_Abdominal < -1 && zscore_Abdominal > -2)
          {    
            document.getElementById('abdominal_plot5').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot5'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot5') ;
          }
          else if(zscore_Abdominal == -1)
          {    
            document.getElementById('abdominal_plot6').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot6'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot6') ;
          }
          else if(zscore_Abdominal < 0 && zscore_Abdominal > -1)
          {    
            document.getElementById('abdominal_plot7').style.opacity = "1" ;
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot7'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot7') ;
          } 
          else if(zscore_Abdominal == 0)
          {    
            document.getElementById('abdominal_plot8').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot8'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot8') ;
          }
          else if(zscore_Abdominal < 1 && zscore_Abdominal > 0)
          {    
            document.getElementById('abdominal_plot9').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot9'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot9') ;
          }
          else if(zscore_Abdominal == 1)
          {    
            document.getElementById('abdominal_plot10').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot10'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot10') ;
          }
          else if(zscore_Abdominal < 2 && zscore_Abdominal > 1)
          {    
            document.getElementById('abdominal_plot11').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot11'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot11') ;
          } 
          else if(zscore_Abdominal == 2)
          {    
            document.getElementById('abdominal_plot12').style.opacity = "1" ;
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot12'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot12') ;
          }
          else if(zscore_Abdominal < 3 && zscore_Abdominal > 2)
          {    
            document.getElementById('abdominal_plot13').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot13'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot13') ;
          } 
          else if(zscore_Abdominal == 3)
          {    
            document.getElementById('abdominal_plot14').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot14'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot14') ;
          }
          else if(zscore_Abdominal > 3)
          {    
            document.getElementById('abdominal_plot15').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot15'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot15') ;
          } 
          
        }


     var zscore_Thigh = document.getElementById("zscore_Thigh").innerHTML ;
     
        if(zscore_Thigh !== "")
        {
          if(zscore_Thigh < -3)
          {    
            document.getElementById('thigh_plot1').style.opacity = "1" ;  
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot1'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot1') ;
          }
          else if(zscore_Thigh == -3)
          {    
            document.getElementById('thigh_plot2').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot2'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot2') ;
          }
          else if(zscore_Thigh < -2 && zscore_Thigh > -3)
          {    
            document.getElementById('thigh_plot3').style.opacity = "1" ;  
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot3'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot3') ;
          } 
          else if(zscore_Thigh == -2)
          {    
            document.getElementById('thigh_plot4').style.opacity = "1" ;   
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot4'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot4') ;
          }
          else if(zscore_Thigh < -1 && zscore_Thigh > -2)
          {    
            document.getElementById('thigh_plot5').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot5'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot5') ;
          }
          else if(zscore_Thigh == -1)
          {    
            document.getElementById('thigh_plot6').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot6'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot6') ;
          }
          else if(zscore_Thigh < 0 && zscore_Thigh > -1)
          {    
            document.getElementById('thigh_plot7').style.opacity = "1" ; 
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot7'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot7') ;
          } 
          else if(zscore_Thigh == 0)
          {    
            document.getElementById('thigh_plot8').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot8'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot8') ;
          }
          else if(zscore_Thigh < 1 && zscore_Thigh > 0)
          {    
            document.getElementById('thigh_plot9').style.opacity = "1" ;    
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot9'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot9') ;
          }
          else if(zscore_Thigh == 1)
          {    
            document.getElementById('thigh_plot10').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot10'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot10') ;
          }
          else if(zscore_Thigh < 2 && zscore_Thigh > 1)
          {    
            document.getElementById('thigh_plot11').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot11'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot11') ;
          } 
          else if(zscore_Thigh == 2)
          {    
            document.getElementById('thigh_plot12').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot12'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot12') ;
          }
          else if(zscore_Thigh < 3 && zscore_Thigh > 2)
          {    
            document.getElementById('thigh_plot13').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot13'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot13') ;
          } 
          else if(zscore_Thigh == 3)
          {    
            document.getElementById('thigh_plot14').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot14'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot14') ;
          }
          else if(zscore_Thigh > 3)
          {    
            document.getElementById('thigh_plot15').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot15'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot15') ;
          } 
          
        }
        

     var zscore_Calf = document.getElementById("zscore_Calf").innerHTML ;
     
        if(zscore_Calf !== "")
        {
          if(zscore_Calf < -3)
          {    
            document.getElementById('calf_plot1').style.opacity = "1" ;  
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot1'), "#ec6565", 1);           
            }            
          }
          else if(zscore_Calf == -3)
          {    
            document.getElementById('calf_plot2').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot2'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < -2 && zscore_Calf > -3)
          {    
            document.getElementById('calf_plot3').style.opacity = "1" ;  
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot3'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == -2)
          {    
            document.getElementById('calf_plot4').style.opacity = "1" ;   
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot4'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < -1 && zscore_Calf > -2)
          {    
            document.getElementById('calf_plot5').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot5'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf == -1)
          {    
            document.getElementById('calf_plot6').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot6'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < 0 && zscore_Calf > -1)
          {    
            document.getElementById('calf_plot7').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot7'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 0)
          {    
            document.getElementById('calf_plot8').style.opacity = "1" ; 
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot8'), "#ec6565", 1);
            }       
          }
          else if(zscore_Calf < 1 && zscore_Calf > 0)
          {    
            document.getElementById('calf_plot9').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot9'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf == 1)
          {    
            document.getElementById('calf_plot10').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot10'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < 2 && zscore_Calf > 1)
          {    
            document.getElementById('calf_plot11').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot11'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 2)
          {    
            document.getElementById('calf_plot12').style.opacity = "1" ; 
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot12'), "#ec6565", 1);           
            }            
          }
          else if(zscore_Calf < 3 && zscore_Calf > 2)
          {    
            document.getElementById('calf_plot13').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot13'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 3)
          {    
            document.getElementById('calf_plot14').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot14'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf > 3)
          {    
            document.getElementById('calf_plot15').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot15'), "#ec6565", 1);            
            }            
          } 
          
        }
    
    
    var zscore_HeadG = document.getElementById("zscore_HeadG").innerHTML ;
     
        if(zscore_HeadG !== "")
        {
          if(zscore_HeadG < -3)
          {    
            document.getElementById('headG_plot1').style.opacity = "1" ;  
            var start8 = document.getElementById('headG_plot1') ;           
          }
          else if(zscore_HeadG == -3)
          {    
            document.getElementById('headG_plot2').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot2') ;  
          }
          else if(zscore_HeadG < -2 && zscore_HeadG > -3)
          {    
            document.getElementById('headG_plot3').style.opacity = "1" ;  
            var start8 = document.getElementById('headG_plot3') ;  
          } 
          else if(zscore_HeadG == -2)
          {    
            document.getElementById('headG_plot4').style.opacity = "1" ;   
            var start8 = document.getElementById('headG_plot4') ;  
          }
          else if(zscore_HeadG < -1 && zscore_HeadG > -2)
          {    
            document.getElementById('headG_plot5').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot5') ;         
          }
          else if(zscore_HeadG == -1)
          {    
            document.getElementById('headG_plot6').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot6') ;
          }
          else if(zscore_HeadG < 0 && zscore_HeadG > -1)
          {    
            document.getElementById('headG_plot7').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot7') ;
          } 
          else if(zscore_HeadG == 0)
          {    
            document.getElementById('headG_plot8').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot8') ;
          }
          else if(zscore_HeadG < 1 && zscore_HeadG > 0)
          {    
            document.getElementById('headG_plot9').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot9') ;            
          }
          else if(zscore_HeadG == 1)
          {    
            document.getElementById('headG_plot10').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot10') ;           
          }
          else if(zscore_HeadG < 2 && zscore_HeadG > 1)
          {    
            document.getElementById('headG_plot11').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot11') ;
          } 
          else if(zscore_HeadG == 2)
          {    
            document.getElementById('headG_plot12').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot12') ;
          }
          else if(zscore_HeadG < 3 && zscore_HeadG > 2)
          {    
            document.getElementById('headG_plot13').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot13') ;
          } 
          else if(zscore_HeadG == 3)
          {    
            document.getElementById('headG_plot14').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot14') ;
          }
          else if(zscore_HeadG > 3)
          {    
            document.getElementById('headG_plot15').style.opacity = "1" ;          
            var start8 = document.getElementById('headG_plot15') ;
          } 
          
        }
    
    var zscore_NeckG = document.getElementById("zscore_NeckG").innerHTML ;
     
        if(zscore_NeckG !== "")
        {
          if(zscore_NeckG < -3)
          {    
            document.getElementById('neckG_plot1').style.opacity = "1" ;  
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot1'), "#ec6565", 1); 
            }             
            var start9 = document.getElementById('neckG_plot1') ;
          }
          else if(zscore_NeckG == -3)
          {    
            document.getElementById('neckG_plot2').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot2'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot2') ;
          }
          else if(zscore_NeckG < -2 && zscore_NeckG > -3)
          {    
            document.getElementById('neckG_plot3').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot3'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot3') ;
          } 
          else if(zscore_NeckG == -2)
          {    
            document.getElementById('neckG_plot4').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot4'), "#ec6565", 1);  
            }            
            var start9 = document.getElementById('neckG_plot4') ;
          }
          else if(zscore_NeckG < -1 && zscore_NeckG > -2)
          {    
            document.getElementById('neckG_plot5').style.opacity = "1" ;   
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot5'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot5') ;
          }
          else if(zscore_NeckG == -1)
          {    
            document.getElementById('neckG_plot6').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot6'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot6') ;
          }
          else if(zscore_NeckG < 0 && zscore_NeckG > -1)
          {    
            document.getElementById('neckG_plot7').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot7'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot7') ;
          } 
          else if(zscore_NeckG == 0)
          {    
            document.getElementById('neckG_plot8').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot8'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot8') ;
          }
          else if(zscore_NeckG < 1 && zscore_NeckG > 0)
          {    
            document.getElementById('neckG_plot9').style.opacity = "1" ; 
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot9'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot9') ;
          }
          else if(zscore_NeckG == 1)
          {    
            document.getElementById('neckG_plot10').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot10'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot10') ;
          }
          else if(zscore_NeckG < 2 && zscore_NeckG > 1)
          {    
            document.getElementById('neckG_plot11').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot11'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot11') ;
          } 
          else if(zscore_NeckG == 2)
          {    
            document.getElementById('neckG_plot12').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot12'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot12') ;
          }
          else if(zscore_NeckG < 3 && zscore_NeckG > 2)
          {    
            document.getElementById('neckG_plot13').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot13'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot13') ;
          } 
          else if(zscore_NeckG == 3)
          {    
            document.getElementById('neckG_plot14').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot14'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot14') ;
          }
          else if(zscore_NeckG > 3)
          {    
            document.getElementById('neckG_plot15').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('neckG_plot15'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('neckG_plot15') ;
          }          
         
        }

     var zscore_RelArmG = document.getElementById("zscore_RelArmG").innerHTML ;
     
        if(zscore_RelArmG !== "")
        {
          if(zscore_RelArmG < -3)
          {    
            document.getElementById('relArmG_plot1').style.opacity = "1" ; 
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot1'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot1') ;           
          }
          else if(zscore_RelArmG == -3)
          {    
            document.getElementById('relArmG_plot2').style.opacity = "1" ;
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot2'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot2') ;  
          }
          else if(zscore_RelArmG < -2 && zscore_RelArmG > -3)
          {    
            document.getElementById('relArmG_plot3').style.opacity = "1" ; 
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot3'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot3') ;  
          } 
          else if(zscore_RelArmG == -2)
          {    
            document.getElementById('relArmG_plot4').style.opacity = "1" ;   
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot4'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot4') ;  
          }
          else if(zscore_RelArmG < -1 && zscore_RelArmG > -2)
          {    
            document.getElementById('relArmG_plot5').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot5'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot5') ;         
          }
          else if(zscore_RelArmG == -1)
          {    
            document.getElementById('relArmG_plot6').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot6'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot6') ;
          }
          else if(zscore_RelArmG < 0 && zscore_RelArmG > -1)
          {    
            document.getElementById('relArmG_plot7').style.opacity = "1" ;  
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot7'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot7') ;
          } 
          else if(zscore_RelArmG == 0)
          {    
            document.getElementById('relArmG_plot8').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot8'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot8') ;
          }
          else if(zscore_RelArmG < 1 && zscore_RelArmG > 0)
          {    
            document.getElementById('relArmG_plot9').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot9'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot9') ;            
          }
          else if(zscore_RelArmG == 1)
          {    
            document.getElementById('relArmG_plot10').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot10'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot10') ;           
          }
          else if(zscore_RelArmG < 2 && zscore_RelArmG > 1)
          {    
            document.getElementById('relArmG_plot11').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot11'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot11') ;
          } 
          else if(zscore_RelArmG == 2)
          {    
            document.getElementById('relArmG_plot12').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot12'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot12') ;
          }
          else if(zscore_RelArmG < 3 && zscore_RelArmG > 2)
          {    
            document.getElementById('relArmG_plot13').style.opacity = "1" ;
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot13'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot13') ;
          } 
          else if(zscore_RelArmG == 3)
          {    
            document.getElementById('relArmG_plot14').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot14'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot14') ;
          }
          else if(zscore_RelArmG > 3)
          {    
            document.getElementById('relArmG_plot15').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('relArmG_plot15'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('relArmG_plot15') ;
          } 
          
        }
        

    var zscore_FlexArmG = document.getElementById("zscore_FlexArmG").innerHTML ;
     
        if(zscore_FlexArmG !== "")
        {
          if(zscore_FlexArmG < -3)
          {    
            document.getElementById('flexArmG_plot1').style.opacity = "1" ; 
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot1'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot1') ;
          }
          else if(zscore_FlexArmG == -3)
          {    
            document.getElementById('flexArmG_plot2').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot2'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot2') ;
          }
          else if(zscore_FlexArmG < -2 && zscore_FlexArmG > -3)
          {    
            document.getElementById('flexArmG_plot3').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot3'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot3') ;
          } 
          else if(zscore_FlexArmG == -2)
          {    
            document.getElementById('flexArmG_plot4').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot4'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot4') ;
          }
          else if(zscore_FlexArmG < -1 && zscore_FlexArmG > -2)
          {    
            document.getElementById('flexArmG_plot5').style.opacity = "1" ;   
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot5'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot5') ;
          }
          else if(zscore_FlexArmG == -1)
          {    
            document.getElementById('flexArmG_plot6').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot6'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot6') ;
          }
          else if(zscore_FlexArmG < 0 && zscore_FlexArmG > -1)
          {    
            document.getElementById('flexArmG_plot7').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot7'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot7') ;
          } 
          else if(zscore_FlexArmG == 0)
          {    
            document.getElementById('flexArmG_plot8').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot8'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot8') ;
          }
          else if(zscore_FlexArmG < 1 && zscore_FlexArmG > 0)
          {    
            document.getElementById('flexArmG_plot9').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot9'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot9') ;
          }
          else if(zscore_FlexArmG == 1)
          {    
            document.getElementById('flexArmG_plot10').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot10'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot10') ;
          }
          else if(zscore_FlexArmG < 2 && zscore_FlexArmG > 1)
          {    
            document.getElementById('flexArmG_plot11').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot11'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot11') ;
          } 
          else if(zscore_FlexArmG == 2)
          {    
            document.getElementById('flexArmG_plot12').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot12'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot12') ;
          }
          else if(zscore_FlexArmG < 3 && zscore_FlexArmG > 2)
          {    
            document.getElementById('flexArmG_plot13').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot13'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot13') ;
          } 
          else if(zscore_FlexArmG == 3)
          {    
            document.getElementById('flexArmG_plot14').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot14'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot14') ;
          }
          else if(zscore_FlexArmG > 3)
          {    
            document.getElementById('flexArmG_plot15').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('flexArmG_plot15'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('flexArmG_plot15') ;
          }          
         
        }
        

    var zscore_ForearmG = document.getElementById("zscore_ForearmG").innerHTML ;
     
        if(zscore_ForearmG !== "")
        {
          if(zscore_ForearmG < -3)
          {    
            document.getElementById('forearmG_plot1').style.opacity = "1" ; 
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot1'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot1') ;
          }
          else if(zscore_ForearmG == -3)
          {    
            document.getElementById('forearmG_plot2').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot2'), "#ec6565", 1);
            }             
            var start12 = document.getElementById('forearmG_plot2') ;
          }
          else if(zscore_ForearmG < -2 && zscore_ForearmG > -3)
          {    
            document.getElementById('forearmG_plot3').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot3'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot3') ;
          } 
          else if(zscore_ForearmG == -2)
          {    
            document.getElementById('forearmG_plot4').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot4'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot4') ;
          }
          else if(zscore_ForearmG < -1 && zscore_ForearmG > -2)
          {    
            document.getElementById('forearmG_plot5').style.opacity = "1" ;   
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot5'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot5') ;
          }
          else if(zscore_ForearmG == -1)
          {    
            document.getElementById('forearmG_plot6').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot6'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot6') ;
          }
          else if(zscore_ForearmG < 0 && zscore_ForearmG > -1)
          {    
            document.getElementById('forearmG_plot7').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot7'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot7') ;
          } 
          else if(zscore_ForearmG == 0)
          {    
            document.getElementById('forearmG_plot8').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot8'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot8') ;
          }
          else if(zscore_ForearmG < 1 && zscore_ForearmG > 0)
          {    
            document.getElementById('forearmG_plot9').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot9'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot9') ;
          }
          else if(zscore_ForearmG == 1)
          {    
            document.getElementById('forearmG_plot10').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot10'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot10') ;
          }
          else if(zscore_ForearmG < 2 && zscore_ForearmG > 1)
          {    
            document.getElementById('forearmG_plot11').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot11'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot11') ;
          } 
          else if(zscore_ForearmG == 2)
          {    
            document.getElementById('forearmG_plot12').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot12'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot12') ;
          }
          else if(zscore_ForearmG < 3 && zscore_ForearmG > 2)
          {    
            document.getElementById('forearmG_plot13').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot13'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot13') ;
          } 
          else if(zscore_ForearmG == 3)
          {    
            document.getElementById('forearmG_plot14').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot14'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot14') ;
          }
          else if(zscore_ForearmG > 3)
          {    
            document.getElementById('forearmG_plot15').style.opacity = "1" ; 
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('forearmG_plot15'), "#ec6565", 1); 
            }            
            var start12 = document.getElementById('forearmG_plot15') ;
          }          
         
        }



    var zscore_WristG = document.getElementById("zscore_WristG").innerHTML ;
     
        if(zscore_WristG !== "")
        {
          if(zscore_WristG < -3)
          {    
            document.getElementById('wristG_plot1').style.opacity = "1" ;  
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot1'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot1') ;
          }
          else if(zscore_WristG == -3)
          {    
            document.getElementById('wristG_plot2').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot2'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot2') ;
          }
          else if(zscore_WristG < -2 && zscore_WristG > -3)
          {    
            document.getElementById('wristG_plot3').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot3'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot3') ;
          } 
          else if(zscore_WristG == -2)
          {    
            document.getElementById('wristG_plot4').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot4'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot4') ;
          }
          else if(zscore_WristG < -1 && zscore_WristG > -2)
          {    
            document.getElementById('wristG_plot5').style.opacity = "1" ;   
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot5'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot5') ;
          }
          else if(zscore_WristG == -1)
          {    
            document.getElementById('wristG_plot6').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot6'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot6') ;
          }
          else if(zscore_WristG < 0 && zscore_WristG > -1)
          {   
            document.getElementById('wristG_plot7').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot7'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot7') ;
          } 
          else if(zscore_WristG == 0)
          {    
            document.getElementById('wristG_plot8').style.opacity = "1" ;
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot8'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot8') ;
          }
          else if(zscore_WristG < 1 && zscore_WristG > 0)
          {    
            document.getElementById('wristG_plot9').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot9'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot9') ;
          }
          else if(zscore_WristG == 1)
          {    
            document.getElementById('wristG_plot10').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot10'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot10') ;
          }
          else if(zscore_WristG < 2 && zscore_WristG > 1)
          {    
            document.getElementById('wristG_plot11').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot11'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot11') ;
          } 
          else if(zscore_WristG == 2)
          {    
            document.getElementById('wristG_plot12').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot12'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot12') ;
          }
          else if(zscore_WristG < 3 && zscore_WristG > 2)
          {    
            document.getElementById('wristG_plot13').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot13'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot13') ;
          } 
          else if(zscore_WristG == 3)
          {    
            document.getElementById('wristG_plot14').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot14'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot14') ;
          }
          else if(zscore_WristG > 3)
          {    
            document.getElementById('wristG_plot15').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('wristG_plot15'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('wristG_plot15') ;
          }          
         
        } 


    var zscore_ChestG = document.getElementById("zscore_ChestG").innerHTML ;
     
        if(zscore_ChestG !== "")
        {
          if(zscore_ChestG < -3)
          {    
            document.getElementById('chestG_plot1').style.opacity = "1" ;
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot1'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot1') ;
          }
          else if(zscore_ChestG == -3)
          {    
            document.getElementById('chestG_plot2').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot2'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot2') ;
          }
          else if(zscore_ChestG < -2 && zscore_ChestG > -3)
          {    
            document.getElementById('chestG_plot3').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot3'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot3') ;
          } 
          else if(zscore_ChestG == -2)
          {    
            document.getElementById('chestG_plot4').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot4'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot4') ;
          }
          else if(zscore_ChestG < -1 && zscore_ChestG > -2)
          {    
            document.getElementById('chestG_plot5').style.opacity = "1" ;   
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot5'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot5') ;
          }
          else if(zscore_ChestG == -1)
          {    
            document.getElementById('chestG_plot6').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot6'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot6') ;
          }
          else if(zscore_ChestG < 0 && zscore_ChestG > -1)
          {   
            document.getElementById('chestG_plot7').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot7'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot7') ;
          } 
          else if(zscore_ChestG == 0)
          {    
            document.getElementById('chestG_plot8').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot8'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot8') ;
          }
          else if(zscore_ChestG < 1 && zscore_ChestG > 0)
          {    
            document.getElementById('chestG_plot9').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot9'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot9') ;
          }
          else if(zscore_ChestG == 1)
          {    
            document.getElementById('chestG_plot10').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot10'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot10') ;
          }
          else if(zscore_ChestG < 2 && zscore_ChestG > 1)
          {    
            document.getElementById('chestG_plot11').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot11'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot11') ;
          } 
          else if(zscore_ChestG == 2)
          {    
            document.getElementById('chestG_plot12').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot12'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot12') ;
          }
          else if(zscore_ChestG < 3 && zscore_ChestG > 2)
          {    
            document.getElementById('chestG_plot13').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot13'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot13') ;
          } 
          else if(zscore_ChestG == 3)
          {    
            document.getElementById('chestG_plot14').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot14'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot14') ;
          }
          else if(zscore_ChestG > 3)
          {    
            document.getElementById('chestG_plot15').style.opacity = "1" ;          
            if(start13 !== undefined)
            {
             connect(start13, document.getElementById('chestG_plot15'), "#ec6565", 1); 
            }            
            var start14 = document.getElementById('chestG_plot15') ;
          }          
         
        } 
        

    var zscore_WaistG = document.getElementById("zscore_WaistG").innerHTML ;
     
        if(zscore_WaistG !== "")
        {
          if(zscore_WaistG < -3)
          {    
            document.getElementById('waistG_plot1').style.opacity = "1" ;  
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot1'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot1') ;
          }
          else if(zscore_WaistG == -3)
          {    
            document.getElementById('waistG_plot2').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot2'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot2') ;
          }
          else if(zscore_WaistG < -2 && zscore_WaistG > -3)
          {    
            document.getElementById('waistG_plot3').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot3'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot3') ;
          } 
          else if(zscore_WaistG == -2)
          {    
            document.getElementById('waistG_plot4').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot4'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot4') ;
          }
          else if(zscore_WaistG < -1 && zscore_WaistG > -2)
          {    
            document.getElementById('waistG_plot5').style.opacity = "1" ;   
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot5'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot5') ;
          }
          else if(zscore_WaistG == -1)
          {    
            document.getElementById('waistG_plot6').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot6'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot6') ;
          }
          else if(zscore_WaistG < 0 && zscore_WaistG > -1)
          {    
            document.getElementById('waistG_plot7').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot7'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot7') ;
          } 
          else if(zscore_WaistG == 0)
          {    
            document.getElementById('waistG_plot8').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot8'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot8') ;
          }
          else if(zscore_WaistG < 1 && zscore_WaistG > 0)
          {    
            document.getElementById('waistG_plot9').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot9'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot9') ;
          }
          else if(zscore_WaistG == 1)
          {    
            document.getElementById('waistG_plot10').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot10'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot10') ;
          }
          else if(zscore_WaistG < 2 && zscore_WaistG > 1)
          {    
            document.getElementById('waistG_plot11').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot11'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot11') ;
          } 
          else if(zscore_WaistG == 2)
          {    
            document.getElementById('waistG_plot12').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot12'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot12') ;
          }
          else if(zscore_WaistG < 3 && zscore_WaistG > 2)
          {    
            document.getElementById('waistG_plot13').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot13'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot13') ;
          } 
          else if(zscore_WaistG == 3)
          {    
            document.getElementById('waistG_plot14').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot14'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot14') ;
          }
          else if(zscore_WaistG > 3)
          {    
            document.getElementById('waistG_plot15').style.opacity = "1" ;          
            if(start14 !== undefined)
            {
             connect(start14, document.getElementById('waistG_plot15'), "#ec6565", 1); 
            }            
            var start15 = document.getElementById('waistG_plot15') ;
          }          
         
        }
        
        
    var zscore_HipG = document.getElementById("zscore_HipG").innerHTML ;
     
        if(zscore_HipG !== "")
        {
          if(zscore_HipG < -3)
          {    
            document.getElementById('hipG_plot1').style.opacity = "1" ;  
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot1'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot1') ;
          }
          else if(zscore_HipG == -3)
          {    
            document.getElementById('hipG_plot2').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot2'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot2') ;
          }
          else if(zscore_HipG < -2 && zscore_HipG > -3)
          {    
            document.getElementById('hipG_plot3').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot3'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot3') ;
          } 
          else if(zscore_HipG == -2)
          {    
            document.getElementById('hipG_plot4').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot4'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot4') ;
          }
          else if(zscore_HipG < -1 && zscore_HipG > -2)
          {    
            document.getElementById('hipG_plot5').style.opacity = "1" ;   
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot5'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot5') ;
          }
          else if(zscore_HipG == -1)
          {    
            document.getElementById('hipG_plot6').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot6'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot6') ;
          }
          else if(zscore_HipG < 0 && zscore_HipG > -1)
          {   
            document.getElementById('hipG_plot7').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot7'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot7') ;
          } 
          else if(zscore_HipG == 0)
          {    
            document.getElementById('hipG_plot8').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot8'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot8') ;
          }
          else if(zscore_HipG < 1 && zscore_HipG > 0)
          {    
            document.getElementById('hipG_plot9').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot9'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot9') ;
          }
          else if(zscore_HipG == 1)
          {    
            document.getElementById('hipG_plot10').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot10'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot10') ;
          }
          else if(zscore_HipG < 2 && zscore_HipG > 1)
          {    
            document.getElementById('hipG_plot11').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot11'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot11') ;
          } 
          else if(zscore_HipG == 2)
          {    
            document.getElementById('hipG_plot12').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot12'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot12') ;
          }
          else if(zscore_HipG < 3 && zscore_HipG > 2)
          {    
            document.getElementById('hipG_plot13').style.opacity = "1" ;     
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot13'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot13') ;
          } 
          else if(zscore_HipG == 3)
          {    
            document.getElementById('hipG_plot14').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot14'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot14') ;
          }
          else if(zscore_HipG > 3)
          {    
            document.getElementById('hipG_plot15').style.opacity = "1" ;          
            if(start15 !== undefined)
            {
             connect(start15, document.getElementById('hipG_plot15'), "#ec6565", 1); 
            }            
            var start16 = document.getElementById('hipG_plot15') ;
          }          
         
        }        

    var zscore_ThighG = document.getElementById("zscore_ThighG").innerHTML ;
     
        if(zscore_ThighG !== "")
        {
          if(zscore_ThighG < -3)
          {    
            document.getElementById('thighG_plot1').style.opacity = "1" ; 
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot1'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot1') ;
          }
          else if(zscore_ThighG == -3)
          {    
            document.getElementById('thighG_plot2').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot2'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot2') ;
          }
          else if(zscore_ThighG < -2 && zscore_ThighG > -3)
          {    
            document.getElementById('thighG_plot3').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot3'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot3') ;
          } 
          else if(zscore_ThighG == -2)
          {    
            document.getElementById('thighG_plot4').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot4'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot4') ;
          }
          else if(zscore_ThighG < -1 && zscore_ThighG > -2)
          {    
            document.getElementById('thighG_plot5').style.opacity = "1" ;   
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot5'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot5') ;
          }
          else if(zscore_ThighG == -1)
          {    
            document.getElementById('thighG_plot6').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot6'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot6') ;
          }
          else if(zscore_ThighG < 0 && zscore_ThighG > -1)
          {   
            document.getElementById('thighG_plot7').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot7'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot7') ;
          } 
          else if(zscore_ThighG == 0)
          {    
            document.getElementById('thighG_plot8').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot8'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot8') ;
          }
          else if(zscore_ThighG < 1 && zscore_ThighG > 0)
          {    
            document.getElementById('thighG_plot9').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot9'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot9') ;
          }
          else if(zscore_ThighG == 1)
          {    
            document.getElementById('thighG_plot10').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot10'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot10') ;
          }
          else if(zscore_ThighG < 2 && zscore_ThighG > 1)
          {    
            document.getElementById('thighG_plot11').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot11'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot11') ;
          } 
          else if(zscore_ThighG == 2)
          {    
            document.getElementById('thighG_plot12').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot12'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot12') ;
          }
          else if(zscore_ThighG < 3 && zscore_ThighG > 2)
          {    
            document.getElementById('thighG_plot13').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot13'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot13') ;
          } 
          else if(zscore_ThighG == 3)
          {    
            document.getElementById('thighG_plot14').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot14'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot14') ;
          }
          else if(zscore_ThighG > 3)
          {    
            document.getElementById('thighG_plot15').style.opacity = "1" ;          
            if(start16 !== undefined)
            {
             connect(start16, document.getElementById('thighG_plot15'), "#ec6565", 1); 
            }            
            var start17 = document.getElementById('thighG_plot15') ;
          }          
         
        } 


    var zscore_MidThighG = document.getElementById("zscore_MidThighG").innerHTML ;
     
        if(zscore_MidThighG !== "")
        {
          if(zscore_MidThighG < -3)
          {    
            document.getElementById('midThighG_plot1').style.opacity = "1" ;  
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot1'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot1') ;
          }
          else if(zscore_MidThighG == -3)
          {    
            document.getElementById('midThighG_plot2').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot2'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot2') ;
          }
          else if(zscore_MidThighG < -2 && zscore_MidThighG > -3)
          {    
            document.getElementById('midThighG_plot3').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot3'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot3') ;
          } 
          else if(zscore_MidThighG == -2)
          {    
            document.getElementById('midThighG_plot4').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot4'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot4') ;
          }
          else if(zscore_MidThighG < -1 && zscore_MidThighG > -2)
          {    
            document.getElementById('midThighG_plot5').style.opacity = "1" ;   
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot5'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot5') ;
          }
          else if(zscore_MidThighG == -1)
          {    
            document.getElementById('midThighG_plot6').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot6'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot6') ;
          }
          else if(zscore_MidThighG < 0 && zscore_MidThighG > -1)
          {   
            document.getElementById('midThighG_plot7').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot7'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot7') ;
          } 
          else if(zscore_MidThighG == 0)
          {    
            document.getElementById('midThighG_plot8').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot8'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot8') ;
          }
          else if(zscore_MidThighG < 1 && zscore_MidThighG > 0)
          {    
            document.getElementById('midThighG_plot9').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot9'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot9') ;
          }
          else if(zscore_MidThighG == 1)
          {    
            document.getElementById('midThighG_plot10').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot10'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot10') ;
          }
          else if(zscore_MidThighG < 2 && zscore_MidThighG > 1)
          {    
            document.getElementById('midThighG_plot11').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot11'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot11') ;
          } 
          else if(zscore_MidThighG == 2)
          {    
            document.getElementById('midThighG_plot12').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot12'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot12') ;
          }
          else if(zscore_MidThighG < 3 && zscore_MidThighG > 2)
          {    
            document.getElementById('midThighG_plot13').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot13'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot13') ;
          } 
          else if(zscore_MidThighG == 3)
          {    
            document.getElementById('midThighG_plot14').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot14'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot14') ;
          }
          else if(zscore_MidThighG > 3)
          {    
            document.getElementById('midThighG_plot15').style.opacity = "1" ;          
            if(start17 !== undefined)
            {
             connect(start17, document.getElementById('midThighG_plot15'), "#ec6565", 1); 
            }            
            var start18 = document.getElementById('midThighG_plot15') ;
          }          
         
        } 
        
  
    var zscore_CalfG = document.getElementById("zscore_CalfG").innerHTML ;
     
        if(zscore_CalfG !== "")
        {
          if(zscore_CalfG < -3)
          {    
            document.getElementById('calfG_plot1').style.opacity = "1" ;  
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot1'), "#ec6565", 1); 
            }            
            var start19 = document.getElementById('calfG_plot1') ;             
          }
          else if(zscore_CalfG == -3)
          {    
            document.getElementById('calfG_plot2').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot2'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot2') ;             
          }
          else if(zscore_CalfG < -2 && zscore_CalfG > -3)
          {    
            document.getElementById('calfG_plot3').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot3'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot3') ;             
          } 
          else if(zscore_CalfG == -2)
          {    
            document.getElementById('calfG_plot4').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot4'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot4') ;             
          }
          else if(zscore_CalfG < -1 && zscore_CalfG > -2)
          {    
            document.getElementById('calfG_plot5').style.opacity = "1" ;
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot5'), "#ec6565", 1); 
            }            
	    var start19 = document.getElementById('calfG_plot5') ;             
          }
          else if(zscore_CalfG == -1)
          {    
            document.getElementById('calfG_plot6').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot6'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot6') ;             
          }
          else if(zscore_CalfG < 0 && zscore_CalfG > -1)
          {   
            document.getElementById('calfG_plot7').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot7'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot7') ;             
          } 
          else if(zscore_CalfG == 0)
          {    
            document.getElementById('calfG_plot8').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot8'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot8') ;             
          }
          else if(zscore_CalfG < 1 && zscore_CalfG > 0)
          {    
            document.getElementById('calfG_plot9').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot9'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot9') ;             
          }
          else if(zscore_CalfG == 1)
          {    
            document.getElementById('calfG_plot10').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot10'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot10') ;             
          }
          else if(zscore_CalfG < 2 && zscore_CalfG > 1)
          {    
            document.getElementById('calfG_plot11').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot11'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot11') ;            
          } 
          else if(zscore_CalfG == 2)
          {    
            document.getElementById('calfG_plot12').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot12'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot12') ;             
          }
          else if(zscore_CalfG < 3 && zscore_CalfG > 2)
          {    
            document.getElementById('calfG_plot13').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot13'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot13') ;             
          } 
          else if(zscore_CalfG == 3)
          {    
            document.getElementById('calfG_plot14').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot14'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot14') ;             
          }
          else if(zscore_CalfG > 3)
          {    
            document.getElementById('calfG_plot15').style.opacity = "1" ;          
            if(start18 !== undefined)
            {
             connect(start18, document.getElementById('calfG_plot15'), "#ec6565", 1);
            }            
	    var start19 = document.getElementById('calfG_plot15') ;             
          }          
         
        }
        
     var zscore_AnkleG = document.getElementById("zscore_AnkleG").innerHTML ;
     
        if(zscore_AnkleG !== "")
        {
          if(zscore_AnkleG < -3)
          {    
            document.getElementById('ankleG_plot1').style.opacity = "1" ;  
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot1'), "#ec6565", 1);                          
            }             
          }
          else if(zscore_AnkleG == -3)
          {    
            document.getElementById('ankleG_plot2').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot2'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < -2 && zscore_AnkleG > -3)
          {    
            document.getElementById('ankleG_plot3').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot3'), "#ec6565", 1);
            }            
          } 
          else if(zscore_AnkleG == -2)
          {    
            document.getElementById('ankleG_plot4').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot4'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < -1 && zscore_AnkleG > -2)
          {    
            document.getElementById('ankleG_plot5').style.opacity = "1" ;   
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot5'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG == -1)
          {    
            document.getElementById('ankleG_plot6').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot6'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < 0 && zscore_AnkleG > -1)
          {   
            document.getElementById('ankleG_plot7').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot7'), "#ec6565", 1);
            }            
          } 
          else if(zscore_AnkleG == 0)
          {    
            document.getElementById('ankleG_plot8').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot8'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < 1 && zscore_AnkleG > 0)
          {    
            document.getElementById('ankleG_plot9').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot9'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG == 1)
          {    
            document.getElementById('ankleG_plot10').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot10'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < 2 && zscore_AnkleG > 1)
          {    
            document.getElementById('ankleG_plot11').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot11'), "#ec6565", 1);
            }            
          } 
          else if(zscore_AnkleG == 2)
          {    
            document.getElementById('ankleG_plot12').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot12'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG < 3 && zscore_AnkleG > 2)
          {    
            document.getElementById('ankleG_plot13').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot13'), "#ec6565", 1);
            }            
          } 
          else if(zscore_AnkleG == 3)
          {    
            document.getElementById('ankleG_plot14').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot14'), "#ec6565", 1);
            }            
          }
          else if(zscore_AnkleG > 3)
          {    
            document.getElementById('ankleG_plot15').style.opacity = "1" ;          
            if(start19 !== undefined)
            {
             connect(start19, document.getElementById('ankleG_plot15'), "#ec6565", 1);
            }            
          }          
         
        }
        
        
    var zscore_AcRad = document.getElementById("zscore_AcRad").innerHTML ;
     
        if(zscore_AcRad !== "")
        {
          if(zscore_AcRad < -3)
          {    
            document.getElementById('acRad_plot1').style.opacity = "1" ;             
            var start20 = document.getElementById('acRad_plot1') ;
          }
          else if(zscore_AcRad == -3)
          {    
            document.getElementById('acRad_plot2').style.opacity = "1" ;           
            var start20 = document.getElementById('acRad_plot2') ;
          }
          else if(zscore_AcRad < -2 && zscore_AcRad > -3)
          {    
            document.getElementById('acRad_plot3').style.opacity = "1" ;     
            var start20 = document.getElementById('acRad_plot3') ;
          } 
          else if(zscore_AcRad == -2)
          {    
            document.getElementById('acRad_plot4').style.opacity = "1" ;         
            var start20 = document.getElementById('acRad_plot4') ;
          }
          else if(zscore_AcRad < -1 && zscore_AcRad > -2)
          {    
            document.getElementById('acRad_plot5').style.opacity = "1" ;     
            var start20 = document.getElementById('acRad_plot5') ;
          }
          else if(zscore_AcRad == -1)
          {    
            document.getElementById('acRad_plot6').style.opacity = "1" ;         
            var start20 = document.getElementById('acRad_plot6') ;
          }
          else if(zscore_AcRad < 0 && zscore_AcRad > -1)
          {   
            document.getElementById('acRad_plot7').style.opacity = "1" ;          
            var start20 = document.getElementById('acRad_plot7') ;
          } 
          else if(zscore_AcRad == 0)
          {    
            document.getElementById('acRad_plot8').style.opacity = "1" ;     
            var start20 = document.getElementById('acRad_plot8') ;
          }
          else if(zscore_AcRad < 1 && zscore_AcRad > 0)
          {    
            document.getElementById('acRad_plot9').style.opacity = "1" ;      
            var start20 = document.getElementById('acRad_plot9') ;
          }
          else if(zscore_AcRad == 1)
          {    
            document.getElementById('acRad_plot10').style.opacity = "1" ;         
            var start20 = document.getElementById('acRad_plot10') ;
          }
          else if(zscore_AcRad < 2 && zscore_AcRad > 1)
          {    
            document.getElementById('acRad_plot11').style.opacity = "1" ;       
            var start20 = document.getElementById('acRad_plot11') ;
          } 
          else if(zscore_AcRad == 2)
          {    
            document.getElementById('acRad_plot12').style.opacity = "1" ;        
            var start20 = document.getElementById('acRad_plot12') ;
          }
          else if(zscore_AcRad < 3 && zscore_AcRad > 2)
          {    
            document.getElementById('acRad_plot13').style.opacity = "1" ;        
            var start20 = document.getElementById('acRad_plot13') ;
          } 
          else if(zscore_AcRad == 3)
          {    
            document.getElementById('acRad_plot14').style.opacity = "1" ;    
            var start20 = document.getElementById('acRad_plot14') ;
          }
          else if(zscore_AcRad > 3)
          {    
            document.getElementById('acRad_plot15').style.opacity = "1" ;    
            var start20 = document.getElementById('acRad_plot15') ;
          }          
         
        } 
        
        
    var zscore_RadStyl = document.getElementById("zscore_RadStyl").innerHTML ;
     
        if(zscore_RadStyl !== "")
        {
          if(zscore_RadStyl < -3)
          {    
            document.getElementById('radStyl_plot1').style.opacity = "1" ; 
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot1'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot1') ;
          }
          else if(zscore_RadStyl == -3)
          {    
            document.getElementById('radStyl_plot2').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot2'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot2') ;
          }
          else if(zscore_RadStyl < -2 && zscore_RadStyl > -3)
          {    
            document.getElementById('radStyl_plot3').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot3'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot3') ;
          } 
          else if(zscore_RadStyl == -2)
          {    
            document.getElementById('radStyl_plot4').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot4'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot4') ;
          }
          else if(zscore_RadStyl < -1 && zscore_RadStyl > -2)
          {    
            document.getElementById('radStyl_plot5').style.opacity = "1" ;   
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot5'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot5') ;
          }
          else if(zscore_RadStyl == -1)
          {    
            document.getElementById('radStyl_plot6').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot6'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot6') ;
          }
          else if(zscore_RadStyl < 0 && zscore_RadStyl > -1)
          {   
            document.getElementById('radStyl_plot7').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot7'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot7') ;
          } 
          else if(zscore_RadStyl == 0)
          {    
            document.getElementById('radStyl_plot8').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot8'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot8') ;
          }
          else if(zscore_RadStyl < 1 && zscore_RadStyl > 0)
          {    
            document.getElementById('radStyl_plot9').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot9'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot9') ;
          }
          else if(zscore_RadStyl == 1)
          {    
            document.getElementById('radStyl_plot10').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot10'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot10') ;
          }
          else if(zscore_RadStyl < 2 && zscore_RadStyl > 1)
          {    
            document.getElementById('radStyl_plot11').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot11'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot11') ;
          } 
          else if(zscore_RadStyl == 2)
          {    
            document.getElementById('radStyl_plot12').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot12'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot12') ;
          }
          else if(zscore_RadStyl < 3 && zscore_RadStyl > 2)
          {    
            document.getElementById('radStyl_plot13').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot13'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot13') ;
          } 
          else if(zscore_RadStyl == 3)
          {    
            document.getElementById('radStyl_plot14').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot14'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot14') ;
          }
          else if(zscore_RadStyl > 3)
          {    
            document.getElementById('radStyl_plot15').style.opacity = "1" ;          
            if(start20 !== undefined)
            {
             connect(start20, document.getElementById('radStyl_plot15'), "#ec6565", 1); 
            }            
            var start21 = document.getElementById('radStyl_plot15') ;
          }          
         
        }
        
    var zscore_midStylDact = document.getElementById("zscore_midStylDact").innerHTML ;
     
        if(zscore_midStylDact !== "")
        {
          if(zscore_midStylDact < -3)
          {    
            document.getElementById('midStylDact_plot1').style.opacity = "1" ;  
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot1'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot1') ;
          }
          else if(zscore_midStylDact == -3)
          {    
            document.getElementById('midStylDact_plot2').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot2'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot2') ;
          }
          else if(zscore_midStylDact < -2 && zscore_midStylDact > -3)
          {    
            document.getElementById('midStylDact_plot3').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot3'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot3') ;
          } 
          else if(zscore_midStylDact == -2)
          {    
            document.getElementById('midStylDact_plot4').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot4'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot4') ;
          }
          else if(zscore_midStylDact < -1 && zscore_midStylDact > -2)
          {    
            document.getElementById('midStylDact_plot5').style.opacity = "1" ;   
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot5'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot5') ;
          }
          else if(zscore_midStylDact == -1)
          {    
            document.getElementById('midStylDact_plot6').style.opacity = "1" ; 
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot6'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot6') ;
          }
          else if(zscore_midStylDact < 0 && zscore_midStylDact > -1)
          {   
            document.getElementById('midStylDact_plot7').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot7'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot7') ;
          } 
          else if(zscore_midStylDact == 0)
          {    
            document.getElementById('midStylDact_plot8').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot8'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot8') ;
          }
          else if(zscore_midStylDact < 1 && zscore_midStylDact > 0)
          {    
            document.getElementById('midStylDact_plot9').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot9'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot9') ;
          }
          else if(zscore_midStylDact == 1)
          {    
            document.getElementById('midStylDact_plot10').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot10'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot10') ;
          }
          else if(zscore_midStylDact < 2 && zscore_midStylDact > 1)
          {    
            document.getElementById('midStylDact_plot11').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot11'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot11') ;
          } 
          else if(zscore_midStylDact == 2)
          {    
            document.getElementById('midStylDact_plot12').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot12'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot12') ;
          }
          else if(zscore_midStylDact < 3 && zscore_midStylDact > 2)
          {    
            document.getElementById('midStylDact_plot13').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot13'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot13') ;
          } 
          else if(zscore_midStylDact == 3)
          {    
            document.getElementById('midStylDact_plot14').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot14'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot14') ;
          }
          else if(zscore_midStylDact > 3)
          {    
            document.getElementById('midStylDact_plot15').style.opacity = "1" ;          
            if(start21 !== undefined)
            {
             connect(start21, document.getElementById('midStylDact_plot15'), "#ec6565", 1); 
            }            
            var start22 = document.getElementById('midStylDact_plot15') ;
          }          
         
        }        
        

    var zscore_Iliospinale = document.getElementById("zscore_Iliospinale").innerHTML ;
     
        if(zscore_Iliospinale !== "")
        {
          if(zscore_Iliospinale < -3)
          {    
            document.getElementById('iliospinale_plot1').style.opacity = "1" ; 
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot1'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot1') ;
          }
          else if(zscore_Iliospinale == -3)
          {    
            document.getElementById('iliospinale_plot2').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot2'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot2') ;
          }
          else if(zscore_Iliospinale < -2 && zscore_Iliospinale > -3)
          {    
            document.getElementById('iliospinale_plot3').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot3'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot3') ;
          } 
          else if(zscore_Iliospinale == -2)
          {    
            document.getElementById('iliospinale_plot4').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot4'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot4') ;
          }
          else if(zscore_Iliospinale < -1 && zscore_Iliospinale > -2)
          {    
            document.getElementById('iliospinale_plot5').style.opacity = "1" ;   
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot5'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot5') ;
          }
          else if(zscore_Iliospinale == -1)
          {    
            document.getElementById('iliospinale_plot6').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot6'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot6') ;
          }
          else if(zscore_Iliospinale < 0 && zscore_Iliospinale > -1)
          {   
            document.getElementById('iliospinale_plot7').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot7'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot7') ;
          } 
          else if(zscore_Iliospinale == 0)
          {    
            document.getElementById('midStylDact_plot8').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('midStylDact_plot8'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('midStylDact_plot8') ;
          }
          else if(zscore_Iliospinale < 1 && zscore_Iliospinale > 0)
          {    
            document.getElementById('iliospinale_plot9').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot9'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot9') ;
          }
          else if(zscore_Iliospinale == 1)
          {    
            document.getElementById('iliospinale_plot10').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot10'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot10') ;
          }
          else if(zscore_Iliospinale < 2 && zscore_Iliospinale > 1)
          {    
            document.getElementById('iliospinale_plot11').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot11'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot11') ;
          } 
          else if(zscore_Iliospinale == 2)
          {    
            document.getElementById('iliospinale_plot12').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot12'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot12') ;
          }
          else if(zscore_Iliospinale < 3 && zscore_Iliospinale > 2)
          {    
            document.getElementById('iliospinale_plot13').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot13'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot13') ;
          } 
          else if(zscore_Iliospinale == 3)
          {    
            document.getElementById('iliospinale_plot14').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot14'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot14') ;
          }
          else if(zscore_Iliospinale > 3)
          {    
            document.getElementById('iliospinale_plot15').style.opacity = "1" ;          
            if(start22 !== undefined)
            {
             connect(start22, document.getElementById('iliospinale_plot15'), "#ec6565", 1); 
            }            
            var start23 = document.getElementById('iliospinale_plot15') ;
          }          
         
        }
        
    var zscore_Troch = document.getElementById("zscore_Troch").innerHTML ;
     
        if(zscore_Troch !== "")
        {
          if(zscore_Troch < -3)
          {    
            document.getElementById('troch_plot1').style.opacity = "1" ;  
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot1'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot1') ;
          }
          else if(zscore_Troch == -3)
          {    
            document.getElementById('troch_plot2').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot2'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot2') ;
          }
          else if(zscore_Troch < -2 && zscore_Troch > -3)
          {    
            document.getElementById('troch_plot3').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot3'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot3') ;
          } 
          else if(zscore_Troch == -2)
          {    
            document.getElementById('troch_plot4').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot4'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot4') ;
          }
          else if(zscore_Troch < -1 && zscore_Troch > -2)
          {    
            document.getElementById('troch_plot5').style.opacity = "1" ;   
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot5'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot5') ;
          }
          else if(zscore_Troch == -1)
          {    
            document.getElementById('troch_plot6').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot6'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot6') ;
          }
          else if(zscore_Troch < 0 && zscore_Troch > -1)
          {   
            document.getElementById('troch_plot7').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot7'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot7') ;
          } 
          else if(zscore_Troch == 0)
          {    
            document.getElementById('troch_plot8').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot8'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot8') ;
          }
          else if(zscore_Troch < 1 && zscore_Troch > 0)
          {    
            document.getElementById('troch_plot9').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot9'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot9') ;
          }
          else if(zscore_Troch == 1)
          {    
            document.getElementById('troch_plot10').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot10'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot10') ;
          }
          else if(zscore_Troch < 2 && zscore_Troch > 1)
          {    
            document.getElementById('troch_plot11').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot11'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot11') ;
          } 
          else if(zscore_Troch == 2)
          {    
            document.getElementById('troch_plot12').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot12'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot12') ;
          }
          else if(zscore_Troch < 3 && zscore_Troch > 2)
          {    
            document.getElementById('troch_plot13').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot13'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot13') ;
          } 
          else if(zscore_Troch == 3)
          {    
            document.getElementById('troch_plot14').style.opacity = "1" ;          
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot14'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot14') ;
          }
          else if(zscore_Troch > 3)
          {    
            document.getElementById('troch_plot15').style.opacity = "1" ;
            if(start23 !== undefined)
            {
             connect(start23, document.getElementById('troch_plot15'), "#ec6565", 1); 
            }            
            var start24 = document.getElementById('troch_plot15') ;
          }          
         
        }        
        
        
    var zscore_TibLat = document.getElementById("zscore_TibLat").innerHTML ;
     
        if(zscore_TibLat !== "")
        {
          if(zscore_TibLat < -3)
          {    
            document.getElementById('tibLat_plot1').style.opacity = "1" ;
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot1'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot1') ;
          }
          else if(zscore_TibLat == -3)
          {    
            document.getElementById('tibLat_plot2').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot2'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot2') ;
          }
          else if(zscore_TibLat < -2 && zscore_TibLat > -3)
          {    
            document.getElementById('tibLat_plot3').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot3'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot3') ;
          } 
          else if(zscore_TibLat == -2)
          {    
            document.getElementById('tibLat_plot4').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot4'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot4') ;
          }
          else if(zscore_TibLat < -1 && zscore_TibLat > -2)
          {    
            document.getElementById('tibLat_plot5').style.opacity = "1" ;   
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot5'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot5') ;
          }
          else if(zscore_TibLat == -1)
          {    
            document.getElementById('tibLat_plot6').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot6'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot6') ;
          }
          else if(zscore_TibLat < 0 && zscore_TibLat > -1)
          {   
            document.getElementById('tibLat_plot7').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot7'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot7') ;
          } 
          else if(zscore_TibLat == 0)
          {    
            document.getElementById('tibLat_plot8').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot8'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot8') ;
          }
          else if(zscore_TibLat < 1 && zscore_TibLat > 0)
          {    
            document.getElementById('tibLat_plot9').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot9'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot9') ;
          }
          else if(zscore_TibLat == 1)
          {    
            document.getElementById('tibLat_plot10').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot10'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot10') ;
          }
          else if(zscore_TibLat < 2 && zscore_TibLat > 1)
          {    
            document.getElementById('tibLat_plot11').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot11'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot11') ;
          } 
          else if(zscore_TibLat == 2)
          {    
            document.getElementById('tibLat_plot12').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot12'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot12') ;
          }
          else if(zscore_TibLat < 3 && zscore_TibLat > 2)
          {    
            document.getElementById('tibLat_plot13').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot13'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot13') ;
          } 
          else if(zscore_TibLat == 3)
          {    
            document.getElementById('tibLat_plot14').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot14'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot14') ;
          }
          else if(zscore_TibLat > 3)
          {    
            document.getElementById('tibLat_plot15').style.opacity = "1" ;          
            if(start24 !== undefined)
            {
             connect(start24, document.getElementById('tibLat_plot15'), "#ec6565", 1); 
            }            
            var start25 = document.getElementById('tibLat_plot15') ;
          }          
         
        }
        
        
    var zscore_TibMed = document.getElementById("zscore_TibMed").innerHTML ;
     
        if(zscore_TibMed !== "")
        {
          if(zscore_TibMed < -3)
          {    
            document.getElementById('tibMed_plot1').style.opacity = "1" ; 
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot1'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed == -3)
          {    
            document.getElementById('tibMed_plot2').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot2'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < -2 && zscore_TibMed > -3)
          {    
            document.getElementById('tibMed_plot3').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot3'), "#ec6565", 1); 
            }            
          } 
          else if(zscore_TibMed == -2)
          {    
            document.getElementById('tibMed_plot4').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot4'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < -1 && zscore_TibMed > -2)
          {    
            document.getElementById('tibMed_plot5').style.opacity = "1" ;   
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot5'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed == -1)
          {    
            document.getElementById('tibMed_plot6').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot6'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < 0 && zscore_TibMed > -1)
          {   
            document.getElementById('tibMed_plot7').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot7'), "#ec6565", 1);
            }            
          } 
          else if(zscore_TibMed == 0)
          {    
            document.getElementById('tibMed_plot8').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot8'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < 1 && zscore_TibMed > 0)
          {    
            document.getElementById('tibMed_plot9').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot9'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed == 1)
          {    
            document.getElementById('tibMed_plot10').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot10'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < 2 && zscore_TibMed > 1)
          {    
            document.getElementById('tibMed_plot11').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot11'), "#ec6565", 1);
            }            
          } 
          else if(zscore_TibMed == 2)
          {    
            document.getElementById('tibMed_plot12').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot12'), "#ec6565", 1);
            }            
          }
          else if(zscore_TibMed < 3 && zscore_TibMed > 2)
          {    
            document.getElementById('tibMed_plot13').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot13'), "#ec6565", 1);
            }            
          } 
          else if(zscore_TibMed == 3)
          {    
            document.getElementById('tibMed_plot14').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot14'), "#ec6565", 1); 
            }            
          }
          else if(zscore_TibMed > 3)
          {    
            document.getElementById('tibMed_plot15').style.opacity = "1" ;          
            if(start25 !== undefined)
            {
             connect(start25, document.getElementById('tibMed_plot15'), "#ec6565", 1);
            }            
          }          
         
        } 
        
        
        
    var zscore_Biac = document.getElementById("zscore_Biac").innerHTML ;
     
        if(zscore_Biac !== "")
        {
          if(zscore_Biac < -3)
          {    
            document.getElementById('biac_plot1').style.opacity = "1" ;             
            var start26 = document.getElementById('biac_plot1') ;
          }
          else if(zscore_Biac == -3)
          {    
            document.getElementById('biac_plot2').style.opacity = "1" ;           
            var start26 = document.getElementById('biac_plot2') ;
          }
          else if(zscore_Biac < -2 && zscore_Biac > -3)
          {    
            document.getElementById('biac_plot3').style.opacity = "1" ;     
            var start26 = document.getElementById('biac_plot3') ;
          } 
          else if(zscore_Biac == -2)
          {    
            document.getElementById('biac_plot4').style.opacity = "1" ;         
            var start26 = document.getElementById('biac_plot4') ;
          }
          else if(zscore_Biac < -1 && zscore_Biac > -2)
          {    
            document.getElementById('biac_plot5').style.opacity = "1" ;     
            var start26 = document.getElementById('biac_plot5') ;
          }
          else if(zscore_Biac == -1)
          {    
            document.getElementById('biac_plot6').style.opacity = "1" ;         
            var start26 = document.getElementById('biac_plot6') ;
          }
          else if(zscore_Biac < 0 && zscore_Biac > -1)
          {   
            document.getElementById('biac_plot7').style.opacity = "1" ;          
            var start26 = document.getElementById('biac_plot7') ;
          } 
          else if(zscore_Biac == 0)
          {    
            document.getElementById('biac_plot8').style.opacity = "1" ;     
            var start26 = document.getElementById('biac_plot8') ;
          }
          else if(zscore_Biac < 1 && zscore_Biac > 0)
          {    
            document.getElementById('biac_plot9').style.opacity = "1" ;      
            var start26 = document.getElementById('biac_plot9') ;
          }
          else if(zscore_Biac == 1)
          {    
            document.getElementById('biac_plot10').style.opacity = "1" ;         
            var start26 = document.getElementById('biac_plot10') ;
          }
          else if(zscore_Biac < 2 && zscore_Biac > 1)
          {    
            document.getElementById('biac_plot11').style.opacity = "1" ;       
            var start26 = document.getElementById('biac_plot11') ;
          } 
          else if(zscore_Biac == 2)
          {    
            document.getElementById('biac_plot12').style.opacity = "1" ;        
            var start26 = document.getElementById('biac_plot12') ;
          }
          else if(zscore_Biac < 3 && zscore_Biac > 2)
          {    
            document.getElementById('biac_plot13').style.opacity = "1" ;        
            var start26 = document.getElementById('biac_plot13') ;
          } 
          else if(zscore_Biac == 3)
          {    
            document.getElementById('biac_plot14').style.opacity = "1" ;    
            var start26 = document.getElementById('biac_plot14') ;
          }
          else if(zscore_Biac > 3)
          {  
            document.getElementById('biac_plot15').style.opacity = "1" ;    
            var start26 = document.getElementById('biac_plot15') ;
          }          
         
        } 
        
        
    var zscore_Bideltoid = document.getElementById("zscore_Bideltoid").innerHTML ;
     
        if(zscore_Bideltoid !== "")
        {
          if(zscore_Bideltoid < -3)
          {    
            document.getElementById('bideltoid_plot1').style.opacity = "1" ;  
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot1'), "#ec6565", 1); 
            }             
            var start27 = document.getElementById('bideltoid_plot1') ;
          }
          else if(zscore_Bideltoid == -3)
          {    
            document.getElementById('bideltoid_plot2').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot2'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot2') ;
          }
          else if(zscore_Bideltoid < -2 && zscore_Bideltoid > -3)
          {    
            document.getElementById('bideltoid_plot3').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot3'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot3') ;
          } 
          else if(zscore_Bideltoid == -2)
          {    
            document.getElementById('bideltoid_plot4').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot4'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot4') ;
          }
          else if(zscore_Bideltoid < -1 && zscore_Bideltoid > -2)
          {    
            document.getElementById('bideltoid_plot5').style.opacity = "1" ;   
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot5'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot5') ;
          }
          else if(zscore_Bideltoid == -1)
          {    
            document.getElementById('bideltoid_plot6').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot6'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot6') ;
          }
          else if(zscore_Bideltoid < 0 && zscore_Bideltoid > -1)
          {   
            document.getElementById('bideltoid_plot7').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot7'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot7') ;
          } 
          else if(zscore_Bideltoid == 0)
          {    
            document.getElementById('bideltoid_plot8').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot8'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot8') ;
          }
          else if(zscore_Bideltoid < 1 && zscore_Bideltoid > 0)
          {    
            document.getElementById('bideltoid_plot9').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot9'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot9') ;
          }
          else if(zscore_Bideltoid == 1)
          {    
            document.getElementById('bideltoid_plot10').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot10'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot10') ;
          }
          else if(zscore_Bideltoid < 2 && zscore_Bideltoid > 1)
          {    
            document.getElementById('bideltoid_plot11').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot11'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot11') ;
          } 
          else if(zscore_Bideltoid == 2)
          {    
            document.getElementById('bideltoid_plot12').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot12'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot12') ;
          }
          else if(zscore_Bideltoid < 3 && zscore_Bideltoid > 2)
          {    
            document.getElementById('bideltoid_plot13').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot13'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot13') ;
          } 
          else if(zscore_Bideltoid == 3)
          {    
            document.getElementById('bideltoid_plot14').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot14'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot14') ;
          }
          else if(zscore_Bideltoid > 3)
          {    
            document.getElementById('bideltoid_plot15').style.opacity = "1" ;          
            if(start26 !== undefined)
            {
             connect(start26, document.getElementById('bideltoid_plot15'), "#ec6565", 1); 
            }            
            var start27 = document.getElementById('bideltoid_plot15') ;
          }          
         
        }        
    
    var zscore_Billio = document.getElementById("zscore_Billio").innerHTML ;
     
        if(zscore_Billio !== "")
        {
          if(zscore_Billio < -3)
          {    
            document.getElementById('billio_plot1').style.opacity = "1" ;
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot1'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot1') ;
          }
          else if(zscore_Billio == -3)
          {    
            document.getElementById('billio_plot2').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot2'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot2') ;
          }
          else if(zscore_Billio < -2 && zscore_Billio > -3)
          {    
            document.getElementById('billio_plot3').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot3'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot3') ;
          } 
          else if(zscore_Billio == -2)
          {    
            document.getElementById('billio_plot4').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot4'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot4') ;
          }
          else if(zscore_Billio < -1 && zscore_Billio > -2)
          {    
            document.getElementById('billio_plot5').style.opacity = "1" ;   
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot5'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot5') ;
          }
          else if(zscore_Billio == -1)
          {    
            document.getElementById('billio_plot6').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot6'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot6') ;
          }
          else if(zscore_Billio < 0 && zscore_Billio > -1)
          {   
            document.getElementById('billio_plot7').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot7'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot7') ;
          } 
          else if(zscore_Billio == 0)
          {    
            document.getElementById('billio_plot8').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot8'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot8') ;
          }
          else if(zscore_Billio < 1 && zscore_Billio > 0)
          {    
            document.getElementById('billio_plot9').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot9'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot9') ;
          }
          else if(zscore_Billio == 1)
          {    
            document.getElementById('billio_plot10').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot10'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot10') ;
          }
          else if(zscore_Billio < 2 && zscore_Billio > 1)
          {    
            document.getElementById('billio_plot11').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot11'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot11') ;
          } 
          else if(zscore_Billio == 2)
          {    
            document.getElementById('billio_plot12').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot12'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot12') ;
          }
          else if(zscore_Billio < 3 && zscore_Billio > 2)
          {    
            document.getElementById('billio_plot13').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot13'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot13') ;
          } 
          else if(zscore_Billio == 3)
          {    
            document.getElementById('billio_plot14').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot14'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot14') ;
          }
          else if(zscore_Billio > 3)
          {    
            document.getElementById('billio_plot15').style.opacity = "1" ;          
            if(start27 !== undefined)
            {
             connect(start27, document.getElementById('billio_plot15'), "#ec6565", 1); 
            }            
            var start28 = document.getElementById('billio_plot15') ;
          }          
         
        }  
        
    var zscore_Bitrochanteric = document.getElementById("zscore_Bitrochanteric").innerHTML ;
     
        if(zscore_Bitrochanteric !== "")
        {
          if(zscore_Bitrochanteric < -3)
          {    
            document.getElementById('bitrochanteric_plot1').style.opacity = "1" ;  
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot1'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot1') ;
          }
          else if(zscore_Bitrochanteric == -3)
          {    
            document.getElementById('bitrochanteric_plot2').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot2'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot2') ;
          }
          else if(zscore_Bitrochanteric < -2 && zscore_Bitrochanteric > -3)
          {    
            document.getElementById('bitrochanteric_plot3').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot3'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot3') ;
          } 
          else if(zscore_Bitrochanteric == -2)
          {    
            document.getElementById('bitrochanteric_plot4').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot4'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot4') ;
          }
          else if(zscore_Bitrochanteric < -1 && zscore_Bitrochanteric > -2)
          {    
            document.getElementById('bitrochanteric_plot5').style.opacity = "1" ;   
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot5'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot5') ;
          }
          else if(zscore_Bitrochanteric == -1)
          {    
            document.getElementById('bitrochanteric_plot6').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot6'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot6') ;
          }
          else if(zscore_Bitrochanteric < 0 && zscore_Bitrochanteric > -1)
          {   
            document.getElementById('bitrochanteric_plot7').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot7'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot7') ;
          } 
          else if(zscore_Bitrochanteric == 0)
          {    
            document.getElementById('bitrochanteric_plot8').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot8'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot8') ;
          }
          else if(zscore_Bitrochanteric < 1 && zscore_Bitrochanteric > 0)
          {    
            document.getElementById('bitrochanteric_plot9').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot9'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot9') ;
          }
          else if(zscore_Bitrochanteric == 1)
          {    
            document.getElementById('bitrochanteric_plot10').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot10'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot10') ;
          }
          else if(zscore_Bitrochanteric < 2 && zscore_Bitrochanteric > 1)
          {    
            document.getElementById('bitrochanteric_plot11').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot11'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot11') ;
          } 
          else if(zscore_Bitrochanteric == 2)
          {    
            document.getElementById('bitrochanteric_plot12').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot12'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot12') ;
          }
          else if(zscore_Bitrochanteric < 3 && zscore_Bitrochanteric > 2)
          {    
            document.getElementById('bitrochanteric_plot13').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot13'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot13') ;
          } 
          else if(zscore_Bitrochanteric == 3)
          {    
            document.getElementById('bitrochanteric_plot14').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot14'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot14') ;
          }
          else if(zscore_Bitrochanteric > 3)
          {    
            document.getElementById('bitrochanteric_plot15').style.opacity = "1" ;          
            if(start28 !== undefined)
            {
             connect(start28, document.getElementById('bitrochanteric_plot15'), "#ec6565", 1); 
            }            
            var start29 = document.getElementById('bitrochanteric_plot15') ;
          }          
         
        }  
        
    var zscore_Foot = document.getElementById("zscore_Foot").innerHTML ;
     
        if(zscore_Foot !== "")
        {
          if(zscore_Foot < -3)
          {    
            document.getElementById('foot_plot1').style.opacity = "1" ;
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot1'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot1') ;
          }
          else if(zscore_Foot == -3)
          {    
            document.getElementById('foot_plot2').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot2'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot2') ;
          }
          else if(zscore_Foot < -2 && zscore_Foot > -3)
          {    
            document.getElementById('foot_plot3').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot3'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot3') ;
          } 
          else if(zscore_Foot == -2)
          {    
            document.getElementById('foot_plot4').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot4'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot4') ;
          }
          else if(zscore_Foot < -1 && zscore_Foot > -2)
          {    
            document.getElementById('foot_plot5').style.opacity = "1" ;   
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot5'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot5') ;
          }
          else if(zscore_Foot == -1)
          {    
            document.getElementById('foot_plot6').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot6'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot6') ;
          }
          else if(zscore_Foot < 0 && zscore_Foot > -1)
          {   
            document.getElementById('foot_plot7').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot7'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot7') ;
          } 
          else if(zscore_Foot == 0)
          {    
            document.getElementById('foot_plot8').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot8'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot8') ;
          }
          else if(zscore_Foot < 1 && zscore_Foot > 0)
          {    
            document.getElementById('foot_plot9').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot9'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot9') ;
          }
          else if(zscore_Foot == 1)
          {    
            document.getElementById('foot_plot10').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot10'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot10') ;
          }
          else if(zscore_Foot < 2 && zscore_Foot > 1)
          {    
            document.getElementById('foot_plot11').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot11'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot11') ;
          } 
          else if(zscore_Foot == 2)
          {    
            document.getElementById('foot_plot12').style.opacity = "1" ; 
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot12'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot12') ;
          }
          else if(zscore_Foot < 3 && zscore_Foot > 2)
          {    
            document.getElementById('foot_plot13').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot13'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot13') ;
          } 
          else if(zscore_Foot == 3)
          {    
            document.getElementById('foot_plot14').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot14'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot14') ;
          }
          else if(zscore_Foot > 3)
          {    
            document.getElementById('foot_plot15').style.opacity = "1" ;          
            if(start29 !== undefined)
            {
             connect(start29, document.getElementById('foot_plot15'), "#ec6565", 1); 
            }            
            var start30 = document.getElementById('foot_plot15') ;
          }          
         
        }  
        
    var zscore_Sitting = document.getElementById("zscore_Sitting").innerHTML ;
     
        if(zscore_Sitting !== "")
        {
          if(zscore_Sitting < -3)
          {    
            document.getElementById('sitting_plot1').style.opacity = "1" ; 
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot1'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot1') ;
          }
          else if(zscore_Sitting == -3)
          {    
            document.getElementById('sitting_plot2').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot2'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot2') ;
          }
          else if(zscore_Sitting < -2 && zscore_Sitting > -3)
          {    
            document.getElementById('sitting_plot3').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot3'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot3') ;
          } 
          else if(zscore_Sitting == -2)
          {    
            document.getElementById('sitting_plot4').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot4'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot4') ;
          }
          else if(zscore_Sitting < -1 && zscore_Sitting > -2)
          {    
            document.getElementById('sitting_plot5').style.opacity = "1" ;   
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot5'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot5') ;
          }
          else if(zscore_Sitting == -1)
          {    
            document.getElementById('sitting_plot6').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot6'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot6') ;
          }
          else if(zscore_Sitting < 0 && zscore_Sitting > -1)
          {   
            document.getElementById('sitting_plot7').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot7'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot7') ;
          } 
          else if(zscore_Sitting == 0)
          {    
            document.getElementById('sitting_plot8').style.opacity = "1" ;  
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot8'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot8') ;
          }
          else if(zscore_Sitting < 1 && zscore_Sitting > 0)
          {    
            document.getElementById('sitting_plot9').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot9'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot9') ;
          }
          else if(zscore_Sitting == 1)
          {    
            document.getElementById('sitting_plot10').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot10'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot10') ;
          }
          else if(zscore_Sitting < 2 && zscore_Sitting > 1)
          {    
            document.getElementById('sitting_plot11').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot11'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot11') ;
          } 
          else if(zscore_Sitting == 2)
          {    
            document.getElementById('sitting_plot12').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot12'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot12') ;
          }
          else if(zscore_Sitting < 3 && zscore_Sitting > 2)
          {    
            document.getElementById('sitting_plot13').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot13'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot13') ;
          } 
          else if(zscore_Sitting == 3)
          {    
            document.getElementById('sitting_plot14').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot14'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot14') ;
          }
          else if(zscore_Sitting > 3)
          {    
            document.getElementById('sitting_plot15').style.opacity = "1" ;          
            if(start30 !== undefined)
            {
             connect(start30, document.getElementById('sitting_plot15'), "#ec6565", 1); 
            }            
            var start31 = document.getElementById('sitting_plot15') ;
          }          
         
        }         
        
        
    var zscore_TrChest = document.getElementById("zscore_TrChest").innerHTML ;
     
        if(zscore_TrChest !== "")
        {
          if(zscore_TrChest < -3)
          {    
            document.getElementById('trChest_plot1').style.opacity = "1" ;  
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot1'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot1') ;
          }
          else if(zscore_TrChest == -3)
          {    
            document.getElementById('trChest_plot2').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot2'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot2') ;
          }
          else if(zscore_TrChest < -2 && zscore_TrChest > -3)
          {    
            document.getElementById('trChest_plot3').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot3'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot3') ;
          } 
          else if(zscore_TrChest == -2)
          {    
            document.getElementById('trChest_plot4').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot4'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot4') ;
          }
          else if(zscore_TrChest < -1 && zscore_TrChest > -2)
          {    
            document.getElementById('trChest_plot5').style.opacity = "1" ;   
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot5'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot5') ;
          }
          else if(zscore_TrChest == -1)
          {    
            document.getElementById('trChest_plot6').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot6'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot6') ;
          }
          else if(zscore_TrChest < 0 && zscore_TrChest > -1)
          {   
            document.getElementById('trChest_plot7').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot7'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot7') ;
          } 
          else if(zscore_TrChest == 0)
          {    
            document.getElementById('trChest_plot8').style.opacity = "1" ;  
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot8'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot8') ;
          }
          else if(zscore_TrChest < 1 && zscore_TrChest > 0)
          {    
            document.getElementById('trChest_plot9').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot9'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot9') ;
          }
          else if(zscore_TrChest == 1)
          {    
            document.getElementById('trChest_plot10').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot10'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot10') ;
          }
          else if(zscore_TrChest < 2 && zscore_TrChest > 1)
          {    
            document.getElementById('trChest_plot11').style.opacity = "1" ;   
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot11'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot11') ;
          } 
          else if(zscore_TrChest == 2)
          {    
            document.getElementById('trChest_plot12').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot12'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot12') ;
          }
          else if(zscore_TrChest < 3 && zscore_TrChest > 2)
          {    
            document.getElementById('trChest_plot13').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot13'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot13') ;
          } 
          else if(zscore_TrChest == 3)
          {    
            document.getElementById('trChest_plot14').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot14'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot14') ;
          }
          else if(zscore_TrChest > 3)
          {    
            document.getElementById('trChest_plot15').style.opacity = "1" ;          
            if(start31 !== undefined)
            {
             connect(start31, document.getElementById('trChest_plot15'), "#ec6565", 1); 
            }            
            var start32 = document.getElementById('trChest_plot15') ;
          }          
         
        }
        
        
    var zscore_ApChest = document.getElementById("zscore_ApChest").innerHTML ;
     
        if(zscore_ApChest !== "")
        {
          if(zscore_ApChest < -3)
          {    
            document.getElementById('apChest_plot1').style.opacity = "1" ; 
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot1'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot1') ;
          }
          else if(zscore_ApChest == -3)
          {    
            document.getElementById('apChest_plot2').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot2'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot2') ;
          }
          else if(zscore_ApChest < -2 && zscore_ApChest > -3)
          {    
            document.getElementById('apChest_plot3').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot3'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot3') ;
          } 
          else if(zscore_ApChest == -2)
          {    
            document.getElementById('apChest_plot4').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot4'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot4') ;
          }
          else if(zscore_ApChest < -1 && zscore_ApChest > -2)
          {    
            document.getElementById('apChest_plot5').style.opacity = "1" ;   
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot5'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot5') ;
          }
          else if(zscore_ApChest == -1)
          {    
            document.getElementById('apChest_plot6').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot6'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot6') ;
          }
          else if(zscore_ApChest < 0 && zscore_ApChest > -1)
          {   
            document.getElementById('apChest_plot7').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot7'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot7') ;
          } 
          else if(zscore_ApChest == 0)
          {    
            document.getElementById('apChest_plot8').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot8'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot8') ;
          }
          else if(zscore_ApChest < 1 && zscore_ApChest > 0)
          {    
            document.getElementById('apChest_plot9').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot9'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot9') ;
          }
          else if(zscore_ApChest == 1)
          {    
            document.getElementById('apChest_plot10').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot10'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot10') ;
          }
          else if(zscore_ApChest < 2 && zscore_ApChest > 1)
          {    
            document.getElementById('apChest_plot11').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot11'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot11') ;
          } 
          else if(zscore_ApChest == 2)
          {    
            document.getElementById('apChest_plot12').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot12'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot12') ;
          }
          else if(zscore_ApChest < 3 && zscore_ApChest > 2)
          {    
            document.getElementById('apChest_plot13').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot13'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot13') ;
          } 
          else if(zscore_ApChest == 3)
          {    
            document.getElementById('apChest_plot14').style.opacity = "1" ;   
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot14'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot14') ;
          }
          else if(zscore_ApChest > 3)
          {    
            document.getElementById('apChest_plot15').style.opacity = "1" ;          
            if(start32 !== undefined)
            {
             connect(start32, document.getElementById('apChest_plot15'), "#ec6565", 1); 
            }            
            var start33 = document.getElementById('apChest_plot15') ;
          }          
         
        } 
            

    var zscore_ArmSpan = document.getElementById("zscore_ArmSpan").innerHTML ;
     
        if(zscore_ArmSpan !== "")
        {
          if(zscore_ArmSpan < -3)
          {    
            document.getElementById('armSpan_plot1').style.opacity = "1" ;  
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot1'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot1') ;
          }
          else if(zscore_ArmSpan == -3)
          {    
            document.getElementById('armSpan_plot2').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot2'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot2') ;
          }
          else if(zscore_ArmSpan < -2 && zscore_ArmSpan > -3)
          {    
            document.getElementById('armSpan_plot3').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot3'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot3') ;
          } 
          else if(zscore_ArmSpan == -2)
          {    
            document.getElementById('armSpan_plot4').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot4'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot4') ;
          }
          else if(zscore_ArmSpan < -1 && zscore_ArmSpan > -2)
          {    
            document.getElementById('armSpan_plot5').style.opacity = "1" ;   
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot5'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot5') ;
          }
          else if(zscore_ArmSpan == -1)
          {    
            document.getElementById('armSpan_plot6').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot6'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot6') ;
          }
          else if(zscore_ArmSpan < 0 && zscore_ArmSpan > -1)
          {   
            document.getElementById('armSpan_plot7').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot7'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot7') ;
          } 
          else if(zscore_ArmSpan == 0)
          {    
            document.getElementById('armSpan_plot8').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot8'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot8') ;
          }
          else if(zscore_ArmSpan < 1 && zscore_ArmSpan > 0)
          {    
            document.getElementById('armSpan_plot9').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot9'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot9') ;
          }
          else if(zscore_ArmSpan == 1)
          {    
            document.getElementById('armSpan_plot10').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot10'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot10') ;
          }
          else if(zscore_ArmSpan < 2 && zscore_ArmSpan > 1)
          {    
            document.getElementById('armSpan_plot11').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot11'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot11') ;
          } 
          else if(zscore_ArmSpan == 2)
          {    
            document.getElementById('armSpan_plot12').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot12'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot12') ;
          }
          else if(zscore_ArmSpan < 3 && zscore_ArmSpan > 2)
          {    
            document.getElementById('armSpan_plot13').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot13'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot13') ;
          } 
          else if(zscore_ArmSpan == 3)
          {    
            document.getElementById('armSpan_plot14').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot14'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot14') ;
          }
          else if(zscore_ArmSpan > 3)
          {    
            document.getElementById('armSpan_plot15').style.opacity = "1" ;          
            if(start33 !== undefined)
            {
             connect(start33, document.getElementById('armSpan_plot15'), "#ec6565", 1); 
            }            
            var start34 = document.getElementById('armSpan_plot15') ;
          }          
         
        }
        
      
    var zscore_Humerus = document.getElementById("zscore_Humerus").innerHTML ;
     
        if(zscore_Humerus !== "")
        {
          if(zscore_Humerus < -3)
          {    
            document.getElementById('humerus_plot1').style.opacity = "1" ; 
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot1'), "#ec6565", 1); 
            }            
            var start35 = document.getElementById('humerus_plot1') ;
          }
          else if(zscore_Humerus == -3)
          {    
            document.getElementById('humerus_plot2').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot2'), "#ec6565", 1);            
            }            
            var start35 = document.getElementById('humerus_plot2') ;
          }
          else if(zscore_Humerus < -2 && zscore_Humerus > -3)
          {    
            document.getElementById('humerus_plot3').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot3'), "#ec6565", 1);      
            }            
            var start35 = document.getElementById('humerus_plot3') ;
          } 
          else if(zscore_Humerus == -2)
          {    
            document.getElementById('humerus_plot4').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot4'), "#ec6565", 1);          
            }            
            var start35 = document.getElementById('humerus_plot4') ;
          }
          else if(zscore_Humerus < -1 && zscore_Humerus > -2)
          {    
            document.getElementById('humerus_plot5').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot5'), "#ec6565", 1);      
            }            
            var start35 = document.getElementById('humerus_plot5') ;
          }
          else if(zscore_Humerus == -1)
          {    
            document.getElementById('humerus_plot6').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot6'), "#ec6565", 1);          
            }            
            var start35 = document.getElementById('humerus_plot6') ;
          }
          else if(zscore_Humerus < 0 && zscore_Humerus > -1)
          {   
            document.getElementById('humerus_plot7').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot7'), "#ec6565", 1);           
            }            
            var start35 = document.getElementById('humerus_plot7') ;
          } 
          else if(zscore_Humerus == 0)
          {    
            document.getElementById('humerus_plot8').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot8'), "#ec6565", 1);      
            }            
            var start35 = document.getElementById('humerus_plot8') ;
          }
          else if(zscore_Humerus < 1 && zscore_Humerus > 0)
          {    
            document.getElementById('humerus_plot9').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot9'), "#ec6565", 1);       
            }            
            var start35 = document.getElementById('humerus_plot9') ;
          }
          else if(zscore_Humerus == 1)
          {    
            document.getElementById('humerus_plot10').style.opacity = "1" ; 
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot10'), "#ec6565", 1);          
            }            
            var start35 = document.getElementById('humerus_plot10') ;
          }
          else if(zscore_Humerus < 2 && zscore_Humerus > 1)
          {    
            document.getElementById('humerus_plot11').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot11'), "#ec6565", 1);        
            }            
            var start35 = document.getElementById('humerus_plot11') ;
          } 
          else if(zscore_Humerus == 2)
          {    
            document.getElementById('humerus_plot12').style.opacity = "1" ; 
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot12'), "#ec6565", 1);         
            }            
            var start35 = document.getElementById('humerus_plot12') ;
          }
          else if(zscore_Humerus < 3 && zscore_Humerus > 2)
          {    
            document.getElementById('humerus_plot13').style.opacity = "1" ; 
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot13'), "#ec6565", 1);         
            }            
            var start35 = document.getElementById('humerus_plot13') ;
          } 
          else if(zscore_Humerus == 3)
          {    
            document.getElementById('humerus_plot14').style.opacity = "1" ;
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot14'), "#ec6565", 1);     
            }            
            var start35 = document.getElementById('humerus_plot14') ;
          }
          else if(zscore_Humerus > 3)
          {    
            document.getElementById('humerus_plot15').style.opacity = "1" ; 
            if(start34 !== undefined)
            {
             connect(start34, document.getElementById('humerus_plot15'), "#ec6565", 1);     
            }            
            var start35 = document.getElementById('humerus_plot15') ;
          }          
         
        }
        
        
    var zscore_Femur = document.getElementById("zscore_Femur").innerHTML ;
     
        if(zscore_Femur !== "")
        {
          if(zscore_Femur < -3)
          {    
            document.getElementById('femur_plot1').style.opacity = "1" ;  
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot1'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur == -3)
          {    
            document.getElementById('femur_plot2').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot2'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur < -2 && zscore_Femur > -3)
          {    
            document.getElementById('femur_plot3').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot3'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_Femur == -2)
          {    
            document.getElementById('femur_plot4').style.opacity = "1" ;     
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot4'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur < -1 && zscore_Femur > -2)
          {    
            document.getElementById('femur_plot5').style.opacity = "1" ;   
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot5'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur == -1)
          {    
            document.getElementById('femur_plot6').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot6'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur < 0 && zscore_Femur > -1)
          {   
            document.getElementById('femur_plot7').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot7'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_Femur == 0)
          {    
            document.getElementById('femur_plot8').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot8'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur < 1 && zscore_Femur > 0)
          {    
            document.getElementById('femur_plot9').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot9'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur == 1)
          {    
            document.getElementById('femur_plot10').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot10'), "#ec6565", 1);             
            }            
          }
          else if(zscore_Femur < 2 && zscore_Femur > 1)
          {    
            document.getElementById('femur_plot11').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot11'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Femur == 2)
          {    
            document.getElementById('femur_plot12').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot12'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Femur < 3 && zscore_Femur > 2)
          {    
            document.getElementById('femur_plot13').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot13'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_Femur == 3)
          {    
            document.getElementById('femur_plot14').style.opacity = "1" ;          
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot14'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Femur > 3)
          {    
            document.getElementById('femur_plot15').style.opacity = "1" ; 
            if(start35 !== undefined)
            {
             connect(start35, document.getElementById('femur_plot15'), "#ec6565", 1);             
            }            
          }          
         
        }      
  
}); 

</script> 


<script>

 function connect(div1, div2, color, thickness) {
    var off1 = getOffset(div1);
    var off2 = getOffset(div2);
    // bottom right
    var xhalf1 = off1.width;
    var x1 = off1.left + (xhalf1 / 2);        
    var y1 = off1.top + off1.height;
    
    // top right
    var xhalf2 = off2.width;
    var x2 = off2.left + (xhalf2 / 2);
    var y2 = off2.top;
    // distance
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    // center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    // angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);
    // make hr
    var htmlLine = "<div style='padding:0px; margin:0px; border-bottom:1px dashed " + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
    //
    //alert(htmlLine);
    document.body.innerHTML += htmlLine; 
}

function getOffset(el) {
    var rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.pageXOffset,
        top: rect.top + window.pageYOffset,
        width: rect.width || el.offsetWidth,
        height: rect.height || el.offsetHeight
    };
}   
    
</script>
    
</body>
</html>
