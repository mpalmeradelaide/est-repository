<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""></a>
                 <div class="print_icon">
                    <a class="strength_btns print_icon_toggle_btn"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""></a>
                    <ul class="print_icon_toggle">                                      
                        <li><form class="form_print"><a onclick="window.print(); return false;">Print Report</a></form></li>                        
                        <li><form class="form_print"><a href="#">E-mail Report</a></form></li>
                    </ul>
                </div>  
                <h1 class="page_head">Skinfold analysis</h1>   
                <h1 class="page_head"><span><?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</span></h1>               
              </td>
              </tr>
                 <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>">                     
            	<tr>                
                <td colspan="3" align="center"> 
                	<table width="100%">
                      <tbody> 
                        <tr> 
                          <td valign="top" width="30%">
                          		<div class="skinfold_head">sum of skinfolds</div>
                                
                          		<table width="100%" class="tabel_bord tabel_bord_skinfold" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>&nbsp;</td> 	
                                      <td>Skinfolds</td>
                                      <td>mm</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>Triceps</td>
                                      <td><input type="text" id="triceps" name="triceps" class="cus-input width_60" value="<?php echo $skinfolds_Values["triceps"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>Subscapular</td>
                                      <td><input type="text" id="subscapular" name="subscapular" class="cus-input width_60" value="<?php echo $skinfolds_Values["subscapular"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>biceps</td>
                                      <td><input type="text" id="biceps" name="biceps" class="cus-input width_60" value="<?php echo $skinfolds_Values["biceps"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>iliac crest</td>
                                      <td><input type="text" id="iliac_crest" name="iliac_crest" class="cus-input width_60" value="<?php echo $skinfolds_Values["iliac_crest"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>supraspinale</td>
                                      <td><input type="text" id="supraspinale" name="supraspinale" class="cus-input width_60" value="<?php echo $skinfolds_Values["supraspinale"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>abdominal</td>
                                      <td><input type="text" id="abdominal" name="abdominal" class="cus-input width_60" value="<?php echo $skinfolds_Values["abdominal"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>front thigh</td>
                                      <td><input type="text" id="thigh" name="thigh" class="cus-input width_60" value="<?php echo $skinfolds_Values["thigh"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>medial calf</td>
                                      <td><input type="text" id="calf" name="calf" class="cus-input width_60" value="<?php echo $skinfolds_Values["calf"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                    	<td><div class="red_circle">&nbsp;</div></td> 	
                                      <td>mid-axilla</td>
                                      <td><input type="text" id="mid_axilla" name="mid_axilla" class="cus-input width_60" value="<?php echo $skinfolds_Values["mid_axilla"] ; ?>"> </td>
                                    </tr>                                
                                  </tbody>
                            </table>
                          </td> 
                          <td valign="top">
                          		<div class="skinfold_head">population norms</div> 
                          		<div class="diag_chart">
                                	<div class="graph">
                                      
                                      <ul class="bottom_values">
                                        <li><span>1</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>5</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>25</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>50</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>75</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>90</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                        <li><span>95</span>
                                          <div class="blue_dot green_circle" style="display:none;">&nbsp;</div>
                                        </li>
                                      </ul>
                                      
                                      <div class="graph_pulse_box"><img src="http://115.112.118.252/healthscreen//assets/images//pulse2.png" alt=""></div>
                                      <div class="percentile_box">
                                      	<input type="text" id="percentile" name="percentile" class="cus-input width_60"> %'ile
                                      </div>
                                    </div>
                                
                                </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
              <td align="left">
              	<table class="sigma_table">
                	<tr>
                            <td><a class="rest_bot_btns" id="four_Sites"><span><div id="sum4" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a></td>
                        <td align="left">&Sigma; 4 skinfolds</td>
                        <td><input type="text" name="skinfold" class="cus-input"> mm</td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="five_Sites"><span><div id="sum5" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 5 skinfolds</td>
                        <td><input type="text" name="skinfold" class="cus-input"></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="six_Sites"><span><div id="sum6" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 6 skinfolds</td>
                        <td><input type="text" name="skinfold" class="cus-input"></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="seven_Sites"><span><div id="sum7" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 7 skinfolds</td>
                        <td><input type="text" name="skinfold" class="cus-input"></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="eight_Sites"><span><div id="sum8" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 8 skinfolds</td>
                        <td><input type="text" name="skinfold" class="cus-input"></td>
                    </tr>
                </table>
              </td> 
			  
              <td align="right" colspan="2" valign="bottom">       
                <div style="display:block; margin-bottom: 115px;"><button id="plot" name="plot" class="plot_btn" style="margin-top:0;">Skinfold Map</button></div>
                <button class="number_btn">1</button>
                <button class="number_btn">2</button>
                <button class="number_btn">3</button>
                <button class="number_btn">4</button>
                <button class="number_btn">5</button>
                <button class="number_btn">6</button>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>

             <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

        $(document).on('click','#exit', function(){
            document.forms["myform"].submit();
            //return false;
        });
        
        $(document).on('click','#plot', function(){
         location.href = '<?php echo site_url();?>/Body/skinfold_Map' ;
        });
        
         $(document).on('click','#four_Sites', function(){
            document.getElementById("sumSites").value = "4" ; 
            document.getElementById("sum4").setAttribute('style', ""); 
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#five_Sites', function(){
            document.getElementById("sumSites").value = "5" ; 
            document.getElementById("sum5").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#six_Sites', function(){
            document.getElementById("sumSites").value = "6" ; 
            document.getElementById("sum6").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#seven_Sites', function(){
            document.getElementById("sumSites").value = "7" ; 
            document.getElementById("sum7").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum8").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ; 
            document.getElementById("sum8").setAttribute('style', "");
            document.getElementById("sum4").setAttribute('style', "display:none");
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("sum7").setAttribute('style', "display:none");
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ;   
        });
	
	$(document).on('click','.info_icon_btn', function(){
	    $(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
</script>  



<script>   
//calculate %Body Fat value
    function getSumSkinfolds()
    { 
        var ageValue = document.getElementById("age").value;      
      
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        
        var sumSites = document.getElementById("sumSites").value ;
  
  
            if(parseInt(sumSites) == 4)
            {
                 var sum4Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ; 
                 var sumSquare4Skinfold = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;   

                 if(document.getElementById('gender').value == "Male")
                 {    
                   var percentFat = (0.29288 * sum4Skinfold) - (0.0005 * sumSquare4Skinfold) +  (0.15845 * (parseInt(ageValue))) + 5.76377 ;
                   percentFat = Math.round(parseFloat(percentFat) * 10) / 10 ;
                 }
                 else if(document.getElementById('gender').value == "Female") 
                 {
                   var percentFat = (0.29669 * sum4Skinfold) - (0.00043 * sumSquare4Skinfold) + (0.02963 * (parseInt(ageValue))) + 1.4072 ;                
                 }

                 percentFat = Math.round(percentFat * 10) / 10 ;
            }  
            
            document.getElementById("percentile").value = parseFloat(percentFat) ;   

    }	  
 
  

    
</script>
    
</body>
</html>
