<?php
class client_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          session_start();
    }
           
    public function save()
      {
        //$originalDate = $this->input->post('dob');
        //$newDate = date("d/m/Y", strtotime($originalDate));
          // print_r($this->input->post());
        $dob = $this->input->post('daydropdown')."/".$this->input->post('monthdropdown')."/".$this->input->post('yeardropdown');
            $code = array('first_name' => $this->input->post('first_name'),'last_name' => $this->input->post('last_name'),'email' => $this->input->post('email') ,'p_name' => $this->input->post('p_name'),'p_desc' => $this->input->post('p_desc'),'gender' => $this->input->post('gender'),'dob' => $dob,'country' => $this->input->post('country'),'occupation' => $this->input->post('occupation'));
			 //print_r($code);die;
            //$this->input->post('first_name')
         //   if($this->isExist( 'client_info',  $this->session->userdata('userid') )){
            if($this->isExist( 'client_info',  $_SESSION['userid'] )){
			
                   // $this->db->where('id', $this->session->userdata('userid'));
                    $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_info', $code); 
                    // return  $this->session->userdata('userid');
                     return $_SESSION['userid'];
                }else{
					
                      $this->db->insert('client_info', $code); 
                     return $this->db->insert_id();
      }
                
        
         
        
      }
      
    public function saveMedicalInfo()
      {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'notes' => $this->input->post('notes'),
              'option_8' => $this->input->post('option_8'));
                ///print_r($code ); die;
                //if($this->isExist( 'client_medical_info',  $this->session->userdata('userid') )){
                if($this->isExist( 'client_medical_info',  $_SESSION['userid'] )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medical_info', $code); 
                }else{

                    return $this->db->insert('client_medical_info', $code); 
                }               
          
      }
      
      
       public function savePhysicalActivityInfo()
      {
           
    
          $code = array(
             
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'));
        
          
           //if($this->isExist( 'client_physical_activity_info',  $this->session->userdata('userid') )){
           if($this->isExist( 'client_physical_activity_info', $_SESSION['userid'] )){
                    $this->db->where('c_id', $_SESSION['userid']);
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    return  $this->db->update('client_physical_activity_info', $code); 
                }else{

                    return  $this->db->insert('client_physical_activity_info', $code); 
                }   
                   
         
      }
      
      public function saveRiskFactorInfo()
      {
           
        
          $code = array(
            //  'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'),
              'option_10' => $this->input->post('options_10'),
              'option_11' => $this->input->post('options_11'),
              'option_12' => $this->input->post('options_12'),
              'option_13' => $this->input->post('options_13'),
              'option_gender' => $this->input->post('option_gender'),
              'option_age' => $this->input->post('option_age'),
              'option_smoke' => $this->input->post('option_smoke'),
              'option_smoke_6' => $this->input->post('option_smoke_6'));
            
          
            if($this->isExist( 'client_risk_factor_info',  $_SESSION['userid'] )){
            //if($this->isExist( 'client_risk_factor_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id',$_SESSION['userid']);
                    return  $this->db->update('client_risk_factor_info', $code); 
                }else{

                    return  $this->db->insert('client_risk_factor_info', $code); 
                }   
          
          //return  $this->db->insert('client_risk_factor_info', $code); 
      }
      
      
       public function saveBodyCompositionInfo()
      {
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_height' => $this->input->post('options_height'),
              'option_weight' => $this->input->post('options_weight'),
              'option_height_measured' => $this->input->post('options_height_measured'),
              'option_weight_measured' => $this->input->post('options_weight_measured'),
              'option_bmi' => round($this->input->post('bmi'), 1),
              'option_waist' => $this->input->post('waist'),
              'option_hip' => $this->input->post('hip'),
              'option_whr' => round($this->input->post('whr'), 2),
              'option_triceps' => $this->input->post('triceps'),
              'option_biceps' => $this->input->post('biceps'),
              'option_subscapular' => $this->input->post('subscapular'),
              'option_sos' => round($this->input->post('sos'), 1));
             
          
             if($this->isExist( 'client_body_composition_info',  $_SESSION['userid'])){
           //  if($this->isExist( 'client_body_composition_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_body_composition_info', $code); 
                }else{

                    return  $this->db->insert('client_body_composition_info', $code); 
                } 
                        
         // return  $this->db->insert('client_body_composition_info', $code); 
      }
      
      
       public function saveMeditationInfo()
      {
           
        
          $code = array(
             // 'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('option_1'),
              'medical_conditions' => $this->input->post('medicalConditions'),
              'medical_regular' => $this->input->post('medicalRegular'),
              'option_4' => $this->input->post('option_4'),
              'notes' => $this->input->post('notes'),
              'option_pregnant' => $this->input->post('option_pregnant'));
             
         // print_r($code)  ;
          
        //  if($this->isExist( 'client_medication_info',  $this->session->userdata('userid') )){
          if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
                   // $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medication_info', $code); 
                }else{

                    return  $this->db->insert('client_medication_info', $code); 
                } 
          
         // return  $this->db->insert('client_medication_info', $code); 
      }
      
       public function fetchScreeningInfo()
      {
           $this->fetchScreeningStatusInfo();
     //   $user_id=$this->session->userdata('userid');   
        $user_id=$_SESSION['userid'];   
        
       $q="SELECT ci.id,ci.p_desc as client_info, (pai.option_2 + 2*pai.option_4 + pai.option_6) as physical_activity ,rfi.option_1 as heart_disease,rfi.option_2 as smoking ,rfi.option_7 as hbp,rfi.option_8 as dbp ,rfi.option_11 as colestrol,rfi.option_9 as hdl,rfi.option_10 as ldl,rfi.option_13 as triglycerides,rfi.option_12 as hbg ,bci.option_bmi as bmi ,bci.option_waist as waist ,bci.option_hip as hip,ci.gender as gender ,ci.dob ,mi.option_8 as proceed ,rfi.option_gender,rfi.option_gender,rfi.option_age,rfi.option_3 as smoking2 ,rfi.option_4 as high_blood_pressure,rfi.option_5 as high_colestrol,rfi.option_6 as high_sugar , medi.medical_regular as llm , mi.notes as medical_notes ,medi.notes as medication_notes,medi.option_4 as medication_pic,medi.pt1 as pt1,medi.pt2 as pt2,medi.pt3 as pt3,medi.pt4 as pt4,medi.pt5 as pt5,medi.pt6 as pt6,medi.pt7 as pt7,medi.pt8 as pt8,medi.pt9 as pt9,medi.pt10 as pt10,medi.pt11 as pt11,medi.pt12 as pt12,medi.pt13 as pt13,medi.pt14 as pt14,medi.pt15 as pt15 ,bci.option_height as self_height ,bci.option_weight as self_weight  ,bci.option_height_measured as measured_height ,bci.option_weight_measured as measured_weight , pai.option_2 as pa2 , pai.option_6 as pa6, pai.option_4 as pa4 , pai.option_7 as pa7 , pai.option_8 as pa8,pai.option_9 as pa9, rfi.option_1 as family_history , rfi.option_2 as rf4 , rfi.option_smoke as rf5 , rfi.option_7 as rf_systolic , rfi.option_8 as rf_diastolic , rfi.option_smoke_6 as rf7 , rfi.option_4 as rf8, rfi.option_12 as rf_fbg , rfi.option_6 as rf10 , rfi.option_5 as rf9 ,bci.option_triceps,bci.option_biceps,option_subscapular,bci.option_sos FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id  where  ci.id= $user_id ";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
           
            /*** a date before 1970 ***/
        
            $date = str_replace('/', '-', $row['dob']);
                      
           $dateOfBirth=   date("Y-m-d", strtotime( $date) );
          
           $dob = strtotime( $dateOfBirth);

            /*** another date ***/
            $tdate = strtotime(date('Y-m-d'));
          
            array_push($row,array('age'=>$this->getAge($dob, $tdate)));
            array_push($row,array('status'=>$this->fetchScreeningStatusInfo()));
            return $row;
           
      }
      
      
      function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
        public function fetchScreeningStatusInfo()
      {
// var_dump($_SESSION);

    //  $user_id=$this->session->userdata('userid');   
      $user_id=$_SESSION['userid'];   
      //  echo "aaaaa";
    //  die(__FILE__);
        $q="SELECT * FROM `client_medical_info` where  c_id= $user_id";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
          // print_r($row);
           if($row['option_1']=='N' && $row['option_2']=='N' && $row['option_3']=='N' && $row['option_4']=='N' && $row['option_5']=='N' && $row['option_6']=='N' && $row['option_7']=='N' ){
           return 'N';
          }else{
               return 'Y';
           }
           
           // return $row;
           
      } 

  public function updateRiskFactorInfo($riskFacor,$variable)
      {
           
     

          $code = array(
             // 'id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'risk_factor' => $riskFacor,
              'screening_decision_category' => $variable);
          
         
                 //   $this->db->where('id', $this->session->userdata('userid'));
                    $this->db->where('id', $_SESSION['userid']);
                      $this->db->update('client_info', $code); 
                
          
      }  

  public function updateMedicationInfo($column,$status)
      {
           
       if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
             echo  $sql = "UPDATE client_medication_info SET ". $column ." = ".$status." WHERE c_id =".$_SESSION['userid'];
		}else{
		echo $sql = "insert into client_medication_info (". $column .",c_id) values ( ".$status.",".$_SESSION['userid'].")";
		}	   
$this->db->query($sql);
          
      } 	  
      
      function getAllProjects()
    {
        /*
        $query = $this->db->get('location');

        foreach ($query->result() as $row)
        {
            echo $row->description;
        }*/

        $query = $this->db->query('SELECT * FROM project_info');


        
        $return = array();
if($query->num_rows() > 0) {
        foreach($query->result_array() as $row) {
        //array_push($return,  $row['p_name']);
         $return [$row['id']] = $row['p_name'];
        }

}else{
        $return[$row['0']] = "select";   
         //array_push($return, "select");
        }
         return $return;
    }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    
    
    
      
}
?>