-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 15, 2016 at 07:28 AM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `fitnessr_stafi14`
--

-- --------------------------------------------------------

--
-- Table structure for table `age`
--

CREATE TABLE IF NOT EXISTS `age` (
  `id` int(11) NOT NULL,
  `age` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `age`
--

INSERT INTO `age` (`id`, `age`) VALUES
(1, '18-29'),
(2, '30-39'),
(3, '40-49'),
(4, '50-59'),
(5, '60-69'),
(6, '70+');

-- --------------------------------------------------------

--
-- Table structure for table `client_body_composition_info`
--

CREATE TABLE IF NOT EXISTS `client_body_composition_info` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `option_height` varchar(10) NOT NULL,
  `option_weight` varchar(10) NOT NULL,
  `option_height_measured` varchar(10) NOT NULL,
  `option_weight_measured` varchar(10) NOT NULL,
  `option_bmi` varchar(10) NOT NULL,
  `option_waist` varchar(10) NOT NULL,
  `option_hip` varchar(10) NOT NULL,
  `option_whr` varchar(10) NOT NULL,
  `option_triceps` varchar(10) NOT NULL,
  `option_biceps` varchar(10) NOT NULL,
  `option_subscapular` varchar(10) NOT NULL,
  `option_sos` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_body_composition_info`
--

INSERT INTO `client_body_composition_info` (`id`, `c_id`, `option_height`, `option_weight`, `option_height_measured`, `option_weight_measured`, `option_bmi`, `option_waist`, `option_hip`, `option_whr`, `option_triceps`, `option_biceps`, `option_subscapular`, `option_sos`) VALUES
(42, 76, '165', '73', '', '', '26.8', '', '', '0', '', '', '', '0'),
(43, 77, '187', '76', '187', '76', '21.7', '85', '99', '0.86', '7', '5', '9', '21'),
(44, 78, '', '', '199', '89', '22.5', '83', '', '0', '', '', '', '0'),
(45, 79, '193', '85', '', '', '22.8', '', '', '0', '', '', '', '0'),
(46, 80, '185', '96', '185', '99.5', '29.1', '103', '111', '0.93', '', '', '', '0'),
(47, 81, '177', '88', '', '', '28.1', '', '', '0', '', '', '', '0'),
(48, 82, '173', '67', '', '', '22.4', '', '', '0', '', '', '', '0'),
(49, 83, '180', '83', '', '', '25.6', '83', '102', '0.81', '7', '5', '9.5', '21.5'),
(50, 84, '', '', '', '', '0', '', '', '0', '', '', '', '0'),
(51, 86, '', '', '', '', '0', '', '', '0', '', '', '', '0'),
(52, 95, '', '', '', '', '0', '', '', '0', '', '', '', '0'),
(53, 96, '', '', '4', '2', '1250', '545', '', '0', '', '', '', '0'),
(54, 99, '', '', '', '', '0', '', '', '0', '', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `client_info`
--

CREATE TABLE IF NOT EXISTS `client_info` (
  `id` int(11) NOT NULL,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_desc` varchar(2000) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `occupation` varchar(200) NOT NULL,
  `risk_factor` varchar(50) DEFAULT NULL,
  `screening_decision_category` varchar(100) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_info`
--

INSERT INTO `client_info` (`id`, `first_name`, `last_name`, `email`, `p_name`, `p_desc`, `gender`, `dob`, `country`, `occupation`, `risk_factor`, `screening_decision_category`, `created_date`) VALUES
(76, 'Steph', 'G', 'stephanie@aep.net.au', '12', '', 'F', '18/11/1989', 'AU', 'Health care', '-1', 'Low', '2015-09-24 23:18:59'),
(77, 'Keith', 'Whitcher', 'keith@aep.net.au', '12', '', 'M', '01/10/1983', 'AU', 'Health care', '-2', 'Low', '2015-09-24 23:48:41'),
(78, 'Caelum', 'Schild', 'caelum@aep.net.au', '5', '', 'M', '08/1/1989', 'AU', 'Health care', '-1', 'Low', '2015-09-25 03:09:01'),
(79, 'Adrian', 'Barei', 'adrian@aep.net.au', '12', '', 'M', '03/10/1972', 'AU', 'Health care', '-1', 'HIGH', '2015-09-25 03:42:13'),
(80, 'Example', 'Example', 'as', '5', '', 'M', '13/2/1979', 'AU', 'Garden care', '4', 'Moderate', '2015-10-06 05:39:30'),
(81, 'Adam', 'Browne', 'adam@aep.net.au', '15', '', 'M', '27/12/1987', 'AU', 'Health care', '-1', 'Low', '2015-10-07 10:57:35'),
(82, 'Holly', 'Upton', 'holly@aep.net.eu', '15', '', 'F', '21/5/1976', 'AU', 'Health care', '-1', 'Low', '2015-10-14 02:40:12'),
(83, 'Kevin', 'Norton', 'k.norton@unisa.edu.au', '15', '', 'M', '20/3/1959', 'AU', 'Education', '0', 'Low', '2015-12-04 00:09:10'),
(84, 'ca', 'ca', 'ca', '15', '', 'M', '18/1/1994', 'BH', 'Garden care', '1', 'Low', '2015-12-07 04:39:24'),
(85, 'Chris', 'Ftinogiannis', 'chris@aep.net.au', '15', '', 'M', '15/3/1978', 'AU', 'Health care', NULL, NULL, '2016-01-27 03:16:44'),
(86, 'd', 'd', 'd', '15', '', 'M', '17/11/1992', 'AT', 'Garden care', '0', 'Low', '2016-01-27 03:19:18'),
(87, 'd', 'd', 'd', '15', '', 'M', '17/11/1992', 'AT', 'Garden care', NULL, NULL, '2016-01-27 04:22:16'),
(88, 'ko', 'h', 'ihi', '15', 'uhu', 'M', '17/1/1993', 'BS', 'Garden care', NULL, NULL, '2016-01-27 05:50:16'),
(89, 'jkhj', 'hjkh', 'ihi', '15', 'jhkjhk', 'M', '15/5/1995', 'AZ', 'Farmer/primary production', '1', 'HIGH', '2016-02-04 10:06:33'),
(90, 'nmbnm', 'ghh', 'ghj', '15', 'hggjhj', 'M', '17/4/1995', 'AU', 'Entertainer', '0', 'Low', '2016-02-04 10:18:32'),
(91, 'jhkhj', 'hjkh', 'ghj', '15', 'l;op', 'M', '17/3/1995', 'BS', 'Finance', '0', 'Low', '2016-02-04 10:22:55'),
(92, 'ghjghj', 'hjhj', 'ghjgh', '15', 'ghghk', 'M', '17/3/1997', 'AM', 'Engineering', '0', 'Low', '2016-02-04 10:42:56'),
(93, 'jkhj', 'hjkhj', 'hjkh', '15', 'hhk', 'M', '17/1/1994', 'BS', 'Farmer/primary production', NULL, NULL, '2016-02-05 03:48:31'),
(94, 'jkhjk', 'hjkhjk', 'hjkhjk', '15', 'jhkhjk', 'M', '18/3/1997', 'AM', 'Entertainer', NULL, NULL, '2016-02-05 05:00:12'),
(95, 'dg', 'dg', 'gdf', '15', 'dfgdfg', 'M', '18/2/1993', 'AT', 'Finance', '2', 'HIGH', '2016-02-05 10:59:16'),
(96, 'ewt', 'wetr', 'wet', '15', 'ewtwe', 'M', '18/2/1994', 'AW', 'Engineering', '2', 'HIGH', '2016-02-05 11:01:07'),
(97, 'jhkhj', 'hjkh', 'ghj', '15', ';klll;lkkl;', 'M', '14/2/1998', 'AU', 'Farmer/primary production', NULL, NULL, '2016-02-17 11:54:10'),
(98, 'hjkhjk', 'hjkhjk', 'hjk', '15', 'jkhjk', 'M', '16/3/1998', 'AT', 'Emergency services', NULL, NULL, '2016-02-19 05:07:53'),
(99, 'hrfhfg', 'fghfgh', 'ihi', '15', 'ghfghfgh', 'M', '03/2/2006', 'AU', 'General business', '0', 'Low', '2016-02-24 06:07:11');

-- --------------------------------------------------------

--
-- Table structure for table `client_medical_info`
--

CREATE TABLE IF NOT EXISTS `client_medical_info` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `notes` varchar(400) NOT NULL,
  `option_8` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=98 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_medical_info`
--

INSERT INTO `client_medical_info` (`id`, `c_id`, `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `option_6`, `option_7`, `notes`, `option_8`) VALUES
(76, 76, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(77, 77, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(78, 78, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(79, 79, 'N', 'N', 'N', 'N', 'N', 'Y', 'N', '', '0'),
(80, 80, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(81, 81, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(82, 82, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(83, 83, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(84, 84, 'N', 'N', 'N', 'N', 'N', 'N', 'N', 'foot, ankile', '0'),
(85, 85, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(86, 86, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(87, 87, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(88, 88, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', '0'),
(89, 89, 'N', 'N', 'Y', 'Y', 'N', 'Y', 'N', '', 'guide'),
(90, 90, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', 'proceed'),
(91, 91, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', 'proceed'),
(92, 92, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', 'proceed'),
(93, 94, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', 'proceed'),
(94, 95, 'N', 'Y', 'Y', 'Y', 'N', 'N', 'N', '', 'guide'),
(95, 96, 'Y', 'Y', 'Y', 'N', 'N', 'N', 'N', '', 'guide'),
(96, 97, 'N', 'N', 'Y', 'N', 'Y', 'N', 'N', '', 'guide'),
(97, 99, 'N', 'N', 'N', 'N', 'N', 'N', 'N', '', 'proceed');

-- --------------------------------------------------------

--
-- Table structure for table `client_medication_info`
--

CREATE TABLE IF NOT EXISTS `client_medication_info` (
  `id` int(11) NOT NULL,
  `c_id` int(11) DEFAULT NULL,
  `option_1` varchar(10) DEFAULT NULL,
  `medical_conditions` varchar(100) DEFAULT NULL,
  `medical_regular` varchar(100) DEFAULT NULL,
  `option_4` varchar(10) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `option_pregnant` varchar(10) DEFAULT NULL,
  `pt1` int(11) DEFAULT '0',
  `pt2` int(11) DEFAULT '0',
  `pt3` int(11) DEFAULT '0',
  `pt4` int(11) DEFAULT '0',
  `pt5` int(11) DEFAULT '0',
  `pt6` int(11) DEFAULT '0',
  `pt7` int(11) DEFAULT '0',
  `pt8` int(11) DEFAULT '0',
  `pt9` int(11) DEFAULT '0',
  `pt10` int(11) DEFAULT '0',
  `pt11` int(11) DEFAULT '0',
  `pt12` int(11) DEFAULT '0',
  `pt13` int(11) DEFAULT '0',
  `pt14` int(11) DEFAULT '0',
  `pt15` int(11) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_medication_info`
--

INSERT INTO `client_medication_info` (`id`, `c_id`, `option_1`, `medical_conditions`, `medical_regular`, `option_4`, `notes`, `option_pregnant`, `pt1`, `pt2`, `pt3`, `pt4`, `pt5`, `pt6`, `pt7`, `pt8`, `pt9`, `pt10`, `pt11`, `pt12`, `pt13`, `pt14`, `pt15`) VALUES
(53, 76, 'N', '', '', 'Y', '', 'N', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 1),
(54, 77, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(55, 78, 'N', '', '', 'Y', '', '0', 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0, 0, 0),
(56, 79, 'N', '', '', 'Y', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0),
(57, 80, 'N', 'arthritis or osteoporosis,', 'musculo-skeletal,', 'Y', '', '0', 0, 0, 0, 0, 0, 1, 0, 1, 0, 0, 0, 0, 0, 0, 0),
(58, 81, 'N', 'thyroid disease,', 'other,', 'Y', '', '0', 0, 0, 0, 0, 0, 1, 0, 0, 0, 1, 0, 0, 1, 0, 0),
(59, 82, 'N', '', '', 'N', '', 'N', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(60, 83, 'N', '', '', 'Y', '', '0', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0),
(61, 84, 'Y', 'arthritis or osteoporosis,', 'BLOOD PRESSURE-lowering medication,', 'Y', 'T-spine and r lumbar, occasional knee pain', '0', 0, 0, 0, 0, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(62, 85, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(63, 86, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(64, 87, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(65, 96, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0),
(66, 99, 'N', '', '', 'N', '', '0', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `client_physical_activity_info`
--

CREATE TABLE IF NOT EXISTS `client_physical_activity_info` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `option_8` varchar(10) NOT NULL,
  `option_9` varchar(10) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_physical_activity_info`
--

INSERT INTO `client_physical_activity_info` (`id`, `c_id`, `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `option_6`, `option_7`, `option_8`, `option_9`) VALUES
(45, 76, '10', '250', '5', '300', '1', '60', '15', '45', 'N'),
(46, 77, '7', '120', '5', '150', '2', '60', '1', '.5', 'N'),
(47, 78, '7', '210', '7', '145', '2', '40', '2', '1', 'N'),
(48, 79, '0', '0', '2', '240', '0', '0', '2', '1', 'N'),
(49, 80, '4', '100', '1', '10', '3', '10', '2', '2', 'N'),
(50, 81, '8', '400', '4', '120', '4', '90', '5', '4', 'N'),
(51, 82, '3', '120', '2', '60', '0', '0', '30', '8', 'Y'),
(52, 83, '3', '100', '5', '250', '', '', '4', '1', 'Y'),
(53, 84, '', '', '', '', '', '', '', '', 'N'),
(54, 86, '', '', '', '', '', '', '', '', 'Y'),
(55, 88, '', '', '', '', '', '', '', '', 'Y'),
(56, 89, '', '12', '', '13', '', '12', '3', '3', 'Y'),
(57, 94, '', '', '', '', '', '', '', '', 'Y'),
(58, 95, '', '', '', '', '', '', '', '', 'Y'),
(59, 96, '', '', '', '', '', '', '', '', 'Y'),
(60, 97, '', '', '', '', '', '', '', '', 'Y'),
(61, 99, '', '', '', '', '', '', '', '', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `client_risk_factor_info`
--

CREATE TABLE IF NOT EXISTS `client_risk_factor_info` (
  `id` int(11) NOT NULL,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `option_8` varchar(10) NOT NULL,
  `option_9` varchar(10) NOT NULL,
  `option_10` varchar(10) NOT NULL,
  `option_11` varchar(10) NOT NULL,
  `option_12` varchar(10) NOT NULL,
  `option_13` varchar(100) NOT NULL,
  `option_gender` varchar(10) NOT NULL,
  `option_age` varchar(100) NOT NULL,
  `option_smoke` varchar(100) NOT NULL,
  `option_smoke_6` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `client_risk_factor_info`
--

INSERT INTO `client_risk_factor_info` (`id`, `c_id`, `option_1`, `option_2`, `option_3`, `option_4`, `option_5`, `option_6`, `option_7`, `option_8`, `option_9`, `option_10`, `option_11`, `option_12`, `option_13`, `option_gender`, `option_age`, `option_smoke`, `option_smoke_6`) VALUES
(41, 76, 'N', 'N', 'N', 'N', 'N', 'N', '120', '80', '', '', '', '3.6', '', '0', '', '', ''),
(42, 77, 'N', 'N', 'N', 'N', 'N', 'N', '130', '85', '1.7', '2.3', '4', '4.6', '1.2', '0', '', '', ''),
(43, 78, 'N', 'N', 'N', 'N', 'N', 'N', '124', '76', '', '', '', '5.3', '', '0', '', '', ''),
(44, 79, 'N', 'N', '0', 'N', 'N', 'N', '', '', '', '', '', '', '', '0', '', '', ''),
(45, 80, 'N', 'Y', 'N', 'Y', 'N', 'N', '132', '84', '0.8', '4.3', '5.1', '5.3', '1.7', '0', '', '10', ''),
(46, 81, 'N', 'N', 'N', 'N', 'N', 'N', '', '', '', '', '', '', '', '0', '', '', ''),
(47, 82, 'N', 'N', 'N', 'N', 'N', 'N', '', '', '', '', '', '', '', '0', '', '', ''),
(48, 83, 'N', 'N', '0', '0', 'N', 'N', '', '', '', '', '', '', '', '0', '', '', ''),
(49, 84, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(50, 86, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(51, 85, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(52, 87, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(53, 89, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(54, 95, '0', 'Y', '0', 'Y', '0', '0', '', '', '', '', '', '', '', '0', '', '', ''),
(55, 99, '0', '0', '0', '0', '0', '0', '', '', '', '', '', '', '', '0', '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `code_info`
--

CREATE TABLE IF NOT EXISTS `code_info` (
  `id` int(11) NOT NULL,
  `code` varchar(500) NOT NULL,
  `validity` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `code_info`
--

INSERT INTO `code_info` (`id`, `code`, `validity`) VALUES
(13, 'FREE', '');

-- --------------------------------------------------------

--
-- Table structure for table `comparison_list`
--

CREATE TABLE IF NOT EXISTS `comparison_list` (
  `id` int(11) NOT NULL,
  `sport` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `comparison_list`
--

INSERT INTO `comparison_list` (`id`, `sport`) VALUES
(1, 'archery'),
(2, 'badminton'),
(3, 'baseball'),
(4, 'basketball'),
(5, 'bodybuilding'),
(6, 'boxing - general'),
(7, 'boxing - heavyweight'),
(8, 'canoe polo'),
(9, 'canoeing (Canadian)'),
(10, 'cricket'),
(11, 'cycling - mountain bike'),
(12, 'cycling - road'),
(13, 'cycling - track'),
(14, 'decathlon'),
(15, 'discus'),
(16, 'diving'),
(17, 'fencing'),
(18, 'golf'),
(19, 'gymnastics'),
(20, 'handball'),
(21, 'heptathlon'),
(22, 'high jump'),
(23, 'hockey (field)'),
(24, 'hockey (ice)'),
(25, 'hurdles'),
(26, 'javelin'),
(27, 'jockey'),
(28, 'judo'),
(29, 'karate'),
(30, 'kayak - general'),
(31, 'kayak - marathon'),
(32, 'kayak - slalom'),
(33, 'kayak- sprint'),
(34, 'lacrosse'),
(35, 'long jump'),
(36, 'netball'),
(37, 'orienteering'),
(38, 'powerlifting'),
(39, 'rockclimbing'),
(40, 'rollerskating'),
(41, 'rowing - heavyweight'),
(42, 'rowing - lightweight'),
(43, 'rugby League - backs'),
(44, 'rugby League - forwards'),
(45, 'rugby union'),
(46, 'running - distance'),
(47, 'running - middle distance'),
(48, 'running - sprint'),
(49, 'sailing'),
(50, 'shooting'),
(51, 'shot put'),
(52, 'skating - figure'),
(53, 'soccer'),
(54, 'softball'),
(55, 'squash'),
(56, 'sumo wrestling'),
(57, 'surfing'),
(58, 'swimming'),
(59, 'synchronised swimming'),
(60, 'table tennis'),
(61, 'tennis'),
(62, 'ten-pin bowling'),
(63, 'triathlon'),
(64, 'volleyball'),
(65, 'walking'),
(66, 'waterpolo'),
(67, 'weightlifting'),
(68, 'wrestling');

-- --------------------------------------------------------

--
-- Table structure for table `probability`
--

CREATE TABLE IF NOT EXISTS `probability` (
  `id` int(10) NOT NULL,
  `prob_range` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `probability`
--

INSERT INTO `probability` (`id`, `prob_range`, `value`) VALUES
(1, '1 in 5', '52'),
(2, '1 in 10', '56'),
(3, '1 in 20', '59'),
(4, '1 in 50', '63'),
(5, '1 in 100', '65'),
(6, '1 in 200', '68'),
(7, '1 in 500', '70'),
(8, '1 in 1,000', '72'),
(9, '1 in 2,500', '75'),
(10, '1 in 5,000', '76'),
(11, '1 in 10,000', '78'),
(12, '1 in 50,000', '81'),
(13, '1 in 100,000', '83'),
(14, '1 in 1,000,000', '87'),
(15, '1 in 10,000,000', '91');

-- --------------------------------------------------------

--
-- Table structure for table `project_info`
--

CREATE TABLE IF NOT EXISTS `project_info` (
  `id` int(11) NOT NULL,
  `p_name` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `project_info`
--

INSERT INTO `project_info` (`id`, `p_name`) VALUES
(15, 'AEP Health Group');

-- --------------------------------------------------------

--
-- Table structure for table `tests`
--

CREATE TABLE IF NOT EXISTS `tests` (
  `id` int(10) NOT NULL,
  `test` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tests`
--

INSERT INTO `tests` (`id`, `test`) VALUES
(1, 'Vertical jump[cm]'),
(2, 'Flight:Contact time'),
(3, 'Peak power[W]'),
(4, '30 s total work[kj]'),
(5, 'VO2max'),
(6, 'Shuttle');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `age`
--
ALTER TABLE `age`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_body_composition_info`
--
ALTER TABLE `client_body_composition_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_info`
--
ALTER TABLE `client_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_medical_info`
--
ALTER TABLE `client_medical_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_medication_info`
--
ALTER TABLE `client_medication_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_physical_activity_info`
--
ALTER TABLE `client_physical_activity_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `client_risk_factor_info`
--
ALTER TABLE `client_risk_factor_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `code_info`
--
ALTER TABLE `code_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `comparison_list`
--
ALTER TABLE `comparison_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `probability`
--
ALTER TABLE `probability`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `project_info`
--
ALTER TABLE `project_info`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tests`
--
ALTER TABLE `tests`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `age`
--
ALTER TABLE `age`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `client_body_composition_info`
--
ALTER TABLE `client_body_composition_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `client_info`
--
ALTER TABLE `client_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=100;
--
-- AUTO_INCREMENT for table `client_medical_info`
--
ALTER TABLE `client_medical_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=98;
--
-- AUTO_INCREMENT for table `client_medication_info`
--
ALTER TABLE `client_medication_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=67;
--
-- AUTO_INCREMENT for table `client_physical_activity_info`
--
ALTER TABLE `client_physical_activity_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=62;
--
-- AUTO_INCREMENT for table `client_risk_factor_info`
--
ALTER TABLE `client_risk_factor_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=56;
--
-- AUTO_INCREMENT for table `code_info`
--
ALTER TABLE `code_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `comparison_list`
--
ALTER TABLE `comparison_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=69;
--
-- AUTO_INCREMENT for table `probability`
--
ALTER TABLE `probability`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `project_info`
--
ALTER TABLE `project_info`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `tests`
--
ALTER TABLE `tests`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
