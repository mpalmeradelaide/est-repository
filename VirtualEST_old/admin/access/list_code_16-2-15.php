<?php
include('db.php');
$result = mysql_query("SELECT * FROM code_info");
?>
<html>
<head>
<title>Code List</title>
<link rel="stylesheet" href="./css/style.css" type="text/css" />
<script type="text/javascript" src="./js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.pager.js"></script>

<link rel="stylesheet" type="text/css" href="./styles.css" />
</head>
<body>
<form name="frmUser" method="post" action="">
<div align="center"  width="800px">
<div class="message"><?php if(isset($message)) { echo $message; } ?></div>
<div align="right" style="padding-bottom:5px;"><a href="add_code.php" class="link"><img alt='Add' title='Add' src='images/add.png' width='15px' height='15px'/> Add Code</a></div>
<div align="center" >
<table id="insured_list" class="tablesorter">
    <thead> 
<tr> 
    <th >Code</th>
    
    <th >#</th>
  </tr> 
</thead> 
<tbody> 
<?php
$i=0;
while($row = mysql_fetch_array($result)) {
if($i%2==0)
$classname="evenRow";
else
$classname="oddRow";
?>
<tr class="<?php if(isset($classname)) echo $classname;?>">
<td><?php echo $row["code"]; ?></td>


<td><a href="edit_code.php?p_id=<?php echo $row["id"]; ?>" class="link"><img alt='Edit' title='Edit' src='images/edit.png' width='15px' height='15px' hspace='10' /></a>  <a href="delete_code.php?p_id=<?php echo $row["id"]; ?>"  class="link"><img alt='Delete' title='Delete' src='images/delete.png' width='15px' height='15px'hspace='10' /></a></td>
</tr>
<?php
$i++;
}
?>
</tbody>
</table>
    <div id="pager" class="pager">
	<form>
		<img src="images/first.png" class="first"/>
		<img src="images/prev.png" class="prev"/>
		<input type="text" class="pagedisplay"/>
		<img src="images/next.png" class="next"/>
		<img src="images/last.png" class="last"/>
		<select class="pagesize">
			<option value="">LIMIT</option>
			<option value="2">2 per page</option>
			<option value="5">5 per page</option>
			<option value="10">10 per page</option>
			
		</select>
	</form>
</div>
<script defer="defer">
	$(document).ready(function() 
    { 
        $("#insured_list")
		.tablesorter({widthFixed: true, widgets: ['zebra']})
		.tablesorterPager({container: $("#pager")}); 
    } 
	); 
</script>
    
</div>
</form>
</div>
</body></html>