<?php
include('db.php');
$result = mysql_query("SELECT * FROM code_info");
?>
<html>
<head>
<title>Code List</title>
<link rel="stylesheet" href="./css/style.css" type="text/css" />
<script type="text/javascript" src="./js/jquery-1.3.1.min.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.js"></script>
<script type="text/javascript" src="./js/jquery.tablesorter.pager.js"></script>

<link rel="stylesheet" type="text/css" href="./styles.css" />
<style>
    
.btn_remove {
padding: 8px 18px;height: 18px;width: 31px;font-size: 12px;font-weight: bold;color: #527881;text-shadow: 0 1px #e3f1f1;
background: #cde5ef;border: 1px solid;border-color: #b4ccce #b3c0c8 #9eb9c2;border-radius: 16px;
outline: 0;-webkit-box-sizing: content-box;-moz-box-sizing: content-box;box-sizing: content-box;
background-image: -webkit-linear-gradient(top, #edf5f8, #cde5ef);
background-image: -moz-linear-gradient(top, #edf5f8, #cde5ef);
background-image: -o-linear-gradient(top, #edf5f8, #cde5ef);
background-image: linear-gradient(to bottom, #edf5f8, #cde5ef);
-webkit-box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
box-shadow: inset 0 1px white, 0 1px 2px rgba(0, 0, 0, 0.15);
}    
.btn_remove a{text-decoration: none;}
</style>
</head>
<body>
<form name="frmUser" method="post" action="">
<div align="center"  width="800px">
<div class="message"><?php if(isset($message)) { echo $message; } ?></div>
<div style="padding-bottom: 5px;margin-left: 0px;float:left" class="btn_remove"><a href="https://corporate.aep.net.au/healthscreen/admin/download.php" class="link"> Back</a></div>
<div style="padding-bottom:5px;width: 100px;float:right"><a href="add_code.php" class="link"><img alt='Add' title='Add' src='images/add.png' width='15px' height='15px'/> Add Code</a></div>
<div align="center" style="padding-top: 40px;">
<table id="insured_list" class="tablesorter">
    <thead> 
<tr> 
    <th >Code</th>
    
    <th >#</th>
  </tr> 
</thead> 
<tbody> 
<?php
$i=0;
while($row = mysql_fetch_array($result)) {
if($i%2==0)
$classname="evenRow";
else
$classname="oddRow";
?>
<tr class="<?php if(isset($classname)) echo $classname;?>">
<td><?php echo $row["code"]; ?></td>


<td><a href="edit_code.php?p_id=<?php echo $row["id"]; ?>" class="link"><img alt='Edit' title='Edit' src='images/edit.png' width='15px' height='15px' hspace='10' /></a>  <a href="delete_code.php?p_id=<?php echo $row["id"]; ?>"  class="link"><img alt='Delete' title='Delete' src='images/delete.png' width='15px' height='15px'hspace='10' /></a></td>
</tr>
<?php
$i++;
}
?>
</tbody>
</table>
    <div id="pager" class="pager">
	<form>
		<img src="images/first.png" class="first"/>
		<img src="images/prev.png" class="prev"/>
		<input type="text" class="pagedisplay"/>
		<img src="images/next.png" class="next"/>
		<img src="images/last.png" class="last"/>
		<select class="pagesize">
			<option value="">LIMIT</option>
			<option value="2">2 per page</option>
			<option value="5">5 per page</option>
			<option value="10">10 per page</option>
			
		</select>
	</form>
</div>
<script defer="defer">
	$(document).ready(function() 
    { 
        $("#insured_list")
		.tablesorter({widthFixed: true, widgets: ['zebra']})
		.tablesorterPager({container: $("#pager")}); 
    } 
	); 
</script>
    
</div>
</form>
</div>
</body></html>