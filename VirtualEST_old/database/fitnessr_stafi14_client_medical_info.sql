-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_medical_info`
--

DROP TABLE IF EXISTS `client_medical_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_medical_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `notes` varchar(400) NOT NULL,
  `option_8` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_medical_info`
--

LOCK TABLES `client_medical_info` WRITE;
/*!40000 ALTER TABLE `client_medical_info` DISABLE KEYS */;
INSERT INTO `client_medical_info` VALUES (76,76,'N','N','N','N','N','N','N','','0'),(77,77,'N','N','N','N','N','N','N','','0'),(78,78,'N','N','N','N','N','N','N','','0'),(79,79,'N','N','N','N','N','Y','N','','0'),(80,80,'N','N','N','N','N','N','N','','0'),(81,81,'N','N','N','N','N','N','N','','0'),(82,82,'N','N','N','N','N','N','N','','0'),(83,83,'N','N','N','N','N','N','N','','0'),(84,84,'N','N','N','N','N','N','N','foot, ankile','0'),(85,85,'N','N','N','N','N','N','N','','0'),(86,86,'N','N','N','N','N','N','N','','0'),(87,87,'N','N','N','N','N','N','N','','0'),(88,88,'N','N','N','N','N','N','N','','0');
/*!40000 ALTER TABLE `client_medical_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:57
