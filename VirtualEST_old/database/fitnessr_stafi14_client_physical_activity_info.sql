-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_physical_activity_info`
--

DROP TABLE IF EXISTS `client_physical_activity_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_physical_activity_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `option_8` varchar(10) NOT NULL,
  `option_9` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=56 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_physical_activity_info`
--

LOCK TABLES `client_physical_activity_info` WRITE;
/*!40000 ALTER TABLE `client_physical_activity_info` DISABLE KEYS */;
INSERT INTO `client_physical_activity_info` VALUES (45,76,'10','250','5','300','1','60','15','45','N'),(46,77,'7','120','5','150','2','60','1','.5','N'),(47,78,'7','210','7','145','2','40','2','1','N'),(48,79,'0','0','2','240','0','0','2','1','N'),(49,80,'4','100','1','10','3','10','2','2','N'),(50,81,'8','400','4','120','4','90','5','4','N'),(51,82,'3','120','2','60','0','0','30','8','Y'),(52,83,'3','100','5','250','','','4','1','Y'),(53,84,'','','','','','','','','N'),(54,86,'','','','','','','','','Y'),(55,88,'','','','','','','','','Y');
/*!40000 ALTER TABLE `client_physical_activity_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:50
