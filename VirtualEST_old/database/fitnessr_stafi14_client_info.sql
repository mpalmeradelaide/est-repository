-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_info`
--

DROP TABLE IF EXISTS `client_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(200) NOT NULL,
  `last_name` varchar(200) NOT NULL,
  `email` varchar(100) NOT NULL,
  `p_name` varchar(100) NOT NULL,
  `p_desc` varchar(2000) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `dob` varchar(100) DEFAULT NULL,
  `country` varchar(100) DEFAULT NULL,
  `occupation` varchar(200) NOT NULL,
  `risk_factor` varchar(50) DEFAULT NULL,
  `screening_decision_category` varchar(100) DEFAULT NULL,
  `created_date` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=89 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_info`
--

LOCK TABLES `client_info` WRITE;
/*!40000 ALTER TABLE `client_info` DISABLE KEYS */;
INSERT INTO `client_info` VALUES (76,'Steph','G','stephanie@aep.net.au','12','','F','18/11/1989','AU','Health care','-1','Low','2015-09-24 23:18:59'),(77,'Keith','Whitcher','keith@aep.net.au','12','','M','01/10/1983','AU','Health care','-2','Low','2015-09-24 23:48:41'),(78,'Caelum','Schild','caelum@aep.net.au','5','','M','08/1/1989','AU','Health care','-1','Low','2015-09-25 03:09:01'),(79,'Adrian','Barei','adrian@aep.net.au','12','','M','03/10/1972','AU','Health care','-1','HIGH','2015-09-25 03:42:13'),(80,'Example','Example','as','5','','M','13/2/1979','AU','Garden care','4','Moderate','2015-10-06 05:39:30'),(81,'Adam','Browne','adam@aep.net.au','15','','M','27/12/1987','AU','Health care','-1','Low','2015-10-07 10:57:35'),(82,'Holly','Upton','holly@aep.net.eu','15','','F','21/5/1976','AU','Health care','-1','Low','2015-10-14 02:40:12'),(83,'Kevin','Norton','k.norton@unisa.edu.au','15','','M','20/3/1959','AU','Education','0','Low','2015-12-04 00:09:10'),(84,'ca','ca','ca','15','','M','18/1/1994','BH','Garden care','1','Low','2015-12-07 04:39:24'),(85,'Chris','Ftinogiannis','chris@aep.net.au','15','','M','15/3/1978','AU','Health care',NULL,NULL,'2016-01-27 03:16:44'),(86,'d','d','d','15','','M','17/11/1992','AT','Garden care','0','Low','2016-01-27 03:19:18'),(87,'d','d','d','15','','M','17/11/1992','AT','Garden care',NULL,NULL,'2016-01-27 04:22:16'),(88,'ko','h','ihi','15','uhu','M','17/1/1993','BS','Garden care',NULL,NULL,'2016-01-27 05:50:16');
/*!40000 ALTER TABLE `client_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:52
