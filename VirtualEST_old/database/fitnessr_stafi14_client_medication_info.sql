-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_medication_info`
--

DROP TABLE IF EXISTS `client_medication_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_medication_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) DEFAULT NULL,
  `option_1` varchar(10) DEFAULT NULL,
  `medical_conditions` varchar(100) DEFAULT NULL,
  `medical_regular` varchar(100) DEFAULT NULL,
  `option_4` varchar(10) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `option_pregnant` varchar(10) DEFAULT NULL,
  `pt1` int(11) DEFAULT '0',
  `pt2` int(11) DEFAULT '0',
  `pt3` int(11) DEFAULT '0',
  `pt4` int(11) DEFAULT '0',
  `pt5` int(11) DEFAULT '0',
  `pt6` int(11) DEFAULT '0',
  `pt7` int(11) DEFAULT '0',
  `pt8` int(11) DEFAULT '0',
  `pt9` int(11) DEFAULT '0',
  `pt10` int(11) DEFAULT '0',
  `pt11` int(11) DEFAULT '0',
  `pt12` int(11) DEFAULT '0',
  `pt13` int(11) DEFAULT '0',
  `pt14` int(11) DEFAULT '0',
  `pt15` int(11) DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=65 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_medication_info`
--

LOCK TABLES `client_medication_info` WRITE;
/*!40000 ALTER TABLE `client_medication_info` DISABLE KEYS */;
INSERT INTO `client_medication_info` VALUES (53,76,'N','','','Y','','N',1,0,0,0,0,0,0,0,0,0,1,0,0,1,1),(54,77,'N','','','N','','0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(55,78,'N','','','Y','','0',0,0,0,0,0,1,0,0,0,0,1,0,0,0,0),(56,79,'N','','','Y','','0',0,0,0,0,0,0,0,0,0,0,0,0,1,0,0),(57,80,'N','arthritis or osteoporosis,','musculo-skeletal,','Y','','0',0,0,0,0,0,1,0,1,0,0,0,0,0,0,0),(58,81,'N','thyroid disease,','other,','Y','','0',0,0,0,0,0,1,0,0,0,1,0,0,1,0,0),(59,82,'N','','','N','','N',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(60,83,'N','','','Y','','0',1,0,0,0,0,0,0,0,0,0,1,0,0,1,0),(61,84,'Y','arthritis or osteoporosis,','BLOOD PRESSURE-lowering medication,','Y','T-spine and r lumbar, occasional knee pain','0',0,0,0,0,1,1,0,0,0,0,0,0,0,0,0),(62,85,'N','','','N','','0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(63,86,'N','','','N','','0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0),(64,87,'N','','','N','','0',0,0,0,0,0,0,0,0,0,0,0,0,0,0,0);
/*!40000 ALTER TABLE `client_medication_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:56
