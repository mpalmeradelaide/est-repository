<?php
class body_model extends CI_Model
{
    function __construct()
    {
        parent::__construct(); 
        session_start();
    }

	
	
	
  		//Fetch Age Range and Population 
  public function fetch_blood_norms(){
    $this->db->select('age');
    $this->db->group_by('age');
    $query=$this->db->get('blood_norms_all_ctry');
    $age_range=$query->result();   
    //Country
    $this->db->select('country');
    $this->db->group_by('country');
    $query_ctry=$this->db->get('blood_norms_all_ctry');
    $ctry=$query_ctry->result();
     
    $age_ctry=array('age_range'=>$age_range,'country'=>$ctry);
	return $age_ctry;
  }
		
	
    public function getnormdata($gender,$age,$population)
   {
	    $this->db->select("*");	     
		$this->db->from('blood_norms_all_ctry');
	    $this->db->where('age', $age);
		$this->db->where('gender', $gender);  
		$this->db->where('country',$population); 
		$query = $this->db->get();
		return $query->result();
			
   }
		

		  //savesomatotypeInfo Save
	public function savesomatotypeInfo()
    {
	  //[post_endomorph] => 5.72 [post_ectomorph] => 0.46 [post_mesomorph] => 5.95 ) 
	  
	  $c_id = $_SESSION['userid'];
      $code = array('c_id' =>$c_id,
	                'endomorph' =>$this->input->post('post_endomorph'),
					'ectomorph' =>$this->input->post('post_ectomorph'),
					'mesomorph' =>$this->input->post('post_mesomorph'),
					);
					
// code added DAYA
     $code_client = array('id' =>$c_id,
	                'endomorph_score' =>$this->input->post('post_endomorph'),
					'ectomorph_score' =>$this->input->post('post_ectomorph'),
					'mesomorph_score' =>$this->input->post('post_mesomorph'),
					);					
            $query = $this->db->query("SELECT id FROM client_person_info
                               WHERE id = '".$_SESSION['userid']."'");
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
                else
                {
                    $this->db->insert('client_person_info', $code_client); 
                }     
	


	
			$query = $this->db->query("SELECT c_id FROM somatotype_test
                               WHERE c_id = '".$_SESSION['userid']."'");
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('somatotype_test', $code); 
                }
                else
                {
                    return $this->db->insert('somatotype_test', $code); 
                }     
	}
	
	
	
		// DAYA
		  //savesomatotypeInfo Save
	public function savefractionationInfo()
    {
	  //[post_endomorph] => 5.72 [post_ectomorph] => 0.46 [post_mesomorph] => 5.95 ) 
	  
	  $c_id = $_SESSION['userid'];
      $code = array('c_id' =>$c_id,
	                'fat_mass' =>$this->input->post('fat_mass_post'),
					'muscle_mass' =>$this->input->post('muscle_mass_post'),
					'bone_mass' =>$this->input->post('bone_mass_post'),
					'residual_mass' =>$this->input->post('residual_mass_post'),
					);
					
					
					
// code added DAYA

     $code_client = array('id' =>$c_id,
	                'fat_mass' =>$this->input->post('fat_mass_post'),
					'muscle_mass' =>$this->input->post('muscle_mass_post'),
					'bone_mass' =>$this->input->post('bone_mass_post'),
					'residual_mass' =>$this->input->post('residual_mass_post'),
					);					
	

            $query = $this->db->query("SELECT id FROM client_person_info
                               WHERE id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
                else
                {
                    $this->db->insert('client_person_info', $code_client); 
                }     
	


	
			$query = $this->db->query("SELECT c_id FROM fraction_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('fraction_test', $code); 
                }
                else
                {
                    return $this->db->insert('fraction_test', $code); 
                }     
	}
	
	
	
	public function saveBodyFatInfo()
    {
	  //[post_endomorph] => 5.72 [post_ectomorph] => 0.46 [post_mesomorph] => 5.95 ) 
	  
	  $c_id = $_SESSION['userid'];
      $code = array('c_id' =>$c_id,
	                'meanbodyfat' =>$this->input->post('meanbodyfat'),
					'sdbodyfat' =>$this->input->post('sdbodyfat')
					); 
					
					
					
// code added DAYA

     $code_client = array('id' =>$c_id,
	                'mean_Body_Fat' =>$this->input->post('meanbodyfat'),
					'sd_Body_Fat' =>$this->input->post('sdbodyfat')					
					);					
	

            $query = $this->db->query("SELECT id FROM client_person_info
                               WHERE id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
                else
                {
                    $this->db->insert('client_person_info', $code_client); 
                }     
	
	
// Added END	

	
			$query = $this->db->query("SELECT c_id FROM percBodyFat_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('percBodyFat_test', $code); 
                }
                else
                {
                    return $this->db->insert('percBodyFat_test', $code); 
                }     
	}
	
	
	
	
	
	
	
	
	
   public function all_sports_male(){
    
    $this->db->select("sport");	     
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%m%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  
   public function all_sports_female(){
    
    $this->db->select("*");      
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%f%')"); 
	$this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
   
   public function fetch_anthropometetry($gender){
    $this->db->select("*");	     
    $this->db->from('sport_match');
    $this->db->where("( gender like '$gender%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  
  
  public function fetch_sportslist($gender){
    $this->db->select("sport,endomorph,mesomorph,ectomorph");	     
    $this->db->from('sport_match');
    $this->db->where("( gender like '$gender%')");
    $this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  }     
    
  
  
   public function all_ages(){
    
    $this->db->select("*");	     
    $this->db->from('age');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  
  public function all_tests(){
    
    $this->db->select("*");	     
    $this->db->from('tests');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  public function all_ranges(){
    
    $this->db->select("*");	     
    $this->db->from('probability');    
    $query = $this->db->get(); 
    return $query->result();
  }
  //Get Info Data 
  public function fetch_info_data($info_id){
   
      $this->db->select("*");
        $this->db->from('body_comp_info');
        $this->db->where('id', $info_id);
        $query = $this->db->get();
        return $query->result();
    }
  
  function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
  
public function saveRestrictedInfo()
 {
    //var_dump($this->session->all_userdata());         
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'height' => $this->input->post('height'),
              'body_mass' => $this->input->post('body_mass'),
              'triceps' => $this->input->post('triceps'),
              'subscapular' => $this->input->post('subscapular'),
              'biceps' => $this->input->post('biceps'),
              'iliac_crest' => $this->input->post('iliac_crest'),
              'supraspinale' => $this->input->post('supraspinale'),
              'abdominal' => $this->input->post('abdominal'),
              'thigh' => $this->input->post('thigh'),
              'calf' => $this->input->post('calf'),
              'mid_axilla' => $this->input->post('mid_axilla'),
              'relArmG' => $this->input->post('relArmG'),
              'flexArmG' => $this->input->post('flexArmG'),
              'waistG' => $this->input->post('waistG'),
              'hipG' => $this->input->post('hipG'),
              'calfG' => $this->input->post('calfG'),
              'humerus' => $this->input->post('humerus'),
              'femur' => $this->input->post('femur')
              );
			  
			  
			  // CODE ADDED DAYA 
			$code_client = array(
              //'c_id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'illiacrest' => $this->input->post('iliac_crest'),
              'supraspinale' => $this->input->post('supraspinale'),
              'abdominal' => $this->input->post('abdominal'),
              'frontthigh' => $this->input->post('thigh'),
              'Medialcalf' => $this->input->post('calf'),
              'midaxilla' => $this->input->post('mid_axilla'),
              'armrelaxed' => $this->input->post('relArmG'),
              'armflexed' => $this->input->post('flexArmG'),
              'waistminimum' => $this->input->post('waistG'),
              'Gluteal_hips' => $this->input->post('hipG'),
              'calfmaximum' => $this->input->post('calfG'),
              'humerus' => $this->input->post('humerus'),
              'femur' => $this->input->post('femur')
              );  
			
			$query = $this->db->query("SELECT id FROM client_person_info
                               WHERE id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
                else
                {
                    $this->db->insert('client_person_info', $code_client); 
                }    
			  
			  
			  
			  
			  
			  
			  
			  
               
            $query = $this->db->query("SELECT c_id FROM restricted_profile
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('restricted_profile', $code); 
                }
                else
                {
                    return $this->db->insert('restricted_profile', $code); 
                }               
          
      }
      

public function saveFullInfo()
 {
    //var_dump($this->session->all_userdata());         
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'height' => $this->input->post('height'),
              'body_mass' => $this->input->post('body_mass'),
              'triceps' => $this->input->post('triceps'),
              'subscapular' => $this->input->post('subscapular'),
              'biceps' => $this->input->post('biceps'),
              'iliac_crest' => $this->input->post('iliac_crest'),
              'supraspinale' => $this->input->post('supraspinale'),
              'abdominal' => $this->input->post('abdominal'),
              'thigh' => $this->input->post('thigh'),
              'calf' => $this->input->post('calf'),
              'mid_axilla' => $this->input->post('mid_axilla'),              
              'headG' => $this->input->post('headG'),
              'neckG' => $this->input->post('neckG'),
              'relArmG' => $this->input->post('relArmG'),
              'flexArmG' => $this->input->post('flexArmG'),
              'forearmG' => $this->input->post('forearmG'),
              'wristG' => $this->input->post('wristG'),
              'chestG' => $this->input->post('chestG'),
              'waistG' => $this->input->post('waistG'),
              'hipG' => $this->input->post('hipG'),
              'thighG' => $this->input->post('thighG'),
              'midThighG' => $this->input->post('midThighG'),
              'calfG' => $this->input->post('calfG'),              
              'ankleG' => $this->input->post('ankleG'),
              'acRad' => $this->input->post('acRad'),
              'radStyl' => $this->input->post('radStyl'),
              'midStylDact' => $this->input->post('midStylDact'),
              'iliospinale' => $this->input->post('iliospinale'),
              'troch' => $this->input->post('troch'),
              'trochTib' => $this->input->post('trochTib'),
              'tibLat' => $this->input->post('tibLat'),
              'tibMed' => $this->input->post('tibMed'),              
              'biac' => $this->input->post('biac'),
              'bideltoid' => $this->input->post('bideltoid'),
              'billio' => $this->input->post('billio'),
              'bitrochanteric' => $this->input->post('bitrochanteric'),
              'foot' => $this->input->post('foot'),
              'sitting' => $this->input->post('sitting'),
              'trChest' => $this->input->post('trChest'),
              'apChest' => $this->input->post('apChest'),
              'armSpan' => $this->input->post('armSpan'),       
              'humerus' => $this->input->post('humerus'),
              'femur' => $this->input->post('femur')
              );
               
			   
			  //Code Added DAYA
            $code_client = array(
              //'c_id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'head' => $this->input->post('headG'),
              'neck' => $this->input->post('neckG'),
              'forearmmax' => $this->input->post('forearmG'),
              'waist_distal_styloids' => $this->input->post('wristG'),
              'chest_mesosternale' => $this->input->post('chestG'),
             /*  'waistG' => $this->input->post('waistG'),
              'hipG' => $this->input->post('hipG'), */
              'thigh_onecmdistal' => $this->input->post('thighG'),
              'thigh_mid' => $this->input->post('midThighG'),
              //'calfG' => $this->input->post('calfG'),              
              'ankle_minimum' => $this->input->post('ankleG'),
              'acromiale_radiale' => $this->input->post('acRad'),
              'radiale_styl' => $this->input->post('radStyl'),
              'midstyl_dacty' => $this->input->post('midStylDact'),
              'illiospinale' => $this->input->post('iliospinale'),
              'trochanterion' => $this->input->post('troch'),
              'trochant_tibial' => $this->input->post('trochTib'),
              'tibiale_laterale' => $this->input->post('tibLat'),
              'tib_med_sphytib' => $this->input->post('tibMed'),              
              'biacromial' => $this->input->post('biac'),
              'bideltoid' => $this->input->post('bideltoid'),
              'biiliocristal' => $this->input->post('billio'),
              'bitrochanteric' => $this->input->post('bitrochanteric'),
              'footlength' => $this->input->post('foot'),
              'sitting_length' => $this->input->post('sitting'),
              'transverse_chest' => $this->input->post('trChest'),
              'A_PChest' => $this->input->post('apChest'),
              'arm_span' => $this->input->post('armSpan')
              
              );			
			  
			  
			  
			 $query = $this->db->query("SELECT id FROM client_person_info
                               WHERE id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
                else
                {
                    $this->db->insert('client_person_info', $code_client); 
                }    
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
			   
            $query = $this->db->query("SELECT c_id FROM full_profile
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('full_profile', $code); 
                }
                else
                {
                    return $this->db->insert('full_profile', $code); 
                }               
          
      }
      
      
  public function saveErrorConfidenceInfo()
  {
    //var_dump($this->session->all_userdata());         
          
          $code = array(             
              'c_id' => $_SESSION['userid'],              
              'single_raw' => $this->input->post('single_raw'),
              'tem_percent' => $this->input->post('tem_percent'),
              'interval' => $this->input->post('interval')
              );
                            
            $query = $this->db->query("SELECT c_id FROM error_confidence
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('error_confidence', $code); 
                }
                else
                {
                    return $this->db->insert('error_confidence', $code); 
                }               
          
      }
      
  public function savetErrorRealChangeInfo()
  {
    //var_dump($this->session->all_userdata());         
          
          $code = array(             
              'c_id' => $_SESSION['userid'],              
              'first_score' => $this->input->post('first_score'),
              'second_score' => $this->input->post('second_score'),
              'tem_percent' => $this->input->post('tem_percent'),
              'interval' => $this->input->post('interval')
              );
                            
            $query = $this->db->query("SELECT c_id FROM error_real_change
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('error_real_change', $code); 
                }
                else
                {
                    return $this->db->insert('error_real_change', $code); 
                }               
          
      }      
  
}


?>