<?php
class client_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          session_start();
    }
           
    public function save()
      {
        $session_dob=$_SESSION['user_dob'];
		$session_fname=$_SESSION['user_first_name'];
		$session_lname=$_SESSION['user_last_name'];
		$user_fname=$this->input->post('first_name');
		$last_name=$this->input->post('last_name');
		 $dob = $this->input->post('daydropdown')."/".$this->input->post('monthdropdown')."/".$this->input->post('yeardropdown');
		//Calculate Age and SET vp_age to session.
		$date = str_replace('/', '-',$dob);                      
	    $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
	    $dobnew = strtotime( $dateOfBirth);						 
	    $tdate = strtotime(date('Y-m-d')) ;			
	    $_SESSION['vp_age'] = $this->getAge($dobnew, $tdate) ;
		
		
		//Changes done on 26-02-2018
		$vp_age=$_SESSION['vp_age'];
		$subpopulation=$this->input->post('subpopulation');
		$access_code=$this->input->post('code');
		//end of code
		
		$code = array('first_name' => $this->input->post('first_name'),'last_name' => $this->input->post('last_name'),'email' => $this->input->post('email') ,'p_name' => $this->input->post('p_name'),'p_desc' => $this->input->post('p_desc'),'gender' => $this->input->post('gender'),'dob' => $dob,'country' => $this->input->post('country'),'occupation' => $this->input->post('occupation'),'random_mass'=>$this->input->post('MASS'),'random_bmi'=>$this->input->post('BMI'),'height'=>$this->input->post('HEIGHT'),'vp_age'=>$vp_age,'subpopulation'=>$subpopulation,'code'=>$access_code);
		$is_update=0;
		if($session_dob==$dob && $session_fname==$user_fname && $last_name==$session_lname)
		{
		$is_update=1;	
		}
		
		if($this->isExist( 'client_info',  $_SESSION['userid']) && $is_update==1){
			        $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_info', $code); 
					 $uid=$_SESSION['userid'];
					 //Code Client Update
					$code_client = array('id'=>$uid,'firstname' => $this->input->post('first_name'),'lastname' => $this->input->post('last_name'),'Gender' => $this->input->post('gender'),'dob' => $dob,'project'=>$this->input->post('projectname'),'access_code'=>$access_code,'country' => $this->input->post('country'),'occupation' => $this->input->post('occupation'),'Age'=>$vp_age,'subpopulation'=>$subpopulation);
					 $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_person_info', $code_client); 
					 
                     return $_SESSION['userid'];
                }else{
					  $this->db->insert('client_info', $code); 
					  $uid=$this->db->insert_id();
					//Added for client_person_info	
					  $code_client = array('id'=>$uid,'firstname' => $this->input->post('first_name'),'lastname' => $this->input->post('last_name'),'Gender' => $this->input->post('gender'),'dob' => $dob,'project'=>$this->input->post('projectname'),'access_code'=>$access_code,'country' => $this->input->post('country'),'occupation' => $this->input->post('occupation'),'Age'=>$vp_age,'subpopulation'=>$subpopulation);
					  $this->db->insert('client_person_info',$code_client); 
					 
                     return  $uid;
      }
      }
	  
	  
 	// For getiing Athlete Age 
/*   	public function get_randomage()
	 {
		    $today_date=date('Y-m-d');
			$start = strtotime("$today_date -40 year");
			$end= strtotime("$today_date -18 year");
			$int= mt_rand($start,$end);
			$between_date=date('Y-m-d',$int);
			$get_date_values=explode("-",$between_date)	;
			$valueyears=$get_date_values[0];
			$valuemonth=$get_date_values[1];
			$valuedate=$get_date_values[2];
			$dateofBirth = $valueyears.'/'.$valuemonth.'/'.$valuedate;
			return $dateofBirth;
			
			/* $datebrth= date("Y-m-d",strtotime($dateofBirth));
			$dob = strtotime($datebrth);
		    $tdate = strtotime(date('Y-m-d'));
		    $vp_age=$this->getAge($dob, $tdate);
		    $agerange=$this->getAgeRange($vp_age); 
	 } 	 */
	
	  
	  
	  
	  
	  
	  
	  
	  
	  
     
  //Register New Virtual Person
  function register_VP()
  {
      $dob = $_SESSION['user_dob'];
      $code = array('first_name' =>$_SESSION['user_first_name'],'last_name' =>$_SESSION['user_last_name'],'email' =>'' ,'p_name' =>'','p_desc' =>'','gender' =>$_SESSION['user_gender'],'dob' => $dob,'country' =>$_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'random_mass'=>$_SESSION['MASS'],'random_bmi'=>$_SESSION['BMI'],'height'=>$_SESSION['HEIGHT']);
		
            if($this->isExist( 'client_info',  $_SESSION['userid'] )){
		$this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_info', $code); 
					 
					  $uid=$_SESSION['userid'];
					 $vp_age=$_SESSION['vp_age'];
					//Code Client Update
					$code_client = array('id'=>$uid,'firstname' => $_SESSION['user_first_name'],'lastname' =>$_SESSION['user_last_name'],'Gender' => $_SESSION['user_gender'],'dob' => $dob,'project'=>'','access_code'=>'','country' => $_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'Age'=>$vp_age,'subpopulation'=>$_SESSION['subpopulation'],'Mass'=>round($_SESSION['MASS'],1),'bmi'=>round($_SESSION['BMI'],1),'Height'=>round($_SESSION['HEIGHT'],1));
					 $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_person_info', $code_client); 
					 
                     return $_SESSION['userid'];
                }else{
					
                      $this->db->insert('client_info', $code); 
					  $uid=$this->db->insert_id();
					  //Added for client_person_info	
					  $code_client = array('id'=>$uid,'firstname' => $_SESSION['user_first_name'],'lastname' =>$_SESSION['user_last_name'],'Gender' => $_SESSION['user_gender'],'dob' => $dob,'project'=>'','access_code'=>'','country' => $_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'Age'=>$vp_age,'subpopulation'=>$_SESSION['subpopulation'],'Mass'=>round($_SESSION['MASS'],1),'bmi'=>round($_SESSION['BMI'],1),'Height'=>round($_SESSION['HEIGHT'],1));
					  $this->db->insert('client_person_info',$code_client); 
					 
					  return $uid; 
                  }
  }	 
	  public function cvdRiskInfo($years="")
      {
       $user_id=$_SESSION['userid'];   
       $gender=$_SESSION['user_gender']; 
       $vp_age=$_SESSION['vp_age'];
       $years = 5;      
     
        $cvdqry="select rfi.option_1 as heart_disease,rfi.option_2 as smoking , rfi.option_11 as colestrol,rfi.option_9 as hdl,rfi.option_10 as ldl,rfi.option_13 as triglycerides,rfi.option_12 as hbg , rfi.option_3 as smoking2 ,rfi.option_5 as high_colestrol ,rfi.option_7 AS SBP, mi.option_1 as ecgstroke, medi.medical_conditions as diab ,bci.option_waist as waist ,(rfi.option_11/rfi.option_9) as hdlratio
        
        from client_risk_factor_info AS rfi 
        
        left JOIN client_medical_info mi ON mi.c_id = rfi.c_id 
        left JOIN client_medication_info medi ON medi.c_id = rfi.c_id
        left JOIN client_body_composition_info bci ON bci.c_id=rfi.c_id
        where rfi.c_id = $user_id ";
    
        $query = $this->db->query($cvdqry);
        $row = $query->row_array();
          
		if(empty($row))
		{
			$diabetes=0; 
            $ecgstroke=0;  
            $smoking=0;   
             
            $colestrol=5;
            $HDL=1.2;
			$SBP=120;
			$hdlratio=round(($colestrol/$HDL),1);
			
            
			    $a=11.1122-0.9119*log($SBP)-0.2767*$smoking - 0.7181*log($hdlratio)-0.5865 * $ecgstroke;
		        $a=round($a,9);
				if($gender =='M' || $gender=='Male')
				{
					$m = $a - 1.4792*log($vp_age)- 0.1759 * $diabetes;
					$m=round($m,9);
				}
				if($gender =='F' || $gender=='Female') 
				{
				   //  $m = $a - 5.8549 + 1.8549 + 1.8515 * (log($vp_age/74))- 0.3758 * $diabetes;  // Original
					$m = $a - 5.8549 + 1.8515 * pow(log($vp_age/74),2)- 0.3758 * $diabetes;  
					$m=round($m,9);
				}
				//Step 3	μ = 4.4181+m  
				$mu=4.4181 + $m;
				$mu=round($mu,9);
				//Step 4	σ = EXP(-0.3155-0.2784*m) 
				$sigma= exp(-0.3155-0.2784*$m);
				$sigma=round($sigma,9);
			   // Step 5	υ = (ln(t) - μ)/σ    where t= years for risk probability and is typically 5 or 10 years  
				$u = (log($years) - $mu)/ $sigma;
				$u=round($u,9);
			   //Step 6	ρ = 1-EXP(-EXP(υ))      
				$p = 1 - exp(-exp($u)) ;  
				$p=round($p,6);
				//probability % = ρ * 100
				$probper=$p*100;
				$_SESSION['probper']=$probper;  
				//echo $_SESSION['probper'];
			  //  return $row;
			    
		}		
        else
		{			
        $_SESSION['years']=$years;
       
        // Calculate Smoking 
		if($row['smoking'] == "" )
		{
		  $smoke='N';	
		}else{
        $smoke=$row['smoking'];
		}
        if($smoke == 'Y')
        {
            $smoking=1;
        }else{
            $smoking=0;
        }
        $_SESSION['smoking']=$smoke;  // Y N
        $_SESSION['smokingval']=$smoking;  // 1 0
		
		
		
          
        $heart_disease=$row['heart_disease'];
        $_SESSION['heart_disease']=$row['heart_disease'];
        
        
		
		// Colestol Start
        if($row['colestrol'] == "")
		{ $colestrol=5;
    	}
		else{
			$colestrol=$row['colestrol'];
		}		       
        $_SESSION['colestrol']=$colestrol;
		// Colestrol End
		
		
      
         // hdl calculation start 
		if($row['hdl'] == "")
		{ $hdl=1.2;
		}
		else{
          $hdl=$row['hdl'];  
		}
        $_SESSION['hdl']=$hdl;                         
        // hdl end

		
        
        // Calculate Smoking 
		if($row['smoking2'] == "")
		{
			$smoke2='N';
		}else{
         $smoke2=$row['smoking2'];         
		}
          if($smoke2  == 'Y')
        {
            $smoking2=1;
        }else{
            $smoking2=0;
        }
        $_SESSION['smoking2']=$smoke2  ;
        $_SESSION['smoking2val']=$smoking2;
		
		
		
		
        
        // ecg Stroke 
        if($row['ecgstroke'] == "")
		{
		  $ecg='N';
		}
        else
	    {
          $ecg=$row['ecgstroke'];
		}		
        $_SESSION['ecgstroke']= $ecg ; // For 'y'|| 'n'
        if($ecg=='Y')
             { $ecgstroke=1;    
             }   
             else
             { $ecgstroke=0;       
             } 
        $_SESSION['ecgstrokeval']=$ecgstroke  ;  // For '1 || '0'
        // ecg end
		
		
		
          
        // For diabetes
        $diabetes=$row['diab']  ;           
        $_SESSION['diabetes']=$row['diab']  ;
          
        $arr = explode(",",$row['diab']);
        //print_r($arr);  
        
        $diabetes='N';  
        for($i=0;$i<sizeof($arr);$i++)
        {
            if(strtolower($arr[$i]) == "diabetes")
            {  $diabetes='Y';
            }
        } 
        if($diabetes == 'Y')
        {   $diabetes='1';  }
        else
        {   $diabetes='0';  }
        $_SESSION['diabetesval']=$diabetes  ; 
         
		 
		 
       // SBP Start    
		if($row['SBP'] == "")
		{ $SBP=120; }
		else{		
		$SBP=$row['SBP']  ;
		}
	    if($SBP!="")
        { 
        $_SESSION['SBP']=round($SBP,0)  ;
        }
		// SBP END
       
         // hdl ratio
	    if($row['hdlratio'] == "")
		{ $hdlratio=($colestrol/$hdl);
		}
		else{   
            $hdlratio=$row['hdlratio'];
		}
		
        if($hdlratio!="")
        {
          $_SESSION['hdlratio']=$hdlratio;
        }
		// hdlend
		
		
		
		
        //Calculations Start
        //a = 11.1122-0.9119*ln(SBP)-0.2767*smoking - 0.7181*ln(cholesterol/HDL ratio)-0.5865 * ECG LVH  
         if($SBP>0 && isset($hdlratio))
        { 
		 $a=11.1122-0.9119*log($SBP)-0.2767*$smoking - 0.7181*log($hdlratio)-0.5865 * $ecgstroke;
         $a=round($a,9);
         //echo $a;  //5.1602658
          
        /*Step 2	IF GENDER = MALE:
        m = a-1.4792*ln(age)-0.1759*diabetes

        IF GENDER = FEMALE:
        m = a-5.8549 + 1.8549 + 1.8515 * (ln(age/74)) -3.758*diabetes 
        */
         
        if($gender =='M' || $gender=='Male')
        {
            $m = $a - 1.4792*log($vp_age)- 0.1759 * $diabetes;
            $m=round($m,9);
        }
        if($gender =='F' || $gender=='Female') 
        {
           //  $m = $a - 5.8549 + 1.8549 + 1.8515 * (log($vp_age/74))- 0.3758 * $diabetes;  // Original
            $m = $a - 5.8549 + 1.8515 * pow(log($vp_age/74),2)- 0.3758 * $diabetes;  
            $m=round($m,9);
        }
        //echo $m; //-0.9266
          

        //Step 3	μ = 4.4181+m  
        $mu=4.4181 + $m;
        $mu=round($mu,9);
        //echo $mu; //3.491492483 
          
        //Step 4	σ = EXP(-0.3155-0.2784*m) 
        $sigma= exp(-0.3155-0.2784*$m);
        $sigma=round($sigma,9);
          
       // Step 5	υ = (ln(t) - μ)/σ    where t= years for risk probability and is typically 5 or 10 years  
        $u = (log($years) - $mu)/ $sigma;
        $u=round($u,9);
          
       //Step 6	ρ = 1-EXP(-EXP(υ))      
        $p = 1 - exp(-exp($u)) ;  
        $p=round($p,6);
          
        //probability % = ρ * 100
        $probper=$p*100;
		//echo $probper;
		
        $_SESSION['probper']=$probper;  
      
	  return $row;
		}
     }	
   }
	  
	  
	  
	  
	  
	  
   public function saveMedicalInfo()
      {
        $code = array(
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'notes' => $this->input->post('notes'),
              'option_8' => $this->input->post('option_8'));
			  
			  
			  
			 	// added another table DAYA  
		$code_client = array(
              'id' => $_SESSION['userid'],
              'heart_condition' => $this->input->post('options_1'),
              'pain_in_chaist' => $this->input->post('options_2'),
              'feel_faint' => $this->input->post('options_3'),
              'asthma_attack' => $this->input->post('options_4'),
              'diabetes' => $this->input->post('options_5'),
              'bone_muscle_prob' => $this->input->post('options_6'),
              'other_medical_cond' => $this->input->post('options_7'));	  
			  
			   if($this->isExist( 'client_person_info',  $_SESSION['userid'] )){
                    $this->db->where('id', $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{
                    $this->db->insert('client_person_info', $code_client); 
                } 
			   
			  
			  
               if($this->isExist( 'client_medical_info',  $_SESSION['userid'] )){
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medical_info', $code); 
                }else{

                    return $this->db->insert('client_medical_info', $code); 
                }               
          
      }
      
      
       public function savePhysicalActivityInfo()
      {
         $code = array(
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'));
			  
			  
			 // ADDEd table DAYA
		 $code_client = array(
              'id' => $_SESSION['userid'],
              'walking' => $this->input->post('options_1'),
              'minutes_walking' => $this->input->post('options_2'),
              'vigorous' => $this->input->post('options_3'),
              'minute_vigorous' => $this->input->post('options_4'),
              'moderate' => $this->input->post('options_5'),
              'minute_moderate' => $this->input->post('options_6'),
              'tv_hours_games' => $this->input->post('options_7'),
              'driving_hours' => $this->input->post('options_8'),
              'is_setting_long_period' => $this->input->post('options_9'));  
		  
           //if($this->isExist( 'client_physical_activity_info',  $this->session->userdata('userid') )){
			   
			   
			 if($this->isExist( 'client_person_info', $_SESSION['userid'] )){
                    $this->db->where('id', $_SESSION['userid']);
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }      
			  
			  
        
          
           //if($this->isExist( 'client_physical_activity_info',  $this->session->userdata('userid') )){
           if($this->isExist( 'client_physical_activity_info', $_SESSION['userid'] )){
                    $this->db->where('c_id', $_SESSION['userid']);
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    return  $this->db->update('client_physical_activity_info', $code); 
                }else{

                    return  $this->db->insert('client_physical_activity_info', $code); 
                }                    
         
        }
      
      public function saveRiskFactorInfo()
      {
           
        
          $code = array(
            //  'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('options_1'),
              'option_2' => $this->input->post('options_2'),
              'option_3' => $this->input->post('options_3'),
              'option_4' => $this->input->post('options_4'),
              'option_5' => $this->input->post('options_5'),
              'option_6' => $this->input->post('options_6'),
              'option_7' => $this->input->post('options_7'),
              'option_8' => $this->input->post('options_8'),
              'option_9' => $this->input->post('options_9'),
              'option_10' => $this->input->post('options_10'),
              'option_11' => $this->input->post('options_11'),
              'option_12' => $this->input->post('options_12'),
              'option_13' => $this->input->post('options_13'),
              'option_gender' => $this->input->post('option_gender'),
              'option_age' => $this->input->post('option_age'),
              'option_smoke' => $this->input->post('option_smoke'),
              'option_smoke_6' => $this->input->post('option_smoke_6'));
            
          
		    	  
			  // CODE ADDED DAYA
			$code_client = array(
            //  'c_id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'family_history' => $this->input->post('options_1'),
              'current_smoker' => $this->input->post('options_2'),
              'quit_smoke_recently' => $this->input->post('options_3'),
              'self_report_BP' => $this->input->post('options_4'),
              'self_report_Chol' => $this->input->post('options_5'),
              'self_report_Glucose' => $this->input->post('options_6'),
              'Measured_SBP' => $this->input->post('options_7'),
              'Measured_DBP' => $this->input->post('options_8'),
              'HDL' => $this->input->post('options_9'),
              'LDL' => $this->input->post('options_10'),
              'Measured_blood_Cholesterol' => $this->input->post('options_11'),
              'Measured_fast_blood_glucose' => $this->input->post('options_12'),
              'triglycerides' => $this->input->post('options_13'),
              'family_gender' => $this->input->post('option_gender'),
              'age_at_heart_attack' => $this->input->post('option_age'),
              'how_many' => $this->input->post('option_smoke'),
              'how_many_recently' => $this->input->post('option_smoke_6'));  
			  
            
			if($this->isExist( 'client_person_info',  $_SESSION['userid'] )){
            //if($this->isExist( 'client_risk_factor_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('id',$_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{
					$this->db->insert('client_person_info', $code_client); 
                } 
			
		  
		  
		  
		  
		  
		  
		  
		  
            if($this->isExist( 'client_risk_factor_info',  $_SESSION['userid'] )){
            //if($this->isExist( 'client_risk_factor_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id',$_SESSION['userid']);
                    return  $this->db->update('client_risk_factor_info', $code); 
                }else{

                    return  $this->db->insert('client_risk_factor_info', $code); 
                }   
          
          //return  $this->db->insert('client_risk_factor_info', $code); 
      }
      
// kritika 
    
       public function saveBodyCompositionInfo()
      {          
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_height' => $this->input->post('options_height'),
              'option_weight' => $this->input->post('options_weight'),
              'option_height_measured' => $this->input->post('options_height_measured'),
              'option_weight_measured' => $this->input->post('options_weight_measured'),
              'option_bmi' => round($this->input->post('bmi'), 1),
              'option_waist' => $this->input->post('waist'),
              'option_hip' => $this->input->post('hip'),
              'option_whr' => round($this->input->post('whr'), 2),
              'option_triceps' => $this->input->post('triceps'),
              'option_biceps' => $this->input->post('biceps'),
              'option_subscapular' => $this->input->post('subscapular'),
              'option_sos' => round($this->input->post('sos'), 1));
          
                // CODE ADDED DAYA
			  $code_client = array(
              //'c_id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
			  'self_report_height' => $this->input->post('options_height'),
              'self_report_weight' => $this->input->post('options_weight'),
              'Height' => $this->input->post('options_height_measured'),
              'Mass' => $this->input->post('options_weight_measured'),
              'bmi' => round($this->input->post('bmi'), 1),
              //'option_waist' => $this->input->post('waist'),
              'hip' => $this->input->post('hip'),
              'waist_hip_ratio' => round($this->input->post('whr'), 2),
              'triceps' => $this->input->post('triceps'),
              'biceps' => $this->input->post('biceps'),
              'subscapular' => $this->input->post('subscapular'),
              'Sum_of_3_skinfolds' => round($this->input->post('sos'), 1));
						  
			  
			  if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
           //  if($this->isExist( 'client_body_composition_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }
				
				
				
				
				
				
				
          
             if($this->isExist( 'client_body_composition_info',  $_SESSION['userid'])){
           //  if($this->isExist( 'client_body_composition_info',  $this->session->userdata('userid') )){
                  //  $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id',  $_SESSION['userid']);
                    return  $this->db->update('client_body_composition_info', $code); 
                }else{

                    return  $this->db->insert('client_body_composition_info', $code); 
                } 
                        
         // return  $this->db->insert('client_body_composition_info', $code); 
      }
// kritika      
      
       public function saveMeditationInfo()
      {
           
        
          $code = array(
             // 'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'option_1' => $this->input->post('option_1'),
              'medical_conditions' => $this->input->post('medicalConditions'),
              'medical_regular' => $this->input->post('medicalRegular'),
              'option_4' => $this->input->post('option_4'),
              'notes' => $this->input->post('notes'),
              'option_pregnant' => $this->input->post('option_pregnant'));
             
			 //Code added 

			 $code_client = array(
              'id' => $_SESSION['userid'],
              'medical_cond_selected' => $this->input->post('medicalConditions'),
              'medication_selected' => $this->input->post('medicalRegular') );   

			 if(isset($_SESSION['sitespoints'])) {
				ChromePhp::log("Sitespoints: '" .$_SESSION['sitespoints'] ."'");
				$code_client['MBJSites'] = $_SESSION['sitespoints'];
			 }

			 if(isset($_SESSION['checkedval'])) {
				ChromePhp::log("Checkedval: '" .$_SESSION['checkedval'] ."'");
				$code_client['MBJOption'] = $_SESSION['checkedval'];
			 }
			  
			 //ChromePhp::log(print_r($code_client), true);

          if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }
				
				
				
          
        //  if($this->isExist( 'client_medication_info',  $this->session->userdata('userid') )){
          if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
                   // $this->db->where('c_id', $this->session->userdata('userid'));
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('client_medication_info', $code); 
                }else{

                    return  $this->db->insert('client_medication_info', $code); 
                } 
          
         // return  $this->db->insert('client_medication_info', $code); 
      }
	  
	 public function calc_meditation($medical_history)
     {
		$gender=$_SESSION['gender'];
		$medical_conditions=array();
		$medication=array();
		$HEART=($medical_history[0]->option_1 === 'Y')?"Y":"N";

       //For Heart Condition
        if($HEART=='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $heartcondition=rand($min,$max);  
            if($heartcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($heartcondition >= 8)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $heartmeds=rand($min,$max);  
        if($heartmeds <= 8)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($heartmeds <= 4)
        {
         $medication[]='cardiovascular';  
        }
        if($heartmeds >= 6)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($heartmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='musculo-skeletal';   
            break;
         case 5:
        $medication[]='psychiatric';   
            break;
         case 6:
        $medication[]='renal medication';   
            break; 
        case 7:
        $medication[]='respiratory';   
            break;
         case 8:
        $medication[]='other';   
            break;
        }
          
        }
        $CHESTPAIN =($medical_history[0]->option_2 === 'Y')?"Y":"N";

       //For CHESTPAIN Condition
        if($CHESTPAIN =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $chestcondition=rand($min,$max);  
            if($chestcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($chestcondition >= 9)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $chestmeds=rand($min,$max);  
        if($chestmeds <= 9)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($chestmeds <= 6)
        {
         $medication[]='cardiovascular';  
        }
        if($chestmeds >= 3)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($chestmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='other';   
            break;
        
        }
          
        }
        $FAINT  =($medical_history[0]->option_3 === 'Y')?"Y":"N";

       //For FAINT Condition
        if($FAINT =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $faintcondition=rand($min,$max);  
            if($faintcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        switch($faintcondition)
        {
        case 7:  
        $medical_conditions[]='thyroid disease'; 
        break;    
        case 8:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 9:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 10:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        }
        $min_other=1;
        $max_other=10;
        $othercondition=rand($min_other,$max_other);  
        if($othercondition>=7)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $faintmeds=rand($min,$max);  
        if($faintmeds <= 3)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($faintmeds <= 5)
        {
         $medication[]='cardiovascular';  
        }
        if($faintmeds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($faintmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
       $ASTHMA  =($medical_history[0]->option_4 === 'Y')?"Y":"N";

       //For ASTHMA Condition
        if($ASTHMA =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $asthmacondition=rand($min,$max);  
            if($asthmacondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($asthmacondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='cerebrovascular';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($asthmacondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $asthmameds=rand($min,$max);  
        if($asthmameds >= 2)
        {
         $medication[]='respiratory';  
        }
        if($asthmameds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($asthmameds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($asthmameds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        //For DIABETES Condition
         $DIABETES   =($medical_history[0]->option_5 === 'Y')?"Y":"N";
        if($DIABETES =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $diabetescondition=rand($min,$max);  
            if($diabetescondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($diabetescondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='arthritis or osteoporosis';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($diabetescondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $diabetesmeds=rand($min,$max);  
        if($diabetesmeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($diabetesmeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($diabetesmeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($diabetesmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
        
        }
        if($diabetesmeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        }
       
         //For BONE Condition
         $BONE =($medical_history[0]->option_6 === 'Y')?"Y":"N";
        if($BONE =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $bonecondition=rand($min,$max);  
            if($bonecondition <= 8)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($bonecondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='diabetes';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($bonecondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $bonemeds=rand($min,$max);  
        if($bonemeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($bonemeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($bonemeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($bonemeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($bonemeds >= 5 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($bonemeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        
        }

        
       //For OTHERMED  Condition
         $OTHERMED =($medical_history[0]->option_7 === 'Y')?"Y":"N";
        if($OTHERMED =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $othercondition=rand($min,$max);  
            if($othercondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($othercondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='psychiatric illness';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($othercondition >= 6)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $lastmeds=rand($min,$max);  
        if($lastmeds >= 8)
        {
         $medication[]='psychiatric illness';  
        }
        if($lastmeds <= 2)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($lastmeds >= 6)
        {
         $medication[]='other';  
        }
        switch($lastmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='metabolic';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($lastmeds >= 9 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($lastmeds >= 7) 
        {
         $medication[]='psychiatric';     
        }
       }
         //##################IF ALL NO ANSWER SELECTED#######################################      
     if($HEART=='N' && $CHESTPAIN=='N' && $FAINT=='N' && $ASTHMA=='N' && $DIABETES=='N' && $BONE=='N' && $OTHERMED=='N')
     {
        
         $min=1;
        $max=100;
        $medcondition=rand($min,$max); 
        //echo $medcondition;
        if($medcondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';  
          $medication[]='other';
        }
        switch($medcondition)
        {
        case 3:
        $medical_conditions[]='cardiovascular';   
        $medication[]='LIPID-lowering medication';  
            break;
         case 4:
        $medical_conditions[]='cerebrovascular';
        $medication[]='other';       
            break;
        case 5:
            $medical_conditions[]='diabetes';
            $medication[]='diabetic';   
            break;
         case 6:
             $medical_conditions[]='kidney disease';
             $medication[]='BLOOD PRESSURE-lowering medication';   
            break;
         case 7:
         $medical_conditions[]='liver or metabolic disorder';
         $medication[]='metabolic';      
            break;
         case 8:
         $medical_conditions[]='psychiatric illness';
         $medication[]='psychiatric';      
            break;
        case 9:
         $medical_conditions[]='respiratory disease';
         $medication[]='respiratory';      
            break;
         case 10:
         $medical_conditions[]='thyroid disease';
         $medication[]='metabolic';      
            break;
         }
     if($medcondition >= 98)
     {
     $medical_conditions[]='other';
         $medication[]='other';         
     }
   }
                
        $medication_selected=implode(',',array_unique($medication));
        $medical_cond_selected=implode(',',array_unique($medical_conditions));
    
        $medi_data=array();
    
        $medi_data["medication_list"]=$medication_selected;
        $medi_data["medical_cond_selected"]=$medical_cond_selected;
    
    
//calculate Rabdom Value for GLU CHOL etc     
    
                $finalarr=array();
                $med_values=explode(",",$medication_selected);
                $object1 = new PHPExcel_Calculation_Statistical();
                $gender=$_SESSION['gender'];  
                if(in_array("BLOOD PRESSURE-lowering medication",$med_values))
                {
                    /*Random Male SBP = NORM.INV(RAND(),124,7)
                    Random Male DBP = NORM.INV(RAND(),80,4)*/
                    // Male SBP Calculation

                    if($gender == "M")
                    {           
                        $SBPmale_BPprob = $this->frand(0,1);
                        $SBPmale_BPprob = round($SBPmale_BPprob, 9);
                        $SBP_BPmale = $object1->NORMINV($SBPmale_BPprob,124,7);
                        $Male_BPSBP= $SBP_BPmale;

                        // Male DBP Calculation
                        $DBPmale_BPprob = $this->frand(0,1);
                        $DBPmale_BPprob = round($DBPmale_BPprob, 9);
                        $DBP_BPmale = $object1->NORMINV($DBPmale_BPprob,80,4);
                        $Male_BPDBP= $DBP_BPmale;
                        
                        
                        //$finalarr["SBP"]=$Male_BPSBP;
                        $medi_data["SBP"]=$Male_BPSBP;
                        
                        //$finalarr["DBP"]=$Male_BPDBP;
                        $medi_data["DBP"]=$Male_BPDBP;
                        
                    }

                    else
                    {           
                        /*Random Female SBP = NORM.INV(RAND(),122,7)
                        Random Female DBP = NORM.INV(RAND(),78,4)
                        */
                        // Female SBP
                        $SBPfemale_BPprob = $this->frand(0,1);
                        $SBPfemlae_BPprob = round($SBPfemale_BPprob, 9);
                        $SBP_BPfemale = $object1->NORMINV($SBPfemale_BPprob,122,7);
                        $Female_BPSBP= $SBP_BPfemale;

                        //Female DBP Calculation
                        $DBPfemale_BPprob = $this->frand(0,1);
                        $DBPfemale_BPprob = round($DBPfemale_BPprob, 9);
                        $DBP_BPfemale = $object1->NORMINV($DBPfemale_BPprob,78,4);
                        $Female_BPDBP= $DBP_BPfemale;
                        
                        //$finalarr["SBP"]=$Female_BPSBP;
                        $medi_data["SBP"]=$Female_BPSBP;
                        
                        //$finalarr["DBP"]=$Female_BPDBP;
                        $medi_data["DBP"]=$Female_BPDBP;
                        
                        
                    }
                }


                if(in_array("LIPID-lowering medication",$med_values))
                {
                   /* Random Male CHOL = NORM.INV(RAND(),5.0,0.5)
                      Random Female CHOL = NORM.INV(RAND(),5.1,0.55)*/

                    if($gender == "M")
                    {
                        // MALE CHOL 
                        $Male_lipidprob = $this->frand(0,1);
                        $Male_lipidprob = round($Male_lipidprob, 9);
                        $Male_lipid = $object1->NORMINV($Male_lipidprob,5.0,0.5);
                        $Male_lipidCHOL= $Male_lipid;
                        
                        //$finalarr["chol"]=$Male_lipidCHOL;
                        $medi_data["chol"]=$Male_lipidCHOL;
                    }
                    else
                    {
                        //FEMALE CHOL
                        $Female_lipidprob = $this->frand(0,1);
                        $Female_lipidprob = round($Female_lipidprob, 9);
                        $Female_lipid = $object1->NORMINV($Female_lipidprob,5.1,0.55);
                        $Female_lipidCHOL= $Female_lipid;
                        
                        //$finalarr["chol"]=$Female_lipidCHOL;
                        $medi_data["chol"]=$Female_lipidCHOL;
                    }
                }  


                if(in_array("GLUCOSE-lowering medication",$med_values))
                {
                    /*If MEDICATION list includes ‘GLUCOSE-lowering medication’ then 
                    Random Male GLU = NORM.INV(RAND(),5.2,0.4)
                    Random Female GLU = NORM.INV(RAND(),5.1,0.5)*/

                    if($gender == "M")
                    {
                        $male_gluprob = $this->frand(0,1);
                        $male_gluprob = round($male_gluprob, 9);
                        $male_glu = $object1->NORMINV($male_gluprob,5.2,0.4);
                        $Male_glu= $male_lipid;
                        
                        //$finalarr["glucose"]=$Male_glu;
                        $medi_data["glucose"]=$Male_glu;
                    }
                    else{
                        $female_gluprob = $this->frand(0,1);
                        $female_gluprob = round($female_gluprob, 9);
                        $female_glu = $object1->NORMINV($female_gluprob,5.1,0.5);
                        $Female_glu= $female_glu;
                        
                        //$finalarr["glucose"]=$Female_glu;
                        $medi_data["glucose"]=$Female_glu;
                    }
                }

             //End Of Medications code
          
              
          

              //MBJ Code

              $MBJ_site=array();
              $MBJPain=rand(1,10);
 
              if($MBJPain <= 3){
                  $option_MBJ="Y"; }
              else{
                  $option_MBJ="N"; }
              
			  //$finalarr["MBJoption"]=$option_MBJ;
			  $medi_data["MBJoption"]=$option_MBJ;
			  
              if($option_MBJ == "Y")
              {
                  $MBJ_number=rand(1,5);
                  if($MBJ_number == 1)
                  {
                      for($i=0;$i<1;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 2)
                  {
                      for($i=0;$i<2;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 3)
                  {
                      for($i=0;$i<3;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 4)
                  {
                      for($i=0;$i<4;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  elseif($MBJ_number == 5)
                  {
                      for($i=0;$i<5;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  else
                  {
                      $MBJ_site[0] = "No value exists";
                  }
              }
              else
              {
                  $MBJ_site = "MBJ Site NO";
              }
              $medi_data["mbjsite"]=$MBJ_site;
// calculate Random Values for CHOL GLU etc End



         // PREGNANT ALGO
         if($_SESSION['gender'] == "F" && $_SESSION['age'] < 38)
         {
            /* RANDBETWEEN(1,100) = PREGPROB
             If PREGPROB  = 1 then pregnancy = YES*/
             $pregprob = rand(1,100);
             //echo $pregprob;
             if($pregprob == 1)
             {
                 $checked_pregnant='Y';
             }
             else
             {
                 $checked_pregnant='N';
             }
         }
         
             $medi_data["pregnant"]=$checked_pregnant;


        //print_r($medi_data); die;
              return $medi_data;
    
    
}
// Meditation Model Kritika 
	  
	  
	  
//kritika  Medication
    public function calculate_medicationsdetail()  // When data is present
    {
                $getdata=$this->fetchDetail('client_medication_info');
                //print_r($getdata);   
                $finalarr=array();
                $med_values=$getdata[0]->medical_regular; // to get medications
                $mbj_value=$getdata[0]->option_4;
             
                if(!isset($med_values))
                {
                 $finalarr["medical_values"]="Medical Values are empty";
                }
                else
                {
                $med_values=explode(",",$med_values);
                //print_r($med_values); 
                $object1 = new PHPExcel_Calculation_Statistical();

                $gender=$_SESSION['gender'];  

                     
                if(in_array("BLOOD PRESSURE-lowering medication",$med_values))
                {
                    /*Random Male SBP = NORM.INV(RAND(),124,7)
                    Random Male DBP = NORM.INV(RAND(),80,4)*/
                    // Male SBP Calculation

                    if($gender == "M")
                    {           
                        $SBPmale_BPprob = $this->frand(0,1);
                        $SBPmale_BPprob = round($SBPmale_BPprob, 9);
                        $SBP_BPmale = $object1->NORMINV($SBPmale_BPprob,124,7);
                        $Male_BPSBP= $SBP_BPmale;

                        // Male DBP Calculation
                        $DBPmale_BPprob = $this->frand(0,1);
                        $DBPmale_BPprob = round($DBPmale_BPprob, 9);
                        $DBP_BPmale = $object1->NORMINV($DBPmale_BPprob,80,4);
                        $Male_BPDBP= $DBP_BPmale;
                        
                        
                        $finalarr["SBP"]=$Male_BPSBP;
                        $finalarr["DBP"]=$Male_BPDBP;
                        
                    }

                    else
                    {           
                        /*Random Female SBP = NORM.INV(RAND(),122,7)
                        Random Female DBP = NORM.INV(RAND(),78,4)
                        */
                        // Female SBP
                        $SBPfemale_BPprob = $this->frand(0,1);
                        $SBPfemlae_BPprob = round($SBPfemale_BPprob, 9);
                        $SBP_BPfemale = $object1->NORMINV($SBPfemale_BPprob,122,7);
                        $Female_BPSBP= $SBP_BPfemale;

                        //Female DBP Calculation
                        $DBPfemale_BPprob = $this->frand(0,1);
                        $DBPfemale_BPprob = round($DBPfemale_BPprob, 9);
                        $DBP_BPfemale = $object1->NORMINV($DBPfemale_BPprob,78,4);
                        $Female_BPDBP= $DBP_BPfemale;
                        
                        $finalarr["SBP"]=$Female_BPSBP;
                        $finalarr["DBP"]=$Female_BPDBP;
                        
                        
                    }
                }


                if(in_array("LIPID-lowering medication",$med_values))
                {
                   /* Random Male CHOL = NORM.INV(RAND(),5.0,0.5)
                      Random Female CHOL = NORM.INV(RAND(),5.1,0.55)*/

                    if($gender == "M")
                    {
                        // MALE CHOL 
                        $Male_lipidprob = $this->frand(0,1);
                        $Male_lipidprob = round($Male_lipidprob, 9);
                        $Male_lipid = $object1->NORMINV($Male_lipidprob,5.0,0.5);
                        $Male_lipidCHOL= $Male_lipid;
                        
                        $finalarr["chol"]=$Male_lipidCHOL;
                    }
                    else
                    {
                        //FEMALE CHOL
                        $Female_lipidprob = $this->frand(0,1);
                        $Female_lipidprob = round($Female_lipidprob, 9);
                        $Female_lipid = $object1->NORMINV($Female_lipidprob,5.1,0.55);
                        $Female_lipidCHOL= $Female_lipid;
                        
                        $finalarr["chol"]=$Female_lipidCHOL;
                    }
                }  


                if(in_array("GLUCOSE-lowering medication",$med_values))
                {
                    /*If MEDICATION list includes ‘GLUCOSE-lowering medication’ then 
                    Random Male GLU = NORM.INV(RAND(),5.2,0.4)
                    Random Female GLU = NORM.INV(RAND(),5.1,0.5)*/

                    if($gender == "M")
                    {
                        $male_gluprob = $this->frand(0,1);
                        $male_gluprob = round($male_gluprob, 9);
                        $male_glu = $object1->NORMINV($male_gluprob,5.2,0.4);
                        $Male_glu= $male_lipid;
                        
                        $finalarr["glucose"]=$Male_glu;
                    }
                    else{
                        $female_gluprob = $this->frand(0,1);
                        $female_gluprob = round($female_gluprob, 9);
                        $female_glu = $object1->NORMINV($female_gluprob,5.1,0.5);
                        $Female_glu= $female_glu;
                        
                        $finalarr["glucose"]=$Female_glu;
                    }
                }

                }// End of Else
             
             //End Of Medications code
          
              
          

              //MBJ Code

              $MBJ_site=array();
              $MBJPain=rand(1,10);
 
              if($MBJPain <= 3){
                  $option_MBJ="Y"; }
              else{
                  $option_MBJ="N"; }
              
			  $finalarr["MBJoption"]=$option_MBJ;
			  
              if($option_MBJ == "Y")
              {
                  $MBJ_number=rand(1,5);
                  if($MBJ_number == 1)
                  {
                      for($i=0;$i<1;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 2)
                  {
                      for($i=0;$i<2;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 3)
                  {
                      for($i=0;$i<3;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 4)
                  {
                      for($i=0;$i<4;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  elseif($MBJ_number == 5)
                  {
                      for($i=0;$i<5;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  else
                  {
                      $MBJ_site[0] = "No value exists";
                  }
              }
              else
              {
                  $MBJ_site = "MBJ Site NO";
              }
              $finalarr["mbjsite"]=$MBJ_site;
              //print_r($finalarr); die;
        
              return $finalarr;   
       // return "true";
        
    }
    
// Kritika MEdications end 	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
	  
      
       public function fetchScreeningInfo()
      {
           $this->fetchScreeningStatusInfo();
     //   $user_id=$this->session->userdata('userid');   
        $user_id=$_SESSION['userid'];   
        
       $q="SELECT ci.id,ci.p_desc as client_info, (pai.option_2 + 2*pai.option_4 + pai.option_6) as physical_activity ,rfi.option_1 as heart_disease,rfi.option_2 as smoking ,rfi.option_7 as hbp,rfi.option_8 as dbp ,rfi.option_11 as colestrol,rfi.option_9 as hdl,rfi.option_10 as ldl,rfi.option_13 as triglycerides,rfi.option_12 as hbg ,bci.option_bmi as bmi ,bci.option_waist as waist ,bci.option_hip as hip,ci.gender as gender ,ci.dob ,mi.option_8 as proceed ,rfi.option_gender,rfi.option_gender,rfi.option_age,rfi.option_3 as smoking2 ,rfi.option_4 as high_blood_pressure,rfi.option_5 as high_colestrol,rfi.option_6 as high_sugar , medi.medical_regular as llm , mi.notes as medical_notes ,medi.notes as medication_notes,medi.option_4 as medication_pic,medi.pt1 as pt1,medi.pt2 as pt2,medi.pt3 as pt3,medi.pt4 as pt4,medi.pt5 as pt5,medi.pt6 as pt6,medi.pt7 as pt7,medi.pt8 as pt8,medi.pt9 as pt9,medi.pt10 as pt10,medi.pt11 as pt11,medi.pt12 as pt12,medi.pt13 as pt13,medi.pt14 as pt14,medi.pt15 as pt15 ,bci.option_height as self_height ,bci.option_weight as self_weight  ,bci.option_height_measured as measured_height ,bci.option_weight_measured as measured_weight , pai.option_2 as pa2 , pai.option_6 as pa6, pai.option_4 as pa4 , pai.option_7 as pa7 , pai.option_8 as pa8,pai.option_9 as pa9, rfi.option_1 as family_history , rfi.option_2 as rf4 , rfi.option_smoke as rf5 , rfi.option_7 as rf_systolic , rfi.option_8 as rf_diastolic , rfi.option_smoke_6 as rf7 , rfi.option_4 as rf8, rfi.option_12 as rf_fbg , rfi.option_6 as rf10 , rfi.option_5 as rf9 ,bci.option_triceps,bci.option_biceps,option_subscapular,bci.option_sos FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id  where  ci.id= $user_id ";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
           
            /*** a date before 1970 ***/
        
            $date = str_replace('/', '-', $row['dob']);
                      
           $dateOfBirth=   date("Y-m-d", strtotime( $date) );
          
           $dob = strtotime($dateOfBirth);

            /*** another date ***/
            $tdate = strtotime(date('Y-m-d'));
          
            array_push($row,array('age'=>$this->getAge($dob, $tdate)));
            array_push($row,array('status'=>$this->fetchScreeningStatusInfo()));
            return $row;
           
      }
      
      //Get the Random Value In Pre-exercise Form
        public function fetch_preexercise_details()
      {
         $fieldData=array();
        $user_id=$_SESSION['userid'];   
        $vp_age=$_SESSION['vp_age'];
        $MASS=$_SESSION['MASS'];
        $BMI=$_SESSION['BMI'];
        $HEIGHT=$_SESSION['HEIGHT'];
        $sub_population=$_SESSION['subpopulation'];
        $gender=$_SESSION['gender'];
              //HEART QUESTION
        //calculate Heart Probability
      //FOR MALE HEART QUES
       if($gender=='M')
       {
        $HEARTPROB=1+(0.00000161*pow($vp_age,3.7892425)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';
        
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       }  
       
//FOR FEMALE HEART QUES
       if($gender=='F')
       {
        $HEARTPROB=1+(0.0000113*pow($vp_age,3.1941547)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';    
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       
      }
          
//END HEART QUESTION
  
      //FOR MALE CHEST PAIN
       if($gender=='M')
       {
        $CHESTPROB=1+(0.00105*pow($vp_age,2)+0.08262*$vp_age-3.15); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       }   
      
//FOR FEMALE CHEST PAIN QUES
       if($gender=='F')
       {
        $CHESTPROB=1+(0.00223*pow($vp_age,2)-0.06514*($vp_age)+0.56); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       } 
      // FEEL FAINT QUES
         //FOR MALE FAINT
       if($gender=='M')
       {
        $FAINTPROB=(0.00208*pow($vp_age,2)-0.09524*($vp_age)+1.7); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       }
        
       
         //FOR FEMALE FAINT
       if($gender=='F')
       {
        $FAINTPROB=(0.00262*pow($vp_age,2)-0.10952*($vp_age)+2.3); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       } 
       //ASTHMA ATTACK QUES
       
         //FOR MALE ASTHMA ATTACK
       if($gender=='M')
       {
        $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
         //FOR FEMALE ASTHMA ATTACK
       if($gender=='F')
       {
           $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
        
          //FOR MALE DIABETES
       if($gender=='M')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(5 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=3;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
         //FOR FEMALE DIABETES
       if($gender=='F')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(2 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=2;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
          //FOR MALE BONE OR MUSCLE PROBLEM
       if($gender=='M' || $gender=='F')
       {
         $BONEPROB =0.002321* pow($vp_age,2) - 0.152143*($vp_age)+ 2.8; 
        /*Random No*/
        $min=1;
        $max=100;
        $bone=rand($min,$max);
       
        // 1 Question yes no
        if($bone<=$BONEPROB)
        {
        $fieldData[0]->option_6='Y';    
        }
        else
        {
         $fieldData[0]->option_6='N';      
        }
       } 
     
    //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=45 && $gender=='M')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
      
       //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=55 && $gender=='F')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
     
       return $fieldData;      
    }//end of pre exercising Option
      
    
    //GET PHYSICAL ACTIVITY DETAILS  
    public function fetch_physical_activity_details()
      {
       $fieldData=array();
       $subpopulation=$_SESSION['subpopulation'];
       
	   //$subpopulation='Sedentary';
	   
	   
       $gender=$_SESSION['user_gender'];
       $age=$_SESSION['vp_age'];
       $BMI=$_SESSION['BMI'];
	   
	  // print_r($_SESSION);
	  // echo "Sunpopulation".$_SESSION['subpopulation'];
	   
	   //echo $subpopulation;
	   //die('aa');
	   
       if($subpopulation=='Sedentary')
       {
        $walk_freq='0';
        $walk_tot_min='0';
        $vig_freq='0';
        $vig_freq_tot_min='0';
        $other_mod_freq='0';
        $other_mod_tot_min='0';
       
		
       }
           



//GENERAL MALE
       if($subpopulation=='General' && $gender=='M')
       {
        $minoccu=-1;
        $maxoccu=4;
        $walk_freq=rand($minoccu,$maxoccu)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=6;
                $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
           
           
           
           
           
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }                
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      //FOR FEMALE 
          if($subpopulation=='General' && $gender=='F')
       {
        $minoccu=-1;
        $maxoccu=6;
        $walk_freq=rand($minoccu,$maxoccu)+1;
        $walk_duration=rand(2,5)*5;   
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='10';   
        }
        $other_mod_tot_min=$other_mod_freq * 10;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
       
      //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='M')
       {
        $walk_freq=rand(-1,10)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=10;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,4)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur=rand(2,10) * 5;       
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='25';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
       //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='F')
       {
        $walk_freq=rand(-1,12)+1;
        $walk_duration=rand(2,10)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
         $vigdur=rand(2,10) * 5;       
      
         $vig_freq_tot_min=$vig_freq * 10;
        
        $other_mod_freq=rand(-1,2)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='20';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
        //Athlete SUB WITH MALE 
      if($subpopulation=='Athlete')
       {
        $walk_freq=rand(1,5)+1;
        $walk_duration=rand(2,60)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(3,7)+1 ;
         $vigdur=rand(4,12) * 5;       
      
         $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,12) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
      
      } 
     if($age < 60)
       {
        $SedTV = (rand(-1,7)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }
      //IF AGE > 60
       if($age >= 60)
       {
        $SedTV = (rand(0-1,11)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }  
       // Driving or being driven
        //IF AGE < 60
       if($age < 65)
       {
        $DriveTime = (rand(-1,5)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 0.5;   
       } 
      }  
          //IF AGE < 60
       if($age >= 65)
       {
        $DriveTime = (rand(-1,4)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 2;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 1;   
       } 
      } 
       
      //Job requires sitting?
        //IF AGE < 60
       if($age < 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 10)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }  
         //IF AGE < 60
       if($age >= 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 8)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }
      $fieldData[0]->option_1=$walk_freq;
      $fieldData[0]->option_2=$walk_tot_min;
      $fieldData[0]->option_3=$vig_freq;
      $fieldData[0]->option_4=$vig_freq_tot_min;
      $fieldData[0]->option_5=$other_mod_freq;
      $fieldData[0]->option_6=$other_mod_tot_min;
      $fieldData[0]->option_7=$SedTV;
      $fieldData[0]->option_8=$DriveTime;
      $fieldData[0]->option_9=$jobsitopt;
      //$fieldData[0]->total_PA=$total_PA;
	  
	  $fieldData[0]->total_PA=$total_PA;
      $fieldData[0]->vig_total=$vig_freq_tot_min;
         $_SESSION['total_PA']=$total_PA;
		 $_SESSION['vig_total']=$vig_freq_tot_min;
     // $resultPA=array('total_PA'=>$total_PA,'walk_tot_min'=>$walk_tot_min,'vig_freq_tot_min'=>$vig_freq_tot_min,'other_mod_tot_min'=>$other_mod_tot_min,'walk_freq'=>$walk_freq,'vig_freq'=>$vig_freq,'other_mod_freq'=>$other_mod_freq,'SedTV'=>$SedTV,'DriveTime'=>$DriveTime,'jobsitopt'=>$jobsitopt); 
          
    // print_r($fieldData); die;
          
          
     return ($fieldData);
     }
      
      
	     //GET RISK FACTOR
     public function fetch_risk_factors_details()
     {
      // print_r($_SESSION); die;
       $fieldData=array();
       $subpopulation=$_SESSION['subpopulation'];
       $gender=$_SESSION['user_gender'];
       $age=$_SESSION['vp_age'];
       $BMI=$_SESSION['BMI'];   
       $family_his_num=rand(1,10); 
       if($family_his_num=='1')
       {
       $family_his='Y';    
       }
       else
       {
       $family_his='N';    
       }
     if($family_his=='Y')
     {
      $family_gen_num=rand(1,2);    
     }
      switch($family_gen_num)
      {
        case 1:
        $family_gen='M';
           break;
       case 2:
        $family_gen='F';
           break;
      }
     $age_of_ralative=rand(1,30)+40;
//     SMOKING PROFILE 
//    [MALES] – current smoker
     if($gender=='M' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)   = -0.00004155*(Age)^4 + 0.00803066*(Age)^3 - 0.57352160*(Age)^2 + 17.52605448*(Age) - 156.13741177
        $smoke_prob_per= -0.00004155 * pow($age,4) +  0.00803066 * pow($age,3) - 0.57352160 * pow($age,2) + 17.52605448 * $age - 156.13741177;    
        }
     }
     
    //General  
     if($gender=='M' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002770*(Age)^4 + 0.00535377*(Age)^x3 - 0.38234773*(Age)^2 + 11.68403632*(Age) - 104.09160785
        $smoke_prob_per= -0.00002770 * pow($age,4) +  0.00535377 * pow($age,3) -0.38234773 * pow($age,2) + 11.68403632 * $age - 104.09160785;    
        }
     }
     //Active   
     if($gender=='M' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%) = -0.00000692*(Age)^4 + 0.00133844*(Age)^3 - 0.09558693*(Age)^2 + 2.92100908*(Age) - 26.02290196
        $smoke_prob_per= -0.00000692 * pow($age,4) +  0.00133844 * pow($age,3) - 0.09558693 * pow($age,2) + 2.92100908 * $age - 26.02290196;    
        }
     }
     // FEMALE SMOKEING PROFILE
     if($gender=='F' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //= -0.00003046*(Age)^4 + 0.00578225*(Age)^3 - 0.40274444*(Age)^2 + 11.95970651*(Age) - 102.32997609
        $smoke_prob_per= -0.00003046 * pow($age,4) +   0.00578225 * pow($age,3) - 0.40274444 * pow($age,2) +  11.95970651 * $age - 102.32997609;    
        }
     }
     
    //General  
     if($gender=='F' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002030*(Age)^4 + 0.00385484*(Age)^3 - 0.26849630*(Age)^2 + 7.97313767*(Age) - 68.21998406
        $smoke_prob_per= -0.00002030 * pow($age,4) + 0.00385484 * pow($age,3) - 0.26849630 * pow($age,2) + 7.97313767 * $age - 68.21998406;    
        }
     }
     //Active   
     if($gender=='F' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00000508*(Age)^4 + 0.00096371*(Age)^3 - 0.06712407*(Age)^2 + 1.99328442*(Age) - 17.05499601
        $smoke_prob_per= -0.00000508 * pow($age,4) + 0.00096371 * pow($age,3) - 0.06712407 * pow($age,2) + 1.99328442 * $age - 17.05499601;    
        }
     }
     
     //Male and Female smokers: 
     $random_num=rand(1,100);
     if($random_num > $smoke_prob_per)
     {
      $smoking='N';      
     }
     else
     {
       $smoking='Y';   
     }
     
     if($smoking=='Y')
     {
     $smoke_perday=rand(1,4)*5;      
     }
     
     if($subpopulation=='Athlete')
     {
      $smoking='N';  
        }
    //Have you quit smoking in the last 6 months?
     if($smoking=='N')
     {
       if($subpopulation=='Sedentary' || $subpopulation=='General')
       {
        $randomval=rand(1,15);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
     } 
     //Active
      if($subpopulation=='Active')
       {
        $randomval=rand(1,40);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
      } 
      if($subpopulation=='Athlete')
      {
      $month_six_smoke='N';  
      }
     }
   //Have you been told you have high blood pressure?
   $RNBP= rand(1,100); 
   $SRBPprob = 0.00464 * pow($age,2) - 0.26429 * ($age) + 5.06250;  
   
   If($RNBP <= $SRBPprob)
     {
        $high_bp='Y'; 
     }
     else
     {
      $high_bp='N';    
     }
     //Have you been told you have high lipids? 
     $RNLIPID=rand(1,100); 
     $SRLIPIDprob = 0.00464 * pow($age,2) - 0.26429*($age) + 5.06250;
     If($RNLIPID<=$SRLIPIDprob)
     {
        $high_cols='Y'; 
     }
     else
     {
      $high_cols='N';    
     }
     //Have you been told you have high blood sugar?
     $RNSUGAR=rand(1,100); 
     $SRSUGARprob = 0.0019 * pow($age,2) - 0.0881 * ($age) + 2.1071;
     //If SRSUGARprob < RNSUGAR then ‘YES’ else
     if($RNSUGAR <=$SRSUGARprob)
     {
      $blood_sugar='Y';   
     }
    else{
      $blood_sugar='N';   
    }
 $getbloodvalue=$this->Blood_values_norms($age,$gender);    
//MEASURED BLOOD PRESSURE
    //SBP and DBP
  // Random SBP =ROUND(NORM.INV(RAND(), mean SBP, SD SBP))
 $object1 = new PHPExcel_Calculation_Statistical();
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_sbp=$getbloodvalue['mean_sbp'];
 $sd_sbp=$getbloodvalue['sd_sbp'];
 $x=$object1->NORMINV($probability,$mean_sbp,$sd_sbp);
  $randomsbp=round($x);   
  //Generate a random DBP using the normal distribution function :
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_dbp=$getbloodvalue['mean_dbp'];
 $sd_dbp=$getbloodvalue['sd_dbp'];
 $get_dbp=$object1->NORMINV($probability,$mean_dbp,$sd_dbp);
  $randomdbp=round($get_dbp);
//Adjustment 
  if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 0.94;
     $DBP_adjustment = 0.97;
    }
    else
    {
     $SBP_adjustment = 0.93;
     $DBP_adjustment = 0.96;    
    }
  }
    if($subpopulation=='Sedentary' || $subpopulation=='General')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;
    }
    else
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;    
    }
  }
  //Therefore, for all virtual subjects:
  //SBP = round(random SBP * SBP adjustment) 
  //DBP = round(random DBP * DBP adjustment) 
  $SBP=round($randomsbp * $SBP_adjustment);
  $DBP=round($randomdbp * $DBP_adjustment);
  //[adjustment for overweight /obese people]   
  if($BMI >= 35)
  {
   $SBP = $SBP + 9;
   $DBP = $DBP + 6;
  }
   if($BMI >= 30 && $BMI<35)
  {
   $SBP = $SBP + 5;
   $DBP = $DBP + 4;
  }
  if($BMI >= 28 && $BMI<30)
  {
   $SBP = $SBP + 2;
   $DBP = $DBP + 2;
  }
  //[check to ensure no unrealistic lower values]  
  if($gender=='M' && $SBP<90)
  {
   $SBP =90;   
  }
   //[check to ensure no unrealistic lower values]  
  if($gender=='F' && $SBP < 85)
  {
   $SBP =85;   
  }
   if($gender=='M' && $DBP < 52)
  {
   $DBP =52;   
  }
   if($gender=='F' && $DBP < 50)
  {
   $DBP =50;   
  }
 // [check to prevent to SBP and DBP from being unrealistically close by this algorithm]:
  //if SBP-DBP < 22 then SBP = DBP+22 
  if(($SBP - $DBP) < 22)
  {
   $SBP = $DBP + 22;   
  }
 
  //TOTAL BLOOD CHOLESTEROL [CHOL]
  $bloodprob=$this->frand(0,1);
  $bloodprob=round($bloodprob,9); 
  $mean_cols=$getbloodvalue['mean_cols'];
  $sd_cols=$getbloodvalue['sd_cols'];
  $cols_random=$object1->NORMINV($bloodprob,$mean_cols,$sd_cols);
  $cols_random=round($cols_random,2);   
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $cols_adjustment = 0.97;
    }
    else
    {
     $cols_adjustment = 0.96; 
    }
  $CHOL=($cols_random * $cols_adjustment);
    
    }
    else
    {
    $CHOL=$cols_random;    
    }
  //[If Male and CHOL <2.5 then CHOL = 2.5 ; If Female and CHOL <2.5 then CHOL = 2.5]
  if($CHOL < 2.5)
  {
   $CHOL = 2.5;   
  }
 //HIGH-DENSITY LIPOPROTEIN [HDL]
  $proprob=$this->frand(0,1);
  $proprob=round($proprob,9); 
  $mean_ln_hdl=$getbloodvalue['mean_ln_hdl'];
  $sd_ln_hdl=$getbloodvalue['sd_ln_hdl'];
  $pro_random=$object1->NORMINV($proprob,$mean_ln_hdl,$sd_ln_hdl);
  $pro_random=round($pro_random,2);   
  $random_hdl=exp($pro_random);
  $random_hdl=round($random_hdl,2);
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $hdl_adjustment = 1.15;
    }
    else
    {
     $hdl_adjustment = 1.05; 
    }
  $HDL=($random_hdl * $hdl_adjustment);
    
    }
    else
    {
    $HDL=$random_hdl;    
    }
  if($gender=='M' && $HDL < 0.6)
  {
   $HDL = 0.6;   
  }
  if($gender=='F' && $HDL < 0.75)
  {
   $HDL = 0.75;   
  }
 
  //LOW-DENSITY LIPOPROTEIN [LDL]
  $ldlprob=$this->frand(0,1);
  $ldlprob=round($ldlprob,9); 
  $mean_ldl=$getbloodvalue['mean_ldl'];
  $sd_ldl=$getbloodvalue['sd_ldl'];
  $ldl_random=$object1->NORMINV($ldlprob,$mean_ldl,$sd_ldl);
  $ldl_random=round($ldl_random,2);   
  $random_ldl=$ldl_random;
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $ldl_adjustment = 0.94;
    }
    else
    {
     $ldl_adjustment = 0.95; 
    }
  $LDL=($random_ldl * $ldl_adjustment);
    
    }
    else
    {
    $LDL=$random_ldl;    
    }
  
 if($gender=='M' && $LDL < 1.3)
  {
   $LDL = 1.3 ;   
  }
  if($gender=='F' && $LDL < 1.2)
  {
   $LDL = 1.2;   
  }
    
  //TRIGLYCERIDES [TRIG]  
   $triprob=$this->frand(0,1);
  $triprob=round($triprob,9); 
  $mean_ln_trig=$getbloodvalue['mean_ln_trig'];
  $sd_ln_trig=$getbloodvalue['sd_ln_trig'];
  $tri_random=$object1->NORMINV($triprob,$mean_ln_trig,$sd_ln_trig);
  $tri_random=round($tri_random,2);   
  $random_tri=exp($tri_random);
  $random_tri=round($random_tri,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $trig_adjustment = 0.92;
    }
    else
    {
     $trig_adjustment = 0.93; 
    }
  $TRIG=($random_tri * $trig_adjustment);
    
    }
    else
    {
    $TRIG=$random_tri;    
    }
  
 if($gender=='M' && $TRIG < 0.45)
  {
   $TRIG = 0.45 ;   
  }
  if($gender=='F' && $TRIG < 0.4)
  {
   $TRIG = 0.4;   
  } 
    
  //FASTING BLOOD GLUCOSE [GLU] 
  //TRIGLYCERIDES [TRIG]  
   $gluprob=$this->frand(0,1);
  $gluprob=round($gluprob,9); 
  $mean_ln_glu=$getbloodvalue['mean_ln_glu'];
  $sd_ln_glu=$getbloodvalue['sd_ln_glu'];
  $glu_random=$object1->NORMINV($gluprob,$mean_ln_glu,$sd_ln_glu);
  $glu_random=round($glu_random,2);   
  $random_glu=exp($glu_random);
  $random_glu=round($random_glu,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $glu_adjustment = 1.05;
    }
    else
    {
     $glu_adjustment = 1.04; 
    }
  $GLU=($random_glu * $glu_adjustment);
    
    }
    else
    {
    $GLU=$random_glu;    
    }
  
 if($gender=='M' && $GLU < 3.9)
  {
   $GLU = 3.9 ;   
  }
  if($gender=='F' && $GLU < 3.7)
  {
   $GLU = 3.7;   
  }
  //other BLOOD PARAMETER PROFILE [Hb, Hct,] [not inserted into any fields at this stage in the EST program]
  if($gender=='M')
  {
  $Hbgen =  -0.00087 * pow($age,2) + 0.04542 * ($age) + 14.32083;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.36);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
  if($gender=='F')
  {
  //Hb = -0.00000132*(Age)^4 + 0.0002767*(Age)^3 - 0.02091004*(Age)^2 + 0.66274621*(Age) + 5.7875
      $Hbgen =  -0.00000132 * pow($age,4) + 0.0002767 * pow($age,3)- 0.02091004 * pow($age,2)+ 0.66274621 * ($age) +  5.7875;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.24);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
 
  //Haematocrit [Hct] [%]
  $Hct_gen = 2.63 * $Hb + 5.83;
  $hctmprob=$this->frand(0,1);
  $hctmprob=round($hctmprob,9); 
  $hct_random=$object1->NORMINV($hctmprob,$Hct_gen,1.22);
  $Random_Male_Hct= round($hct_random,2);
  $hctfprob=$this->frand(0,1);
  $hctfprob=round($hctfprob,9); 
  $hctf_random=$object1->NORMINV($hctfprob,$Hct_gen,1.16);
  $Random_Female_Hct= round($hctf_random,2); 
  
  
  $fieldData[0]->option_1=$family_his;
  $fieldData[0]->option_gender=$family_gen;
  $fieldData[0]->option_age=$age_of_ralative;
  $fieldData[0]->option_2=$smoking;
  $fieldData[0]->option_smoke=$smoke_perday;
  $fieldData[0]->option_3=$month_six_smoke;
  $fieldData[0]->option_smoke_6=$total_smoke_perday;
  $fieldData[0]->option_4 =$high_bp;
  $fieldData[0]->option_5 =$high_cols; 
  $fieldData[0]->option_6 = $blood_sugar;
  $fieldData[0]->option_7 = $SBP;
  $fieldData[0]->option_8 = $DBP;
  $fieldData[0]->option_9 = round($HDL,1);
  $fieldData[0]->option_10 = round($LDL,1);
  $fieldData[0]->option_11 = round($CHOL,1);
  $fieldData[0]->option_12 = round($GLU,1);
  $fieldData[0]->option_13 = round($TRIG,1);
  
 return ($fieldData);
  }

    
    
    
   
// kritika 29 Sept BODY COMPOSITION()    
public function fetch_body_composition($key="")
{
    $object1 = new PHPExcel_Calculation_Statistical();
    $fieldData=array();
    $meanwaist=0.0;     $waist=0.0;     $bodycompprob=0.0;    $hip=0.0;     $waistnorm=0.0;     $hipnorm=0.0;   $meantri=0.0;   $tricepsnorm=0.0; $triceps=0.0; $options_height=0.0;  $options_weight=0.0;
    
    $measuredheight=$_SESSION['HEIGHT'];
    $measuredmass=$_SESSION['MASS'];
    $measuredBMI=$_SESSION['BMI'];
    $measuredpopulation=$_SESSION['subpopulation'];
    $gender=$_SESSION['user_gender'];
    $measuredage=$_SESSION['vp_age'];
    $mass=$_SESSION['MASS'];
    $bmi=$_SESSION['BMI'];
    $height=$_SESSION['HEIGHT'];
    $age=$_SESSION['vp_age'];

// SELF REPORT 
/*SRH = RANDBETWEEN(1,100) 
If SRH < 50 then: [else there will be no value entered in the self-report fields for HEIGHT and MASS]
SRHEIGHT = (NORM.INV(RAND(),HEIGHT,0.5)+0.5

Enter SRHEIGHT in the ‘Self report HEIGHT’ field on screen 4

SRMASS = (NORM.INV(RAND(),MASS,0.8)-1.2

Enter SRMASS in the ‘Self report MASS’ field on screen 4
*/
       $srh=rand(1,100);
       if($srh <= 50)
       {
       //SRHEIGHT = (NORM.INV(RAND(),HEIGHT,0.5)+0.5
       $srhprob = $this->frand(0,1);
       $srhprob = round($srhprob, 9);
       $srh=$object1->NORMINV($srhprob,$measuredheight,0.5)+0.5;
       
           
       $options_height=round($srh,2);
           
           
       $massprob = $this->frand(0,1);
       $massprob = round($massprob, 9);
       $srmass=$object1->NORMINV($massprob,$measuredmass,0.8)-1.2;
       $options_weight=round($srmass,2);
        
       }
       $options_height = $options_height; 
       $options_weight = $options_weight; 
      
    if($gender == 'M')      // MALE 
    {
        /*MeanWaist = 2.639*(BMI) + 21.959 
        WAISTNORM = (NORM.INV(RAND(),MeanWaist,4.5)*/
        //Calculate for Male
        $object1 = new PHPExcel_Calculation_Statistical();
        $meanwaist= round(2.639*($measuredBMI) + 21.959);
        $bodycompprob=$this->frand(0,1);
        $bodycompprob=round($bodycompprob,9); 
        $waistnorm = $object1->NORMINV($bodycompprob,$meanwaist,4.5);
       // echo $waistnorm; die;
        
        // HIP CALCULATION
        /*MeanHip = 1.659*(BMI) + 59.1 
        HIPNORM = (NORM.INV(RAND(),MeanHip,4.3)*/
        $meanhip = 1.659*($measuredBMI) + 59.1 ;
        $hipprob=$this->frand(0,1);
        $hipprob=round($hipprob,9); 
        //$hipnorm = $object1->NORMINV($hipprob,$meanhip,4.3);
        
        /*HIPNORM = (NORM.INV(RAND(),MeanHip,3.3)*/       
        $hipnorm = $object1->NORMINV($hipprob,$meanhip,3.3);  // new
        
       
       if($measuredpopulation == "Sedentary")
        {
            //$waist = $waistnorm * 1.08; //OLD
            $waist = $waistnorm * 1.1;  // NEW
            //$hip = $hipnorm * 1.08;
            $hip = $hipnorm * 1.1;       // new
        }
        elseif($measuredpopulation == "General")
        {
            $waist = $waistnorm * 1.04;
            $hip = $hipnorm * 1.04;
        }
        elseif($measuredpopulation == "Active")
        {
            $waist = $waistnorm * 0.98;
            $hip = $hipnorm * 0.98;
        }
        elseif($measuredpopulation == "Athlete")
        {
            $waist = $waistnorm * 0.94;
            $hip = $hipnorm * 0.94;
        }
		
		
		// WAIST ADJUSTMENT 12 oct
/*
 PLEASE ADD Adjustment because the values are causing problems:

MALE WAIST adjustment = -0.0018* AGE^2 + 0.2964*AGE - 6 
eg., a 40 yr old adjustment = -0.0018*40^2 + 0.2964*40 - 6 = 2.98 cm
Therefore, 2.98 cm would be ADDED to the WAIST value to give the FINAL [adjusted] WAIST value  [used from now on]
*/
    
        $m_waist_adj = - 0.0018* pow($age,2) + 0.2964 * $age - 6 ;
        $waist = $waist +  $m_waist_adj;
		
		
    }
    
    else     // FEMALE
    {
        /*MeanWaist = 2.293*(BMI) + 21.950 
        WAISTNORM = (NORM.INV(RAND(),MeanWaist,4.9)*/
        //Calculate for FeMale
        $object1 = new PHPExcel_Calculation_Statistical();
        $meanwaist= round(2.293*($measuredBMI) + 21.950);
        $bodycompprob=$this->frand(0,1);
        $bodycompprob=round($bodycompprob,9); 
        $waistnorm = $object1->NORMINV($bodycompprob,$meanwaist,4.9);
       // echo $waistnorm; die;
        
        
         // HIP CALCULATION
        /*If FEMALE then
			MeanHip = 1.903*(BMI) + 55.97 
			HIPNORM = (NORM.INV(RAND(),MeanHip,4.3)*/
        $meanhip = 1.903*($measuredBMI) + 55.97 ;
        $hipprob=$this->frand(0,1);
        $hipprob=round($hipprob,9); 
         //$hipnorm = $object1->NORMINV($hipprob,$meanhip,4.3);
        
        /// HIPNORM = (NORM.INV(RAND(),MeanHip,3.6)              //NEW        
        $hipnorm = $object1->NORMINV($hipprob,$meanhip,3.6);     // new
		
		
        if($measuredpopulation == "Sedentary")
        {
             //$waist = $waistnorm * 1.08; // Old
            $waist = $waistnorm * 1.1; // New
           // $hip = $hipnorm * 1.08;
            $hip = $hipnorm * 1.1;      // new
        }
        elseif($measuredpopulation == "General")
        {
            $waist = $waistnorm * 1.04;
            $hip = $hipnorm * 1.04;
        }
        elseif($measuredpopulation == "Active")
        {
            $waist = $waistnorm * 0.98;
            $hip = $hipnorm * 0.98;
        }
        elseif($measuredpopulation == "Athlete")
        {
            $waist = $waistnorm * 0.94;
            $hip = $hipnorm * 0.94;
        }
		
		
		 // WAIST ADJUSTMENT 12 oct
/*
PLEASE ADD Adjustment because the values are causing problems:
FEMALE WAIST adjustment = 0.0006*AGE^2 + 0.0262*AGE + 1.2
eg., a 40 yr old adjustment = 0.0006*40^2 + 0.0262*40 + 1.2  =  3.2 cm 
Therefore, 3.2 cm would be ADDED to the WAIST value to give the FINAL [adjusted] WAIST value  [used from now on]
*/
            
      $f_waist_adj =   0.0006 * pow($age,2) + 0.0262 * $age + 1.2;
      $waist = $waist + $f_waist_adj;  
		
    }
    
    
    
    if(isset($_SESSION['total_PA'] ) && isset( $_SESSION['vig_total']) )
    {
        $total_PA=$_SESSION['total_PA'];
        $vig_Total=$_SESSION['vig_total'];
    }
    else
    {
        $getdata= $this->fetch_physical_activity_details();
        $total_PA=$getdata[0]->total_PA;
        $vig_Total=$getdata[0]->vig_total;
    }
    
    if($gender  == 'M')
    {
        // -0.0594*(Age) + 0.8699*(BMI) – 0.00191*(TOTAL PA) – 7.5 (OLD)

        /* New Formula
        If MALE then
            MeanTri = -0.0594*(Age) + 0.76*(BMI) – 0.00191*(TOTAL PA) – 7.5 
            TRICEPSNORM = (NORM.INV(RAND(),MeanTri,3.3)
        */		
            $meantri  =-0.0594 * ($measuredage) + 0.76 * ($measuredBMI) - 0.00191 * ($total_PA) - 7.5;
            $prob = $this->frand(0, 1);
            $prob = round($prob, 9);
            $x = $object1->NORMINV($prob, $meantri, 3.3);
            $tricepsnorm =$x; 
        
        
        
           // Biceps MAle
           /*
           If MALE then
            MeanBi = 0.637*(BMI) - 0.002*(2*VIG TOTAL) – 10.1 
            BICEPSNORM = (NORM.INV(RAND(),MeanBi,2.8)
           */
            $meanbiceps = 0.637 * ($measuredBMI) - 0.002 * (2 * $vig_Total) - 10.1 ;
            $prb = $this->frand(0, 1);
            $prb = round($prb, 9);
            $bnorm = $object1->NORMINV($prb, $meanbiceps, 2.8);
            $bicepsnorm =$bnorm; 
        
        
          // subscaluar
          /*
          If MALE then
            MeanSub = 0.931*(BMI) + 0.219*(WAIST) – 0.004*(TOTAL PA) – 23.3
            SUBSCAPULARNORM = (NORM.INV(RAND(),MeanSub,5.9)
          */
            $meansubs  =  0.931 * ($measuredBMI) + 0.219 * ($waist) - 0.004 * ($total_PA) - 23.3;
            $pb = $this->frand(0, 1);
            $pb = round($pb, 9);
            $subs = $object1->NORMINV($pb, $meansubs,5.9);
            $subcabularnorm  = $subs; 
        
        
        
     }
    if($gender  ==  'F')
    {
              //MeanTri = 0.0366*(Age) + 1.0898*(BMI) – 0.0027*(TOTAL PA) – 5.3
			  
			/*(New)
        If FEMALE then
            MeanTri = 0.0366*(Age) + 0.898*(BMI) – 0.0027*(TOTAL PA) – 5.3
            TRICEPSNORM = (NORM.INV(RAND(),MeanTri,4.8) 
        */			 
			  
            $meantri = 0.0366 * ($measuredage) + 0.898 * ($measuredBMI) - 0.0027 * ($total_PA) - 5.3;
            $prob = $this->frand(0, 1);
            $prob = round($prob, 9);
            $x = $object1->NORMINV($prob, $meantri, 4.8);
            $tricepsnorm=$x; 
        
        
        
           // female Biceps
           /*
           If FEMALE then
            MeanBi = 0.71*(BMI) + 0.118*(WAIST) – 0.105*(HIP) – 0.001*(TOTAL PA) – 5.6
            BICEPSNORM = (NORM.INV(RAND(),MeanBi,3.6)
           */
        
            $meanbiceps = 0.71 * ($measuredBMI)+ 0.118 * ($waist) - 0.105 * ($hip) - 0.001 * ($total_PA) - 5.6;
            $prb = $this->frand(0, 1);
            $prb = round($prb, 9);
            $bnorm = $object1->NORMINV($prb, $meanbiceps,3.6);
            $bicepsnorm =$bnorm; 
        
        
         //Female subscapular
           /*(NEW)
          If FEMALE then
                MeanSub = 0.81*(BMI) + 0.495*(WAIST) – 0.42*(HIP) – 0.005*(2*VIG TOTAL) – 1.4
                SUBSCAPULARNORM = (NORM.INV(RAND(),MeanSub,6.5)      
          */
            //$meansubs = 1.181 * ($measuredBMI)+ 0.495 * ($waist) - 0.42 * ($hip) - 0.005 * (2 * $vig_Total) - 1.4;
			
			$meansubs = 0.81 * ($measuredBMI)+ 0.495 * ($waist) - 0.42 * ($hip) - 0.005 * (2 * $vig_Total) - 1.4;
			
            $pb = $this->frand(0, 1);
            $pb = round($pb, 9);
            $subs = $object1->NORMINV($pb, $meansubs,6.5);
            $subcabularnorm  = $subs; 
        
        
        
     }
      if($measuredpopulation=='Sedentary')
      {
         $triceps =$tricepsnorm * 1.2;   // Altered 
         $biceps =$bicepsnorm * 1.2;     // ALtered
         $subscapular  =$subcabularnorm * 1.2;  // ALtered
      }
     if($measuredpopulation=='General')
      {
         $triceps =$tricepsnorm * 1.1;   // Altered
         $biceps =$bicepsnorm * 1.1;    // Altered
         $subscapular =$subcabularnorm * 1.11;  // ALtered   
      }
      if($measuredpopulation=='Active')
      {
          $triceps=$tricepsnorm * 0.9; 
          $biceps=$bicepsnorm * 0.9; 
          $subscapular=$subcabularnorm * 0.9; 
      }
      if($measuredpopulation=='Athlete')
      {
         $triceps=$tricepsnorm * 0.7; 
         $biceps=$bicepsnorm * 0.7; 
         $subscapular=$subcabularnorm * 0.7; 
      }
       
       if($triceps < 4)
       {
        $triceps=4;   
       } 
    
       if($biceps < 3)
       {
        $biceps=3;   
       } 
    
       if($subscapular < 5)
       {
        $subscapular=5;   
       }
    
    // Skinfolds Calculate
    $skinfold_summ=$triceps+$biceps+$subscapular;
    $fieldData[0]->skinfold_summ=round($skinfold_summ,2);
    $_SESSION['skinfold_summ']=round($skinfold_summ,2);
    // waist hip ratio   
    $whr=abs($waist/$hip);
  
    
    
    // kritika
    // ANOTHER BODY COMPOSITION MODULE VARIABLES
    //ILIAC CREST skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.46,2.5)
   if($gender  == 'M')
    {
      $object1 = new PHPExcel_Calculation_Statistical(); 
      // Iliac Crest
      $iliaccrest=$this->frand(0,1);
      $iliaccrest=round($iliaccrest,9);
      $Iliac_crest_skinfold = $object1->NORMINV($iliaccrest,$skinfold_summ*0.46,2.5);
      if($Iliac_crest_skinfold < 2.9)
      {
          $Iliac_crest_skinfold=2.9;
      }
        
      // SUPRASPINALE skinfold   
        //SUPRASPINALE skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.30,2)
      $supraspinale=$this->frand(0,1);
      $supraspinale=round($supraspinale,9);
      $Supraspinale_skinfold=  $object1->NORMINV($supraspinale,$skinfold_summ*0.30,2);
        if($Supraspinale_skinfold < 2.2)
        {
            $Supraspinale_skinfold=2.2;
        }
                       
    //Abdominal Skinfold
        //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.50,2.5)
		
		/*New
        ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.50,2.5) +0.05*age       
        */
      $abdominal=$this->frand(0,1);
      $abdominal=round($abdominal,9);   
      $Abdominal_skinfold = $object1->NORMINV($abdominal,$skinfold_summ*0.50,2.5)+0.05 * $measuredage;        // ALtered 
        if($Abdominal_skinfold < 4)
        {
            $Abdominal_skinfold=4;
        }
            
    //Front THIGH
        //FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.4,2.5)
		
		// //FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.45,2.3)      // NEW
      $front_thigh=$this->frand(0,1);
      $front_thigh=round($front_thigh,9);                  
      //$Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.4,2.5);
      $Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.45,2.3);
        if($Front_thigh_skinfold  < 3.2)
        {
            $Front_thigh_skinfold=3.2;
        }
                              
   // MEdialCalf Skinfold
        //MEDIAL CALF skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $medial_calf=$this->frand(0,1);
      $medial_calf=round($medial_calf,9);                        
      $Medial_calf_skinfold=  $object1->NORMINV($medial_calf,$skinfold_summ*0.25,2); 
         if($Medial_calf_skinfold < 2.9)
         {
             $Medial_calf_skinfold=2.9;
         }
                       
  // Mid-Axilla Skinfold  
        //MID-AXILLA skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $midaxilla=$this->frand(0,1);
      $midaxilla=round($midaxilla,9);                        
      $Midaxilla_skinfold=  $object1->NORMINV($midaxilla,$skinfold_summ*0.25,2);
        if($Midaxilla_skinfold < 2.6)
        {
            $Midaxilla_skinfold=2.6;
        }
    }
    
 else // Female Case
 {
      $object1 = new PHPExcel_Calculation_Statistical(); 
      // Iliac Crest
     // ILIAC CREST skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.40,3)
      $iliaccrest=$this->frand(0,1);
      $iliaccrest=round($iliaccrest,9);
      $Iliac_crest_skinfold = $object1->NORMINV($iliaccrest,$skinfold_summ*0.40,3);
      if($Iliac_crest_skinfold < 4)
      {
          $Iliac_crest_skinfold=4;
      }
        
      // SUPRASPINALE skinfold   
        //SUPRASPINALE skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.28,3)
      $supraspinale=$this->frand(0,1);
      $supraspinale=round($supraspinale,9);
      $Supraspinale_skinfold=  $object1->NORMINV($supraspinale,$skinfold_summ*0.28,3);
        if($Supraspinale_skinfold < 3.2)
        {
            $Supraspinale_skinfold=3.2;
        }
                       
    //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.43,3.5)     (OLD)
     
     //ABDOMINAL skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.43,3.5)+0.05*age  (NEW)
      $abdominal=$this->frand(0,1);
      $abdominal=round($abdominal,9);   
      $Abdominal_skinfold = $object1->NORMINV($abdominal,$skinfold_summ*0.43,3.5) +0.05 * $measuredage; 
        if($Abdominal_skinfold < 5)
        {
            $Abdominal_skinfold=5;
        }
            
     //Front THIGH
     // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.8,3.5)     (OLD)
     
     // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.65,3)  (NEW)
	 
	 // FRONT THIGH skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.7,2.5)  // new
	 
     
      $front_thigh=$this->frand(0,1);
      $front_thigh=round($front_thigh,9);                  
      //$Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.65,3);
      $Front_thigh_skinfold = $object1->NORMINV($front_thigh,$skinfold_summ*0.7,2.5);
        if($Front_thigh_skinfold  < 5.2)
        {
            $Front_thigh_skinfold=5.2;
        }
                              
   // MEdialCalf Skinfold
     // MEDIAL CALF skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.45,3)
      $medial_calf=$this->frand(0,1);
      $medial_calf=round($medial_calf,9);                        
      $Medial_calf_skinfold=  $object1->NORMINV($medial_calf,$skinfold_summ*0.45,3); 
         if($Medial_calf_skinfold < 3.9)
         {
             $Medial_calf_skinfold=3.9;
         }
                       
  // Mid-Axilla Skinfold   
     //  MID-AXILLA skinfold = (NORM.INV(RAND(),Sum3skinfolds*0.25,2)
      $midaxilla=$this->frand(0,1);
      $midaxilla=round($midaxilla,9);                        
      $Midaxilla_skinfold=  $object1->NORMINV($midaxilla,$skinfold_summ*0.25,2);
        if($Midaxilla_skinfold < 3.6)
        {
            $Midaxilla_skinfold=3.6;
        }
    
    
    
 }
    // HEAD CALCULATION
/*    EAD
If MALE then:
HEADMEAN = 50.02 + 0.087 * MASS
HEAD = (NORM.INV(RAND(),HEADMEAN,0.7)

*/
    
if($gender  == 'M')
{
         $headmean= 50.02 + 0.087 * $mass;
         $headprob=$this->frand(0,1);
         $headprob=round($headprob,9); 
         $Head = $object1->NORMINV($headprob,$headmean,0.7);
      
   	    /*  NECK
            If MALE then:
            NECKMEAN = 22.09 + 0.201 * MASS
            NECK = (NORM.INV(RAND(),NECKMEAN,0.9)
        */
         $neckmean= 22.09 + 0.201 * $mass;
         $neckprob=$this->frand(0,1);
         $neckprob=round($neckprob,9);
         $neck = $object1->NORMINV($neckprob,$neckmean,0.9);
        
        
        
        
        /*
ARM [relaxed]
If MALE then:
ARMMEAN = 1.121 * BMI + 2.8
ARMR [relaxed] = (NORM.INV(RAND(),ARMMEAN,2.0)

*/        
        $armmean=1.121 * $bmi + 2.8;
        $armprob=$this->frand(0,1);
        $armprob=round($armprob,9);
        $relaxed_arm=$object1->NORMINV($armprob,$armmean,2.0);
         
/*    ARM [flexed]
If MALE then:
ARMFMEAN = 1.041 * ARMR [relaxed] + 0.897
ARMF [flexed] = (NORM.INV(RAND(),ARMFMEAN,1.1)
*/        
        $armmean_fl=1.041 * $relaxed_arm + 0.897;
        $armprob_fl=$this->frand(0,1);
        $armprob_fl=round($armprob_fl,9);
        $relaxed_arm_fl=$object1->NORMINV($armprob_fl,$armmean_fl,1.1);
        
/*
FOREARM [max]
If MALE then:
FOREARMMEAN = 14.28 + 0.182 * MASS
FOREARM [max] = (NORM.INV(RAND(),FOREARMMEAN,0.7)
*/        
        
        $forearmmean = 14.28 + 0.182 * $mass;
        $forearmprob=$this->frand(0,1);
        $forearmprob=round($forearmprob,9);
        $forearm_max = $object1->NORMINV($forearmprob,$forearmmean,0.7);        

        
/*    WRIST [distal styloids]
If MALE then:
WRISTMEAN = 11.256 + 0.083 * MASS
WRIST = (NORM.INV(RAND(),WRISTMEAN,0.5)
*/        
 
        $wristmean=11.256 + 0.083 * $mass;
        $wristprob=$this->frand(0,1);
        $wristprob=round($wristprob,9);
        $wrist = $object1->NORMINV($wristprob,$wristmean,0.5);
 /*CHEST
If MALE then:
CHESTMEAN = 3.091 * BMI + 19.194
CHEST = (NORM.INV(RAND(),CHESTMEAN,3.5)
*/       
        $chestmean=3.091 * $bmi + 19.194;
        $chestprob=$this->frand(0,1);
        $chestprob=round($chestprob,9);
        $chest = $object1->NORMINV($chestprob,$chestmean,3.5);        

/*    THIGH [1 cm distal]
If MALE then:
THIGHMEAN = 28.993 + 0.38 * MASS
THIGH = (NORM.INV(RAND(),THIGHMEAN,1.8)
*/

/*
THIGHMEAN = 31.6+0.3452*MASS
*/
        //$thighmean_distal = 28.993 + 0.38 * $mass;
        $thighmean_distal = 31.6 + 0.3452 * $mass;            // new
        $thighprob_distal=$this->frand(0,1);
        $thighprob_distal=round($thighprob_distal,9);
        $thigh_distal = $object1->NORMINV($thighprob_distal,$thighmean_distal,1.8);
 
        
/*  THIGH [mid]
If MALE then:
THIGHMMEAN = 3.57 + 0.839 * THIGH
THIGH mid = (NORM.INV(RAND(),THIGHMMEAN,1.5)
 */ 
        
        
        $thighmean_mid = 3.57 + 0.839 * $thigh_distal;
        $thighprob_mid=$this->frand(0,1);
        $thighprob_mid=round($thighprob_mid,9);
        $thigh_mid = $object1->NORMINV($thighprob_mid,$thighmean_mid,1.5);      
        
 /*    CALF
If MALE then:
CALFMEAN = 21.138 + 0.22 * MASS
CALF = (NORM.INV(RAND(),CALFMEAN,1.0)
*/
        
        
    $calfmean=21.138 + 0.22 * $mass;
    $calfprob=$this->frand(0,1);
    $calfprob=round($calfprob,9);
    $calf = $object1->NORMINV($calfprob,$calfmean,1.0);    
        
              
/*ANKLE
If MALE then:
ANKLEMEAN = 15.11 + 0.11 * MASS
ANKLE = (NORM.INV(RAND(),ANKLEMEAN,0.8)
*/    
        
    $anklemean= 15.11 + 0.11 * $mass;
    $ankleprob=$this->frand(0,1);
    $ankleprob=round($ankleprob,9);
    $ankle=$object1->NORMINV($ankleprob,$anklemean,0.8);
        
              

// Lengths and breadths
/*   SITTING HEIGHT
If MALE then:
SITHEIGHT% = -0.0008*HEIGHT + 0.6758
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.006)
If sub-population = ‘Athlete’ then:
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.006,0.003)

CHECK:
If MALE then
If SITTING HEIGHT / HEIGHT < 0.48 then SITTING HEIGHT = HEIGHT * 0.48


If FEMALE then:
SITHEIGHT% = -0.0010*HEIGHT + 0.7263
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.005)
If sub-population = ‘Athlete’ then:
SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.004,0.002)


CHECK:
If FEMALE then
If SITTING HEIGHT / HEIGHT < 0.49 then SITTING HEIGHT = HEIGHT * 0.49*/ 
    
    
        $sitheight_per = - 0.0008 * $height + 0.6758;
        $sitprob=$this->frand(0,1);
        $sitprob=round($sitprob,9);
        $sittingheight=$object1->NORMINV($sitprob,$sitheight_per,0.006);
        
        if($measuredpopulation == 'Athlete')
        {
            $sittingheight=$object1->NORMINV($sitprob,$sitheight_per - 0.006,0.003);
            
        }
       if(($sittingheight / $height) < 0.48)
            {
                $sittingheight =  $height * 0.48;
            } 
        
 /*    
ACROMIALE-RADIALE length
If MALE then:
ARmean = 0.17 * HEIGHT + 0.021 * MASS + 1.625
AR = (NORM.INV(RAND(),ARmean,0.9)
*/           
        
        $acromialemean= 0.17 * $height + 0.021 * $mass + 1.625;
        $acroprob=$this->frand(0,1);
        $acroprob=round($acroprob,9);
        $AR = $object1->NORMINV($acroprob,$acromialemean,0.9);
       
        
  /*RADIALE-STYL length
If MALE then:
RSmean = 0.2156 * AR - 0.0721 * AGE + 0.0819 * HEIGHT + 0.0329 * MASS + 3.415
RS = (NORM.INV(RAND(),RSmean,0.8)
 */       
        
         $rsmean = 0.2156 * $AR - 0.0721 * $age + 0.0819 * $height + 0.0329 * $mass + 3.415;
        $rsprob=$this->frand(0,1);
        $rsprob=round($rsprob,9);
        $RS = $object1->NORMINV($rsprob,$rsmean,0.8);
        
        
 /*    
MIDSTYL-DACTY length
If MALE then:
MDmean = 0.1515 * AR + 0.01294 * RS + 0.0471 * HEIGHT + 0.0304 * AGE + 1.463
MD = (NORM.INV(RAND(),MDmean,0.65)
 */ 
        $MDmean=0.1515 * $AR + 0.01294 * $RS + 0.0471 * $height + 0.0304 * $age + 1.463;
        $mdprob=$this->frand(0,1);
        $mdprob=round($mdprob,9);
        $MD = $object1->NORMINV($mdprob,$MDmean,0.65);
  
}

    
    
    
//if($gender == 'F')
else   // in case of female    
{
/*
If FEMALE then:
HEADMEAN = 50.22 + 0.066 * MASS
HEAD = (NORM.INV(RAND(),HEADMEAN,0.7)
*/
         $headmean= 50.22 + 0.066 * $mass;
         $headprob=$this->frand(0,1);
         $headprob=round($headprob,9); 
         $Head = $object1->NORMINV($headprob,$headmean,0.7);
        
/*
If FEMALE then:
NECKMEAN = 23.26 + 0.146 * MASS
NECK = (NORM.INV(RAND(),NECKMEAN,0.75)
*/
        
         $neckmean=  23.26 + 0.146 * $mass;
         $neckprob=$this->frand(0,1);
         $neckprob=round($neckprob,9);
         $neck = $object1->NORMINV($neckprob,$neckmean,0.75);
               
        
/* 
If FEMALE then:
ARMMEAN = 6.684 * BMI + 0.917
ARMR [relaxed] = (NORM.INV(RAND(),ARMMEAN,1.3)


Relaxed arm (New Formula)
-6.97 + (2.208*BMI) - (0.025*BMI^2)
*/

//$armmean=6.684 * $bmi + 0.917;
         $armmean=  -6.97 + (2.208 *$bmi) - (0.025 * pow($bmi,2) ) ;
         $armprob=$this->frand(0,1);
         $armprob=round($armprob,9);
         $relaxed_arm=$object1->NORMINV($armprob,$armmean,1.3);  
        
        
/*If FEMALE then:
ARMFMEAN = 0.967 * ARMR [relaxed] + 2.034
ARMF [flexed] = (NORM.INV(RAND(),ARMFMEAN,0.8)*/
    
        $armmean_fl=0.967 * $relaxed_arm + 2.034;
        $armprob_fl=$this->frand(0,1);
        $armprob_fl=round($armprob_fl,9);
        $relaxed_arm_fl=$object1->NORMINV($armprob_fl,$armmean_fl,0.8);
        
		/*If FEMALE then:
FOREARMMEAN = 15.03 + 0.153 * MASS
FOREARM [max] = (NORM.INV(RAND(),FOREARMMEAN,0.6) */
        
        $forearmmean = 15.03 + 0.153 * $mass;
        $forearmprob=$this->frand(0,1);
        $forearmprob=round($forearmprob,9);
        $forearm_max = $object1->NORMINV($forearmprob,$forearmmean,0.6);
        
		
/*If FEMALE then:
WRISTMEAN = 11.043 + 0.077 * MASS
WRIST = (NORM.INV(RAND(),WRISTMEAN,0.4)		*/
		
        $wristmean=11.043 + 0.077 * $mass;
        $wristprob=$this->frand(0,1);
        $wristprob=round($wristprob,9);
        $wrist = $object1->NORMINV($wristprob,$wristmean,0.4);
       
 /*If FEMALE then:
CHESTMEAN = 2.354 * BMI + 35.484
CHEST = (NORM.INV(RAND(),CHESTMEAN,3)*/       
        
        $chestmean=2.354 * $bmi + 35.484;
        $chestprob=$this->frand(0,1);
        $chestprob=round($chestprob,9);
        $chest = $object1->NORMINV($chestprob,$chestmean,3);
        
        
 /*THIGHMEAN = 26.643 + 0.5 * MASS
 THIGH = (NORM.INV(RAND(),THIGHMEAN,1.6)*/
 /*  (NEW)
THIGHMEAN = 28.3 + 0.4735*MASS
*/
 
        // $thighmean_distal = 26.643 + 0.5 * $mass;
        $thighmean_distal = 28.3 + 0.4735 * $mass;       // NEW
        $thighprob_distal=$this->frand(0,1);
        $thighprob_distal=round($thighprob_distal,9);
        $thigh_distal = $object1->NORMINV($thighprob_distal,$thighmean_distal,1.6);
        
        /*
         THIGHMMEAN = 2.89 + 0.835 * THIGH
         THIGH mid = (NORM.INV(RAND(),THIGHMMEAN,1.4) 
        */
        $thighmean_mid = 2.89 + 0.835 * $thigh_distal;
        $thighprob_mid=$this->frand(0,1);
        $thighprob_mid=round($thighprob_mid,9);
        $thigh_mid = $object1->NORMINV($thighprob_mid,$thighmean_mid,1.4);
        
        
/*  If FEMALE then:
CALFMEAN = 19.66 + 0.263 * MASS
CALF = (NORM.INV(RAND(),CALFMEAN,0.9)*/
        $calfmean=19.66 + 0.263 * $mass;
        $calfprob=$this->frand(0,1);
        $calfprob=round($calfprob,9);
        $calf = $object1->NORMINV($calfprob,$calfmean,0.9);
        
 /*
If FEMALE then:
ANKLEMEAN = 14.27 + 0.123 * MASS
ANKLE = (NORM.INV(RAND(),ANKLEMEAN,0.65)
*/ 
        
        $anklemean= 14.27 + 0.123 * $mass;
        $ankleprob=$this->frand(0,1);
        $ankleprob=round($ankleprob,9);
        $ankle=$object1->NORMINV($ankleprob,$anklemean,0.65);
        
        
        
        /*
        
        If FEMALE then:
        SITHEIGHT% = -0.0010*HEIGHT + 0.7263
        SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT%,0.005)
        If sub-population = ‘Athlete’ then:
        SITTING HEIGHT = (NORM.INV(RAND(),SITHEIGHT% - 0.004,0.002)


        CHECK:
        If FEMALE then
        If SITTING HEIGHT / HEIGHT < 0.49 then SITTING HEIGHT = HEIGHT * 0.49
        */
        $sitheight_per = -0.0010 * $height + 0.7263;
        $sitprob=$this->frand(0,1);
        $sitprob=round($sitprob,9);
        $sittingheight=$object1->NORMINV($sitprob,$sitheight_per,0.005);
        
                if($measuredpopulation == 'Athlete')
                {
                    $sittingheight=$object1->NORMINV($sitprob,$sitheight_per - 0.004,0.002);
                    
                }
         if(($sittingheight / $height) < 0.49)
                    {
                        $sittingheight =  $height * 0.49;
                    }
        
        /*
        If FEMALE then:
            ARmean = 0.181 * HEIGHT + 0.071 * BMI - 0.63
            AR = (NORM.INV(RAND(),ARmean,0.8)
        */
        
        $acromialemean= 0.181 * $height + 0.071 * $bmi - 0.63;
        $acroprob=$this->frand(0,1);
        $acroprob=round($acroprob,9);
        $AR = $object1->NORMINV($acroprob,$acromialemean,0.8);
        
/*   If FEMALE then:
RSmean = 0.327 * AR - 0.0752 * AGE + 0.835 * HEIGHT + 1.3472
RS = (NORM.INV(RAND(),RSmean,0.9)*/
 
 /*(NEW)
If FEMALE then:
RSmean = 0.327 * AR - 0.0752 * AGE + 0.0835 * HEIGHT + 1.3472
*/
		
        $rsmean = 0.327 * $AR - 0.0752 * $age + 0.0835 * $height + 1.3472;
        $rsprob=$this->frand(0,1);
        $rsprob=round($rsprob,9);
        $RS = $object1->NORMINV($rsprob,$rsmean,0.9); 
		
		
        
 /*If FEMALE then:
MDmean = 0.231 * AR + 0.0544 * HEIGHT + 0.0295 * AGE + 0.8369 
MD = (NORM.INV(RAND(),MDmean,0.6)*/       
        $MDmean=0.231 * $AR + 0.0544 * $height + 0.0295 * $age + 0.8369; 
        $mdprob=$this->frand(0,1);
        $mdprob=round($mdprob,9);
        $MD = $object1->NORMINV($mdprob,$MDmean,0.6); 
        
        
    }


    
/*        [both MALE and FEMALE]:
ARM LENGTH = AR + RS + MD

If ARM LENGTH / HEIGHT < 0.41 then 
AR = AR * 1.03
RS = RS * 1.03
MD = MD * 1.03

If ARM LENGTH / HEIGHT > 0.47 then 
AR = AR * 0.98
RS = RS * 0.98
MD = MD * 0.98*/
        
    $armlength=$AR+$RS+$MD;
    if(($armlength/$height) < 0.41)
    {
        $AR = $AR * 1.03;
        $RS = $RS * 1.03;
        $MD = $MD * 1.03;
    }
    
    if(($armlength/$height)  > 0.47){
        $AR = $AR * 0.98;
        $RS = $RS * 0.98;
        $MD = $MD * 0.98;
    }
       
   
    
    
/*    BRACHIAL INDEX = RS / AR
If MALE

If BRACHIAL INDEX < 0.7 then 
AR = RS * 1.429

If BRACHIAL INDEX > 0.8 then 
AR = RS * 1.25

If FEMALE

If BRACHIAL INDEX < 0.71 then 
AR = RS * 1.429

If BRACHIAL INDEX > 0.78 then 
AR = RS * 1.25*/
    
  $brachial_index=$RS/$AR;
  if($gender == 'M' )
  {
     if($brachial_index < 0.7)
     {
        $AR = $RS * 1.429;
     }
     if($brachial_index > 0.8) 
     {
         $AR = $RS * 1.25;
     }
  }
 if($gender == 'F' )
 {
         if($brachial_index < 0.71)
         {
            $AR = $RS * 1.429;
         }
         if($brachial_index > 0.78) 
         {
             $AR = $RS * 1.25;
         } 
 }
    
    $fieldData[0]->radStyl=round($RS,2);
    $_SESSION['radStyl']=round($RS,2);
    
    $fieldData[0]->midStylDact=round($MD,2);    
    $_SESSION['midStylDact']=round($MD,2);   
  
    $fieldData[0]->acRad=round($AR,2);    
    $_SESSION['acRad']=round($AR,2);   
    
    
    
    
    
/*
ILIOSPINALE height
If MALE then:
ILmean = -1.184 * AR - 0.648 * SITTING HEIGHT + 1.095 * HEIGHT - 2.316 
IL = (NORM.INV(RAND(),ILmean,6)
*/

// MALE CASE


    if( $gender == 'M')
    {
    $ILmean= -1.184 * $AR - 0.648 * $sittingheight + 1.095 * $height - 2.316;   
    $ILprob=$this->frand(0,1);
    $ILprob=round($ILprob,9);    
    $IL = $object1->NORMINV($ILprob,$ILmean,6);
        
/*If MALE then:
TROCHmean = 0.883 * IL + 2.211 
TROCH = (NORM.INV(RAND(),TROCHmean,3.3)
 */           
  
        $TROCHmean = 0.883 * $IL + 2.211;
        $TROCHprob=$this->frand(0,1);
        $TROCHprob=round($TROCHprob,9); 
        $TROCH = $object1->NORMINV($TROCHprob,$TROCHmean,3.3);
 
//TROCHANT-TIBIALE height
/*If MALE then:
TTmean = 0.149 * RS + 0.212 * HEIGHT + 2.6 
TT = (NORM.INV(RAND(),TTmean,1.4)
 */  
        
        $TTmean=0.149 * $RS + 0.212 * $height + 2.6 ; 
        $TTprob=$this->frand(0,1);
        $TTprob=round($TTprob,9);
        $TT = $object1->NORMINV($TTprob,$TTmean,1.4);        
    }
    else // FEMALE CASE
    {
		 /* If FEMALE then:
ILmean = -0.063 * AR - 0.021 * SITTING HEIGHT + 0.029 * AGE + 0.071 
IL = (NORM.INV(RAND(),ILmean,4)*/
	    /*
        (NEW)
        If FEMALE then:
            ILmean = -21.96 – 0.381 * MASS + 0.786 * HEIGHT
            IL = (NORM.INV(RAND(),ILmean,4)
        */
        //$ILmean= -0.063 * $AR - 0.021 * $sittingheight + 0.029 * $age + 0.071 ;   (OLD)  
       
        $ILmean= -21.96 - 0.381 * $measuredmass + 0.786 * $height;  //(ALtered)	
        $ILprob=$this->frand(0,1);
        $ILprob=round($ILprob,9);    
        $IL = $object1->NORMINV($ILprob,$ILmean,4);
        
        /* If FEMALE then:
TROCHmean = 0.874 * IL + 4.252 
TROCH = (NORM.INV(RAND(),TROCHmean,3.8) */
        $TROCHmean = 0.874 * $IL + 4.252;
        $TROCHprob=$this->frand(0,1);
        $TROCHprob=round($TROCHprob,9); 
        $TROCH = $object1->NORMINV($TROCHprob,$TROCHmean,3.8);
        
   /* If FEMALE then:
TTmean = 0.2741 * HEIGHT – 3.3626 
TT = (NORM.INV(RAND(),TTmean,1.8) */     
        $TTmean= 0.2741 * $height - 3.3626;
        $TTprob=$this->frand(0,1);
        $TTprob=round($TTprob,9);
        $TT = $object1->NORMINV($TTprob,$TTmean,1.8);
    }
    
    $fieldData[0]->iliospinale=round($IL,2); 
    $_SESSION['iliospinale']=round($IL,2); 
    
    $fieldData[0]->troch=round($TROCH,2); 
    $_SESSION['troch']=round($TROCH,2); 
    
    $fieldData[0]->trochTib=round($TT,2);    
    $_SESSION['trochTib']=round($TT,2);    
    
 //TIBIALE LATERALE height
/*If MALE then:
TLmean = 0.2166 * RS + 0.2982 * AR + 0.1995 * HEIGHT – 0.1167 * AGE – 1.13 
TL = (NORM.INV(RAND(),TLmean,1.5)

If FEMALE then:
TLmean = 0.2927 * RS + 0.331 * AR + 0.1675 * HEIGHT – 0.0884 * AGE – 0.19 
TL = (NORM.INV(RAND(),TLmean,1.4) */
    
   /* echo $AR; echo "<br>";
    echo $height; echo "<br>";
    echo $age; echo "<br>";*/
    
    if($gender == 'M')
    {
        $TLmean = 0.2166 * $RS + 0.2982 * $AR + 0.1995 * $height - 0.1167 * $age - 1.13 ;
        $TLprob=$this->frand(0,1);
        $TLprob=round($TLprob,9);
        $TL = $object1->NORMINV($TLprob,$TLmean,1.5);
    }
    if($gender == 'F')
    {
        $TLmean = 0.2927 * $RS + 0.331 * $AR + 0.1675 * $height - 0.0884 * $age - 0.19 ;
        $TLprob=$this->frand(0,1);
        $TLprob=round($TLprob,9);
        $TL = $object1->NORMINV($TLprob,$TLmean,1.4); 
    }
 
    $fieldData[0]->tibLat=round($TL,2);   
    $_SESSION['tibLat']=round($TL,2);  
	
//TIB MED-SPHY TIB height
/*If MALE then:
TMSTmean = 0.1009 * TL + 0.1146 * TT + 0.2942 * RS + 0.1536 * AR – 0.0625 * AGE + 0.1129 * HEIGHT – 1.55 
TMST = (NORM.INV(RAND(),TMSTmean,1.1)

If FEMALE then:
TMSTmean = 0.1933 * TL + 0.0821 * TT + 0.1322 * RS + 0.3407 * AR + 0.0773 * HEIGHT – 1.679 
TMST = (NORM.INV(RAND(),TMSTmean,1.15) */   
    
    
    if($gender == 'M')
    {
        $TMSTmean = 0.1009 * $TL + 0.1146 * $TT + 0.2942 * $RS + 0.1536 * $AR - 0.0625 * $age + 0.1129 * $height - 1.55;
        $TMSprob=$this->frand(0,1);
        $TMSprob=round($TMSprob,9);
        $TMST = $object1->NORMINV($TMSprob,$TMSTmean,1.1);
    }
     if($gender == 'F')
    {
        $TMSTmean = 0.1933 * $TL + 0.0821 * $TT + 0.1322 * $RS + 0.3407 * $AR + 0.0773 * $height - 1.679 ;
        $TMSprob=$this->frand(0,1);
        $TMSprob=round($TMSprob,9);
        $TMST = $object1->NORMINV($TMSprob,$TMSTmean,1.15);
    }
    
 
    $fieldData[0]->tibMed=round($TMST,2);   
    $_SESSION['tibMed']=round($TMST,2); 
        
//BIACROMIAL breadth
/*If MALE then:
BIAmean = 0.1671 * RS + 0.0475 * AGE + 0.0905 * HEIGHT + 0.1137 * MASS + 9.55 
BIA = (NORM.INV(RAND(),BIAmean,1.0)

If FEMALE then:
BIAmean = 0.1335 * SITTING HEIGHT + 0.1548 * RS + 0.1815 * AR + 0.1202 * AGE + 0.1031 * MASS + 6.67 
BIA = (NORM.INV(RAND(),BIAmean,1.1)*/  
       
    if($gender == 'M')
    {
       $BIAmean= 0.1671 * $RS + 0.0475 * $age + 0.0905 * $height + 0.1137 * $mass + 9.55;
       $BIAprob=$this->frand(0,1);
       $BIAprob=round($BIAprob,9);  
       $BIA = $object1->NORMINV($BIAprob,$BIAmean,1.0);    
    }
     if($gender == 'F')
    {
       $BIAmean= 0.1335 * $sittingheight + 0.1548 * $RS + 0.1815 * $AR + 0.1202 * $age + 0.1031 * $mass + 6.67;
       $BIAprob=$this->frand(0,1);
       $BIAprob=round($BIAprob,9);  
       $BIA = $object1->NORMINV($BIAprob,$BIAmean,1.1);  
    }
    

    $fieldData[0]->biac=round($BIA,2);   
    $_SESSION['biac']=round($BIA,2);               $biac=round($BIA,2); 
/* BIDELTOID breadth
If MALE then:
BIDEL = (NORM.INV(RAND(),BIA + 4,1.0)

If FEMALE then:
BIDEL = (NORM.INV(RAND(),BIA + 3,0.8)

CHECK :

If BIDEL < BIA + 0.6 then
BIDEL = BIA + 0.6  */ 
    
    if($gender == 'M')
    {
        $BIDELprob=$this->frand(0,1);
        $BIDELprob=round($BIDELprob,9);
        $BIDEL = $object1->NORMINV($BIDELprob,$BIA + 4,1.0);
    }
     if($gender == 'F')
    {
        $BIDELprob=$this->frand(0,1);
        $BIDELprob=round($BIDELprob,9);
        $BIDEL = $object1->NORMINV($BIDELprob,$BIA + 3,0.8);
    }
    if($BIDEL < $BIA + 0.6)
    {
        $BIDEL =$BIA + 0.6;
    }
    

    $fieldData[0]->bideltoid=round($BIDEL,2);   
    $_SESSION['bideltoid']=round($BIDEL,2); 
    
 /*
BIILIOCRISTAL breadth
If MALE then:
BIILmean = 0.2053 * BIA + 0.0929 * RS + 0.1067 * AR - 0.0368 * HEIGHT + 0.114 * MASS + 12 
BIIL = (NORM.INV(RAND(),BIILmean,0.85)

If FEMALE then:
BIILmean = 0.1102 * BMI + 0.3379 * BIA + 0.04 * CHEST + 0.0512 * HEIGHT + 0.0621 * AGE – 0.71
BIIL = (NORM.INV(RAND(),BIILmean,0.85)*/
    
    if($gender == 'M')
    {
        $BIILmean=0.2053 * $BIA + 0.0929 * $RS + 0.1067 * $AR - 0.0368 * $height + 0.114 * $mass + 12;
        $BIILprob=$this->frand(0,1);
        $BIILprob=round($BIILprob,9);
        $BIIL = $object1->NORMINV($BIILprob,$BIILmean,0.85);
    }
     if($gender == 'F')
    {
        $BIILmean=0.1102 * $bmi + 0.3379 * $BIA + 0.04 * $chest + 0.0512 * $height + 0.0621 * $age - 0.71;
        $BIILprob=$this->frand(0,1);
        $BIILprob=round($BIILprob,9);
        $BIIL = $object1->NORMINV($BIILprob,$BIILmean,0.85);
    }

    $fieldData[0]->billio=round($BIIL,2);   
    $_SESSION['billio']=round($BIIL,2);    $billio=round($BIIL,2);
    
/*    BITROCHANTERIC breadth
If MALE then:
BITROCH = (NORM.INV(RAND(),BIIL * 1.08,1.2)

If FEMALE then:
BITROCH = (NORM.INV(RAND(),BIIL * 1.1,2)*/
    
    if($gender == 'M')
    {
        $BITROCHprob=$this->frand(0,1);
        $BITROCHprob=round($BITROCHprob,9);
        $BITROCH = $object1->NORMINV($BITROCHprob,$BIIL * 1.08,1.2);
    }
     if($gender == 'F')
    {
        $BITROCHprob=$this->frand(0,1);
        $BITROCHprob=round($BITROCHprob,9);
        $BITROCH = $object1->NORMINV($BITROCHprob,$BIIL * 1.1,2);
    }
    

     $fieldData[0]->bitrochanteric=round($BITROCH,2); 
     $_SESSION['bitrochanteric']=round($BITROCH,2); 
    
/*FOOT LENGTH
If MALE then:
FOOTmean = 0.058 * BIIL + 0.0574 * BIA + 0.1653 * AR + 0.0668 * HEIGHT + 5.34
FOOT = (NORM.INV(RAND(),FOOTmean,0.7)

If FEMALE then:
FOOTmean = -0.0684 * BMI + 0.0485 * SITTING HEIGHT + 0.1241 * BIIL + 0.1151 * AR + 0.0965 * RS + 6.12
FOOT = (NORM.INV(RAND(),FOOTmean,0.85)    */
    
    
    if($gender == 'M')
    {
        $FOOTmean = 0.058 * $BIIL + 0.0574 * $BIA + 0.1653 * $AR + 0.0668 * $height + 5.34;
        $FOOTprob=$this->frand(0,1);
        $FOOTprob=round($FOOTprob,9);
        $FOOT = $object1->NORMINV($FOOTprob,$FOOTmean,0.7);
    }
    if($gender == 'F')
    {
        $FOOTmean = -0.0684 * $bmi + 0.0485 * $sittingheight + 0.1241 * $BIIL + 0.1151 * $AR + 0.0965 * $RS + 6.12;
        $FOOTprob=$this->frand(0,1);
        $FOOTprob=round($FOOTprob,9);
        $FOOT = $object1->NORMINV($FOOTprob,$FOOTmean,0.7);
    }

    $fieldData[0]->foot=round($FOOT,2); 
    $_SESSION['foot']=round($FOOT,2); 
    
/*
    TRANSVERSE CHEST
If MALE then:
TCmean = 0.0291 * SITTING HEIGHT + 0.3399 * BIIL + 0.2428 * BIA + 0.0953 * CHEST + 0.0295 * AGE – 1.94
TC = (NORM.INV(RAND(),TCmean,0.8)

If FEMALE then:
TCmean = 0.2651 * BIIL + 0.4214 * BIA + 0.0508 * WAIST + 0.19
TC = (NORM.INV(RAND(),TCmean,1.1)

*/
    
    
if($gender == 'M')    
{
    $TCmean = 0.0291 * $sittingheight + 0.3399 * $BIIL + 0.2428 * $BIA + 0.0953 * $chest + 0.0295 * $age - 1.94;
    $TCprob=$this->frand(0,1);
    $TCprob=round($TCprob,9);
    $TC = $object1->NORMINV($TCprob,$TCmean,0.8);
}
else
 {
    $TCmean = 0.2651 * $BIIL + 0.4214 * $BIA + 0.0508 * $waist + 0.19;
    $TCprob=$this->frand(0,1);
    $TCprob=round($TCprob,9);
    $TC = $object1->NORMINV($TCprob,$TCmean,1.1);
 }
    

    $fieldData[0]->trChest=round($TC,2); 
    $_SESSION['trChest']=round($TC,2);         $trChest=round($TC,2); 
    
/*    A-P CHEST
If MALE then:
A-Pmean = -0.0611 * SITTING HEIGHT - 0.026 * WAIST + 0.145 * MASS + 16.5351 
A-P = (NORM.INV(RAND(),A-Pmean,0.9)

If FEMALE then:
A-Pmean = 0.1691 * BMI + 0.1096 * TC + 0.0991 * AR + 0.0393 * CHEST + 4.39
A-P = (NORM.INV(RAND(),A-Pmean,0.8)
    */
    
    
if($gender == 'M')
{
    $APmean = - 0.0611 * $sittingheight - 0.026 * $waist + 0.145 * $mass + 16.5351; 
    $APprob=$this->frand(0,1);
    $APprob=round($APprob,9);
    $AP = $object1->NORMINV($APprob,$APmean,0.9);    
}
else
{
    $APmean = 0.1691 * $bmi + 0.1096 * $TC + 0.0991 * $AR + 0.0393 * $chest + 4.39; 
    $APprob=$this->frand(0,1);
    $APprob=round($APprob,9);
    $AP = $object1->NORMINV($APprob,$APmean,0.8);    
}
    
    $fieldData[0]->apChest=round($AP,2);     
    $_SESSION['apChest']=round($AP,2);       $apChest=round($AP,2);
    
/*ARM SPAN breadth
[both MALE and FEMALE]:
ALR [arm length ratio] = (AR + RS + MD)/HEIGHT [same as equation generated above]
ARMSP% = -11.9048* ALR^2 + 12.1905 * ALR - 2.0286
ARMCM = ARMSP% * HEIGHT
ARM SPAN = (NORM.INV(RAND(),ARMCM,1.0)

CHECK :

If ARM SPAN < HEIGHT * 0.90 then
ARM SPAN = HEIGHT * 0.90

If ARM SPAN > HEIGHT * 1.13 then
ARM SPAN = HEIGHT * 1.13  */  
    
    
    $alr=($AR+$RS+$MD)/$height;
    $armsp_per= - 11.9048 * pow($alr,2) + 12.1905 * $alr - 2.0286;
    $ARMCM = $armsp_per * $height;
    $ARMprob=$this->frand(0,1);
    $ARMprob=round($ARMprob,9);
    $ARMSPAN = $object1->NORMINV($ARMprob,$ARMCM,1.0);
     
    if($ARMSPAN  < $height * 0.90 )
    {
        $ARMSPAN = $height * 0.90;
    }
    else
    if($ARMSPAN  > $height * 1.13 )
    {
        $ARMSPAN = $height * 1.13;
    }
    
 
    $fieldData[0]->armSpan=round($ARMSPAN,2);     
    $_SESSION['armSpan']=round($ARMSPAN,2);        $armSpan=round($ARMSPAN,2);
/*HUMERUS (bi-epi)
If MALE then:
BIHUMmean = 0.0191 * BIIL + 0.0644 * FOOT + 0.161 * MASS + 0.0091 * HEIGHT + 2.25 
BIHUM= (NORM.INV(RAND(),BIHUMmean,0.2)

(NEW)
If MALE then:
NEW one is: BIHUMmean = 0.01929 * BIIL + 0.0639 * FOOT + 0.0161 * MASS + 0.00922 * HEIGHT + 2.252


If FEMALE then:
BIHUMmean = 0.0465 * FOOT + 0.0455 * BMI + 0.0225 * HEIGHT + 0.63
BIHUM= (NORM.INV(RAND(),BIHUMmean,0.2)*/    
    
    if($gender =='M')
    {
        $BIHUMmean = 0.01929 * $BIIL + 0.0639 * $FOOT + 0.0161 * $mass + 0.00922 * $height + 2.252 ;  //(Altered)
        $BIHUprob=$this->frand(0,1);
        $BIHUprob=round($BIHUprob,9);
        $BIHUM= $object1->NORMINV($BIHUprob,$BIHUMmean,0.2);
    }
    if($gender == 'F')
    {
        $BIHUMmean = 0.0465 * $FOOT + 0.0455 * $bmi + 0.0225 * $height + 0.63;
        $BIHUprob=$this->frand(0,1);
        $BIHUprob=round($BIHUprob,9);
        $BIHUM= $object1->NORMINV($BIHUprob,$BIHUMmean,0.2);
    }
    
    
    
    
/*FEMUR (bi-epi)
If MALE then:
BIFEMmean = 0.094 * BIHUM + 0.08999 * FOOT - 0.0119 * AGE + 0.0257 * MASS + 5.254 
BIFEM= (NORM.INV(RAND(),BIFEMmean,0.25)

If FEMALE then:
BIFEMmean = 0.0332 * FOOT + 0.0273 * BIIL + 0.0324 * MASS + 5.537 
BIFEM= (NORM.INV(RAND(),BIFEMmean,0.22) */ 
    
    
    if($gender == 'M')
    {
        $BIFEMmean = 0.094 * $BIHUM + 0.08999 * $FOOT - 0.0119 * $age + 0.0257 * $mass + 5.254 ;
        $BIFEprob=$this->frand(0,1);
        $BIFEprob=round($BIFEprob,9);
        $BIFE= $object1->NORMINV($BIFEprob,$BIFEMmean,0.25);
    }
    if($gender == 'F')
    {
        $BIFEMmean = 0.0332 * $FOOT + 0.0273 * $BIIL + 0.0324 * $mass + 5.537  ;
        $BIFEprob=$this->frand(0,1);
        $BIFEprob=round($BIFEprob,9);
        $BIFE= $object1->NORMINV($BIFEprob,$BIFEMmean,0.22);
    }
    
    
    
   /* echo "Femur Mean".$BIFEMmean;
    echo "<br>";
    echo "Femur".$BIFE;
    echo "<br>";
    echo "humurus Mean".$BIHUMmean;
    echo "Humures".$BIHUM;
    echo "<br>";*/
    
    
    
// ANOTHER BODY COMPOSITION MODULE VARIABLES */
    
    $fieldData[0]->option_height_measured=round($measuredheight,1);
    $_SESSION['option_height_measured']=round($measuredheight,1);
    
    $fieldData[0]->option_weight_measured=round($measuredmass,2);
    $_SESSION['option_weight_measured']=round($measuredmass,2);
        
    $fieldData[0]->option_bmi=round($measuredBMI,2);
    $_SESSION['option_bmi']=round($measuredBMI,2);
    
    $fieldData[0]->option_measuredpopulation=$measuredpopulation;    
    $_SESSION['option_measuredpopulation']=$measuredpopulation;    
    
    $fieldData[0]->option_waist=round($waist,1);
    $_SESSION['option_waist']=round($waist,1);
    
    $fieldData[0]->option_hip=round($hip,1);
    $_SESSION['option_hip']=round($hip,1);
    
    $fieldData[0]->option_whr=round($whr,2);
    $_SESSION['option_whr']=round($whr,2);
        
    $fieldData[0]->option_triceps=round($triceps,1);
    $_SESSION['option_triceps']=round($triceps,1);       
        
    $fieldData[0]->option_biceps=round($biceps,1);
    $_SESSION['option_biceps']=round($biceps,1);         
    
    $fieldData[0]->option_subscapular=round($subscapular,1);
    $_SESSION['option_subscapular']=round($subscapular,1);       
    
    $fieldData[0]->option_sos=round($skinfold_summ,1);
    $_SESSION['option_sos']=round($skinfold_summ,1);
    
    $fieldData[0]->option_height=round($options_height,1);
    $_SESSION['option_height']=round($options_height,1);
    
    $fieldData[0]->option_weight=round($options_weight,2);
    $_SESSION['option_weight']=round($options_weight,2);
    
    
    
    
    
    
       // Other factors
    $fieldData[0]->triceps=round($triceps,1);
    $_SESSION['triceps']=round($triceps,1);      $triceps=round($triceps,1);
    
    $fieldData[0]->biceps=round($biceps,1);
    $_SESSION['biceps']=round($biceps,1);         $biceps=round($biceps,1);
    
    $fieldData[0]->subscapular=round($subscapular,1);
    $_SESSION['subscapular']=round($subscapular,1);        $subscapular=round($subscapular,1);
    
    $fieldData[0]->iliac_crest=round($Iliac_crest_skinfold,1);
    $_SESSION['iliac_crest']=round($Iliac_crest_skinfold,1);   $iliac=round($Iliac_crest_skinfold,1);
    
    $fieldData[0]->supraspinale=round($Supraspinale_skinfold,1);
    $_SESSION['supraspinale']=round($Supraspinale_skinfold,1);   $supraspinale=round($Supraspinale_skinfold,1);
    
    $fieldData[0]->abdominal=round($Abdominal_skinfold,1);
    $_SESSION['abdominal']=round($Abdominal_skinfold,1);     $abdominal=round($Abdominal_skinfold,1);
    
/*     $fieldData[0]->thigh=round($Iliac_crest_skinfold,1);
    $_SESSION['thigh']=round($Iliac_crest_skinfold,1); */
    
    $fieldData[0]->calf=round($Medial_calf_skinfold,1);
    $_SESSION['calf']=round($Medial_calf_skinfold,1);      
	  $calf_m=round($Medial_calf_skinfold,1);
    
    $fieldData[0]->mid_axilla=round($Midaxilla_skinfold,1);
    $_SESSION['mid_axilla']=round($Midaxilla_skinfold,1);   $mid_axilla=round($Midaxilla_skinfold,1);
    
    $fieldData[0]->thigh=round($Front_thigh_skinfold,1);
    $_SESSION['thigh']=round($Front_thigh_skinfold,1);    $thigh=round($Front_thigh_skinfold,1);
    
    
    $fieldData[0]->relArmG=round($relaxed_arm,2);
    $_SESSION['relArmG']=round($relaxed_arm,2);      $relArmG=round($relaxed_arm,2);
    
    $fieldData[0]->flexArmG=round($relaxed_arm_fl,2);
    $_SESSION['flexArmG']=round($relaxed_arm_fl,2);      $flexArmG=round($relaxed_arm_fl,2);
    
    $fieldData[0]->waistG=round($waist,2);
    $_SESSION['waistG']=round($waist,2);       $waistG=round($waist,2);
    
    $fieldData[0]->hipG=round($hip,2);
    $_SESSION['hipG']=round($hip,2);        $hipG=round($hip,2);
    
    $fieldData[0]->calfG=round($calf,2);
    $_SESSION['calfG']=round($calf,2);    $calfG=round($calf,2);
   
    
    
    // Lengths and breadhs
    $fieldData[0]->humerus=round($BIHUM,2);
    $_SESSION['humerus']=round($BIHUM,2);   $humerus=round($BIHUM,2);
    
    $fieldData[0]->femur=round($BIFE,2);
    $_SESSION['femur']=round($BIFE,2);      $femur=round($BIFE,2);
    
    // heigth and body mass
    $fieldData[0]->height=round($measuredheight,1);
    $_SESSION['height']=round($measuredheight,1);    $ht=round($measuredheight,1);
    
    $fieldData[0]->body_mass=round($measuredmass,2);
    $_SESSION['body_mass']=round($measuredmass,2);   $body_mass=round($measuredmass,2);
    
    
     
    $fieldData[0]->headG=round($Head,2);
    $_SESSION['headG']=round($Head,2);    $headG=round($Head,2);
   
    $fieldData[0]->neckG=round($neck,2);
    $_SESSION['neckG']=round($neck,2);    $neckG=round($neck,2);
    
    $fieldData[0]->relArmG=round($relaxed_arm,2);
    $_SESSION['relArmG']=round($relaxed_arm,2);    $relArmG=round($relaxed_arm,2);  
   
    $fieldData[0]->flexArmG=round($relaxed_arm_fl,2);
    $_SESSION['flexArmG']=round($relaxed_arm_fl,2);   $flexArmG=round($relaxed_arm_fl,2); 
   
    $fieldData[0]->forearmG=round($forearm_max,2);
    $_SESSION['forearmG']=round($forearm_max,2);   $forearmG=round($forearm_max,2);
    
    $fieldData[0]->wristG=round($wrist,2);
    $_SESSION['wristG']=round($wrist,2);    $wristG=round($wrist,2);
    
    $fieldData[0]->chestG=round($chest,2);
    $_SESSION['chestG']=round($chest,2);    $chestG=round($chest,2);
    
    $fieldData[0]->thighG=round($thigh_distal,2);
    $_SESSION['thighG']=round($thigh_distal,2);    $thighG=round($thigh_distal,2);
    
    $fieldData[0]->midThighG=round($thigh_mid,2);
    $_SESSION['midThighG']=round($thigh_mid,2);    $midThighG=round($thigh_mid,2);
    
   
    $fieldData[0]->ankleG=round($ankle,2);
    $_SESSION['ankleG']=round($ankle,2);   $ankleG=round($ankle,2);
    
    $fieldData[0]->sitting=round($sittingheight,2);
    $_SESSION['sitting']=round($sittingheight,2);   $sitting=round($sittingheight,2);

	
	
	
	
	
	    // BODY FAT MEAN Variables Calculation
    // 16 Oct Start
    if($gender == 'M')
     {       
             
              $n = 0 ; // Total number of %BF  
              $totalBF = 0 ; // Total %BF
              $sumSquares = 0 ; // Sum of squares of %BF
             // Body Fat Mean    

              if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $supraspinale !== "" && $abdominal !== "" && $thigh !== "" && $calf_m !== "") 
              {   
               $BDa = 1.0988 - 0.0004 * ($triceps + $subscapular + $biceps + $supraspinale + $abdominal + $thigh + $calf_m)  ;
               //echo $BDa;
                  
               $per_BFa = 495 / $BDa - 450 ; // %Body Fat 1 
               $lower_BFa = (($per_BFa - 4.8) * 10) / 10 ; // Lower Limit 1  
               $upper_BFa = (($per_BFa + 4.8) * 10) / 10 ; // Upper Limit 1  
               $totalBF = $totalBF + $per_BFa ;
               $sumSquares = $sumSquares + $per_BFa * $per_BFa ;
               $n = $n+1 ;
              } 

              if($abdominal !== "" && $thigh !== "") 
              {     
               $BDb = 1.08543 - 0.000886 * $abdominal - 0.0004 * $thigh ;
               $per_BFb = 495 / $BDb - 450 ; // %Body Fat 2 
               $lower_BFb = (($per_BFb - 6.6) * 10) / 10 ; // Lower Limit 2  
               $upper_BFb = (($per_BFb + 6.6) * 10) / 10 ; // Upper Limit 2  
               $totalBF = $totalBF + $per_BFb ;
               $sumSquares = $sumSquares + $per_BFb * $per_BFb ;
               $n = $n+1 ;
              }

              if($subscapular !== "" && $thigh !== "") 
              {
               $BDc = 1.1043 - 0.001327 * $thigh - 0.00131 * $subscapular ;
               $per_BFc = 495 / $BDc - 450 ; // %Body Fat 3  
               $lower_BFc = (($per_BFc - 5.1) * 10) / 10 ; // Lower Limit 3  
               $upper_BFc = (($per_BFc + 5.1) * 10) / 10 ; // Upper Limit 3
               $totalBF = $totalBF + $per_BFc ;
               $sumSquares = $sumSquares + $per_BFc * $per_BFc;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $abdominal !== "") 
              { 
               $BDd = 1.09665 - 0.00103 * $triceps - 0.00056 * $subscapular - 0.00054 * $abdominal ;
               $per_BFd = 495 / $BDd - 450 ; // %Body Fat 4
               $lower_BFd = (($per_BFd - 6.2) * 10) / 10 ; // Lower Limit 4  
               $upper_BFd = (($per_BFd + 6.2) * 10) / 10 ; // Upper Limit 4
               $totalBF = $totalBF + $per_BFd ;
               $sumSquares = $sumSquares + ($per_BFd * $per_BFd) ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $iliac !== "") 
              { 
               $BDe = 1.1765 - 0.0744 * log10($triceps + $biceps + $subscapular + $iliac) ;
               $per_BFe = 495 / $BDe - 450 ; // %Body Fat 5
               $lower_BFe = (($per_BFe - 9.1) * 10) / 10 ; // Lower Limit 5  
               $upper_BFe = (($per_BFe + 9.1) * 10) / 10 ; // Upper Limit 5 
               $totalBF = $totalBF + $per_BFe ;
               $sumSquares = $sumSquares + $per_BFe * $per_BFe ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $iliac !== "" && $abdominal !== "" && $thigh !== "" && $calf_m !== "" && $mid_axilla !== "") 
              { 
               $BDf = 1.1091 - 0.00052 * ($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf_m) + 0.00000032 *  (($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf_m ) * ($triceps + $subscapular + $mid_axilla + $iliac + $abdominal + $thigh + $calf_m ) )  ;  // edited bracket [=>()]
               $per_BFf = 495 / $BDf - 450 ; // %Body Fat 6
               $lower_BFf = (($per_BFf - 4.6) * 10) / 10 ; // Lower Limit 6  
               $upper_BFf = (($per_BFf + 4.6) * 10) / 10 ; // Upper Limit 6  
               $totalBF = $totalBF + $per_BFf ;
               $sumSquares = $sumSquares + ($per_BFf * $per_BFf) ;
               $n = $n+1 ;
              }

              if($triceps !== "" && $subscapular !== "" && $abdominal !== "" && $mid_axilla !== "") 
              { 
               $BDg = 1.10647 - 0.00162 * $subscapular - 0.00144 * $abdominal - 0.00077 * $triceps + 0.00071 * $mid_axilla ;
               $per_BFg = 495 / $BDg - 450 ; // %Body Fat 7
               $lower_BFg = (($per_BFg - 5) * 10) / 10 ; // Lower Limit 7  
               $upper_BFg = (($per_BFg + 5) * 10) / 10 ; // Upper Limit 7 
               $totalBF = $totalBF + $per_BFg ;
               $sumSquares = $sumSquares + ($per_BFg * $per_BFg) ;
               $n = $n+1 ;
              } 
                // BODY FATMean Calculation
                $meanBF = $totalBF / $n ; // Mean value 
                //echo "Mean".$meanBF;

                $fieldData[0]->meanbodyfat=round($meanBF,1);
                $_SESSION['bodyfat']=round($meanBF,1);
         
         
     }
    
     if($gender == 'F')
     {
                 $n = 0 ; // Total number of %BF  
                 $totalBF = 0 ; // Total %BF
                 $sumSquares = 0 ; // Sum of squares of %BF  

                  if($triceps !== "" && $subscapular !== "" && $supraspinale !== "" && $abdominal !== "" && $thigh !== "" && $calf_m !== "") 
                  {  
                   $BDa = 1.20953 - 0.08294 * log10($triceps + $subscapular + $supraspinale + $abdominal + $thigh + $calf_m )  ;
                   //echo "FEmale".$BDa;
                      
                   $per_BFa = 495 / $BDa - 450 ; // %Body Fat 1
                   $lower_BFa = (($per_BFa - 5.6) * 10) / 10 ; // Lower Limit 1  
                   $upper_BFa = (($per_BFa + 5.6) * 10) / 10 ; // Upper Limit 1 
                   $totalBF = $totalBF + $per_BFa ;
                   $sumSquares = $sumSquares + ($per_BFa * $per_BFa) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $supraspinale !== "" && $calf_m !== "") 
                  { 
                   $BDb = 1.17484 - 0.07229 * log10($triceps + $subscapular + $supraspinale + $calf_m ) ;
                   $per_BFb = 495 / $BDb - 450 ; // %Body Fat 2 
                   $lower_BFb = (($per_BFb - 5.6) * 10) / 10 ; // Lower Limit 2  
                   $upper_BFb = (($per_BFb + 5.6) * 10) / 10 ; // Upper Limit 2 
                   $totalBF = $totalBF + $per_BFb ;
                   $sumSquares = $sumSquares + ($per_BFb * $per_BFb) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $thigh !== "") 
                  { 
                   $BDc = 1.06234 - 0.000688 * $subscapular - 0.00039 * $triceps - 0.00025 * $thigh ;
                   $per_BFc = 495 / $BDc - 450 ; // %Body Fat 3
                   $lower_BFc = (($per_BFc - 5.6) * 10) / 10 ; // Lower Limit 3  
                   $upper_BFc = (($per_BFc + 5.6) * 10) / 10 ; // Upper Limit 3
                   $totalBF = $totalBF + $per_BFc ;
                   $sumSquares = $sumSquares + ($per_BFc * $per_BFc) ;
                   $n = $n+1 ;
                  } 

                  if($triceps !== "" && $subscapular !== "" && $iliac !== "") 
                  { 
                   $BDd =1.0987 - 0.00122 * ($triceps + $subscapular + $iliac) + 0.00000263 *  ($triceps + $subscapular + $iliac) * ($triceps + $subscapular + $iliac )  ;
                   $per_BFd = 495 / $BDd - 450 ; // %Body Fat 4
                   $lower_BFd = (($per_BFd - 5.2) * 10) / 10 ; // Lower Limit 4  
                   $upper_BFd = (($per_BFd + 5.2) * 10) / 10 ; // Upper Limit 4  
                   $totalBF = $totalBF + $per_BFd ;
                   $sumSquares = $sumSquares + ($per_BFd * $per_BFd) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $iliac !== "") 
                  {  
                   $BDe = 1.0764 - 0.00081 * $iliac - 0.00088 * $triceps ;
                   $per_BFe = 495 / $BDe - 450 ; // %Body Fat 5
                   $lower_BFe = (($per_BFe - 7.2) * 10) / 10 ; // Lower Limit 5  
                   $upper_BFe = (($per_BFe + 7.2) * 10) / 10 ; // Upper Limit 5  
                   $totalBF = $totalBF + $per_BFe ;
                   $sumSquares = $sumSquares + ($per_BFe * $per_BFe) ;
                   $n = $n+1 ;       
                  }

                  if($triceps !== "" && $abdominal !== "" && $thigh !== "" && $iliac !== "" && $hipG !== "") 
                  { 
                   $BDf = 1.24374 - 0.03162 * log( $triceps + $abdominal + $thigh + $iliac ) - 0.00066 * $hipG;
                   $per_BFf = 495 / $BDf - 450 ; // %Body Fat 6
                   $lower_BFf = (($per_BFf - 7.2) * 10) / 10 ; // Lower Limit 6  
                   $upper_BFf = (($per_BFf + 7.2) * 10) / 10 ; // Upper Limit 6  
                   $totalBF = $totalBF + $per_BFf ;
                   $sumSquares = $sumSquares + ($per_BFf * $per_BFf) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $thigh !== "" && $iliac !== "") 
                  { 
                   $BDg = 1.21389 - 0.04057 * log($triceps + $thigh + $iliac) - 0.00016 * $age ;
                   $per_BFg = 495 / $BDg - 450 ; // %Body Fat 7
                   $lower_BFg = (($per_BFg - 7.8) * 10) / 10 ; // Lower Limit 7  
                   $upper_BFg = (($per_BFg + 7.8) * 10) / 10 ; // Upper Limit 7  
                   $totalBF = $totalBF + $per_BFg ;
                   $sumSquares = $sumSquares + ($per_BFg * $per_BFg) ;
                   $n = $n+1 ;
                  } 

                  if($triceps !== "" && $subscapular !== "" && $biceps !== "" && $iliac !== "") 
                  {
                   $BDh = 1.1567 - 0.0717 * log10( $triceps+ $biceps + $subscapular + $iliac ) ;
                   $per_BFh = 495 / $BDh - 450 ; // %Body Fat 8
                   $lower_BFh = (($per_BFh - 10.8) * 10) / 10 ; // Lower Limit 8  
                   $upper_BFh = (($per_BFh + 10.8) * 10) / 10 ; // Upper Limit 8 
                   $totalBF = $totalBF + $per_BFh ;
                   $sumSquares = $sumSquares + ($per_BFh * $per_BFh) ;
                   $n = $n+1 ;
                  }

                  if($thigh !== "" && $iliac !== "") 
                  {
                   $BDi = 1.0852 - 0.0008 * $iliac - 0.0011 * $thigh ;
                   $per_BFi = 495 / $BDi - 450 ; // %Body Fat 9
                   $lower_BFi = (($per_BFi - 8.2) * 10) / 10 ; // Lower Limit 9  
                   $upper_BFi = (($per_BFi + 8.2) * 10) / 10 ; // Upper Limit 9
                   $totalBF = $totalBF + $per_BFi ;
                   $sumSquares = $sumSquares + ($per_BFi * $per_BFi) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $flexArmG !== "" && $subscapular !== "" && $hipG !== "") 
                  { 
                   $BDj = 1.12569 - 0.001835 * $triceps - 0.002779 / 2.54 * $hipG + 0.005419 / 2.54 * $flexArmG - 0.0007167 * $subscapular  ;
                   $per_BFj = 495 / $BDj - 450 ; // %Body Fat 10
                   $lower_BFj = (($per_BFj - 7.2) * 10) / 10 ; // Lower Limit 10  
                   $upper_BFj = (($per_BFj + 7.2) * 10) / 10 ; // Upper Limit 10
                   $totalBF = $totalBF + $per_BFj ;
                   $sumSquares = $sumSquares + ($per_BFj * $per_BFj) ;
                   $n = $n+1 ;
                  }

                  if($triceps !== "" && $subscapular !== "" && $relArmG !== "") 
                  { 
                   //$BDk = 0.97845 - 0.0002 * $triceps + 0.00088 * $ht - 0.00122 * $subscapular - 0.00234 * $relArmG  ;
                   $BDk = 0.97845 - 0.0002 * $triceps + 0.00088 * $ht - 0.00122 * $subscapular - 0.00234 * $relArmG  ;
                   $per_BFk = 495 / $BDk - 450 ; // %Body Fat 11
                   $lower_BFk = (($per_BFk - 7) * 10) / 10 ; // Lower Limit 11  
                   $upper_BFk = (($per_BFk + 7) * 10) / 10 ; // Upper Limit 11    
                   $totalBF = $totalBF + $per_BFk ;
                   $sumSquares = $sumSquares + ($per_BFk * $per_BFk) ;
                   $n = $n+1 ;
                  }
         
         
                   if($key == "fullprofile")
                      {
                             if($iliac !== "" && $subscapular !== "" && $thighG !== "" && $humerus !== "") 
                              { 
                               $BDl = 1.09246 - 0.00049 * $subscapular - 0.00075 * $iliac + 0.0071 * $humerus - 0.00121 * $thighG  ;
                               $per_BFl = 495 / $BDl - 450 ; // %Body Fat 12
                               $lower_BFl = (($per_BFl - 7.8) * 10) / 10 ; // Lower Limit 12  
                               $upper_BFl = (($per_BFl + 7.8) * 10) / 10 ; // Upper Limit 12  
                               $totalBF = $totalBF + $per_BFl ;
                               $sumSquares = $sumSquares + (($per_BFl) * ($per_BFl)) ;
                               $n = $n+1 ;
                              }  

                              if($triceps !== "" && $subscapular !== "" && $supraspinale !== "" && $abdominal !== "" && $thigh !== "" && $calf_m !== "" && $hipG !== "" && $forearmG !== "" && $humerus !== "") 
                              { 
                               $BDm = 1.16957 - 0.06447 *  log10($triceps + $subscapular + $supraspinale + $abdominal + $thigh + $calf_m ) - 0.000806 * $hipG + 0.0017 * $forearmG + 0.00606 * $humerus ;
                               $per_BFm = 495 / $BDm - 450 ; // %Body Fat 13
                               $lower_BFm = (($per_BFm - 5) * 10) / 10 ; // Lower Limit 13  
                               $upper_BFm = (($per_BFm + 5) * 10) / 10 ; // Upper Limit 13  
                               $totalBF = $totalBF + $per_BFm ;
                               $sumSquares = $sumSquares + ($per_BFm * $per_BFm) ;
                               $n = $n+1 ;
                              } 

                              if($iliac !== "" && $wristG !== "" && $femur !== "" && $thigh !== "") 
                              { 
                               $BDn = 1.0836 - 0.0007 * $iliac - 0.0007 * $thigh + 0.0048 * $wristG - 0.0088 * $femur ;
                               $per_BFn = 495 / $BDn - 450 ; // %Body Fat 14
                               $lower_BFn = (($per_BFn - 7.4) * 10) / 10 ; // Lower Limit 14  
                               $upper_BFn = (($per_BFn + 7.4) * 10) / 10 ; // Upper Limit 14 
                               $totalBF = $totalBF + $per_BFn ;
                               $sumSquares = $sumSquares + ( $per_BFn * $per_BFn ) ;
                               $n = $n+1 ;
                              }  
                     }
         
         
          // BODY FATMean Calculation
    $meanBF = $totalBF / $n ; // Mean value 
    
    $fieldData[0]->meanbodyfat=round($meanBF,1);
    $_SESSION['bodyfat']=round($meanBF,1);
         
         
         
     }
    // BODY FAT MEAN Variables Calculation
    // 16 Oct End
    
	
	
	
	
	 //Somatotype Variables Added 
    $total = $triceps + $supraspinale + $subscapular ;
    
    $endomorph_score = (-0.7182 + 0.1451 * ($total) - 0.00068 * ($total * $total) + 0.0000014 * ($total * $total * $total)) ;  
    $mesomorph_score = 0.858 * $humerus + 0.601 * $femur + 0.188 * ( $flexArmG - ($triceps / 10)) + 0.161 * ( $calfG - ($calf / 10)) - 0.131 * $ht + 4.5 ;
        
    
    //echo "Meso Score".$mesomorph_score;
    //echo "Endo".$endomorph_score;
    
    // Additional Calculation  , Dt: 11 oct 17
    //if HWR > 38.25 and HWR <40.75 then ECTOMORPH score = .463* HWR -17.63          
    //$hwr = $ht / (Math.cbrt(parseFloat(body_mass))) ; 
    $hwr = $ht / (pow($body_mass,1/3)) ; 
    
   
    if($hwr >= 40.75)
    {
     $ectomorph_score = 0.732 * $hwr - 28.58 ;  
    }
    else if($hwr < 38.25)
    {
     $ectomorph_score = 0.1 ;       
    }     
    else if($hwr > 38.25 && $hwr < 40.75 )  // added condition    
    {
     $ectomorph_score = .463 * $hwr - 17.63 ; 
    }
       
    //document.getElementById("somato").innerHTML = (Math.round(endomorph_score * 100) / 100)+", "+(Math.round(mesomorph_score * 100) / 100)+", "+(Math.round(ectomorph_score * 100) / 100) ;  
    $endomorph_score=   ($endomorph_score * 100) / 100;
    $mesomorph_score=   ($mesomorph_score * 100) / 100; 
    $ectomorph_score=   ($ectomorph_score * 100) / 100;
   
	
	
	$fieldData[0]->ectomorph_score=round($ectomorph_score,1);    $_SESSION['ecto_score']=round($ectomorph_score,1);
    $fieldData[0]->mesomorph_score=round($mesomorph_score,1);    $_SESSION['meso_score']=round($mesomorph_score,1);
    $fieldData[0]->endomorph_score=round($endomorph_score,1);    $_SESSION['endo_score']=round($endomorph_score,1);
	
	
	
	
    return $fieldData;
    
}
   
    

	 
	//PUT MASS and BMI 
       function Blood_values_norms($vp_age,$v)
       {
           if(($vp_age >=18 && $vp_age<=24) && $v=='M')
           {
			  $qry="SELECT * FROM blood_norm where age='18-24' and gender='M'";
		   }
           if(($vp_age >=25 && $vp_age<=34) && $v=='M')
           {
			   $qry="SELECT * FROM blood_norm where age='25-34' and gender='M'";
		   }
		   if(($vp_age >=35 && $vp_age<=44) && $v=='M')
	        {
				$qry="SELECT * FROM blood_norm where age='35-44' and gender='M'";
			}
           if(($vp_age >=45 && $vp_age<=54) && $v=='M')
           {
			   $qry="SELECT * FROM blood_norm where age='45-54' and gender='M'";
		   }
		   if(($vp_age >=55 && $vp_age<=64) && $v=='M')
           {
			   $qry="SELECT * FROM blood_norm where age='55-64' and gender='M'";
		   }
		    if(($vp_age >=65 && $vp_age<=74) && $v=='M')
           {
			   $qry="SELECT * FROM blood_norm where age='65-74' and gender='M'";
		   }
		   if(($vp_age >=75) && $v=='M')
           {
			   $qry="SELECT * FROM blood_norm where age='75+' and gender='M'";
		   }
		   
		   if(($vp_age >=18 && $vp_age<=24) && $v=='F')
           {
			  $qry="SELECT * FROM blood_norm where age='18-24' and gender='F'";
		   }
           if(($vp_age >=25 && $vp_age<=34) && $v=='F')
           {
			   $qry="SELECT * FROM blood_norm where age='25-34' and gender='F'";
		   }
		   if(($vp_age >=35 && $vp_age<=44) && $v=='F')
	        {
				$qry="SELECT * FROM blood_norm where age='35-44' and gender='F'";
			}
           if(($vp_age >=45 && $vp_age<=54) && $v=='F')
           {
			   $qry="SELECT * FROM blood_norm where age='45-54' and gender='F'";
		   }
		   if(($vp_age >=55 && $vp_age<=64) && $v=='F')
           {
			   $qry="SELECT * FROM blood_norm where age='55-64' and gender='F'";
		   }
		    if(($vp_age >=65 && $vp_age<=74) && $v=='F')
           {
			   $qry="SELECT * FROM blood_norm where age='65-74' and gender='F'";
		   }
		   if(($vp_age >=75) && $v=='F')
           {
			   $qry="SELECT * FROM blood_norm where age='75+' and gender='F'";
		   }
		   
           $query = $this->db->query($qry); 
		 if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $mean_sbp=$result['mean_sbp'];   
       $sd_sbp=$result['sd_sbp'];   
       $mean_dbp=$result['mean_dbp'];   
       $sd_dbp=$result['sd_dbp'];   
       $mean_cols=$result['mean_cols'];   
       $sd_cols=$result['sd_cols'];   
       $mean_ln_hdl=$result['mean_ln_hdl'];   
       $sd_ln_hdl=$result['sd_ln_hdl'];   
       $mean_ln_trig=$result['mean_ln_trig'];   
       $sd_ln_trig=$result['sd_ln_trig'];   
       $mean_ldl=$result['mean_ldl'];   
       $sd_ldl=$result['sd_ldl'];   
       $mean_ln_glu=$result['mean_ln_glu'];   
       $sd_ln_glu=$result['sd_ln_glu'];   
     
      
     $data=array('mean_sbp'=>$mean_sbp
             ,'sd_sbp'=>$sd_sbp
             ,'mean_dbp'=>$mean_dbp
             ,'sd_dbp'=>$sd_dbp
             ,'mean_cols'=>$mean_cols
             ,'sd_cols'=>$sd_cols
             ,'mean_ln_hdl'=>$mean_ln_hdl
             ,'sd_ln_hdl'=>$sd_ln_hdl
              ,'mean_ln_trig'=>$mean_ln_trig
              ,'sd_ln_trig'=>$sd_ln_trig
              ,'mean_ldl'=>$mean_ldl
              ,'sd_ldl'=>$sd_ldl
              ,'mean_ln_glu'=>$mean_ln_glu
              ,'sd_ln_glu'=>$sd_ln_glu
             );
      return $data;
      
       }
    
	 function getAge( $dob,$tdate)
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info' || $table=='client_person_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
        public function fetchScreeningStatusInfo()
      {
// var_dump($_SESSION);

    //  $user_id=$this->session->userdata('userid');   
      $user_id=$_SESSION['userid'];   
      //  echo "aaaaa";
    //  die(__FILE__);
        $q="SELECT * FROM `client_medical_info` where  c_id= $user_id";
        $query = $this->db->query($q);

            $row = $query->row_array();
            
          // print_r($row);
           if($row['option_1']=='N' && $row['option_2']=='N' && $row['option_3']=='N' && $row['option_4']=='N' && $row['option_5']=='N' && $row['option_6']=='N' && $row['option_7']=='N' ){
           return 'N';
          }else{
               return 'Y';
           }
           
           // return $row;
           
      } 

  public function updateRiskFactorInfo($riskFacor,$variable)
      {
           
     

          $code = array(
             // 'id' => $this->session->userdata('userid'),
              'id' => $_SESSION['userid'],
              'risk_factor' => $riskFacor,
              'screening_decision_category' => $variable);
          
         
                 //   $this->db->where('id', $this->session->userdata('userid'));
                    $this->db->where('id', $_SESSION['userid']);
                      $this->db->update('client_info', $code); 
                
          
      }  

  public function updateMedicationInfo($column,$status)
      {
           
       if($this->isExist( 'client_medication_info',  $_SESSION['userid'] )){
             echo  $sql = "UPDATE client_medication_info SET ". $column ." = ".$status." WHERE c_id =".$_SESSION['userid'];
		}else{
		echo $sql = "insert into client_medication_info (". $column .",c_id) values ( ".$status.",".$_SESSION['userid'].")";
		}	   
$this->db->query($sql);
          
      } 	  
      
      function getAllProjects()
    {
       $query = $this->db->query('SELECT * FROM project');
        $return = array();
        if($query->num_rows() > 0) {
        foreach($query->result_array() as $row) {
        //array_push($return,  $row['p_name']);
         $return [$row['projectname']] = $row['projectname'];
        }

    }else{
        $return[$row['']] = "select";   
         }
         return $return;
    }
   
      //GET RANDOM VAULES 
function generateRandomValues($valarr="", $subpop="",$vp_age="", $case="",$day="",$month="",$year="",$range="")
{
	if($case == "dob")
    {
			if(!($day=="" && $month=="" && $year=="") && ($case == "dob"))
			  {
			   $dateofBirth=$year.'-'.$month.'-'.$day;
			   $datebrth= date("Y-m-d",strtotime($dateofBirth));
			   $dob = strtotime($datebrth);
			   $tdate = strtotime(date('Y-m-d'));
			   $vp_age=$this->getAge($dob, $tdate);
			   $range=$this->getAgeRange($vp_age);
			   
			   
			   if($subpop=='Athlete')
			{
			$occupation="Athlete/coach";	
			}
			else
			{
			$occupation=$_SESSION['occupation'];	
			}
			   $randomData=array(
									 'firstname'=>$_SESSION['firstname'],
									 'lastname'=>$_SESSION['lastname'],
									 'gender'=>$_SESSION['gender'],
									 'daydropdown'=>$day,
									 'monthdropdown'=>$month,
									 'yeardropdown'=>$year,
									 'age_category'=>$range,
									 'occupation'=>$occupation,
									 'sub_population'=>$_SESSION['sub_population'],
									 'country'=>$_SESSION['country'],
									 'Random_generated_log_BMI'=>$_SESSION['Random_generated_log_BMI'],
									 'MASS'=>$_SESSION['MASS'],
									 'BMI'=>$_SESSION['BMI'],
									 //'BMI'=>$_SESSION['randombmi'],
									 'HEIGHT'=>$_SESSION['HEIGHT'],
									 'probability_LOG_BMI'=>$_SESSION['probability'] ,
									 'probability_random_BMI'=>$_SESSION['probabilitybmi'],
									 'm_BMI'=>$_SESSION['m_BMI'],
									 'b_BMI'=>$_SESSION['b_BMI'],
									 's_BMI'=>$_SESSION['s_BMI'],
									 'mean_log_Mass'=>$_SESSION['mean_log_Mass'],
									 'sd_log_Mass'=>$_SESSION['sd_log_Mass'] ,
									 'vp_age'=>$vp_age
									); 
			  
				 // print_r($randomData);
		           return $randomData;  
				   
			  }
    } 
        
if( !($subpop=="") )    
{
    //echo $subpop.$valarr.$vp_age;
   //if(isset($subpop) && !empty($subpop))
	if( !($subpop=="") && ($case == "third") )  
    {
        $subpopulation=$subpop;
    }
   if($valarr =='M')
    {  $v='Male';  }
    else{ $v='Female';  }
           $randomgenbmi=$this->mass_and_bmi($_SESSION['vp_age'],$v); 
           $object1 = new PHPExcel_Calculation_Statistical();
           $mean_log_Mass=$randomgenbmi['mean_log_Mass'];
           $sd_log_Mass=$randomgenbmi['sd_log_Mass'];
           $m_BMI=$randomgenbmi['m_BMI'];
           $b_BMI=$randomgenbmi['b_BMI'];
           $s_BMI=$randomgenbmi['s_BMI'];
    
           if($subpop=='Sedentary' && $v=='Male')
           {
              $probability=$this->frand(0,1);
              $probability=round($probability,9); 
              $massfactor='1.007';
              $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
              $randomprob=round($x,9);   
              $MASS=(exp($randomprob))*$massfactor;
              $MASS=round($MASS,8); 

              //GET BMI
              $probabilitybmi=$this->frand(0,1);
              $probabilitybmi=round($probabilitybmi,9); 
              $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
              $BMI=round($randombmi,8);

              //GET BMI
               $getheight=sqrt($MASS/$BMI)*100;
               $HEIGHT=round($getheight,7);
           }
          if($subpop=='Sedentary' && $v=='Female')
           {
              $probability=$this->frand(0,1);
              $probability=round($probability,9); 
              $massfactor='1.007';
              $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
              $randomprob=round($x,9);   
              $MASS=(exp($randomprob))*$massfactor;
              $MASS=round($MASS,8); 

              //GET BMI
              $probabilitybmi=$this->frand(0,1);
              $probabilitybmi=round($probabilitybmi,9); 
              $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
              $BMI=round($randombmi,8);

              //GET BMI
               $getheight=sqrt($MASS/$BMI)*100;
               $HEIGHT=round($getheight,7);
           }
     
    
         //for Male and General
       if($v=='Male' && $subpop=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.25),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and General
       if($v=='Female' && $subpop=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
      //for Male and Athlete or active
       if($v=='Male' && ($subpop=='Athlete' || $subpop=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.975';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
       $probabilitybmi=$this->frand(0,1);
       $probabilitybmi=round($probabilitybmi,9); 
       $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
       $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Athlete or active
       if($v=='Female' && ($subpop=='Athlete' || $subpop=='Active'))
     {
       $probability=$this->frand(0,1);
       $probability=round($probability,9); 
       $massfactor='0.962';
       $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
       $randomprob=round($x,9);   
       $MASS=(exp($randomprob))*$massfactor;
       $MASS=round($MASS,8); 
      
      //GET BMI
       $probabilitybmi=$this->frand(0,1);
       $probabilitybmi=round($probabilitybmi,9); 
       $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
       $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }

   /*  $randomData=array(
                             'firstname'=>$_SESSION['firstname'],
                             'lastname'=>$_SESSION['lastname'],
                             'gender'=>$v,
                             'daydropdown'=>$_SESSION['daydropdown'],
                             'monthdropdown'=>$_SESSION['monthdropdown'],
                             'yeardropdown'=>$_SESSION['yeardropdown'],
                             'age_category'=>$_SESSION['age_category'],
                             'occupation'=>$_SESSION['occupation'],
                             'sub_population'=>$subpopulation,
                             'country'=>$_SESSION['country'],
                             'Random_generated_log_BMI'=>$_SESSION['Random_generated_log_BMI'],
                             'MASS'=>round($MASS,2),
                             'BMI'=>$randombmi,
                             'HEIGHT'=>$HEIGHT,
                             'probability_LOG_BMI'=>$probability ,
                              'probability_random_BMI'=>$probabilitybmi,
                             'm_BMI'=>$m_BMI,
                             'b_BMI'=>$b_BMI,
                             's_BMI'=>$s_BMI,
                             'mean_log_Mass'=>$mean_log_Mass,
                             'sd_log_Mass'=>$sd_log_Mass ,
                             'vp_age'=>$_SESSION['vp_age']
                            );  */
							
			if($subpopulation=='Athlete')
			{
			$occupation="Athlete/coach";	
			}
			else
			{
			$occupation=$_SESSION['occupation'];	
			}
		
							
	$randomData=array(
                             'firstname'=>$_SESSION['firstname'],
                             'lastname'=>$_SESSION['lastname'],
                             'gender'=>$v,
                             'daydropdown'=>$day,
                             'monthdropdown'=>$month,
                             'yeardropdown'=>$year,
                             'age_category'=>$range,
                             'occupation'=>$occupation,
                             'sub_population'=>$subpopulation,
                             'country'=>$_SESSION['country'],
                             'Random_generated_log_BMI'=>$_SESSION['Random_generated_log_BMI'],
                             'MASS'=>round($MASS,2),
                             'BMI'=>$randombmi,
                             'HEIGHT'=>$HEIGHT,
                             'probability_LOG_BMI'=>$probability ,
                             'probability_random_BMI'=>$probabilitybmi,
                             'm_BMI'=>$m_BMI,
                             'b_BMI'=>$b_BMI,
                             's_BMI'=>$s_BMI,
                             'mean_log_Mass'=>$mean_log_Mass,
                             'sd_log_Mass'=>$sd_log_Mass ,
                             'vp_age'=>$vp_age
                            ); 						
							
							
							
							
							
							
							
							
    
return $randomData;    

}
        

// FOR GENDER CHANGE        
if(!($valarr=="") )
        {
            //echo "if part";
            if($valarr =='M')
            {  $v='Male';  $k=1;}
            else{ $v='Female'; $k=2;}
            if($k=='1')
            {
            $columnname="boys_fname";    
            }
            else
            {
            $columnname="girls_fname";   
            }
            
           //GENERATE RANDOM NAMES 
           $randomnamesQry="SELECT $columnname as firstname FROM random_names where $columnname!='' ORDER BY RAND() LIMIT 1";
           $query = $this->db->query($randomnamesQry);
           if($query->num_rows() > 0) {
             $result=$query->row_array();
            }
           $firstname=$result['firstname'];
           $randomlastQry="SELECT last_name FROM random_names where last_name!='' ORDER BY RAND() LIMIT 1";
           $queryrandom = $this->db->query($randomlastQry);
           if($queryrandom->num_rows() > 0) {
             $resultlastname=$queryrandom->row_array();
            }
           $lastname=$resultlastname['last_name'];
           //$selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
           //END GENERATE RANDOM NAMES 
            
            $randomData=array(
                             'firstname'=>$firstname,
                             'lastname'=>$lastname,
                             'gender'=>$v,
                             'daydropdown'=>$_SESSION['daydropdown'],
                             'monthdropdown'=>$_SESSION['monthdropdown'],
                             'yeardropdown'=>$_SESSION['yeardropdown'],
                             'age_category'=>$_SESSION['age_category'],
                             'occupation'=>$_SESSION['occupation'],
                             'sub_population'=>$_SESSION['sub_population'],
                             'country'=>$_SESSION['country'],
                             'Random_generated_log_BMI'=>$_SESSION['Random_generated_log_BMI'],
                             'MASS'=>$_SESSION['MASS'],
                             'BMI'=>$_SESSION['BMI'],
                             'HEIGHT'=>$_SESSION['HEIGHT'],
                             'probability_LOG_BMI'=>$_SESSION['probability_LOG_BMI'],
                              'probability_random_BMI'=>$_SESSION['probability_random_BMI'],
                             'm_BMI'=>$_SESSION['m_BMI'],
                             'b_BMI'=>$_SESSION['b_BMI'],
                             's_BMI'=>$_SESSION['s_BMI'],
                             'mean_log_Mass'=>$_SESSION['mean_log_Mass'],
                             'sd_log_Mass'=>$_SESSION['sd_log_Mass'],
                             'vp_age'=>$_SESSION['vp_age']
                            ); 
return $randomData;
}

// When no changes
else
{
           // echo "else part";
               //FOR MALE FEMALE 
               $genderarray = array( '1'  => 'Male','2'  => 'Female');  
               $k = array_rand($genderarray);
               $v = $genderarray[$k]; 
               $selectedgender=array('gendervalue'=>$k,'gendertext'=>$v);
                //FOR MALE FEMALE
               if($k=='1')
               {
                $columnname="boys_fname";    
               }
               else
               {
                $columnname="girls_fname";   
               }
       
        
       //GENERATE RANDOM NAMES 
       $randomnamesQry="SELECT $columnname as firstname FROM random_names where $columnname!='' ORDER BY RAND() LIMIT 1";
       $query = $this->db->query($randomnamesQry);
       if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $firstname=$result['firstname'];
       $randomlastQry="SELECT last_name FROM random_names where last_name!='' ORDER BY RAND() LIMIT 1";
       $queryrandom = $this->db->query($randomlastQry);
       if($queryrandom->num_rows() > 0) {
         $resultlastname=$queryrandom->row_array();
        }
       $lastname=$resultlastname['last_name'];
       $selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
       //END GENERATE RANDOM NAMES 
       
       //RANDOM DATE OF BIRTH GENERATION
        
       $dates = array(
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
       
                            $month = array(
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
       
       
       $years = array('1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927');		
       /*Day Selection*/
       $daykey = array_rand($dates);
       $valuedate = $dates[$daykey]; 
       $selecteddates=array('datekey'=>$daykey,'datevalue'=>$valuedate);
        /*Month Selection*/
       $monthkey = array_rand($month);
       $valuemonth = $month[$monthkey]; 
       $selectedmonth=array('monthkey'=>$monthkey,'monthvalue'=>$valuemonth);
        /*Month Selection*/
        
       /*Year Selection*/
       $yearskey = array_rand($years);
       $valueyears = $years[$yearskey]; 
       $selectedyears=array('yearskey'=>$yearskey,'yearsvalue'=>$valueyears);
        /*Year Selection*/
       
       /*COUNTRY PROFILE*/
        $min=1;
        $max=100;
        $randomcountryval=rand($min,$max);
        if($randomcountryval<90)
        {
         $selectedyears=array('countrykey'=>'AU','countryvalue'=>'Australia');   
        }
       else
       {
        $randomctryQry="SELECT country_code,country_name FROM country ORDER BY RAND() LIMIT 1";
       $queryrandomctry = $this->db->query($randomctryQry);
       if($queryrandomctry->num_rows() > 0) {
         $resultctry=$queryrandomctry->row_array();
        }    
       $selectedyears=array('countrykey'=>$resultctry['country_code'],'countryvalue'=>$resultctry['country_name']);    
       } 
      /*END COUNTRY PROFILE*/
      /*OCCUPATION PROFILE*/
     
        $minoccu=1;
        $maxoccu=30;
        $randomoccuval=rand($minoccu,$maxoccu);
       
       $occupationQry="SELECT `occupation` FROM occupation where id=$randomoccuval";
       $queryrandomoccu = $this->db->query($occupationQry);
       if($queryrandomoccu->num_rows() > 0) {
         $resultoccupation=$queryrandomoccu->row_array();
        }    
       $selectedoccupation=array('occupationkey'=>$resultoccupation['occupation'],'occupationval'=>$resultoccupation['occupation']);
       $occupation=$resultoccupation['occupation']; 
       /*END OCCUPATION PROFILE*/ 
       $dateofBirth=$valueyears.'-'.$valuemonth.'-'.$valuedate;
       $datebrth= date("Y-m-d",strtotime($dateofBirth));
       $dob = strtotime($datebrth);
       $tdate = strtotime(date('Y-m-d'));
       $vp_age=$this->getAge($dob, $tdate);
       $agerange=$this->getAgeRange($vp_age);
      //SUB-POPULATION PROFILE CONDITION
       $subpopulation="";
       //FOR MALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 6)
            {
            $subpopulation="Sedentary";    
            }
            else if($RN >= 6 && $RN< 20)
            {
            $subpopulation="General";    
            }
             else if($RN >=20  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
     
          }
      
      //FOR FEMALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 8)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 8 && $RN< 24)
            {
            $subpopulation="General";    
            }
             else if($RN >=24  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
        
            }
       
        //FOR MALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 9)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 9 && $RN< 30)
            {
            $subpopulation="General";    
            }
             else if($RN >=30  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
            }
      
      //FOR FEMALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
       //FOR Male 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //FOR Female 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 14)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 14 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //ANother Condition Based on Subpopulation and Age..
       if($subpopulation=='Athlete' && $vp_age>40)
       {
        $subpopulation="Active";    
       }
       //FOR MALE 45-54 yr
       if(($vp_age >=45 && $vp_age<=54) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 15)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 15 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 45-54 
       if(($vp_age >=45 && $vp_age<=54) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 18)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 18 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR MALE 55-64 yr
       if(($vp_age >=55 && $vp_age<=64) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 16)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 16 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 55-64 
       if(($vp_age >=55 && $vp_age<=64) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       
        //FOR MALE 65-74  yr
       if(($vp_age >=65 && $vp_age<=74) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 65-74 
       if(($vp_age >=65 && $vp_age<=74) && $v=='Female')
       {
        $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 24)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 24 && $RN< 52)
            {
            $subpopulation="General";    
            }
             else if($RN >=52)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Male 75+ 
       if($vp_age>= 75 && $v=='Male')
       {
        $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 31)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 31 && $RN< 62)
            {
            $subpopulation="General";    
            }
             else if($RN >=62)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Female 75+ 
       if($vp_age>= 75 && $v=='Female')
       {
        $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 36)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 36 && $RN< 68)
            {
            $subpopulation="General";    
            }
             else if($RN >=68)
            {
            $subpopulation="Active";    
            }
       }

    
       $randomgenbmi=$this->mass_and_bmi($vp_age,$v); 
       $object1 = new PHPExcel_Calculation_Statistical();
       $mean_log_Mass=$randomgenbmi['mean_log_Mass'];
       $sd_log_Mass=$randomgenbmi['sd_log_Mass'];
       $m_BMI=$randomgenbmi['m_BMI'];
       $b_BMI=$randomgenbmi['b_BMI'];
       $s_BMI=$randomgenbmi['s_BMI'];
     
//for male and Sedentary
       if($v=='Male' && $subpopulation=='Sedentary' )
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.007';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
      $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
        
        
      
        
     //for Female and Sedentary
       if($v=='Female' && $subpopulation=='Sedentary' )
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.038';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.75),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     
     //for Male and General
       if($v=='Male' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.25),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and General
       if($v=='Female' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
      //for Male and Athlete or active
       if($v=='Male' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.975';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Athlete or active
       if($v=='Female' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.962';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
       $probabilitybmi=$this->frand(0,1);
       $probabilitybmi=round($probabilitybmi,9); 
       $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
       $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
 
        
$_SESSION['firstname']=   $firstname;     
$_SESSION['lastname']=   $lastname;     
$_SESSION['gender']=      $v ; 
$_SESSION['daydropdown']=  $valuedate      ;
$_SESSION['monthdropdown']= $monthkey       ;
$_SESSION['yeardropdown']=   $valueyears     ;
$_SESSION['age_category']=    $agerange    ;
$_SESSION['occupation']=      $occupation  ;
$_SESSION['sub_population']=   $subpopulation;     
$_SESSION['country']=        $selectedyears;
$_SESSION['Random_generated_log_BMI']= $randomprob       ;
$_SESSION['MASS']=        round($MASS,2);
$_SESSION['BMI']=        $randombmi;
$_SESSION['HEIGHT']=      $HEIGHT  ;
$_SESSION['probability_LOG_BMI']= $probability       ;
$_SESSION['probability_random_BMI']= $probabilitybmi  ;     
$_SESSION['m_BMI']=  $m_BMI      ;
$_SESSION['b_BMI']=  $b_BMI      ;
$_SESSION['s_BMI']=  $s_BMI      ;
$_SESSION['mean_log_Mass']=  $mean_log_Mass      ;
$_SESSION['sd_log_Mass']=    $sd_log_Mass    ;
$_SESSION['vp_age']=       $vp_age ;
$_SESSION['is_virtual']=1;        
        
        
        
 $randomData=array(
     'firstname'=>$firstname,
     'lastname'=>$lastname,
     'gender'=>$v,
     'daydropdown'=>$valuedate,
     'monthdropdown'=>$monthkey,
     'yeardropdown'=>$valueyears,
     'age_category'=>$agerange,
     'occupation'=>$occupation,
     'sub_population'=>$subpopulation,
     'country'=>$selectedyears,
     'Random_generated_log_BMI'=>$randomprob,
     'MASS'=>round($MASS,2),
     'BMI'=>$randombmi,
     'HEIGHT'=>$HEIGHT,
     'probability_LOG_BMI'=>$probability,
      'probability_random_BMI'=>$probabilitybmi,
     'm_BMI'=>$m_BMI,
     'b_BMI'=>$b_BMI,
     's_BMI'=>$s_BMI,
     'mean_log_Mass'=>$mean_log_Mass,
     'sd_log_Mass'=>$sd_log_Mass,
     'vp_age'=>$vp_age
     ); 

   //print_r($_SESSION);   
        
      return $randomData; 
            
}

    
}//END OF FUNCTION GENERATE RANDOM
  
        
        
        
        
     //fraction random number.
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }
       //PUT MASS and BMI 
       function mass_and_bmi($vp_age,$v)
       {
	if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
           {
			  $qry="SELECT * FROM bmi_norm where age='18-24' and gender='M'";
		   }
            if(($vp_age >=25 && $vp_age<=29) && $v=='Male')
           {
			   $qry="SELECT * FROM bmi_norm where age='25-29' and gender='M'";
		   }
		   if(($vp_age >=30 && $vp_age<=34) && $v=='Male')
	        {
				$qry="SELECT * FROM bmi_norm where age='30-34' and gender='M'";
			}
           if(($vp_age >=35 && $vp_age<=39) && $v=='Male')
           {
			   $qry="SELECT * FROM bmi_norm where age='35-39' and gender='M'";
		   }
		   if(($vp_age >=40 && $vp_age<=44) && $v=='Male')
           {
			   $qry="SELECT * FROM bmi_norm where age='40-44' and gender='M'";
		   }
		    if(($vp_age >=45 && $vp_age<=49) && $v=='Male')
           {
			   $qry="SELECT * FROM bmi_norm where age='45-49' and gender='M'";
		   }
		    if(($vp_age >=50 && $vp_age<=54) && $v=='Male')
           {
			   $qry="SELECT * FROM bmi_norm where age='50-54' and gender='M'";
		   }
		   if(($vp_age >=55 && $vp_age<=59) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='55-59' and gender='M'";
		   } 
		   if(($vp_age >=60 && $vp_age<=64) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='60-64' and gender='M'";
		   }
		   if(($vp_age >=65 && $vp_age<=69) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='65-69' and gender='M'";
		   }
		   if(($vp_age >=70 && $vp_age<=74) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='70-74' and gender='M'";
		   }
		   
		    if(($vp_age >=75 && $vp_age<=79) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='75-79' and gender='M'";
		   }
		    
		    if(($vp_age >=80 ) && $v=='Male')
		   {
		       $qry="SELECT * FROM bmi_norm where age='80+' and gender='M'";
		   }
		   
      
	  
	  // Female CASE
	       	if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
           {
			  $qry="SELECT * FROM bmi_norm where age='18-24' and gender='F'";
		   }
            if(($vp_age >=25 && $vp_age<=29) && $v=='Female')
           {
			   $qry="SELECT * FROM bmi_norm where age='25-29' and gender='F'";
		   }
		   if(($vp_age >=30 && $vp_age<=34) && $v=='Female')
	        {
				$qry="SELECT * FROM bmi_norm where age='30-34' and gender='F'";
			}
           if(($vp_age >=35 && $vp_age<=39) && $v=='Female')
           {
			   $qry="SELECT * FROM bmi_norm where age='35-39' and gender='F'";
		   }
		   if(($vp_age >=40 && $vp_age<=44) && $v=='Female')
           {
			   $qry="SELECT * FROM bmi_norm where age='40-44' and gender='F'";
		   }
		    if(($vp_age >=45 && $vp_age<=49) && $v=='Female')
           {
			   $qry="SELECT * FROM bmi_norm where age='45-49' and gender='F'";
		   }
		    if(($vp_age >=50 && $vp_age<=54) && $v=='Female')
           {
			   $qry="SELECT * FROM bmi_norm where age='50-54' and gender='F'";
		   }
		   if(($vp_age >=55 && $vp_age<=59) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='55-59' and gender='F'";
		   } 
		   if(($vp_age >=60 && $vp_age<=64) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='60-64' and gender='F'";
		   }
		   if(($vp_age >=65 && $vp_age<=69) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='65-69' and gender='F'";
		   }
		   if(($vp_age >=70 && $vp_age<=74) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='70-74' and gender='F'";
		   }
		   
		    if(($vp_age >=75 && $vp_age<=79) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='75-79' and gender='F'";
		   }
		    
		    if(($vp_age >=80 ) && $v=='Female')
		   {
		       $qry="SELECT * FROM bmi_norm where age='80+' and gender='F'";
		   }
	  
	     // echo $qry; die;
	         $query = $this->db->query($qry); 
		 if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
		
		
	   $mean_log_mass=$result['mean_log_mass'];   
       $sd_log_mass=$result['sd_log_mass'];   
       $m_BMI=$result['m_BMI'];   
       $b_BMI=$result['b_BMI'];   
       $s_BMI=$result['s_BMI'];   
       
        
      
      $data=array('mean_log_Mass'=>$mean_log_mass,'sd_log_Mass'=>$sd_log_mass,'m_BMI'=>$m_BMI,'b_BMI'=>$b_BMI,'s_BMI'=>$s_BMI);
	  
	  //print_r($data); die;
      return $data;
      
       }
    

//GET AGE RANGE
       function getAgeRange($age)
       {
       	
			 if($age >= 18 && $age <=29)
			 {
				$agerange = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $agerange = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				 $agerange = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				$agerange = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				 $agerange = '60-69' ;
			 }
			 elseif($age >=70 && $age <=79)
			 {
				$agerange = '70-79' ;
			 }
                          elseif($age >=80 && $age <=84)
			 {
				$agerange = '80-84' ;
			 }
                          elseif($age >84)
			 {
				$agerange = '84+' ;
			 }
                    
                         return $agerange;     
                         
                   }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    //Life Expectancy	
	 public function lifeExpectancyInfo()
	 {
		$actual_age=$_SESSION['vp_age'];
		$gender=$_SESSION['user_gender'];;
		$user_id=$_SESSION['userid'];
		$result=array();
		
		//echo "Actual Age".$actual_age;
		
	$lifeexpectqry="select rfi.option_1 as heart_disease,rfi.option_2 as smoking,rfi.option_4 AS high_bp,rfi.option_6 as high_blood_sugar,rfi.option_11 as colestrol,rfi.option_9 as hdl,rfi.option_10 as ldl,rfi.option_13 as triglycerides,rfi.option_12 as hbg , rfi.option_3 as smoking2 ,rfi.option_5 as high_colestrol ,rfi.option_7 AS SBP, mi.option_1 as ecgstroke, medi.medical_conditions as diab ,bci.option_waist as waist,(rfi.option_11/rfi.option_9) as hdlratio ,bci.option_bmi as BMI,bci.option_height as self_height,bci.option_weight as self_weight,bci.option_height_measured as height,bci.option_weight_measured as weight,bci.option_hip as hip,bci.option_biceps,bci.option_triceps,bci.option_subscapular,rfi.option_7 as SBP ,rfi.option_8 as DBP,(pai.option_2 + 2*pai.option_4 + pai.option_6) as physical_activity ,( pai.option_7 + pai.option_8 ) as total_sed ,pai.option_4 as vigorous_time,pai.option_9 AS sitting_job,rfi.option_smoke as smokeperday 
        from client_risk_factor_info AS rfi 
        left JOIN client_medical_info mi ON mi.c_id = rfi.c_id 
        left JOIN client_medication_info medi ON medi.c_id = rfi.c_id
        left JOIN client_body_composition_info bci ON bci.c_id=rfi.c_id
        left JOIN client_physical_activity_info pai ON pai.c_id = rfi.c_id 
        where rfi.c_id = $user_id";
       
	 $query = $this->db->query($lifeexpectqry);
        $row = $query->row_array();
        $sumSK =$row['option_biceps'] +$row['option_triceps'] + $row['option_subscapular'];
        
		//print_r($row); die;
		if(empty($row['physical_activity'] ) || empty($row['DBP']) || empty($row['SBP']) || empty($row['BMI']) || empty($row['waist']) || empty($row['colestrol']) || empty($row['triglycerides']) || empty($row['hbg']) || empty($row['total_sed']) 
			     || $_SESSION['total_PA'] == "" || $_SESSION['healthage'] == "" )
			{
				$_SESSION['no_visit']=1;   // For screen visits.
			}
		
		if( $row['physical_activity'] != "" )
		{
				
		        $actual_age=$_SESSION['vp_age'];
				$health_age=round($_SESSION['healthage'],0);
				$age_gap=$actual_age - $health_age;
             		if($row['smoking'] == 'Y' || $row['smoking2'] == 'Y')
				{   $smoking=1;	
					$numbr_of_smoke = $row['smokeperday'];
					$_SESSION['numbr_of_smoke']=$numbr_of_smoke;
				}else{
					$smoking=0;			
					$numbr_of_smoke = 0;
					$_SESSION['numbr_of_smoke']=0;
				}
				if($row['smoking'] == "")
				{
					$smoke='N';
				}else{
					$smoke='Y';
				}
				$_SESSION['smoking']=$smoke;
				$_SESSION['smokingval']=$smoking; 
        
		 
			 // calculate life expectancy
			 //y = -0.000090995563364x3 + 0.000452083896037x2 + 0.508746011970253x + 81.3068468339833
			 
			  $life_expectancy = -0.000090995563364 * pow($age_gap,3) + 0.000452083896037*pow($age_gap,2) + 0.508746011970253* $age_gap + 81.3068468339833;
			  
			  //echo $_SESSION['LE'];
			  
			 //$life_expectancy = ( -0.000090995563364 * ($age_gap * $age_gap * $age_gap) ) + (0.000452083896037 * ($age_gap * $age_gap)) + (0.508746011970253 * ($age_gap)) + 81.3068468339833  ;
		     //echo "Life expectancy".$life_expectancy ;
		 
		     // Exponent of the equation
			 //y = 0.000000000112981x5 - 0.000000005784660x4 - 0.000000403605651x3 + 0.000046117808940x2 - 0.001214174971577x + 6.257928433678860
			 $y= (0.000000000112981*($age_gap * $age_gap * $age_gap * $age_gap * $age_gap )) - (0.000000005784660 *($age_gap * $age_gap * $age_gap * $age_gap )) - (0.000000403605651 * ($age_gap * $age_gap * $age_gap)) +  ( 0.000046117808940 *($age_gap * $age_gap)) - (0.001214174971577 *($age_gap)) + 6.257928433678860;
			 
			// echo "ecpo_".$y;
			 /*
			 IF Age difference <-40 years then x = -40
			 IF Age difference >50 years then x = 50
			 */
			 if($age_gap < -40)
			 {
				 $age_gap = -40;
			 }				 
			 if($age_gap > 50)
			 {
				 $age_gap = 50;
			 }
			 //FIRST ADJUSTMENT  = (80^y)/10000000000			 
			 $first_adj = (pow(80,$y))/10000000000;
			// echo "adj_". $first_adj;

			 // Step 4
			 /* Quality Years
			 y = 0.00723x2 - 1.71606x + 97.89004
			 */			 
			 $quality_year = 0.00723 * ($age_gap * $age_gap) - 1.71606 *($age_gap) + 97.89004;
			 
			 // echo "quality_year_". $quality_year;
			/* Step 5 
			Life expectancy years [that is, DEATH AGE] = y
			Y = (AGE GAP * Quality years fraction)/100 + FIRST ADJUSTMENT
			Where y = DEATH AGE:
			*/
			$deathage = ($age_gap * $quality_year)/100 + $first_adj;
			
			if($gender == 'F' || $gender == 'Female')
			{
				// Therefore FEMALE DEATH AGE = DEATH AGE + (DEATH AGE *0.1 – 4)
                 $deathage=$deathage+($deathage * 0.1 - 4);			
			}
		 
		 //IF DEATH AGE < Actual Age +2 THEN DEATH AGE = Actual Age + 4		 
			 if($deathage < $actual_age + 2 )
			 {
				 $deathage = $actual_age  + 4;
			 }
		 
	//echo "deathage_". $deathage;
		$_SESSION['LE']=round($deathage,1);
		$medicalhistory_status=$this->fetchScreeningStatusInfo();
		
		$result=array('physical_activity'=>$row['physical_activity'],'total_sed'=>$row['total_sed'],'waist'=>$row['waist'],'SBP'=>$row['SBP'],'DBP'=>$row['DBP'],'colestrol'=>$row['colestrol'],'HDL'=>$row['hdl'],'ldl'=>$row['ldl'],'triglycerides'=>$row['triglycerides'],'triglycerides'=>$row['triglycerides'],'smoking'=>$smoking,'numbr_of_smoke'=>$numbr_of_smoke,'glucose'=>$row['hbg'],'med_status'=>$medicalhistory_status,'high_chol'=>$row['high_colestrol'],'high_blood_sugar'=>$row['high_blood_sugar'],'vigorous_time'=>$row['vigorous_time'],'heart_disease'=>$row['heart_disease'],'high_bp'=>$row['high_bp'],'BMI'=>$row['BMI'],'self_height'=>$row['self_height'],'self_weight'=>$row['self_weight'],'hip'=>$row['hip'],'option_biceps'=>$row['option_biceps'],'option_triceps'=>$row['option_triceps'],'option_subscapular'=>$row['option_subscapular'],'sitting_job'=>$row['sitting_job'],'smokeperday'=>$row['smokeperday'],'health_age'=>$health_age,'sumSK'=>$sumSK,'height'=>$row['height'],'weight'=>$row['weight']);
	
		return $result;
        }		
		return $result;
	 }	 
		
    
    
      
}
?>