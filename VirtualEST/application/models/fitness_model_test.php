<?php
require(APPPATH .'/third_party/Classes/PHPExcel/Calculation/Statistical.php');

class fitness_model extends CI_Model
{
    function __construct()
    {
        parent::__construct(); 
        session_start();
    }

   public function all_sports_male(){
    
    $this->db->select("sport");	     
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%m%')");
	$this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  

    //Get Sport List According to Gender
    public function get_sports_by_gender($gender)
	 {
    $this->db->select("sports");	     
    $this->db->from('sports_fitness_norms');
    $this->db->where("( gender like '$gender')");
	$this->db->order_by("sports", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
   } 
  
  
  
     //Trademill Save
	public function trademill_save()
    {
	  //print_r($this->input->post()); die;
	 
	 
	      $c_id = $_SESSION['userid'];
      $code = array('c_id' =>$c_id,
	                'gender' =>$this->input->post('gend'),
					'weight' =>$this->input->post('weight'),
					'age' =>$this->input->post('age'),
					'contprotocolspeed' =>$this->input->post('speed'),
					'contprotocolvo2' =>$this->input->post('contvo2maxvalue_save'),
	                'discontprotocolspeed' =>$this->input->post('discontprotocolspeed'),
					'discontprotocolvo2' =>$this->input->post('discontvo2maxvalue_save'),
					'ergometer' =>$this->input->post('ergometer'),
					'ergometervo2max'=>$this->input->post('ergovo2maxvalue_save'));
		
		
		
		// code added DAYA

     $code_client = array('id' =>$c_id,
					'Continous_vo2max_treadmill' =>$this->input->post('contvo2maxvalue_save'),
	                'Discontinous_vo2max_distreadmill' =>$this->input->post('discontvo2maxvalue_save'),
					'Ergometer_Continous_vo2max'=>$this->input->post('ergovo2maxvalue_save')
					);					

		  if($this->isExist( 'client_person_info',  $_SESSION['userid'] )){
		  $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_person_info', $code_client); 
                     //return $_SESSION['userid'];
                }else{
				      $this->db->insert('client_person_info', $code_client); 
                    //  return $this->db->insert_id();    
				}
		
		
		
		
		
		
		//print_r($code);
            if($this->isExist( 'trademill_test',  $_SESSION['userid'] )){
		$this->db->where('c_id', $_SESSION['userid']);
                     $this->db->update('trademill_test', $code); 
                     return $_SESSION['userid'];
                }else{
				      $this->db->insert('trademill_test', $code); 
                      return $this->db->insert_id();    
				}
	}
  
  
  
  
  
  
  
  
  
  
  	//Get Sports Fitness Norms for Fitness Module.
	public function sports_fitness_norms($gender,$sportValue)
	{
    if($gender=='Male')
	{
	$gender='M';	
	}
	else
	{
	$gender='F';	
	}
	$this->db->select("*");	     
    $this->db->from('sports_fitness_norms');
    $this->db->where('gender',$gender);
	$this->db->where('sports',$sportValue);
    $query = $this->db->get();
	$ret = $query->row();	
    return $ret;
   } 
	
  
  

	 //Register New Virtual Person
  function register_VP()
  {
   
      $dob = $_SESSION['user_dob'];
      $code = array('first_name' =>$_SESSION['user_first_name'],'last_name' =>$_SESSION['user_last_name'],'email' =>'' ,'p_name' =>'','p_desc' =>'','gender' =>$_SESSION['user_gender'],'dob' => $dob,'country' =>$_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'random_mass'=>$_SESSION['MASS'],'random_bmi'=>$_SESSION['BMI'],'height'=>$_SESSION['HEIGHT']);
		
            if($this->isExist( 'client_info',  $_SESSION['userid'] )){
		$this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_info', $code); 
					 $uid=$_SESSION['userid'];
					 $vp_age=$_SESSION['vp_age'];
					 //Code Client Update
					 $code_client = array('id'=>$uid,'firstname' => $_SESSION['user_first_name'],'lastname' =>$_SESSION['user_last_name'],'Gender' => $_SESSION['user_gender'],'dob' => $dob,'project'=>'','access_code'=>'','country' => $_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'Age'=>$vp_age,'subpopulation'=>$_SESSION['subpopulation'],'Mass'=>round($_SESSION['MASS'],1),'bmi'=>round($_SESSION['BMI'],1),'Height'=>round($_SESSION['HEIGHT'],1));
					 $this->db->where('id', $_SESSION['userid']);
                     $this->db->update('client_person_info', $code_client); 
					 
					 
					 
                     return $_SESSION['userid'];
                }else{
					
                     $vp_age=$_SESSION['vp_age'];
					 $this->db->insert('client_info', $code); 
                     $uid=$this->db->insert_id();
					 //Added for client_person_info	
					 $code_client = array('id'=>$uid,'firstname' => $_SESSION['user_first_name'],'lastname' =>$_SESSION['user_last_name'],'Gender' => $_SESSION['user_gender'],'dob' => $dob,'project'=>'','access_code'=>'','country' => $_SESSION['user_country'],'occupation' =>$_SESSION['user_occupation'],'Age'=>$vp_age,'subpopulation'=>$_SESSION['subpopulation'],'Mass'=>round($_SESSION['MASS'],1),'bmi'=>round($_SESSION['BMI'],1),'Height'=>round($_SESSION['HEIGHT'],1));
					  $this->db->insert('client_person_info',$code_client); 
					 
					 return $uid; 
  }
  }

  function isExist( $table, $c_id )
        {
          if($table=='client_info'  || $table=='client_person_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          if($query->num_rows())return true;
		return false;
        }
		
		
   public function calculate_norm($vo2max)
  {
	   //echo $vo2max;
	   $object1 = new PHPExcel_Calculation_Statistical(); 
	   $prob_biketest=$this->frand(0,1);
       $prob_biketest=round($prob_biketest,9);
	    //(=NORM.INV(RAND(),0,0.004*VO2max)	
       $bikeres=$object1->NORMINV($prob_biketest,0,0.004*$vo2max);	  
	   return $bikeres; 
  }			
   		
		
	
	
	//GET VERTICAL JUMP
  public function get_Vertical_jump(){
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
		$subpopulation = $_SESSION['subpopulation'];
		$age_range = $_SESSION['age_range'];
		$gender = $_SESSION['user_gender'];
   $fitness_array=$this->fitness_module_norms($gender,$age_range);
      $MeanVJ=$fitness_array['MeanVJ'];
      $SDVJ=$fitness_array['SDVJ'];
  
   //For Subpopulation
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
  //For General
  if($subpopulation=='General')
  {
      $MeanVJ = $MeanVJ - (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanVJ = $MeanVJ + (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
    $MeanVJ = $MeanVJ + (1.5 * $SDVJ);   
    $SDVJ= 0.5 * $SDVJ;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanVJ,$SDVJ);
      $randomprob=round($x,9);   
      $vj=$randomprob;
	  
	 // echo $vj; die;
	  
      //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_meansd($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($vj - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
	$vj=round($vj,1);
    $vj = floor($vj*2)/2;
	
	//echo $vj; die;
	
	$_SESSION['vj']=$vj;
    $_SESSION['perform_percent']=$perform_percent;
    $_SESSION['z_score']=$z_score;
  return;
  }//END OF VJ ASSIGNFUNCTION 
    //Flight : Contact time ratio
  public function get_Flight_ratio(){
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
        $age_range = $_SESSION['age_range'];
        $gender = $_SESSION['user_gender'];
    $fitness_array=$this->fitness_module_norms($gender,$age_range);
	
	
	
    $MeanFTCT=$fitness_array['MeanFTCT'];
    $SDFTCT=$fitness_array['SDFTCT'];
  if($subpopulation=='Sedentary')
  {
   $MeanFTCT =$MeanFTCT - (1.5 * $SDFTCT);   
    $SDFTCT= 0.65 * $SDFTCT;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanFTCT = $MeanFTCT - (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanFTCT = $MeanFTCT + (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
   $MeanFTCT = $MeanFTCT + (1.5 * $SDFTCT);   
    $SDFTCT= 0.5 *$SDFTCT;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanFTCT,$SDFTCT);
      $randomprob=round($x,9);   
      $FTCT =$randomprob;
      
    //If FTCT < -0.0138*(Age) + 1.62 then FTCT = -0.0138*(Age) + 1.62
      if($gender=='M' && ($FTCT < (-0.0138*($age) + 1.62)))
      {
      $FTCT = -0.0138*($age) + 1.62 ;  
      }
      //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
      if($gender=='M' && ($FTCT > (-0.0403*($age) + 5.1)))
      {
      $FTCT = -0.0403*($age) + 5.1 ;  
      }
//      FOR FEMALE
//       If FTCT < -0.0094*(Age) + 1.16 then FTCT = -0.0094*(Age) + 1.16
      if($gender=='F' && ($FTCT < (-0.0094*($age) +1.16)))
      {
      $FTCT = -0.0094*($age) + 1.16 ;  
      }
    //  If FTCT > -0.0295*(Age) + 3.8107 then FTCT = -0.0295*(Age) + 3.8
    if($gender=='F' && ($FTCT > (-0.0295*($age) + 3.8107)))
      {
      $FTCT = -0.0295*($age) + 3.8 ;  
      }
      
    $_SESSION['FTCT']=round($FTCT,2);
    //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_flight($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($FTCT - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    
    $_SESSION['flight_percent']=$perform_percent;
    $_SESSION['flight_z_score']=$z_score;
  return;
  }
 
 //Flight : Contact time ratio
  public function peak_power_test()
     {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
    $fitness_array=$this->fitness_module_norms($gender,$age_range);
    $MeanPPWKg=$fitness_array['MeanPPWKg'];
    $SDPPWKg=$fitness_array['SDPPWKg'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanPPWKg =$MeanPPWKg - (1.5 *  $SDPPWKg);   
    $SDFTCT= 0.65 *  $SDPPWKg;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanPPWKg = $MeanPPWKg - (0.5 * $SDPPWKg);   
    $SDPPWKg= 0.75 * $SDPPWKg;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanPPWKg = $MeanPPWKg + (0.5 * $SDPPWKg);   
    $SDFTCT= 0.75 * $SDPPWKg;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
   $MeanPPWKg = $MeanPPWKg + (1.5 * $SDPPWKg);   
    $SDPPWKg= 0.5 *$SDPPWKg;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanPPWKg,$SDPPWKg);
      $randomprob=round($x,9);   
      $PPWKg =$randomprob;
      
    //If FTCT < -0.0138*(Age) + 1.62 then FTCT = -0.0138*(Age) + 1.62
      if($gender=='M' && ($PPWKg < (-0.0763 * ($age) + 9.3 )))
      {
      $PPWKg = -0.0763 * ($age) + 9.3 ;  
      }
      //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
      if($gender=='M' && ($PPWKg > (-0.2425 * ($age) + 31)))
      {
      $PPWKg = -0.2425 * ($age) + 31 ;  
      }
//      FOR FEMALE
//      
      if($gender=='F' && ($PPWKg < (-0.0577 * ($age) + 6.6)))
      {
      $PPWKg = -0.0577 * ($age) + 6.6 ;  
      }
    // If PPWKg  > -0.1994*(Age) + 24.2 then PPWKg = -0.1994*(Age) + 24.2 
    if($gender=='F' && ($PPWKg > (-0.1994 *($age) +  24.2)))
      {
      $PPWKg = -0.1994 *($age) +  24.2 ;  
      }
      
      $PPWKg=round($PPWKg,2);
      $_SESSION['PPWKg']=$PPWKg;
    //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_pickpower($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($PPWKg - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    /*
    
    */
    $weight=$_SESSION['MASS'];
    $pickpower_vo2Max = $PPWKg / $weight ;
    $pickpower_vo2Max=round($pickpower_vo2Max,2);
            
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    $watts_val=$PPWKg * $weight;
    $_SESSION['pickpower_percent']=$perform_percent;
    $_SESSION['pickpower_z_score']=$z_score;
    $_SESSION['pickpower_vo2Max']=$pickpower_vo2Max;
	$_SESSION['watts_val']=round($watts_val,1);
    return;
  }

     //STRENGTH TESTS
  public function strength_test()
     {
      $setagerange = $this->getagerange();
        $object1 = new PHPExcel_Calculation_Statistical();
        $age = $_SESSION['vp_age'];
        $mass=$_SESSION['MASS'];
        $subpopulation = $_SESSION['subpopulation'];
        $age_range = $_SESSION['age_range'];
        $gender = $_SESSION['user_gender'];
		 $BMI=round($_SESSION['BMI'],2);
        $strenght_array = $this->fitness_module_norms($gender, $age_range);
        $MeanGS = $strenght_array['MeanGS'];
        $SDGS = $strenght_array['SDGS'];

        if($gender=='M')
      {
          if ($subpopulation == 'Sedentary') {
            $MeanGS = $MeanGS + 0.2 * $BMI - (1.5 * $SDGS);
            $SDGS = 0.65 * $SDGS;
        }

        //For General
        if ($subpopulation == 'General') {
           $MeanGS = $MeanGS + 0.2 * $BMI - (0.5 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
        //For Active
        if ($subpopulation == 'Active') {
            $MeanGS = $MeanGS + 0.2 * $BMI + (0.3 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }

//For Active
        if ($subpopulation == 'Athlete') {
           //If sub-population = ‘Athlete’ then MeanGS = (MeanGS+0.2*BMI + (0.5* SDGS))
           //If sub-population = ‘Athlete’ then SDGS = (0.5* SDGS)
             $MeanGS = $MeanGS + 0.2 * $BMI + (0.2 * $SDGS);
            $SDGS = 0.5 * $SDGS;
        }
      }  
      //FOR FEMALE 
      if($gender=='F')
      {
        if ($subpopulation == 'Sedentary') {
            //If sub-population = ‘Sedentary’ then MeanGS = (MeanGS+0.06*BMI – (1.5* SDGS))
            //If sub-population = ‘Sedentary’ then SDGS = (0.65* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI - (1.5 * $SDGS);
            $SDGS = 0.65 * $SDGS;
        }

        //For General
        if ($subpopulation == 'General') {
           //If sub-population = ‘General’ then MeanGS = (MeanGS+0.06*BMI – (0.5* SDGS))
           //If sub-population = ‘General’ then SDGS = (0.75* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI - (0.5 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
        //For Active
        if ($subpopulation == 'Active') {
            //If sub-population = ‘Active’ then MeanGS = (MeanGS+0.06*BMI + (0.3* SDGS))
           //If sub-population = ‘Active’ then SDGS = (0.75* SDGS)
           $MeanGS = $MeanGS + 0.06 * $BMI + (0.3 * $SDGS);
            $SDGS = 0.75 * $SDGS;
        }
    //For Active
        if ($subpopulation == 'Athlete') {
           //If sub-population = ‘Athlete’ then MeanGS = (MeanGS+0.06*BMI + (0.5* SDGS))
           //If sub-population = ‘Athlete’ then SDGS = (0.5* SDGS)
            $MeanGS = $MeanGS + 0.06 * $BMI + (0.5 * $SDGS);
            $SDGS = 0.5 * $SDGS;
        }
      }
        $probability = $this->frand(0, 1);
        $probability = round($probability, 9);

        $leftgsval = $object1->NORMINV($probability, $MeanGS, $SDGS);
        $GSLEFT = round($leftgsval, 1);

        
        if ($gender == 'M' && ($GSLEFT < (-0.1778 * ($age) + 29.7))) {
            $GSLEFT = -0.1778 * ($age) + 29.7;
        }
        //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
        if ($gender == 'M' && ($GSLEFT > (-0.5132 * ($age) + 101.4))) {
            $GSLEFT = -0.5132 * ($age) + 101.4;
        }
//      FOR FEMALE
//      
        if ($gender == 'F' && ($GSLEFT < (-0.0857 * ($age) + 16.7))) {
            $GSLEFT = -0.0857 * ($age) + 16.7;
        }
        // If GS > -0.2706*(Age) + 59.5 then GS = -0.2706*(Age) + 59.5
	if ($gender == 'F' && ($GSLEFT > (-0.2706 * ($age) + 59.5))) {
            $GSLEFT = -0.2706 * ($age) + 59.5;
        }

        $GSLEFT = round($GSLEFT, 1);
        //RIGHT GS
        //RIGHT GS = (NORM.INV(RAND(),LEFT GS, 1.5),   [round to 1 decimal place]
        $probabilityright = $this->frand(0, 1);
        $probabilityright = round($probabilityright, 9);
        $rightgsval = $object1->NORMINV($probabilityright, $GSLEFT, 1.5);
        $GSRIGHT = round($rightgsval, 1);
		if ($gender == 'M' && ($GSRIGHT < (-0.1778 * ($age) + 29.7))) {
            $GSRIGHT = -0.1778 * ($age) + 29.7;
        }
        //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
        if ($gender == 'M' && ($GSRIGHT > (-0.5132 * ($age) + 101.4))) {
            $GSRIGHT = -0.5132 * ($age) + 101.4;
        }
//      FOR FEMALE
//      
        if ($gender == 'F' && ($GSRIGHT < (-0.0857 * ($age) + 16.7))) {
            $GSRIGHT = -0.0857 * ($age) + 16.7;
        }
        // If GS > -0.2706*(Age) + 59.5 then GS = -0.2706*(Age) + 59.5
	if ($gender == 'F' && ($GSRIGHT > (-0.2706 * ($age) + 59.5))) {
            $GSRIGHT = -0.2706 * ($age) + 59.5;
        }
        
        
        $_SESSION['GSLEFT'] = $GSLEFT;
        $_SESSION['GSRIGHT'] =$GSRIGHT;
        $total = $GSLEFT + $GSREGHT;
        $SESSION['avgGrip'] = $total;

//Get Mead and SD for Zscore
        $get_meansd = $this->getzscore_strength($age_range, $gender);
        $mean = $get_meansd['mean'];
        $sd = $get_meansd['sd'];

        $grip_score = ($mean - $total) / $sd;
        $z_score = round($grip_score, 3);

        $perform_percent = $object1->NORMSDIST($z_score);
        $perform_percent = $perform_percent * 100;
        $perform_percent = 100 - $perform_percent;
        $perform_percent = round($perform_percent, 3);

        $_SESSION['gripRank'] = $perform_percent;
        $_SESSION['strength_z_score'] = $z_score;
     
        //BENCH PRESS
    //strength testing calculations
        if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench * (1.5-0.6) + 0.6;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(1.0-0.6)+0.6;
        } 
          if ($subpopulation == 'Athlete') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(1.5-1.2)+1.2;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench * (0.9 - 0.45)+0.45;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(0.6-0.45)+0.45;
        } 
          if ($subpopulation == 'Athlete') {
            $randbench = $this->frand(0, 1);
            $randbench = round($randbench, 9);
             $SWratio =$randbench *(0.9-0.7)+0.7;
        } 
      }
      //[ie., ADJS:W = S : W * (((25-AGE)*0.01025)+1)]  
      if($age>=25)
        {
         $ADJSW = $SWratio * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $SWratio;  
        }
      $BPadjMAX=$mass * $ADJSW;  
      $BPadjMAX=round($BPadjMAX,1);
      $BPreps = rand(5,15);
      //(BPadjMAX)*(1.0278-(BPreps*0.0278)) 
      $BPmass =$BPadjMAX *(1.0278-($BPreps*0.0278));
      $BPmass=round($BPmass,1);
      if($BPmass<10)
      {
       $BPmass=10;   
      }
       $_SESSION['BPmass'] = $BPmass;
       $_SESSION['BPreps'] = $BPreps;
       $_SESSION['SWratio'] = $SWratio;
     
//Arm curl	
//ACmass = arm curl weight lifted 			
//ACreps = number of arm curl reps			
      if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl * (0.7 - 0.25) + 0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.5 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(0.7-0.55)+0.55
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.7-0.55)+0.55;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl * ( 0.5 - 0.14)+0.14;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.25-0.14)+0.14;
        } 
          if ($subpopulation == 'Athlete') {
            $randarmcurl = $this->frand(0, 1);
            $randarmcurl = round($randarmcurl, 9);
             $AC_SW =$randarmcurl *(0.5-0.35)+0.35;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $AC_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $AC_SW;  
        }
      $ACadjMAX=$mass * $ADJSW;  
      $ACadjMAX=round($ACadjMAX,1);
      $ACreps = rand(5,15);
      //(BPadjMAX)*(1.0278-(BPreps*0.0278)) 
      $ACmass =$ACadjMAX *(1.0278-($ACreps*0.0278));
      $ACmass=round($ACmass,1);
      if($ACmass<4)
      {
       $ACmass=4;   
      } 
      $_SESSION['ACmass'] = $ACmass;
      $_SESSION['ACreps'] = $ACreps;
     
     //LATERAL PULLDOWN
      //LPDmass = lat pulldown weight lifted		
      //LPDreps = number of lat pulldown reps	
     if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //[ie., in excel this is done using  LPDS:W = RAND( )*( 1.2 - 0.75)+0.75
            $LPD_SW =$rand_lateral * (1.2 - 0.75) + 0.75;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
           // LPDS : W = RAND( )*(0.9-0.75)+0.75
            $LPD_SW =$rand_lateral *(0.9 - 0.75) + 0.75;
        } 
          if ($subpopulation == 'Athlete') {
            //LPDS : W = RAND( )*(1.2-1.0)+1.0
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
             $LPD_SW =$rand_lateral *(1.2 - 1.0) + 1.0;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //LPDS:W = RAND( )*( 0.85 - 0.4)+0.4
            $LPD_SW =$rand_lateral * (0.85 - 0.4) + 0.4;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //RAND( )*(0.55-0.4)+0.4 
            $LPD_SW =$rand_lateral *(0.55 - 0.4) + 0.4;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_lateral = $this->frand(0, 1);
            $rand_lateral = round($rand_lateral, 9);
            //RAND( )*(0.85-0.7)+0.7
             $LPD_SW =$rand_lateral *(0.85 - 0.7) + 0.7;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LPD_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LPD_SW;  
        }
      $LPDadjMAX =$mass * $ADJSW;  
      $LPDadjMAX=round($LPDadjMAX,1);
      $LPDreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LPDmass =$LPDadjMAX *(1.0278-($LPDreps*0.0278));
      $LPDmass=round($LPDmass,1);
      if($LPDmass<8)
      {
       $LPDmass=8;   
      } 
      $_SESSION['LPDmass'] = $LPDmass;
      $_SESSION['LPDreps'] = $LPDreps;
      
        //LEG PRESS
       //LPmass = leg press weight lifted		
      //LPreps = number of leg press reps		
 if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //RAND( )*( 3.0 - 1.2)+1.2
            $LP_SW =$rand_legpress * ( 3.0 - 1.2) + 1.2;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
           // LPS : W = RAND( )*(1.9-1.2)+1.2
            $LP_SW =$rand_legpress *(1.9 - 1.2) + 1.2;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(3.0-2.3)+2.3
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
             $LP_SW =$rand_legpress *(3.0-2.3) + 2.3;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //LPS:W = RAND( )*( 2.7 - 0.9)+0.9
            $LP_SW =$rand_legpress * (2.7 - 0.9) + 0.9;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legpress = $this->frand(0, 1);
            $rand_legpress = round($rand_legpress, 9);
            //RAND( )*(1.6-0.9)+0.9
            $LP_SW =$rand_legpress *(1.6-0.9) + 0.9;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legpress = $this->frand(0, 1);
            $rand_lateral = round($rand_legpress, 9);
            //RAND( )*(2.7-2.1)+2.1
             $LP_SW =$rand_legpress *(2.7 - 2.1) + 2.1;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LP_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LP_SW;  
        }
      $LPadjMAX  =$mass * $ADJSW;  
      $LPadjMAX=round($LPadjMAX,1);
      $LPreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LPmass =$LPadjMAX *(1.0278-($LPreps*0.0278));
      $LPmass=round($LPmass,1);
      if($LPmass < 15)
      {
       $LPmass=15;   
      } 
      $_SESSION['LPmass'] = $LPmass;
      $_SESSION['LPreps'] = $LPreps;
      
      //LEG EXTENSION
      //LEmass  = leg extension weight lifted
      //LEreps = number of leg extension reps
      if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //RAND( )*( 0.8 – 0.35)+0.35
            $LE_SW =$rand_legext *(0.8 - 0.35 ) + 0.35;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
           // LES : W = RAND( )*(0.5-0.35)+0.35
            $LE_SW =$rand_legext *(0.5 - 0.35) + 0.35;
        } 
          if ($subpopulation == 'Athlete') {
            //LES : W = RAND( )*(0.8-0.6)+0.6
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            $LE_SW =$rand_legext *(0.8 - 0.6) + 0.6;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES:W = RAND( )*( 0.7 - 0.25)+0.25
            $LE_SW =$rand_legext * ( 0.7 - 0.25) + 0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES : W = RAND( )*(0.45-0.25)+0.25
            $LE_SW =$rand_legext *(0.45 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legext = $this->frand(0, 1);
            $rand_legext = round($rand_legext, 9);
            //LES : W = RAND( )*(0.7-0.5)+0.5
             $LE_SW =$rand_legext * (0.7 - 0.5) + 0.5;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LE_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LE_SW;  
        }
      $LEadjMAX   =$mass * $ADJSW;  
      $LEadjMAX=round($LEadjMAX,1);
      $LEreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LEmass =$LEadjMAX *(1.0278-($LEreps*0.0278));
      $LEmass=round($LEmass,1);
      if($LEmass < 8)
      {
       $LEmass=8;   
      } 
      $_SESSION['LEmass'] = $LEmass;
      $_SESSION['LEreps'] = $LEreps;

    //LEG CURL
     //LCmass = leg curl weight lifted 
     //LCreps = number of leg curl reps 
         if($gender=='M')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS:W = RAND( )*( 0.7 – 0.25)+0.25
            $LC_SW =$rand_legcurl *(0.7 - 0.25)+0.25;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legcurl = $this->frand(0,1);
            $rand_legcurl = round($rand_legcurl,9);
           // RAND( )*(0.45-0.25)+0.25
            $LC_SW =$rand_legcurl *(0.45 - 0.25) + 0.25;
        } 
          if ($subpopulation == 'Athlete') {
            //RAND( )*(0.7-0.5)+0.5
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            $LC_SW =$rand_legcurl *(0.7 - 0.5) + 0.5;
        } 
      }
         if($gender=='F')
        {
         if ($subpopulation == 'General' || $subpopulation == 'Active') {
             $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //RAND( )*( 0.6 - 0.15)+0.15
            $LC_SW =$rand_legcurl * ( 0.6 - 0.15) + 0.15;
        } 
        //
        if ($subpopulation == 'Sedentary') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS : W = RAND( )*(0.35-0.15)+0.15
            $LC_SW =$rand_legcurl *(0.35 - 0.15) + 0.15;
        } 
          if ($subpopulation == 'Athlete') {
            $rand_legcurl = $this->frand(0, 1);
            $rand_legcurl = round($rand_legcurl, 9);
            //LCS : W = RAND( )*(0.6-0.4)+0.4
             $LC_SW =$rand_legcurl * (0.6 - 0.4) + 0.4;
        } 
      } 
       if($age>=25)
        {
         $ADJSW = $LC_SW * (((25-$age)*0.01025)+1);  
        }
        else
        {
         $ADJSW= $LC_SW;  
        }
      $LCadjMAX   =$mass * $ADJSW;  
      $LCadjMAX=round($LCadjMAX,1);
      $LCreps = rand(5,15);
     //LPDmass = (LPDadjMAX)*(1.0278-(LPDreps*0.0278)) 
      $LCmass =$LCadjMAX *(1.0278-($LCreps*0.0278));
      $LCmass=round($LCmass,1);
      if($LCmass < 4)
      {
       $LCmass=4;   
      } 
      $_SESSION['LCmass'] = $LCmass;
      $_SESSION['LCreps'] = $LCreps;

            return;
      }
    
   
	
      //30s Total Work [kJ]
  public function total_work()
     {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
    $fitness_array=$this->fitness_module_norms($gender,$age_range);
    $MeanTW=$fitness_array['MeanTW'];
    $SDTW=$fitness_array['SDTW'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanTW =$MeanTW - (1.5 *  $SDTW);   
    $SDTW= 0.65 *  $SDTW;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanTW = $MeanTW - (0.5 * $SDTW);   
    $SDTW= 0.75 * $SDTW;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanTW = $MeanTW + (0.5 * $SDTW);   
    $SDTW= 0.75 * $SDTW;
  }  
  
//For Athlete
  if($subpopulation=='Athlete')
  {
   $MeanTW = $MeanTW + (1.5 * $SDTW);   
    $SDTW= 0.5 *$SDTW;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanTW,$SDTW);
      $randomprob=round($x,9);   
      $TW =$randomprob;
      
    //If TW < -0.00121*(Age) + 0.13765 then TW = -0.00121*(Age) + 0.13765
      if($gender=='M' && ($TW < (-0.00121 * ($age) + 0.13765 )))
      {
      $TW = -0.00121 * ($age) + 0.13765; 
      }
      //If TW > -0.00451*(Age) + 0.54384 then TW = -0.00451*(Age) + 0.54384
      if($gender=='M' && ($TW > (-0.00451 * ($age) + 0.54384)))
      {
      $TW = -0.00451 * ($age) + 0.54384 ;  
      }
//      FOR FEMALE
//    If TW < -0.00099*(Age) + 0.11769 then TW = -0.00099*(Age) + 0.11769  
      if($gender=='F' && ($TW < (-0.00099 * ($age) + 0.11769)))
      {
      $TW = -0.00099 * ($age) + 0.11769;  
      }
    // If TW > -0.00388*(Age) + 0.48017 then TW = -0.00388*(Age) + 0.48017
      if($gender=='F' && ($TW > (-0.00388 *($age) +  0.48017)))
      {
      $TW = -0.00388 *($age) +  0.48017 ;  
      }
      
     $TW=round($TW,4);
     $_SESSION['TW']=$TW;
     $weight=$_SESSION['MASS'];
     $valuekj=$TW*$weight;
     $_SESSION['valuekj']=round($valuekj,1);
 
    return;
  }
      	
         //Predicted VO2max and PWC
  public function get_VO2max()
     {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
      $mass = $_SESSION['MASS'];
      if($gender=='M')
      {
       //HRmax = (NORM.INV(RAND(),209-0.69*AGE, 10) [no decimal places]
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $x=$object1->NORMINV($probability,209-0.69*$age,10);
      $HRmax=round($x);  
      }
      if($gender=='F')
      {
       //HRmax = (NORM.INV(RAND(),209-0.69*AGE, 10) [no decimal places]
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $x=$object1->NORMINV($probability,205-0.75*$age,10);
      $HRmax=round($x);  
      }
    if($HRmax < 135)
    {
     $HRmax=135;   
    }
    if($HRmax > 220)
    {
     $HRmax=220;   
    }  
    
    //PREDICTED Resting HR [HRrest]
    //HRrest = (NORM.INV(RAND(),0.2*AGE+55, 2) [no decimal places]
      $prob_hrrest=$this->frand(0,1);
      $prob_hrrest=round($prob_hrrest,9); 
      $HRrest=$object1->NORMINV($prob_hrrest,0.2*$age+55,2); 
    if($subpopulation=='Athlete')
    {
      $prob_hrrest=$this->frand(0,1);
      $prob_hrrest=round($prob_hrrest,9); 
      $HRrest=$object1->NORMINV($prob_hrrest,0.2*$age+50,2);     
    }
   
    $HRrest=round($HRrest);
   
// Predicted HRmax can be entered in to the field on the screen in the ‘Known HRmax [BPM] field 
    //Calculate the HR reserve [HRreserve] :
    //HRreserve = HRmax – HRrest  
    $HRreserve=$HRmax - $HRrest;
    //Calculate the HR for each of the three stages on the screen :
    //STAGE 1 HR = (NORM.INV(RAND(),0.5, 0.015)* HRreserve + HRrest    [no decimal places]
      $prob_stage1=$this->frand(0,1);
      $prob_stage1=round($prob_stage1,9); 
      $stage1HR=$object1->NORMINV($prob_stage1,0.5,0.015) * $HRreserve + $HRrest; 
      $stage1HR=round($stage1HR);
    //STAGE 2 HR = (NORM.INV(RAND(),0.65, 0.015)* HRreserve + HRrest [no decimal places]
      $prob_stage2=$this->frand(0,1);
      $prob_stage2=round($prob_stage2,9); 
      $stage2HR=$object1->NORMINV($prob_stage2,0.65, 0.015) * $HRreserve + $HRrest; 
      $stage2HR=round($stage2HR);
      //STAGE 3 HR = (NORM.INV(RAND(),0.8, 0.015)* HRreserve + HRrest [no decimal places]
      $prob_stage3=$this->frand(0,1);
      $prob_stage3=round($prob_stage3,9); 
      $stage3HR=$object1->NORMINV($prob_stage3,0.8, 0.015) * $HRreserve + $HRrest; 
      $stage3HR=round($stage3HR);
      //If STAGE 2 HR – STAGE 1 HR < 8 then STAGE 2 HR = STAGE 1 HR + 8
      if(($stage2HR-$stage1HR) < 8)
      {
      $stage2HR=$stage1HR +8;    
      }  
    //If STAGE 3 HR – STAGE 2 HR < 8 then STAGE 3 HR = STAGE 2 HR + 8
      if(($stage3HR-$stage2HR) < 8)
      {
      $stage3HR=$stage2HR +8;    
      }  
      //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Sedentary')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 7.5)*34/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,7.5) * 34 / 38.3;     
      if($VO2max < 10)
      {
       $VO2max=10;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
       //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='General')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 8) )*37/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,8) * 37 / 38.3;     
      if($VO2max < 12)
      {
       $VO2max=12;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
        //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Active')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 53, 7) )*39/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+53,7) * 39 / 38.3;     
      if($VO2max < 15)
      {
       $VO2max=15;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
         //PREDICTED VO2MAX 
      if($gender == 'M' && $subpopulation=='Athlete')
      {
      //VO2max = (NORM.INV(RAND(),-0.33*AGE + 56, 5) )*45/38.3
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.33*$age+56,5) * 45 / 38.3;     
      if($VO2max < 40)
      {
       $VO2max=40;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
     
       //PREDICTED VO2MAX Female
      if($gender == 'F' && $subpopulation=='Sedentary')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 42, 7.5)*27/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+42,7.5) * 27/31.1;     
      if($VO2max < 10)
      {
       $VO2max=10;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
       //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='General')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 44, 8) )*30/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+44,8) * 30/31.1;     
      if($VO2max < 12)
      {
       $VO2max=12;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
        //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='Active')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 44, 7) )*32/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+44,7) *32/31.1;     
      if($VO2max < 15)
      {
       $VO2max=15;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
         //PREDICTED VO2MAX 
      if($gender == 'F' && $subpopulation=='Athlete')
      {
      //VO2max = (NORM.INV(RAND(),-0.29*AGE + 46, 5) )*38/31.1
      $prob_vo2max=$this->frand(0,1);
      $prob_vo2max=round($prob_vo2max,9); 
      $VO2max=$object1->NORMINV($prob_vo2max,-0.29*$age+46,5) * 38 / 31.1;     
      if($VO2max < 32)
      {
       $VO2max=32;   
      }
     $VO2maxLM  = $VO2max*$mass/1000 ;//[ie., litres per minute]
      }
      
     //PREDICTED VO2rest  
      if($gender=='M')
      {
      //VO2rest = 0.005*MASS + 0.1	[ in litres per minute]
       $VO2rest= 0.005 * $mass + 0.1;  
      }
      if($gender=='F')
      {
      //VO2rest = 0.005*MASS 	[ in litres per minute]
       $VO2rest= 0.005 * $mass;  
      }
      //NOW generate a regression equation predicting VO2 from HR
      //SLOPEVO2 = (HRmax – HRrest) / (VO2maxLM – VO2rest) (changed according to new document.)
      $SLOPEVO2= ($VO2maxLM - $VO2rest)/($HRmax - $HRrest);
	  $Intercept=$VO2rest-($SLOPEVO2 * $HRrest);	
	//The equation = VO2 = SLOPEVO2 * HR + VO2rest
      $stage1VO2 =$SLOPEVO2 * $stage1HR + $Intercept;
      $stage2VO2 =$SLOPEVO2 * $stage2HR + $Intercept;
      $stage3VO2 =$SLOPEVO2 * $stage3HR + $Intercept;
      //Converting VO2 to Watts :
      //WATTS = (VO2 – VO2rest) / (NORM.INV(RAND(),0.01197,0.001))  [this is a random adjustment for differences in mechanical efficiency]
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
	  $useforwatt=$object1->NORMINV($prob_watt,0.01197,0.001);
      $stage1WATT=($stage1VO2 - $VO2rest)/ $useforwatt;
        //Stage 2 WATT
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
      $stage2WATT=($stage2VO2 - $VO2rest)/$useforwatt;
      //Stage 3 WATT
      $prob_watt=$this->frand(0,1);
      $prob_watt=round($prob_watt,9); 
      $stage3WATT=($stage3VO2 - $VO2rest)/$useforwatt;
      
      //PREDICTED RPE at rest [RPErest]
      //RPEgradient = (NORM.INV(RAND(),0.2,0.007)) 
      $prob_RPE=$this->frand(0,1);
      $prob_RPE=round($prob_RPE,9); 
      $RPEgradient=$object1->NORMINV($prob_RPE,0.2,0.007);
      
    //RPEintercept = (NORM.INV(RAND(),0,0.5)) 
       $prob_intercept=$this->frand(0,1);
      $prob_intercept=round($prob_intercept,9); 
      $RPEintercept=$object1->NORMINV($prob_intercept,0,0.5);
      //RPE [at each stage] = RPEgradient * [100 - (((HRmax-STAGE 1 HR)/(HRmax – HRrest))*100)] + RPEintercept  [round to nearest 0.5 RPE unit]
     // [example 1: STAGE 1 HR = 110 BPM and HRmax = 185 BPM and HRrest = 60 BMP. 
   //Therefore,  RPE at this stage = 100 – [(185 – 110) / (185 – 60) * 100] + RPEintercept  
  //= 100 – (75/125 *100) = 100 - 60  = 40% [HRreserve]. This means RPE = 0.2  * 40 = 8. [assume RPEgradient = 0.2 and RPEintercept = 0]  
      $stage1RPE=$RPEgradient*(100 - (($HRmax - $stage1HR)/($HRmax-$HRrest))*100)+$RPEintercept; 
      if($stage1RPE <=8)
      {
       $stage1RPE=8;   
      }
      if($stage1RPE >20)
      {
       $stage1RPE=20;   
      }
       //Stage 2 RPE
      $stage2RPE=$RPEgradient*(100 - (($HRmax - $stage2HR)/($HRmax-$HRrest))*100)+$RPEintercept;
    if($stage2RPE <=8)
      {
       $stage2RPE=8;   
      }
      if($stage2RPE >20)
      {
       $stage2RPE=20;   
      }  
    //Stage 3 RPE
      $stage3RPE=$RPEgradient*(100 - (($HRmax - $stage3HR)/($HRmax-$HRrest))*100)+$RPEintercept;
      if($stage3RPE <=8)
      {
       $stage3RPE=8;   
      }
      if($stage3RPE >20)
      {
       $stage3RPE=20;   
      }  
      $_SESSION['HRmax']=$HRmax;
      $_SESSION['stage1HR']=$stage1HR;
      $_SESSION['stage2HR']=$stage2HR;
      $_SESSION['stage3HR']=$stage3HR;
      $_SESSION['VO2max']=round($VO2max,1);
	  $_SESSION['VO2maxLM']=round($VO2maxLM,2);
	   $_SESSION['VO2rest']=round($VO2rest,2);
      $_SESSION['HRrest']= round($HRrest);
      $_SESSION['stage1WATT']=round($stage1WATT);
      $_SESSION['stage2WATT']=round($stage2WATT);
      $_SESSION['stage3WATT']=round($stage3WATT);
      $_SESSION['stage1RPE']= round($stage1RPE);
      $_SESSION['stage2RPE']= round($stage2RPE);
      $_SESSION['stage3RPE']= round($stage3RPE);
	  $_SESSION['useforwatt']=$useforwatt; 
      
      return;
  }
	// 20 m shuttle test  
  public function calculate_maod()
  {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
      $mass = $_SESSION['MASS'];    
      if($_SESSION['VO2max']=="")
      {
      $setvo2=$this->get_VO2max();    
      }
      $VO2max=$_SESSION['VO2max'];
      $VO2maxLM=$_SESSION['VO2maxLM'];
      $useforwatt=$_SESSION['useforwatt'];  
      $VO2rest=$_SESSION['VO2rest'];
    //Stage 1 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.50, 0.005)
      $prob_st1_maod=$this->frand(0,1);
      $prob_st1=round($prob_st1_maod,9); 
      //$stg1_inv=$object1->NORMINV($prob_st1,0.50,0.005);
      $stg1_inv=$object1->NORMINV($prob_st1,0.4,0.01);
      $stage1_maod=$VO2maxLM *$stg1_inv; 
     //Stage1 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st1_workload=$this->frand(0,1);
      $prob_st1_workload=round($prob_st1_workload,9); 
      $stage1_workload=(($stage1_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st1_workload,1,0.05));
      //Stage 2 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.535, 0.01)
      $prob_st2_maod=$this->frand(0,1);
      $prob_st2=round($prob_st2_maod,9); 
      //$stg2_inv=$object1->NORMINV($prob_st1,0.535,0.01);
      $stg2_inv=$object1->NORMINV($prob_st1,0.45,0.01);
      $stage2_maod=$VO2maxLM *$stg2_inv; 
     //Stage2 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st2_workload=$this->frand(0,1);
      $prob_st2_workload=round($prob_st2_workload,9); 
      $stage2_workload=(($stage2_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st2_workload,1,0.05));
      
      //Stage 3 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.57, 0.005)
      $prob_st3_maod=$this->frand(0,1);
      $prob_st3=round($prob_st3_maod,9); 
      //$stg3_inv=$object1->NORMINV($prob_st3,0.57,0.005);
      $stg3_inv=$object1->NORMINV($prob_st3,0.48,0.01);
      $stage3_maod=$VO2maxLM *$stg3_inv; 
     //Stage2 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st3_workload=$this->frand(0,1);
      $prob_st3_workload=round($prob_st3_workload,9); 
      $stage3_workload=(($stage3_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st3_workload,1,0.05));
      
      //Stage 4 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.60, 0.005)
      $prob_st4_maod=$this->frand(0,1);
      $prob_st4=round($prob_st4_maod,9); 
      //$stg4_inv=$object1->NORMINV($prob_st4,0.60,0.005);
      $stg4_inv=$object1->NORMINV($prob_st4,0.5,0.01);
      $stage4_maod=$VO2maxLM *$stg4_inv; 
     //Stage4 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st4_workload=$this->frand(0,1);
      $prob_st4_workload=round($prob_st4_workload,9); 
      $stage4_workload=(($stage4_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st4_workload,1,0.05));
      //Stage 5 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.635, 0.005)
      $prob_st5_maod=$this->frand(0,1);
      $prob_st5=round($prob_st5_maod,9); 
      //$stg5_inv=$object1->NORMINV($prob_st5,0.635,0.005);
      $stg5_inv=$object1->NORMINV($prob_st5,0.55,0.01);
      $stage5_maod=$VO2maxLM *$stg5_inv; 
     //Stage5 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st5_workload=$this->frand(0,1);
      $prob_st5_workload=round($prob_st5_workload,9); 
      $stage5_workload=(($stage5_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st5_workload,1,0.05));
      
      //Stage 6 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.67, 0.005)
      $prob_st6_maod=$this->frand(0,1);
      $prob_st6=round($prob_st6_maod,9); 
      //$stg6_inv=$object1->NORMINV($prob_st6,0.67,0.005);
      $stg6_inv=$object1->NORMINV($prob_st6,0.6,0.01);
      $stage6_maod=$VO2maxLM *$stg6_inv; 
     //Stage5 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st6_workload=$this->frand(0,1);
      $prob_st6_workload=round($prob_st6_workload,9); 
      $stage6_workload=(($stage6_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st6_workload,1,0.05));
      
      //Stage 7 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.70, 0.005)
      $prob_st7_maod=$this->frand(0,1);
      $prob_st7=round($prob_st7_maod,9); 
      //$stg7_inv=$object1->NORMINV($prob_st7,0.70,0.005);
      $stg7_inv=$object1->NORMINV($prob_st7,0.65,0.01);
      $stage7_maod=$VO2maxLM *$stg7_inv; 
     //Stage7 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st7_workload=$this->frand(0,1);
      $prob_st7_workload=round($prob_st7_workload,9); 
      $stage7_workload=(($stage7_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st7_workload,1,0.05));
      //Stage 8 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.735, 0.005)
      $prob_st8_maod=$this->frand(0,1);
      $prob_st8=round($prob_st8_maod,9); 
     // $stg8_inv=$object1->NORMINV($prob_st8,0.735,0.005);
      $stg8_inv=$object1->NORMINV($prob_st8,0.7,0.01);
      $stage8_maod=$VO2maxLM * $stg8_inv; 
     //Stage8 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st8_workload=$this->frand(0,1);
      $prob_st8_workload=round($prob_st8_workload,9); 
      $stage8_workload=(($stage8_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st8_workload,1,0.05));
      
    //Stage 9 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.77, 0.005)
      $prob_st9_maod=$this->frand(0,1);
      $prob_st9=round($prob_st9_maod,9); 
      //$stg9_inv=$object1->NORMINV($prob_st9,0.77,0.005);
      $stg9_inv=$object1->NORMINV($prob_st9,0.75,0.01);
      $stage9_maod=$VO2maxLM * $stg9_inv; 
     //Stage9 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st9_workload=$this->frand(0,1);
      $prob_st9_workload=round($prob_st9_workload,9); 
      $stage9_workload=(($stage9_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st9_workload,1,0.05));
      
     //Stage 10 MAOD [L/min field 1] = VO2maxLM [i.e., in L/min] * (NORM.INV(RAND(),0.80, 0.005)
       $prob_st10_maod=$this->frand(0,1);
      $prob_st10=round($prob_st10_maod,9); 
      //$stg10_inv=$object1->NORMINV($prob_st10,0.80,0.005);
       $stg10_inv=$object1->NORMINV($prob_st10,0.8,0.01);
      $stage10_maod=$VO2maxLM * $stg10_inv; 
     //Stage9 Workload    
    //(($stage1_maod-$VO2rest)/$useforwatt) *(NORM.INV(RAND(),1, 0.05))
      $prob_st10_workload=$this->frand(0,1);
      $prob_st10_workload=round($prob_st10_workload,9); 
      $stage10_workload=(($stage10_maod-$VO2rest)/$useforwatt)*($object1->NORMINV($prob_st10_workload,1,0.05));
      //SET VALUE TO SESSION FOR MAOD SCREEN
      $_SESSION['stage1_maod']=round($stage1_maod,3);
      $_SESSION['stage1_workload']=round($stage1_workload);
      $_SESSION['stage2_maod']=round($stage2_maod,3);
      $_SESSION['stage2_workload']=round($stage2_workload);
      $_SESSION['stage3_maod']=round($stage3_maod,3);
      $_SESSION['stage3_workload']=round($stage3_workload);
      $_SESSION['stage4_maod']=round($stage4_maod,3);
      $_SESSION['stage4_workload']=round($stage4_workload);
      $_SESSION['stage5_maod']=round($stage5_maod,3);
      $_SESSION['stage5_workload']=round($stage5_workload);
      $_SESSION['stage6_maod']=round($stage6_maod,3);
      $_SESSION['stage6_workload']=round($stage6_workload);
      $_SESSION['stage7_maod']=round($stage7_maod,3);
      $_SESSION['stage7_workload']=round($stage7_workload);
      $_SESSION['stage8_maod']=round($stage8_maod,3);
      $_SESSION['stage8_workload']=round($stage8_workload);
      $_SESSION['stage9_maod']=round($stage9_maod,3);
      $_SESSION['stage9_workload']=round($stage9_workload);
      $_SESSION['stage10_maod']=round($stage10_maod,3);
      $_SESSION['stage10_workload']=round($stage10_workload);
      
        //SCREEN 2 CALCULATION
     // =Predicted VO2max*(NORM.INV(RAND(),0.2, 0.03))
      $prob_vo2_1=$this->frand(0,1);
      $prob_vo2_1=round($prob_vo2_1,9); 
      $stage1_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_1,0.2,0.03));
      
      $prob_vo2_2=$this->frand(0,1);
      $prob_vo2_2=round($prob_vo2_2,9); 
      $stage2_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_2,0.45,0.03));
      
       $prob_vo2_3=$this->frand(0,1);
      $prob_vo2_3=round($prob_vo2_3,9); 
      $stage3_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_3,0.575,0.025));
   
      $prob_vo2_4=$this->frand(0,1);
      $prob_vo2_4=round($prob_vo2_4,9); 
      $stage4_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_4,0.625,0.025));
     
      $prob_vo2_5=$this->frand(0,1);
      $prob_vo2_5=round($prob_vo2_5,9); 
      $stage5_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_5,0.67,0.025));

      $prob_vo2_6=$this->frand(0,1);
      $prob_vo2_6=round($prob_vo2_6,9); 
      $stage6_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_6,0.72,0.02));
      //=C23*(NORM.INV(RAND(),0.765, 0.02))
      $prob_vo2_7=$this->frand(0,1);
      $prob_vo2_7=round($prob_vo2_7,9); 
      $stage7_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_7,0.765,0.02));
      $prob_vo2_8=$this->frand(0,1);
      $prob_vo2_8=round($prob_vo2_8,9); 
      $stage8_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_8,0.805,0.02));
     
      $prob_vo2_9=$this->frand(0,1);
      $prob_vo2_9=round($prob_vo2_9,9); 
      $stage9_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_9,0.84,0.02));
      $prob_vo2_10=$this->frand(0,1);
      $prob_vo2_10=round($prob_vo2_10,9); 
      $stage10_vo2=$VO2maxLM * ($object1->NORMINV($prob_vo2_10,0.875,0.02));
      $prob_vo2_11=$this->frand(0,1);
      $prob_vo2_11=round($prob_vo2_11,9); 
      $stage11_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_11,0.91,0.02));
      
      $prob_vo2_12=$this->frand(0,1);
      $prob_vo2_12=round($prob_vo2_12,9); 
      $stage12_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_12,0.93,0.02));
      
      $prob_vo2_13=$this->frand(0,1);
      $prob_vo2_13=round($prob_vo2_13,9); 
      $stage13_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_13,0.95,0.02));
      //=C23*(NORM.INV(RAND(),0.96, 0.02))
      $prob_vo2_14=$this->frand(0,1);
      $prob_vo2_14=round($prob_vo2_14,9); 
      $stage14_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_14,0.96, 0.02));
      //=C23*(NORM.INV(RAND(),0.97, 0.015))
      $prob_vo2_15=$this->frand(0,1);
      $prob_vo2_15=round($prob_vo2_15,9); 
      $stage15_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_15,0.97, 0.015));
      //=C23*(NORM.INV(RAND(),0.98, 0.015))
      $prob_vo2_16=$this->frand(0,1);
      $prob_vo2_16=round($prob_vo2_16,9); 
      $stage16_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_16,0.98, 0.015));
      //=C23*(NORM.INV(RAND(),0.99, 0.005))
      $prob_vo2_17=$this->frand(0,1);
      $prob_vo2_17=round($prob_vo2_17,9); 
      $stage17_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_17,0.99, 0.005));
      //=C23*(NORM.INV(RAND(),0.99, 0.005))
      $prob_vo2_18=$this->frand(0,1);
      $prob_vo2_18=round($prob_vo2_18,9); 
      $stage18_vo2=$VO2maxLM *($object1->NORMINV($prob_vo2_18,0.99,0.005));
      
      $_SESSION['stage1_vo2']=round($stage1_vo2,1);
      $_SESSION['stage2_vo2']=round($stage2_vo2,1);
      $_SESSION['stage3_vo2']=round($stage3_vo2,1);
      $_SESSION['stage4_vo2']=round($stage4_vo2,1);
      $_SESSION['stage5_vo2']=round($stage5_vo2,1);
      $_SESSION['stage6_vo2']=round($stage6_vo2,1);
      $_SESSION['stage7_vo2']=round($stage7_vo2,1);
      $_SESSION['stage8_vo2']=round($stage8_vo2,1);
      $_SESSION['stage9_vo2']=round($stage9_vo2,1);
      $_SESSION['stage10_vo2']=round($stage10_vo2,1);
      $_SESSION['stage11_vo2']=round($stage11_vo2,1);
      $_SESSION['stage12_vo2']=round($stage12_vo2,1);
      $_SESSION['stage13_vo2']=round($stage13_vo2,1);
      $_SESSION['stage14_vo2']=round($stage14_vo2,1);
      $_SESSION['stage15_vo2']=round($stage15_vo2,1);
      $_SESSION['stage16_vo2']=round($stage16_vo2,1);
      $_SESSION['stage17_vo2']=round($stage17_vo2,1);
      $_SESSION['stage18_vo2']=round($stage18_vo2,1);
      
      }
		
  	
	// 20 m shuttle test  
  public function twenty_shuttle_test()
  {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
      $mass = $_SESSION['MASS'];    
      if($_SESSION['VO2max']=="")
      {
      $setvo2=$this->get_VO2max();    
      }
      $VO2max=$_SESSION['VO2max'];
   //LAP = 0.01664*VO2^2 + 1.68560*VO2 - 32.36922
     // $lap=(-0.000515 * pow($VO2max,2)) + (0.401077 * $VO2max) + 17.076112;
     $lap=(0.01664 * pow($VO2max,2)) + (1.68560 * $VO2max)  - 32.36922;
	 $lap=round($lap);
      if($lap > 171)
     {
     $lap = 171;    
     }   
	 $_SESSION['lap']=$lap;
    if($VO2max >= 20)
    {
    $shuttle_level=$this->get_shuttle_level($lap);  
     $_SESSION['shuttle_level']=$shuttle_level;
     $_SESSION['sixmin_test']="";
     
    }
    else
    {
      $_SESSION['shuttle_level']="";
      //meters walked = (predicted VO2max-4.948)/0.023(RAND()*(1.1-0.9)+0.9					
      $prob_meterwalked=$this->frand(0,1);
      $prob_meterwalked=round($prob_meterwalked,9); 
      $walked_meter=($VO2max - 4.948) / 0.023 * ($prob_meterwalked * (1.1-0.9)+0.9);
      
      $_SESSION['sixmin_test']=round($walked_meter);
    }
  
  }
	
	// 20 m shuttle test  
  public function Lactate_Threshold()
  {
      $setagerange=$this->getagerange();
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $_SESSION['vp_age'];
      $subpopulation = $_SESSION['subpopulation'];
      $age_range = $_SESSION['age_range'];
      $gender = $_SESSION['user_gender'];
      $mass = $_SESSION['MASS'];    
      //VO2 = (0.4303795 * Lap) - (0.000939652*(Lap^2)) + (0.000001646*(lap^3)) + 16.65
      if($_SESSION['VO2max']=="")
      {
      $setvo2=$this->get_VO2max();    
      }
      $VO2max=$_SESSION['VO2max'];
      $VO2maxLM=$_SESSION['VO2maxLM'];
      $VO2rest=$_SESSION['VO2rest'];
      $HRmax=$_SESSION['HRmax'];
      $HRrest=$_SESSION['HRrest'];
     // WATTSmax = (VO2max – VO2rest) / (NORM.INV(RAND(),0.01197,0.001))  
      $prob_wattsmax=$this->frand(0,1);
      $prob_wattsmax=round($prob_wattsmax,9); 
      $wattsmax_calc=$object1->NORMINV($prob_wattsmax,0.01197,0.001); 
      $WATTSmax = ($VO2maxLM - $VO2rest )/ $wattsmax_calc;
      //WATTS1 = (NORM.INV(RAND(),30,1))/100 * WATTSmax
      $prob_watts1=$this->frand(0,1);
      $prob_watts1=round($prob_watts1,9); 
      $watts1_calc=$object1->NORMINV($prob_watts1,30,1)/100 * $WATTSmax;    
      $watts1=round($watts1_calc);
      
      $prob_watts2=$this->frand(0,1);
      $prob_watts2=round($prob_watts2,9); 
      $watts2_calc=$object1->NORMINV($prob_watts2,36,1)/100 * $WATTSmax;    
      $watts2=round($watts2_calc);
      
       $prob_watts3=$this->frand(0,1);
      $prob_watts3=round($prob_watts3,9); 
      $watts3_calc=$object1->NORMINV($prob_watts3,42,1)/100 * $WATTSmax;    
      $watts3=round($watts3_calc);
      
      $prob_watts4=$this->frand(0,1);
      $prob_watts4=round($prob_watts4,9); 
      $watts4_calc=$object1->NORMINV($prob_watts4,48,1)/100 * $WATTSmax;    
      $watts4=round($watts4_calc);
      
      $prob_watts5=$this->frand(0,1);
      $prob_watts5=round($prob_watts5,9); 
      $watts5_calc=$object1->NORMINV($prob_watts5,54,1)/100 * $WATTSmax;    
      $watts5=round($watts5_calc);
      
      $prob_watts6=$this->frand(0,1);
      $prob_watts6=round($prob_watts6,9); 
      $watts6_calc=$object1->NORMINV($prob_watts6,60,1)/100 * $WATTSmax;    
      $watts6=round($watts6_calc);
    
      $prob_watts7=$this->frand(0,1);
      $prob_watts7=round($prob_watts7,9); 
      $watts7_calc=$object1->NORMINV($prob_watts7,66,1)/100 * $WATTSmax;    
      $watts7=round($watts7_calc);
      
      $prob_watts8=$this->frand(0,1);
      $prob_watts8=round($prob_watts8,9); 
      $watts8_calc=$object1->NORMINV($prob_watts8,72,1)/100 * $WATTSmax;    
      $watts8=round($watts8_calc);
      
      $prob_watts9=$this->frand(0,1);
      $prob_watts9=round($prob_watts9,9); 
      $watts9_calc=$object1->NORMINV($prob_watts9,78,1)/100 * $WATTSmax;    
      $watts9=round($watts9_calc);
      
      $prob_watts10=$this->frand(0,1);
      $prob_watts10=round($prob_watts10,9); 
      $watts10_calc=$object1->NORMINV($prob_watts10,84,1)/100 * $WATTSmax;    
      $watts10=round($watts10_calc);
     
      //The percent of WATTSmax
      $watts1_per=($watts1/$watts10)*100;
      $watts2_per=($watts2/$watts10)*100;
      $watts3_per=($watts3/$watts10)*100;
      $watts4_per=($watts4/$watts10)*100;
      $watts5_per=($watts5/$watts10)*100;
      $watts6_per=($watts6/$watts10)*100;
      $watts7_per=($watts7/$watts10)*100;
      $watts8_per=($watts8/$watts10)*100;
      $watts9_per=($watts9/$watts10)*100;
      $watts10_per=($watts10/$watts10)*100;
      //Generate Heart Rate values [BPM] that correspond to the % Power Values
      //HR1 = (RAND1 / 100) * (HRmax – HRrest)		
      $HR1=($watts1_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR2=($watts2_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR3=($watts3_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR4=($watts4_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR5=($watts5_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR6=($watts6_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR7=($watts7_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR8=($watts8_per/100)*($HRmax-$HRrest)+$HRrest;
      $HR9=($watts9_per/100)*($HRmax-$HRrest)+$HRrest;
      //=R26/100*(G3-I3)+I3*(RAND()*(1-0.9)+0.9)
      $prob_HR10=$this->frand(0,1);
      $prob_HR10=round($prob_HR10,9);
      $HR10=($watts10_per/100)*($HRmax-$HRrest)+$HRrest*($prob_HR10*(1-0.9)+0.9);
      //LACTATE levels [mmol/L]
      $prob_LACTINT1=$this->frand(0,1);
      $prob_LACTINT1=round($prob_LACTINT1,9);
      $LACTINT1=$object1->NORMINV($prob_LACTINT1,0.6,0.17);
      $LACTATE1=0.020649 * pow(1,3) - 0.186538 * pow(1,2) + 0.627661 * (1) + $LACTINT1;
      
      $prob_LACTINT2=$this->frand(0,1);
      $prob_LACTINT2=round($prob_LACTINT2,9);
      $LACTINT2=$object1->NORMINV($prob_LACTINT2,0.6,0.17);
      $LACTATE2=0.020649 * pow(2,3) - 0.186538 * pow(2,2) + 0.627661 * (2) + $LACTINT2;
      
      $prob_LACTINT3=$this->frand(0,1);
      $prob_LACTINT3=round($prob_LACTINT3,9);
      $LACTINT3=$object1->NORMINV($prob_LACTINT3,0.6,0.18);
      $LACTATE3=0.020649 * pow(3,3) - 0.186538 * pow(3,2) + 0.627661 * (3) + $LACTINT3;
      
      $prob_LACTINT4=$this->frand(0,1);
      $prob_LACTINT4=round($prob_LACTINT4,9);
      $LACTINT4=$object1->NORMINV($prob_LACTINT4,0.6,0.19);
      $LACTATE4=0.020649 * pow(4,3) - 0.186538 * pow(4,2) + 0.627661 * (4) + $LACTINT4;
    
      $prob_LACTINT5=$this->frand(0,1);
      $prob_LACTINT5=round($prob_LACTINT5,9);
      $LACTINT5=$object1->NORMINV($prob_LACTINT5,0.6,0.2);
      $LACTATE5=0.020649 * pow(5,3) - 0.186538 * pow(5,2) + 0.627661 * (5) + $LACTINT5;
      
      $prob_LACTINT6=$this->frand(0,1);
      $prob_LACTINT6=round($prob_LACTINT6,9);
      $LACTINT6=$object1->NORMINV($prob_LACTINT6,0.6,0.2);
      $LACTATE6=0.020649 * pow(6,3) - 0.186538 * pow(6,2) + 0.627661 * (6) + $LACTINT6;
      
      $prob_LACTINT7=$this->frand(0,1);
      $prob_LACTINT7=round($prob_LACTINT7,9);
      $LACTINT7=$object1->NORMINV($prob_LACTINT7,0.8,0.25);
      $LACTATE7=0.020649 * pow(7,3) - 0.186538 * pow(7,2) + 0.627661 * (7) + $LACTINT7;
      
      $prob_LACTINT8=$this->frand(0,1);
      $prob_LACTINT8=round($prob_LACTINT8,9);
      $LACTINT8=$object1->NORMINV($prob_LACTINT8,0.8,0.3);
      $LACTATE8=0.020649 * pow(8,3) - 0.186538 * pow(8,2) + 0.627661 * (8) + $LACTINT8; 
      
      $prob_LACTINT9=$this->frand(0,1);
      $prob_LACTINT9=round($prob_LACTINT9,9);
      $LACTINT9=$object1->NORMINV($prob_LACTINT9,0.8,0.35);
      $LACTATE9=0.020649 * pow(9,3) - 0.186538 * pow(9,2) + 0.627661 * (9) + $LACTINT9; 
      //LACTINT10 = (NORM.INV(RAND(),0.9,0.5))
    //LACTATE10 [Stage 10] = 0.021*(10^3) - 0.187*(102) + 0.628*(10) + LACTINT10
    //[note equation differs slightly from the others]
      $prob_LACTINT10=$this->frand(0,1);
      $prob_LACTINT10=round($prob_LACTINT10,9);
      $LACTINT10=$object1->NORMINV($prob_LACTINT10,0.9,0.5);
      $LACTATE10=0.021 * pow(10,3) - 0.187 * pow(10,2) + 0.628 * (10) + $LACTINT10; 
      if($LACTATE1 < 0.6)
      {
      $LACTATE1 =0.6;   
      }   
      
      $_SESSION['watts1']=$watts1;
      $_SESSION['watts2']=$watts2;
      $_SESSION['watts3']=$watts3;
      $_SESSION['watts4']=$watts4;
      $_SESSION['watts5']=$watts5;
      $_SESSION['watts6']=$watts6;
      $_SESSION['watts7']=$watts7;
      $_SESSION['watts8']=$watts8;
      $_SESSION['watts9']=$watts9;
      $_SESSION['watts10']=$watts10;
      
      $_SESSION['HR1']=round($HR1);
      $_SESSION['HR2']=round($HR2);
      $_SESSION['HR3']=round($HR3);
      $_SESSION['HR4']=round($HR4);
      $_SESSION['HR5']=round($HR5);
      $_SESSION['HR6']=round($HR6);
      $_SESSION['HR7']=round($HR7);
      $_SESSION['HR8']=round($HR8);
      $_SESSION['HR9']=round($HR9);
      $_SESSION['HR10']=round($HR10);
      
      $_SESSION['LACTATE1']=round($LACTATE1,1);
      $_SESSION['LACTATE2']=round($LACTATE2,1);
      $_SESSION['LACTATE3']=round($LACTATE3,1);
      $_SESSION['LACTATE4']=round($LACTATE4,1);
      $_SESSION['LACTATE5']=round($LACTATE5,1);
      $_SESSION['LACTATE6']=round($LACTATE6,1);
      $_SESSION['LACTATE7']=round($LACTATE7,1);
      $_SESSION['LACTATE8']=round($LACTATE8,1);
      $_SESSION['LACTATE9']=round($LACTATE9,1);
      $_SESSION['LACTATE10']=round($LACTATE10,1);
   
      }

	// Get Shuttle Level
  public function get_shuttle_level($lap)
  {
      $qry = $this->db->select('shuttle_level')->from('shuttle_level')->where('lap', $lap)->get();
        $res = $qry->result();
        if (count($res) > 0) {
            $shuttle_level = $res[0]->shuttle_level;
            return $shuttle_level;
        } else {
            $shuttle_level = "";
            return $shuttle_level;
        }
  }
  
	
	//Get zscore Mean and SD
  function getzscore_strength($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.6 ;
            $sd = 8.3 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 49.8 ;
            $sd = 8.5 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 48.5 ;
           $sd = 8.2 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 46.25  ;
           $sd = 7.2 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 40.3 ;
           $sd = 7.1 ;
        
      }else if($age_range == "70+") {
        
           $mean =35.5 ;
           $sd = 7 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 29.1 ;
            $sd = 4.95 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.96 ;
           $sd = 5.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 29.5 ;
           $sd =  4.9;
        
      }else if($age_range == "50-59") {
        
           $mean = 27.45 ;
           $sd = 5.05;
        
      }else if($age_range == "60-69") {
        
           $mean =25.4 ;
           $sd = 4.56 ;
        
      }else if($age_range == "70+") {
        
           $mean = 22.3;
           $sd = 5.2;        
      }                  
   }
      
   $meansd_strength=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_strength;
      }

	//Get zscore Mean and SD
  function getzscore_pickpower($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 15.86 ;
            $sd = 1.59 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 14.38 ;
            $sd = 1.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 13.24 ;
           $sd = 1.05 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 11.65  ;
           $sd = 0.86 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 10.03 ;
           $sd = 0.70 ;
        
      }else if($age_range == "70+") {
        
           $mean =8.53 ;
           $sd = 0.58 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 13.89 ;
            $sd = 1.39 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 12.65 ;
           $sd = 1.10 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 11.45 ;
           $sd =  0.90;
        
      }else if($age_range == "50-59") {
        
           $mean = 10.00 ;
           $sd = 0.74;
        
      }else if($age_range == "60-69") {
        
           $mean =8.47 ;
           $sd = 0.59 ;
        
      }else if($age_range == "70+") {
        
           $mean = 7.34 ;
           $sd = 0.50 ;        
      }                  
   }
      
   $meansd_pickpower=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_pickpower;
      }

	 //Fitness Module Norms Data  
 function fitness_module_norms($gender,$age_range)
 { 
		   if($gender=='M' && $age_range=='18-29')
		   {
			  $qry="SELECT * FROM fitness_norm where age='18-29' and gender='M'";
		   }
           if($gender=='M' && $age_range=='30-39')
		   {
			   $qry="SELECT * FROM fitness_norm where age='30-39' and gender='M'";
		   }
		   if($gender=='M' && $age_range=='40-49')
			{
				$qry="SELECT * FROM fitness_norm where age='40-49' and gender='M'";
			}
          if($gender=='M' && $age_range=='50-59')
			{
			   $qry="SELECT * FROM fitness_norm where age='50-59' and gender='M'";
		    }
		  if($gender=='M' && $age_range=='60-69')
			{
			   $qry="SELECT * FROM fitness_norm where age='60-69' and gender='M'";
		    }
		  if($gender=='M' && $age_range=='70+')
			{
			   $qry="SELECT * FROM fitness_norm where age='70+' and gender='M'";
		    }
		   
		   
		   
		  
		    if($gender=='F' && $age_range=='18-29')
		   {
			  $qry="SELECT * FROM fitness_norm where age='18-29' and gender='F'";
		   }
           if($gender=='F' && $age_range=='30-39')
		   {
			   $qry="SELECT * FROM fitness_norm where age='30-39' and gender='F'";
		   }
		   if($gender=='F' && $age_range=='40-49')
			{
				$qry="SELECT * FROM fitness_norm where age='40-49' and gender='F'";
			}
          if($gender=='F' && $age_range=='50-59')
			{
			   $qry="SELECT * FROM fitness_norm where age='50-59' and gender='F'";
		    }
		  if($gender=='F' && $age_range=='60-69')
			{
			   $qry="SELECT * FROM fitness_norm where age='60-69' and gender='F'";
		    }
		  if($gender=='F' && $age_range=='70+')
			{
			   $qry="SELECT * FROM fitness_norm where age='70+' and gender='F'";
		    }
		   
		   
		  // echo $qry;
		   
		   
           $query = $this->db->query($qry); 
		 if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $MeanVJ=$result['MeanVJ'];   
       $SDVJ=$result['SDVJ'];   
       $MeanFTCT=$result['MeanFTCT'];   
       $SDFTCT=$result['SDFTCT'];   
       $MeanPPWKg=$result['MeanPPWKg'];   
       $SDPPWKg=$result['SDPPWKg'];   
       $MeanTW=$result['MeanTW'];   
       $SDTW=$result['SDTW'];   
       $endurance_VO2max=$result['endurance_VO2max'];   
       $SD_endurance_VO2max=$result['SD_endurance_VO2max'];   
       $Lactate_threshold_w=$result['Lactate_threshold_w'];   
       $SD_Lactate_threshold_w=$result['SD_Lactate_threshold_w'];   
       $Lactate_threshold_km=$result['Lactate_threshold_km'];   
       $SD_Lactate_threshold_km=$result['SD_Lactate_threshold_km'];   
       $MeanGS=$result['MeanGS'];   
       $SDGS=$result['SDGS'];   
   
   $fitness_norms =array('MeanVJ'=>$MeanVJ,
    'SDVJ'=>$SDVJ,
    'MeanFTCT'=>$MeanFTCT,
    'SDFTCT'=>$SDFTCT,
    'MeanPPWKg'=>$MeanPPWKg,
    'SDPPWKg'=>$SDPPWKg,
    'MeanTW'=>$MeanTW,
    'SDTW'=>$SDTW,        
    'endurance_VO2max'=>$endurance_VO2max,
    'SD_endurance_VO2max'=>$SD_endurance_VO2max,
    'Lactate_threshold_w'=>$Lactate_threshold_w,
    'SD_Lactate_threshold_w'=>$SD_Lactate_threshold_w,  
    'Lactate_threshold_km'=>$Lactate_threshold_km,
    'SD_Lactate_threshold_km'=>$SD_Lactate_threshold_km,
    'MeanGS' =>$MeanGS,
    'SDGS' =>$SDGS);
  
   //  print_r($fitness_norms); die;
  
   return $fitness_norms;
   
   }
 //GET AGE RANGE
  public function getagerange()
  { 
      if($_SESSION['vp_age'] >= 18 && $_SESSION['vp_age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['vp_age'] >= 30 && $_SESSION['vp_age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['vp_age'] >= 40 && $_SESSION['vp_age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['vp_age'] >= 50 && $_SESSION['vp_age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['vp_age'] >= 60 && $_SESSION['vp_age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['vp_age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 }
  }
  //Get zscore Mean and SD
  function getzscore_meansd($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.0 ;
            $sd = 8.50 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 42.8 ;
            $sd = 8.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 36.1 ;
           $sd = 7.35 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 31.0 ;
           $sd = 6.84 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 26.9 ;
           $sd = 6.36 ;
        
      }else if($age_range == "70+") {
        
           $mean = 23.6 ;
           $sd = 5.91 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 33.5 ;
            $sd = 6.20 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.1 ;
           $sd = 5.77 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 25.4 ;
           $sd = 5.36 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 22.1 ;
           $sd = 4.99 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 19.2 ;
           $sd = 4.64 ;
        
      }else if($age_range == "70+") {
        
           $mean = 16.7 ;
           $sd = 4.31 ;        
      }                  
   }
      
   $meansd_array=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_array;
      }
      
	  //Get zscore Mean and SD
  function getzscore_flight($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 2.28 ;
            $sd = 0.319 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 2.17 ;
            $sd = 0.303 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.99 ;
           $sd = 0.279 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.73 ;
           $sd = 0.243 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 1.47 ;
           $sd = 0.221 ;
        
      }else if($age_range == "70+") {
        
           $mean =1.22 ;
           $sd = 0.196 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 1.90 ;
            $sd = 0.266 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 1.81 ;
           $sd = 0.253 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.66 ;
           $sd =  0.241 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.44 ;
           $sd = 0.217 ;
        
      }else if($age_range == "60-69") {
        
           $mean =1.23 ;
           $sd = 0.190 ;
        
      }else if($age_range == "70+") {
        
           $mean = 1.02 ;
           $sd = 0.163 ;        
      }                  
   }
      
   $meansd_flight=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_flight;
      }

	  //Get zscore Mean and SD
  function getzscore_totalwork($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 0.21605 ;
            $sd = 0.04 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 0.19375 ;
            $sd = 0.036425 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 0.17648;
           $sd = 0.0326480422;
        
      }else if($age_range == "50-59") {
        
           $mean = 0.15353;
           $sd = 0.0279431946;
        
      }else if($age_range == "60-69") {
        
           $mean = 0.13210;
           $sd = 0.0240413705;
        
      }else if($age_range == "70+") {
        
           $mean =0.11103;
           $sd = 0.0202067719;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 0.19683;
            $sd = 0.034;
            
        }else if($age_range == "30-39") {
        
           $mean = 0.17742 ;
           $sd = 0.0301606154 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 0.15967;
           $sd =  0.0255478154 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 0.14109;
           $sd =0.0239847581;
        
      }else if($age_range == "60-69") {
        
           $mean =0.11992;
           $sd = 0.0203870444 ;
        
      }else if($age_range == "70+") {
        
           $mean = 0.10151;
           $sd = 0.0172566466 ;        
      }                  
   }
      
   $meansd_totalwork=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_totalwork;
      }


	  //fraction random number.
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }
   public function all_sports_female(){
    
    $this->db->select("*");      
    $this->db->from('comparison_list');
    $this->db->where("( gender like '%f%')"); 
	$this->db->order_by("sport", "asc"); 
    $query = $this->db->get(); 
    return $query->result();
  } 
  
   public function all_ages(){
    
    $this->db->select("*");	     
    $this->db->from('age');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  
  public function all_tests(){
    
    $this->db->select("*");	     
    $this->db->from('tests');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  public function all_ranges(){
    
    $this->db->select("*");	     
    $this->db->from('probability');    
    $query = $this->db->get(); 
    return $query->result();
  }
  
  
  function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
  
 public function saveVo2MaxInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
               
          $query = $this->db->query("SELECT c_id FROM vo2max_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('vo2max_test', $code); 
                }
                else
                {
                    return $this->db->insert('vo2max_test', $code); 
                }               
          
      }
      
 public function saveVerticalJumpInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
               
			   
		  //Added by Daya	
			$code_client = array(
			   'id' => $_SESSION['userid'],	
              'vj' => $this->input->post('vo2Max_value'),
			  'perform_percent' => $this->input->post('percentile')
              );
			  
			  $c_id=$_SESSION['userid'];
			  $vj=$this->input->post('vo2Max_value');
			  $p_per=$this->input->post('percentile');
			  $this->db->query("update client_person_info set id=$c_id,vj='".$vj."',perform_percent='".$p_per."' WHERE id = $c_id");
			  
			  //$this->db->where('id',  $_SESSION['userid']);
               //$this->db->update('client_person_info', $code_client); 
			  
            /* if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }	   */ 
			  print_r($this->db->last_query());
			   
          $query = $this->db->query("SELECT c_id FROM verticaljump_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('verticaljump_test', $code); 
                }
                else
                {
                    return $this->db->insert('verticaljump_test', $code); 
                }               
          
      }
	  
public function saveFlightContactInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
			  
			       
          //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'FTCT' => $this->input->post('vo2Max_value')
			  
              );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }  
			  
			  
			  
			  
			  
               
          $query = $this->db->query("SELECT c_id FROM flightcontact_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('flightcontact_test', $code); 
                }
                else
                {
                    return $this->db->insert('flightcontact_test', $code); 
                }               
          
      }	  
	  
	  
public function savePeakPowerInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
			  'weight' => $this->input->post('weight'),
			  'peak_value' => $this->input->post('peak_value'),
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
           
           //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'PPWKg' => $this->input->post('vo2Max_value'),
			  'PPW_val' => $this->input->post('peak_value')
			  );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                } 


		   
          $query = $this->db->query("SELECT c_id FROM peakpower_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('peakpower_test', $code); 
                }
                else
                {
                    return $this->db->insert('peakpower_test', $code); 
                }               
          
      }	  
  
  
public function saveShuttleInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
			  'shuttle_level' => $this->input->post('shuttle_level'),			  
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
          

			   //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'shuttle_VO2max' =>$this->input->post('vo2Max_value'),
			  'shuttle_level' => $this->input->post('shuttle_level')
			  );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid']))
				{
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
				else
				{
				$this->db->insert('client_person_info', $code_client); 
                }





		  
          $query = $this->db->query("SELECT c_id FROM shuttle_test
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('shuttle_test', $code); 
                }
                else
                {
                    return $this->db->insert('shuttle_test', $code); 
                }               
          
      }  
	  

public function saveStrengthInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'weight' => $this->input->post('weight'),
			  'leftGrip' => $this->input->post('leftGrip'),			  
              'avgGrip' => $this->input->post('avgGrip'),
              'rightGrip' => $this->input->post('rightGrip'),
              'gripRank' => $this->input->post('gripRank'),
              'benchPressKG' => $this->input->post('benchPressKG'),
			  'benchPressRM' => $this->input->post('benchPressRM'),
              'bpPred' => $this->input->post('bpPred'),
			  'bpSW' => $this->input->post('bpSW'),
			  'bpRank' => $this->input->post('bpRank'),
			  'armCurlKG' => $this->input->post('armCurlKG'),
			  'armCurlRM' => $this->input->post('armCurlRM'),
			  'acPred' => $this->input->post('acPred'),
			  'acSW' => $this->input->post('acSW'),
			  'acRank' => $this->input->post('acRank'),
			  'lPullKG' => $this->input->post('lPullKG'),
			  'lPullRM' => $this->input->post('lPullRM'),
			  'lPullPred' => $this->input->post('lPullPred'),
			  'lPullSW' => $this->input->post('lPullSW'),
			  'lPullRank' => $this->input->post('lPullRank'),
			  'lPressKG' => $this->input->post('lPressKG'),
			  'lPressRM' => $this->input->post('lPressRM'),
			  'lPressPred' => $this->input->post('lPressPred'),
			  'lPressSW' => $this->input->post('lPressSW'),
			  'lPressRank' => $this->input->post('lPressRank'),
			  'lExtKG' => $this->input->post('lExtKG'),
			  'lExtRM' => $this->input->post('lExtRM'),
			  'lExtPred' => $this->input->post('lExtPred'),
			  'lExtSW' => $this->input->post('lExtSW'),
			  'lExtRank' => $this->input->post('lExtRank'),
			  'lCurlKG' => $this->input->post('lCurlKG'),
			  'lCurlRM' => $this->input->post('lCurlRM'),
			  'lCurlPred' => $this->input->post('lCurlPred'),
			  'lCurlSW' => $this->input->post('lCurlSW'),
			  'lCurlRank' => $this->input->post('lCurlRank'),
			  'avgPerRank' => $this->input->post('avgPerRank')			  
              );
           

			  //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'L_grip' => $this->input->post('leftGrip'),
			  'R_Grip' => $this->input->post('rightGrip'),
			  'Bench_press_reps' => $this->input->post('benchPressRM'),
			  'Bench_press_kg' => $this->input->post('benchPressKG'),
			  'Arm_curl_reps' => $this->input->post('armCurlRM'),
			  'Arm_curl_kg' => $this->input->post('armCurlKG'),
			  'Lateral_pulldown_reps' => $this->input->post('lPullRM'),
			  'Lateral_pulldown_kg' => $this->input->post('lPullKG'),
			  'Leg_press_reps' => $this->input->post('lPressRM'),
			  'Leg_press_kg' => $this->input->post('lPressKG'),
			  'Leg_extension_reps' => $this->input->post('lExtRM'),
			  'Leg_extension_kg' => $this->input->post('lExtKG'),
			  'Leg_curl_reps' => $this->input->post('lCurlRM'),
			  'Leg_curl_kg' => $this->input->post('lCurlKG'),
              );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid'])){
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }else{

                    $this->db->insert('client_person_info', $code_client); 
                }






		   
          $query = $this->db->query("SELECT c_id FROM strength
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('strength', $code); 
                }
                else
                {
                    return $this->db->insert('strength', $code); 
                }               
          
      }  


public function saveSubmaximalInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'weight' => $this->input->post('weight'),
			  'workload1' => $this->input->post('workload1'),			  
              'heart_rate1' => $this->input->post('heart_rate1'),
              'rpe1' => $this->input->post('rpe1'),
              'workload2' => $this->input->post('workload2'),
              'heart_rate2' => $this->input->post('heart_rate2'),
			  'rpe2' => $this->input->post('rpe2'),
              'workload3' => $this->input->post('workload3'),
			  'heart_rate3' => $this->input->post('heart_rate3'),
			  'rpe3' => $this->input->post('rpe3'),
			  'workload4' => $this->input->post('workload4'),
			  'heart_rate4' => $this->input->post('heart_rate4'),
			  'rpe4' => $this->input->post('rpe4')			  	  
              );
          
 
              //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'Watts_HRmax' =>$this->input->post('max_wl'),
			  'Predicted_VO2_submax_test' => $this->input->post('vo_max'),
			  'Max_Power_RPE' =>$this->input->post('max_wl_rpe'),
			  'VO2max_RPE' => $this->input->post('vo_max_rpe')
			  );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid']))
				{
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
				else
				{
				$this->db->insert('client_person_info', $code_client); 
                }
		  






		  
          $query = $this->db->query("SELECT c_id FROM submaximal
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('submaximal', $code); 
                }
                else
                {
                    return $this->db->insert('submaximal', $code); 
                }               
          
      } 

public function save30sTotalWorkInfo()
 {
//var_dump($this->session->all_userdata());
           
        
          $code = array(
              //'c_id' => $this->session->userdata('userid'),
              'c_id' => $_SESSION['userid'],
              'gender' => $this->input->post('gender'),
              'age' => $this->input->post('age'),
              'sport' => $this->input->post('sport'),
			  'weight' => $this->input->post('weight'),
			  'anaerobic_value' => $this->input->post('anaerobic_value'),
              'value' => $this->input->post('vo2Max_value'),
              'percentile' => $this->input->post('percentile'),
              'scorePlot' => $this->input->post('scorePlot'),
              'sportPlot' => $this->input->post('sportPlot'),
              'sportMeanScore' => $this->input->post('sportMeanScore')
              );
			  
			  
		    //Added by Daya
		  $code_client = array(
			   'id' => $_SESSION['userid'],	
              'total_work_done_in30s_kJ' =>$this->input->post('anaerobic_value')
			   );
			  
            if($this->isExist( 'client_person_info',  $_SESSION['userid']))
				{
                    $this->db->where('id',  $_SESSION['userid']);
                    $this->db->update('client_person_info', $code_client); 
                }
				else
				{
				$this->db->insert('client_person_info', $code_client); 
                }	  
			  
               
          $query = $this->db->query("SELECT c_id FROM total_work_30s
                               WHERE c_id = '".$_SESSION['userid']."'");
          
            if($query->num_rows() != 0)
                {                 
                    $this->db->where('c_id', $_SESSION['userid']);
                    return  $this->db->update('total_work_30s', $code); 
                }
                else
                {
                    return $this->db->insert('total_work_30s', $code); 
                }               
          
      }	  	  
  
}


?>