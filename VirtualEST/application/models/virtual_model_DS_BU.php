<?php

class virtual_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          
    }
	  //fetch list of test variables  
 public function gettest_variables() 
      { 
    $query = $this->db->get('test_variables'); 
    return($query->result());
   
   } 		
		//add_VP_Data  
      public function add_VP_Data($data) 
      { 
              $this->db->insert('virtual_person_info', $data); 
                     return $this->db->insert_id();
      } 
	  
	  
  
	  
	  
	  
	  
	  

    //Get the Random Value In Pre-exercise Form
        public function fetch_preexercise_details($age,$mass,$height,$bmi,$gender,$subpopulation)
      {
         $fieldData=array();
        $fieldData[0] = new StdClass;
        $vp_age=$age;
        $MASS=$mass;
        $BMI=$bmi;
        $HEIGHT=$height;
        $sub_population=$subpopulation;
        $gender=$gender;
              //HEART QUESTION
        //calculate Heart Probability
      //FOR MALE HEART QUES
       if($gender=='M')
       {
        $HEARTPROB=1+(0.00000161*pow($vp_age,3.7892425)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';
        
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       }  
       
//FOR FEMALE HEART QUES
       if($gender=='F')
       {
        $HEARTPROB=1+(0.0000113*pow($vp_age,3.1941547)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';    
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       
      }
          
//END HEART QUESTION
  
      //FOR MALE CHEST PAIN
       if($gender=='M')
       {
        $CHESTPROB=1+(0.00105*pow($vp_age,2)+0.08262*$vp_age-3.15); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       }   
      
//FOR FEMALE CHEST PAIN QUES
       if($gender=='F')
       {
        $CHESTPROB=1+(0.00223*pow($vp_age,2)-0.06514*($vp_age)+0.56); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       } 
      // FEEL FAINT QUES
         //FOR MALE FAINT
       if($gender=='M')
       {
        $FAINTPROB=(0.00208*pow($vp_age,2)-0.09524*($vp_age)+1.7); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       }
        
       
         //FOR FEMALE FAINT
       if($gender=='F')
       {
        $FAINTPROB=(0.00262*pow($vp_age,2)-0.10952*($vp_age)+2.3); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       } 
       //ASTHMA ATTACK QUES
       
         //FOR MALE ASTHMA ATTACK
       if($gender=='M')
       {
        $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
         //FOR FEMALE ASTHMA ATTACK
       if($gender=='F')
       {
           $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
        
          //FOR MALE DIABETES
       if($gender=='M')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(5 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=3;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
         //FOR FEMALE DIABETES
       if($gender=='F')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(2 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=2;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
          //FOR MALE BONE OR MUSCLE PROBLEM
       if($gender=='M' || $gender=='F')
       {
         $BONEPROB =0.002321* pow($vp_age,2) - 0.152143*($vp_age)+ 2.8; 
        /*Random No*/
        $min=1;
        $max=100;
        $bone=rand($min,$max);
       
        // 1 Question yes no
        if($bone<=$BONEPROB)
        {
        $fieldData[0]->option_6='Y';    
        }
        else
        {
         $fieldData[0]->option_6='N';      
        }
       } 
     
    //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=45 && $gender=='M')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
      
       //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=55 && $gender=='F')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
     
      if(!isset($fieldData[0]->option_7))
      {
       $fieldData[0]->option_7='N';  
      }
       return $fieldData;      
    
       
         }//end of pre exercising Option
      
    //medication_condition
     public function medication_condition($medical_history,$gender)
     {
        
         if(count($medical_history)>0)
        {
    
    $medical_conditions=array();
    $medication=array();
    $HEART=($medical_history[0]->option_1 === 'Y')?"Y":"N";
      
//For Heart Condition
        if($HEART=='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $heartcondition=rand($min,$max);  
            if($heartcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($heartcondition >= 8)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $heartmeds=rand($min,$max);  
        if($heartmeds <= 8)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($heartmeds <= 4)
        {
         $medication[]='cardiovascular';  
        }
        if($heartmeds >= 6)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($heartmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='musculo-skeletal';   
            break;
         case 5:
        $medication[]='psychiatric';   
            break;
         case 6:
        $medication[]='renal medication';   
            break; 
        case 7:
        $medication[]='respiratory';   
            break;
         case 8:
        $medication[]='other';   
            break;
        }
          
        }
        $CHESTPAIN =($medical_history[0]->option_2 === 'Y')?"Y":"N";
        
 
       //For CHESTPAIN Condition
        if($CHESTPAIN =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $chestcondition=rand($min,$max);  
            if($chestcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($chestcondition >= 9)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $chestmeds=rand($min,$max);  
        if($chestmeds <= 9)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($chestmeds <= 6)
        {
         $medication[]='cardiovascular';  
        }
        if($chestmeds >= 3)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($chestmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='other';   
            break;
        
        }
          
        }
       
      $FAINT  =($medical_history[0]->option_3 === 'Y')?"Y":"N";
         
//For FAINT Condition
        if($FAINT =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $faintcondition=rand($min,$max);  
            if($faintcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        switch($faintcondition)
        {
        case 7:  
        $medical_conditions[]='thyroid disease'; 
        break;    
        case 8:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 9:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 10:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        }
        $min_other=1;
        $max_other=10;
        $othercondition=rand($min_other,$max_other);  
        if($othercondition>=7)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $faintmeds=rand($min,$max);  
        if($faintmeds <= 3)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($faintmeds <= 5)
        {
         $medication[]='cardiovascular';  
        }
        if($faintmeds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($faintmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        
        $ASTHMA  =($medical_history[0]->option_4 === 'Y')?"Y":"N";

       //For ASTHMA Condition
        if($ASTHMA =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $asthmacondition=rand($min,$max);  
            if($asthmacondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($asthmacondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='cerebrovascular';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($asthmacondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $asthmameds=rand($min,$max);  
        if($asthmameds >= 2)
        {
         $medication[]='respiratory';  
        }
        if($asthmameds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($asthmameds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($asthmameds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        //For DIABETES Condition
         $DIABETES   =($medical_history[0]->option_5 === 'Y')?"Y":"N";
        if($DIABETES =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $diabetescondition=rand($min,$max);  
            if($diabetescondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($diabetescondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='arthritis or osteoporosis';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($diabetescondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $diabetesmeds=rand($min,$max);  
        if($diabetesmeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($diabetesmeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($diabetesmeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($diabetesmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
        
        }
        if($diabetesmeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        }
       
         //For BONE Condition
         $BONE =($medical_history[0]->option_6 === 'Y')?"Y":"N";
        if($BONE =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $bonecondition=rand($min,$max);  
            if($bonecondition <= 8)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($bonecondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='diabetes';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($bonecondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $bonemeds=rand($min,$max);  
        if($bonemeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($bonemeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($bonemeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($bonemeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($bonemeds >= 5 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($bonemeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        
        }

        
       //For OTHERMED  Condition
         $OTHERMED =($medical_history[0]->option_7 === 'Y')?"Y":"N";
        if($OTHERMED =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $othercondition=rand($min,$max);  
            if($othercondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($othercondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='psychiatric illness';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($othercondition >= 6)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $lastmeds=rand($min,$max);  
        if($lastmeds >= 8)
        {
         $medication[]='psychiatric illness';  
        }
        if($lastmeds <= 2)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($lastmeds >= 6)
        {
         $medication[]='other';  
        }
        switch($lastmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='metabolic';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($lastmeds >= 9 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($lastmeds >= 7) 
        {
         $medication[]='psychiatric';     
        }
       }
         //##################IF ALL NO ANSWER SELECTED#######################################      
     if($HEART=='N' && $CHESTPAIN=='N' && $FAINT=='N' && $ASTHMA=='N' && $DIABETES=='N' && $BONE=='N' && $OTHERMED=='N')
     {
        
         $min=1;
        $max=100;
        $medcondition=rand($min,$max); 
       
        if($medcondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';  
          $medication[]='other';
        }
        switch($medcondition)
        {
        case 3:
        $medical_conditions[]='cardiovascular';   
        $medication[]='LIPID-lowering medication';  
            break;
         case 4:
        $medical_conditions[]='cerebrovascular';
        $medication[]='other';       
            break;
        case 5:
            $medical_conditions[]='diabetes';
            $medication[]='diabetic';   
            break;
         case 6:
             $medical_conditions[]='kidney disease';
             $medication[]='BLOOD PRESSURE-lowering medication';   
            break;
         case 7:
         $medical_conditions[]='liver or metabolic disorder';
         $medication[]='metabolic';      
            break;
         case 8:
         $medical_conditions[]='psychiatric illness';
         $medication[]='psychiatric';      
            break;
        case 9:
         $medical_conditions[]='respiratory disease';
         $medication[]='respiratory';      
            break;
         case 10:
         $medical_conditions[]='thyroid disease';
         $medication[]='metabolic';      
            break;
         }
     if($medcondition >= 98)
     {
     $medical_conditions[]='other';
         $medication[]='other';         
     }
   }
                
        $medication_selected=implode(',',array_unique($medication));
        $medical_cond_selected=implode(',',array_unique($medical_conditions));
       return(array('medication_selected'=>$medication_selected,'medical_cond_selected'=>$medical_cond_selected));
       } 
     }
 
     // VP_physical_activity
     public function VP_physical_activity($age,$mass,$height,$bmi,$gender,$subpopulation)
      {
       $fieldData=array();
       $fieldData[0] = new StdClass; 
       $subpopulation=$subpopulation;
       $gender=$gender;
       $age=$age;
       $BMI=$bmi;
       $SedTV="";
       $DriveTime="";
       $jobsitopt="";
       $total_PA="";
       if($subpopulation=='Sedentary')
       {
        $walk_freq='0';
        $walk_tot_min='0';
        $vig_freq='0';
        $vig_freq_tot_min='0';
        $other_mod_freq='0';
        $other_mod_tot_min='0';
       }
 //GENERAL MALE
       if($subpopulation=='General' && $gender=='M')
       {
        $minoccu=-1;
        $maxoccu=4;
        $walk_freq=rand($minoccu,$maxoccu)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=6;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      //FOR FEMALE 
          if($subpopulation=='General' && $gender=='F')
       {
        $minoccu=-1;
        $maxoccu=6;
        $walk_freq=rand($minoccu,$maxoccu)+1;
        $walk_duration=rand(2,5)*5;   
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='10';   
        }
        $other_mod_tot_min=$other_mod_freq * 10;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       
       if($total_PA >= 150)
       {
       $walk_freq=rand(-1,1)+1;  
       if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
               $walk_duration=rand(2,3)*5;   
           }
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(-1,1)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur='10';      
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(-1,1)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
         $mod_dur='15';   
        }
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
       
      //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='M')
       {
        $walk_freq=rand(-1,10)+1;
           if($walk_freq=='0')
           {
            $walk_duration='0';   
           }
           else
           {
                $walkmin=2;
                $walkmax=10;
               $walk_duration=rand($walkmin,$walkmax)*5;   
           }
        
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,4)+1 ;
        if($vig_freq=='0')
        {
         $vigdur='0';   
        }
        else
        {
         $vigdur=rand(2,10) * 5;       
        }
        $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA < 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='25';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
       //ACTIVE SUB WITH MALE 
      if($subpopulation=='Active' && $gender=='F')
       {
        $walk_freq=rand(-1,12)+1;
        $walk_duration=rand(2,10)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(-1,2)+1 ;
         $vigdur=rand(2,10) * 5;       
      
         $vig_freq_tot_min=$vig_freq * 10;
        
        $other_mod_freq=rand(-1,2)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,10) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * 15;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       if($total_PA < 150)
       {
       $walk_freq=rand(2,5);  
       $walk_duration=rand(4,6)*5;  
       $walk_tot_min=$walk_freq * $walk_duration;
       $vig_freq=rand(3,5) ;
         $vigdur='20';      
        $vig_freq_tot_min=$vig_freq * $vigdur;
        $other_mod_freq=rand(2,3);
        $mod_dur=rand(6,8) * 5; 
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
       }
      } 
      
        //Athlete SUB WITH MALE 
      if($subpopulation=='Athlete')
       {
        $walk_freq=rand(1,5)+1;
        $walk_duration=rand(2,60)*5;      
        $walk_tot_min=$walk_freq * $walk_duration;
        $vig_freq=rand(3,7)+1 ;
         $vigdur=rand(4,12) * 5;       
      
         $vig_freq_tot_min=$vig_freq * $vigdur;
        
        $other_mod_freq=rand(-1,3)+1;
        if($other_mod_freq=='0')
        {
         $mod_dur='0';   
        }
        else
        {
           $mod_dur=rand(2,12) * 5;   
        }
        $other_mod_tot_min=$other_mod_freq * $mod_dur;
    
       $total_PA=$walk_tot_min + (2 * $vig_freq_tot_min) + $other_mod_tot_min;
      
      } 
    
         if($age < 60)
       {
        $SedTV = (rand(-1,7)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }
      //IF AGE > 60
       if($age >= 60)
       {
        $SedTV = (rand(0-1,11)+1)/2;    
       if($BMI >= 35)
       {
        $SedTV = $SedTV + 3;   
       }
        if($BMI >= 30)
       {
        $SedTV = $SedTV + 1.5;   
       }
        if($BMI >= 28)
       {
        $SedTV = $SedTV + 1;   
       } 
        
       }  
      
  // Driving or being driven
        //IF AGE < 60
       if($age < 65)
       {
        $DriveTime = (rand(-1,5)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 0.5;   
       } 
      }  
          //IF AGE < 60
       if($age >= 65)
       {
        $DriveTime = (rand(-1,4)+1)/2;    
       if($BMI >= 35)
       {
       $DriveTime = $DriveTime + 2;   
       }
        if($BMI >= 30)
       {
        $DriveTime = $DriveTime + 1.5;   
       }
        if($BMI >= 28)
       {
        $DriveTime = $DriveTime + 1;   
       } 
      } 
       
      //Job requires sitting?
        //IF AGE < 60
       if($age < 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 10)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }  
         //IF AGE < 60
       if($age >= 65)
       {
        $JobSit = rand(1,10);    
       if($JobSit < 8)
       {
       $jobsitopt='YES';   
       }
       else 
        {
        $jobsitopt='NO'; 
        }
      }
      $fieldData[0]->option_1=$walk_freq;
      $fieldData[0]->option_2=$walk_tot_min;
      $fieldData[0]->option_3=$vig_freq;
      $fieldData[0]->option_4=$vig_freq_tot_min;
      $fieldData[0]->option_5=$other_mod_freq;
      $fieldData[0]->option_6=$other_mod_tot_min;
      $fieldData[0]->option_7=$SedTV;
      $fieldData[0]->option_8=$DriveTime;
      $fieldData[0]->option_9=$jobsitopt;
      $fieldData[0]->total_PA=$total_PA;
      
     // $resultPA=array('total_PA'=>$total_PA,'walk_tot_min'=>$walk_tot_min,'vig_freq_tot_min'=>$vig_freq_tot_min,'other_mod_tot_min'=>$other_mod_tot_min,'walk_freq'=>$walk_freq,'vig_freq'=>$vig_freq,'other_mod_freq'=>$other_mod_freq,'SedTV'=>$SedTV,'DriveTime'=>$DriveTime,'jobsitopt'=>$jobsitopt); 
     return ($fieldData);
     }
    
		//GET RISK FACTOR
     public function fetch_risk_factors_details($age,$mass,$height,$bmi,$gender,$subpopulation)
     {
       $fieldData=array();
       $fieldData[0] = new StdClass; 
       $subpopulation=$subpopulation;
       $gender=$gender;
       $age=$age;
       $BMI=$bmi;   
       $age_of_ralative="";
       $family_gen="";
       $smoke_perday="";
       $total_smoke_perday="";
       $month_six_smoke="";
       $family_his_num=rand(1,10); 
       if($family_his_num=='1')
       {
       $family_his='Y';    
       }
       else
       {
       $family_his='N';    
       }
     if($family_his=='Y')
     {
      $family_gen_num=rand(1,2);    
    
      switch($family_gen_num)
      {
        case 1:
        $family_gen='M';
           break;
       case 2:
        $family_gen='F';
           break;
      }
     $age_of_ralative=rand(1,30)+40;
     }
      
//     SMOKING PROFILE 
//    [MALES] – current smoker
     if($gender=='M' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)   = -0.00004155*(Age)^4 + 0.00803066*(Age)^3 - 0.57352160*(Age)^2 + 17.52605448*(Age) - 156.13741177
        $smoke_prob_per= -0.00004155 * pow($age,4) +  0.00803066 * pow($age,3) - 0.57352160 * pow($age,2) + 17.52605448 * $age - 156.13741177;    
        }
     }
     
    //General  
     if($gender=='M' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002770*(Age)^4 + 0.00535377*(Age)^x3 - 0.38234773*(Age)^2 + 11.68403632*(Age) - 104.09160785
        $smoke_prob_per= -0.00002770 * pow($age,4) +  0.00535377 * pow($age,3) -0.38234773 * pow($age,2) + 11.68403632 * $age - 104.09160785;    
        }
     }
     //Active   
     if($gender=='M' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%) = -0.00000692*(Age)^4 + 0.00133844*(Age)^3 - 0.09558693*(Age)^2 + 2.92100908*(Age) - 26.02290196
        $smoke_prob_per= -0.00000692 * pow($age,4) +  0.00133844 * pow($age,3) - 0.09558693 * pow($age,2) + 2.92100908 * $age - 26.02290196;    
        }
     }
     // FEMALE SMOKEING PROFILE
     if($gender=='F' && $subpopulation=='Sedentary')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //= -0.00003046*(Age)^4 + 0.00578225*(Age)^3 - 0.40274444*(Age)^2 + 11.95970651*(Age) - 102.32997609
        $smoke_prob_per= -0.00003046 * pow($age,4) +   0.00578225 * pow($age,3) - 0.40274444 * pow($age,2) +  11.95970651 * $age - 102.32997609;    
        }
     }
     
    //General  
     if($gender=='F' && $subpopulation=='General')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00002030*(Age)^4 + 0.00385484*(Age)^3 - 0.26849630*(Age)^2 + 7.97313767*(Age) - 68.21998406
        $smoke_prob_per= -0.00002030 * pow($age,4) + 0.00385484 * pow($age,3) - 0.26849630 * pow($age,2) + 7.97313767 * $age - 68.21998406;    
        }
     }
     //Active   
     if($gender=='F' && $subpopulation=='Active')
     {
        if($age > 75 || $age < 15)
        {
         $smoke_prob_per='2';
        }
        else
        {
        //Smoking probability (%)  = -0.00000508*(Age)^4 + 0.00096371*(Age)^3 - 0.06712407*(Age)^2 + 1.99328442*(Age) - 17.05499601
        $smoke_prob_per= -0.00000508 * pow($age,4) + 0.00096371 * pow($age,3) - 0.06712407 * pow($age,2) + 1.99328442 * $age - 17.05499601;    
        }
     }
     
     //Male and Female smokers: 
     $random_num=rand(1,100);
     if($random_num > $smoke_prob_per)
     {
      $smoking='N';      
     }
     else
     {
       $smoking='Y';   
     }
     
     if($smoking=='Y')
     {
     $smoke_perday=rand(1,4)*5;      
     }
     
     if($subpopulation=='Athlete')
     {
      $smoking='N';  
        }
    //Have you quit smoking in the last 6 months?
     if($smoking=='N')
     {
       if($subpopulation=='Sedentary' || $subpopulation=='General')
       {
        $randomval=rand(1,15);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
     } 
     //Active
      if($subpopulation=='Active')
       {
        $randomval=rand(1,40);
        $month_six_smoke=($randomval == 1 ? 'Y' : 'N');
      //How many did you smoke / day?
        if($month_six_smoke=='Y')
        {
          $total_smoke_perday=rand(1,4)*5;  
        }
      } 
      if($subpopulation=='Athlete')
      {
      $month_six_smoke='N';  
      }
     }
   //Have you been told you have high blood pressure?
   $RNBP= rand(1,100); 
   $SRBPprob = 0.00464 * pow($age,2) - 0.26429 * ($age) + 5.06250;  
   
   If($SRBPprob <= $RNBP)
     {
        $high_bp='Y'; 
     }
     else
     {
      $high_bp='N';    
     }
     //Have you been told you have high lipids? 
     $RNLIPID=rand(1,100); 
     $SRLIPIDprob = 0.00464 * pow($age,2) - 0.26429*($age) + 5.06250;
     If($SRLIPIDprob <= $RNLIPID)
     {
        $high_cols='Y'; 
     }
     else
     {
      $high_cols='N';    
     }
     //Have you been told you have high blood sugar?
     $RNSUGAR=rand(1,100); 
     $SRSUGARprob = 0.0019 * pow($age,2) - 0.0881 * ($age) + 2.1071;
     //If SRSUGARprob < RNSUGAR then ‘YES’ else
     if($SRSUGARprob < $RNSUGAR)
     {
      $blood_sugar='Y';   
     }
    else{
      $blood_sugar='N';   
    }
 $getbloodvalue=$this->Blood_values_norms($age,$gender);    
//MEASURED BLOOD PRESSURE
    //SBP and DBP
  // Random SBP =ROUND(NORM.INV(RAND(), mean SBP, SD SBP))
 $object1 = new PHPExcel_Calculation_Statistical();
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_sbp=$getbloodvalue['mean_sbp'];
 $sd_sbp=$getbloodvalue['sd_sbp'];
 $x=$object1->NORMINV($probability,$mean_sbp,$sd_sbp);
  $randomsbp=round($x);   
  //Generate a random DBP using the normal distribution function :
 $probability=$this->frand(0,1);
 $probability=round($probability,9); 
 $mean_dbp=$getbloodvalue['mean_dbp'];
 $sd_dbp=$getbloodvalue['sd_dbp'];
 $get_dbp=$object1->NORMINV($probability,$mean_dbp,$sd_dbp);
  $randomdbp=round($get_dbp);
//Adjustment 
  if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 0.94;
     $DBP_adjustment = 0.97;
    }
    else
    {
     $SBP_adjustment = 0.93;
     $DBP_adjustment = 0.96;    
    }
  }
    if($subpopulation=='Sedentary' || $subpopulation=='General')
  {
    if($gender=='M')
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;
    }
    else
    {
     $SBP_adjustment = 1.04;
     $DBP_adjustment = 1.05;    
    }
  }
  //Therefore, for all virtual subjects:
  //SBP = round(random SBP * SBP adjustment) 
  //DBP = round(random DBP * DBP adjustment) 
  $SBP=round($randomsbp * $SBP_adjustment);
  $DBP=round($randomdbp * $DBP_adjustment);
  //[adjustment for overweight /obese people]   
  if($BMI >= 35)
  {
   $SBP = $SBP + 9;
   $DBP = $DBP + 6;
  }
   if($BMI >= 30 && $BMI<35)
  {
   $SBP = $SBP + 5;
   $DBP = $DBP + 4;
  }
  if($BMI >= 28 && $BMI<30)
  {
   $SBP = $SBP + 2;
   $DBP = $DBP + 2;
  }
  //[check to ensure no unrealistic lower values]  
  if($gender=='M' && $SBP<90)
  {
   $SBP =90;   
  }
   //[check to ensure no unrealistic lower values]  
  if($gender=='F' && $SBP < 85)
  {
   $SBP =85;   
  }
   if($gender=='M' && $DBP < 52)
  {
   $DBP =52;   
  }
   if($gender=='F' && $DBP < 50)
  {
   $DBP =50;   
  }
 // [check to prevent to SBP and DBP from being unrealistically close by this algorithm]:
  //if SBP-DBP < 22 then SBP = DBP+22 
  if(($SBP - $DBP) < 22)
  {
   $SBP = $DBP + 22;   
  }
 
  //TOTAL BLOOD CHOLESTEROL [CHOL]
  $bloodprob=$this->frand(0,1);
  $bloodprob=round($bloodprob,9); 
  $mean_cols=$getbloodvalue['mean_cols'];
  $sd_cols=$getbloodvalue['sd_cols'];
  $cols_random=$object1->NORMINV($bloodprob,$mean_cols,$sd_cols);
  $cols_random=round($cols_random,2);   
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $cols_adjustment = 0.97;
    }
    else
    {
     $cols_adjustment = 0.96; 
    }
  $CHOL=($cols_random * $cols_adjustment);
    
    }
    else
    {
    $CHOL=$cols_random;    
    }
  //[If Male and CHOL <2.5 then CHOL = 2.5 ; If Female and CHOL <2.5 then CHOL = 2.5]
  if($CHOL < 2.5)
  {
   $CHOL = 2.5;   
  }
 //HIGH-DENSITY LIPOPROTEIN [HDL]
   $proprob=$this->frand(0,1);
  $proprob=round($proprob,9); 
  $mean_ln_hdl=$getbloodvalue['mean_ln_hdl'];
  $sd_ln_hdl=$getbloodvalue['sd_ln_hdl'];
  $pro_random=$object1->NORMINV($proprob,$mean_ln_hdl,$sd_ln_hdl);
  $pro_random=round($pro_random,2);   
  $random_hdl=exp($pro_random);
  $random_hdl=round($pro_random,2);
  
     if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $hdl_adjustment = 1.15;
    }
    else
    {
     $hdl_adjustment = 1.05; 
    }
  $HDL=($random_hdl * $hdl_adjustment);
    
    }
    else
    {
    $HDL=$random_hdl;    
    }
  if($gender=='M' && $HDL < 0.6)
  {
   $HDL = 0.6;   
  }
  if($gender=='F' && $HDL < 0.75)
  {
   $HDL = 0.75;   
  }
 
  //LOW-DENSITY LIPOPROTEIN [LDL]
  $ldlprob=$this->frand(0,1);
  $ldlprob=round($ldlprob,9); 
  $mean_ldl=$getbloodvalue['mean_ldl'];
  $sd_ldl=$getbloodvalue['sd_ldl'];
  $ldl_random=$object1->NORMINV($ldlprob,$mean_ldl,$sd_ldl);
  $ldl_random=round($ldl_random,2);   
  $random_ldl=$ldl_random;
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $ldl_adjustment = 0.94;
    }
    else
    {
     $ldl_adjustment = 0.95; 
    }
  $LDL=($random_ldl * $ldl_adjustment);
    
    }
    else
    {
    $LDL=$random_ldl;    
    }
  
 if($gender=='M' && $LDL < 1.3)
  {
   $LDL = 1.3 ;   
  }
  if($gender=='F' && $LDL < 1.2)
  {
   $LDL = 1.2;   
  }
    
  //TRIGLYCERIDES [TRIG]  
   $triprob=$this->frand(0,1);
  $triprob=round($triprob,9); 
  $mean_ln_trig=$getbloodvalue['mean_ln_trig'];
  $sd_ln_trig=$getbloodvalue['sd_ln_trig'];
  $tri_random=$object1->NORMINV($triprob,$mean_ln_trig,$sd_ln_trig);
  $tri_random=round($tri_random,2);   
  $random_tri=exp($tri_random);
  $random_tri=round($random_tri,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $trig_adjustment = 0.92;
    }
    else
    {
     $trig_adjustment = 0.93; 
    }
  $TRIG=($random_tri * $trig_adjustment);
    
    }
    else
    {
    $TRIG=$random_tri;    
    }
  
 if($gender=='M' && $TRIG < 0.45)
  {
   $TRIG = 0.45 ;   
  }
  if($gender=='F' && $TRIG < 0.4)
  {
   $TRIG = 0.4;   
  } 
    
  //FASTING BLOOD GLUCOSE [GLU] 
  //TRIGLYCERIDES [TRIG]  
   $gluprob=$this->frand(0,1);
  $gluprob=round($gluprob,9); 
  $mean_ln_glu=$getbloodvalue['mean_ln_glu'];
  $sd_ln_glu=$getbloodvalue['sd_ln_glu'];
  $glu_random=$object1->NORMINV($gluprob,$mean_ln_glu,$sd_ln_glu);
  $glu_random=round($glu_random,2);   
  $random_glu=exp($glu_random);
  $random_glu=round($random_glu,2);
   if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
    if($gender=='M')
    {
     $glu_adjustment = 1.05;
    }
    else
    {
     $glu_adjustment = 1.04; 
    }
  $GLU=($random_glu * $glu_adjustment);
    
    }
    else
    {
    $GLU=$random_glu;    
    }
  
 if($gender=='M' && $GLU < 3.9)
  {
   $GLU = 3.9 ;   
  }
  if($gender=='F' && $GLU < 3.7)
  {
   $GLU = 3.7;   
  }
  //other BLOOD PARAMETER PROFILE [Hb, Hct,] [not inserted into any fields at this stage in the EST program]
  if($gender=='M')
  {
  $Hbgen =  -0.00087 * pow($age,2) + 0.04542 * ($age) + 14.32083;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.36);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
  if($gender=='F')
  {
  //Hb = -0.00000132*(Age)^4 + 0.0002767*(Age)^3 - 0.02091004*(Age)^2 + 0.66274621*(Age) + 5.7875
      $Hbgen =  -0.00000132 * pow($age,4) + 0.0002767 * pow($age,3)- 0.02091004 * pow($age,2)+ 0.66274621 * ($age) +  5.7875;
  $mean_Hb = $Hbgen;
  $hbprob=$this->frand(0,1);
  $hbprob=round($hbprob,9); 
  $hb_random=$object1->NORMINV($hbprob,$mean_Hb,1.24);
  $Hb=round($hb_random,2);   
 if($subpopulation=='Active' || $subpopulation=='Athlete')
  {
  $Hb=$Hb*0.96; 
  }
  }
 
  //Haematocrit [Hct] [%]
  $Hct_gen = 2.63 * $Hb + 5.83;
  $hctmprob=$this->frand(0,1);
  $hctmprob=round($hctmprob,9); 
  $hct_random=$object1->NORMINV($hctmprob,$Hct_gen,1.22);
  $Random_Male_Hct= round($hct_random,2);
  $hctfprob=$this->frand(0,1);
  $hctfprob=round($hctfprob,9); 
  $hctf_random=$object1->NORMINV($hctfprob,$Hct_gen,1.16);
  $Random_Female_Hct= round($hctf_random,2); 
   $fieldData[0]->option_1=$family_his;
  $fieldData[0]->option_gender=$family_gen;
  $fieldData[0]->option_age=$age_of_ralative;
  $fieldData[0]->option_2=$smoking;
  $fieldData[0]->option_smoke=$smoke_perday;
  $fieldData[0]->option_3=$month_six_smoke;
  $fieldData[0]->option_smoke_6=$total_smoke_perday;
  $fieldData[0]->option_4 =$high_bp;
  $fieldData[0]->option_5 =$high_cols; 
  $fieldData[0]->option_6 = $blood_sugar;
  $fieldData[0]->option_7 = round($SBP);
  $fieldData[0]->option_8 = round($DBP);
  $fieldData[0]->option_9 = round($HDL,2);
  $fieldData[0]->option_10 = round($LDL,2);
  $fieldData[0]->option_11 = round($CHOL,2);
  $fieldData[0]->option_12 = round($GLU,2);
  $fieldData[0]->option_13 = round($TRIG,2);
  
 return ($fieldData);
  }
     
	//PUT MASS and BMI 
       function Blood_values_norms($vp_age,$v)
       {
            if(($vp_age >=18 && $vp_age<=24) && $v=='M')
           {
      //MASS AND BMI VALUE
        $mean_sbp='118';    
        $sd_sbp='12.2';
        $mean_dbp='71';
        $sd_dbp='9.3';
        $mean_cols='4.62';
        $sd_cols='0.87'; 
        $mean_ln_hdl='0.1172';
        $sd_ln_hdl='0.219';
        $mean_ln_trig='0.12295';
        $sd_ln_trig='0.3869';
        $mean_ldl='2.9';
        $sd_ldl='0.8';
        $mean_ln_glu='1.569';
        $sd_ln_glu='0.14';
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=34) && $v=='M')
       {
        //MASS AND BMI VALUE
       $mean_sbp='120';    
        $sd_sbp='11.5';
        $mean_dbp='74';
        $sd_dbp='8.4';
        $mean_cols='5.32';
        $sd_cols='1.15'; 
        $mean_ln_hdl='0.1151';
        $sd_ln_hdl='0.2258';
        $mean_ln_trig='0.40576';
        $sd_ln_trig='0.593';
        $mean_ldl='3.4';
        $sd_ldl='0.8';
        $mean_ln_glu='1.609';
        $sd_ln_glu='0.15';
       }
       //FOR MALE 35-54
       if(($vp_age >=35 && $vp_age<=44) && $v=='M')
       {
      
//MASS AND BMI VALUE
        $mean_sbp='122';    
        $sd_sbp='12.1';
        $mean_dbp='77';
        $sd_dbp='9.8';
        $mean_cols='5.5';
        $sd_cols='1.08'; 
        $mean_ln_hdl='0.1222';
        $sd_ln_hdl='0.2194';
        $mean_ln_trig='0.48708';
        $sd_ln_trig='0.5128';
        $mean_ldl='3.5';
        $sd_ldl='0.9';
        $mean_ln_glu='1.649';
        $sd_ln_glu='0.16';
       }

//FOR MALE 35-39
       if(($vp_age >=45 && $vp_age<=54) && $v=='M')
       {
        //MASS AND BMI VALUE
        $mean_sbp='126';    
        $sd_sbp='11.8';
        $mean_dbp='80';
        $sd_dbp='9.6';
        $mean_cols='5.35';
        $sd_cols='1.09'; 
        $mean_ln_hdl='0.1092';
        $sd_ln_hdl='0.2661';
        $mean_ln_trig='0.49794';
        $sd_ln_trig='0.5421';
        $mean_ldl='3.3';
        $sd_ldl='0.9';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.17';
       }
      
       //FOR MALE 55-64
       if(($vp_age >=55 && $vp_age<=64) && $v=='M')
       {
        $mean_sbp='133';    
        $sd_sbp='14.1';
        $mean_dbp='81';
        $sd_dbp='8.8';
        $mean_cols='5.33';
        $sd_cols='0.96'; 
        $mean_ln_hdl='0.1077';
        $sd_ln_hdl='0.2319';
        $mean_ln_trig='0.39828';
        $sd_ln_trig='0.42942';
        $mean_ldl='3.1';
        $sd_ldl='0.9';
        $mean_ln_glu='1.740';
        $sd_ln_glu='0.165';
       }
 
//FOR MALE 65-74
      if(($vp_age >=65 && $vp_age<=74) && $v=='M')
       {
    
        $mean_sbp='136';    
        $sd_sbp='14.6';
        $mean_dbp='78';
        $sd_dbp='9.1';
        $mean_cols='5.14';
        $sd_cols='0.91'; 
        $mean_ln_hdl='0.1058';
        $sd_ln_hdl='0.2111';
        $mean_ln_trig='0.31785';
        $sd_ln_trig='0.42675';
        $mean_ldl='2.8';
        $sd_ldl='0.8';
        $mean_ln_glu='1.758';
        $sd_ln_glu='0.19';
       }
 
//FOR MALE 80+
       if(($vp_age >=75) && $v=='M')
       {
      $mean_sbp='138';    
        $sd_sbp='15.3';
        $mean_dbp='75';
        $sd_dbp='8.2';
        $mean_cols='5';
        $sd_cols='1'; 
        $mean_ln_hdl='0.1018';
        $sd_ln_hdl='0.2011';
        $mean_ln_trig='0.3085';
        $sd_ln_trig='0.40675';
        $mean_ldl='2.7';
        $sd_ldl='0.9';
        $mean_ln_glu='1.723';
        $sd_ln_glu='0.17';
       }
      
       
//FOR FEMALE 
        if(($vp_age >=18 && $vp_age<=24) && $v=='F')
           {
        //MASS AND BMI VALUE
        $mean_sbp='109';    
        $sd_sbp='12.4';
        $mean_dbp='72';
        $sd_dbp='8.9';
        $mean_cols='4.6';
        $sd_cols='0.92'; 
        $mean_ln_hdl='0.2781';
        $sd_ln_hdl='0.1985';
        $mean_ln_trig='-0.00734';
        $sd_ln_trig='0.34472';
        $mean_ldl='2.7';
        $sd_ldl='0.7';
        $mean_ln_glu='1.526';
        $sd_ln_glu='0.145';
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=34) && $v=='F')
       {
    //MASS AND BMI VALUE
       $mean_sbp='109';    
        $sd_sbp='12.9';
        $mean_dbp='74';
        $sd_dbp='9.4';
        $mean_cols='4.75';
        $sd_cols='0.95'; 
        $mean_ln_hdl='0.3187';
        $sd_ln_hdl='0.216';
        $mean_ln_trig='0.12792';
        $sd_ln_trig='0.35516';
        $mean_ldl='3';
        $sd_ldl='0.8';
        $mean_ln_glu='1.569';
        $sd_ln_glu='0.142';
       }
       //FOR MALE 35-54
       if(($vp_age >=35 && $vp_age<=44) && $v=='F')
       {
   
//MASS AND BMI VALUE
        $mean_sbp='114';    
        $sd_sbp='13.5';
        $mean_dbp='76';
        $sd_dbp='8.9';
        $mean_cols='5';
        $sd_cols='0.94'; 
        $mean_ln_hdl='0.2893';
        $sd_ln_hdl='0.2281';
        $mean_ln_trig='0.1318';
        $sd_ln_trig='0.51077';
        $mean_ldl='3.2';
        $sd_ldl='0.7';
        $mean_ln_glu='1.609';
        $sd_ln_glu='0.16';
       }

//FOR F 45-54
       if(($vp_age >=45 && $vp_age<=54) && $v=='F')
       {
        
//MASS AND BMI VALUE
        $mean_sbp='121';    
        $sd_sbp='14.7';
        $mean_dbp='79';
        $sd_dbp='9.2';
        $mean_cols='5.25';
        $sd_cols='1.02'; 
        $mean_ln_hdl='0.311';
        $sd_ln_hdl='0.2467';
        $mean_ln_trig='0.195';
        $sd_ln_trig='0.49851';
        $mean_ldl='3.5';
        $sd_ldl='0.9';
        $mean_ln_glu='1.629';
        $sd_ln_glu='0.157';
       }
      
       //FOR FEMALE 55-64
       if(($vp_age >=55 && $vp_age<=64) && $v=='F')
       {
        $mean_sbp='128';    
        $sd_sbp='14.3';
        $mean_dbp='80';
        $sd_dbp='9';
        $mean_cols='5.68';
        $sd_cols='1.11'; 
        $mean_ln_hdl='0.394';
        $sd_ln_hdl='0.2411';
        $mean_ln_trig='0.35695';
        $sd_ln_trig='0.47582';
        $mean_ldl='3.3';
        $sd_ldl='0.9';
        $mean_ln_glu='1.668';
        $sd_ln_glu='0.162';
       }
 
//FOR MALE 65-74
      if(($vp_age >=65 && $vp_age<=74) && $v=='F')
       {  
        $mean_sbp='136';    
        $sd_sbp='13.2';
        $mean_dbp='78';
        $sd_dbp='8.9';
        $mean_cols='5.57';
        $sd_cols='0.98'; 
        $mean_ln_hdl='0.2952';
        $sd_ln_hdl='0.245';
        $mean_ln_trig='0.38101';
        $sd_ln_trig='0.44512';
        $mean_ldl='2.9';
        $sd_ldl='0.9';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.17';
       }
 
//FOR MALE 80+
       if(($vp_age >=75) && $v=='F')
       {
        $mean_sbp='141';    
        $sd_sbp='12.6';
        $mean_dbp='76';
        $sd_dbp='9.5';
        $mean_cols='5.15';
        $sd_cols='1'; 
        $mean_ln_hdl='0.2752';
        $sd_ln_hdl='0.225';
        $mean_ln_trig='0.37101';
        $sd_ln_trig='0.4312';
        $mean_ldl='2.8';
        $sd_ldl='0.8';
        $mean_ln_glu='1.686';
        $sd_ln_glu='0.155';
       }
      
     $data=array('mean_sbp'=>$mean_sbp
             ,'sd_sbp'=>$sd_sbp
             ,'mean_dbp'=>$mean_dbp
             ,'sd_dbp'=>$sd_dbp
             ,'mean_cols'=>$mean_cols
             ,'sd_cols'=>$sd_cols
             ,'mean_ln_hdl'=>$mean_ln_hdl
             ,'sd_ln_hdl'=>$sd_ln_hdl
              ,'mean_ln_trig'=>$mean_ln_trig
              ,'sd_ln_trig'=>$sd_ln_trig
              ,'mean_ldl'=>$mean_ldl
              ,'sd_ldl'=>$sd_ldl
              ,'mean_ln_glu'=>$mean_ln_glu
              ,'sd_ln_glu'=>$sd_ln_glu
             );
      return $data;
      
       }

	//Flight : Contact time ratio
  public function get_Flight_ratio($age,$subpopulation,$gender){
      $setagerange=$this->getagerangeforvp($age);
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age = $age;
      $subpopulation = $subpopulation;
      $age_range = $setagerange;
      $gender =$gender;
    $fitness_array=$this->fintness_module_norms($gender,$age_range);
    $MeanFTCT=$fitness_array['MeanFTCT'];
    $SDFTCT=$fitness_array['SDFTCT'];
   
  if($subpopulation=='Sedentary')
  {
   $MeanFTCT =$MeanFTCT - (1.5 * $SDFTCT);   
    $SDFTCT= 0.65 * $SDFTCT;
  }
  
  //For General
  if($subpopulation=='General')
  {
    $MeanFTCT = $MeanFTCT - (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanFTCT = $MeanFTCT + (0.5 * $SDFTCT);   
    $SDFTCT= 0.75 * $SDFTCT;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
   $MeanFTCT = $MeanFTCT + (1.5 * $SDFTCT);   
    $SDFTCT= 0.5 *$SDFTCT;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanFTCT,$SDFTCT);
      $randomprob=round($x,9);   
      $FTCT =$randomprob;
      
    //If FTCT < -0.0138*(Age) + 1.62 then FTCT = -0.0138*(Age) + 1.62
      if($gender=='M' && ($FTCT < (-0.0138*($age) + 1.62)))
      {
      $FTCT = -0.0138*($age) + 1.62 ;  
      }
      //If FTCT > -0.0403*(Age) + 5.1 then FTCT = -0.0403*(Age) + 5.1
      if($gender=='M' && ($FTCT > (-0.0403*($age) + 5.1)))
      {
      $FTCT = -0.0403*($age) + 5.1 ;  
      }
//      FOR FEMALE
//       If FTCT < -0.0094*(Age) + 1.16 then FTCT = -0.0094*(Age) + 1.16
      if($gender=='F' && ($FTCT < (-0.0094*($age) +1.16)))
      {
      $FTCT = -0.0094*($age) + 1.16 ;  
      }
    //  If FTCT > -0.0295*(Age) + 3.8107 then FTCT = -0.0295*(Age) + 3.8
    if($gender=='F' && ($FTCT > (-0.0295*($age) + 3.8107)))
      {
      $FTCT = -0.0295*($age) + 3.8 ;  
      }
      
    $FTCT=round($FTCT,2);
    //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_flight($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($FTCT - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    
    $flight_array=array('FTCT'=>$FTCT,'flight_percent'=>$perform_percent,'flight_z_score'=>$z_score);
   
    return ($flight_array);
  }
    
     //Fitness Module Norms Data  
 function fintness_module_norms($gender,$age_range)
 { 
     if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
    $MeanFTCT='2.48';
    $SDFTCT='0.471';
    $MeanPPWKg='14.86';
    $SDPPWKg='2.59';
    $MeanTW='0.21605';
    $SDTW='0.04';        
    $endurance_VO2max='44.7';
    $SD_endurance_VO2max='8.9';
    $Lactate_threshold_w='148';
    $SD_Lactate_threshold_w='28';  
    $Lactate_threshold_km='10.9';
    $SD_Lactate_threshold_km='1.8';
    $MeanGS ='46.6';
    $SDGS ='8.3';        
   }
   if($gender=='M' && $age_range=='30-39')
   {
    
    $MeanVJ='45.6';
    $SDVJ='7.91';
    $MeanFTCT='2.36';
    $SDFTCT='0.424';
    $MeanPPWKg='13.38';
    $SDPPWKg='2.25';
    $MeanTW='0.19375';
    $SDTW='0.036425';        
    $endurance_VO2max='39.4';
    $SD_endurance_VO2max='8.55';
    $Lactate_threshold_w='133';
    $SD_Lactate_threshold_w='25';  
    $Lactate_threshold_km='9.7';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='49.8';
    $SDGS ='8.5';        
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
    $MeanFTCT='2.17';
    $SDFTCT='0.325';
    $MeanPPWKg='12.24';
    $SDPPWKg='2.05';
    $MeanTW='0.17648';
    $SDTW='0.032648042';        
    $endurance_VO2max='37.3';
    $SD_endurance_VO2max='8.54';
    $Lactate_threshold_w='117';
    $SD_Lactate_threshold_w='22';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='48.5';
    $SDGS ='8.2';        
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
    $MeanFTCT='1.89';
    $SDFTCT='0.283';
    $MeanPPWKg='11.15';
    $SDPPWKg='1.86';
    $MeanTW='0.15353';
    $SDTW='0.027943195';        
    $endurance_VO2max='32.95';
    $SD_endurance_VO2max='8.57';
    $Lactate_threshold_w='100';
    $SD_Lactate_threshold_w='19';  
    $Lactate_threshold_km='8.3';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='46.25';
    $SDGS ='7.2';        
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
    $MeanFTCT='1.60';
    $SDFTCT='0.240';
    $MeanPPWKg='10.03';
    $SDPPWKg='1.70';
    $MeanTW='0.13210';
    $SDTW='0.02404137';        
    $endurance_VO2max='30.6';
    $SD_endurance_VO2max='8.33';
    $Lactate_threshold_w='82';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='40.3';
    $SDGS ='7.1';        
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
    $MeanFTCT='1.33';
    $SDFTCT='0.213';
    $MeanPPWKg='8.13';
    $SDPPWKg='1.58';
    $MeanTW='0.11103';
    $SDTW='0.020206772';        
    $endurance_VO2max='29.5';
    $SD_endurance_VO2max='8.31';
    $Lactate_threshold_w='65';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='35.5';
    $SDGS ='7';        
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
    $MeanFTCT='1.90';
    $SDFTCT='0.361';
    $MeanPPWKg='10.69';
    $SDPPWKg='2.09';
    $MeanTW='0.19683';
    $SDTW='0.034';        
    $endurance_VO2max='36';
    $SD_endurance_VO2max='10.5';
    $Lactate_threshold_w='96';
    $SD_Lactate_threshold_w='18';  
    $Lactate_threshold_km='9.2';
    $SD_Lactate_threshold_km='1.6';
    $MeanGS ='29.1';
    $SDGS ='4.95';        
   }
   if($gender=='F' && $age_range=='30-39')
   {
    
    $MeanVJ='33.1';
    $SDVJ='5.77';
    $MeanFTCT='1.81';
    $SDFTCT='0.325';
    $MeanPPWKg='9.85';
    $SDPPWKg='1.80';
    $MeanTW='0.17742';
    $SDTW='0.030160615';        
    $endurance_VO2max='31.6';
    $SD_endurance_VO2max='9.92';
    $Lactate_threshold_w='85';
    $SD_Lactate_threshold_w='16';  
    $Lactate_threshold_km='8.6';
    $SD_Lactate_threshold_km='1.5';
    $MeanGS ='29.96';
    $SDGS ='5.25';        
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
    $MeanFTCT='1.66';
    $SDFTCT='0.266';
    $MeanPPWKg='9.05';
    $SDPPWKg='1.60';
    $MeanTW='0.15967';
    $SDTW='0.025547815';        
    $endurance_VO2max='30.5';
    $SD_endurance_VO2max='10.1';
    $Lactate_threshold_w='73';
    $SD_Lactate_threshold_w='14';  
    $Lactate_threshold_km='8.2';
    $SD_Lactate_threshold_km='1.4';
    $MeanGS ='29.5';
    $SDGS ='4.9';        
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
    $MeanFTCT='1.44';
    $SDFTCT='0.217';
    $MeanPPWKg='8.00';
    $SDPPWKg='1.54';
    $MeanTW='0.14109';
    $SDTW='0.023984758';        
    $endurance_VO2max='29.6';
    $SD_endurance_VO2max='9.88';
    $Lactate_threshold_w='61';
    $SD_Lactate_threshold_w='12';  
    $Lactate_threshold_km='7.9';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='27.45';
    $SDGS ='5.05';        
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
    $MeanFTCT='1.23';
    $SDFTCT='0.190';
    $MeanPPWKg='6.27';
    $SDPPWKg='1.59';
    $MeanTW='0.11992';
    $SDTW='0.020387044';        
    $endurance_VO2max='25.7';
    $SD_endurance_VO2max='9.79';
    $Lactate_threshold_w='49';
    $SD_Lactate_threshold_w='9';  
    $Lactate_threshold_km='7.7';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='25.4';
    $SDGS ='4.56';        
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
    $MeanFTCT='1.02';
    $SDFTCT='0.163';
    $MeanPPWKg='5.14';
    $SDPPWKg='1.50';
    $MeanTW='0.10151';
    $SDTW='0.017256647';        
    $endurance_VO2max='23.1';
    $SD_endurance_VO2max='8.7';
    $Lactate_threshold_w='38';
    $SD_Lactate_threshold_w='7';  
    $Lactate_threshold_km='7.4';
    $SD_Lactate_threshold_km='1.3';
    $MeanGS ='22.3';
    $SDGS ='5.2';        
   }
   
   $fitness_norms =array('MeanVJ'=>$MeanVJ,
    'SDVJ'=>$SDVJ,
    'MeanFTCT'=>$MeanFTCT,
    'SDFTCT'=>$SDFTCT,
    'MeanPPWKg'=>$MeanPPWKg,
    'SDPPWKg'=>$SDPPWKg,
    'MeanTW'=>$MeanTW,
    'SDTW'=>$SDTW,        
    'endurance_VO2max'=>$endurance_VO2max,
    'SD_endurance_VO2max'=>$SD_endurance_VO2max,
    'Lactate_threshold_w'=>$Lactate_threshold_w,
    'SD_Lactate_threshold_w'=>$SD_Lactate_threshold_w,  
    'Lactate_threshold_km'=>$Lactate_threshold_km,
    'SD_Lactate_threshold_km'=>$SD_Lactate_threshold_km,
    'MeanGS' =>$MeanGS,
    'SDGS' =>$SDGS);
  
   return $fitness_norms;
   
   }
         
//Get zscore Mean and SD
  function getzscore_flight($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 2.28 ;
            $sd = 0.319 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 2.17 ;
            $sd = 0.303 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.99 ;
           $sd = 0.279 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.73 ;
           $sd = 0.243 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 1.47 ;
           $sd = 0.221 ;
        
      }else if($age_range == "70+") {
        
           $mean =1.22 ;
           $sd = 0.196 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 1.90 ;
            $sd = 0.266 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 1.81 ;
           $sd = 0.253 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 1.66 ;
           $sd =  0.241 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 1.44 ;
           $sd = 0.217 ;
        
      }else if($age_range == "60-69") {
        
           $mean =1.23 ;
           $sd = 0.190 ;
        
      }else if($age_range == "70+") {
        
           $mean = 1.02 ;
           $sd = 0.163 ;        
      }                  
   }
      
   $meansd_flight=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_flight;
      }



//GET AGE RANGE
  public function getagerangeforvp($age)
  { 
      if($age >= 18 && $age <=29)
			 {
				 $age_range = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $age_range = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				$age_range = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				 $age_range = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				$age_range = '60-69' ;
			 }
			 elseif($age >=70)
			 {
				 $age_range = '70+' ;
			 }
  
                         return $age_range;
                         }
    
     

 //Get zscore Mean and SD
  function getzscore_meansd($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.0 ;
            $sd = 8.50 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 42.8 ;
            $sd = 8.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 36.1 ;
           $sd = 7.35 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 31.0 ;
           $sd = 6.84 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 26.9 ;
           $sd = 6.36 ;
        
      }else if($age_range == "70+") {
        
           $mean = 23.6 ;
           $sd = 5.91 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 33.5 ;
            $sd = 6.20 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.1 ;
           $sd = 5.77 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 25.4 ;
           $sd = 5.36 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 22.1 ;
           $sd = 4.99 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 19.2 ;
           $sd = 4.64 ;
        
      }else if($age_range == "70+") {
        
           $mean = 16.7 ;
           $sd = 4.31 ;        
      }                  
   }
      
   $meansd_array=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_array;
      }
   
//GET VERTICAL JUMP
  public function get_Vertical_jump($age,$subpopulation,$gender){
      $setagerange=$this->getagerangeforvp($age);
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age =$age;
      $subpopulation = $subpopulation;
        $age_range = $setagerange;
        $gender = $gender;
    if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
   }
   if($gender=='M' && $age_range=='30-39')
   {
    $MeanVJ='45.6';
    $SDVJ='7.91';
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
   }
   if($gender=='F' && $age_range=='30-39')
   {
    $MeanVJ='33.1';
    $SDVJ='5.77';
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
   } 
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
   //For Subpopulation
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
  //For General
  if($subpopulation=='General')
  {
      $MeanVJ = $MeanVJ - (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanVJ = $MeanVJ + (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
    $MeanVJ = $MeanVJ + (1.5 * $SDVJ);   
    $SDVJ= 0.5 * $SDVJ;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanVJ,$SDVJ);
      $randomprob=round($x,9);   
      $vj=$randomprob;
      //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_meansd($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($vj - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    $vj=round($vj,1);
    $vj = floor($vj*2)/2;
   
    $arrayvj=array('vj'=>$vj,'perform_percent'=>$perform_percent,'z_score'=>$z_score);

  return ($arrayvj);
  }


//GET AGE   
     function getAge( $dob,$tdate)
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
     
    
    //GET RANDOM VAULES 
    function generateRandomValues($gender="",$population="")
    {
       
       //FOR MALE FEMALE 
       if($gender=="")
       {   
       $genderarray = array( '1'  => 'Male','2'  => 'Female');  
       $k = array_rand($genderarray);
       $v = $genderarray[$k]; 
       $selectedgender=array('gendervalue'=>$k,'gendertext'=>$v);
        //FOR MALE FEMALE
       }
    else{
        if($gender=='M')
        {
        $k='1';    
       $v="Male";
        }
        if($gender=='F')
        {
        $k='2';    
        $v="Female";
        }
        }
       
       if($k=='1')
       {
       $columnname="boys_fname";    
       }
      if($k=='2')
       {
        $columnname="girls_fname";   
       }
       
       //GENERATE RANDOM NAMES 
       $randomnamesQry="SELECT $columnname as firstname FROM random_names where $columnname!='' ORDER BY RAND() LIMIT 1";
       $query = $this->db->query($randomnamesQry);
       if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $firstname=$result['firstname'];
       $randomlastQry="SELECT last_name FROM random_names where last_name!='' ORDER BY RAND() LIMIT 1";
       $queryrandom = $this->db->query($randomlastQry);
       if($queryrandom->num_rows() > 0) {
         $resultlastname=$queryrandom->row_array();
        }
       $lastname=$resultlastname['last_name'];
       $selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
     
       //END GENERATE RANDOM NAMES 
       
       //RANDOM DATE OF BIRTH GENERATION
        
       $dates = array(
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
       
                            $month = array(
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
       
       
       $years = array('1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927');		
       /*Day Selection*/
       $daykey = array_rand($dates);
       $valuedate = $dates[$daykey]; 
       $selecteddates=array('datekey'=>$daykey,'datevalue'=>$valuedate);
        /*Month Selection*/
       $monthkey = array_rand($month);
       $valuemonth = $month[$monthkey]; 
       $selectedmonth=array('monthkey'=>$monthkey,'monthvalue'=>$valuemonth);
        /*Month Selection*/
        
       /*Year Selection*/
       $yearskey = array_rand($years);
       $valueyears = $years[$yearskey]; 
       $selectedyears=array('yearskey'=>$yearskey,'yearsvalue'=>$valueyears);
        /*Year Selection*/
       
       /*COUNTRY PROFILE*/
        $min=1;
        $max=100;
        $randomcountryval=rand($min,$max);
        if($randomcountryval<90)
        {
         $selectedyears=array('countrykey'=>'AU','countryvalue'=>'Australia');   
        }
       else
       {
        $randomctryQry="SELECT country_code,country_name FROM country ORDER BY RAND() LIMIT 1";
       $queryrandomctry = $this->db->query($randomctryQry);
       if($queryrandomctry->num_rows() > 0) {
         $resultctry=$queryrandomctry->row_array();
        }    
       $selectedyears=array('countrykey'=>$resultctry['country_code'],'countryvalue'=>$resultctry['country_name']);    
       } 
      /*END COUNTRY PROFILE*/
      /*OCCUPATION PROFILE*/
     
        $minoccu=1;
        $maxoccu=30;
        $randomoccuval=rand($minoccu,$maxoccu);
       
       $occupationQry="SELECT `occupation` FROM occupation where id=$randomoccuval";
       $queryrandomoccu = $this->db->query($occupationQry);
       if($queryrandomoccu->num_rows() > 0) {
         $resultoccupation=$queryrandomoccu->row_array();
        }    
       $selectedoccupation=array('occupationkey'=>$resultoccupation['occupation'],'occupationval'=>$resultoccupation['occupation']);
       $occupation=$resultoccupation['occupation']; 
       /*END OCCUPATION PROFILE*/ 
       $dateofBirth=$valueyears.'-'.$valuemonth.'-'.$valuedate;
       $datebrth= date("Y-m-d",strtotime($dateofBirth));
       $dob = strtotime($datebrth);
       $tdate = strtotime(date('Y-m-d'));
       $vp_age=$this->getAge($dob, $tdate);
       $agerange=$this->getAgeRange($vp_age);
       if($vp_age>70)
       {
        $occupation='retired';   
       }
	  //SUB-POPULATION PROFILE CONDITION
       $subpopulation="";
      
       if($population=="")
       {
        //FOR MALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 6)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 6 && $RN< 20)
            {
            $subpopulation="General";    
            }
             else if($RN >=20  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
     
          }
      
      //FOR FEMALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 8)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 8 && $RN< 24)
            {
            $subpopulation="General";    
            }
             else if($RN >=24  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
        
            }
       
        //FOR MALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 9)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 9 && $RN< 30)
            {
            $subpopulation="General";    
            }
             else if($RN >=30  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
            }
      
      //FOR FEMALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
       //FOR Male 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //FOR Female 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 14)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 14 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //ANother Condition Based on Subpopulation and Age..
       if($subpopulation=='Athlete' && $vp_age>40)
       {
        $subpopulation="Active";    
       }
       //FOR MALE 45-54 yr
       if(($vp_age >=45 && $vp_age<=54) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 15)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 15 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 45-54 
       if(($vp_age >=45 && $vp_age<=54) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 18)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 18 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR MALE 55-64 yr
       if(($vp_age >=55 && $vp_age<=64) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 16)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 16 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 55-64 
       if(($vp_age >=55 && $vp_age<=64) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       
        //FOR MALE 65-74  yr
       if(($vp_age >=65 && $vp_age<=74) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 65-74 
       if(($vp_age >=65 && $vp_age<=74) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 24)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 24 && $RN< 52)
            {
            $subpopulation="General";    
            }
             else if($RN >=52)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Male 75+ 
       if($vp_age>= 75 && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 31)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 31 && $RN< 62)
            {
            $subpopulation="General";    
            }
             else if($RN >=62)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Female 75+ 
       if($vp_age>= 75 && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 36)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 36 && $RN< 68)
            {
            $subpopulation="General";    
            }
             else if($RN >=68)
            {
            $subpopulation="Active";    
            }
       }
    }//If Sub-population is not taken firstly then it generates
     else
     {
     $subpopulation=$population;    
     }
    
     $randomgenbmi=$this->mass_and_bmi($vp_age,$v); 
      
     $object1 = new PHPExcel_Calculation_Statistical();
      $mean_log_Mass=$randomgenbmi['mean_log_Mass'];
      $sd_log_Mass=$randomgenbmi['sd_log_Mass'];
       $m_BMI=$randomgenbmi['m_BMI'];
       $b_BMI=$randomgenbmi['b_BMI'];
       $s_BMI=$randomgenbmi['s_BMI'];
        
     
//for male and Sedentary
       if($v=='Male' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.007';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Sedentary
       if($v=='Female' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.038';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.75),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     
     //for Male and General
       if($v=='Male' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.25),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and General
       if($v=='Female' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
      //for Male and Athlete or active
       if($v=='Male' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.975';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Athlete or active
       if($v=='Female' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.962';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
 $randomData=array(
     'firstname'=>$firstname,
     'lastname'=>$lastname,
     'gender'=>$v,
     'daydropdown'=>$valuedate,
     'monthdropdown'=>$monthkey,
     'yeardropdown'=>$valueyears,
     'age_category'=>$agerange,
     'occupation'=>$occupation,
     'sub_population'=>$subpopulation,
     'country'=>$selectedyears,
     'Random_generated_log_BMI'=>$randomprob,
     'MASS'=>$MASS,
     'BMI'=>$randombmi,
     'HEIGHT'=>$HEIGHT,
     'probability_LOG_BMI'=>$probability,
      'probability_random_BMI'=>$probabilitybmi,
     'm_BMI'=>$m_BMI,
     'b_BMI'=>$b_BMI,
     's_BMI'=>$s_BMI,
     'mean_log_Mass'=>$mean_log_Mass,
     'sd_log_Mass'=>$sd_log_Mass,
     'vp_age'=>$vp_age
     ); 
 return $randomData; 
       }//END OF FUNCTION GENERATE RANDOM
     
     //fraction random number.
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }
       //PUT MASS and BMI 
       function mass_and_bmi($vp_age,$v)
       {
            if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.38';    
        $sd_log_Mass='0.157';
        $m_BMI='0.2305';
        $b_BMI='6.44';
        $s_BMI='1.543';
            
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.385';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2325';
        $b_BMI='6.44';
        $s_BMI='1.532';
       }
       //FOR MALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.46';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2395';
        $b_BMI='6.44';
        $s_BMI='1.52';
       }

//FOR MALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.475';    
        $sd_log_Mass='0.165';
        $m_BMI='0.2435';
        $b_BMI='6.44';
        $s_BMI='1.506';
       }
      
       //FOR MALE 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.483';    
        $sd_log_Mass='0.1653';
        $m_BMI='0.2545';
        $b_BMI='5.77';
        $s_BMI='1.998';
       }
 
//FOR MALE 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.484';    
        $sd_log_Mass='0.176';
        $m_BMI='0.2567';
        $b_BMI='5.77';
        $s_BMI='1.994';
       }
 
//FOR MALE 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.494';    
        $sd_log_Mass='0.1725';
        $m_BMI='0.2842';
        $b_BMI='3.42';
        $s_BMI='1.685';
       }
       

//FOR MALE 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.486';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2868';
        $b_BMI='3.22';
        $s_BMI='1.683';
       }

//FOR MALE 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.481';    
        $sd_log_Mass='0.151';
        $m_BMI='0.2892';
        $b_BMI='3.7';
        $s_BMI='1.85';
       }
	
//FOR MALE 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.467';    
        $sd_log_Mass='0.15';
        $m_BMI='0.2892';
        $b_BMI='3.2';
        $s_BMI='1.799';
       }
      //FOR MALE 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.452';    
        $sd_log_Mass='0.148';
        $m_BMI='0.3211';
        $b_BMI='1.74';
        $s_BMI='1.87';
       }
      
       //FOR MALE 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.362';    
        $sd_log_Mass='0.143';
        $m_BMI='0.2677';
        $b_BMI='6.04';
        $s_BMI='1.85';
       }
       
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.339';    
        $sd_log_Mass='0.1375';
        $m_BMI='0.267';
        $b_BMI='6.11';
        $s_BMI='1.529';
       }
      
       
//FOR FEMALE 
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.1835';    
        $sd_log_Mass='0.1785';
        $m_BMI='0.2655';
        $b_BMI='6.24';
        $s_BMI='1.456';
            
           } 
        //FOR FEMALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.205';    
        $sd_log_Mass='0.167';
        $m_BMI='0.2686';
        $b_BMI='6.44';
        $s_BMI='1.56';
       }
      //FOR FEMALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.2442';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2685';
        $b_BMI='6.8';
        $s_BMI='1.466';
       }

//FOR FEMALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.262';    
        $sd_log_Mass='0.165';
        $m_BMI='0.279';
        $b_BMI='6.44';
        $s_BMI='1.456';
       }
      
      //FOR Female 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.293';    
        $sd_log_Mass='0.1724';
        $m_BMI='0.3018';
        $b_BMI='5.15';
        $s_BMI='1.8022';
       }
 
//FOR Female 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.176';
        $m_BMI='0.3135';
        $b_BMI='4.45';
        $s_BMI='1.8022';
       }
 

//FOR Female 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.309';    
        $sd_log_Mass='0.1844';
        $m_BMI='0.323';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }
       
//FOR Female 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.169';
        $m_BMI='0.3248';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }

//FOR Female 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.302';    
        $sd_log_Mass='0.1715';
        $m_BMI='0.3312';
        $b_BMI='3.88';
        $s_BMI='1.821';
       }

//FOR Female 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.3';    
        $sd_log_Mass='0.15';
        $m_BMI='0.3481';
        $b_BMI='2.88';
        $s_BMI='1.821';
       }
//FOR Female 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.292';    
        $sd_log_Mass='0.1575';
        $m_BMI='0.362';
        $b_BMI='3.11';
        $s_BMI='1.845';
       }
      
       //FOR Female 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.229';    
        $sd_log_Mass='0.143';
        $m_BMI='0.323';
        $b_BMI='5.11';
        $s_BMI='1.845';
       }
  
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.192';    
        $sd_log_Mass='0.143';
        $m_BMI='0.355';
        $b_BMI='3.65';
        $s_BMI='1.658';
       }
      
     $data=array('mean_log_Mass'=>$mean_log_Mass,'sd_log_Mass'=>$sd_log_Mass,'m_BMI'=>$m_BMI,'b_BMI'=>$b_BMI,'s_BMI'=>$s_BMI);
      return $data;
      
       }
    

//GET AGE RANGE
       function getAgeRange($age)
       {
       	
			 if($age >= 18 && $age <=29)
			 {
				$agerange = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $agerange = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				 $agerange = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				$agerange = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				 $agerange = '60-69' ;
			 }
			 elseif($age >=70 && $age <=79)
			 {
				$agerange = '70-79' ;
			 }
                          elseif($age >=80 && $age <=84)
			 {
				$agerange = '80-84' ;
			 }
                          elseif($age >84)
			 {
				$agerange = '84+' ;
			 }
                    
                         return $agerange;     
                         
                   }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    
    
    
      
}
?>