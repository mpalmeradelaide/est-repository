<?php

class virtual_model extends CI_Model
{
    function __construct()
    {
        parent::__construct();
          
    }
         //add_VP_Data  
      public function add_VP_Data($data) 
      { 
              $this->db->insert('virtual_person_info', $data); 
                     return $this->db->insert_id();
      } 

    //Get the Random Value In Pre-exercise Form
        public function fetch_preexercise_details($age,$mass,$height,$bmi,$gender,$subpopulation)
      {
         $fieldData=array();
        $fieldData[0] = new StdClass;
        $vp_age=$age;
        $MASS=$mass;
        $BMI=$bmi;
        $HEIGHT=$height;
        $sub_population=$subpopulation;
        $gender=$gender;
              //HEART QUESTION
        //calculate Heart Probability
      //FOR MALE HEART QUES
       if($gender=='M')
       {
        $HEARTPROB=1+(0.00000161*pow($vp_age,3.7892425)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';
        
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       }  
       
//FOR FEMALE HEART QUES
       if($gender=='F')
       {
        $HEARTPROB=1+(0.0000113*pow($vp_age,3.1941547)); 
         /*Random No*/
        $min=1;
        $max=100;
        $randomnum=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $HEARTPROB=$HEARTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $HEARTPROB=$HEARTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $HEARTPROB=$HEARTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $HEARTPROB=$HEARTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $HEARTPROB=$HEARTPROB + 2;   
        }
        // 1 Question yes no
        if($randomnum<=$HEARTPROB)
        {
        $fieldData[0]->option_1='Y';    
        }
        else
        {
         $fieldData[0]->option_1='N';      
        }
       
      }
          
//END HEART QUESTION
  
      //FOR MALE CHEST PAIN
       if($gender=='M')
       {
        $CHESTPROB=1+(0.00105*pow($vp_age,2)+0.08262*$vp_age-3.15); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       }   
      
//FOR FEMALE CHEST PAIN QUES
       if($gender=='F')
       {
        $CHESTPROB=1+(0.00223*pow($vp_age,2)-0.06514*($vp_age)+0.56); 
         /*Random No*/
        $min=1;
        $max=100;
        $chestpain=rand($min,$max);
        if($sub_population=='Sedentary')
        {
        $CHESTPROB=$CHESTPROB*1.2;   
        }
        if($sub_population=='General')
        {
        $CHESTPROB=$CHESTPROB*1.1;   
        }
         if($sub_population=='Active' || $sub_population=='Athlete')
        {
        $CHESTPROB=$CHESTPROB*0.9;   
        }
         if($BMI >= 35)
        {
        $CHESTPROB=$CHESTPROB + 4;   
        }
         if($BMI > 30 && $BMI < 35)
        {
        $CHESTPROB=$CHESTPROB + 2;   
        }
        // 1 Question yes no
        if($chestpain<=$CHESTPROB)
        {
        $fieldData[0]->option_2='Y';    
        }
        else
        {
         $fieldData[0]->option_2='N';      
        }
       } 
      // FEEL FAINT QUES
         //FOR MALE FAINT
       if($gender=='M')
       {
        $FAINTPROB=(0.00208*pow($vp_age,2)-0.09524*($vp_age)+1.7); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       }
        
       
         //FOR FEMALE FAINT
       if($gender=='F')
       {
        $FAINTPROB=(0.00262*pow($vp_age,2)-0.10952*($vp_age)+2.3); 
         /*Random No*/
        $min=1;
        $max=100;
        $faint=rand($min,$max);
       
        // 1 Question yes no
        if($faint<=$FAINTPROB)
        {
        $fieldData[0]->option_3='Y';    
        }
        else
        {
         $fieldData[0]->option_3='N';      
        }
       } 
       //ASTHMA ATTACK QUES
       
         //FOR MALE ASTHMA ATTACK
       if($gender=='M')
       {
        $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
         //FOR FEMALE ASTHMA ATTACK
       if($gender=='F')
       {
           $ASTHMAPROB=(0.00100*pow($vp_age,2)-0.14429*($vp_age)+6.17); 
         /*Random No*/
        $min=1;
        $max=100;
        $asthma=rand($min,$max);
       
        // 1 Question yes no
        if($asthma<=$ASTHMAPROB)
        {
        $fieldData[0]->option_4='Y';    
        }
        else
        {
         $fieldData[0]->option_4='N';      
        }
       } 
        
          //FOR MALE DIABETES
       if($gender=='M')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(5 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=3;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
         //FOR FEMALE DIABETES
       if($gender=='F')
       {
           If($vp_age <= 65)
        {
          $DIABETESPROB=(2 + (0.00764 * pow($vp_age,2)- 0.34286 *($vp_age) + 4.66))* 0.2; 
        }
        else
        {
         $DIABETESPROB=2;   
        }
        /*Random No*/
        $min=1;
        $max=100;
        $diabetes=rand($min,$max);
       
        // 1 Question yes no
        if($diabetes<=$DIABETESPROB)
        {
        $fieldData[0]->option_5='Y';    
        }
        else
        {
         $fieldData[0]->option_5='N';      
        }
       } 
       
          //FOR MALE BONE OR MUSCLE PROBLEM
       if($gender=='M' || $gender=='F')
       {
         $BONEPROB =0.002321* pow($vp_age,2) - 0.152143*($vp_age)+ 2.8; 
        /*Random No*/
        $min=1;
        $max=100;
        $bone=rand($min,$max);
       
        // 1 Question yes no
        if($bone<=$BONEPROB)
        {
        $fieldData[0]->option_6='Y';    
        }
        else
        {
         $fieldData[0]->option_6='N';      
        }
       } 
     
    //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=45 && $gender=='M')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
      
       //OTHER MEDICAL CONDITIONS MALE
      if($vp_age>=55 && $gender=='F')
      {
      /*Random No*/
        $min=1;
        $max=100;
        $othermed=rand($min,$max);
         if($othermed<=2)
         {
         $fieldData[0]->option_7='Y';    
         }
         else
         {
         $fieldData[0]->option_7='N';       
         } 
       }
     
      if(!isset($fieldData[0]->option_7))
      {
       $fieldData[0]->option_7='N';  
      }
       return $fieldData;      
    
       
         }//end of pre exercising Option
      
    //medication_condition
     public function medication_condition($medical_history,$gender)
     {
        
         if(count($medical_history)>0)
        {
    
    $medical_conditions=array();
    $medication=array();
    $HEART=($medical_history[0]->option_1 === 'Y')?"Y":"N";
      
//For Heart Condition
        if($HEART=='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $heartcondition=rand($min,$max);  
            if($heartcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($heartcondition >= 8)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $heartmeds=rand($min,$max);  
        if($heartmeds <= 8)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($heartmeds <= 4)
        {
         $medication[]='cardiovascular';  
        }
        if($heartmeds >= 6)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($heartmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='musculo-skeletal';   
            break;
         case 5:
        $medication[]='psychiatric';   
            break;
         case 6:
        $medication[]='renal medication';   
            break; 
        case 7:
        $medication[]='respiratory';   
            break;
         case 8:
        $medication[]='other';   
            break;
        }
          
        }
        $CHESTPAIN =($medical_history[0]->option_2 === 'Y')?"Y":"N";
        
 
       //For CHESTPAIN Condition
        if($CHESTPAIN =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $chestcondition=rand($min,$max);  
            if($chestcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($chestcondition >= 9)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $chestmeds=rand($min,$max);  
        if($chestmeds <= 9)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($chestmeds <= 6)
        {
         $medication[]='cardiovascular';  
        }
        if($chestmeds >= 3)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($chestmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='other';   
            break;
        
        }
          
        }
       
      $FAINT  =($medical_history[0]->option_3 === 'Y')?"Y":"N";
         
//For FAINT Condition
        if($FAINT =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $faintcondition=rand($min,$max);  
            if($faintcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        switch($faintcondition)
        {
        case 7:  
        $medical_conditions[]='thyroid disease'; 
        break;    
        case 8:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 9:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 10:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        }
        $min_other=1;
        $max_other=10;
        $othercondition=rand($min_other,$max_other);  
        if($othercondition>=7)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $faintmeds=rand($min,$max);  
        if($faintmeds <= 3)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($faintmeds <= 5)
        {
         $medication[]='cardiovascular';  
        }
        if($faintmeds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($faintmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        
        $ASTHMA  =($medical_history[0]->option_4 === 'Y')?"Y":"N";

       //For ASTHMA Condition
        if($ASTHMA =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $asthmacondition=rand($min,$max);  
            if($asthmacondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($asthmacondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='cerebrovascular';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($asthmacondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $asthmameds=rand($min,$max);  
        if($asthmameds >= 2)
        {
         $medication[]='respiratory';  
        }
        if($asthmameds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($asthmameds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($asthmameds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        //For DIABETES Condition
         $DIABETES   =($medical_history[0]->option_5 === 'Y')?"Y":"N";
        if($DIABETES =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $diabetescondition=rand($min,$max);  
            if($diabetescondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($diabetescondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='arthritis or osteoporosis';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($diabetescondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $diabetesmeds=rand($min,$max);  
        if($diabetesmeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($diabetesmeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($diabetesmeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($diabetesmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
        
        }
        if($diabetesmeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        }
       
         //For BONE Condition
         $BONE =($medical_history[0]->option_6 === 'Y')?"Y":"N";
        if($BONE =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $bonecondition=rand($min,$max);  
            if($bonecondition <= 8)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($bonecondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='diabetes';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($bonecondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $bonemeds=rand($min,$max);  
        if($bonemeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($bonemeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($bonemeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($bonemeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($bonemeds >= 5 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($bonemeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        
        }

        
       //For OTHERMED  Condition
         $OTHERMED =($medical_history[0]->option_7 === 'Y')?"Y":"N";
        if($OTHERMED =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $othercondition=rand($min,$max);  
            if($othercondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($othercondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='psychiatric illness';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($othercondition >= 6)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $lastmeds=rand($min,$max);  
        if($lastmeds >= 8)
        {
         $medication[]='psychiatric illness';  
        }
        if($lastmeds <= 2)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($lastmeds >= 6)
        {
         $medication[]='other';  
        }
        switch($lastmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='metabolic';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($lastmeds >= 9 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($lastmeds >= 7) 
        {
         $medication[]='psychiatric';     
        }
       }
         //##################IF ALL NO ANSWER SELECTED#######################################      
     if($HEART=='N' && $CHESTPAIN=='N' && $FAINT=='N' && $ASTHMA=='N' && $DIABETES=='N' && $BONE=='N' && $OTHERMED=='N')
     {
        
         $min=1;
        $max=100;
        $medcondition=rand($min,$max); 
       
        if($medcondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';  
          $medication[]='other';
        }
        switch($medcondition)
        {
        case 3:
        $medical_conditions[]='cardiovascular';   
        $medication[]='LIPID-lowering medication';  
            break;
         case 4:
        $medical_conditions[]='cerebrovascular';
        $medication[]='other';       
            break;
        case 5:
            $medical_conditions[]='diabetes';
            $medication[]='diabetic';   
            break;
         case 6:
             $medical_conditions[]='kidney disease';
             $medication[]='BLOOD PRESSURE-lowering medication';   
            break;
         case 7:
         $medical_conditions[]='liver or metabolic disorder';
         $medication[]='metabolic';      
            break;
         case 8:
         $medical_conditions[]='psychiatric illness';
         $medication[]='psychiatric';      
            break;
        case 9:
         $medical_conditions[]='respiratory disease';
         $medication[]='respiratory';      
            break;
         case 10:
         $medical_conditions[]='thyroid disease';
         $medication[]='metabolic';      
            break;
         }
     if($medcondition >= 98)
     {
     $medical_conditions[]='other';
         $medication[]='other';         
     }
   }
                
        $medication_selected=implode(',',array_unique($medication));
        $medical_cond_selected=implode(',',array_unique($medical_conditions));
       return(array('medication_selected'=>$medication_selected,'medical_cond_selected'=>$medical_cond_selected));
       } 
     }
 //GET AGE RANGE
  public function getagerangeforvp($age)
  { 
      if($age >= 18 && $age <=29)
			 {
				 $age_range = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $age_range = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				$age_range = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				 $age_range = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				$age_range = '60-69' ;
			 }
			 elseif($age >=70)
			 {
				 $age_range = '70+' ;
			 }
  
                         return $age_range;
                         }
    
     

 //Get zscore Mean and SD
  function getzscore_meansd($age_range,$gender)
  {
   if($gender=='M')
   {
      if($age_range == "18-29")
        {
            $mean = 46.0 ;
            $sd = 8.50 ;                   
            
        }else if($age_range == "30-39") {
        
           $mean = 42.8 ;
            $sd = 8.25 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 36.1 ;
           $sd = 7.35 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 31.0 ;
           $sd = 6.84 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 26.9 ;
           $sd = 6.36 ;
        
      }else if($age_range == "70+") {
        
           $mean = 23.6 ;
           $sd = 5.91 ;        
      }   
   }  
  //FOR FEMALE
   if($gender=='F')
   {
    if($age_range == "18-29")
        {
            $mean = 33.5 ;
            $sd = 6.20 ;
            
        }else if($age_range == "30-39") {
        
           $mean = 29.1 ;
           $sd = 5.77 ;
        
      }else if($age_range == "40-49") {
        
           $mean = 25.4 ;
           $sd = 5.36 ;
        
      }else if($age_range == "50-59") {
        
           $mean = 22.1 ;
           $sd = 4.99 ;
        
      }else if($age_range == "60-69") {
        
           $mean = 19.2 ;
           $sd = 4.64 ;
        
      }else if($age_range == "70+") {
        
           $mean = 16.7 ;
           $sd = 4.31 ;        
      }                  
   }
      
   $meansd_array=array('mean'=>$mean,'sd'=>$sd);
   return $meansd_array;
      }
   
//GET VERTICAL JUMP
  public function get_Vertical_jump($age,$subpopulation,$gender){
      $setagerange=$this->getagerangeforvp($age);
      $object1 = new PHPExcel_Calculation_Statistical(); 
      $age =$age;
      $subpopulation = $subpopulation;
        $age_range = $setagerange;
        $gender = $gender;
    if($gender=='M' && $age_range=='18-29')
   {
    $MeanVJ='50.5';
    $SDVJ='8.90';
   }
   if($gender=='M' && $age_range=='30-39')
   {
    $MeanVJ='45.6';
    $SDVJ='7.91';
   }
   if($gender=='M' && $age_range=='40-49')
   {
    $MeanVJ='39.1';
    $SDVJ='7.35';
   }
  if($gender=='M' && $age_range=='50-59')
   {
    $MeanVJ='34.0';
    $SDVJ='6.84';
   }  
   if($gender=='M' && $age_range=='60-69')
   {
    $MeanVJ='29.3';
    $SDVJ='6.36';
   }
    if($gender=='M' && $age_range=='70+')
   {
    $MeanVJ='24.2';
    $SDVJ='5.91';
   }    
  
   //For FEMALE
      if($gender=='F' && $age_range=='18-29')
   {
    $MeanVJ='37.5';
    $SDVJ='6.20';
   }
   if($gender=='F' && $age_range=='30-39')
   {
    $MeanVJ='33.1';
    $SDVJ='5.77';
   }
   if($gender=='F' && $age_range=='40-49')
   {
    $MeanVJ='28.1';
    $SDVJ='5.36';
   }
  if($gender=='F' && $age_range=='50-59')
   {
    $MeanVJ='24.1';
    $SDVJ='4.99';
   }  
   if($gender=='F' && $age_range=='60-69')
   {
    $MeanVJ='20.1';
    $SDVJ='4.64';
   }
    if($gender=='F' && $age_range=='70+')
   {
    $MeanVJ='16.4';
    $SDVJ='4.31';
   } 
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
   //For Subpopulation
  if($subpopulation=='Sedentary')
  {
   $MeanVJ = $MeanVJ - (1.5 * $SDVJ);   
    $SDVJ= 0.65 * $SDVJ;
  }
  //For General
  if($subpopulation=='General')
  {
      $MeanVJ = $MeanVJ - (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  } 
  //For Active
  
  if($subpopulation=='Active')
  {
    $MeanVJ = $MeanVJ + (0.5 * $SDVJ);   
    $SDVJ= 0.75 * $SDVJ;
  }  
  
//For Active
  if($subpopulation=='Athlete')
  {
    $MeanVJ = $MeanVJ + (1.5 * $SDVJ);   
    $SDVJ= 0.5 * $SDVJ;
  } 
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
  
      $x=$object1->NORMINV($probability,$MeanVJ,$SDVJ);
      $randomprob=round($x,9);   
      $vj=$randomprob;
      //Get Mead and SD for Zscore
      $get_meansd=$this->getzscore_meansd($age_range,$gender);
      $mean=$get_meansd['mean'];
      $sd=$get_meansd['sd'];
    $z_score = ($vj - $mean) / $sd ; 
    $z_score=round($z_score,3);  
    
    $perform_percent = $object1->NORMSDIST($z_score);
    $perform_percent=$perform_percent * 100;
    $perform_percent=round($perform_percent,6);
    $vj=round($vj,1);
    $vj = floor($vj*2)/2;
   
    $arrayvj=array('vj'=>$vj,'perform_percent'=>$perform_percent,'z_score'=>$z_score);

  return ($arrayvj);
  }


//GET AGE   
     function getAge( $dob,$tdate)
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
      function isExist( $table, $c_id )
        {
          if($table=='client_info'){
          $query = $this->db->query("SELECT `id` FROM `$table` WHERE id = '".$c_id ."'");
          }else{
            $query = $this->db->query("SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'");
          }
          
                //$this->str = "SELECT `id` FROM `$table` WHERE c_id = '".$c_id ."'";
		//$this->ExecuteQuery();
		//$this->CountRow();
		if($query->num_rows())return true;
		return false;
        }  
        
         function fetchDetail( $table )
            {
            //  $c_id = $this->session->userdata('userid');
              $c_id = $_SESSION['userid'];
              $query = $this->db->query("SELECT * FROM `$table` WHERE c_id = '".$c_id ."'");


                    if($query->num_rows() > 0)
                    {
                        return $query->result();
                    }
                    return array();
            }  
            
     
    
    //GET RANDOM VAULES 
    function generateRandomValues($gender="",$population="")
    {
       
       //FOR MALE FEMALE 
       if($gender=="")
       {   
       $genderarray = array( '1'  => 'Male','2'  => 'Female');  
       $k = array_rand($genderarray);
       $v = $genderarray[$k]; 
       $selectedgender=array('gendervalue'=>$k,'gendertext'=>$v);
        //FOR MALE FEMALE
       }
    else{
        if($gender=='M')
        {
        $k='1';    
       $v="Male";
        }
        if($gender=='F')
        {
        $k='2';    
        $v="Female";
        }
        }
       
       if($k=='1')
       {
       $columnname="boys_fname";    
       }
      if($k=='2')
       {
        $columnname="girls_fname";   
       }
       
       //GENERATE RANDOM NAMES 
       $randomnamesQry="SELECT $columnname as firstname FROM random_names where $columnname!='' ORDER BY RAND() LIMIT 1";
       $query = $this->db->query($randomnamesQry);
       if($query->num_rows() > 0) {
         $result=$query->row_array();
        }
       $firstname=$result['firstname'];
       $randomlastQry="SELECT last_name FROM random_names where last_name!='' ORDER BY RAND() LIMIT 1";
       $queryrandom = $this->db->query($randomlastQry);
       if($queryrandom->num_rows() > 0) {
         $resultlastname=$queryrandom->row_array();
        }
       $lastname=$resultlastname['last_name'];
       $selectednames=array('firstname'=>$firstname,'lastname'=>$lastname);
     
       //END GENERATE RANDOM NAMES 
       
       //RANDOM DATE OF BIRTH GENERATION
        
       $dates = array(
                                        '01' => '01',  
                                        '02' => '02',  
                                        '03' => '03',  
                                        '04' => '04',  
                                        '05' => '05',  
                                        '06' => '06',  
                                        '07' => '07',  
                                        '08' => '08',  
                                        '09' => '09',  
                                        '10' => '10',  
                                        '11' => '11',  
                                        '12' => '12',  
                                        '13' => '13',  
                                        '14' => '14',  
                                        '15' => '15',  
                                        '16' => '16',  
                                        '17' => '17',  
                                        '18' => '18',  
                                        '19' => '19',  
                                        '20' => '20',  
                                        '21' => '21',  
                                        '22' => '22',  
                                        '23' => '23',  
                                        '24' => '24',  
                                        '25' => '25',  
                                        '26' => '26', 
                                        '27' => '27', 
                                        '28' => '28', 
                                        '29' => '29', 
                                        '30' => '30', 
                                        '31' => '31'                                      
                                      );
       
                            $month = array(
                                    '1' => 'JAN',
                                    '2' => 'FEB',
                                    '3' => 'MAR',
                                    '4' => 'APR',
                                    '5' => 'MAY',
                                    '6' => 'JUN',
                                    '7' => 'JUL',
                                    '8' => 'AUG',
                                    '9' => 'SEP',
                                    '10' => 'OCT',
                                    '11' => 'NOV',
                                    '12' => 'DEC');
       
       
       $years = array('1998'=>'1998', '1997'=>'1997', '1996'=>'1996', '1995'=>'1995', '1994'=>'1994', '1993'=>'1993', '1992'=>'1992', '1991'=>'1991', '1990'=>'1990', '1989'=>'1989', '1988'=>'1988', '1987'=>'1987' , '1986'=>'1986', '1985'=>'1985' , '1984'=>'1984', '1983'=>'1983', '1982'=>'1982' , '1981'=>'1981', '1980'=>'1980', '1979'=>'1979', '1978'=>'1978', '1977'=>'1977', '1976'=>'1976', '1975'=>'1975','1974'=>'1974', '1973'=>'1973', '1972'=>'1972', '1971'=>'1971', '1970'=>'1970', '1969'=>'1969', '1968'=>'1968', '1967'=>'1967', '1966'=>'1966', '1965'=>'1965', '1964'=>'1964', '1963'=>'1963', '1962'=>'1962', '1961'=>'1961', '1960'=>'1960', '1959'=>'1959', '1958'=>'1958', '1957'=>'1957', '1956'=>'1956', '1955'=>'1955', '1954'=>'1954' , '1953'=>'1953', '1952'=>'1952', '1951'=>'1951', '1950'=>'1950', '1949'=>'1949', '1948'=>'1948', '1947'=>'1947', '1946'=>'1946', '1945'=>'1945', '1944'=>'1944', '1943'=>'1943', '1942'=>'1942', '1941'=>'1941', '1940'=>'1940', '1939'=>'1939', '1938'=>'1938', '1937'=>'1937', '1936'=>'1936', '1935'=>'1935', '1934'=>'1934', '1933'=>'1933', '1932'=>'1932', '1931'=>'1931', '1930'=>'1930', '1929'=>'1929', '1928'=>'1928', '1927'=>'1927');		
       /*Day Selection*/
       $daykey = array_rand($dates);
       $valuedate = $dates[$daykey]; 
       $selecteddates=array('datekey'=>$daykey,'datevalue'=>$valuedate);
        /*Month Selection*/
       $monthkey = array_rand($month);
       $valuemonth = $month[$monthkey]; 
       $selectedmonth=array('monthkey'=>$monthkey,'monthvalue'=>$valuemonth);
        /*Month Selection*/
        
       /*Year Selection*/
       $yearskey = array_rand($years);
       $valueyears = $years[$yearskey]; 
       $selectedyears=array('yearskey'=>$yearskey,'yearsvalue'=>$valueyears);
        /*Year Selection*/
       
       /*COUNTRY PROFILE*/
        $min=1;
        $max=100;
        $randomcountryval=rand($min,$max);
        if($randomcountryval<90)
        {
         $selectedyears=array('countrykey'=>'AU','countryvalue'=>'Australia');   
        }
       else
       {
        $randomctryQry="SELECT country_code,country_name FROM country ORDER BY RAND() LIMIT 1";
       $queryrandomctry = $this->db->query($randomctryQry);
       if($queryrandomctry->num_rows() > 0) {
         $resultctry=$queryrandomctry->row_array();
        }    
       $selectedyears=array('countrykey'=>$resultctry['country_code'],'countryvalue'=>$resultctry['country_name']);    
       } 
      /*END COUNTRY PROFILE*/
      /*OCCUPATION PROFILE*/
     
        $minoccu=1;
        $maxoccu=30;
        $randomoccuval=rand($minoccu,$maxoccu);
       
       $occupationQry="SELECT `occupation` FROM occupation where id=$randomoccuval";
       $queryrandomoccu = $this->db->query($occupationQry);
       if($queryrandomoccu->num_rows() > 0) {
         $resultoccupation=$queryrandomoccu->row_array();
        }    
       $selectedoccupation=array('occupationkey'=>$resultoccupation['occupation'],'occupationval'=>$resultoccupation['occupation']);
       $occupation=$resultoccupation['occupation']; 
       /*END OCCUPATION PROFILE*/ 
       $dateofBirth=$valueyears.'-'.$valuemonth.'-'.$valuedate;
       $datebrth= date("Y-m-d",strtotime($dateofBirth));
       $dob = strtotime($datebrth);
       $tdate = strtotime(date('Y-m-d'));
       $vp_age=$this->getAge($dob, $tdate);
       $agerange=$this->getAgeRange($vp_age);
      //SUB-POPULATION PROFILE CONDITION
       $subpopulation="";
      
       if($population=="")
       {
        //FOR MALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 6)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 6 && $RN< 20)
            {
            $subpopulation="General";    
            }
             else if($RN >=20  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
     
          }
      
      //FOR FEMALE 18-24
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 8)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 8 && $RN< 24)
            {
            $subpopulation="General";    
            }
             else if($RN >=24  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
        
            }
       
        //FOR MALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 9)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 9 && $RN< 30)
            {
            $subpopulation="General";    
            }
             else if($RN >=30  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       
            }
      
      //FOR FEMALE 25-34
       if(($vp_age >=25 && $vp_age<=34) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 98)
            {
            $subpopulation="Active";    
            }
            if($RN >= 98)
            {
            $subpopulation="Athlete";    
            }
       }
       
       //FOR Male 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 11)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 11 && $RN< 34)
            {
            $subpopulation="General";    
            }
             else if($RN >=34  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //FOR Female 35-44 
       if(($vp_age >=35 && $vp_age<=44) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 14)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 14 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39  && $RN< 99)
            {
            $subpopulation="Active";    
            }
            if($RN >= 99)
            {
            $subpopulation="Athlete";    
            }
       }
       //ANother Condition Based on Subpopulation and Age..
       if($subpopulation=='Athlete' && $vp_age>40)
       {
        $subpopulation="Active";    
       }
       //FOR MALE 45-54 yr
       if(($vp_age >=45 && $vp_age<=54) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 15)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 15 && $RN< 39)
            {
            $subpopulation="General";    
            }
             else if($RN >=39)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 45-54 
       if(($vp_age >=45 && $vp_age<=54) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 18)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 18 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR MALE 55-64 yr
       if(($vp_age >=55 && $vp_age<=64) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 16)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 16 && $RN< 43)
            {
            $subpopulation="General";    
            }
             else if($RN >=43)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 55-64 
       if(($vp_age >=55 && $vp_age<=64) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       
        //FOR MALE 65-74  yr
       if(($vp_age >=65 && $vp_age<=74) && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 20)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 20 && $RN< 48)
            {
            $subpopulation="General";    
            }
             else if($RN >=48)
            {
            $subpopulation="Active";    
            }
       }
       //FOR Female 65-74 
       if(($vp_age >=65 && $vp_age<=74) && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 24)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 24 && $RN< 52)
            {
            $subpopulation="General";    
            }
             else if($RN >=52)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Male 75+ 
       if($vp_age>= 75 && $v=='Male')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 31)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 31 && $RN< 62)
            {
            $subpopulation="General";    
            }
             else if($RN >=62)
            {
            $subpopulation="Active";    
            }
       }
       
       //FOR Female 75+ 
       if($vp_age>= 75 && $v=='Female')
       {
       $min=1;
        $max=100;
        $RN=rand($min,$max);    
            if($RN < 36)
            {
            $subpopulation="Sedentary";    
            }
             else if($RN >= 36 && $RN< 68)
            {
            $subpopulation="General";    
            }
             else if($RN >=68)
            {
            $subpopulation="Active";    
            }
       }
    }//If Sub-population is not taken firstly then it generates
     else
     {
     $subpopulation=$population;    
     }
    
     $randomgenbmi=$this->mass_and_bmi($vp_age,$v); 
      
     $object1 = new PHPExcel_Calculation_Statistical();
      $mean_log_Mass=$randomgenbmi['mean_log_Mass'];
      $sd_log_Mass=$randomgenbmi['sd_log_Mass'];
       $m_BMI=$randomgenbmi['m_BMI'];
       $b_BMI=$randomgenbmi['b_BMI'];
       $s_BMI=$randomgenbmi['s_BMI'];
        
     
//for male and Sedentary
       if($v=='Male' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.007';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Sedentary
       if($v=='Female' && $subpopulation=='Sedentary')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1.038';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.75),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     
     //for Male and General
       if($v=='Male' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.25),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and General
       if($v=='Female' && $subpopulation=='General')
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='1';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
      $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)+0.5),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
      //for Male and Athlete or active
       if($v=='Male' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.975';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
     
     //for Female and Athlete or active
       if($v=='Female' && ($subpopulation=='Athlete' || $subpopulation=='Active'))
     {
      $probability=$this->frand(0,1);
      $probability=round($probability,9); 
      $massfactor='0.962';
      $x=$object1->NORMINV($probability,$mean_log_Mass,$sd_log_Mass);
      $randomprob=round($x,9);   
      $MASS=(exp($randomprob))*$massfactor;
      $MASS=round($MASS,8); 
      
      //GET BMI
      $probabilitybmi=$this->frand(0,1);
     $probabilitybmi=round($probabilitybmi,9); 
     $randombmi=$object1->NORMINV($probabilitybmi,(($m_BMI*$MASS+$b_BMI)),$s_BMI);
       $BMI=round($randombmi,8);
     
      //GET BMI
      $getheight=sqrt($MASS/$BMI)*100;
       $HEIGHT=round($getheight,7);
     }
 $randomData=array(
     'firstname'=>$firstname,
     'lastname'=>$lastname,
     'gender'=>$v,
     'daydropdown'=>$valuedate,
     'monthdropdown'=>$monthkey,
     'yeardropdown'=>$valueyears,
     'age_category'=>$agerange,
     'occupation'=>$occupation,
     'sub_population'=>$subpopulation,
     'country'=>$selectedyears,
     'Random_generated_log_BMI'=>$randomprob,
     'MASS'=>$MASS,
     'BMI'=>$randombmi,
     'HEIGHT'=>$HEIGHT,
     'probability_LOG_BMI'=>$probability,
      'probability_random_BMI'=>$probabilitybmi,
     'm_BMI'=>$m_BMI,
     'b_BMI'=>$b_BMI,
     's_BMI'=>$s_BMI,
     'mean_log_Mass'=>$mean_log_Mass,
     'sd_log_Mass'=>$sd_log_Mass,
     'vp_age'=>$vp_age
     ); 
 return $randomData; 
       }//END OF FUNCTION GENERATE RANDOM
     
     //fraction random number.
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }
       //PUT MASS and BMI 
       function mass_and_bmi($vp_age,$v)
       {
            if(($vp_age >=18 && $vp_age<=24) && $v=='Male')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.38';    
        $sd_log_Mass='0.157';
        $m_BMI='0.2305';
        $b_BMI='6.44';
        $s_BMI='1.543';
            
           } 
           //FOR MALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.385';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2325';
        $b_BMI='6.44';
        $s_BMI='1.532';
       }
       //FOR MALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.46';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2395';
        $b_BMI='6.44';
        $s_BMI='1.52';
       }

//FOR MALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Male')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.475';    
        $sd_log_Mass='0.165';
        $m_BMI='0.2435';
        $b_BMI='6.44';
        $s_BMI='1.506';
       }
      
       //FOR MALE 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.483';    
        $sd_log_Mass='0.1653';
        $m_BMI='0.2545';
        $b_BMI='5.77';
        $s_BMI='1.998';
       }
 
//FOR MALE 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.484';    
        $sd_log_Mass='0.176';
        $m_BMI='0.2567';
        $b_BMI='5.77';
        $s_BMI='1.994';
       }
 
//FOR MALE 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.494';    
        $sd_log_Mass='0.1725';
        $m_BMI='0.2842';
        $b_BMI='3.42';
        $s_BMI='1.685';
       }
       

//FOR MALE 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.486';    
        $sd_log_Mass='0.159';
        $m_BMI='0.2868';
        $b_BMI='3.22';
        $s_BMI='1.683';
       }

//FOR MALE 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.481';    
        $sd_log_Mass='0.151';
        $m_BMI='0.2892';
        $b_BMI='3.7';
        $s_BMI='1.85';
       }
	
//FOR MALE 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.467';    
        $sd_log_Mass='0.15';
        $m_BMI='0.2892';
        $b_BMI='3.2';
        $s_BMI='1.799';
       }
      //FOR MALE 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.452';    
        $sd_log_Mass='0.148';
        $m_BMI='0.3211';
        $b_BMI='1.74';
        $s_BMI='1.87';
       }
      
       //FOR MALE 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.362';    
        $sd_log_Mass='0.143';
        $m_BMI='0.2677';
        $b_BMI='6.04';
        $s_BMI='1.85';
       }
       
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Male')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.339';    
        $sd_log_Mass='0.1375';
        $m_BMI='0.267';
        $b_BMI='6.11';
        $s_BMI='1.529';
       }
      
       
//FOR FEMALE 
       if(($vp_age >=18 && $vp_age<=24) && $v=='Female')
           {
        //MASS AND BMI VALUE
        $mean_log_Mass='4.1835';    
        $sd_log_Mass='0.1785';
        $m_BMI='0.2655';
        $b_BMI='6.24';
        $s_BMI='1.456';
            
           } 
        //FOR FEMALE 25-29
       if(($vp_age >=25 && $vp_age<=29) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.205';    
        $sd_log_Mass='0.167';
        $m_BMI='0.2686';
        $b_BMI='6.44';
        $s_BMI='1.56';
       }
      //FOR FEMALE 30-34
       if(($vp_age >=30 && $vp_age<=34) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.2442';    
        $sd_log_Mass='0.177';
        $m_BMI='0.2685';
        $b_BMI='6.8';
        $s_BMI='1.466';
       }

//FOR FEMALE 35-39
       if(($vp_age >=35 && $vp_age<=39) && $v=='Female')
       {
      
        //MASS AND BMI VALUE
        $mean_log_Mass='4.262';    
        $sd_log_Mass='0.165';
        $m_BMI='0.279';
        $b_BMI='6.44';
        $s_BMI='1.456';
       }
      
      //FOR Female 40-44
       if(($vp_age >=40 && $vp_age<=44) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.293';    
        $sd_log_Mass='0.1724';
        $m_BMI='0.3018';
        $b_BMI='5.15';
        $s_BMI='1.8022';
       }
 
//FOR Female 45-49
       if(($vp_age >=45 && $vp_age<=49) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.176';
        $m_BMI='0.3135';
        $b_BMI='4.45';
        $s_BMI='1.8022';
       }
 

//FOR Female 50-54
       if(($vp_age >=50 && $vp_age<=54) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.309';    
        $sd_log_Mass='0.1844';
        $m_BMI='0.323';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }
       
//FOR Female 55-59
       if(($vp_age >=55 && $vp_age<=59) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.303';    
        $sd_log_Mass='0.169';
        $m_BMI='0.3248';
        $b_BMI='4.11';
        $s_BMI='1.92';
       }

//FOR Female 60-64
       if(($vp_age >=60 && $vp_age<=64) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.302';    
        $sd_log_Mass='0.1715';
        $m_BMI='0.3312';
        $b_BMI='3.88';
        $s_BMI='1.821';
       }

//FOR Female 65-69
       if(($vp_age >=65 && $vp_age<=69) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.3';    
        $sd_log_Mass='0.15';
        $m_BMI='0.3481';
        $b_BMI='2.88';
        $s_BMI='1.821';
       }
//FOR Female 70-74
       if(($vp_age >=70 && $vp_age<=74) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.292';    
        $sd_log_Mass='0.1575';
        $m_BMI='0.362';
        $b_BMI='3.11';
        $s_BMI='1.845';
       }
      
       //FOR Female 75-79
       if(($vp_age >=75 && $vp_age<=79) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.229';    
        $sd_log_Mass='0.143';
        $m_BMI='0.323';
        $b_BMI='5.11';
        $s_BMI='1.845';
       }
  
//FOR MALE 80+
       if(($vp_age >=80) && $v=='Female')
       {
      //MASS AND BMI VALUE
        $mean_log_Mass='4.192';    
        $sd_log_Mass='0.143';
        $m_BMI='0.355';
        $b_BMI='3.65';
        $s_BMI='1.658';
       }
      
     $data=array('mean_log_Mass'=>$mean_log_Mass,'sd_log_Mass'=>$sd_log_Mass,'m_BMI'=>$m_BMI,'b_BMI'=>$b_BMI,'s_BMI'=>$s_BMI);
      return $data;
      
       }
    

//GET AGE RANGE
       function getAgeRange($age)
       {
       	
			 if($age >= 18 && $age <=29)
			 {
				$agerange = '18-29' ;
			 }
			 elseif($age >= 30 && $age <=39)
			 {
				 $agerange = '30-39' ;
			 }
			 elseif($age >= 40 && $age <=49)
			 {
				 $agerange = '40-49' ;
			 }
			 elseif($age >= 50 && $age <=59)
			 {
				$agerange = '50-59' ;
			 }
			 elseif($age >= 60 && $age <=69)
			 {
				 $agerange = '60-69' ;
			 }
			 elseif($age >=70 && $age <=79)
			 {
				$agerange = '70-79' ;
			 }
                          elseif($age >=80 && $age <=84)
			 {
				$agerange = '80-84' ;
			 }
                          elseif($age >84)
			 {
				$agerange = '84+' ;
			 }
                    
                         return $agerange;     
                         
                   }
    
     function isCodeExist(  $code )
        {
         
          $query = $this->db->query("SELECT `id` FROM `code_info` WHERE code = '".$code ."'");
         
          
		if($query->num_rows())return true;
		return false;
        } 
    
    
    
      
}
?>