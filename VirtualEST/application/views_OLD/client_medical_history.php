<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medical History</title>
<link href="<?php echo "$base/$css" ;?>" rel="stylesheet" type="text/css">

<!--For scroller start-->
<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script>
  $(function(){
     proceed = <?php  echo $fieldData[0]->option_8 ;  ?> 
            proceedTest = proceed.value
      if( proceedTest == 'proceed'  ){
      $("#proceed").prop('checked', true);
      /*
        if($(".yes:checked").length > 0) {
                   $("#guide").prop('checked', true);
                 }else{

                     // $("#proceed").removeAttr("checked");
                      $("#proceed").prop('checked', true);
                 } */
     // }else{
         
                   //   $("#proceed").prop('checked', true);
                 
          
      }else{
         $("#guide").prop('checked', true);
          
      }
      $(".yes").click(function(){

		if($(".yes:checked").length > 0) {
                    
			 $("#guide").prop('checked', true);
		}else{
                   
                    // $("#proceed").removeAttr("checked");
                     $("#proceed").prop('checked', true);;
                } 
	});
      
      $(".no").click(function(){

		if($(".yes:checked").length > 0) {
                     
			 $("#guide").prop('checked', true);;
		}else{
                 
                     
                    // $("#proceed").removeAttr("checked");
                     $("#proceed").prop('checked', true);
                } 
	});
      
	  
	   $(window).bind('scroll', function() {
		    
	   var navHeight = $( window ).height() - 400;
	  // console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});
	});
        
 


	


  
</script>
<!--For scroller End-->

</head>
   <?php // print_r($fieldData);
//var_dump($this->session->all_userdata());
?>
<body>
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo" id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="auto" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="post" id="inputs"> -->
    <?php echo form_open('welcome/saveClientMedicalInfo',$attributes, $hidden); ?>
    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required  value="<?php echo $_SESSION['user_first_name'] ;?>"  disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled"><input name ="submitMedical" type="submit" value="" title="edit client details" /></span>
     </div>
<!--	</form> -->
</div><!--End login --> 




 <!--Start Mid --> 
  <div class="mid3">
  <?php
$hidden = array('userid' => $id  );
//print_r($hidden);
//echo "aaa";
//die;

  $attributes = array('id' => 'myform' , 'name'=>'myform');
 echo form_open('welcome/saveClientMedicalInfo',$attributes, $hidden); ?>
   
<!--Start contain --> 
<div class="contain">
   
   <!--Start left --> 
   <div class="left">
   		<div class="btn">
        <?php echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
        </div>

   </div><!--End Left --> 
 
 
 <?php //print_r($fieldData); ?>
   <!--Start right --> 
   <div class="right">
   		<div class="right-head">Medical History</div>
   		<div class="right-section">
      
      <span><ol class="gen"><li>Has your doctor ever told you that you have a heart condition or have you ever suffered a stroke?</li></ol>
          <?php  $radio_is_checked = ($fieldData[0]->option_1 === 'N')?"checked":"";
          echo form_radio(array("name"=>"options_1","value"=>"N",'checked' =>'checked','class'=>'no')); ?>&nbsp;No&nbsp;&nbsp;
        <?php  $radio_is_checked = ($fieldData[0]->option_1 === 'Y')?"checked":"";
        echo form_radio(array("name"=>"options_1","id"=>"yes","value"=>"Y",'checked' =>$radio_is_checked ,'class'=>'yes')); ?>&nbsp;Yes</span>
        

      
      
      <span><ol class="gen" start="2"><li>Do you ever  experience unexplained pains in your chest at rest or during physical activity/exercise?</li></ol>
          <?php  $radio_is_checked = ($fieldData[0]->option_2 === 'N')?"checked":"";
                 echo form_radio(array("name"=>"options_2","id"=>"no","value"=>"N", 'checked' =>'checked', 'class'=>'no')); ?>&nbsp;No&nbsp;&nbsp;
         <?php $radio_is_checked = ($fieldData[0]->option_2 === 'Y')?"checked":"";
               echo form_radio(array("name"=>"options_2","id"=>"yes","value"=>"Y","checked"=>$radio_is_checked ,'class'=>'yes' )); ?>&nbsp;Yes</span>
      
      <span><ol class="gen" start="3"><li>Do you ever feel faint or have spells of dizziness during physical activity/exercise that causes you to lose balance?</li></ol>
          <?php  $radio_is_checked = ($fieldData[0]->option_3 === 'N')?"checked":"";
          echo form_radio(array("name"=>"options_3","id"=>"no","value"=>"N", 'checked' =>'checked', 'class'=>'no')); ?>&nbsp;No&nbsp;&nbsp;
<?php  $radio_is_checked = ($fieldData[0]->option_3 === 'Y')?"checked":"";
        echo form_radio(array("name"=>"options_3","id"=>"yes","value"=>"Y","checked"=>$radio_is_checked ,'class'=>'yes' )); ?>&nbsp;Yes</span>
      
      <span><ol class="gen" start="4"><li>Have you had an asthma attack that required immediate medical attention at any time over the last 12 months?</li></ol>
          <?php $radio_is_checked = ($fieldData[0]->option_4 === 'N')?"checked":"";
          echo form_radio(array("name"=>"options_4","id"=>"no","value"=>"N",'checked' =>'checked', 'class'=>'no')); ?>&nbsp;No&nbsp;&nbsp;
<?php $radio_is_checked = ($fieldData[0]->option_4 === 'Y')?"checked":"";
        echo form_radio(array("name"=>"options_4","id"=>"yes","value"=>"Y","checked"=>$radio_is_checked ,'class'=>'yes' )); ?>&nbsp;Yes</span>
      
       <span><ol class="gen" start="5"><li>If you have diabetes (type 1 or type 2) have you had trouble controlling your blood glucose in the last 3 months?</li></ol>
           <?php $radio_is_checked = ($fieldData[0]->option_5 === 'N')?"checked":"";
           echo form_radio(array("name"=>"options_5","id"=>"no","value"=>"N",'checked' =>'checked', 'class'=>'no' )); ?>&nbsp;No&nbsp;&nbsp;
<?php $radio_is_checked = ($fieldData[0]->option_5 === 'Y')?"checked":"";
echo form_radio(array("name"=>"options_5","id"=>"yes","value"=>"Y","checked"=>$radio_is_checked ,'class'=>'yes')); ?>&nbsp;Yes</span>
       
       <span><ol class="gen" start="6"><li> Do you have any diagnosed muscle, bone or joint problem that you have been told could be made worse by participating in physical activity/exercise?</li></ol>
           <?php $radio_is_checked = ($fieldData[0]->option_6 === 'N')?"checked":"";
           echo form_radio(array("name"=>"options_6","id"=>"no","value"=>"N",'checked' =>'checked', 'class'=>'no' )); ?>&nbsp;No&nbsp;&nbsp;
<?php $radio_is_checked = ($fieldData[0]->option_6 === 'Y')?"checked":"";
echo form_radio(array("name"=>"options_6","id"=>"yes","value"=>"Y", "checked"=>$radio_is_checked ,'class'=>'yes' )); ?>&nbsp;Yes</span>
       
       <span><ol class="gen" start="7"><li>Do you have any other medical conditions that may make it dangerous for you to participate in physical activity/ exercise?</li></ol>
           <?php $radio_is_checked = ($fieldData[0]->option_7 === 'N')?"checked":"";
           echo form_radio(array("name"=>"options_7","id"=>"no","value"=>"N",'checked' =>'checked', 'class'=>'no')); ?>&nbsp;No&nbsp;&nbsp;
<?php $radio_is_checked = ($fieldData[0]->option_7 === 'Y')?"checked":"";
echo form_radio(array("name"=>"options_7","id"=>"yes","value"=>"Y","checked"=>$radio_is_checked ,'class'=>'yes' )); ?>&nbsp;Yes</span>
       
       <span><p>Notes:</p>
       <?php 
       $notes = $fieldData[0]->notes==''?'':$fieldData[0]->notes;
       $opts = 'placeholder="Notes"';
       echo form_textarea('notes',set_value('description', $notes),$opts); ?>
       </span>
      
      <span style="display:none;"><p class="new">What is your professional decision?</p>
          <?php  $radio_is_checked = ($fieldData[0]->option_8 === 'proceed')?True:"";
          echo form_radio(array("name"=>"option_8","id"=>"proceed","value"=>"proceed")); ?>&nbsp;Proceed&nbsp;&nbsp;
<?php $radio_is_checked = ($fieldData[0]->option_8 === 'guide')?"checked":"";
echo form_radio(array("name"=>"option_8","id"=>"guide","value"=>"guide","checked"=>$radio_is_checked )); ?>&nbsp;Refer for guidance</span>
      
                   
                    <?php echo form_close(); ?>
      </div><!--End right section--> 
   </div><!--End right --> 
   
   
</div><!--End contain -->
<!--
<div class="footer footer-top">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Mid --> 
</div><!--End Wrapper --> 

</body>
</html>
