<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
</head>

<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$health_logo" ;?>" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientSubmaximalInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?> 
	
    <div class="mid container">   
	
        <table class="graph_table submaximal_page" width="100%">
          <tbody>
            <tr>
              <td colspan="8" align="center" class="name_field_td position_r">
              	<a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>
                <label for="name">Name</label> <input type="text" name="name" id="name" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>">     
                <a href="#" class="info_icon_btn"><img src="<?php echo "$base/$image"?>/info_icon.png" style="float:right;"> </a>
                <div class="info_block">
                    <p>The test involves 3 x 3 minute stages on a bicycle ergometer. Collect workload at the end of each stage (Watts) as well as heart rate (BMP) and rating of perceived exertion (use Borg's 6-20 RPE scale). The RPE scale is shown below:
                       At the end of each 3-minute exercise period the client should rate their perception of exercise. That is, how heavy and strenuous the exercise feels to them. The perception of exertion depends on a range of input variables that are coming from
                       various sensory / pain receptions and assimilated in the brain. Much of the sensory input is based on the strain and fatigue in the muscles and on feelings of breathlessness, aches in the chest and perception of heaviness. The client should be 
                       shown the RPE rating scale and asked: "we want you to use this scale from 6 to 20, where 6 means 'no exertion at all' and 20 means 'maximal exertion'. Values can be entered as half-units if necessary [such as 11.5].
                       Try to summarise your feeling of exertion as honestly as possible, without thinking about what the actual physical load is. It's your own feeling of effort and exertion that's important. Look at the scale and the expressions and then give a number according to the RPE table below":
                    </p>     
                    <br/>
                    6 No exertion at all<br/>
                    7 Extremely light<br/>8<br/>
                    9 Very light &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Normal, healthy person walking slowly at own pace for some minutes<br/>
                    10<br/>
                    11 Light<br/>
                    12<br/>
                    13 Somewhat hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Somewhat hard but still feels OK to continue<br/>
                    14<br/>
                    15 Hard (heavy)<br/>
                    16<br/>
                    17 Very hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Very strenuous. A healthy person can still go on, but really has to push him/herself. Feels very heavy and the person is very tired<br/>
                    18<br/>
                    19 Extremely hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Extremely strenuous exercise level. For most people the most strenuous exercise they have ever experienced<br/>
                    20 Maximal exertion<br/><br/>
                    <p>It is suggested that you use the range 9-15 for most people unless they are reasonably well trained in which case you might consider the range 9-17.</p>
              </td>
              </tr>
              
              <tr>
              	<td width="320"><div class="predicted_head">Predicted VO<sub>2max</sub></div></td>
                <td style="visibility: hidden;" rowspan="2" align="right" valign="middle" width="230"><label>Male <input type="radio" name="gender" id="gender_Male" value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "checked";}?>></label> &nbsp; &nbsp; <label>Female <input type="radio" name="gender" id="gender_Female" value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "checked";}?>></label></td>
                <td width="10">&nbsp;</td>
                <td width="50">&nbsp;</td>
                <td align="center">Power [W]</td>
                <td align="center">Heart rate [BPM]</td>
                <td align="center">RPE</td>
                <td>&nbsp;</td>
              </tr>
              
              <tr>
              	<td>&nbsp;</td>
                <td>&nbsp;</td>
                
              <!-- Stage 1 Values -->  
                <td>Stage 1</td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="workload1" id="workload1" value="<?php echo isset($fieldData[0]->workload1)?$fieldData[0]->workload1:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="heart_rate1" id="heart_rate1" value="<?php echo isset($fieldData[0]->heart_rate1)?$fieldData[0]->heart_rate1:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="rpe1" id="rpe1" value="<?php echo isset($fieldData[0]->rpe1)?$fieldData[0]->rpe1:""; ?>"></td>
                <td>&nbsp;</td>
              </tr>
              
            <!-- Stage 2 Values --> 
              <tr>
              	<td>&nbsp;</td>
                <td class="sml_text_field" align="right">Age <input class="cus-input" type="text" name="age" id="age" value="<?php echo isset($fieldData[0]->age)?$fieldData[0]->age:$_SESSION['age']; ?>"> yr</td>
                <td>&nbsp;</td>
                
                <td>Stage 2</td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="workload2" id="workload2" value="<?php echo isset($fieldData[0]->workload2)?$fieldData[0]->workload2:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="heart_rate2" id="heart_rate2" value="<?php echo isset($fieldData[0]->heart_rate2)?$fieldData[0]->heart_rate2:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="rpe2" id="rpe2" value="<?php echo isset($fieldData[0]->rpe2)?$fieldData[0]->rpe2:""; ?>"></td>
                <td>&nbsp;</td>
              </tr>
              
            <!-- Stage 3 Values --> 
              <tr>                
              	<td>&nbsp;</td>                
                <td class="sml_text_field" align="right">Body weight <input class="cus-input" type="text" name="weight" id="weight" value="<?php echo isset($fieldData[0]->weight)?$fieldData[0]->weight:""; ?>"> kg</td>
                <td>&nbsp;</td>
                
                <td>Stage 3</td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="workload3" id="workload3" value="<?php echo isset($fieldData[0]->workload3)?$fieldData[0]->workload3:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="heart_rate3" id="heart_rate3" value="<?php echo isset($fieldData[0]->heart_rate3)?$fieldData[0]->heart_rate3:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="rpe3" id="rpe3" value="<?php echo isset($fieldData[0]->rpe3)?$fieldData[0]->rpe3:""; ?>"></td>
                <td>&nbsp;</td>
              </tr>              
              
               <tr id="stage_4" style="display: none;">
              	<td colspan="3">&nbsp;</td>       
                <td>Stage 4</td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="workload4" id="workload4" value="<?php echo isset($fieldData[0]->workload4)?$fieldData[0]->workload4:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="heart_rate4" id="heart_rate4" value="<?php echo isset($fieldData[0]->heart_rate4)?$fieldData[0]->heart_rate4:""; ?>"></td>
                <td class="sml_text_field" align="center"><input class="cus-input" type="text" name="rpe4" id="rpe4" value="<?php echo isset($fieldData[0]->rpe4)?$fieldData[0]->rpe4:""; ?>"></td>
                <td>&nbsp;</td>
              </tr>    
              
              
              <tr>
              	<td class="sml_text_field" align="center">
                    
                    <span style="margin-left:-55px;">Predicted values</span>
                    <div id="heatRateVal" class="predicted_value">Max HR &nbsp; &nbsp; &nbsp; &nbsp;<input type="text" name="max_hr" id="max_hr" readonly="readonly"> BPM</div>
                    <div id="rpeVal" style="display: none;" class="predicted_value">Max RPE &nbsp; &nbsp; &nbsp;<input type="text" name="max_rpe" id="max_rpe" readonly="readonly"></div>
                    <div class="predicted_value">Max Power <input type="text" name="max_wl" id="max_wl" readonly="readonly"> Watts</div>
                    <div class="predicted_value">VO<sub>2max </sub> &nbsp; &nbsp; &nbsp; <input type="text" name="vo_max" id="vo_max" readonly="readonly"> mL/kg/min</div>
                    
                    <div style="text-align: center; margin-top: 90px;">
                    <button id="plot" name="plot" onclick="show()" class="plot_btn">Plot</button>
                    <div style="margin: 10px 0; display: inline-block; width: 100%;">
                        <button id="add_stage" onclick="changeStage()" class="plot_btn submax_btn stg_btn">Add Stage</button>                    
                        <input type="hidden" name="stageFlag" id="stageFlag">
                    </div>
                    <button id="y_hr" onclick="regHeartRate()" class="plot_btn submax_btn">Y<sub>HR</sub></button>&nbsp;&nbsp;<button id="y_rpe" onclick="regRPE()" class="plot_btn submax_btn">Y<sub>RPE</sub></button>
                    </div>
                </td>
                
                <td colspan="5">
                 <!-- <div class="graph_submaximal"><img src="<?php echo "$base/$image"?>/graph.jpg" alt=""/></div>  -->
                    
                      <div class="graph_submaximal">                           
                          <span id="grf_label" style="display: none;"></span>
                        <div id="chartdiv" style="width: 450px; height: 340px; margin-left: 48px; margin-bottom:20px;"></div>
                      </div>                         
                </td>
                
                <td class="sml_text_field" align="center"><span id="maxVal">Known HRmax</span><div class="predicted_value"><input class="cus-input" type="text" name="known_max_hr" id="known_max_hr" onkeyUp="regHeartRate()"> <input class="cus-input" type="text" name="known_max_rpe" id="known_max_rpe" onkeyUp="regRPE()" style="display: none;"></div></td>
                <td><div class="predicted_value" id="known_max_hr_val" style="display: block;">BPM</div></td>
              </tr>
              
              
          </tbody>
        </table>
    </div>
</div>

<?php echo form_close(); ?>
<!-- Form ends -->	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	
   $(document).on('click','#plot', function(event){
	event.preventDefault();
   });
   
   $(document).on('click','#add_stage', function(event){
	event.preventDefault();
   });

   $(document).on('click','#y_hr', function(event){
	event.preventDefault();
   });

   $(document).on('click','#y_rpe', function(event){
	event.preventDefault();
   });   

   $(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
   });

	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.trendline.min.js"></script>
<link rel="stylesheet" type="text/css" src=<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
   
   function changeStage()
   {  
       if(document.getElementById("stageFlag").value == "" ){
          
         document.getElementById("add_stage").innerHTML = "Remove Stage";
         document.getElementById("stage_4").setAttribute('style', ""); 
         document.getElementById("stageFlag").value = "1" ;
       }
       else{            
            document.getElementById("workload4").value = "" ;  
            document.getElementById("heart_rate4").value = "" ;
            document.getElementById("rpe4").value = "" ;       
            document.getElementById("stage_4").setAttribute('style', "display:none");     
            document.getElementById("add_stage").innerHTML = "Add Stage";
            document.getElementById("stageFlag").value = "" ;
         }
   }
     
   function regHeartRate() 
    {
        var heartRateDiv =  document.getElementById("heatRateVal") ;
        var rpeDiv =  document.getElementById("rpeVal") ;
        
        rpeDiv.style.display = "none" ; 
        heartRateDiv.style.display = "block" ;
        
        document.getElementById("grf_label").style.display = "block" ;
        document.getElementById("grf_label").setAttribute('class', "");
        document.getElementById("grf_label").setAttribute('class', "grf_label2");            
        document.getElementById("grf_label").innerHTML = "Heart rate [BPM]";
        
        
        document.getElementById("known_max_rpe").style.display = "none" ;    
        document.getElementById("known_max_hr").style.display = "block" ;                  
        document.getElementById("maxVal").innerHTML = "Known HRmax";
        document.getElementById("known_max_hr_val").style.display = "block" ; 
        
        
        
        var chartDiv=document.getElementById('chartdiv');
        document.getElementById("max_wl").value = "" ;
        document.getElementById("vo_max").value = "" ;
        
        if(chartDiv.innerHTML !== "")
        {
             while (chartDiv.hasChildNodes()) {
            chartDiv.removeChild(chartDiv.firstChild);
            }         
        }
        
        var ageVal = document.getElementById("age").value ;
        var weightVal = document.getElementById("weight").value ;
        
        var x1 = document.getElementById("workload1").value ;
        var x2 = document.getElementById("workload2").value ;
        var x3 = document.getElementById("workload3").value ;
        var x4 = document.getElementById("workload4").value ;
              
        var y1 = document.getElementById("heart_rate1").value ;
        var y2 = document.getElementById("heart_rate2").value ;
        var y3 = document.getElementById("heart_rate3").value ;        
        var y4 = document.getElementById("heart_rate4").value ;        
               
        document.getElementById("max_hr").value = "" ;
        
        if(ageVal !== "")
        {
           var hrMax = (208 - (0.7 * ageVal)) ;           
           document.getElementById("max_hr").value = parseFloat(hrMax) ;
        }
        
        
    if(document.getElementById("workload4").value == "" && document.getElementById("heart_rate4").value == ""){  
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 ; // sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ; // sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) ; // sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (3 * sumxy - (sumx * sumy)) / (3 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 3  ; 
       
        if(document.getElementById("known_max_hr").value == "")
        {
          var y = document.getElementById("max_hr").value ;  
        }
        else
        {
          var y = document.getElementById("known_max_hr").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
        
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
            
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
        }
      
        var max_x = Math.floor(x);  
        var reg_x = x3 ;
        var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)]] ;
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(0)], [parseFloat(max_x),parseFloat(y)]] ;
        
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {         
          label: 'Power [W]',
          ticks : ['50', '100', '150', '200', '250', '300', '350', '400'],
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
        yaxis: {              
                ticks:['0', '20', '40', '60', '80', '100', '120', '140', '160', '180', '200'],               
              // label: 'Heart Rate',
              // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
              // tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                   }               
              }
         },
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }      
        
     
      }); 
    }
   
     else{  
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        var x4y4 = parseFloat(x4) * parseFloat(y4) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 + x4y4 ; // sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ; // sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) + parseFloat(y4) ; // sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) + (parseFloat(x3) * parseFloat(x3)) + (parseFloat(x4) * parseFloat(x4)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (4 * sumxy - (sumx * sumy)) / (4 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 4  ; 
             
        if(document.getElementById("known_max_hr").value == "")
        {
          var y = document.getElementById("max_hr").value ;  
        }
        else
        {
          var y = document.getElementById("known_max_hr").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
        
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
        }
      
      var max_x = Math.floor(x);      
      var reg_x = x4 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)], [parseFloat(x4), parseFloat(y4)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;     
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(0)], [parseFloat(max_x),parseFloat(y)]] ;
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {         
          label: 'Power [W]',
          ticks : ['50', '100', '150', '200', '250', '300', '350', '400'],
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
        yaxis: {              
                ticks:['0', '20', '40', '60', '80', '100', '120', '140', '160', '180', '200'],               
              // label: 'Heart Rate',
              // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
              // tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                   }               
              }
         }, 
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }           
        
     
      }); 
    }
   
  } 
    
    function regRPE() 
    {
        var heartRateDiv =  document.getElementById("heatRateVal") ;
        var rpeDiv =  document.getElementById("rpeVal") ;
        
        heartRateDiv.style.display = "none" ; 
        rpeDiv.style.display = "block" ;
        
        document.getElementById("grf_label").style.display = "block" ;
        document.getElementById("grf_label").setAttribute('class', "");
        document.getElementById("grf_label").setAttribute('class', "grf_label1");
        document.getElementById("grf_label").innerHTML = "RPE" ;
        
        
        document.getElementById("known_max_hr").style.display = "none" ;    
        document.getElementById("known_max_rpe").style.display = "block" ;                  
        document.getElementById("maxVal").innerHTML = "Known Max RPE";
        document.getElementById("known_max_hr_val").style.display = "none" ; 
        
        
       var chartDiv=document.getElementById('chartdiv');
        document.getElementById("max_wl").value = "" ;
        document.getElementById("vo_max").value = "" ;
        
        if(chartDiv.innerHTML !== "")
        {         
          while (chartDiv.hasChildNodes()) {
            chartDiv.removeChild(chartDiv.firstChild);
            }
            //chartDiv.innerHTML = "" ;
        }
        
        var ageVal = document.getElementById("age").value ;
        var weightVal = document.getElementById("weight").value ;
        
        var x1 = document.getElementById("workload1").value ;
        var x2 = document.getElementById("workload2").value ;
        var x3 = document.getElementById("workload3").value ;
        var x4 = document.getElementById("workload4").value ;
              
        var y1 = document.getElementById("rpe1").value ;
        var y2 = document.getElementById("rpe2").value ;
        var y3 = document.getElementById("rpe3").value ;
        var y4 = document.getElementById("rpe4").value ;
     
        if(document.getElementById("max_rpe").value == "")
        {
            document.getElementById("max_rpe").value = "20" ;
        } 
              
     if(document.getElementById("workload4").value == "" && document.getElementById("rpe4").value == "")
     {    
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 ; //sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ;     //sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) ;     //sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (3 * sumxy - (sumx * sumy)) / (3 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 3  ; 
     
       // alert(slope); die;    
        
        if(document.getElementById("known_max_rpe").value == "")
        {
          var y = document.getElementById("max_rpe").value ;
        }
        else
        {
          var y = document.getElementById("known_max_rpe").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
        
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;            
        }
        else
        {
           document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
        }
     
      var max_x = Math.floor(x); 
      var reg_x = x3 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(6)], [parseFloat(max_x),parseFloat(y)]] ;
    
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {            
          ticks : ['50', '100', '150', '200', '250', '300', '350', '400'],
          label: 'Power [W]',          
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],               
               // label: 'RPE',               
               // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
                //tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }                  
     
      }); 
     }
  
     else
     {    
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        var x4y4 = parseFloat(x4) * parseFloat(y4) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 + x4y4 ; //sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ;     //sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) + parseFloat(y4) ;     //sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) + (parseFloat(x3) * parseFloat(x3)) + (parseFloat(x4) * parseFloat(x4)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (4 * sumxy - (sumx * sumy)) / (4 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 4  ; 
        
       if(document.getElementById("known_max_rpe").value == "")
        {
          var y = document.getElementById("max_rpe").value ;
        }
        else
        {
          var y = document.getElementById("known_max_rpe").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
        
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
            
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
        }
     
      var max_x = Math.floor(x);       
      var reg_x = x4 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;   
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)], [parseFloat(x4), parseFloat(y4)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(6)], [parseFloat(max_x),parseFloat(y)]] ;
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {            
          ticks : ['50', '100', '150', '200', '250', '300', '350', '400'],
          label: 'Power [W]',          
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],               
               // label: 'RPE',               
               // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
                //tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }                  
     
      }); 
     }      
    }
    
    function show()
    {         
       // Heart Values  
        if(document.getElementById("heart_rate1").value !== "" && document.getElementById("heart_rate2").value !== "" && document.getElementById("heart_rate3").value !== "")
        {
          regHeartRate() ;
        }
        
        
       // RPE Values  
        else if(document.getElementById("rpe1").value !== "" && document.getElementById("rpe2").value !== "" && document.getElementById("rpe3").value !== "")
        {
          regRPE() ;
        }
        
        else
        {
            alert ("No values have been entered");
            return false;
        }
        
    }
    
</script>
</body>
</html>
