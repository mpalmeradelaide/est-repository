<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>overlay.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
.left_block{float:left; width:40%; margin-top:70px;}
.left_block ul{margin:0; padding:0; list-style:none;}
.left_block ul li{text-align:right; width:100%; display: inline-block; margin-bottom: 13px; line-height: 90px; font-size: 16px; color:#f00;} 
.left_block ul li .value_box{float:right; height:90px; width:90px; background:#f00; text-align:center; line-height:90px; color:#fff; margin-left:15px; font-size: 22px; -moz-box-shadow: inset 3px 3px 3px rgba(255, 255, 255, .4), inset -3px -3px 3px rgba(0, 0, 0, .4); -webkit-box-shadow: inset 3px 3px 3px rgba(255, 255, 255, .4), inset -3px -3px 3px rgba(0, 0, 0, .4); box-shadow: inset 3px 3px 3px rgba(255, 255, 255, .4), inset -3px -3px 3px rgba(0, 0, 0, .4); border: 1px solid rgba(0, 0, 0, 0.1);} 
.left_block ul li:nth-child(2){color:#ed8222;}
.left_block ul li:nth-child(2) .value_box{background:#ed8222;}
.left_block ul li:nth-child(3){color:#7dbb36;}
.left_block ul li:nth-child(3) .value_box{background:#7dbb36;}
.left_block ul li:nth-child(4){color:#37a63d;}
.left_block ul li:nth-child(4) .value_box{background:#37a63d;}
.right_block{margin-top:40px; margin-bottom:20px; float: right; width: 60%;} 
.right_block img{width:180px; float:left;}
.right_content{float: left; border-left: 2px solid #000; margin-left: 40px; height: 320px; margin-top: 74px; position: relative;}
.right_content:before{content: ''; height: 2px; width: 30px; position: absolute; left: 0; top: 50%; background: #000;}
.right_content .total_pred{margin: 130px 0 0 50px; line-height: 26px; font-size: 17px;}
.right_content .total_pred strong{font-size: 20px;}
.right_content .body_txt{margin: 50px 0 0 20px; text-align: left; font-size: 17px; line-height: 30px;}
</style>
</head> 
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Body/skinfold_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?> 

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                <a onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';" class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a> 
                <h1 class="page_head">FRACTIONATION OF BODY MASS</h1>
               <h1 class="page_head"><span><?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</span><br/><span id="somato"></span></h1>              
              </td>
            </tr>
            <tr>
              <td align="center">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="cus-input" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
              </td> 
			  
              <td align="center">
                <label>Gender</label>
                <input type="text" id="gender" name="gender" class="cus-input" value="<?php if($_SESSION['user_gender'] == 'M'){ echo "Male" ;}else{ echo "Female" ;} ;?>"> 
              </td>             
              
              <td align="center">
                <label>Actual body mass</label>
                <input type="text" id="actual_mass" name="actual_mass" class="cus-input width_108" value="<?php echo isset($fractionation_Values["body_mass"])?$fractionation_Values["body_mass"]:""; ?>"> kg
              </td>        
            </tr>
            <tr>                
                <td colspan="3" align="center">  
                    <div class="population_div_main">                             
                        <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>"> 
                        <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                        <input type="hidden" id="height" name="height" value="<?php echo isset($fractionation_Values["height"])?$fractionation_Values["height"]:""; ?>"> 
                        <input type="hidden" id="body_mass" name="body_mass" value="<?php echo isset($fractionation_Values["body_mass"])?$fractionation_Values["body_mass"]:""; ?>"> 
                        <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>">                          
                    </div>                      
                </td>
            </tr>
            
            <tr>
            	<td colspan="3" align="center">
                	<!--<h2>ANTHROPOMETRIC FRACTIONATION OF BODY MASS</h2>-->
                	<div class="left_block">
                    	<ul>
                            <li>FAT mass [kg] <div class="value_box" id="fat_mass"></div></li>
                            <li>MUSCLE mass [kg] <div class="value_box"  id="muscle_mass"></div></li>
                            <li>BONE mass [kg] <div class="value_box"  id="bone_mass"></div></li>
                            <li>RESIDUAL mass [kg] <div class="value_box"  id="residual_mass"></div></li>
                        </ul>
                    </div>
                    <div class="right_block">
                    	<div class="right_content">
                        	<div class="total_pred">
                            	<p>TOTAL predicted</p>
                                <strong><span id="total_mass"></span> kg</strong>
                            </div>
                            <div class="body_txt">
                            	<p>Lean body mass = <span id="lean_mass"></span> kg</p>
                                <p>Z Lean body mass = <span id="z_lean_mass"></span></p>
                                <p>% body fat = <span id="body_fat"></span>%</p>
                                <p>Z % body fat = <span id="z_body_fat"></span></p>
                            </div>                           
                        </div>
                    	<img src="<?php echo "$base/$image"?>/human_skelton.png" alt="">
                    </div>
                </td>
            </tr>
            
            <tr>                        			  
              <td align="right" colspan="3" valign="bottom">      
              	 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>

             <input type="hidden" id="client_x" name="client_x" value="">
             <input type="hidden" id="client_y" name="client_y" value="">
             
             <input type="hidden" id="selectedSports_x" name="selectedSports_x">
             <input type="hidden" id="selectedSports_y" name="selectedSports_y">
             
             <input type="hidden" id="triceps" name="triceps" value="<?php echo isset($fractionation_Values["triceps"])?$fractionation_Values["triceps"]:""; ?>">              
             <input type="hidden" id="subscapular" name="subscapular" value="<?php echo isset($fractionation_Values["subscapular"])?$fractionation_Values["subscapular"]:""; ?>">              
             <input type="hidden" id="thigh" name="thigh" value="<?php echo isset($fractionation_Values["thigh"])?$fractionation_Values["thigh"]:""; ?>"> 
             <input type="hidden" id="calf" name="calf" value="<?php echo isset($fractionation_Values["calf"])?$fractionation_Values["calf"]:""; ?>"> 
             <input type="hidden" id="relArmG" name="relArmG" value="<?php echo isset($fractionation_Values["relArmG"])?$fractionation_Values["relArmG"]:""; ?>"> 
             <input type="hidden" id="chestG" name="chestG" value="<?php echo isset($fractionation_Values["chestG"])?$fractionation_Values["chestG"]:""; ?>"> 
             <input type="hidden" id="calfG" name="calfG" value="<?php echo isset($fractionation_Values["calfG"])?$fractionation_Values["calfG"]:""; ?>"> 
             <input type="hidden" id="thighG" name="thighG" value="<?php echo isset($fractionation_Values["thighG"])?$fractionation_Values["thighG"]:""; ?>">
             
             <input type="hidden" id="zscore_Triceps" name="zscore_Triceps" value="<?php echo isset($fractionation_Values["zscore_Triceps"])?$fractionation_Values["zscore_Triceps"]:""; ?>">
             <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular" value="<?php echo isset($fractionation_Values["zscore_Subscapular"])?$fractionation_Values["zscore_Subscapular"]:""; ?>">
             <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale" value="<?php echo isset($fractionation_Values["zscore_Supspinale"])?$fractionation_Values["zscore_Supspinale"]:""; ?>">
             <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_Thigh" name="zscore_Thigh" value="<?php echo isset($fractionation_Values["zscore_Thigh"])?$fractionation_Values["zscore_Thigh"]:""; ?>">
             <input type="hidden" id="zscore_Calf" name="zscore_Calf" value="<?php echo isset($fractionation_Values["zscore_Calf"])?$fractionation_Values["zscore_Calf"]:""; ?>">
             <input type="hidden" id="zscore_ForearmG" name="zscore_ForearmG" value="<?php echo isset($fractionation_Values["zscore_ForearmG"])?$fractionation_Values["zscore_ForearmG"]:""; ?>">
             <input type="hidden" id="zscore_WristG" name="zscore_WristG" value="<?php echo isset($fractionation_Values["zscore_WristG"])?$fractionation_Values["zscore_WristG"]:""; ?>">
             <input type="hidden" id="zscore_AnkleG" name="zscore_AnkleG" value="<?php echo isset($fractionation_Values["zscore_AnkleG"])?$fractionation_Values["zscore_AnkleG"]:""; ?>">
             <input type="hidden" id="zscore_Humerus" name="zscore_Humerus" value="<?php echo isset($fractionation_Values["zscore_Humerus"])?$fractionation_Values["zscore_Humerus"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_Femur" name="zscore_Femur" value="<?php echo isset($fractionation_Values["zscore_Femur"])?$fractionation_Values["zscore_Femur"]:""; ?>">
             <input type="hidden" id="zscore_Biac" name="zscore_Biac" value="<?php echo isset($fractionation_Values["zscore_Biac"])?$fractionation_Values["zscore_Biac"]:""; ?>">
             <input type="hidden" id="zscore_Billio" name="zscore_Billio" value="<?php echo isset($fractionation_Values["zscore_Billio"])?$fractionation_Values["zscore_Billio"]:""; ?>">
             <input type="hidden" id="zscore_TrChest" name="zscore_TrChest" value="<?php echo isset($fractionation_Values["zscore_TrChest"])?$fractionation_Values["zscore_TrChest"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>"><input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal" value="<?php echo isset($fractionation_Values["zscore_Abdominal"])?$fractionation_Values["zscore_Abdominal"]:""; ?>">
             <input type="hidden" id="zscore_ApChest" name="zscore_ApChest" value="<?php echo isset($fractionation_Values["zscore_ApChest"])?$fractionation_Values["zscore_ApChest"]:""; ?>">             
             
                                     
             <input type="hidden" id="exit_key" name="exit_key" value="">
             <input type="hidden" id="action_key" name="action_key" value="">
             
	<!-- <?php echo form_close(); ?>-->
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
	
	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
    }); 
	
	$(document).ready(function() {	  
		$(".overlay").overlay();
	});
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>overlay.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<link rel="stylesheet" type="text/css" src=<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
 
   $(window).bind("load", function() {
      
    var age = document.getElementById("age").value ; 
    var ht = document.getElementById("height").value ;
    var body_mass = document.getElementById("body_mass").value ;
    
    var triceps = document.getElementById("triceps").value ;            
    var subscapular = document.getElementById("subscapular").value ;
    var thigh = document.getElementById("thigh").value ;  
    var calf = document.getElementById("calf").value ;  
    var relArmG = document.getElementById("relArmG").value ;            
    var chestG = document.getElementById("chestG").value ;    
    var calfG = document.getElementById("calfG").value ; 
    var thighG = document.getElementById("thighG").value ; 
    
    var zscore_Triceps = document.getElementById("zscore_Triceps").value ;            
    var zscore_Subscapular = document.getElementById("zscore_Subscapular").value ;   
    var zscore_Supspinale = document.getElementById("zscore_Supspinale").value ; 
    var zscore_Abdominal = document.getElementById("zscore_Abdominal").value ;  
    var zscore_Thigh = document.getElementById("zscore_Thigh").value ;  
    var zscore_Calf = document.getElementById("zscore_Calf").value ;
    var zscore_ForearmG = document.getElementById("zscore_ForearmG").value ;   
    var zscore_WristG = document.getElementById("zscore_WristG").value ;    
    var zscore_AnkleG = document.getElementById("zscore_AnkleG").value ; 
    var zscore_Humerus = document.getElementById("zscore_Humerus").value ; 
    var zscore_Femur = document.getElementById("zscore_Femur").value ; 
    var zscore_Biac = document.getElementById("zscore_Biac").value ; 
    var zscore_Billio = document.getElementById("zscore_Billio").value ; 
    var zscore_TrChest = document.getElementById("zscore_TrChest").value ; 
    var zscore_ApChest = document.getElementById("zscore_ApChest").value ; 
    
    var pi = 3.1415962 ;    
     
    var meanFatZ = (parseFloat(zscore_Calf) + parseFloat(zscore_Thigh) + parseFloat(zscore_Abdominal) + parseFloat(zscore_Triceps) + parseFloat(zscore_Subscapular) + parseFloat(zscore_Supspinale))/6 ; 
    var fat_mass = (meanFatZ * 3.25 + 12.13)/[(170.18/ht) * (170.18/ht) * (170.18/ht)];    
    document.getElementById("fat_mass").innerHTML = Math.floor(parseFloat(fat_mass) * 10)/10 ; // Fat Mass
    
    var ZCorrChestG = ((parseFloat(chestG) - parseFloat(subscapular) * pi / 10) * 170.18 / ht - 82.46) / 4.86 ; 
    var ZCorrRelArmG = ((parseFloat(relArmG) - parseFloat(triceps) * pi / 10) * 170.18 / ht - 22.05) / 1.91 ; 
    var ZCorrThighG = ((parseFloat(thighG) - parseFloat(thigh) * pi / 10) * 170.18 / ht - 47.34) / 3.59 ; 
    var ZCorrCalfG = ((parseFloat(calfG) - parseFloat(calf) * pi / 10) * 170.18 / ht - 30.22) / 1.97 ; 
    var meanLeanZ = (ZCorrCalfG + ZCorrThighG + ZCorrRelArmG + ZCorrChestG + zscore_ForearmG) / 5 ; 
    var muscle_mass = (meanLeanZ * 2.99 + 25.55)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("muscle_mass").innerHTML = Math.floor(parseFloat(muscle_mass) * 10)/10 ; // Muscle Mass
        
    var meanSkeletalZ = (parseFloat(zscore_Femur) + parseFloat(zscore_Humerus) + parseFloat(zscore_AnkleG) + parseFloat(zscore_WristG)) / 4 ; 
    var bone_mass = (meanSkeletalZ * 1.57 + 10.49)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("bone_mass").innerHTML = Math.floor(parseFloat(bone_mass) * 10)/10 ; // Bone Mass
    
    var MeanResidualZ = (parseFloat(zscore_Biac) + parseFloat(zscore_TrChest) + parseFloat(zscore_Billio) + parseFloat(zscore_ApChest)) / 4 ; 
    var residual_mass = (MeanResidualZ * 1.9 + 16.41)/[(170.18/ht) * (170.18/ht) * (170.18/ht)] ;    
    document.getElementById("residual_mass").innerHTML = Math.floor(parseFloat(residual_mass) * 10)/10 ; // Residual Mass
    
    var total_mass = fat_mass + muscle_mass + bone_mass + residual_mass ;
    document.getElementById("total_mass").innerHTML = Math.floor(parseFloat(total_mass) * 10)/10 ; // Total predicted mass
    
    var lean_mass = muscle_mass + bone_mass + residual_mass ; 
    document.getElementById("lean_mass").innerHTML = Math.floor(parseFloat(lean_mass) * 10)/10 ; // Lean body mass
    
    var ZLean_mass = (lean_mass * ((170.18/ht) * (170.18/ht) * (170.18/ht)) - 52.45) / 6.14 ;
    document.getElementById("z_lean_mass").innerHTML = Math.floor(parseFloat(ZLean_mass) * 10)/10 ; // Z Lean body mass
        
    var body_fat = 100 * fat_mass / (residual_mass + bone_mass + muscle_mass + fat_mass) ; 
    document.getElementById("body_fat").innerHTML = Math.floor(parseFloat(body_fat) * 10)/10 ;  // % body fat
    
    var ZBody_fat = (body_fat * ((170.18/ht) * (170.18/ht) * (170.18/ht)) - 18.78) / 5.2 ; 
    document.getElementById("z_body_fat").innerHTML = Math.floor(parseFloat(ZBody_fat) * 10)/10 ;  // Z % body fat
    
}); 

</script>
</body>
</html>
