<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

.virtual {
 cursor: pointer;
 height: 36px;
 width: 120px;
 background: #065dcc;
 border: 0;
 color: #fff;
 border-radius: 2px;
 border: 2px solid #0253bb;
 box-shadow: 1px 2px 5px rgba(0,0,0,0.5);
 font-size: 14px;   
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/full_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?> 

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
				<a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>                 
                <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>
                <h1 class="page_head">FULL profile</h1>   
              </td>
              </tr>
            <tr>
              <td align="center">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="cus-input" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
              </td> 
			  
              <td align="center">
                <label>Gender</label>
                <input type="text" id="gender" name="gender" class="cus-input" value="<?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>"> 
              </td>             
              
              <td align="center">
                <label>Age (yr)</label>
                <input type="text" id="age" name="age" class="cus-input" value="<?php if(!empty($fieldData[0]->age)){echo $fieldData[0]->age;}else{echo $_SESSION['age'] ;}?>"> 
              </td>        
            </tr>
            
            <tr>
              <td colspan="3"> 
                <table>
                    <tr>
                      <td align="center" class="sml_text_field" width="336px">
                        <label>Height</label>
                        <input type="text" id="height" name="height" class="cus-input" value="<?php echo isset($fieldData[0]->height)?$fieldData[0]->height:""; ?>" style="width:180px;"> 
                        cm
                      </td>
                      
                      <td align="center" class="sml_text_field" width="324px">
                        <label>Body mass</label>
                        <input type="text" id="body_mass" name="body_mass" class="cus-input" value="<?php echo isset($fieldData[0]->body_mass)?$fieldData[0]->body_mass:""; ?>"> 
                        kg
                      </td> 
                      
                      <td align="center" class="sml_text_field" width="240px">
                        <button id="virtual_profile" name="virtual_profile" class="virtual">Virtual Profile</button>
                        <button id="clear_profile" name="clear_profile" class="virtual"  style="display:none;">Clear</button>
                      </td>
                      
                    </tr>                    
                </table>
              </td>                             
            </tr>
            
            <tr>                
                <td colspan="3" align="center"> 
                	<table width="100%">
                      <tbody> 
                        <tr> 
                          <td valign="top">
                          		<table width="100%" class="tabel_bord" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>Skinfolds</td>
                                      <td>mm</td>
                                    </tr>
                                    </thead>
                                    <tbody>                                    
                                    <tr>
                                      <td>Triceps</td>
                                      <td><input type="text" id="triceps" name="triceps" class="cus-input width_76" value="<?php echo isset($fieldData[0]->triceps)?$fieldData[0]->triceps:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Subscapular</td>
                                      <td><input type="text" id="subscapular" name="subscapular" class="cus-input width_76" value="<?php echo isset($fieldData[0]->subscapular)?$fieldData[0]->subscapular:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Biceps</td>
                                      <td><input type="text" id="biceps" name="biceps" class="cus-input width_76" value="<?php echo isset($fieldData[0]->biceps)?$fieldData[0]->biceps:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Iliac crest</td>
                                      <td><input type="text" id="iliac_crest" name="iliac_crest" class="cus-input width_76" value="<?php echo isset($fieldData[0]->iliac_crest)?$fieldData[0]->iliac_crest:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Supraspinale</td>
                                      <td><input type="text" id="supraspinale" name="supraspinale" class="cus-input width_76" value="<?php echo isset($fieldData[0]->supraspinale)?$fieldData[0]->supraspinale:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Abdominal</td>
                                      <td><input type="text" id="abdominal" name="abdominal" class="cus-input width_76" value="<?php echo isset($fieldData[0]->abdominal)?$fieldData[0]->abdominal:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Front thigh</td>
                                      <td><input type="text" id="thigh" name="thigh" class="cus-input width_76" value="<?php echo isset($fieldData[0]->thigh)?$fieldData[0]->thigh:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Medial calf</td>
                                      <td><input type="text" id="calf" name="calf" class="cus-input width_76" value="<?php echo isset($fieldData[0]->calf)?$fieldData[0]->calf:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Mid-axilla</td>
                                      <td><input type="text" id="mid_axilla" name="mid_axilla" class="cus-input width_76" value="<?php echo isset($fieldData[0]->mid_axilla)?$fieldData[0]->mid_axilla:""; ?>"> </td>
                                    </tr>                                   
                                  </tbody>
                            </table>
                          </td>
                          <td valign="top">
                          	<table width="100%" class="tabel_bord" cellspacing="0">
                                    <thead>                                  
                                    <tr>
                                      <td>Girths</td>
                                      <td>cm</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td>Head</td>                                      
                                      <td><input type="text" id="headG" name="headG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->headG)?$fieldData[0]->headG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Neck</td>
                                      <td><input type="text" id="neckG" name="neckG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->neckG)?$fieldData[0]->neckG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Arm (relaxed)</td>
                                      <td><input type="text" id="relArmG" name="relArmG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->relArmG)?$fieldData[0]->relArmG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Arm (flexed)</td>
                                      <td><input type="text" id="flexArmG" name="flexArmG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->flexArmG)?$fieldData[0]->flexArmG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Forearm (maximum)</td>
                                      <td><input type="text" id="forearmG" name="forearmG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->forearmG)?$fieldData[0]->forearmG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Wrist (distal styloids)</td>
                                      <td><input type="text" id="wristG" name="wristG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->wristG)?$fieldData[0]->wristG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Chest (mesosternale)</td>
                                      <td><input type="text" id="chestG" name="chestG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->chestG)?$fieldData[0]->chestG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Waist (minimum)</td>
                                      <td><input type="text" id="waistG" name="waistG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->waistG)?$fieldData[0]->waistG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Gluteal (hips)</td>
                                      <td><input type="text" id="hipG" name="hipG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->hipG)?$fieldData[0]->hipG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Thigh (1 cm distal)</td>
                                      <td><input type="text" id="thighG" name="thighG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->thighG)?$fieldData[0]->thighG:""; ?>""> </td>
                                    </tr>
                                    <tr>
                                      <td>Thigh (mid)</td>
                                      <td><input type="text" id="midThighG" name="midThighG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->midThighG)?$fieldData[0]->midThighG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Calf (maximum)</td>
                                      <td><input type="text" id="calfG" name="calfG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->calfG)?$fieldData[0]->calfG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Ankle (minimum)</td>
                                      <td><input type="text" id="ankleG" name="ankleG" class="cus-input width_76" value="<?php echo isset($fieldData[0]->ankleG)?$fieldData[0]->ankleG:""; ?>"> </td>
                                    </tr>
                                  </tbody>
                                </table>

                          </td>
                          <td valign="top">
                          	<table width="100%" class="tabel_bord" cellspacing="0">
                                    <thead>
                                    <tr>
                                      <td>Lengths</td>
                                      <td>cm</td>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                      <td>Acromiale-radiale</td>
                                      <td><input type="text" id="acRad" name="acRad" class="cus-input width_76" value="<?php echo isset($fieldData[0]->acRad)?$fieldData[0]->acRad:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Radiale-styl</td>
                                      <td><input type="text" id="radStyl" name="radStyl" class="cus-input width_76" value="<?php echo isset($fieldData[0]->radStyl)?$fieldData[0]->radStyl:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Midstyl-dacty</td>
                                      <td><input type="text" id="midStylDact" name="midStylDact" class="cus-input width_76" value="<?php echo isset($fieldData[0]->midStylDact)?$fieldData[0]->midStylDact:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Iliospinale</td>
                                      <td><input type="text" id="iliospinale" name="iliospinale" class="cus-input width_76" value="<?php echo isset($fieldData[0]->iliospinale)?$fieldData[0]->iliospinale:""; ?>"> </td>
                                    </tr><tr>
                                      <td>Trochanterion</td>
                                      <td><input type="text" id="troch" name="troch" class="cus-input width_76" value="<?php echo isset($fieldData[0]->troch)?$fieldData[0]->troch:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Trochant-tibiale</td>
                                      <td><input type="text" id="trochTib" name="trochTib" class="cus-input width_76" value="<?php echo isset($fieldData[0]->trochTib)?$fieldData[0]->trochTib:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Tibiale laterale</td>
                                      <td><input type="text" id="tibLat" name="tibLat" class="cus-input width_76" value="<?php echo isset($fieldData[0]->tibLat)?$fieldData[0]->tibLat:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Tib med-sphy tib</td>
                                      <td><input type="text" id="tibMed" name="tibMed" class="cus-input width_76" value="<?php echo isset($fieldData[0]->tibMed)?$fieldData[0]->tibMed:""; ?>"> </td>
                                    </tr>
                                  </tbody>
                                </table>
                          </td>
                          <td valign="top">
                          		<table width="100%" class="tabel_bord" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>Breadths</td>
                                      <td>cm</td>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                      <td>Biacromial</td>
                                      <td><input type="text" id="biac" name="biac" class="cus-input width_76" value="<?php echo isset($fieldData[0]->biac)?$fieldData[0]->biac:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Bideltoid</td>
                                      <td><input type="text" id="bideltoid" name="bideltoid" class="cus-input width_76" value="<?php echo isset($fieldData[0]->bideltoid)?$fieldData[0]->bideltoid:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Biiliocristal</td>
                                      <td><input type="text" id="billio" name="billio" class="cus-input width_76" value="<?php echo isset($fieldData[0]->billio)?$fieldData[0]->billio:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Bitrochanteric</td>
                                      <td><input type="text" id="bitrochanteric" name="bitrochanteric" class="cus-input width_76" value="<?php echo isset($fieldData[0]->bitrochanteric)?$fieldData[0]->bitrochanteric:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Foot length</td>
                                      <td><input type="text" id="foot" name="foot" class="cus-input width_76" value="<?php echo isset($fieldData[0]->foot)?$fieldData[0]->foot:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Sitting height</td>
                                      <td><input type="text" id="sitting" name="sitting" class="cus-input width_76" value="<?php echo isset($fieldData[0]->sitting)?$fieldData[0]->sitting:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Transverse chest</td>
                                      <td><input type="text" id="trChest" name="trChest" class="cus-input width_76" value="<?php echo isset($fieldData[0]->trChest)?$fieldData[0]->trChest:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>A-P chest</td>
                                      <td><input type="text" id="apChest" name="apChest" class="cus-input width_76" value="<?php echo isset($fieldData[0]->apChest)?$fieldData[0]->apChest:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Arm span</td>
                                      <td><input type="text" id="armSpan" name="armSpan" class="cus-input width_76" value="<?php echo isset($fieldData[0]->armSpan)?$fieldData[0]->armSpan:""; ?>"> </td>
                                    </tr>                                    
                                    <tr>
                                      <td>Humerus (bi-epi)</td>
                                      <td><input type="text" id="humerus" name="humerus" class="cus-input width_76" value="<?php echo isset($fieldData[0]->humerus)?$fieldData[0]->humerus:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Femur (bi-epi)</td>
                                      <td><input type="text" id="femur" name="femur" class="cus-input width_76" value="<?php echo isset($fieldData[0]->femur)?$fieldData[0]->femur:""; ?>"> </td>
                                    </tr>
                                  </tbody>
                                </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
             <td align="left">
              	<!--<a href="#" class="rest_bot_btns"><span>&nbsp;</span> imperial</a> 
                <a href="#" class="rest_bot_btns"><span><img src="<?php echo "$base/$image"?>/cross.png"></span> metric</a> -->
              </td>
              
              <td align="right" colspan="2">              	 
		<div class="bottom_icons" id="phantom" name="phantom" onclick="getPhantomZscore()"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" id="body_fat" name="body_fat" onclick="getBodyFat()"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" id="skinfold" name="skinfold" onclick="getSkinfolds()"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" id="somatotype" name="somatotype" onclick="getSomatotype()"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" id="anthropometry" name="anthropometry" onclick="getAnthropometry()"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                 <div class="bottom_icons" id="fractionation" name="fractionation" onclick="getFractionation()"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
        
            <input type="hidden" id="zscore_Triceps" name="zscore_Triceps">
            <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular">
            <input type="hidden" id="zscore_Biceps" name="zscore_Biceps">
            <input type="hidden" id="zscore_Iliac" name="zscore_Iliac">
            <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale">
            <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal">
            <input type="hidden" id="zscore_Thigh" name="zscore_Thigh">
            <input type="hidden" id="zscore_Calf" name="zscore_Calf">
            
            <input type="hidden" id="zscore_HeadG" name="zscore_HeadG">
            <input type="hidden" id="zscore_NeckG" name="zscore_NeckG">
            <input type="hidden" id="zscore_RelArmG" name="zscore_RelArmG">
            <input type="hidden" id="zscore_FlexArmG" name="zscore_FlexArmG">
            <input type="hidden" id="zscore_ForearmG" name="zscore_ForearmG">
            <input type="hidden" id="zscore_WristG" name="zscore_WristG">
            <input type="hidden" id="zscore_ChestG" name="zscore_ChestG">
            <input type="hidden" id="zscore_WaistG" name="zscore_WaistG">
            <input type="hidden" id="zscore_HipG" name="zscore_HipG">
            <input type="hidden" id="zscore_ThighG" name="zscore_ThighG">
            <input type="hidden" id="zscore_MidThighG" name="zscore_MidThighG">
            <input type="hidden" id="zscore_CalfG" name="zscore_CalfG">
            <input type="hidden" id="zscore_AnkleG" name="zscore_AnkleG">
            
            <input type="hidden" id="zscore_AcRad" name="zscore_AcRad">
            <input type="hidden" id="zscore_RadStyl" name="zscore_RadStyl">
            <input type="hidden" id="zscore_midStylDact" name="zscore_midStylDact">
            <input type="hidden" id="zscore_Iliospinale" name="zscore_Iliospinale">
            <input type="hidden" id="zscore_Troch" name="zscore_Troch">
            <input type="hidden" id="zscore_TrochTib" name="zscore_TrochTib">
            <input type="hidden" id="zscore_TibLat" name="zscore_TibLat">
            <input type="hidden" id="zscore_TibMed" name="zscore_TibMed">
            
            <input type="hidden" id="zscore_Biac" name="zscore_Biac">
            <input type="hidden" id="zscore_Bideltoid" name="zscore_Bideltoid">
            <input type="hidden" id="zscore_Billio" name="zscore_Billio">
            <input type="hidden" id="zscore_Bitrochanteric" name="zscore_Bitrochanteric">
            <input type="hidden" id="zscore_Foot" name="zscore_Foot">
            <input type="hidden" id="zscore_Sitting" name="zscore_Sitting">
            <input type="hidden" id="zscore_TrChest" name="zscore_TrChest">
            <input type="hidden" id="zscore_ApChest" name="zscore_ApChest"> 
            <input type="hidden" id="zscore_ArmSpan" name="zscore_ArmSpan"> 
            <input type="hidden" id="zscore_Humerus" name="zscore_Humerus">
            <input type="hidden" id="zscore_Femur" name="zscore_Femur">          
        
     </div>

        <input type="hidden" id="exit_key" name="exit_key" value="">
		<input type="hidden" id="action_key" name="action_key" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
    
        $(window).bind("load", function() {
             
          if(document.getElementById("height").value !== "" && 
          document.getElementById("body_mass").value !== "" &&             
          document.getElementById("triceps").value !== "" &&    
          document.getElementById("subscapular").value !== "" &&    
          document.getElementById("biceps").value !== "" &&   
          document.getElementById("iliac_crest").value !== "" &&    
          document.getElementById("supraspinale").value !== "" &&    
          document.getElementById("abdominal").value !== "" &&    
          document.getElementById("thigh").value !== "" &&
          document.getElementById("calf").value !== "" &&    
          document.getElementById("mid_axilla").value !== "" &&
          document.getElementById("headG").value !== "" && 
          document.getElementById("neckG").value !== "" && 
          document.getElementById("relArmG").value !== "" &&    
          document.getElementById("flexArmG").value !== "" &&
          document.getElementById("forearmG").value !== "" &&    
          document.getElementById("wristG").value !== "" &&    
          document.getElementById("chestG").value !== "" &&
          document.getElementById("waistG").value !== "" &&
          document.getElementById("hipG").value !== "" &&
          document.getElementById("thighG").value !== "" &&   
          document.getElementById("midThighG").value !== "" &&             
          document.getElementById("calfG").value !== "" &&    
          document.getElementById("ankleG").value !== "" &&
          document.getElementById("acRad").value !== "" &&   
          document.getElementById("radStyl").value !== "" &&
          document.getElementById("midStylDact").value !== "" &&   
          document.getElementById("iliospinale").value !== "" &&
          document.getElementById("troch").value !== "" &&    
          document.getElementById("trochTib").value !== "" &&  
          document.getElementById("tibLat").value !== "" &&  
          document.getElementById("tibMed").value !== "" && 
          document.getElementById("biac").value !== "" &&              
          document.getElementById("bideltoid").value !== "" &&    
          document.getElementById("billio").value !== "" &&
          document.getElementById("bitrochanteric").value !== "" &&  
          document.getElementById("foot").value !== "" &&  
          document.getElementById("sitting").value !== "" &&    
          document.getElementById("trChest").value !== "" &&
          document.getElementById("apChest").value !== "" &&
          document.getElementById("armSpan").value !== "" &&  
          document.getElementById("humerus").value !== "" &&    
          document.getElementById("femur").value !== "")
          {      
           document.getElementById('clear_profile').style.display = "" ;
           document.getElementById('virtual_profile').style.display = "none" ;
          }          
          
         }); 
        
        $(document).on('click','#exit', function(){
           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
        });	

        document.getElementById("phantom").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("body_fat").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("skinfold").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
	
        document.getElementById("virtual_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("clear_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','#virtual_profile', function(){ 
          
          document.getElementById("height").value = 179.3 ; 
          document.getElementById("body_mass").value = 74.84 ;             
          document.getElementById("triceps").value = 5 ;    
          document.getElementById("subscapular").value = 8.6 ;    
          document.getElementById("biceps").value = 3.5 ;    
          document.getElementById("iliac_crest").value = 6.5 ;    
          document.getElementById("supraspinale").value = 3.4 ;    
          document.getElementById("abdominal").value = 4.7 ;    
          document.getElementById("thigh").value = 9.2 ;
          document.getElementById("calf").value = 5.4 ;    
          document.getElementById("mid_axilla").value = 4.5 ;
          document.getElementById("headG").value = 56 ; 
          document.getElementById("neckG").value = 37.3 ; 
          document.getElementById("relArmG").value = 33.1 ;    
          document.getElementById("flexArmG").value = 35.5 ;
          document.getElementById("forearmG").value = 28 ;    
          document.getElementById("wristG").value = 17.1 ;    
          document.getElementById("chestG").value = 93.7 ; 
          document.getElementById("waistG").value = 75.2 ;
          document.getElementById("hipG").value = 98 ;    
          document.getElementById("thighG").value = 56.4 ;    
          document.getElementById("midThighG").value = 45.2 ;              
          document.getElementById("calfG").value = 42 ;    
          document.getElementById("ankleG").value = 23.3 ;
          document.getElementById("acRad").value = 35 ;    
          document.getElementById("radStyl").value = 27.2 ;  
          document.getElementById("midStylDact").value = 19.1 ;    
          document.getElementById("iliospinale").value = 97 ;  
          document.getElementById("troch").value = 88.3 ;    
          document.getElementById("trochTib").value = 43.5 ;  
          document.getElementById("tibLat").value = 44.87 ;    
          document.getElementById("tibMed").value = 39.2 ;  
          document.getElementById("biac").value = 41.8 ;              
          document.getElementById("bideltoid").value = 44 ;    
          document.getElementById("billio").value = 29.5 ;
          document.getElementById("bitrochanteric").value = 32 ;    
          document.getElementById("foot").value = 26.4 ;  
          document.getElementById("sitting").value = 96.2 ;    
          document.getElementById("trChest").value = 29.6 ;  
          document.getElementById("apChest").value = 17.6 ;    
          document.getElementById("armSpan").value = 182 ;  
          document.getElementById("humerus").value = 7.43 ;    
          document.getElementById("femur").value = 10.97 ;
          
          document.getElementById('clear_profile').style.display = "" ;
          document.getElementById('virtual_profile').style.display = "none" ;
         
        });
        
        $(document).on('click','#clear_profile', function(){ 
          
          document.getElementById("height").value = "" ; 
          document.getElementById("body_mass").value = "" ;              
          document.getElementById("triceps").value = "" ;    
          document.getElementById("subscapular").value = "" ;   
          document.getElementById("biceps").value = "" ;     
          document.getElementById("iliac_crest").value = "" ;    
          document.getElementById("supraspinale").value = "" ;     
          document.getElementById("abdominal").value = "" ;     
          document.getElementById("thigh").value = "" ; 
          document.getElementById("calf").value = "" ;    
          document.getElementById("mid_axilla").value = "" ; 
          document.getElementById("headG").value = "" ; 
          document.getElementById("neckG").value = "" ; 
          document.getElementById("relArmG").value = "" ;   
          document.getElementById("flexArmG").value = "" ; 
          document.getElementById("forearmG").value = "" ;    
          document.getElementById("wristG").value = "" ;     
          document.getElementById("chestG").value = "" ; 
          document.getElementById("waistG").value = "" ; 
          document.getElementById("hipG").value = "" ;   
          document.getElementById("thighG").value = "" ;    
          document.getElementById("midThighG").value = "" ;              
          document.getElementById("calfG").value = "" ;  
          document.getElementById("ankleG").value = "" ; 
          document.getElementById("acRad").value = "" ;    
          document.getElementById("radStyl").value = "" ;   
          document.getElementById("midStylDact").value = "" ;     
          document.getElementById("iliospinale").value = "" ; 
          document.getElementById("troch").value = "" ;     
          document.getElementById("trochTib").value = "" ;  
          document.getElementById("tibLat").value = "" ;     
          document.getElementById("tibMed").value = "" ;  
          document.getElementById("biac").value = "" ;               
          document.getElementById("bideltoid").value = "" ;    
          document.getElementById("billio").value = "" ; 
          document.getElementById("bitrochanteric").value = "" ;     
          document.getElementById("foot").value = "" ;  
          document.getElementById("sitting").value = "" ; 
          document.getElementById("trChest").value = "" ;   
          document.getElementById("apChest").value = "" ;     
          document.getElementById("armSpan").value = "" ;   
          document.getElementById("humerus").value = "" ;     
          document.getElementById("femur").value = "" ; 
          
          document.getElementById('virtual_profile').style.display = "" ;
          document.getElementById('clear_profile').style.display = "none" ; 
        });
        
</script>  

<script>
    
  //calculate phantom z-score value   
    function getPhantomZscore()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
         {
           alert ("Height and Body mass values should be entered");
           return false;
         }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if(triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == ""
               || thigh == "" || calf == "" || headG == "" || neckG == "" || relArmG == "" || flexArmG == "" ||
               forearmG == "" || wristG == "" || chestG == "" || waistG == "" || hipG == "" || thighG == "" || 
               midThighG == "" || calfG == "" || ankleG == "" || acRad == "" || radStyl == "" || midStylDact == "" 
               || iliospinale == "" || troch == "" || trochTib == "" || tibLat == "" || tibMed == "" || biac == "" || 
               bideltoid == "" || billio == "" || bitrochanteric == "" || foot == "" || sitting == "" || trChest == ""
               || apChest == "" || armSpan == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 1 ;
                document.forms["myform"].submit();
           }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 1 ;
                document.forms["myform"].submit();
          }
        }
    }
 
 
   //calculate Body Fat value
    function getBodyFat()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
         {
           alert ("Height and Body mass values should be entered");
           return false;
         }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 


          if(triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == ""
               || thigh == "" || calf == "" || headG == "" || neckG == "" || relArmG == "" || flexArmG == "" ||
               forearmG == "" || wristG == "" || chestG == "" || waistG == "" || hipG == "" || thighG == "" || 
               midThighG == "" || calfG == "" || ankleG == "" || acRad == "" || radStyl == "" || midStylDact == "" 
               || iliospinale == "" || troch == "" || trochTib == "" || tibLat == "" || tibMed == "" || biac == "" || 
               bideltoid == "" || billio == "" || bitrochanteric == "" || foot == "" || sitting == "" || trChest == ""
               || apChest == "" || armSpan == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 2 ;
                document.forms["myform"].submit();
           }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 2 ;
                document.forms["myform"].submit();
          }
        }
    }
    
 
 //calculate skinfolds value
    function getSkinfolds()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 


          if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || mid_axilla == "")
          {
             alert ("All Skinfolds values should be entered");          
          }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 3 ;
                document.forms["myform"].submit();
          }        
        }
    }
    
    
 //calculate somatotype value
    function getSomatotype()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || subscapular == "" || supraspinale == "" || calf == "" || flexArmG == "" || calfG == "" || humerus == "" || femur == "")
          {
            alert ("Some values are missing");          
          }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 4 ;
                document.forms["myform"].submit();
          }        
        }
    }    
    
 
//calculate anthropometry value
    function getAnthropometry()
    {  
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || biceps == ""  || subscapular == "" || thigh == "" || relArmG == "" || waistG == "" || hipG == "" || thighG == "")
          {
            alert ("Some values are missing");          
          }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 5 ;
                document.forms["myform"].submit();
          }        
        }
    } 


 
//calculate fractionation value
    function getFractionation()
    {  
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || subscapular == "" || thigh == "" || calf == "" || relArmG == "" || chestG == "" || thighG == "" || calfG == "")
          {
            alert ("Some values are missing");          
          }  
          else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 6 ;
                document.forms["myform"].submit();
          }        
        }
    } 

 
      function phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur)
     {
              ht = Math.round(parseFloat(ht) * 10) / 10 ;  //upto 2 decimal places  
              mass = Math.round(parseFloat(mass) * 100) / 100 ;  //upto 2 decimal places

              triceps = Math.round(parseFloat(triceps) * 10) / 10 ;
              subscapular = Math.round(parseFloat(subscapular) * 10) / 10 ;
              biceps = Math.round(parseFloat(biceps) * 10) / 10 ;
              iliac = Math.round(parseFloat(iliac) * 10) / 10 ;
              supraspinale = Math.round(parseFloat(supraspinale) * 10) / 10 ;
              abdominal = Math.round(parseFloat(abdominal) * 10) / 10 ;
              thigh = Math.round(parseFloat(thigh) * 10) / 10 ;
              calf = Math.round(parseFloat(calf) * 10) / 10 ;
              
              headG = Math.round(parseFloat(headG) * 10) / 10 ;
              neckG = Math.round(parseFloat(neckG) * 10) / 10 ;
              relArmG = Math.round(parseFloat(relArmG) * 10) / 10 ;
              flexArmG = Math.round(parseFloat(flexArmG) * 10) / 10 ;
              forearmG = Math.round(parseFloat(forearmG) * 10) / 10 ;
              wristG = Math.round(parseFloat(wristG) * 10) / 10 ;
              chestG = Math.round(parseFloat(chestG) * 10) / 10 ;
              waistG = Math.round(parseFloat(waistG) * 10) / 10 ;
              hipG = Math.round(parseFloat(hipG) * 10) / 10 ;
              thighG = Math.round(parseFloat(thighG) * 10) / 10 ;
              midThighG = Math.round(parseFloat(midThighG) * 10) / 10 ;
              calfG = Math.round(parseFloat(calfG) * 10) / 10 ;
              ankleG = Math.round(parseFloat(ankleG) * 10) / 10 ;              
              
              acRad = Math.round(parseFloat(acRad) * 10) / 10 ;
              radStyl = Math.round(parseFloat(radStyl) * 10) / 10 ;
              midStylDact = Math.round(parseFloat(midStylDact) * 10) / 10 ;
              iliospinale = Math.round(parseFloat(iliospinale) * 10) / 10 ;
              troch = Math.round(parseFloat(troch) * 10) / 10 ;
              trochTib = Math.round(parseFloat(trochTib) * 10) / 10 ;
              tibLat = Math.round(parseFloat(tibLat) * 10) / 10 ;
              tibMed = Math.round(parseFloat(tibMed) * 10) / 10 ;
              
              
              biac = Math.round(parseFloat(biac) * 10) / 10 ;
              bideltoid = Math.round(parseFloat(bideltoid) * 10) / 10 ;
              billio = Math.round(parseFloat(billio) * 10) / 10 ;
              bitrochanteric = Math.round(parseFloat(bitrochanteric) * 10) / 10 ;
              foot = Math.round(parseFloat(foot) * 10) / 10 ;
              sitting = Math.round(parseFloat(sitting) * 10) / 10 ;
              trChest = Math.round(parseFloat(trChest) * 10) / 10 ;
              apChest = Math.round(parseFloat(apChest) * 10) / 10 ;
              armSpan = Math.round(parseFloat(armSpan) * 10) / 10 ;
              humerus = Math.round(parseFloat(humerus) * 100) / 100 ; //upto 2 decimal places
              femur = Math.round(parseFloat(femur) * 100) / 100 ; //upto 2 decimal places


          // Skinfolds Phantom Zscores    
              var zscore_Triceps = Math.round([(parseFloat(triceps) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
              var zscore_Subscapular = Math.round([(parseFloat(subscapular) * (170.18 / parseFloat(ht)) - 17.2) / 5.07] * 10) / 10 ;
              var zscore_Biceps= Math.round([(parseFloat(biceps) * (170.18 / parseFloat(ht)) - 8) / 2] * 10) / 10 ;
              var zscore_Iliac = Math.round([(parseFloat(iliac) * (170.18 / parseFloat(ht)) - 22.4) / 6.8] * 10) / 10 ;
              var zscore_Supspinale = Math.round([(parseFloat(supraspinale) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
              var zscore_Abdominal = Math.round([(parseFloat(abdominal) * (170.18 / parseFloat(ht)) - 25.4) / 7.78] * 10) / 10 ;
              var zscore_Thigh = Math.round([(parseFloat(thigh) * (170.18 / parseFloat(ht)) - 27) / 8.33] * 10) / 10 ;
              var zscore_Calf = Math.round([(parseFloat(calf) * (170.18 / parseFloat(ht)) - 16) / 4.67] * 10) / 10 ;

          // Girths Phantom Zscores  
              var zscore_HeadG = Math.round([(parseFloat(headG) * (170.18 / parseFloat(ht)) - 56) / 1.44] * 10) / 10 ;
              var zscore_NeckG = Math.round([(parseFloat(neckG) * (170.18 / parseFloat(ht)) - 34.91) / 1.73] * 10) / 10 ;
              var zscore_RelArmG = Math.round([(parseFloat(relArmG) * (170.18 / parseFloat(ht)) - 26.89) / 2.33] * 10) / 10 ;
              var zscore_FlexArmG = Math.round([(parseFloat(flexArmG) * (170.18 / parseFloat(ht)) - 29.41) / 2.37] * 10) / 10 ;
              var zscore_ForearmG= Math.round([(parseFloat(forearmG) * (170.18 / parseFloat(ht)) - 25.13) / 1.41] * 10) / 10 ;
              var zscore_WristG = Math.round([(parseFloat(wristG) * (170.18 / parseFloat(ht)) - 16.35) / 0.72] * 10) / 10 ;
              var zscore_ChestG = Math.round([(parseFloat(chestG) * (170.18 / parseFloat(ht)) - 87.86) / 5.18] * 10) / 10 ;
              var zscore_WaistG = Math.round([(parseFloat(waistG) * (170.18 / parseFloat(ht)) - 71.91) / 4.45] * 10) / 10 ;
              var zscore_HipG = Math.round([(parseFloat(hipG) * (170.18 / parseFloat(ht)) - 94.67) / 5.58] * 10) / 10 ;              
              var zscore_ThighG = Math.round([(parseFloat(thighG) * (170.18 / parseFloat(ht)) - 55.82) / 4.23] * 10) / 10 ;
              var zscore_MidThighG = Math.round([(parseFloat(midThighG) * (170.18 / parseFloat(ht)) - 51.84) / 3.44] * 10) / 10 ;
              var zscore_CalfG = Math.round([(parseFloat(calfG) * (170.18 / parseFloat(ht)) - 35.25) / 2.3] * 10) / 10 ;
              var zscore_AnkleG = Math.round([(parseFloat(ankleG) * (170.18 / parseFloat(ht)) - 21.71) / 1.33] * 10) / 10 ;          

          // Lengths Phantom Zscores      
              var zscore_AcRad = Math.round([(parseFloat(acRad) * (170.18 / parseFloat(ht)) - 32.53) / 1.77] * 10) / 10 ;
              var zscore_RadStyl = Math.round([(parseFloat(radStyl) * (170.18 / parseFloat(ht)) - 24.57) / 1.37] * 10) / 10 ;
              var zscore_midStylDact = Math.round([(parseFloat(midStylDact) * (170.18 / parseFloat(ht)) - 18.85) / 0.85] * 10) / 10 ;
              var zscore_Iliospinale = Math.round([(parseFloat(iliospinale) * (170.18 / parseFloat(ht)) - 94.11) / 4.71] * 10) / 10 ;
              var zscore_Troch = Math.round([(parseFloat(troch) * (170.18 / parseFloat(ht)) - 86.4) / 4.32] * 10) / 10 ;
              var zscore_TrochTib = Math.round([(parseFloat(trochTib) * (170.18 / parseFloat(ht)) - 41.37) / 2.48] * 10) / 10 ;
              var zscore_TibLat = Math.round([(parseFloat(tibLat) * (170.18 / parseFloat(ht)) - 44.82) / 2.56] * 10) / 10 ;
              var zscore_TibMed = Math.round([(parseFloat(tibMed) * (170.18 / parseFloat(ht)) - 36.81) / 2.1] * 10) / 10 ;
              
          // Breadths Phantom Zscores              
              var zscore_Biac = Math.round([(parseFloat(biac) * (170.18 / parseFloat(ht)) - 38.04) / 1.92] * 10) / 10 ;
              var zscore_Bideltoid = Math.round([(parseFloat(bideltoid) * (170.18 / parseFloat(ht)) - 43.5) / 2.40] * 10) / 10 ;
              var zscore_Billio = Math.round([(parseFloat(billio) * (170.18 / parseFloat(ht)) - 28.84) / 1.75] * 10) / 10 ;
              var zscore_Bitrochanteric = Math.round([(parseFloat(bitrochanteric) * (170.18 / parseFloat(ht)) - 32.66) / 1.8] * 10) / 10 ;
              var zscore_Foot = Math.round([(parseFloat(foot) * (170.18 / parseFloat(ht)) - 25.5) / 1.16] * 10) / 10 ;
              var zscore_Sitting = Math.round([(parseFloat(sitting) * (170.18 / parseFloat(ht)) - 89.92) / 4.5] * 10) / 10 ;
              var zscore_TrChest = Math.round([(parseFloat(trChest) * (170.18 / parseFloat(ht)) - 27.92) / 1.74] * 10) / 10 ;
              var zscore_ApChest = Math.round([(parseFloat(apChest) * (170.18 / parseFloat(ht)) - 17.5) / 1.38] * 10) / 10 ;
              var zscore_ArmSpan = Math.round([(parseFloat(armSpan) * (170.18 / parseFloat(ht)) - 172.35) / 7.41] * 10) / 10 ;
              var zscore_Humerus = Math.round([(parseFloat(humerus) * (170.18 / parseFloat(ht)) - 6.48) / 0.35] * 10) / 10 ;
              var zscore_Femur = Math.round([(parseFloat(femur) * (170.18 / parseFloat(ht)) - 9.52) / 0.48] * 10) / 10 ;	  


              document.getElementById("zscore_Triceps").value = parseFloat(zscore_Triceps) ;
              document.getElementById("zscore_Subscapular").value = parseFloat(zscore_Subscapular) ;
              document.getElementById("zscore_Biceps").value = parseFloat(zscore_Biceps) ;
              document.getElementById("zscore_Iliac").value = parseFloat(zscore_Iliac) ;
              document.getElementById("zscore_Supspinale").value = parseFloat(zscore_Supspinale) ;
              document.getElementById("zscore_Abdominal").value = parseFloat(zscore_Abdominal) ;
              document.getElementById("zscore_Thigh").value = parseFloat(zscore_Thigh) ;
              document.getElementById("zscore_Calf").value = parseFloat(zscore_Calf) ;
              
              document.getElementById("zscore_HeadG").value = parseFloat(zscore_HeadG) ;
              document.getElementById("zscore_NeckG").value = parseFloat(zscore_NeckG) ;
              document.getElementById("zscore_RelArmG").value = parseFloat(zscore_RelArmG) ;
              document.getElementById("zscore_FlexArmG").value = parseFloat(zscore_FlexArmG) ;
              document.getElementById("zscore_ForearmG").value = parseFloat(zscore_ForearmG) ;
              document.getElementById("zscore_WristG").value = parseFloat(zscore_WristG) ;
              document.getElementById("zscore_ChestG").value = parseFloat(zscore_ChestG) ;
              document.getElementById("zscore_WaistG").value = parseFloat(zscore_WaistG) ;
              document.getElementById("zscore_HipG").value = parseFloat(zscore_HipG) ;            
              document.getElementById("zscore_ThighG").value = parseFloat(zscore_ThighG) ;
              document.getElementById("zscore_MidThighG").value = parseFloat(zscore_MidThighG) ;
              document.getElementById("zscore_CalfG").value = parseFloat(zscore_CalfG) ;
              document.getElementById("zscore_AnkleG").value = parseFloat(zscore_AnkleG) ;      
              
              document.getElementById("zscore_AcRad").value = parseFloat(zscore_AcRad) ;
              document.getElementById("zscore_RadStyl").value = parseFloat(zscore_RadStyl) ;
              document.getElementById("zscore_midStylDact").value = parseFloat(zscore_midStylDact) ;
              document.getElementById("zscore_Iliospinale").value = parseFloat(zscore_Iliospinale) ;
              document.getElementById("zscore_Troch").value = parseFloat(zscore_Troch) ;
              document.getElementById("zscore_TrochTib").value = parseFloat(zscore_TrochTib) ;
              document.getElementById("zscore_TibLat").value = parseFloat(zscore_TibLat) ;
              document.getElementById("zscore_TibMed").value = parseFloat(zscore_TibMed) ;
              
              document.getElementById("zscore_Biac").value = parseFloat(zscore_Biac) ;
              document.getElementById("zscore_Bideltoid").value = parseFloat(zscore_Bideltoid) ;
              document.getElementById("zscore_Billio").value = parseFloat(zscore_Billio) ;
              document.getElementById("zscore_Bitrochanteric").value = parseFloat(zscore_Bitrochanteric) ;
              document.getElementById("zscore_Foot").value = parseFloat(zscore_Foot) ;
              document.getElementById("zscore_Sitting").value = parseFloat(zscore_Sitting) ;
              document.getElementById("zscore_TrChest").value = parseFloat(zscore_TrChest) ;
              document.getElementById("zscore_ApChest").value = parseFloat(zscore_ApChest) ;
              document.getElementById("zscore_ArmSpan").value = parseFloat(zscore_ArmSpan) ;              
              document.getElementById("zscore_Humerus").value = parseFloat(zscore_Humerus) ;
              document.getElementById("zscore_Femur").value = parseFloat(zscore_Femur) ;                             

              document.getElementById("exit_key").value = 0 ;
              document.forms["myform"].submit(); 
     }
</script>
    
</body>
</html>
