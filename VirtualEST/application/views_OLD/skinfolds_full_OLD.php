<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                  <a onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';" class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>   
                <h1 class="page_head">SKINFOLD ANALYSIS</h1>   
                <h1 class="page_head"><span><?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</span></h1>               
              </td>
              </tr>
                 <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>">
                 <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                 <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){echo "Male" ;}elseif($_SESSION['user_gender'] == "F"){echo "Female" ;}?>"> 
            	<tr>                
                <td colspan="3" align="center"> 
                	<table width="100%">
                      <tbody> 
                        <tr> 
                          <td valign="top" width="30%">
                          		<div class="skinfold_head">sum of skinfolds</div>
                                
                          		<table width="100%" class="tabel_bord tabel_bord_skinfold" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>&nbsp;</td> 	
                                      <td>Skinfolds</td>
                                      <td>mm</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["triceps"] ; ?>">&nbsp;</div> </td> 	
                                      <td>Triceps</td>
                                      <td><input type="text" id="triceps" name="triceps" class="cus-input width_60" value="<?php echo $skinfolds_Values["triceps"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["subscapular"] ; ?>">&nbsp;</div></td> 	
                                      <td>Subscapular</td>
                                      <td><input type="text" id="subscapular" name="subscapular" class="cus-input width_60" value="<?php echo $skinfolds_Values["subscapular"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)"  value="<?php echo $skinfolds_Values["biceps"] ; ?>">&nbsp;</div></td> 	
                                      <td>Biceps</td>
                                      <td><input type="text" id="biceps" name="biceps" class="cus-input width_60" value="<?php echo $skinfolds_Values["biceps"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["iliac_crest"] ; ?>">&nbsp;</div></td> 	
                                      <td>Iliac crest</td>
                                      <td><input type="text" id="iliac_crest" name="iliac_crest" class="cus-input width_60" value="<?php echo $skinfolds_Values["iliac_crest"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["supraspinale"] ; ?>">&nbsp;</div></td> 	
                                      <td>Supraspinale</td>
                                      <td><input type="text" id="supraspinale" name="supraspinale" class="cus-input width_60" value="<?php echo $skinfolds_Values["supraspinale"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["abdominal"] ; ?>">&nbsp;</div></td> 	
                                      <td>Abdominal</td>
                                      <td><input type="text" id="abdominal" name="abdominal" class="cus-input width_60" value="<?php echo $skinfolds_Values["abdominal"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["thigh"] ; ?>">&nbsp;</div></td> 	
                                      <td>Front thigh</td>
                                      <td><input type="text" id="thigh" name="thigh" class="cus-input width_60" value="<?php echo $skinfolds_Values["thigh"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="green_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["calf"] ; ?>">&nbsp;</div></td> 	
                                      <td>Medial calf</td>
                                      <td><input type="text" id="calf" name="calf" class="cus-input width_60" value="<?php echo $skinfolds_Values["calf"] ; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td><div class="red_circle" onclick="changeState(this)" value="<?php echo $skinfolds_Values["mid_axilla"] ; ?>">&nbsp;</div></td> 	
                                      <td>Mid-axilla</td>
                                      <td><input type="text" id="mid_axilla" name="mid_axilla" class="cus-input width_60" value="<?php echo $skinfolds_Values["mid_axilla"] ; ?>"> </td>
                                    </tr>                                
                                  </tbody>
                            </table>
                          </td> 
                          <td valign="top">
                          		<div class="skinfold_head">population norms</div> 
                          		<div class="diag_chart">
                                	<div class="graph">
                                      
                                      <ul class="bottom_values">
                                        <li><span>1</span></li>
                                        <li><span>5</span></li>
                                        <li><span>25</span></li>
                                        <li><span>50</span></li>
                                        <li><span>75</span></li>
                                        <li><span>90</span></li>
                                        <li><span>95</span></li>
                                      </ul>
                                            
                                      <ul class="bottom_values bottom_values_points">
                                        <li>
                                          <div class="blue_dot green_circle" id="plot1" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot2" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot3" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot4" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot5" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot6" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot7" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot8" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot9" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot10" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot11" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot12" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot13" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot14" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot15" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot16" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot17" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot18" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot19" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot20" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot21" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot22" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot23" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot24" style="visibility:hidden">&nbsp;</div>
                                          <div class="blue_dot green_circle" id="plot25" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                        <li>
                                          
                                        </li>
                                        <li>
                                          <div class="blue_dot green_circle" id="plot25" style="visibility:hidden">&nbsp;</div>
                                        </li>
                                      </ul>      
                                      
                                      <div class="graph_pulse_box"><img src="http://115.112.118.252/healthscreen//assets/images//pulse2.png" alt=""></div>
                                      <div class="percentile_box">
                                      	<input type="text" id="percentile" name="percentile" class="cus-input width_60"> %'ile
                                      </div>
                                    </div>
                                
                                </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
              <td align="left">
              	<table class="sigma_table">
                	<tr>
                        <td><a href="#" class="rest_bot_btns" id="four_Sites"><span><div id="sum4" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a></td>
                        <td align="left">&Sigma; 4 skinfolds</td> <input type="hidden" id="four_last" name="four_last" value="">
                        <td><input type="text" id="four_Sum" name="four_Sum" class="cus-input" value=""> mm</td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="five_Sites"><span><div id="sum5" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 5 skinfolds</td> <input type="hidden" id="five_last" name="five_last" value="">
                        <td><input type="text" id="five_Sum" name="five_Sum" class="cus-input" value=""></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="six_Sites"><span><div id="sum6" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 6 skinfolds</td> <input type="hidden" id="six_last" name="six_last" value="">
                        <td><input type="text" id="six_Sum" name="six_Sum" class="cus-input" value=""></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="seven_Sites"><span><div id="sum7" style="display:none;"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 7 skinfolds</td> <input type="hidden" id="seven_last" name="seven_last" value="">
                        <td><input type="text" id="seven_Sum" name="seven_Sum" class="cus-input" value=""></td>
                    </tr>
                    <tr>
                    	<td><a href="#" class="rest_bot_btns" id="eight_Sites"><span><div id="sum8"><img src="<?php echo "$base/$image"?>/cross.png"></div></span></a> </td>
                        <td align="left">&Sigma; 8 skinfolds</td> <input type="hidden" id="eight_last" name="eight_last" value="">
                        <td><input type="text" id="eight_Sum" name="eight_Sum" class="cus-input" value=""></td>
                    </tr>
                </table>
              </td>  
              
              <td align="right" colspan="2" valign="bottom">      
              	 <div style="display:block; margin-bottom: 115px;"><button id="plot" name="plot" class="plot_btn" style="margin-top:0;">Skinfold Map</button></div>   
              	 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>

             <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">    
    
    $(window).bind("load", function() {
        
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        var mid_axilla = document.getElementById("mid_axilla").value ;          
       
        var sum = parseFloat(triceps) + parseFloat(subscapular) + parseFloat(biceps) + parseFloat(iliac) + parseFloat(supraspinale) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf) ;        
        document.getElementById("eight_Sum").value =  Math.round(parseFloat(sum) * 10) / 10 ; 

    if(document.getElementById('gender').value == "Male")
    { 
        if(document.getElementById("age_range").value == "18-29")
        {
          if(sum >= 15.31 && sum < 16.17)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (16.17 - 15.31) / range_difference ; // each % point
             var difference = sum - 15.31 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 16.17 && sum < 18.01)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (18.01 - 16.17) / range_difference ; // each % point
             var difference = sum - 16.17 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 18.01 && sum < 20.21)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (20.21 - 18.01) / range_difference ; // each % point
             var difference = sum - 18.01 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 20.21 && sum < 23.7)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (23.7 - 20.21) / range_difference ; // each % point
             var difference = sum - 20.21 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 23.7 && sum < 27.09)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (27.09 - 23.7) / range_difference ; // each % point
             var difference = sum - 23.7 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 27.09 && sum < 31.00)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (31.00 - 27.09) / range_difference ; // each % point
             var difference = sum - 27.09 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 31.00 && sum < 34.4)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (34.4 - 31.00) / range_difference ; // each % point
             var difference = sum - 31.00 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 34.4 && sum < 39.03)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (39.03 - 34.4) / range_difference ; // each % point
             var difference = sum - 34.4 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 39.03 && sum < 44.77)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (44.77 - 39.03) / range_difference ; // each % point
             var difference = sum - 39.03 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 44.77 && sum < 50.77)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (50.77 - 44.77) / range_difference ; // each % point
             var difference = sum - 44.77 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 50.77 && sum < 62.21)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (62.21 - 50.77) / range_difference ; // each % point
             var difference = sum - 50.77 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 62.21 && sum < 71.02)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (71.02 - 62.21) / range_difference ; // each % point
             var difference = sum - 62.21 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 71.02 && sum < 81.78)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (81.78 - 71.02) / range_difference ; // each % point
             var difference = sum - 71.02 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 81.78 && sum < 89.86)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (89.86 - 81.78) / range_difference ; // each % point
             var difference = sum - 81.78 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 89.86)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (89.86 - 89.86) / range_difference ; // each % point
             var difference = sum - 89.86 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "30-39")
        {
          if(sum >= 14.93 && sum < 16.3)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (16.3 - 14.93) / range_difference ; // each % point
             var difference = sum - 14.93 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 16.3 && sum < 19.56)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (19.56 - 16.3) / range_difference ; // each % point
             var difference = sum - 16.3 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 19.56 && sum < 22.24)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (22.24 - 19.56) / range_difference ; // each % point
             var difference = sum - 19.56 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 22.24 && sum < 27.98)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (27.98 - 22.24) / range_difference ; // each % point
             var difference = sum - 22.24 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 27.98 && sum < 32.8)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (32.8 - 27.98) / range_difference ; // each % point
             var difference = sum - 27.98 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 32.8 && sum < 36.95)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (36.95 - 32.8) / range_difference ; // each % point
             var difference = sum - 32.8 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 36.95 && sum < 40.76)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (40.76 - 36.95) / range_difference ; // each % point
             var difference = sum - 36.95 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 40.76 && sum < 44.61)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (44.61 - 40.76) / range_difference ; // each % point
             var difference = sum - 40.76 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 44.61 && sum < 49.97)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (49.97 - 44.61) / range_difference ; // each % point
             var difference = sum - 44.61 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 49.97 && sum < 56.09)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (56.09 - 49.97) / range_difference ; // each % point
             var difference = sum - 49.97 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 56.09 && sum < 66.31)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (66.31 - 56.09) / range_difference ; // each % point
             var difference = sum - 56.09 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 66.31 && sum < 75.87)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (75.87 - 66.31) / range_difference ; // each % point
             var difference = sum - 66.31 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 75.87 && sum < 88.57)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (88.57 - 75.87) / range_difference ; // each % point
             var difference = sum - 75.87 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 88.57 && sum < 96.81)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (96.81 - 88.57) / range_difference ; // each % point
             var difference = sum - 88.57 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 96.81)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (96.81 - 96.81) / range_difference ; // each % point
             var difference = sum - 96.81 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "40-49")
        {
          if(sum >= 15.14 && sum < 16.7)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (16.7 - 15.14) / range_difference ; // each % point
             var difference = sum - 15.14 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 16.7 && sum < 20.37)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (20.37 - 16.7) / range_difference ; // each % point
             var difference = sum - 16.7 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 20.37 && sum < 24.84)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (24.84 - 20.37) / range_difference ; // each % point
             var difference = sum - 20.37 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 24.84 && sum < 31.35)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (31.35 - 24.84) / range_difference ; // each % point
             var difference = sum - 24.84 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 31.35 && sum < 35.28)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (35.28 - 31.35) / range_difference ; // each % point
             var difference = sum - 31.35 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 35.28 && sum < 38.27)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (38.27 - 35.28) / range_difference ; // each % point
             var difference = sum - 35.28 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 38.27 && sum < 42.68)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (42.68 - 38.27) / range_difference ; // each % point
             var difference = sum - 38.27 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 42.68 && sum < 46.54)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (46.54 - 42.68) / range_difference ; // each % point
             var difference = sum - 42.68 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 46.54 && sum < 51.22)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (51.22 - 46.54) / range_difference ; // each % point
             var difference = sum - 46.54 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 51.22 && sum < 57.25)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (57.25 - 51.22) / range_difference ; // each % point
             var difference = sum - 51.22 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 57.25 && sum < 67.02)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (67.02 - 57.25) / range_difference ; // each % point
             var difference = sum - 57.25 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 67.02 && sum < 76.69)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (76.69 - 67.02) / range_difference ; // each % point
             var difference = sum - 67.02 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 76.69 && sum < 83.9)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (83.9 - 76.69) / range_difference ; // each % point
             var difference = sum - 76.69 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 83.9 && sum < 90.71)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (90.71 - 83.9) / range_difference ; // each % point
             var difference = sum - 83.9 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 90.71)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (90.71 - 90.71) / range_difference ; // each % point
             var difference = sum - 90.71 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "50-59")
        {
          if(sum >= 15.49 && sum < 18.09)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (18.09 - 15.49) / range_difference ; // each % point
             var difference = sum - 15.49 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 18.09 && sum < 23.6)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (23.6 - 18.09) / range_difference ; // each % point
             var difference = sum - 18.09 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 23.6 && sum < 26.58)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (26.58 - 23.6) / range_difference ; // each % point
             var difference = sum - 23.6 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 26.58 && sum < 31.5)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (31.5 - 26.58) / range_difference ; // each % point
             var difference = sum - 26.58 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 31.5 && sum < 36.53)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (36.53 - 31.5) / range_difference ; // each % point
             var difference = sum - 31.5 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 36.53 && sum < 40.7)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (40.7 - 36.53) / range_difference ; // each % point
             var difference = sum - 36.53 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 40.7 && sum < 44.39)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (44.39 - 40.7) / range_difference ; // each % point
             var difference = sum - 40.7 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 44.39 && sum < 47.96)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (47.96 - 44.39) / range_difference ; // each % point
             var difference = sum - 44.39 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 47.96 && sum < 52.07)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (52.07 - 47.96) / range_difference ; // each % point
             var difference = sum - 47.96 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.07 && sum < 58.06)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (58.06 - 52.07) / range_difference ; // each % point
             var difference = sum - 52.07 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 58.06 && sum < 66.69)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (66.69 - 58.06) / range_difference ; // each % point
             var difference = sum - 58.06 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 66.69 && sum < 74.92)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (74.92 - 66.69) / range_difference ; // each % point
             var difference = sum - 66.69 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 74.92 && sum < 88.89)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (88.89 - 74.92) / range_difference ; // each % point
             var difference = sum - 74.92 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 88.89 && sum < 97.52)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (97.52 - 88.89) / range_difference ; // each % point
             var difference = sum - 88.89 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 97.52)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (97.52 - 97.52) / range_difference ; // each % point
             var difference = sum - 97.52 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "60-69")
        {
          if(sum >= 17.22 && sum < 18.86)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (18.86 - 17.22) / range_difference ; // each % point
             var difference = sum - 17.22 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 18.86 && sum < 24.26)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (24.26 - 18.86) / range_difference ; // each % point
             var difference = sum - 18.86 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 24.26 && sum < 27.95)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (27.95 - 24.26) / range_difference ; // each % point
             var difference = sum - 24.26 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 27.95 && sum < 33)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (33 - 27.95) / range_difference ; // each % point
             var difference = sum - 27.95 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 33 && sum < 37.29)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (37.29 - 33) / range_difference ; // each % point
             var difference = sum - 33 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 37.29 && sum < 40.73)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (40.73 - 37.29) / range_difference ; // each % point
             var difference = sum - 37.29 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 40.73 && sum < 44.46)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (44.46 - 40.73) / range_difference ; // each % point
             var difference = sum - 40.73 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 44.46 && sum < 48.32)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (48.32 - 44.46) / range_difference ; // each % point
             var difference = sum - 44.46 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 48.32 && sum < 52.97)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (52.97 - 48.32) / range_difference ; // each % point
             var difference = sum - 48.32 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.97 && sum < 58.89)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (58.89 - 52.97) / range_difference ; // each % point
             var difference = sum - 52.97 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 58.89 && sum < 69.38)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (69.38 - 58.89) / range_difference ; // each % point
             var difference = sum - 58.89 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 69.38 && sum < 79.36)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (79.36 - 69.38) / range_difference ; // each % point
             var difference = sum - 69.38 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 79.36 && sum < 91.1)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (91.1 - 79.36) / range_difference ; // each % point
             var difference = sum - 79.36 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 91.1 && sum < 95.72)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (95.72 - 91.1) / range_difference ; // each % point
             var difference = sum - 91.1 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 95.72)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (95.72 - 95.72) / range_difference ; // each % point
             var difference = sum - 95.72 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "70+")
        {
          if(sum >= 16.7 && sum < 18.35)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (18.35 - 16.7) / range_difference ; // each % point
             var difference = sum - 16.7 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 18.35 && sum < 21.21)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (21.21 - 18.35) / range_difference ; // each % point
             var difference = sum - 18.35 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 21.21 && sum < 25.22)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (25.22 - 21.21) / range_difference ; // each % point
             var difference = sum - 21.21 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 25.22 && sum < 30.95)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (30.95 - 25.22) / range_difference ; // each % point
             var difference = sum - 25.22 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 30.95 && sum < 34.96)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (34.96 - 30.95) / range_difference ; // each % point
             var difference = sum - 30.95 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 34.96 && sum < 38.35)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (38.35 - 34.96) / range_difference ; // each % point
             var difference = sum - 34.96 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 38.35 && sum < 42.08)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (42.08 - 38.35) / range_difference ; // each % point
             var difference = sum - 38.35 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 42.08 && sum < 48.32)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (48.32 - 42.08) / range_difference ; // each % point
             var difference = sum - 42.08 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 48.32 && sum < 50.76)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (50.76 - 48.32) / range_difference ; // each % point
             var difference = sum - 48.32 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 50.76 && sum < 56.64)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (56.64 - 50.76) / range_difference ; // each % point
             var difference = sum - 50.76 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 56.64 && sum < 65.75)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (65.75 - 56.64) / range_difference ; // each % point
             var difference = sum - 56.64 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 65.75 && sum < 74.96)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (74.96 - 65.75) / range_difference ; // each % point
             var difference = sum - 65.75 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 74.96 && sum < 81.98)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (81.98 - 74.96) / range_difference ; // each % point
             var difference = sum - 74.96 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 81.98 && sum < 89.78)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (89.78 - 81.98) / range_difference ; // each % point
             var difference = sum - 81.98 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 89.78)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (89.78 - 89.78) / range_difference ; // each % point
             var difference = sum - 89.78 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }        
    }
    else if(document.getElementById('gender').value == "Female")
    { 
        if(document.getElementById("age_range").value == "18-29")
        {
          if(sum >= 27.36 && sum < 30.25)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (30.25 - 27.36) / range_difference ; // each % point
             var difference = sum - 27.36 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 30.25 && sum < 34.57)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (34.57 - 30.25) / range_difference ; // each % point
             var difference = sum - 30.25 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 34.57 && sum < 39.6)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (39.6 - 34.57) / range_difference ; // each % point
             var difference = sum - 34.57 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 39.6 && sum < 45.95)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (45.95 - 39.6) / range_difference ; // each % point
             var difference = sum - 39.6 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 45.95 && sum < 52.7)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (52.7 - 45.95) / range_difference ; // each % point
             var difference = sum - 45.95 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.7 && sum < 58.81)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (58.81 - 52.7) / range_difference ; // each % point
             var difference = sum - 52.7 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 58.81 && sum < 64.37)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (64.37 - 58.81) / range_difference ; // each % point
             var difference = sum - 58.81 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 64.37 && sum < 70.36)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (70.36 - 64.37) / range_difference ; // each % point
             var difference = sum - 64.37 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 70.36 && sum < 78.02)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (78.02 - 70.36) / range_difference ; // each % point
             var difference = sum - 70.36 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 78.02 && sum < 87.12)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (87.12 - 78.02) / range_difference ; // each % point
             var difference = sum - 78.02 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 87.12 && sum < 98.23)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (98.23 - 87.12) / range_difference ; // each % point
             var difference = sum - 87.12 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 98.23 && sum < 107.81)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (107.81 - 98.23) / range_difference ; // each % point
             var difference = sum - 98.23 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 107.81 && sum < 117.21)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (117.21 - 107.81) / range_difference ; // each % point
             var difference = sum - 107.81 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 117.21 && sum < 122.81)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (122.81 - 117.21) / range_difference ; // each % point
             var difference = sum - 117.21 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 122.81)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (122.81 - 122.81) / range_difference ; // each % point
             var difference = sum - 122.81 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        } 
        else if(document.getElementById("age_range").value == "30-39")
        {
          if(sum >= 27.57 && sum < 31.49)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (31.49 - 27.57) / range_difference ; // each % point
             var difference = sum - 27.57 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 31.49 && sum < 37.3)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (37.3 - 31.49) / range_difference ; // each % point
             var difference = sum - 31.49 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 37.3 && sum < 43.51)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (43.51 - 37.3) / range_difference ; // each % point
             var difference = sum - 37.3 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 43.51 && sum < 52.86)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (52.86 - 43.51) / range_difference ; // each % point
             var difference = sum - 43.51 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.86 && sum < 61.47)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (61.47 - 52.86) / range_difference ; // each % point
             var difference = sum - 52.86 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 61.47 && sum < 67.79)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (67.79 - 61.47) / range_difference ; // each % point
             var difference = sum - 61.47 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 67.79 && sum < 74.88)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (74.88 - 67.79) / range_difference ; // each % point
             var difference = sum - 67.79 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 74.88 && sum < 81.58)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (81.58 - 74.88) / range_difference ; // each % point
             var difference = sum - 74.88 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 81.58 && sum < 88.85)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (88.85 - 81.58) / range_difference ; // each % point
             var difference = sum - 81.58 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 88.85 && sum < 96.84)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (96.85 - 88.85) / range_difference ; // each % point
             var difference = sum - 88.85 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 96.84 && sum < 107.45)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (107.45 - 96.84) / range_difference ; // each % point
             var difference = sum - 96.84 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 107.45 && sum < 117.15)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (117.15 - 107.45) / range_difference ; // each % point
             var difference = sum - 107.45 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 117.15 && sum < 124.19)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (124.19 - 117.15) / range_difference ; // each % point
             var difference = sum - 117.15 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 124.19 && sum < 128.67)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (128.67 - 124.19) / range_difference ; // each % point
             var difference = sum - 124.19 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 128.67)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (128.67 - 128.67) / range_difference ; // each % point
             var difference = sum - 128.67 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "40-49")
        {
          if(sum >= 29.31 && sum < 35.28)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (35.28 - 29.31) / range_difference ; // each % point
             var difference = sum - 29.31 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 35.28 && sum < 44.75)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (44.75 - 35.28) / range_difference ; // each % point
             var difference = sum - 35.28 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 44.75 && sum < 52.98)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (52.98 - 44.75) / range_difference ; // each % point
             var difference = sum - 44.75 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.98 && sum < 63.04)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (63.04 - 52.98) / range_difference ; // each % point
             var difference = sum - 52.98 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 63.04 && sum < 71.87)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (71.87 - 63.04) / range_difference ; // each % point
             var difference = sum - 63.04 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 71.87 && sum < 77.97)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (77.97 - 71.87) / range_difference ; // each % point
             var difference = sum - 71.87 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 77.97 && sum < 83.78)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (83.78 - 77.97) / range_difference ; // each % point
             var difference = sum - 77.97 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 83.78 && sum < 90.15)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (90.15 - 83.78) / range_difference ; // each % point
             var difference = sum - 83.78 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 90.15 && sum < 96.05)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (96.05 - 90.15) / range_difference ; // each % point
             var difference = sum - 90.15 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 96.05 && sum < 103.68)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (103.68 - 96.05) / range_difference ; // each % point
             var difference = sum - 96.05 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 103.68 && sum < 112.89)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (112.89 - 103.68) / range_difference ; // each % point
             var difference = sum - 103.68 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 112.89 && sum < 118.29)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (118.29 - 112.89) / range_difference ; // each % point
             var difference = sum - 112.89 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 118.29 && sum < 124.14)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (124.14 - 118.29) / range_difference ; // each % point
             var difference = sum - 118.29 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 124.14 && sum < 130.18)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (130.18 - 124.14) / range_difference ; // each % point
             var difference = sum - 124.14 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 130.18)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (130.18 - 130.18) / range_difference ; // each % point
             var difference = sum - 130.18 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "50-59")
        {
          if(sum >= 29.17 && sum < 33.77)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (33.77 - 29.17) / range_difference ; // each % point
             var difference = sum - 29.17 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 33.77 && sum < 43.1)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (43.1 - 33.77) / range_difference ; // each % point
             var difference = sum - 33.77 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 43.1 && sum < 51.04)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (51.04 - 43.1) / range_difference ; // each % point
             var difference = sum - 43.1 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 51.04 && sum < 64.01)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (64.01 - 51.04) / range_difference ; // each % point
             var difference = sum - 51.04 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 64.01 && sum < 70.9)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (70.9 - 64.01) / range_difference ; // each % point
             var difference = sum - 64.01 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 70.9 && sum < 77.96)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (77.96 - 70.9) / range_difference ; // each % point
             var difference = sum - 70.9 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 77.96 && sum < 83.28)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (83.28 - 77.96) / range_difference ; // each % point
             var difference = sum - 77.96 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 83.28 && sum < 89.89)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (89.89 - 83.28) / range_difference ; // each % point
             var difference = sum - 83.28 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 89.89 && sum < 95.18)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (95.18 - 89.89) / range_difference ; // each % point
             var difference = sum - 89.89 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 95.18 && sum < 103.84)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (103.84 - 95.18) / range_difference ; // each % point
             var difference = sum - 95.18 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 103.84 && sum < 112.40)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (112.40 - 103.84) / range_difference ; // each % point
             var difference = sum - 103.84 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 112.40 && sum < 119.71)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (119.71 - 112.40) / range_difference ; // each % point
             var difference = sum - 112.40 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 119.71 && sum < 128.41)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (128.41 - 119.71) / range_difference ; // each % point
             var difference = sum - 119.71 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 128.41 && sum < 132.72)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (132.72 - 128.41) / range_difference ; // each % point
             var difference = sum - 128.41 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 132.72)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (132.72 - 132.72) / range_difference ; // each % point
             var difference = sum - 132.72 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "60-69")
        {
          if(sum >= 26.71 && sum < 33.95)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (33.95 - 26.71) / range_difference ; // each % point
             var difference = sum - 26.71 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 33.95 && sum < 44.45)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (44.45 - 33.95) / range_difference ; // each % point
             var difference = sum - 33.95 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 44.45 && sum < 51.96)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (51.96 - 44.45) / range_difference ; // each % point
             var difference = sum - 44.45 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 51.96 && sum < 59.73)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (59.73 - 51.96) / range_difference ; // each % point
             var difference = sum - 51.96 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 59.73 && sum < 67.25)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (67.25 - 59.73) / range_difference ; // each % point
             var difference = sum - 59.73 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 67.25 && sum < 74.02)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (74.02 - 67.25) / range_difference ; // each % point
             var difference = sum - 67.25 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 74.02 && sum < 80.26)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (80.26 - 74.02) / range_difference ; // each % point
             var difference = sum - 74.02 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 80.26 && sum < 86.59)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (86.59 - 80.26) / range_difference ; // each % point
             var difference = sum - 80.26 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 86.59 && sum < 93.27)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (93.27 - 86.59) / range_difference ; // each % point
             var difference = sum - 86.59 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 93.27 && sum < 100.25)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (100.25 - 93.27) / range_difference ; // each % point
             var difference = sum - 93.27 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 100.25 && sum < 109.56)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (109.56 - 100.25) / range_difference ; // each % point
             var difference = sum - 100.25 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 109.56 && sum < 115.45)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (115.45 - 109.56) / range_difference ; // each % point
             var difference = sum - 109.56 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 115.45 && sum < 122.25)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (122.25 - 115.45) / range_difference ; // each % point
             var difference = sum - 115.45 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 122.25 && sum < 128.47)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (128.47 - 122.25) / range_difference ; // each % point
             var difference = sum - 122.25 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 128.47)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (128.47 - 128.47) / range_difference ; // each % point
             var difference = sum - 128.47 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
        else if(document.getElementById("age_range").value == "70+")
        {
          if(sum >= 25.18 && sum < 29.13)
          {  
             var lower_rank = 0 ; // 0%
             var upper_rank = 1 ; // 1%
             var range_difference = upper_rank - lower_rank  ; // difference between 0% and 1%
             var percent_point = (29.13 - 25.18) / range_difference ; // each % point
             var difference = sum - 25.18 ; // sum greater than 0% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 29.13 && sum < 36.5)
          {  
             var lower_rank = 1 ; // 1%
             var upper_rank = 2 ; // 2%
             var range_difference = upper_rank - lower_rank  ; // difference between 1% and 2%
             var percent_point = (36.5 - 29.13) / range_difference ; // each % point
             var difference = sum - 29.13 ; // sum greater than 1 value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          } 
          else if(sum >= 36.5 && sum < 42.76)
          {  
             var lower_rank = 2 ; // 2%
             var upper_rank = 5 ; // 5%
             var range_difference = upper_rank - lower_rank  ; // difference between 2% and 5%
             var percent_point = (42.76 - 36.5) / range_difference ; // each % point
             var difference = sum - 36.5 ; // sum greater than 2% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 42.76 && sum < 52.11)
          {  
             var lower_rank = 5 ; // 5%
             var upper_rank = 10 ; // 10%
             var range_difference = upper_rank - lower_rank  ; // difference between 5% and 10%
             var percent_point = (52.11 - 42.76) / range_difference ; // each % point
             var difference = sum - 42.76 ; // sum greater than 5% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 52.11 && sum < 59.57)
          {  
             var lower_rank = 10 ; // 10%
             var upper_rank = 20 ; // 20%
             var range_difference = upper_rank - lower_rank  ; // difference between 10% and 20%
             var percent_point = (59.57 - 52.11) / range_difference ; // each % point
             var difference = sum - 52.11 ; // sum greater than 10% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 59.57 && sum < 65.4)
          {  
             var lower_rank = 20 ; // 20%
             var upper_rank = 30 ; // 30%
             var range_difference = upper_rank - lower_rank  ; // difference between 20% and 30%
             var percent_point = (65.4 - 59.57) / range_difference ; // each % point
             var difference = sum - 59.57 ; // sum greater than 20% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 65.4 && sum < 71.32)
          {  
             var lower_rank = 30 ; // 30%
             var upper_rank = 40 ; // 40%
             var range_difference = upper_rank - lower_rank  ; // difference between 30% and 40%
             var percent_point = (71.32 - 65.4) / range_difference ; // each % point
             var difference = sum - 65.4 ; // sum greater than 30% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 71.32 && sum < 77.36)
          {  
             var lower_rank = 40 ; // 40%
             var upper_rank = 50 ; // 50%
             var range_difference = upper_rank - lower_rank  ; // difference between 40% and 50%
             var percent_point = (77.36 - 71.32) / range_difference ; // each % point
             var difference = sum - 71.32 ; // sum greater than 40% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 77.36 && sum < 84.24)
          {  
             var lower_rank = 50 ; // 50%
             var upper_rank = 60 ; // 60%
             var range_difference = upper_rank - lower_rank  ; // difference between 50% and 60%
             var percent_point = (84.24 - 77.36) / range_difference ; // each % point
             var difference = sum - 77.36 ; // sum greater than 50% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 84.24 && sum < 92.08)
          {  
             var lower_rank = 60 ; // 60%
             var upper_rank = 70 ; // 70%
             var range_difference = upper_rank - lower_rank  ; // difference between 60% and 70%
             var percent_point = (92.08 - 84.24) / range_difference ; // each % point
             var difference = sum - 84.24 ; // sum greater than 60% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }    
          else if(sum >= 92.08 && sum < 100.93)
          {  
             var lower_rank = 70 ; // 70%
             var upper_rank = 80 ; // 80%
             var range_difference = upper_rank - lower_rank  ; // difference between 70% and 80%
             var percent_point = (100.93 - 92.08) / range_difference ; // each % point
             var difference = sum - 92.08 ; // sum greater than 70% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 100.93 && sum < 108.45)
          {  
             var lower_rank = 80 ; // 80%
             var upper_rank = 90 ; // 90%
             var range_difference = upper_rank - lower_rank  ; // difference between 80% and 90%
             var percent_point = (108.45 - 100.93) / range_difference ; // each % point
             var difference = sum - 100.93 ; // sum greater than 80% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 108.45 && sum < 114.11)
          {  
             var lower_rank = 90 ; // 90%
             var upper_rank = 95 ; // 95%
             var range_difference = upper_rank - lower_rank  ; // difference between 90% and 95%
             var percent_point = (114.11 - 108.45) / range_difference ; // each % point
             var difference = sum - 108.45 ; // sum greater than 90% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 114.11 && sum < 117.67)
          {  
             var lower_rank = 95 ; // 95%
             var upper_rank = 98 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (117.67 - 114.11) / range_difference ; // each % point
             var difference = sum - 114.11 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
          else if(sum >= 117.67)
          {  
             var lower_rank = 98 ; // 95%
             var upper_rank = 99 ; // 98%
             var range_difference = upper_rank - lower_rank  ; // difference between 95% and 98%
             var percent_point = (117.67 - 117.67) / range_difference ; // each % point
             var difference = sum - 117.67 ; // sum greater than 95% value
             var rank_difference = difference / percent_point ;
             var rank = lower_rank + rank_difference ;
          }
        }
    }  
        
           document.getElementById("percentile").value = Math.round(parseFloat(rank) * 10) / 10 ; 
        
        // Plot range percentile
            document.getElementById("plot1").setAttribute('style', "visibility:hidden");
            document.getElementById("plot2").setAttribute('style', "visibility:hidden");
            document.getElementById("plot3").setAttribute('style', "visibility:hidden");
            document.getElementById("plot4").setAttribute('style', "visibility:hidden");
            document.getElementById("plot5").setAttribute('style', "visibility:hidden");
            document.getElementById("plot6").setAttribute('style', "visibility:hidden");
            document.getElementById("plot7").setAttribute('style', "visibility:hidden");
            document.getElementById("plot8").setAttribute('style', "visibility:hidden");
            document.getElementById("plot9").setAttribute('style', "visibility:hidden");
            document.getElementById("plot10").setAttribute('style', "visibility:hidden");
            document.getElementById("plot11").setAttribute('style', "visibility:hidden");
            document.getElementById("plot12").setAttribute('style', "visibility:hidden");
            document.getElementById("plot13").setAttribute('style', "visibility:hidden");
            document.getElementById("plot14").setAttribute('style', "visibility:hidden");
            document.getElementById("plot15").setAttribute('style', "visibility:hidden");
            document.getElementById("plot16").setAttribute('style', "visibility:hidden");
            document.getElementById("plot17").setAttribute('style', "visibility:hidden");
            document.getElementById("plot18").setAttribute('style', "visibility:hidden");
            document.getElementById("plot19").setAttribute('style', "visibility:hidden");
            document.getElementById("plot20").setAttribute('style', "visibility:hidden");
            document.getElementById("plot21").setAttribute('style', "visibility:hidden");
            document.getElementById("plot22").setAttribute('style', "visibility:hidden");
            document.getElementById("plot23").setAttribute('style', "visibility:hidden");
            document.getElementById("plot24").setAttribute('style', "visibility:hidden");
            document.getElementById("plot25").setAttribute('style', "visibility:hidden");
            
            
            if(document.getElementById("percentile").value > 0 && document.getElementById("percentile").value <= 1)
                {              
                  document.getElementById('plot1').removeAttribute('style');                     
                }
            else if(document.getElementById("percentile").value > 1 && document.getElementById("percentile").value < 5)
                {              
                  document.getElementById('plot2').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value == 5)
                {              
                  document.getElementById('plot3').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 5 && document.getElementById("percentile").value < 25)
                {              
                  document.getElementById('plot4').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 25)
                {              
                  document.getElementById('plot5').removeAttribute('style'); 
                }                   
            else if(document.getElementById("percentile").value > 25 && document.getElementById("percentile").value <= 38)
                {              
                  document.getElementById('plot6').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 38 && document.getElementById("percentile").value < 50)
                {              
                  document.getElementById('plot7').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 50)
                {              
                  document.getElementById('plot8').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 50 && document.getElementById("percentile").value <= 63)
                {              
                  document.getElementById('plot9').removeAttribute('style'); 
                }                
            else if(document.getElementById("percentile").value > 63 && document.getElementById("percentile").value < 75)
                {              
                  document.getElementById('plot10').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 75)
                {              
                  document.getElementById('plot11').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 75 && document.getElementById("percentile").value <= 79)
                {              
                  document.getElementById('plot12').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 79 && document.getElementById("percentile").value <= 83)
                {              
                  document.getElementById('plot13').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 83 && document.getElementById("percentile").value <= 87)
                {              
                  document.getElementById('plot14').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 87 && document.getElementById("percentile").value < 90)
                {              
                  document.getElementById('plot15').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 90)
                {              
                  document.getElementById('plot16').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 90 && document.getElementById("percentile").value <= 91)
                {              
                  document.getElementById('plot17').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 91 && document.getElementById("percentile").value <= 92)
                {              
                  document.getElementById('plot18').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 92 && document.getElementById("percentile").value <= 93)
                {              
                  document.getElementById('plot19').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 93 && document.getElementById("percentile").value < 94)
                {              
                  document.getElementById('plot20').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 94)
                {              
                  document.getElementById('plot21').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value > 94 && document.getElementById("percentile").value < 95)
                {              
                  document.getElementById('plot22').removeAttribute('style'); 
                }    
            else if(document.getElementById("percentile").value == 95)
                {              
                  document.getElementById('plot23').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 95 && document.getElementById("percentile").value <= 97)
                {              
                  document.getElementById('plot24').removeAttribute('style'); 
                }
            else if(document.getElementById("percentile").value > 97 && document.getElementById("percentile").value <= 100)
                {              
                  document.getElementById('plot25').removeAttribute('style'); 
                }    
         
       });          
        
         $(document).on('click','#four_Sites', function(){
                
                    var last4 = document.getElementById("four_last").value ;
                    
                    if(last4 !== "")
                    {    
                        document.getElementById("four_Sum").value = parseFloat(last4) ; 
                    }    
                        document.getElementById("sumSites").value = 4 ;                    
                        document.getElementById("sum4").removeAttribute('style'); 

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
               
        });
        
        $(document).on('click','#five_Sites', function(){
            
                    var last5 = document.getElementById("five_last").value ;
                    
                    if(last5 !== "")
                    {    
                        document.getElementById("five_Sum").value = parseFloat(last5) ; 
                    }               
                        document.getElementById("sumSites").value = 5 ;                    
                        document.getElementById("sum5").removeAttribute('style'); 

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
              
        });
        
        $(document).on('click','#six_Sites', function(){
            
                     var last6 = document.getElementById("six_last").value ;
                    
                     if(last6 !== "")
                     {    
                        document.getElementById("six_Sum").value = parseFloat(last6) ;
                     } 
                     
                        document.getElementById("sumSites").value = 6 ;                    
                        document.getElementById("sum6").removeAttribute('style'); 

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum7").setAttribute('style', "display:none");
                        document.getElementById("seven_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");                    

                        getSumSkinfolds() ; 
                       
        });
        
        $(document).on('click','#seven_Sites', function(){
                              
                     var last7 = document.getElementById("seven_last").value ;         
                     
                     if(last7 !== "")
                     {    
                        document.getElementById("seven_Sum").value = parseFloat(last7) ; 
                     } 
                    
                        document.getElementById("sumSites").value = 7 ;                    
                        document.getElementById("sum7").removeAttribute('style'); 

                        document.getElementById("sum4").setAttribute('style', "display:none");
                        document.getElementById("four_Sum").value = "" ;

                        document.getElementById("sum5").setAttribute('style', "display:none");
                        document.getElementById("five_Sum").value = "" ;

                        document.getElementById("sum6").setAttribute('style', "display:none");
                        document.getElementById("six_Sum").value = "" ;

                        document.getElementById("sum8").setAttribute('style', "display:none");

                        getSumSkinfolds() ; 
             
        });
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = 8 ; 
            document.getElementById("sum8").removeAttribute('style');
            
            document.getElementById("sum4").setAttribute('style', "display:none"); 
            document.getElementById("four_Sum").value = "" ;
            
            document.getElementById("sum5").setAttribute('style', "display:none");
            document.getElementById("five_Sum").value = "" ;
            
            document.getElementById("sum6").setAttribute('style', "display:none");
            document.getElementById("six_Sum").value = "" ;
            
            document.getElementById("sum7").setAttribute('style', "display:none");
            document.getElementById("seven_Sum").value = "" ;
            
            getSumSkinfolds() ;
        });   
        
        $(document).on('click','#plot', function(){
             location.href = '<?php echo site_url();?>/Body/skinfold_Map?profile=full&triceps=<?php echo $skinfolds_Values["triceps"];?>&subscapular=<?php echo $skinfolds_Values["subscapular"];?>&illiac=<?php echo $skinfolds_Values["iliac_crest"];?>&thigh=<?php echo $skinfolds_Values["thigh"];?>&supraspinale=<?php echo $skinfolds_Values["supraspinale"];?>&biceps=<?php echo $skinfolds_Values["biceps"];?>&abdominal=<?php echo $skinfolds_Values["abdominal"];?>&calf=<?php echo $skinfolds_Values["calf"];?>&height=<?php echo $skinfolds_Values["height"];?>&body_mass=<?php echo $skinfolds_Values["body_mass"];?>' ;
        });
        
	$(document).on('click','.info_icon_btn', function(){
	    $(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
        
        function changeState(e)
            {  
               if(e.className == 'red_circle') {
                    e.className = 'green_circle';
                } else {
                    e.className = 'red_circle';
                }
                getSumSkinfolds() ;
            }
        
</script>  



<script>   
//calculate %Body Fat value
    function getSumSkinfolds()
    { 
        var ageValue = document.getElementById("age").value;      
      
        var triceps = document.getElementById("triceps").value ;            
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;  
        var iliac = document.getElementById("iliac_crest").value ;  
        var supraspinale = document.getElementById("supraspinale").value ;  
        var abdominal = document.getElementById("abdominal").value ;  
        var thigh = document.getElementById("thigh").value ;  
        var calf = document.getElementById("calf").value ;
        var mid_axilla = document.getElementById("mid_axilla").value ;
        
        
        var sumSites = document.getElementById("sumSites").value ;
             
        var sites = [] ;
        $('.green_circle').filter(function(){                   
           if(!($(this).attr("value") == undefined))
            {               
             sites.push($(this).attr("value")) ; 
            }                        
          });
                  
            if(sumSites == 4)
            {       
                if(sites.length == 4)
                {
                  var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3];   
                  var sum4Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ;
                 
                  document.getElementById("four_Sum").value = Math.round(parseFloat(sum4Skinfold) * 10) / 10 ; // display Sum total of selected sites    
                  document.getElementById("four_last").value = Math.round(parseFloat(sum4Skinfold) * 10) / 10 ;
                }                  
            }
            else if(sumSites == 5)
            {   
                if(sites.length == 5)
                {
                  var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4];   
                  var sum5Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) ;
        
                  document.getElementById("five_Sum").value = Math.round(sum5Skinfold * 10) / 10 ;  // display Sum total of selected sites
                  document.getElementById("five_last").value = Math.round(sum5Skinfold * 10) / 10 ;
                }          
            }
            else if(sumSites == 6)
            {   
                if(sites.length == 6)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5];   
                 var sum6Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) ;
        
                 document.getElementById("six_Sum").value = Math.round(sum6Skinfold * 10) / 10 ; // display Sum total of selected sites 
                 document.getElementById("six_last").value = Math.round(sum6Skinfold * 10) / 10 ;
                } 
            }
            else if(sumSites == 7)
            {   
                if(sites.length == 7)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5]; var x7 = sites[6];   
                 var sum7Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) + parseFloat(x7) ;
        
                 document.getElementById("seven_Sum").value = Math.round(sum7Skinfold * 10) / 10 ;  // display Sum total of selected sites 
                 document.getElementById("seven_last").value = Math.round(sum7Skinfold * 10) / 10 ; 
             } 
            }
            else if(sumSites == 8)
            {    
                if(sites.length == 8)
                {
                 var x1 = sites[0]; var x2 = sites[1]; var x3 = sites[2]; var x4 = sites[3]; var x5 = sites[4]; var x6 = sites[5]; var x7 = sites[6]; var x8 = sites[7];   
                 var sum8Skinfold = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) + parseFloat(x5) + parseFloat(x6) + parseFloat(x7) + parseFloat(x8) ;
        
                 document.getElementById("eight_Sum").value = Math.round(sum8Skinfold * 10) / 10 ; // display Sum total of selected sites 
                 document.getElementById("eight_last").value = Math.round(sum8Skinfold * 10) / 10 ;
               }
            }
            
               
        }
  

    
</script>
    
</body>
</html>
