<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<script type="text/javascript">
	
	window.onload = function() {
	   changeMean();
	};	
</script> 
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$health_logo" ;?>" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientPeakPowerInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?> 	
	
    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="4" align="center" class="name_field_td position_r">
                <label for="name">Name</label> <input type="text" name="name" id="name" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
                <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a> 
                <a href="#" class="info_icon_btn"><img src="<?php echo "$base/$image"?>/info_icon.png" style="float:right;"> </a>
                <div class="info_block">
                    <p>Peak power is the explosive power measured during 6 s cycle ergometer test. The units of measurement include the highest value recorded during the test or peak power [Watts], and also the peak adjusted for body weight [Watts per kilogram]. This is shown on the graph. Also shown in the percentile rank for the Watts/kg variable. Scores that are further to the right on the graph are ranked higher against the database norms indicating better performance.
                       Peak power is strongly correlated with muscle size and percentage of fast-twitch fibres in the leg muscles. It develops during adolescence reaching the highest levels by about the mid 20's. After this time most people deteriorate at a rate, on average, of about 8-10% per decade. High-intensity training, including resistance work, can help increase power and decrease the rate of deterioration with age.                      
                    </p>
                    <p>The RESULTS screen for peak power shows the client's ranking and the probability distribution for peak power when expressed in Watts per kg body weight. The user should enter both the body weight and peak power [Watts] achieved on the bicycle ergometer. Peak power [Watts/kg] will then be automatically calculated and plotted on the graph after the user clicks 'Plot'. 
                       If the client already has their data in Watts/kg then this can be entered directly in the field 'Watts/kg' and it will be plotted after clicking 'plot'. Normative data are provided for different age groups for both males and females as well as for high-performance athlete across a range of sports.                        
                    </p>
                </div>  
              </td>
            </tr>
            <tr>
              <td align="center" width="25%">
                <label>Test</label>
                <select name="test" onchange="this.form.submit()">
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic strength and power</option>                   
                    <option value="vertical_jump">Vertical jump [cm] test</option>                  
                    <option value="flight_time">Flight : Contact time test</option> 
                    <option value="peak_power" selected="selected">Peak power [W] test</option>
                    <option value="strength">Strength test</option>
                    <option value="#"></option>
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic capacity</option>                   
                    <option value="30s_total_work_done">30 s total work [kJ] test</option>          
                    <option value="#"></option>
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Aerobic fitness</option>                   
                    <option value="#" style="font-weight:bold;">VO&#x0032;&#x006D;&#x0061;&#x0078; [mL/kg/min]</option>                                     
                    <option value="3x3min_submaximal_test">3 x 3 min submaximal test</option> 
                    <option value="shuttle_test">20 m shuttle test</option>
                    <option value="v02max_test">Measured VO&#x0032;&#x006D;&#x0061;&#x0078; test</option> 
                    <option value="lactate_threshold" style="font-weight:bold;">Lactate threshold test</option>
                </select>
              </td>
              <td align="center" width="25%">
                <label>Gender</label>
                <select id="gender" name="gender" onchange="changeMean();">
                    <option value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "Selected";}?>>male</option>
                    <option value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "Selected";}?>>female</option>
                </select>
              </td>
              
              <script type="text/javascript">               
                   $("#gender").change(function ()
                    {
                        var selectedText = $(this).find("option:selected").text();
                        var selectedValue = $(this).val();
                      
                         if(selectedValue=='Male')
                         {
                                $.ajax({
                                type:'POST',
                                url: "<?php echo base_url('index.php/Fitness/get_view_ajax_male/');?>",                   
                                 success: function (response) {
                                    document.getElementById("sport").innerHTML=response; 
                                  }
                                }); 
                             }else{
                               $.ajax({
                                type:'POST',
                                url: "<?php echo base_url('index.php/Fitness/get_view_ajax_female/');?>",                    
                                success: function (response) {
                                    document.getElementById("sport").innerHTML=response; 
                                  }
                                }); 
                        }

                    });
         
               </script>
              
              
              <td align="center" width="25%">
                <label>Age (yr)</label>
                <select id="age" name="age" onchange="changeMean();">
                    <?php foreach($age_array as $age){ ?>
                    <option value="<?php echo $age->age ;?>" <?php if($fieldData[0]->age == $age->age || $_SESSION['age_range'] == $age->age){echo "Selected";}?>><?php echo $age->age ;?></option>                   
                   <?php } ?>
                </select>
              </td>
              <td align="center" width="25%">
                <label>Comparison Sport</label>
                <select name="sport" id="sport" onchange="changeSportMean()">                  
                        <option>Sports</option>
                         <?php  
                            foreach($sports_array as $sport){ ?>
                            <option value="<?php echo $sport->sport ;?>" <?php if($fieldData[0]->sport == $sport->sport){echo "Selected";}?>><?php echo $sport->sport ;?></option>              
                         <?php } ?>            
                </select>
              </td>
            </tr>
            
            <tr>
            	<td colspan="4" class="sml_text_field">
                    <label class="ml_7">Body weight</label><input class="cus-input" type="text" name="weight" id="weight" value="<?php echo isset($fieldData[0]->weight)?$fieldData[0]->weight:""; ?>"> kg
                </td>
            </tr> 
			
			<tr>
            	<td colspan="4">
                    <table> 
						<tr>
						  <td align="right" class="sml_text_field" width="248px"><label for="value">Value</label><input class="cus-input" type="text" name="peak_value" id="peak_value" onKeyUp="peakValue()" value="<?php echo isset($fieldData[0]->peak_value)?$fieldData[0]->peak_value:""; ?>"> Watts</td>
						  <td align="right" class="sml_text_field" width="255px"><input type="text" name="vo2Max_value" id="vo2Max_value" readonly="readonly" value="<?php echo isset($fieldData[0]->value)?$fieldData[0]->value:""; ?>"> Watts/kg</td>
						  <td align="right" class="sml_text_field" width="342px"><label for="percentile">Performance percentile</label><input type="text" name="percentile" id="percentile" readonly="readonly" value="<?php echo isset($fieldData[0]->percentile)?$fieldData[0]->percentile:""; ?>"> %</td>
						</tr> 
					</table>
                </td>
            </tr>
            
              
            <tr>      
              <td colspan="4" valign="bottom">
                  <input id="plot" name="plot" type="button" onclick="show()" class="plot_btn" value="Plot"/>
                   <input id="scorePlot" name="scorePlot" type="hidden" value="<?php echo isset($fieldData[0]->scorePlot)?$fieldData[0]->scorePlot:""; ?>"/>  
                  <input id="sportPlot" name="sportPlot" type="hidden" value="<?php echo isset($fieldData[0]->sportPlot)?$fieldData[0]->sportPlot:""; ?>"/>  
                  <input id="sportMeanScore" name="sportMeanScore" type="hidden" value="<?php echo isset($fieldData[0]->sportMeanScore)?$fieldData[0]->sportMeanScore:""; ?>"/> 
                  
                <div class="graph">
                	<ul class="verticle_lines">                        
                        <li><span id="class1">~24.12</span></li> 
                        <li><span id="class2">~23.42</span></li> 
                        <li><span id="class3">~22.64</span></li> 
                        <li><span id="class4">~22.39</span></li> 
                        <li><span id="class5">~21.77</span></li> 
                        <li><span id="class6">~21.49</span></li> 
                        <li><span id="class7">~21.19</span></li> 
                        <li><span id="class8">~20.77</span></li> 
                        <li><span id="class9">~20.43</span></li> 
                        <li><span id="class10">~19.95</span></li> 
                        <li><span id="class11">~19.55</span></li> 
                        <li><span id="class12">~19.12</span></li> 
                        <li><span id="class13">~18.49</span></li> 
                        <li><span id="class14">~17.89</span></li>
                        <li><span id="class15">~17.19</span></li>                        
                        <li>
                          <div class="graph_dashed_line"><div class="graph_dashed_line">&nbsp;</div> <span id="meanVal" class="graph_dash_text">15.86</span> <span>average</span>                            
                            <div class="custom1"><div id="plot16_1" class="blue_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_1" class="red_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_2" class="blue_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_2" class="red_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_3" class="blue_dot" style="display:none; left:80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_3" class="red_dot" style="display:none; left:80px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom2"><div id="plot17" class="blue_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom5"><div id="score17" class="red_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_8" class="blue_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_8" class="red_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_7" class="blue_dot" style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_7" class="red_dot"  style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_6" class="blue_dot" style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_6" class="red_dot"  style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_5" class="blue_dot" style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_5" class="red_dot"  style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_4" class="blue_dot" style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_4" class="red_dot"  style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_3" class="blue_dot" style="display:none; left:-110px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_3" class="red_dot" style="display:none; left:-110px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_2" class="blue_dot" style="display:none; left:-150px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_2" class="red_dot" style="display:none; left:-150px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_1" class="blue_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_1" class="red_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                          </div>
                        </li>                        
                        </ul>
                    
                        <ul class="bottom_values">
                            <?php $plot=1 ; foreach($prob_array as $val){?>
                            <li><span><?php echo $val->prob_range ;?></span> <div id="<?php echo "plot".$plot ; ?>" class="blue_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div><div id="<?php echo "score".$plot ; ?>" class="red_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></li>
                            <?php $plot++ ; }?>
                        </ul>
                            
                       <div class="score_box">
                            <div class="score_text">
                                    your<br/>score<br>
                                    <div style="margin-top: 10px;">Sport<br/>mean<br/><div id="sportMeanVal"></div></div> 
                            </div>                            
                            <!--<div class="red_dot"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div>-->
                        </div> 
                       
                       
                    <div class="graph_pulse_box">
                    	<span>frequency</span>
                    	<img src="<?php echo "$base/$image"?>/pulse.png" alt=""/> 
                        <div class="pulse_text" style="right: 80px;">Peak power [Watts/kg]</div>
                    </div>
                </div>
              </td>
              </tr>
          </tbody>
        </table>
    </div>
	
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

	$(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
    });
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script> 
<script> 
    
  function peakValue()
  {     
     //  document.getElementById("vo2Max_value").value ;
     var peak_val = document.getElementById("peak_value").value ;     
     var weight =  document.getElementById("weight").value ;
     
     var result_val = (parseFloat(peak_val) / parseFloat(weight)) ;
     
     if(isNaN(result_val)){            
            document.getElementById("vo2Max_value").value = "" ;  
        }
     else
     {
        document.getElementById("vo2Max_value").value = Math.round(parseFloat(result_val) * 100) / 100 ; 
     }
     
     getZscore() ;
  }
    
    function changeMean()
    { 
        var ageValue = document.getElementById("age").value;      
      
        if(document.getElementById('gender').value == "Male") {
                
        if(ageValue == "18-29")
        {
            var mean = 14.86 ;
            var sd = 2.59 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 13.38 ;
           var sd = 2.25 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 12.24 ;
           var sd = 2.05 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 11.15 ;
           var sd = 1.86 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 10.03 ;
           var sd = 1.70 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 8.13 ;
           var sd = 1.58 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 10.69 ;
            var sd = 2.09 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 9.85 ;
           var sd = 1.80 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 9.05 ;
           var sd = 1.60 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 8.00 ;
           var sd = 1.54 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 6.27 ;
           var sd = 1.59 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 5.14 ;
           var sd = 1.50 ;        
      }               
        
    }    
    
    var r1 = (5.2000 * sd) + mean ;
    var r2 = (4.7550 * sd) + mean ;
    var r3 = (4.2650 * sd) + mean ;
    var r4 = (4.1100 * sd) + mean ;
    var r5 = (3.7200 * sd) + mean ;
    var r6 = (3.5410 * sd) + mean ;
    var r7 = (3.3550 * sd) + mean ;
    var r8 = (3.0900 * sd) + mean ;
    var r9 = (2.8785 * sd) + mean ;
    var r10 = (2.5760 * sd) + mean ;
    var r11 = (2.3270 * sd) + mean ;
    var r12 = (2.0550 * sd) + mean ;
    var r13 = (1.6600 * sd) + mean ;
    var r14 = (1.2816 * sd) + mean ;
    var r15 = (0.8420 * sd) + mean ;
     
      document.getElementById('meanVal').innerHTML = parseFloat(mean) ;   
     
     
        document.getElementById('class1').innerHTML = "~"+Math.floor(parseFloat(r1) * 100) / 100 ;
        document.getElementById('class2').innerHTML = "~"+Math.floor(parseFloat(r2) * 100) / 100 ;
        document.getElementById('class3').innerHTML = "~"+Math.floor(parseFloat(r3) * 100) / 100 ;
        document.getElementById('class4').innerHTML = "~"+Math.floor(parseFloat(r4) * 100) / 100 ; 
        document.getElementById('class5').innerHTML = "~"+Math.floor(parseFloat(r5) * 100) / 100 ;
        document.getElementById('class6').innerHTML = "~"+Math.floor(parseFloat(r6) * 100) / 100 ;
        document.getElementById('class7').innerHTML = "~"+Math.floor(parseFloat(r7) * 100) / 100 ;
        document.getElementById('class8').innerHTML = "~"+Math.floor(parseFloat(r8) * 100) / 100 ;
        document.getElementById('class9').innerHTML = "~"+Math.floor(parseFloat(r9) * 100) / 100 ;
        document.getElementById('class10').innerHTML = "~"+Math.floor(parseFloat(r10) * 100) / 100;
        document.getElementById('class11').innerHTML = "~"+Math.floor(parseFloat(r11) * 100) / 100 ;
        document.getElementById('class12').innerHTML = "~"+Math.floor(parseFloat(r12) * 100) / 100 ;
        document.getElementById('class13').innerHTML = "~"+Math.floor(parseFloat(r13) * 100) / 100 ;
        document.getElementById('class14').innerHTML = "~"+Math.floor(parseFloat(r14) * 100) / 100 ;
        document.getElementById('class15').innerHTML = "~"+Math.floor(parseFloat(r15) * 100) / 100 ; 
   

     
     getZscore() ;
    // show() ;
      
  }
    
  //calculate z-score value
    function getZscore()
    { 
        var ageValue = document.getElementById("age").value;
        var vo2MAxVal = document.getElementById("vo2Max_value");
        var vo2Max = vo2MAxVal.value;
      
        
        var div1=document.getElementById('plot1');
        var div2=document.getElementById('plot2');
        var div3=document.getElementById('plot3');
        var div4=document.getElementById('plot4');
        var div5=document.getElementById('plot5');
        var div6=document.getElementById('plot6');
        var div7=document.getElementById('plot7');
        var div8=document.getElementById('plot8');
        var div9=document.getElementById('plot9');
        var div10=document.getElementById('plot10');
        var div11=document.getElementById('plot11');
        var div12=document.getElementById('plot12');
        var div13=document.getElementById('plot13');
        var div14=document.getElementById('plot14');
        var div15=document.getElementById('plot15');
        var div16_1=document.getElementById('plot16_1');
        var div16_2=document.getElementById('plot16_2');
        var div16_3=document.getElementById('plot16_3');
        var div17=document.getElementById('plot17');
        var div18_8=document.getElementById('plot18_8');
        var div18_7=document.getElementById('plot18_7');
        var div18_6=document.getElementById('plot18_6');
        var div18_5=document.getElementById('plot18_5');
        var div18_4=document.getElementById('plot18_4');
        var div18_3=document.getElementById('plot18_3');
        var div18_2=document.getElementById('plot18_2');
        var div18_1=document.getElementById('plot18_1');
        
         if(div1.style.display == "block")
        {
            div1.style.display = "none"
        }
        else if(div2.style.display == "block")
        {
            div2.style.display = "none"
        }
        else if(div3.style.display == "block")
        {
            div3.style.display = "none"
        }
        else if(div4.style.display == "block")
        {
            div4.style.display = "none"
        }
        else if(div5.style.display == "block")
        {
            div5.style.display = "none"
        }
        else if(div6.style.display == "block")
        {
            div6.style.display = "none"
        }
        else if(div7.style.display == "block")
        {
            div7.style.display = "none"
        }
        else if(div8.style.display == "block")
        {
            div8.style.display = "none"
        }
        else if(div9.style.display == "block")
        {
            div9.style.display = "none"
        }
        else if(div10.style.display == "block")
        {
            div10.style.display = "none"
        }
        else if(div11.style.display == "block")
        {
            div11.style.display = "none"
        }
        else if(div12.style.display == "block")
        {
            div12.style.display = "none"
        }
        else if(div13.style.display == "block")
        {
            div13.style.display = "none"
        }
        else if(div14.style.display == "block")
        {
            div14.style.display = "none"
        }
        else if(div15.style.display == "block")
        {
            div15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        } 
        
        
      
        if(document.getElementById('gender').value == "Male") {
                
        if(ageValue == "18-29")
        {
            var mean = 15.86 ;
            var sd = 1.59 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 14.38 ;
           var sd = 1.25 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 13.24 ;
           var sd = 1.05 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 11.65 ;
           var sd = 0.86 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 10.03 ;
           var sd = 0.70 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 8.53 ;
           var sd = 0.58 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 13.89 ;
            var sd = 1.39 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 12.65 ;
           var sd = 1.10 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 11.45 ;
           var sd = 0.90 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 10.00 ;
           var sd = 0.74 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 8.47 ;
           var sd = 0.59 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 7.34 ;
           var sd = 0.50 ;        
      }               
        
    }    
          
     var z_score = (parseFloat(vo2Max) - parseFloat(mean)) / parseFloat(sd) ;    
      if(isNaN(z_score)){            
            document.getElementById("percentile").value = "" ;  
        }
        else
        {
           z_score = Math.floor(parseFloat(z_score) * 1000) / 1000 ;
            
           // NORMDIST for z-score 
           var perform_percent = normalcdf(z_score);
           
            if(isNaN(parseFloat(perform_percent * 100)))
            {              
               document.getElementById("percentile").value = "" ;
             }
             else
             {                
                 document.getElementById("percentile").value = Math.floor(parseFloat(perform_percent * 100) * 1000000) / 1000000 ;                   
                 
             } 
           
           if(document.getElementById("percentile").value < 48)
           {  
             if(document.getElementById("percentile").value >= 0 && document.getElementById("percentile").value < 7)
                {              
                   var scorePlot = '1 in 1_1' ;
                }
             else if(document.getElementById("percentile").value >= 7 && document.getElementById("percentile").value < 14)
                {              
                   var scorePlot = '1 in 1_2' ;
                }  
             else if(document.getElementById("percentile").value >= 14 && document.getElementById("percentile").value < 20)
                {              
                   var scorePlot = '1 in 1_3' ;
                }  
             else if(document.getElementById("percentile").value >= 20 && document.getElementById("percentile").value < 30)
                {              
                   var scorePlot = '1 in 1_4' ;
                }  
             else if(document.getElementById("percentile").value >= 30 && document.getElementById("percentile").value < 35)
                {              
                   var scorePlot = '1 in 1_5' ;
                }  
             else if(document.getElementById("percentile").value >= 35 && document.getElementById("percentile").value < 40)
                {              
                   var scorePlot = '1 in 1_6' ;
                }  
             else if(document.getElementById("percentile").value >= 40 && document.getElementById("percentile").value < 44)
                {              
                   var scorePlot = '1 in 1_7' ;
                }  
             else if(document.getElementById("percentile").value >= 44 && document.getElementById("percentile").value < 48 )
                {              
                   var scorePlot = '1 in 1_8' ;
                }                
           }
           else if(document.getElementById("percentile").value >= 48 && document.getElementById("percentile").value < 52)
           {              
              var scorePlot = '1 in 2' ;
           }          
           else if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 80)
           {              
               if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 60)
               {
                 var scorePlot = '1 in 3_1' ;   
               }
               else if(document.getElementById("percentile").value > 60 && document.getElementById("percentile").value <= 70)
               {
                 var scorePlot = '1 in 3_2' ;   
               }
               else if(document.getElementById("percentile").value > 70 && document.getElementById("percentile").value <= 80)
               {
                 var scorePlot = '1 in 3_3' ;   
               }
                
           }  
           else if(z_score > 0.8100 && z_score <= 0.8420)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 0.8420 && z_score <= 1.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.0100 && z_score <= 1.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.1100 && z_score <= 1.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.2100 && z_score <= 1.2816)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.2816 && z_score <= 1.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.4100 && z_score <= 1.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.5100 && z_score <= 1.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.6100 && z_score <= 1.6600)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.6600 && z_score <= 1.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.8100 && z_score <= 1.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.9100 && z_score <= 2.0550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.0550 && z_score <= 2.1100)
           {
              var perform_percent = 0.98257082 ;
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.1100 && z_score <= 2.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.2100 && z_score <= 2.3270)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.3270 && z_score <= 2.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.4100 && z_score <= 2.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.5100 && z_score <= 2.5760)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.5760 && z_score <= 2.7100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.7100 && z_score <= 2.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.8100 && z_score <= 2.8785)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 2.8785 && z_score <= 3.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 3.0100 && z_score <= 3.0900)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.0900 && z_score <= 3.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.2100 && z_score <= 3.3550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.3550 && z_score <= 3.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.4100 && z_score <= 3.5410)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.5410 && z_score <= 3.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.6100 && z_score <= 3.7200)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.7200 && z_score <= 3.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.8100 && z_score <= 3.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.9100 && z_score <= 4.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 4.0100 && z_score <= 4.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50,000' ;
           }
           else if(z_score > 4.1100 && z_score <= 4.2650)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.2650 && z_score <= 4.3100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.3100 && z_score <= 4.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.4100 && z_score <= 4.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.5100 && z_score <= 4.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.6100 && z_score <= 4.7550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.7550 && z_score <= 4.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.8100 && z_score <= 4.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.9100 && z_score <= 5.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.0100 && z_score <= 5.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.1100 && z_score <= 5.2000)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000,000' ;
           }
           else if(z_score > 5.2000)
           {
              var perform_percent = normalcdf(z_score);	                      
              var scorePlot = '1 in 10,000,000' ;
           }
           
           document.getElementById("scorePlot").value = scorePlot ;   
           
          
            
        }     
       
         changeSportMean() ;
    }
    
    function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    }   
    
    
        function changeSportMean()
    {   
        var sportValue = document.getElementById("sport").value ;           
        
        var testmean1 = document.getElementById('class1').innerHTML.replace('~', '')   ;
        var testmean2 = document.getElementById('class2').innerHTML.replace('~', '')   ;
        var testmean3 = document.getElementById('class3').innerHTML.replace('~', '')   ;
        var testmean4 = document.getElementById('class4').innerHTML.replace('~', '')   ; 
        var testmean5 = document.getElementById('class5').innerHTML.replace('~', '')   ;
        var testmean6 = document.getElementById('class6').innerHTML.replace('~', '')   ;
        var testmean7 = document.getElementById('class7').innerHTML.replace('~', '')   ;
        var testmean8 = document.getElementById('class8').innerHTML.replace('~', '')   ;
        var testmean9 = document.getElementById('class9').innerHTML.replace('~', '')   ;
        var testmean10 = document.getElementById('class10').innerHTML.replace('~', '')  ;
        var testmean11 = document.getElementById('class11').innerHTML.replace('~', '')  ;
        var testmean12 = document.getElementById('class12').innerHTML.replace('~', '')  ;
        var testmean13 = document.getElementById('class13').innerHTML.replace('~', '')  ;
        var testmean14 = document.getElementById('class14').innerHTML.replace('~', '')  ;
        var testmean15 = document.getElementById('class15').innerHTML.replace('~', '')  ;        
        var testmean = document.getElementById('meanVal').innerHTML   ;
        
       
        var divsport1=document.getElementById('score1');
        var divsport2=document.getElementById('score2');
        var divsport3=document.getElementById('score3');
        var divsport4=document.getElementById('score4');
        var divsport5=document.getElementById('score5');
        var divsport6=document.getElementById('score6');
        var divsport7=document.getElementById('score7');
        var divsport8=document.getElementById('score8');
        var divsport9=document.getElementById('score9');
        var divsport10=document.getElementById('score10');
        var divsport11=document.getElementById('score11');
        var divsport12=document.getElementById('score12');
        var divsport13=document.getElementById('score13');
        var divsport14=document.getElementById('score14');
        var divsport15=document.getElementById('score15');
        var div16_1=document.getElementById('score16_1');
        var div16_2=document.getElementById('score16_2');
        var div16_3=document.getElementById('score16_3');
        var div17=document.getElementById('score17');
        var div18_8=document.getElementById('score18_8');
        var div18_7=document.getElementById('score18_7');
        var div18_6=document.getElementById('score18_6');
        var div18_5=document.getElementById('score18_5');
        var div18_4=document.getElementById('score18_4');
        var div18_3=document.getElementById('score18_3');
        var div18_2=document.getElementById('score18_2');
        var div18_1=document.getElementById('score18_1');
       
        
      if(sportValue != "") 
       {
        if(divsport1.style.display == "block")
        {
            divsport1.style.display = "none"
        }
        else if(divsport2.style.display == "block")
        {
            divsport2.style.display = "none"
        }
        else if(divsport3.style.display == "block")
        {
            divsport3.style.display = "none"
        }
        else if(divsport4.style.display == "block")
        {
            divsport4.style.display = "none"
        }
        else if(divsport5.style.display == "block")
        {
            divsport5.style.display = "none"
        }
        else if(divsport6.style.display == "block")
        {
            divsport6.style.display = "none"
        }
        else if(divsport7.style.display == "block")
        {
            divsport7.style.display = "none"
        }
        else if(divsport8.style.display == "block")
        {
            divsport8.style.display = "none"
        }
        else if(divsport9.style.display == "block")
        {
            divsport9.style.display = "none"
        }
        else if(divsport10.style.display == "block")
        {
            divsport10.style.display = "none"
        }
        else if(divsport11.style.display == "block")
        {
            divsport11.style.display = "none"
        }
        else if(divsport12.style.display == "block")
        {
            divsport12.style.display = "none"
        }
        else if(divsport13.style.display == "block")
        {
            divsport13.style.display = "none"
        }
        else if(divsport14.style.display == "block")
        {
            divsport14.style.display = "none"
        }
        else if(divsport15.style.display == "block")
        {
           divsport15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        }
    }
        
        
   // Male     
      if(document.getElementById('gender').value == "Male") {
                
        if(sportValue == "archery")
        {
            var mean = 17.1 ;
            var sd = 1.54 ;                  
            
        }else if(sportValue == "Australian football [AFL midfield]") {
        
           var mean = 18.71 ;
           var sd = 1.68 ;  
           
      }else if(sportValue == "Australian football [AFL]") {
        
           var mean = 19.63 ;
           var sd = 1.77 ;
           
      }else if(sportValue == "badminton") {
        
           var mean = 18.7 ;
           var sd = 1.75 ;        
           
      }else if(sportValue == "baseball") {
        
           var mean = 17.57 ;
           var sd = 1.58 ;    
           
      }else if(sportValue == "basketball") {
        
           var mean = 20.97 ;
           var sd = 1.89 ;     
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 17.8 ;
           var sd = 1.6 ;    
           
      }else if(sportValue == "boxing - general") {
        
           var mean = 18.33 ;
           var sd = 1.65 ;   
           
      }else if(sportValue == "boxing - heavyweight") {
        
           var mean = 17.87 ;
           var sd = 1.61 ;
           
      }else if(sportValue == "canoe polo") {
        
           var mean = 18.36 ;
           var sd = 1.65 ;   
           
      }else if(sportValue == "canoeing (Canadian)") {
        
           var mean = 17.97 ;
           var sd = 1.62 ; 
           
      }else if(sportValue == "cricket") {
        
           var mean = 17.94 ;
           var sd = 1.61 ;      
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 17.67 ;
           var sd = 1.59 ;      
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 17.31 ;
           var sd = 1.56 ; 
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 19.83 ;
           var sd = 1.78 ;   
           
      }else if(sportValue == "decathlon") {
        
           var mean = 18.89 ;
           var sd = 1.7 ; 
           
      }else if(sportValue == "discus") {
        
           var mean = 15.12 ;
           var sd = 1.36 ;   
           
      }else if(sportValue == "diving") {
        
           var mean = 16.36 ;
           var sd = 1.47 ;        
      }else if(sportValue == "fencing") {
        
           var mean = 17.63 ;
           var sd = 1.59 ;   
           
      }else if(sportValue == "golf") {
        
           var mean = 15.23 ;
           var sd = 1.37 ;        
           
      }else if(sportValue == "gymnastics") {
        
           var mean = 17.75 ;
           var sd = 1.6 ; 
           
      }else if(sportValue == "handball") {
        
           var mean = 19.94 ;
           var sd = 1.79 ;
           
      }else if(sportValue == "high jump") {
        
           var mean = 20.8 ;
           var sd = 1.87 ;  
           
      }else if(sportValue == "hockey (field)") {
        
           var mean = 18.57 ;
           var sd = 1.67 ;   
           
      }else if(sportValue == "hockey (ice)") {
        
           var mean = 17.82 ;
           var sd = 1.6 ; 
           
      }else if(sportValue == "hurdles") {
        
           var mean = 20.02 ;
           var sd = 1.8 ;  
           
      }else if(sportValue == "javelin") {
        
           var mean = 16.97 ;
           var sd = 1.53 ;  
           
      }else if(sportValue == "jockey") {
        
           var mean = 16.05 ;
           var sd = 1.44 ; 
           
      }else if(sportValue == "judo") {
        
           var mean = 16.52 ;
           var sd = 1.49 ;    
           
      }else if(sportValue == "karate") {
        
           var mean = 18.66 ;
           var sd = 1.68 ; 
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 18.83 ;
           var sd = 1.69 ; 
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 18.02 ;
           var sd = 1.62 ; 
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 18.87 ;
           var sd = 1.7 ;        
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 18.81 ;
           var sd = 1.69 ;  
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 18.6 ;
           var sd = 1.67 ;  
           
      }else if(sportValue == "long jump") {
        
           var mean = 20.33 ;
           var sd = 1.83 ;   
           
      }else if(sportValue == "orienteering") {
        
           var mean = 18.13 ;
           var sd = 1.63 ;  
           
      }else if(sportValue == "powerlifting") {
        
           var mean = 18.12 ;
           var sd = 1.63 ;        
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 17.47 ;
           var sd = 1.57 ;
           
      }else if(sportValue == "rollerskating") {
        
           var mean = 16.86 ;
           var sd = 1.52 ; 
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 19.59 ;
           var sd = 1.76 ;  
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 19.64 ;
           var sd = 1.77 ;   
           
      }else if(sportValue == "rugby League - backs") {
        
           var mean = 17.55 ;
           var sd = 1.58 ;  
           
      }else if(sportValue == "rugby League - forwards") {
        
           var mean = 16.9 ;
           var sd = 1.52 ;    
           
      }else if(sportValue == "rugby union") {
        
           var mean = 17.3 ;
           var sd = 1.56 ;       
           
      }else if(sportValue == "running - distance") {
        
           var mean = 17.3 ;
           var sd = 1.56 ; 
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 18.39 ;
           var sd = 1.66 ;       
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 19.92 ;
           var sd = 1.79 ;  
           
      }else if(sportValue == "sailing") {
        
           var mean = 17.79 ;
           var sd = 1.6 ;  
           
      }else if(sportValue == "shooting") {
        
           var mean = 18.04 ;
           var sd = 1.62 ;        
           
      }else if(sportValue == "shot put") {
        
           var mean = 17.43 ;
           var sd = 1.57 ; 
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 16.89 ;
           var sd = 1.52 ;        
           
      }else if(sportValue == "soccer") {
        
           var mean = 19.24 ;
           var sd = 1.73 ; 
           
      }else if(sportValue == "squash") {
        
           var mean = 18.39 ;
           var sd = 1.66 ;    
           
      }else if(sportValue == "sumo wrestling") {
        
           var mean = 14.23 ;
           var sd = 1.28 ;    
           
      }else if(sportValue == "surfing") {
        
           var mean = 17.85 ;
           var sd = 1.61 ;  
           
      }else if(sportValue == "swimming") {
        
           var mean = 17.52 ;
           var sd = 1.58 ; 
           
      }else if(sportValue == "table tennis") {
        
           var mean = 16.41 ;
           var sd = 1.48 ;
           
      }else if(sportValue == "tennis") {
        
           var mean = 19.47 ;
           var sd = 1.75 ;        
           
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 18.01 ;
           var sd = 1.62 ;      
           
      }else if(sportValue == "triathlon") {
        
           var mean = 17.52 ;
           var sd = 1.58 ; 
           
      }else if(sportValue == "volleyball") {
        
           var mean = 20.79 ;
           var sd = 1.87 ;  
           
      }else if(sportValue == "walking") {
        
           var mean = 17.2 ;
           var sd = 1.55 ;     
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 17.93 ;
           var sd = 1.61 ;     
           
      }else if(sportValue == "weightlifting") {
        
           var mean = 15.77 ;
           var sd = 1.42 ;   
           
      }else if(sportValue == "wrestling") {
        
          var mean = 16.41 ;
          var sd = 1.48 ;          
      }
             
    }
    
  // Female      
    else if(document.getElementById('gender').value == "Female") {
              
        if(sportValue == "archery")
        {
            var mean = 14.14 ;
            var sd = 1.27 ;                  
            
        }else if(sportValue == "badminton") {
        
           var mean = 15.55 ;
           var sd = 1.4 ; 
           
      }else if(sportValue == "basketball") {
        
           var mean = 16.54 ;
           var sd = 1.49 ;  
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 14.24 ;
           var sd = 1.28 ;     
           
      }else if(sportValue == "cricket") {
        
           var mean = 14.16 ;
           var sd = 1.27 ;
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 14.24 ;
           var sd = 1.28 ;  
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 14.24 ;
           var sd = 1.28 ;    
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 17.79 ;
           var sd = 1.6 ;   
           
      }else if(sportValue == "discus") {
        
           var mean = 14.22 ;
           var sd = 1.28 ;    
           
      }else if(sportValue == "diving") {
        
           var mean = 15.55 ;
           var sd = 1.4 ;
           
      }else if(sportValue == "fencing") {
        
           var mean = 14.37 ;
           var sd = 1.29 ; 
           
      }else if(sportValue == "golf") {
        
           var mean = 14.17 ;
           var sd = 1.28 ;   
           
      }else if(sportValue == "gymnastics") {
        
           var mean = 16.5 ;
           var sd = 1.49 ;    
           
      }else if(sportValue == "handball") {
        
           var mean = 14.73 ;
           var sd = 1.33 ;     
           
      }else if(sportValue == "heptathlon") {
        
           var mean = 14.43 ;
           var sd = 1.3 ; 
           
      }else if(sportValue == "high jump") {
        
           var mean = 17.3 ;
           var sd = 1.56 ;    
           
      }else if(sportValue == "hockey (field)") {
        
           var mean = 14.21 ;
           var sd = 1.28 ;        
           
      }else if(sportValue == "hurdles") {
        
           var mean = 14.13 ;
           var sd = 1.27 ; 
           
      }else if(sportValue == "javelin") {
        
           var mean = 14.56 ;
           var sd = 1.31 ;  
           
      }else if(sportValue == "judo") {
        
           var mean = 14.65 ;
           var sd = 1.32 ;        
           
      }else if(sportValue == "karate") {
        
           var mean = 16.16 ;
           var sd = 1.45 ;  
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 14.32 ;
           var sd = 1.29 ;   
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 13.92 ;
           var sd = 1.25 ;        
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 14.62 ;
           var sd = 1.32 ;    
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 14.26 ;
           var sd = 1.28 ;        
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 14.16 ;
           var sd = 1.27 ;    
           
      }else if(sportValue == "long jump") {
        
           var mean = 16.99 ;
           var sd = 1.53 ;   
           
      }else if(sportValue == "orienteering") {
        
           var mean = 14.3 ;
           var sd = 1.29 ;     
           
      }else if(sportValue == "netball") {
        
           var mean = 16.7 ;
           var sd = 1.5 ; 
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 14.89 ;
           var sd = 1.34 ;       
           
      }else if(sportValue == "rollerskating") {
        
           var mean = 14.12 ;
           var sd = 1.27 ;   
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 14.28 ;
           var sd = 1.29 ;        
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 15.1 ;
           var sd = 1.36 ;   
           
      }else if(sportValue == "rugby union") {
        
           var mean = 14.22 ;
           var sd = 1.28 ;      
           
      }else if(sportValue == "running - distance") {
        
           var mean = 14.4 ;
           var sd = 1.3 ;   
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 14.21 ;
           var sd = 1.28 ;  
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 17.11 ;
           var sd = 1.54 ;
           
      }else if(sportValue == "sailing") {
        
           var mean = 13.47 ;
           var sd = 1.21 ;  
           
      }else if(sportValue == "shooting") {
        
           var mean = 14.32 ;
           var sd = 1.29 ; 
           
      }else if(sportValue == "shot put") {
        
           var mean = 14.47   ;
           var sd = 1.3 ;        
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 14.3 ;
           var sd = 1.29 ;        
           
      }else if(sportValue == "soccer") {
        
           var mean = 15.22 ;
           var sd = 1.37 ; 
           
      }else if(sportValue == "softball") {
        
           var mean = 14.77 ;
           var sd = 1.33 ;        
           
      }else if(sportValue == "squash") {
        
           var mean = 15.1 ;
           var sd = 1.36 ;        
           
      }else if(sportValue == "surfing") {
        
           var mean = 14.52 ;
           var sd = 1.31 ;        
           
      }else if(sportValue == "swimming") {
        
           var mean = 14.66 ;
           var sd = 1.32 ;        
           
      }else if(sportValue == "synchronised swimming") {
        
           var mean = 13.9 ;
           var sd = 1.25 ;        
           
      }else if(sportValue == "table tennis") {
        
           var mean = 14.2 ;
           var sd = 1.28 ;        
           
      }else if(sportValue == "tennis") {
        
           var mean = 15.11 ;
           var sd = 1.36 ;        
           
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 13.88 ;
           var sd = 1.25 ;        
           
      }else if(sportValue == "triathlon") {
        
           var mean = 14.13 ;
           var sd = 1.27 ;        
           
      }else if(sportValue == "volleyball") {
        
           var mean = 16.42 ;
           var sd = 1.48 ;  
           
      }else if(sportValue == "walking") {
        
           var mean = 13.95 ;
           var sd = 1.26 ;        
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 14.29 ;
           var sd = 1.29 ;        
      }             
        
    }
      
    
        
          if(mean < testmean)
           {  
             if(mean < (parseFloat(testmean) / 2))
             {
              if(mean >= 0 && mean < ((parseFloat(testmean) / 2) - 0.6))
                {              
                   var sportPlot = '1 in 1_1' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) - 0.6) && mean < ((parseFloat(testmean) / 2) - 0.4))
                {              
                   var sportPlot = '1 in 1_2' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 0.4) && mean < ((parseFloat(testmean) / 2) - 0.2))
                {              
                   var sportPlot = '1 in 1_3' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 0.2) && mean < (parseFloat(testmean) / 2))
                {              
                   var sportPlot = '1 in 1_4' ;
                }
             }
             else{
                 
             if(mean >= (parseFloat(testmean) / 2) && mean < ((parseFloat(testmean) / 2) + 0.2))
                {              
                   var sportPlot = '1 in 1_5' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) + 0.2) && mean < ((parseFloat(testmean) / 2) + 0.4))
                {              
                  var sportPlot = '1 in 1_6' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 0.4) && mean < ((parseFloat(testmean) / 2) + 0.6))
                {              
                  var sportPlot = '1 in 1_7' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 0.6) && mean < testmean)
                {              
                   var sportPlot = '1 in 1_8' ;
                }        
             }
           }
           
           else if(mean >= testmean && mean < (parseFloat(testmean) + 0.05))
           {              
              var sportPlot = '1 in 2' ;
           }  
      
          else if(mean >= (parseFloat(testmean) + 0.05) && mean < testmean15)
           {           
               if(mean >= (parseFloat(testmean) + 0.05) && mean < (parseFloat(testmean) + 0.1))
               {
                 var sportPlot = '1 in 3_1' ;   
               }
               else if(mean >= (parseFloat(testmean) + 0.1) && mean < (parseFloat(testmean) + 0.15))
               {
                 var sportPlot = '1 in 3_2' ;   
               }
               else if(mean >= (parseFloat(testmean) + 0.15) && mean < testmean15)
               { 
                 var sportPlot = '1 in 3_3' ;   
               }
                
           }       
        
          else if(mean >= testmean15 && mean < testmean14)
           {              
              var sportPlot = '1 in 5' ;
           }           
           else if(mean >= testmean14 && mean < testmean13)
           {             
              var sportPlot = '1 in 10' ;
           }           
           else if(mean >= testmean13 && mean < testmean12)
           {             
              var sportPlot = '1 in 20' ;
           }          
           else if(mean >= testmean12 && mean < testmean11)
           {              
              var sportPlot = '1 in 50' ;
           }          
           else if(mean >= testmean11 && mean < testmean10)
           {              
              var sportPlot = '1 in 100' ;
           }          
           else if(mean >= testmean10 && mean < testmean9)
           {             	
              var sportPlot = '1 in 200' ;
           }          
           else if(mean >= testmean9 && mean < testmean8)
           {              
              var sportPlot = '1 in 500' ;
           }           
           else if(mean >= testmean8 && mean < testmean7)
           {              
              var sportPlot = '1 in 1000' ;
           }          
           else if(mean >= testmean7 && mean < testmean6)
           {              
              var sportPlot = '1 in 2,500' ;
           }          
           else if(mean >= testmean6 && mean < testmean5)
           {              
              var sportPlot = '1 in 5,000' ;
           }        
           else if(mean >= testmean5 && mean < testmean4)
           {              	
              var sportPlot = '1 in 10,000' ;
           }           
           else if(mean >= testmean4 && mean < testmean3)
           {             	
              var sportPlot = '1 in 50,000' ;
           }
           else if(mean >= testmean3 && mean < testmean2)
           {              	
              var sportPlot = '1 in 100,000' ;
           }           
           else if(mean >= testmean2 && mean < testmean1)
           {              
              var sportPlot = '1 in 1,000,000' ;
           }           
           else if(mean >= testmean1)
           {              
              var sportPlot = '1 in 10,000,000' ;
           }           
                               
           document.getElementById("sportPlot").value = sportPlot ;              
           
           document.getElementById("sportMeanScore").value = mean ; // store mean value of sport selected
           
           
  }
    
    
    
   function show()
    {    
        var score = document.getElementById("scorePlot").value ;      
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
      else
      { 
        if(score == '1 in 1_1')
        {
           var div=document.getElementById('plot18_1');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_2')
        {
           var div=document.getElementById('plot18_2');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_3')
        {
           var div=document.getElementById('plot18_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_4')
        {
           var div=document.getElementById('plot18_4');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_5')
        {
           var div=document.getElementById('plot18_5');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_6')
        {
           var div=document.getElementById('plot18_6');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_7')
        {
           var div=document.getElementById('plot18_7');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_8')
        {
           var div=document.getElementById('plot18_8');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 2')
        {
           var div=document.getElementById('plot17');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_1')
        {
           var div=document.getElementById('plot16_1');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_2')
        {
           var div=document.getElementById('plot16_2');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_3')
        {
           var div=document.getElementById('plot16_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 5')
        {
           var div=document.getElementById('plot15');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 10') {
        
           var div=document.getElementById('plot14');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 20') {
        
          var div=document.getElementById('plot13');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 50') {
        
           var div=document.getElementById('plot12');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 100') {
        
           var div=document.getElementById('plot11');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 200') {
        
           var div=document.getElementById('plot10');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 500') {
        
           var div=document.getElementById('plot9');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1000') {
        
           var div=document.getElementById('plot8');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 2,500') {
        
           var div=document.getElementById('plot7');
           div.style.display = "block" ; 
            
      }
      else if(score == '1 in 5,000') {
        
           var div=document.getElementById('plot6');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000') {
        
           var div=document.getElementById('plot5');
           div.style.display = "block" ;      
           
      }
      else if(score == '1 in 50,000') {
        
           var div=document.getElementById('plot4');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 100,000') {
        
           var div=document.getElementById('plot3');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1,000,000') {
        
           var div=document.getElementById('plot2');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000,000') {
        
          var div=document.getElementById('plot1');
          div.style.display = "block" ;     
      }
      
      
      
    }
      
        showSport() ;
  }
    
  
  function showSport()
    {       
        var sport = document.getElementById("sportPlot").value ; 
        var sportMean = document.getElementById('sportMeanScore').value ;
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
        else
      { 
          
        if(sport == '1 in 1_1')
        {
           var div=document.getElementById('score18_1');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_2')
        {
           var div=document.getElementById('score18_2');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_3')
        {
           var div=document.getElementById('score18_3');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_4')
        {
           var div=document.getElementById('score18_4');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_5')
        {
           var div=document.getElementById('score18_5');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_6')
        {
           var div=document.getElementById('score18_6');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_7')
        {
           var div=document.getElementById('score18_7');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_8')
        {
           var div=document.getElementById('score18_8');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 2')
        {
           var div=document.getElementById('score17');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_1')
        {
           var div=document.getElementById('score16_1');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_2')
        {
           var div=document.getElementById('score16_2');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_3')
        {
           var div=document.getElementById('score16_3');
           div.style.display = "block" ;
            
        }   
       
       else if(sport == '1 in 5')
        {
           var div=document.getElementById('score15');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 10') {
        
           var div=document.getElementById('score14');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 20') {
        
          var div=document.getElementById('score13');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 50') {
        
           var div=document.getElementById('score12');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 100') {
        
           var div=document.getElementById('score11');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 200') {
        
           var div=document.getElementById('score10');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 500') {
        
           var div=document.getElementById('score9');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1000') {
        
           var div=document.getElementById('score8');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 2,500') {
        
           var div=document.getElementById('score7');
           div.style.display = "block" ; 
            
      }
      else if(sport == '1 in 5,000') {
        
           var div=document.getElementById('score6');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000') {
        
           var div=document.getElementById('score5');
           div.style.display = "block" ;      
           
      }
      else if(sport == '1 in 50,000') {
        
           var div=document.getElementById('score4');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 100,000') {
        
           var div=document.getElementById('score3');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1,000,000') {
        
           var div=document.getElementById('score2');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000,000') {
        
          var div=document.getElementById('score1');
          div.style.display = "block" ;     
      }
      
    }
 
      
      if(sportMean == 'undefined')
      {   
        document.getElementById('sportMeanVal').innerHTML = "" ;
      }
      else
      {
          document.getElementById('sportMeanVal').innerHTML = "["+sportMean+"]" ;
      }
  }
  
</script>  
</body>
</html>
