<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Body Composition</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>

<script type="text/javascript">
    
  
var specialKeys = new Array();
        specialKeys.push(8,37, 38, 39, 40); //Backspace
        function IsNumeric(e) {
            
            var keyCode = e.which ? e.which : e.keyCode
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1);
        if(!ret)  { 
        alert('Only Numbers');
        }
            return ret;
        }  
    
  $( document ).ready(function() {
    
     
     $('.two-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 2){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(2);
            }  
         }            
         return this; //for chaining
      }); 
      
      $('.one-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
         }            
         return this; //for chaining
      }); 
      
      
      
      
      
      
      
      $(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
	   console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
			// console.log($('.left').hasClass('fixed');
		});
      
    $("#content").hide();
    $("#content_option").hide();
    $("#content_option2").hide();
    
     $("#options_height").blur(function(){
           if($('#options_height_measured').val()=='' && $('#options_weight_measured').val()=='') {
                if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
           }else{
             // $('#bmi').val('');
             
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
               
           
         }
         
      
     });

 
     
      $("#options_weight").blur(function(){
         
         if($('#options_height_measured').val()=='' && $('#options_weight_measured').val()=='') {  
                if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                   $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                   $val = $val.toFixed(2); 
                   $('#bmi').val($val);
                   }else{
                     $('#bmi').val('');
                 }
         }else{
            // alert('hi');
            //  console.log($('#options_height_measured').val());
         // console.log($('#options_weight_measured').val());
               
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
         }
      });
      
      $("#options_height_measured").blur(function(){
         // alert($('#options_height_measured').val()!='');
        if($('#options_height_measured').val()!='' && $('#options_weight_measured').val()!='') {
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(1);  
                 $('#bmi').val($val);
               
           }else{
              if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
         }
           
      });
      
      $("#options_weight_measured").blur(function(){
       if($('#options_height_measured').val()!='' && $('#options_weight_measured').val()!='') {
                
                 $val = ($('#options_weight_measured').val()/($('#options_height_measured').val()*$('#options_height_measured').val()))*10000;
                 $val = $val.toFixed(2); 
                 $('#bmi').val($val);
               
           }else{
             // $('#bmi').val('');
              if($('#options_height').val()!='' && $('#options_weight').val()!='') {
                 
                $val = ($('#options_weight').val()/($('#options_height').val()*$('#options_height').val()))*10000;
                $val = $val.toFixed(1); 
                $('#bmi').val($val);
                 }else{
                     $('#bmi').val('');
                 }
         }
      });
      
      ////////////////Waist Hip Ratio///////
      
     
     $("#waist").blur(function(){
       if($('#waist').val()!='' && $('#hip').val()!='') {
                
                 $val = $('#waist').val() / $('#hip').val();
                 $val = $val.toFixed(2); 
                 $('#whr').val($val);
               
           }else{
              $('#whr').val('');
         }
      });
     
      $("#hip").blur(function(){
       
                if($('#waist').val()!='' && $('#hip').val()!='') {
                   $val = $('#waist').val() / $('#hip').val();
                   $val = $val.toFixed(2); 
                   $('#whr').val($val);
                   }
         else{
              $('#whr').val('');
         }
      });
  
  //////////////// SOS /////////////////////
  
  
  $("#triceps").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
                 $val = $val.toFixed(1); 
                 $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
     
     $("#biceps").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
                 $val = $val.toFixed(1);  
                 $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
  
  $("#subscapular").blur(function(){
       if($('#triceps').val()!='' && $('#biceps').val()!=''&& $('#subscapular').val()!='') {
                
                 $val = Number($('#triceps').val()) +  Number($('#biceps').val()) + Number($('#subscapular').val()) ;
            $val = $val.toFixed(1); 
            $('#sos').val($val);
               
           }else{
              $('#sos').val('');
         }
      });
  
  
});  









</script>



</head>

<body>
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo" id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
    <?php  echo form_open('welcome/saveClientBodyCompositionInfo','',$hidden); ?> 
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="post" id="inputs"> -->
    <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $this->session->userdata('user_first_name') ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $this->session->userdata('user_last_name') ;?>" disabled="disabled" required><input name="submitBodyComposition" type="submit" value="" title="edit client details"/></span>
     </div>
<!--	</form> -->
</div><!--End login --> 

 <?php $hidden = array('userid' => $id);
        //echo form_open('welcome/saveClientBodyCompositionInfo','',$hidden); ?> 


 <!--Start Mid --> 
  <div class="mid3 comp-mid-new">
 
   
<!--Start contain --> 
<div class="contain comp-contain-new">
   
   <!--Start left --> 
   <div class="left">
   		   		<div class="btn">
        <?php echo form_submit('mysubmit1','',"class='client_submit_form1' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form44' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
        </div>
   
   </div><!--End Left --> 
 
  <?php //print_r($fieldData); ?>
 
   <!--Start right --> 
   <div class="right">
        
       
   		<div class="right-head">Body Composition</div>
   		<div class="right-section">
       
       
       <div class="comp-cont">
       		<div class="comp-left">
              <span>
              <p class="gen margin-left"><strong>SELF-REPORT</strong></p></span>
               <?php 
                $options_height = $fieldData[0]->option_height==''?'':$fieldData[0]->option_height;
               $attrib = array(
                        'name'        => 'options_height',
                        'id'          => 'options_height',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$options_height,
                       // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
                         'tabindex'=>'1',
			'type'=>'number',
                         'min'=>'0',
                         'max'=>99999,'step'=>'any' 
                      
                      );
               ?>
              <span><p class="gen">What is your height?</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;cm</div></span>
              <?php  
              $options_weight = $fieldData[0]->option_weight==''?'':$fieldData[0]->option_weight;
              $attrib = array(
                        'name'        => 'options_weight',
                        'id'          => 'options_weight',
                        'class'       => 'compo-text two-decimal',
                         'size'=>'20',
                         'value'=>$options_weight,
                       //  'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                        'tabindex'=>'2',
			'type'=>'number',
                         'min'=>'0',
                         'max'=>'1000','max'=>99999 ,'step'=>'any' 
                      
                      );
               ?>
              <span><p class="gen">What is your weight?</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;kg</div></span>
            </div>
            
            <div class="comp-mid">
            <span><p class="gen margin-right"><strong>MEASURED</strong></p></span>
             <?php  
               $option_height_measured = $fieldData[0]->option_height_measured==''?'':$fieldData[0]->option_height_measured;
             $attrib = array(
                        'name'        => 'options_height_measured',
                        'id'          => 'options_height_measured',
                        'class'       => 'compo-text  one-decimal',
                         'size'=>'20',
                         'value'=>$option_height_measured,
                     //   'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false' 
                         'tabindex'=>'3',
                        'type'=>'number',
                         'min'=>'0','max'=>99999,'step'=>'any' 
                      
                      );
               ?>
              <span class="spanmargin"><p class="gen" align="right">Height</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;cm</div></span>
              <?php
              $option_weight_measured = $fieldData[0]->option_weight_measured==''?'':$fieldData[0]->option_weight_measured;
              $attrib = array(
                        'name'        => 'options_weight_measured',
                        'id'          => 'options_weight_measured',
                        'class'       => 'compo-text  two-decimal',
                         'size'=>'20',
                         'value'=>$option_weight_measured,
                       // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                              'tabindex'=>'4',
			 'type'=>'number',
                         'min'=>'0','max'=>99999,'step'=>'any' 
                      
                      );
               ?>
              <span class="spanmargin"><p class="gen" align="right">Weight</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;kg</div></span>
            </div>
            
            <div class="comp-right">
                 <?php 
                 $option_bmi = $fieldData[0]->option_bmi==''?'':$fieldData[0]->option_bmi;
                 $attrib = array(
                        'name'        => 'bmi',
                        'id'          => 'bmi',
                        'class'       => 'compo-text  two-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_bmi ==0?'':$option_bmi,
			'type'=>'number',
                         'min'=>'0','step'=>'any' 
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                      
                      );
               ?>
              <span><p class="gen" align="right">Body mass index</p><?php echo form_input($attrib); ?></span>
            </div>
         </div>
      
      
     
      <!--Start Row 2--> 
          
       <div class="comp-cont2">
            <div class="comp-mid">
                 <?php  
                  $option_waist = $fieldData[0]->option_waist==''?'':$fieldData[0]->option_waist;
                 $attrib = array(
                        'name'        => 'waist',
                        'id'          => 'waist',
                        'class'       => 'compo-text  one-decimal',
                         'size'=>'20',
                         'value'=>$option_waist,
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                      'tabindex'=>'5',
			'type'=>'number',
                         'min'=>'0','step'=>'any' 
                      
                      );
               ?>
              <span><p class="gen" align="right">Waist</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;cm</div></span>
               <?php 
               $option_hip = $fieldData[0]->option_hip==''?'':$fieldData[0]->option_hip;
               $attrib = array(
                        'name'        => 'hip',
                        'id'          => 'hip',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_hip,
                       //  'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                    'tabindex'=>'6',
		'type'=>'number',
                         'min'=>'0','step'=>'any' 
                      
                      );
               ?>
              <span> <p class="gen" align="right">Hip</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;cm</div></span>
            </div>
            
            <div class="comp-right topmargin">
                 <?php  
                 $option_whr = $fieldData[0]->option_whr==''?'':$fieldData[0]->option_whr;
                 $attrib = array(
                        'name'        => 'whr',
                        'id'          => 'whr',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_whr==0?'':$option_whr,
			'type'=>'number',
                         'min'=>'0','step'=>'any' 
                        // 'onkeypress'=>'return IsNumeric(event)',
                         //'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                      
                      );
               ?>
              <span><p class="gen"  align="left">Waist: hip ratio (WHR)</p><?php echo form_input($attrib); ?></span>
            </div>
            
          </div><!--End Row 2-->
          
           <!--Start Row 3-->
           <div class="comp-cont2">
            <div class="comp-mid">
                 <?php 
                 $option_triceps = $fieldData[0]->option_triceps==''?'':$fieldData[0]->option_triceps;
                 $attrib = array(
                        'name'        => 'triceps',
                        'id'          => 'triceps',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_triceps,
                     'type'=>'number',
                         'step'=>'any' ,
                        // 'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                      'tabindex'=>'7',
                         'min'=>'0'
                      
                      );
               ?>
              <span><p class="gen" align="right">Triceps</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;mm</div></span>
               <?php  
               $option_biceps = $fieldData[0]->option_biceps==''?'':$fieldData[0]->option_biceps;
               
               $attrib = array(
                        'name'        => 'biceps',
                        'id'          => 'biceps',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_biceps,
                         'type'=>'number',
                         'step'=>'any' ,
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false'
                        'tabindex'=>'8',
                         'min'=>'0'
                      
                      );
               ?>
              <span> <p class="gen" align="right">Biceps</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;mm</div></span>
              <?php  
               $option_subscapular = $fieldData[0]->option_subscapular==''?'':$fieldData[0]->option_subscapular;
              
              $attrib = array(
                        'name'        => 'subscapular',
                        'id'          => 'subscapular',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'value'=>$option_subscapular,
                         'type'=>'number',
                         'step'=>'any' ,
                        // 'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                       //  'ondrop'=>'return false'
                   'tabindex'=>'9',
                         'min'=>'0'
                      
                      );
               ?>
              <span> <p class="gen" align="right">Subscapular</p><?php echo form_input($attrib); ?>
              <div class="value">&nbsp;mm</div></span>
            </div>
            
            <div class="comp-right topmargin-third">
                 <?php  
                 $option_sos = $fieldData[0]->option_sos==''?'':$fieldData[0]->option_sos;
                 $attrib = array(
                        'name'        => 'sos',
                        'id'          => 'sos',
                        'class'       => 'compo-text one-decimal',
                         'size'=>'20',
                         'readonly'=>'true',
                         'value'=>$option_sos==0?'':$option_sos,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'min'=>'0',
                         'max'=>'9999'
                      
                      );
               ?>
              <span><p class="gen" align="left">Sum of skinfolds</p><?php echo form_input($attrib); ?></span>
            </div>
            
          </div><!--End Row 3-->
          
            <!--   <input type="submit" name="mysubmit" value="Submit!" class="client_submit"> -->
               
      <?php echo form_close(); ?>
           

          
      </div><!--End right section--> 
   </div><!--End right --> 
   
   
</div><!--End contain -->
<!--
<div class="footer footer-top">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Mid --> 
</div><!--End Wrapper --> 

</body>
</html>
