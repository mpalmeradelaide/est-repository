<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>   
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">              
                  <a onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';" class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a> 
                <h1 class="page_head">PHANTOM z-scores</h1>
              </td>
            </tr>
            <tr>
              <td align="center">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="cus-input" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
              </td> 
			  
              <td align="center">
                <label>Gender</label>
                <input type="text" id="gender" name="gender" class="cus-input" value="<?php if($_SESSION['user_gender'] == 'M'){ echo "Male" ;}else{ echo "Female" ;} ;?>"> 
              </td>             
              
              <td align="center">
                <label>Age (yr)</label>
                <input type="text" id="age" name="age" class="cus-input" value="<?php echo $_SESSION['age'] ;?>"> 
              </td>        
            </tr>
            
            <tr>
              <td colspan="3"> 
                <table>
                    <tr>
                      <td align="center" class="sml_text_field" width="336px">
                        <label>Height</label>
                        <input type="text" id="height" name="height" class="cus-input" value="<?php echo $phantom_Zscores["height"] ; ?>" style="width:180px;"> 
                        cm
                      </td>
                      
                      <td align="center" class="sml_text_field" width="324px">
                        <label>Body mass</label>
                        <input type="text" id="body_mass" name="body_mass" class="cus-input" value="<?php echo $phantom_Zscores["body_mass"] ; ?>"> 
                        kg
                      </td>                             
                    </tr>                    
                </table>
              </td>                             
            </tr>
            
            <tr>                
                <td colspan="3" align="center" style="padding:10px 0; padding-top: 50px;"> 
                	<table width="100%" style="padding:0;">
                      <tbody>
                        <tr>
                            <td valign="top" width="30%" style="padding:10px 0;">
                          	<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>Skinfolds</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Triceps <span id="zscore_Triceps"><?php if($phantom_Zscores["zscore_Triceps"] !== "NaN"){echo $phantom_Zscores["zscore_Triceps"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Subscapular <span id="zscore_Subscapular"><?php if($phantom_Zscores["zscore_Subscapular"] !== "NaN"){echo $phantom_Zscores["zscore_Subscapular"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                      <td>Biceps <span id="zscore_Biceps"><?php if($phantom_Zscores["zscore_Biceps"] !== "NaN"){echo $phantom_Zscores["zscore_Biceps"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Iliac crest <span id="zscore_Iliac"><?php if($phantom_Zscores["zscore_Iliac"] !== "NaN"){echo $phantom_Zscores["zscore_Iliac"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Supraspinale <span id="zscore_Supspinale"><?php if($phantom_Zscores["zscore_Supspinale"] !== "NaN"){echo $phantom_Zscores["zscore_Supspinale"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Abdominal <span id="zscore_Abdominal"><?php if($phantom_Zscores["zscore_Abdominal"] !== "NaN"){echo $phantom_Zscores["zscore_Abdominal"] ;}?></span></td>
                                    </tr> 
                                    <tr>
                                        <td>Front thigh <span id="zscore_Thigh"><?php if($phantom_Zscores["zscore_Thigh"] !== "NaN"){echo $phantom_Zscores["zscore_Thigh"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Medial calf <span id="zscore_Calf"><?php if($phantom_Zscores["zscore_Calf"] !== "NaN"){echo $phantom_Zscores["zscore_Calf"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                      <td>Mid-axilla &nbsp;</td>
                                    </tr>                              
                                  </tbody>
                            </table> 
                            
                            <div class="chart_div chart_div_first">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                 <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="tricep_plot1" name="tricep_plot1" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot2" name="tricep_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot3" name="tricep_plot3" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot4" name="tricep_plot4" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot5" name="tricep_plot5" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot6" name="tricep_plot6" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot7" name="tricep_plot7" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot8" name="tricep_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot9" name="tricep_plot9" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot10" name="tricep_plot10" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot11" name="tricep_plot11" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot12" name="tricep_plot12" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot13" name="tricep_plot13" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot14" name="tricep_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot15" name="tricep_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="subscapular_plot1" name="subscapular_plot1" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot2" name="subscapular_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot3" name="subscapular_plot3" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot4" name="subscapular_plot4" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot5" name="subscapular_plot5" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot6" name="subscapular_plot6" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot7" name="subscapular_plot7" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot8" name="subscapular_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot9" name="subscapular_plot9" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot10" name="subscapular_plot10" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot11" name="subscapular_plot11" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot12" name="subscapular_plot12" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot13" name="subscapular_plot13" class="red_circule">&nbsp;</div>
                                            <div id="subscapular_plot14" name="subscapular_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="subscapular_plot15" name="subscapular_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="bicep_plot1" name="bicep_plot1" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot2" name="bicep_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot3" name="bicep_plot3" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot4" name="bicep_plot4" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot5" name="bicep_plot5" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot6" name="bicep_plot6" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot7" name="bicep_plot7" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot8" name="bicep_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot9" name="bicep_plot9" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot10" name="bicep_plot10" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot11" name="bicep_plot11" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot12" name="bicep_plot12" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot13" name="bicep_plot13" class="red_circule">&nbsp;</div>
                                            <div id="bicep_plot14" name="bicep_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="bicep_plot15" name="bicep_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="iliac_plot1" name="iliac_plot1" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot2" name="iliac_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot3" name="iliac_plot3" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot4" name="iliac_plot4" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot5" name="iliac_plot5" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot6" name="iliac_plot6" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot7" name="iliac_plot7" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot8" name="iliac_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot9" name="iliac_plot9" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot10" name="iliac_plot10" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot11" name="iliac_plot11" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot12" name="iliac_plot12" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot13" name="iliac_plot13" class="red_circule">&nbsp;</div>
                                            <div id="iliac_plot14" name="iliac_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="iliac_plot15" name="iliac_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="supspinale_plot1" name="supspinale_plot1" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot2" name="supspinale_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot3" name="supspinale_plot2" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot4" name="supspinale_plot4" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot5" name="supspinale_plot5" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot6" name="supspinale_plot6" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot7" name="supspinale_plot7" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot8" name="supspinale_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot9" name="supspinale_plot9" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot10" name="supspinale_plot10" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot11" name="supspinale_plot11" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot12" name="supspinale_plot12" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot13" name="supspinale_plot13" class="red_circule">&nbsp;</div>
                                            <div id="supspinale_plot14" name="supspinale_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="supspinale_plot15" name="supspinale_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="abdominal_plot1" name="abdominal_plot1" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot2" name="abdominal_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot3" name="abdominal_plot3" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot4" name="abdominal_plot4" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot5" name="abdominal_plot5" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot6" name="abdominal_plot6" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot7" name="abdominal_plot7" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot8" name="abdominal_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot9" name="abdominal_plot9" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot10" name="abdominal_plot10" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot11" name="abdominal_plot11" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot12" name="abdominal_plot12" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot13" name="abdominal_plot13" class="red_circule">&nbsp;</div>
                                            <div id="abdominal_plot14" name="abdominal_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="abdominal_plot15" name="abdominal_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="thigh_plot1" name="thigh_plot1" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot2" name="thigh_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot3" name="thigh_plot3" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot4" name="thigh_plot4" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot5" name="thigh_plot5" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot6" name="thigh_plot6" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot7" name="thigh_plot7" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot8" name="thigh_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot9" name="thigh_plot9" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot10" name="thigh_plot10" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot11" name="thigh_plot11" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot12" name="thigh_plot12" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot13" name="thigh_plot13" class="red_circule">&nbsp;</div>
                                            <div id="thigh_plot14" name="thigh_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="thigh_plot15" name="thigh_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="calf_plot1" name="calf_plot1" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot2" name="calf_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot3" name="calf_plot3" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot4" name="calf_plot4" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot5" name="calf_plot5" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot6" name="calf_plot6" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot7" name="calf_plot7" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot8" name="calf_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot9" name="calf_plot9" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot10" name="calf_plot10" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot11" name="calf_plot11" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot12" name="calf_plot12" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot13" name="calf_plot13" class="red_circule">&nbsp;</div>
                                            <div id="calf_plot14" name="calf_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="calf_plot15" name="calf_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                     <!-- <div class="chart_circule_row">
                                            <div id="tricep_plot1" name="tricep_plot1" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot2" name="tricep_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot3" name="tricep_plot3" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot4" name="tricep_plot4" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot5" name="tricep_plot5" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot6" name="tricep_plot6" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot7" name="tricep_plot7" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot8" name="tricep_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot9" name="tricep_plot9" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot10" name="tricep_plot10" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot11" name="tricep_plot11" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot12" name="tricep_plot12" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot13" name="tricep_plot13" class="red_circule">&nbsp;</div>
                                            <div id="tricep_plot14" name="tricep_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="tricep_plot15" name="tricep_plot15" class="red_circule">&nbsp;</div>
                                        </div> -->
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>
                            
                          </td>
                          <td valign="top" width="30%" style="padding:10px 0;">
                          		<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                  	<thead>                                  
                                    <tr> 
                                      <td>Girths</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                        <td>Arm (relaxed) <span id="zscore_RelArmG" name="zscore_RelArmG"><?php if($phantom_Zscores["zscore_RelArmG"] !== "NaN"){echo $phantom_Zscores["zscore_RelArmG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Arm (flexed) <span id="zscore_FlexArmG" name="zscore_FlexArmG"><?php if($phantom_Zscores["zscore_FlexArmG"] !== "NaN"){echo $phantom_Zscores["zscore_FlexArmG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Waist (min.) <span id="zscore_WaistG" name="zscore_WaistG"><?php if($phantom_Zscores["zscore_WaistG"] !== "NaN"){echo $phantom_Zscores["zscore_WaistG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Gluteal (hips) <span id="zscore_HipG" name="zscore_HipG"><?php if($phantom_Zscores["zscore_HipG"] !== "NaN"){echo $phantom_Zscores["zscore_HipG"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Calf (max.) <span id="zscore_CalfG" name="zscore_CalfG"><?php if($phantom_Zscores["zscore_CalfG"] !== "NaN"){echo $phantom_Zscores["zscore_CalfG"] ;}?></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                                
                                <div class="chart_div">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                  <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="relArmG_plot1" name="relArmG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot2" name="relArmG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot3" name="relArmG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot4" name="relArmG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot5" name="relArmG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot6" name="relArmG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot7" name="relArmG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot8" name="relArmG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot9" name="relArmG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot10" name="relArmG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot11" name="relArmG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot12" name="relArmG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot13" name="relArmG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="relArmG_plot14" name="relArmG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="relArmG_plot15" name="relArmG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="flexArmG_plot1" name="flexArmG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot2" name="flexArmG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot3" name="flexArmG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot4" name="flexArmG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot5" name="flexArmG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot6" name="flexArmG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot7" name="flexArmG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot8" name="flexArmG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot9" name="flexArmG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot10" name="flexArmG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot11" name="flexArmG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot12" name="flexArmG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot13" name="flexArmG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="flexArmG_plot14" name="flexArmG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="flexArmG_plot15" name="flexArmG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="waistG_plot1" name="waistG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot2" name="waistG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot3" name="waistG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot4" name="waistG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot5" name="waistG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot6" name="waistG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot7" name="waistG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot8" name="waistG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot9" name="waistG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot10" name="waistG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot11" name="waistG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot12" name="waistG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot13" name="waistG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="waistG_plot14" name="waistG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="waistG_plot15" name="waistG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="hipG_plot1" name="hipG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot2" name="hipG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot3" name="hipG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot4" name="hipG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot5" name="hipG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot6" name="hipG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot7" name="hipG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot8" name="hipG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot9" name="hipG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot10" name="hipG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot11" name="hipG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot12" name="hipG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot13" name="hipG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="hipG_plot14" name="hipG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="hipG_plot15" name="hipG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="calfG_plot1" name="calfG_plot1" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot2" name="calfG_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot3" name="calfG_plot3" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot4" name="calfG_plot4" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot5" name="calfG_plot5" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot6" name="calfG_plot6" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot7" name="calfG_plot7" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot8" name="calfG_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot9" name="calfG_plot9" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot10" name="calfG_plot10" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot11" name="calfG_plot11" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot12" name="calfG_plot12" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot13" name="calfG_plot13" class="red_circule">&nbsp;</div>
                                            <div id="calfG_plot14" name="calfG_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="calfG_plot15" name="calfG_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>

                          </td>
                          <td valign="top" width="30%" style="padding:10px 0;">
                          		<table width="42%" class="tabel_bord tabel_bord_sml" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>Breadths</td>
                                    </tr>
                                    </thead>
                                     <tbody>
                                    <tr>
                                        <td>Humerus <span id="zscore_Humerus" name="zscore_Humerus"><?php if($phantom_Zscores["zscore_Humerus"] !== "NaN"){echo $phantom_Zscores["zscore_Humerus"] ;}?></span></td>
                                    </tr>
                                    <tr>
                                        <td>Femur <span id="zscore_Femur" name="zscore_Femur"><?php if($phantom_Zscores["zscore_Femur"] !== "NaN"){echo $phantom_Zscores["zscore_Femur"] ;}?></span></td>
                                    </tr>
                                  </tbody>
                                </table>
                                
                                <div class="chart_div">
                            	<div class="chart_inner">
                                    <ul>
                                    	<li>&nbsp;</li><li>&nbsp;</li>
                                    </ul>
                                    <div class="chart_circule_main">
                                    	<div class="chart_circule_row">
                                            <div id="humerus_plot1" name="humerus_plot1" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot2" name="humerus_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot3" name="humerus_plot3" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot4" name="humerus_plot4" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot5" name="humerus_plot5" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot6" name="humerus_plot6" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot7" name="humerus_plot7" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot8" name="humerus_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot9" name="humerus_plot9" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot10" name="humerus_plot10" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot11" name="humerus_plot11" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot12" name="humerus_plot12" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot13" name="humerus_plot13" class="red_circule">&nbsp;</div>
                                            <div id="humerus_plot14" name="humerus_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="humerus_plot15" name="humerus_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                        <div class="chart_circule_row">
                                            <div id="femur_plot1" name="femur_plot1" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot2" name="femur_plot2" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot3" name="femur_plot3" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot4" name="femur_plot4" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot5" name="femur_plot5" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot6" name="femur_plot6" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot7" name="femur_plot7" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot8" name="femur_plot8" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot9" name="femur_plot9" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot10" name="femur_plot10" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot11" name="femur_plot11" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot12" name="femur_plot12" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot13" name="femur_plot13" class="red_circule">&nbsp;</div>
                                            <div id="femur_plot14" name="femur_plot14" class="red_circule">&nbsp;</div> 
                                            <div id="femur_plot15" name="femur_plot15" class="red_circule">&nbsp;</div>
                                        </div>
                                    </div> 
                                </div> 
                                <div class="chart_value">
                                	<ul>
                                    	<li>-3</li> <li>-2</li> <li>-1</li> <li>0</li>
                                        <li>1</li>
                                        <li>2</li>
                                        <li>3</li>
                                    </ul>
                                </div>
                            </div>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
              <td align="left">&nbsp;</td> 
			  
              <td align="right" colspan="2" valign="bottom">      
              	 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <!--<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>-->
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
    </div>
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">     
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script>   


<script>
 
 $(window).bind("load", function() {   
   
   
     var zscore_Triceps = document.getElementById("zscore_Triceps").innerHTML ;           
        
        if(zscore_Triceps !== "")
        {
          if(zscore_Triceps < -3)
          {    
            document.getElementById('tricep_plot1').style.opacity = "1" ;  
            var start1 = document.getElementById('tricep_plot1');
          }
          else if(zscore_Triceps == -3)
          {    
            document.getElementById('tricep_plot2').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot2');
          }
          else if(zscore_Triceps < -2 && zscore_Triceps > -3)
          {    
            document.getElementById('tricep_plot3').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot3');
          } 
          else if(zscore_Triceps == -2)
          {    
            document.getElementById('tricep_plot4').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot4');
          }
          else if(zscore_Triceps < -1 && zscore_Triceps > -2)
          {    
            document.getElementById('tricep_plot5').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot5');
          }
          else if(zscore_Triceps == -1)
          {    
            document.getElementById('tricep_plot6').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot6');
          }
          else if(zscore_Triceps < 0 && zscore_Triceps > -1)
          {    
            document.getElementById('tricep_plot7').style.opacity = "1" ;  
            var start1 = document.getElementById('tricep_plot7');
          } 
          else if(zscore_Triceps == 0)
          {    
            document.getElementById('tricep_plot8').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot8');
          }
          else if(zscore_Triceps < 1 && zscore_Triceps > 0)
          {    
            document.getElementById('tricep_plot9').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot9');
          }
          else if(zscore_Triceps == 1)
          {    
            document.getElementById('tricep_plot10').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot10');
          }
          else if(zscore_Triceps < 2 && zscore_Triceps > 1)
          {    
            document.getElementById('tricep_plot11').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot11');
          } 
          else if(zscore_Triceps == 2)
          {    
            document.getElementById('tricep_plot12').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot12');
          }
          else if(zscore_Triceps < 3 && zscore_Triceps > 2)
          {    
            document.getElementById('tricep_plot13').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot13');
          } 
          else if(zscore_Triceps == 3)
          {    
            document.getElementById('tricep_plot14').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot14');
          }
          else if(zscore_Triceps > 3)
          {    
            document.getElementById('tricep_plot15').style.opacity = "1" ;          
            var start1 = document.getElementById('tricep_plot15');
          } 
        } 
        
     
     var zscore_Subscapular = document.getElementById("zscore_Subscapular").innerHTML ;
     
        if(zscore_Subscapular !== "")
        {
          if(zscore_Subscapular < -3)
          {    
            document.getElementById('subscapular_plot1').style.opacity = "1" ;  
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot1'), "#ec6565", 1);  
            }        
            var start2 = document.getElementById('subscapular_plot1') ;
          }
          else if(zscore_Subscapular == -3)
          {    
            document.getElementById('subscapular_plot2').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot2'), "#ec6565", 1);  
            }               
            var start2 = document.getElementById('subscapular_plot2') ;
          }
          else if(zscore_Subscapular < -2 && zscore_Subscapular > -3)
          {    
            document.getElementById('subscapular_plot3').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot3'), "#ec6565", 1);  
            }            
            var start2 = document.getElementById('subscapular_plot3') ;
          } 
          else if(zscore_Subscapular == -2)
          {    
            document.getElementById('subscapular_plot4').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot4'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot4') ;
          }
          else if(zscore_Subscapular < -1 && zscore_Subscapular > -2)
          {    
            document.getElementById('subscapular_plot5').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot5'), "#ec6565", 1); 
            }            
            var start2 = document.getElementById('subscapular_plot5') ;
          }
          else if(zscore_Subscapular == -1)
          {    
            document.getElementById('subscapular_plot6').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot6'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot6') ;
          }
          else if(zscore_Subscapular < 0 && zscore_Subscapular > -1)
          {    
            document.getElementById('subscapular_plot7').style.opacity = "1" ;
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot7'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot7') ;
          } 
          else if(zscore_Subscapular == 0)
          {    
            document.getElementById('subscapular_plot8').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot8'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot8') ;
          }
          else if(zscore_Subscapular < 1 && zscore_Subscapular > 0)
          {    
            document.getElementById('subscapular_plot9').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot9'), "#ec6565", 1); 
            }            
            var start2 = document.getElementById('subscapular_plot9') ;
          }
          else if(zscore_Subscapular == 1)
          {    
            document.getElementById('subscapular_plot10').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot10'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot10') ;
          }
          else if(zscore_Subscapular < 2 && zscore_Subscapular > 1)
          {    
            document.getElementById('subscapular_plot11').style.opacity = "1" ;   
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot11'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot11') ;
          } 
          else if(zscore_Subscapular == 2)
          {    
            document.getElementById('subscapular_plot12').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot12'), "#ec6565", 1);  
            }            
            var start2 = document.getElementById('subscapular_plot12') ;
          }
          else if(zscore_Subscapular < 3 && zscore_Subscapular > 2)
          {    
            document.getElementById('subscapular_plot13').style.opacity = "1" ; 
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot13'), "#ec6565", 1);   
            }             
            var start2 = document.getElementById('subscapular_plot13') ;
          } 
          else if(zscore_Subscapular == 3)
          {    
            document.getElementById('subscapular_plot14').style.opacity = "1" ;   
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot14'), "#ec6565", 1); 
            }             
            var start2 = document.getElementById('subscapular_plot14') ;
          }
          else if(zscore_Subscapular > 3)
          {    
            document.getElementById('subscapular_plot15').style.opacity = "1" ;          
            if(start1 !== undefined)
            {
             connect(start1, document.getElementById('subscapular_plot15'), "#ec6565", 1); 
            }              
            var start2 = document.getElementById('subscapular_plot15') ;
          }          
         
        }          
         
         
      
     var zscore_Biceps = document.getElementById("zscore_Biceps").innerHTML ;    
     
        if(zscore_Biceps !== "")
        {            
          if(zscore_Biceps < -3)
          {    
            document.getElementById('bicep_plot1').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot1'), "#ec6565", 1);
            }            
            var start3 = document.getElementById('bicep_plot1') ;
          }
          else if(zscore_Biceps == -3)
          {    
            document.getElementById('bicep_plot2').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot2'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot2') ;
          }
          else if(zscore_Biceps < -2 && zscore_Biceps > -3)
          {    
            document.getElementById('bicep_plot3').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot3'), "#ec6565", 1);            
            } 
            var start3 = document.getElementById('bicep_plot3') ;
          } 
          else if(zscore_Biceps == -2)
          {    
            document.getElementById('bicep_plot4').style.opacity = "1" ;     
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot4'), "#ec6565", 1);            
            }             
            var start3 = document.getElementById('bicep_plot4') ;
          }
          else if(zscore_Biceps < -1 && zscore_Biceps > -2)
          {    
            document.getElementById('bicep_plot5').style.opacity = "1" ;  
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot5'), "#ec6565", 1);            
            }             
            var start3 = document.getElementById('bicep_plot5') ;
          }
          else if(zscore_Biceps == -1)
          {    
            document.getElementById('bicep_plot6').style.opacity = "1" ;   
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot6'), "#ec6565", 1);           
            }            
            var start3 = document.getElementById('bicep_plot6') ;
          }
          else if(zscore_Biceps < 0 && zscore_Biceps > -1)
          {    
            document.getElementById('bicep_plot7').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot7'), "#ec6565", 1);          
            }            
            var start3 = document.getElementById('bicep_plot7') ;
          } 
          else if(zscore_Biceps == 0)
          {    
            document.getElementById('bicep_plot8').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot8'), "#ec6565", 1);       
            }             
            var start3 = document.getElementById('bicep_plot8') ;
          }
          else if(zscore_Biceps < 1 && zscore_Biceps > 0)
          {    
            document.getElementById('bicep_plot9').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot9'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot9') ;
          }
          else if(zscore_Biceps == 1)
          {    
            document.getElementById('bicep_plot10').style.opacity = "1" ;          
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot10'), "#ec6565", 1);
            }            
            var start3 = document.getElementById('bicep_plot10') ;
          }
          else if(zscore_Biceps < 2 && zscore_Biceps > 1)
          {    
            document.getElementById('bicep_plot11').style.opacity = "1" ;
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot11'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot11') ;
          } 
          else if(zscore_Biceps == 2)
          {    
            document.getElementById('bicep_plot12').style.opacity = "1" ; 
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot12'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot12') ;
          }
          else if(zscore_Biceps < 3 && zscore_Biceps > 2)
          {    
            document.getElementById('bicep_plot13').style.opacity = "1" ;          
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot13'), "#ec6565", 1);
            }             
            var start3 = document.getElementById('bicep_plot13') ;
          } 
          else if(zscore_Biceps == 3)
          {    
            document.getElementById('bicep_plot14').style.opacity = "1" ;    
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot14'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot14') ;
          }
          else if(zscore_Biceps > 3)
          {    
            document.getElementById('bicep_plot15').style.opacity = "1" ;   
            if(start2 !== undefined)
            {
             connect(start2, document.getElementById('bicep_plot15'), "#ec6565", 1);
            }              
            var start3 = document.getElementById('bicep_plot15') ;
          }         
        
        }        
       
       

     var zscore_Iliac = document.getElementById("zscore_Iliac").innerHTML ;
     
        if(zscore_Iliac !== "")
        {
          if(zscore_Iliac < -3)
          {    
            document.getElementById('iliac_plot1').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot1'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot1') ;
          }
          else if(zscore_Iliac == -3)
          {    
            document.getElementById('iliac_plot2').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot2'), "#ec6565", 1);
            }             
            var start4 = document.getElementById('iliac_plot2') ;
          }
          else if(zscore_Iliac < -2 && zscore_Iliac > -3)
          {    
            document.getElementById('iliac_plot3').style.opacity = "1" ;  
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot3'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot3') ;
          } 
          else if(zscore_Iliac == -2)
          {    
            document.getElementById('iliac_plot4').style.opacity = "1" ;   
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot4'), "#ec6565", 1);
            }             
            var start4 = document.getElementById('iliac_plot4') ;
          }
          else if(zscore_Iliac < -1 && zscore_Iliac > -2)
          {    
            document.getElementById('iliac_plot5').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot5'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot5') ;
          }
          else if(zscore_Iliac == -1)
          {    
            document.getElementById('iliac_plot6').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot6'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot6') ;
          }
          else if(zscore_Iliac < 0 && zscore_Iliac > -1)
          {    
            document.getElementById('iliac_plot7').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot7'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot7') ;
          } 
          else if(zscore_Iliac == 0)
          {    
            document.getElementById('iliac_plot8').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot8'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot8') ;
          }
          else if(zscore_Iliac < 1 && zscore_Iliac > 0)
          {    
            document.getElementById('iliac_plot9').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot9'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot9') ;
          }
          else if(zscore_Iliac == 1)
          {    
            document.getElementById('iliac_plot10').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot10'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot10') ;
          }
          else if(zscore_Iliac < 2 && zscore_Iliac > 1)
          {    
            document.getElementById('iliac_plot11').style.opacity = "1" ;   
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot11'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot11') ;
          } 
          else if(zscore_Iliac == 2)
          {    
            document.getElementById('iliac_plot12').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot12'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot12') ;
          }
          else if(zscore_Iliac < 3 && zscore_Iliac > 2)
          {    
            document.getElementById('iliac_plot13').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot13'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot13') ;
          } 
          else if(zscore_Iliac == 3)
          {    
            document.getElementById('iliac_plot14').style.opacity = "1" ; 
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot14'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot14') ;
          }
          else if(zscore_Iliac > 3)
          {    
            document.getElementById('iliac_plot15').style.opacity = "1" ;          
            if(start3 !== undefined)
            {
             connect(start3, document.getElementById('iliac_plot15'), "#ec6565", 1);
            }            
            var start4 = document.getElementById('iliac_plot15') ;
          } 
          
        }
   
        

     var zscore_Supspinale = document.getElementById("zscore_Supspinale").innerHTML ;
     
        if(zscore_Supspinale !== "")
        {
          if(zscore_Supspinale < -3)
          {    
            document.getElementById('supspinale_plot1').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot1'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot1') ;
          }
          else if(zscore_Supspinale == -3)
          {    
            document.getElementById('supspinale_plot2').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot2'), "#ec6565", 1);
            }             
            var start5 = document.getElementById('supspinale_plot2') ;
          }
          else if(zscore_Supspinale < -2 && zscore_Supspinale > -3)
          {    
            document.getElementById('supspinale_plot3').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot3'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot3') ;
          } 
          else if(zscore_Supspinale == -2)
          {    
            document.getElementById('supspinale_plot4').style.opacity = "1" ;   
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot4'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot4') ;
          }
          else if(zscore_Supspinale < -1 && zscore_Supspinale > -2)
          {    
            document.getElementById('supspinale_plot5').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot5'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot5') ;
          }
          else if(zscore_Supspinale == -1)
          {    
            document.getElementById('supspinale_plot6').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot6'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot6') ;
          }
          else if(zscore_Supspinale < 0 && zscore_Supspinale > -1)
          {    
            document.getElementById('supspinale_plot7').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot7'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot7') ;
          } 
          else if(zscore_Supspinale == 0)
          {    
            document.getElementById('supspinale_plot8').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot8'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot8') ;
          }
          else if(zscore_Supspinale < 1 && zscore_Supspinale > 0)
          {    
            document.getElementById('supspinale_plot9').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot9'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot9') ;
          }
          else if(zscore_Supspinale == 1)
          {    
            document.getElementById('supspinale_plot10').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot10'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot10') ;
          }
          else if(zscore_Supspinale < 2 && zscore_Supspinale > 1)
          {    
            document.getElementById('supspinale_plot11').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot11'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot11') ;
          } 
          else if(zscore_Supspinale == 2)
          {    
            document.getElementById('supspinale_plot12').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot12'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot12') ;
          }
          else if(zscore_Supspinale < 3 && zscore_Supspinale > 2)
          {    
            document.getElementById('supspinale_plot13').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot13'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot13') ;
          } 
          else if(zscore_Supspinale == 3)
          {    
            document.getElementById('supspinale_plot14').style.opacity = "1" ;  
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot14'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot14') ;
          }
          else if(zscore_Supspinale > 3)
          {    
            document.getElementById('supspinale_plot15').style.opacity = "1" ;          
            if(start4 !== undefined)
            {
             connect(start4, document.getElementById('supspinale_plot15'), "#ec6565", 1);
            }            
            var start5 = document.getElementById('supspinale_plot15') ;
          } 
          
        }
        

     var zscore_Abdominal = document.getElementById("zscore_Abdominal").innerHTML ;
     
        if(zscore_Abdominal !== "")
        {
          if(zscore_Abdominal < -3)
          {    
            document.getElementById('abdominal_plot1').style.opacity = "1" ; 
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot1'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot1') ;
          }
          else if(zscore_Abdominal == -3)
          {    
            document.getElementById('abdominal_plot2').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot2'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot2') ;
          }
          else if(zscore_Abdominal < -2 && zscore_Abdominal > -3)
          {    
            document.getElementById('abdominal_plot3').style.opacity = "1" ;  
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot3'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot3') ;
          } 
          else if(zscore_Abdominal == -2)
          {    
            document.getElementById('abdominal_plot4').style.opacity = "1" ;   
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot4'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot4') ;
          }
          else if(zscore_Abdominal < -1 && zscore_Abdominal > -2)
          {    
            document.getElementById('abdominal_plot5').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot5'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot5') ;
          }
          else if(zscore_Abdominal == -1)
          {    
            document.getElementById('abdominal_plot6').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot6'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot6') ;
          }
          else if(zscore_Abdominal < 0 && zscore_Abdominal > -1)
          {    
            document.getElementById('abdominal_plot7').style.opacity = "1" ;
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot7'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot7') ;
          } 
          else if(zscore_Abdominal == 0)
          {    
            document.getElementById('abdominal_plot8').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot8'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot8') ;
          }
          else if(zscore_Abdominal < 1 && zscore_Abdominal > 0)
          {    
            document.getElementById('abdominal_plot9').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot9'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot9') ;
          }
          else if(zscore_Abdominal == 1)
          {    
            document.getElementById('abdominal_plot10').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot10'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot10') ;
          }
          else if(zscore_Abdominal < 2 && zscore_Abdominal > 1)
          {    
            document.getElementById('abdominal_plot11').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot11'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot11') ;
          } 
          else if(zscore_Abdominal == 2)
          {    
            document.getElementById('abdominal_plot12').style.opacity = "1" ;
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot12'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot12') ;
          }
          else if(zscore_Abdominal < 3 && zscore_Abdominal > 2)
          {    
            document.getElementById('abdominal_plot13').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot13'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot13') ;
          } 
          else if(zscore_Abdominal == 3)
          {    
            document.getElementById('abdominal_plot14').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot14'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot14') ;
          }
          else if(zscore_Abdominal > 3)
          {    
            document.getElementById('abdominal_plot15').style.opacity = "1" ;          
            if(start5 !== undefined)
            {
             connect(start5, document.getElementById('abdominal_plot15'), "#ec6565", 1);
            }            
            var start6 = document.getElementById('abdominal_plot15') ;
          } 
          
        }


     var zscore_Thigh = document.getElementById("zscore_Thigh").innerHTML ;
     
        if(zscore_Thigh !== "")
        {
          if(zscore_Thigh < -3)
          {    
            document.getElementById('thigh_plot1').style.opacity = "1" ;  
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot1'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot1') ;
          }
          else if(zscore_Thigh == -3)
          {    
            document.getElementById('thigh_plot2').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot2'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot2') ;
          }
          else if(zscore_Thigh < -2 && zscore_Thigh > -3)
          {    
            document.getElementById('thigh_plot3').style.opacity = "1" ;  
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot3'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot3') ;
          } 
          else if(zscore_Thigh == -2)
          {    
            document.getElementById('thigh_plot4').style.opacity = "1" ;   
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot4'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot4') ;
          }
          else if(zscore_Thigh < -1 && zscore_Thigh > -2)
          {    
            document.getElementById('thigh_plot5').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot5'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot5') ;
          }
          else if(zscore_Thigh == -1)
          {    
            document.getElementById('thigh_plot6').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot6'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot6') ;
          }
          else if(zscore_Thigh < 0 && zscore_Thigh > -1)
          {    
            document.getElementById('thigh_plot7').style.opacity = "1" ; 
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot7'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot7') ;
          } 
          else if(zscore_Thigh == 0)
          {    
            document.getElementById('thigh_plot8').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot8'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot8') ;
          }
          else if(zscore_Thigh < 1 && zscore_Thigh > 0)
          {    
            document.getElementById('thigh_plot9').style.opacity = "1" ;    
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot9'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot9') ;
          }
          else if(zscore_Thigh == 1)
          {    
            document.getElementById('thigh_plot10').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot10'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot10') ;
          }
          else if(zscore_Thigh < 2 && zscore_Thigh > 1)
          {    
            document.getElementById('thigh_plot11').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot11'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot11') ;
          } 
          else if(zscore_Thigh == 2)
          {    
            document.getElementById('thigh_plot12').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot12'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot12') ;
          }
          else if(zscore_Thigh < 3 && zscore_Thigh > 2)
          {    
            document.getElementById('thigh_plot13').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot13'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot13') ;
          } 
          else if(zscore_Thigh == 3)
          {    
            document.getElementById('thigh_plot14').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot14'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot14') ;
          }
          else if(zscore_Thigh > 3)
          {    
            document.getElementById('thigh_plot15').style.opacity = "1" ;          
            if(start6 !== undefined)
            {
             connect(start6, document.getElementById('thigh_plot15'), "#ec6565", 1);
            }            
            var start7 = document.getElementById('thigh_plot15') ;
          } 
          
        }
        

     var zscore_Calf = document.getElementById("zscore_Calf").innerHTML ;
     
        if(zscore_Calf !== "")
        {
          if(zscore_Calf < -3)
          {    
            document.getElementById('calf_plot1').style.opacity = "1" ;  
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot1'), "#ec6565", 1);           
            }            
          }
          else if(zscore_Calf == -3)
          {    
            document.getElementById('calf_plot2').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot2'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < -2 && zscore_Calf > -3)
          {    
            document.getElementById('calf_plot3').style.opacity = "1" ;  
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot3'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == -2)
          {    
            document.getElementById('calf_plot4').style.opacity = "1" ;   
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot4'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < -1 && zscore_Calf > -2)
          {    
            document.getElementById('calf_plot5').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot5'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf == -1)
          {    
            document.getElementById('calf_plot6').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot6'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < 0 && zscore_Calf > -1)
          {    
            document.getElementById('calf_plot7').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot7'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 0)
          {    
            document.getElementById('calf_plot8').style.opacity = "1" ; 
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot8'), "#ec6565", 1);
            }       
          }
          else if(zscore_Calf < 1 && zscore_Calf > 0)
          {    
            document.getElementById('calf_plot9').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot9'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf == 1)
          {    
            document.getElementById('calf_plot10').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot10'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf < 2 && zscore_Calf > 1)
          {    
            document.getElementById('calf_plot11').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot11'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 2)
          {    
            document.getElementById('calf_plot12').style.opacity = "1" ; 
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot12'), "#ec6565", 1);           
            }            
          }
          else if(zscore_Calf < 3 && zscore_Calf > 2)
          {    
            document.getElementById('calf_plot13').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot13'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_Calf == 3)
          {    
            document.getElementById('calf_plot14').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot14'), "#ec6565", 1);            
            }            
          }
          else if(zscore_Calf > 3)
          {    
            document.getElementById('calf_plot15').style.opacity = "1" ;          
            if(start7 !== undefined)
            {
             connect(start7, document.getElementById('calf_plot15'), "#ec6565", 1);            
            }            
          } 
          
        }
        

     var zscore_RelArmG = document.getElementById("zscore_RelArmG").innerHTML ;
     
        if(zscore_RelArmG !== "")
        {
          if(zscore_RelArmG < -3)
          {    
            document.getElementById('relArmG_plot1').style.opacity = "1" ;  
            var start8 = document.getElementById('relArmG_plot1') ;           
          }
          else if(zscore_RelArmG == -3)
          {    
            document.getElementById('relArmG_plot2').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot2') ;  
          }
          else if(zscore_RelArmG < -2 && zscore_RelArmG > -3)
          {    
            document.getElementById('relArmG_plot3').style.opacity = "1" ;  
            var start8 = document.getElementById('relArmG_plot3') ;  
          } 
          else if(zscore_RelArmG == -2)
          {    
            document.getElementById('relArmG_plot4').style.opacity = "1" ;   
            var start8 = document.getElementById('relArmG_plot4') ;  
          }
          else if(zscore_RelArmG < -1 && zscore_RelArmG > -2)
          {    
            document.getElementById('relArmG_plot5').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot5') ;         
          }
          else if(zscore_RelArmG == -1)
          {    
            document.getElementById('relArmG_plot6').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot6') ;
          }
          else if(zscore_RelArmG < 0 && zscore_RelArmG > -1)
          {    
            document.getElementById('relArmG_plot7').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot7') ;
          } 
          else if(zscore_RelArmG == 0)
          {    
            document.getElementById('relArmG_plot8').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot8') ;
          }
          else if(zscore_RelArmG < 1 && zscore_RelArmG > 0)
          {    
            document.getElementById('relArmG_plot9').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot9') ;            
          }
          else if(zscore_RelArmG == 1)
          {    
            document.getElementById('relArmG_plot10').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot10') ;           
          }
          else if(zscore_RelArmG < 2 && zscore_RelArmG > 1)
          {    
            document.getElementById('relArmG_plot11').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot11') ;
          } 
          else if(zscore_RelArmG == 2)
          {    
            document.getElementById('relArmG_plot12').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot12') ;
          }
          else if(zscore_RelArmG < 3 && zscore_RelArmG > 2)
          {    
            document.getElementById('relArmG_plot13').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot13') ;
          } 
          else if(zscore_RelArmG == 3)
          {    
            document.getElementById('relArmG_plot14').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot14') ;
          }
          else if(zscore_RelArmG > 3)
          {    
            document.getElementById('relArmG_plot15').style.opacity = "1" ;          
            var start8 = document.getElementById('relArmG_plot15') ;
          } 
          
        }
        

    var zscore_FlexArmG = document.getElementById("zscore_FlexArmG").innerHTML ;
     
        if(zscore_FlexArmG !== "")
        {
          if(zscore_FlexArmG < -3)
          {    
            document.getElementById('flexArmG_plot1').style.opacity = "1" ;
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot1'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot1') ;
          }
          else if(zscore_FlexArmG == -3)
          {    
            document.getElementById('flexArmG_plot2').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot2'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot2') ;
          }
          else if(zscore_FlexArmG < -2 && zscore_FlexArmG > -3)
          {    
            document.getElementById('flexArmG_plot3').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot3'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot3') ;
          } 
          else if(zscore_FlexArmG == -2)
          {    
            document.getElementById('flexArmG_plot4').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot4'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot4') ;
          }
          else if(zscore_FlexArmG < -1 && zscore_FlexArmG > -2)
          {    
            document.getElementById('flexArmG_plot5').style.opacity = "1" ;   
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot5'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot5') ;
          }
          else if(zscore_FlexArmG == -1)
          {    
            document.getElementById('flexArmG_plot6').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot6'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot6') ;
          }
          else if(zscore_FlexArmG < 0 && zscore_FlexArmG > -1)
          {    
            document.getElementById('flexArmG_plot7').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot7'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot7') ;
          } 
          else if(zscore_FlexArmG == 0)
          {    
            document.getElementById('flexArmG_plot8').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot8'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot8') ;
          }
          else if(zscore_FlexArmG < 1 && zscore_FlexArmG > 0)
          {    
            document.getElementById('flexArmG_plot9').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot9'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot9') ;
          }
          else if(zscore_FlexArmG == 1)
          {    
            document.getElementById('flexArmG_plot10').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot10'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot10') ;
          }
          else if(zscore_FlexArmG < 2 && zscore_FlexArmG > 1)
          {    
            document.getElementById('flexArmG_plot11').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot11'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot11') ;
          } 
          else if(zscore_FlexArmG == 2)
          {    
            document.getElementById('flexArmG_plot12').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot12'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot12') ;
          }
          else if(zscore_FlexArmG < 3 && zscore_FlexArmG > 2)
          {    
            document.getElementById('flexArmG_plot13').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot13'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot13') ;
          } 
          else if(zscore_FlexArmG == 3)
          {    
            document.getElementById('flexArmG_plot14').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot14'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot14') ;
          }
          else if(zscore_FlexArmG > 3)
          {    
            document.getElementById('flexArmG_plot15').style.opacity = "1" ;          
            if(start8 !== undefined)
            {
             connect(start8, document.getElementById('flexArmG_plot15'), "#ec6565", 1); 
            }            
            var start9 = document.getElementById('flexArmG_plot15') ;
          }          
         
        }
        

    var zscore_WaistG = document.getElementById("zscore_WaistG").innerHTML ;
     
        if(zscore_WaistG !== "")
        {
          if(zscore_WaistG < -3)
          {    
            document.getElementById('waistG_plot1').style.opacity = "1" ;  
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot1'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot1') ;
          }
          else if(zscore_WaistG == -3)
          {    
            document.getElementById('waistG_plot2').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot2'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot2') ;
          }
          else if(zscore_WaistG < -2 && zscore_WaistG > -3)
          {    
            document.getElementById('waistG_plot3').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot3'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot3') ;
          } 
          else if(zscore_WaistG == -2)
          {    
            document.getElementById('waistG_plot4').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot4'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot4') ;
          }
          else if(zscore_WaistG < -1 && zscore_WaistG > -2)
          {    
            document.getElementById('waistG_plot5').style.opacity = "1" ;   
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot5'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot5') ;
          }
          else if(zscore_WaistG == -1)
          {    
            document.getElementById('waistG_plot6').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot6'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot6') ;
          }
          else if(zscore_WaistG < 0 && zscore_WaistG > -1)
          {    
            document.getElementById('waistG_plot7').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot7'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot7') ;
          } 
          else if(zscore_WaistG == 0)
          {    
            document.getElementById('waistG_plot8').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot8'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot8') ;
          }
          else if(zscore_WaistG < 1 && zscore_WaistG > 0)
          {    
            document.getElementById('waistG_plot9').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot9'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot9') ;
          }
          else if(zscore_WaistG == 1)
          {    
            document.getElementById('waistG_plot10').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot10'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot10') ;
          }
          else if(zscore_WaistG < 2 && zscore_WaistG > 1)
          {    
            document.getElementById('waistG_plot11').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot11'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot11') ;
          } 
          else if(zscore_WaistG == 2)
          {    
            document.getElementById('waistG_plot12').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot12'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot12') ;
          }
          else if(zscore_WaistG < 3 && zscore_WaistG > 2)
          {    
            document.getElementById('waistG_plot13').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot13'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot13') ;
          } 
          else if(zscore_WaistG == 3)
          {    
            document.getElementById('waistG_plot14').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot14'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot14') ;
          }
          else if(zscore_WaistG > 3)
          {    
            document.getElementById('waistG_plot15').style.opacity = "1" ;          
            if(start9 !== undefined)
            {
             connect(start9, document.getElementById('waistG_plot15'), "#ec6565", 1); 
            }            
            var start10 = document.getElementById('waistG_plot15') ;
          }          
         
        }
        
        
    var zscore_HipG = document.getElementById("zscore_HipG").innerHTML ;
     
        if(zscore_HipG !== "")
        {
          if(zscore_HipG < -3)
          {    
            document.getElementById('hipG_plot1').style.opacity = "1" ;  
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot1'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot1') ;
          }
          else if(zscore_HipG == -3)
          {    
            document.getElementById('hipG_plot2').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot2'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot2') ;
          }
          else if(zscore_HipG < -2 && zscore_HipG > -3)
          {    
            document.getElementById('hipG_plot3').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot3'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot3') ;
          } 
          else if(zscore_HipG == -2)
          {    
            document.getElementById('hipG_plot4').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot4'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot4') ;
          }
          else if(zscore_HipG < -1 && zscore_HipG > -2)
          {    
            document.getElementById('hipG_plot5').style.opacity = "1" ;   
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot5'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot5') ;
          }
          else if(zscore_HipG == -1)
          {    
            document.getElementById('hipG_plot6').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot6'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot6') ;
          }
          else if(zscore_HipG < 0 && zscore_HipG > -1)
          {   
            document.getElementById('hipG_plot7').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot7'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot7') ;
          } 
          else if(zscore_HipG == 0)
          {    
            document.getElementById('hipG_plot8').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot8'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot8') ;
          }
          else if(zscore_HipG < 1 && zscore_HipG > 0)
          {    
            document.getElementById('hipG_plot9').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot9'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot9') ;
          }
          else if(zscore_HipG == 1)
          {    
            document.getElementById('hipG_plot10').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot10'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot10') ;
          }
          else if(zscore_HipG < 2 && zscore_HipG > 1)
          {    
            document.getElementById('hipG_plot11').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot11'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot11') ;
          } 
          else if(zscore_HipG == 2)
          {    
            document.getElementById('hipG_plot12').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot12'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot12') ;
          }
          else if(zscore_HipG < 3 && zscore_HipG > 2)
          {    
            document.getElementById('hipG_plot13').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot13'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot13') ;
          } 
          else if(zscore_HipG == 3)
          {    
            document.getElementById('hipG_plot14').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot14'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot14') ;
          }
          else if(zscore_HipG > 3)
          {    
            document.getElementById('hipG_plot15').style.opacity = "1" ;          
            if(start10 !== undefined)
            {
             connect(start10, document.getElementById('hipG_plot15'), "#ec6565", 1); 
            }            
            var start11 = document.getElementById('hipG_plot15') ;
          }          
         
        }        
  
  
    var zscore_CalfG = document.getElementById("zscore_CalfG").innerHTML ;
     
        if(zscore_CalfG !== "")
        {
          if(zscore_CalfG < -3)
          {    
            document.getElementById('calfG_plot1').style.opacity = "1" ; 
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot1'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG == -3)
          {    
            document.getElementById('calfG_plot2').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot2'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < -2 && zscore_CalfG > -3)
          {    
            document.getElementById('calfG_plot3').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot3'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_CalfG == -2)
          {    
            document.getElementById('calfG_plot4').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot4'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < -1 && zscore_CalfG > -2)
          {    
            document.getElementById('calfG_plot5').style.opacity = "1" ;   
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot5'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG == -1)
          {    
            document.getElementById('calfG_plot6').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot6'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < 0 && zscore_CalfG > -1)
          {   
            document.getElementById('calfG_plot7').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot7'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_CalfG == 0)
          {    
            document.getElementById('calfG_plot8').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot8'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < 1 && zscore_CalfG > 0)
          {    
            document.getElementById('calfG_plot9').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot9'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG == 1)
          {    
            document.getElementById('calfG_plot10').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot10'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < 2 && zscore_CalfG > 1)
          {    
            document.getElementById('calfG_plot11').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot11'), "#ec6565", 1);            
            }            
          } 
          else if(zscore_CalfG == 2)
          {    
            document.getElementById('calfG_plot12').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot12'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG < 3 && zscore_CalfG > 2)
          {    
            document.getElementById('calfG_plot13').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot13'), "#ec6565", 1);             
            }            
          } 
          else if(zscore_CalfG == 3)
          {    
            document.getElementById('calfG_plot14').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot14'), "#ec6565", 1);             
            }            
          }
          else if(zscore_CalfG > 3)
          {    
            document.getElementById('calfG_plot15').style.opacity = "1" ;          
            if(start11 !== undefined)
            {
             connect(start11, document.getElementById('calfG_plot15'), "#ec6565", 1);             
            }            
          }          
         
        }
        
        
    var zscore_Humerus = document.getElementById("zscore_Humerus").innerHTML ;
     
        if(zscore_Humerus !== "")
        {
          if(zscore_Humerus < -3)
          {    
            document.getElementById('humerus_plot1').style.opacity = "1" ;             
            var start12 = document.getElementById('humerus_plot1') ;
          }
          else if(zscore_Humerus == -3)
          {    
            document.getElementById('humerus_plot2').style.opacity = "1" ;           
            var start12 = document.getElementById('humerus_plot2') ;
          }
          else if(zscore_Humerus < -2 && zscore_Humerus > -3)
          {    
            document.getElementById('humerus_plot3').style.opacity = "1" ;     
            var start12 = document.getElementById('humerus_plot3') ;
          } 
          else if(zscore_Humerus == -2)
          {    
            document.getElementById('humerus_plot4').style.opacity = "1" ;         
            var start12 = document.getElementById('humerus_plot4') ;
          }
          else if(zscore_Humerus < -1 && zscore_Humerus > -2)
          {    
            document.getElementById('humerus_plot5').style.opacity = "1" ;     
            var start12 = document.getElementById('humerus_plot5') ;
          }
          else if(zscore_Humerus == -1)
          {    
            document.getElementById('humerus_plot6').style.opacity = "1" ;         
            var start12 = document.getElementById('humerus_plot6') ;
          }
          else if(zscore_Humerus < 0 && zscore_Humerus > -1)
          {   
            document.getElementById('humerus_plot7').style.opacity = "1" ;          
            var start12 = document.getElementById('humerus_plot7') ;
          } 
          else if(zscore_Humerus == 0)
          {    
            document.getElementById('humerus_plot8').style.opacity = "1" ;     
            var start12 = document.getElementById('humerus_plot8') ;
          }
          else if(zscore_Humerus < 1 && zscore_Humerus > 0)
          {    
            document.getElementById('humerus_plot9').style.opacity = "1" ;      
            var start12 = document.getElementById('humerus_plot9') ;
          }
          else if(zscore_Humerus == 1)
          {    
            document.getElementById('humerus_plot10').style.opacity = "1" ;         
            var start12 = document.getElementById('humerus_plot10') ;
          }
          else if(zscore_Humerus < 2 && zscore_Humerus > 1)
          {    
            document.getElementById('humerus_plot11').style.opacity = "1" ;       
            var start12 = document.getElementById('humerus_plot11') ;
          } 
          else if(zscore_Humerus == 2)
          {    
            document.getElementById('humerus_plot12').style.opacity = "1" ;        
            var start12 = document.getElementById('humerus_plot12') ;
          }
          else if(zscore_Humerus < 3 && zscore_Humerus > 2)
          {    
            document.getElementById('humerus_plot13').style.opacity = "1" ;        
            var start12 = document.getElementById('humerus_plot13') ;
          } 
          else if(zscore_Humerus == 3)
          {    
            document.getElementById('humerus_plot14').style.opacity = "1" ;    
            var start12 = document.getElementById('humerus_plot14') ;
          }
          else if(zscore_Humerus > 3)
          {    
            document.getElementById('humerus_plot15').style.opacity = "1" ;    
            var start12 = document.getElementById('humerus_plot15') ;
          }          
         
        }
        
        
    var zscore_Femur = document.getElementById("zscore_Femur").innerHTML ;
     
        if(zscore_Femur !== "")
        {
          if(zscore_Femur < -3)
          {    
            document.getElementById('femur_plot1').style.opacity = "1" ;  
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot1'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot1') ;
          }
          else if(zscore_Femur == -3)
          {    
            document.getElementById('femur_plot2').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot2'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot2') ;
          }
          else if(zscore_Femur < -2 && zscore_Femur > -3)
          {    
            document.getElementById('femur_plot3').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot3'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot3') ;
          } 
          else if(zscore_Femur == -2)
          {    
            document.getElementById('femur_plot4').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot4'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot4') ;
          }
          else if(zscore_Femur < -1 && zscore_Femur > -2)
          {    
            document.getElementById('femur_plot5').style.opacity = "1" ;   
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot5'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot5') ;
          }
          else if(zscore_Femur == -1)
          {    
            document.getElementById('femur_plot6').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot6'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot6') ;
          }
          else if(zscore_Femur < 0 && zscore_Femur > -1)
          {   
            document.getElementById('femur_plot7').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot7'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot7') ;
          } 
          else if(zscore_Femur == 0)
          {    
            document.getElementById('femur_plot8').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot8'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot8') ;
          }
          else if(zscore_Femur < 1 && zscore_Femur > 0)
          {    
            document.getElementById('femur_plot9').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot9'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot9') ;
          }
          else if(zscore_Femur == 1)
          {    
            document.getElementById('femur_plot10').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot10'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot10') ;
          }
          else if(zscore_Femur < 2 && zscore_Femur > 1)
          {    
            document.getElementById('femur_plot11').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot11'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot11') ;
          } 
          else if(zscore_Femur == 2)
          {    
            document.getElementById('femur_plot12').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot12'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot12') ;
          }
          else if(zscore_Femur < 3 && zscore_Femur > 2)
          {    
            document.getElementById('femur_plot13').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot13'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot13') ;
          } 
          else if(zscore_Femur == 3)
          {    
            document.getElementById('femur_plot14').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot14'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot14') ;
          }
          else if(zscore_Femur > 3)
          {    
            document.getElementById('femur_plot15').style.opacity = "1" ;          
            if(start12 !== undefined)
            {
             connect(start12, document.getElementById('femur_plot15'), "#ec6565", 1); 
            }            
            var start13 = document.getElementById('femur_plot15') ;
          }          
         
        }        
  
}); 

</script> 


<script>

 function connect(div1, div2, color, thickness) {
    var off1 = getOffset(div1);
    var off2 = getOffset(div2);
    // bottom right
    var xhalf1 = off1.width;
    var x1 = off1.left + (xhalf1 / 2);        
    var y1 = off1.top + off1.height;
    
    // top right
    var xhalf2 = off2.width;
    var x2 = off2.left + (xhalf2 / 2);
    var y2 = off2.top;
    // distance
    var length = Math.sqrt(((x2-x1) * (x2-x1)) + ((y2-y1) * (y2-y1)));
    // center
    var cx = ((x1 + x2) / 2) - (length / 2);
    var cy = ((y1 + y2) / 2) - (thickness / 2);
    // angle
    var angle = Math.atan2((y1-y2),(x1-x2))*(180/Math.PI);
    // make hr
    var htmlLine = "<div style='padding:0px; margin:0px; border-bottom:1px dashed " + color + "; line-height:1px; position:absolute; left:" + cx + "px; top:" + cy + "px; width:" + length + "px; -moz-transform:rotate(" + angle + "deg); -webkit-transform:rotate(" + angle + "deg); -o-transform:rotate(" + angle + "deg); -ms-transform:rotate(" + angle + "deg); transform:rotate(" + angle + "deg);' />";
    //
    //alert(htmlLine);
    document.body.innerHTML += htmlLine; 
}

function getOffset(el) {
    var rect = el.getBoundingClientRect();
    return {
        left: rect.left + window.pageXOffset,
        top: rect.top + window.pageYOffset,
        width: rect.width || el.offsetWidth,
        height: rect.height || el.offsetHeight
    };
}   
    
</script>
    
</body>
</html>
