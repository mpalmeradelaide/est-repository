<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="<?php echo "$base/assets/css/"?>jquery.mCustomScrollbar.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                  <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>
                  <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>  
                <h1 class="page_head">ERROR ANALYSIS</h1> 
              </td>
              </tr>
            
            <tr>
            	<td width="29%">
                    <div class="text_list">
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis'); ?>';" id="tem" name="error_radio" value="tem" checked="checked"> Calculate TEM from raw data</p>
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis_confidence'); ?>';" id="single_measure" name="error_radio" value="single_measure"> 95% CI around a single measure</p><!--<img src="<?php echo "$base/$image"?>/red_dot.png">-->
                        <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_real_change'); ?>';" id="real_change" name="error_radio" value="real_change"> Has a real change ocurred?</p>
                    </div>
                    
                    <div class="note_text">
                    	<strong>notes</strong>
                        <p>Enter the raw data from Trial 1 in the first field, and the matching data from Trial 2 in the second field. Use the 'tab' key to move from cell to cell. There must be an equal number of pairs. There is an option to calculate your TEM using 3 datasets in which case enter raw data from Trial 3 in the third field. Click on the COMPUTE button to show the calculated TEM results.</p>
                    </div>
                </td>
                <td width="46%" class="error_select">
                    <div class="select_div">
                    	<p>Trial 1</p>
                        <p id="list1_items" style="color:#c60003;"></p>
                        <div class="Scroll_content">
                        <ul id="ulscroller1">
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">
                    	<p>Trial 2</p>
                        <p id="list2_items" style="color:#c60003;"></p>
                        <div class="Scroll_content">
                    	<ul id="ulscroller2">
                        	<li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">
                    	<p>Trial 3</p>
                        <p id="list3_items" style="color:#c60003;"></p>
                        <div class="Scroll_content">
                    	<ul id="ulscroller3">
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                            <li><input type="text"></li>
                        </ul> 
                        </div>
                    </div>
                </td>
                <td width="25%">
                	<div class="calculation_box">
                        <p>Correlation = <label id="correlation"></label></p>
                        <p>TEM = <label id="tem_value"></label></p>
                        <p>TEM% = <label id="tem_percent"></label></p> 
                    </div>
                    <button id="compute" name="compute" class="plot_btn compute_btn">Compute</button>
                </td>
            </tr>            
            
            <tr>
              <td align="center" colspan="3">
                  <button id="clear" name="clear" onclick="clear_columns();" class="plot_btn compute_btn">Clear columns</button>
              </td>        
            </tr>
          </tbody>
        </table>
    </div>
	
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- custom scrollbar plugin -->
<script src="<?php echo "$base/assets/js/"?>jquery.mCustomScrollbar.concat.min.js"></script>

<script>
	(function($){
		$(window).load(function(){
			
			$(".Scroll_content").mCustomScrollbar({
				snapAmount:40,
				scrollButtons:{enable:true},
				keyboard:{scrollAmount:40},
				mouseWheel:{deltaFactor:40},
				scrollInertia:400
			});
			
		});
	})(jQuery);
</script>
<script type="text/javascript">

        $(document).on('click','#exit', function(){
          window.location.href = "<?php  echo site_url('Body/index'); ?>" ;
            //return false;
        });	
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('focusin', '#ulscroller1 li:last-child', function(){
          $("#ulscroller1").append('<li><input type="text"></li>');         
        });
        
        $(document).on('focusin', '#ulscroller2 li:last-child', function(){
          $("#ulscroller2").append('<li><input type="text"></li>');         
        });
        
        $(document).on('focusin', '#ulscroller3 li:last-child', function(){
          $("#ulscroller3").append('<li><input type="text"></li>');         
        }); 



  
  // Get counts of all values for each trial columns

        $(document).on('keyup', '#ulscroller1 li input', function(){
          var items1 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var values1 = $(this).val();	
            if(values1 !== "")
              { 
                  items1.push(values1);
              }
            }); 
            
            document.getElementById("list1_items").innerHTML = "["+items1.length+"]" ;
        });


        $(document).on('keyup', '#ulscroller2 li input', function(){
          var items2 = []; 
            
            $("#ulscroller2 li input").each(function() {
            var values2 = $(this).val();	
            if(values2 !== "")
              { 
                  items2.push(values2);
              }
            });
            
             document.getElementById("list2_items").innerHTML = "["+items2.length+"]" ;
        });
        
        $(document).on('keyup', '#ulscroller3 li input', function(){
          var items3 = []; 
            
            $("#ulscroller3 li input").each(function() {
            var values3 = $(this).val();	
            if(values3 !== "")
              { 
                  items3.push(values3);
              }
            }); 
            
            document.getElementById("list3_items").innerHTML = "["+items3.length+"]" ;
        });
        
        
               
// Check for equal number of Pairs for all the 3 Trials

        $(document).on('click','#compute', function(){
            
            var trial1 = []; var trial2 = []; var trial3 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var list1 = $(this).val();	
            if(list1 !== "")
              { 
                  trial1.push(list1);
              }
            });
            
            $("#ulscroller2 li input").each(function() {
            var list2 = $(this).val();	
            if(list2 !== "")
              { 
                  trial2.push(list2);
              }
            });
                        
            
            $("#ulscroller3 li input").each(function() {
            var list3 = $(this).val();	
            if(list3 !== "")
              { 
                  trial3.push(list3);
              }
            });
            
            //if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
      
      
         if(trial3.length == 0)
         {   
            if(trial1.length == trial2.length && trial1.length !== 0  && trial2.length !== 0)
            {
                //alert("All equal Pairs");
                get_2tem() ;
            }
            else
            {
                if(trial1.length == 0  && trial2.length == 0)
                {
                    alert("Please enter Trial values");
                }
                else
                {
                    alert("You must have equal number of pairs");
                }
                
            }
        }
        else
        {
            if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
               {
                   //alert("All equal Pairs");
                   get_3tem() ;
               }
               else
               {
                   if(trial1.length == 0  && trial2.length == 0 && trial3.length == 0)
                   {
                       alert("Please enter Trial values");
                   }
                   else
                   {
                       alert("You must have equal number of pairs");
                   }

               }   
        }
            
        });
  
  
    function clear_columns()
    {
        var trial1 = []; var trial2 = []; var trial3 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var list1 = $(this).val();	
            if(list1 !== "")
              { 
                  trial1.push(list1);
              }
            });
            
            $("#ulscroller2 li input").each(function() {
            var list2 = $(this).val();	
            if(list2 !== "")
              { 
                  trial2.push(list2);
              }
            });
                        
            
            $("#ulscroller3 li input").each(function() {
            var list3 = $(this).val();	
            if(list3 !== "")
              { 
                  trial3.push(list3);
              }
            });
        
            var index ;        
            for(index = 1; index <= trial1.length ; index +=1)
            {
                $("#ulscroller1 li input").val("");               
            }

            for(index = 1; index <= trial2.length ; index +=1)
            {
                $("#ulscroller2 li input").val("");               
            }

            for(index = 1; index <= trial3.length ; index +=1)
            {
                $("#ulscroller3 li input").val("");                
            }
            
              
            $('#list1_items').empty();
            $('#list2_items').empty();
            $('#list3_items').empty();
            $('#tem_value').empty(); 
            $('#correlation').empty();
            $('#tem_percent').empty();
    }

  // TEM calculation with 2 trial Columns
    function get_2tem()
    {
       var trial1 = [] ; // Trial1 values
       var trial2 = [] ; // Trial2 values

      // Sum of Trial1 values  
       var sum1 = 0;
       var squareSum1 = 0 ;
        $("#ulscroller1 li input").each(function() {
          var items1 = $(this).val();	
          if(items1 !== "")
          { 
            sum1 = sum1 + parseFloat(items1) ;
            squareSum1 = squareSum1 + (parseFloat(items1) * parseFloat(items1)) ;
            trial1.push(items1);  
          }
         }); 


      // Sum of Trial2 values      
       var sum2 = 0;    
       var squareSum2 = 0 ;
        $("#ulscroller2 li input").each(function() {
          var items2 = $(this).val();	
          if(items2 !== "")
          { 
            sum2 = sum2 + parseFloat(items2) ;
            squareSum2 = squareSum2 + (parseFloat(items2) * parseFloat(items2)) ;
            trial2.push(items2);  
          }
         }); 

         var N1 = trial1.length ;
         var N2 = trial2.length ;     

         var Mean1 = sum1 / N1 ;
         var Mean2 = sum2 / N2 ;     

         var SumTot = sum1 + sum2 ;
         var GrandMean = SumTot / (N1 + N2) ;         

         var SumSquaredTot = squareSum1 + squareSum2 ;

         var SST = SumSquaredTot - ((SumTot * SumTot) / (N1 * 2));      

         var P = [] ;
         var index ;
         for(index = 0; index < trial1.length; ++index) 
         {
            P[index] = parseFloat(trial1[index]) + parseFloat(trial2[index]) ;
         }

         var SumPSquared = 0 ;   
         for(index = 0; index < P.length; ++index) 
         {
           SumPSquared = SumPSquared + (P[index] * P[index]) ;    
         } 

         var SSB = SumPSquared / 2 - (SumTot * SumTot) / (2 * N1) ;

         var MSW = (SST - SSB) / N1 ;
         var MSB = SSB / (N1 - 1) ;       

         var TEM = Math.sqrt(MSW) ; // TEM value  
         var TEM_Percent = TEM / GrandMean * 100 // % TEM value         
         var Correlation = ((MSB - MSW) / (MSB + MSW)) ;  // Correlation value   


         document.getElementById("tem_value").innerHTML = Math.round(TEM * 1000) / 1000 ;   
         document.getElementById("correlation").innerHTML = Math.round(Correlation * 1000) / 1000 ; 
         document.getElementById("tem_percent").innerHTML = Math.round(TEM_Percent * 1000) / 1000 ; 
    }




  // TEM calculation with 3 trial Columns
    function get_3tem()
    {
       var trial1 = [] ; // Trial1 values
       var trial2 = [] ; // Trial2 values
       var trial3 = [] ; // Trial3 values

      // Sum of Trial1 values  
       var sum1 = 0;
       var squareSum1 = 0 ;
        $("#ulscroller1 li input").each(function() {
          var items1 = $(this).val();	
          if(items1 !== "")
          { 
            sum1 = sum1 + parseFloat(items1) ;
            squareSum1 = squareSum1 + (parseFloat(items1) * parseFloat(items1)) ;
            trial1.push(items1);  
          }
         }); 


     // Sum of Trial2 values      
       var sum2 = 0;    
       var squareSum2 = 0 ;
        $("#ulscroller2 li input").each(function() {
          var items2 = $(this).val();	
          if(items2 !== "")
          { 
            sum2 = sum2 + parseFloat(items2) ;
            squareSum2 = squareSum2 + (parseFloat(items2) * parseFloat(items2)) ;
            trial2.push(items2);  
          }
         });
         

     // Sum of Trial3 values      
       var sum3 = 0;    
       var squareSum3 = 0 ;
        $("#ulscroller3 li input").each(function() {
          var items3 = $(this).val();	
          if(items3 !== "")
          { 
            sum3 = sum3 + parseFloat(items3) ;
            squareSum3 = squareSum3 + (parseFloat(items3) * parseFloat(items3)) ;
            trial3.push(items3);  
          }
         });

         var N1 = trial1.length ;
         var N2 = trial2.length ;
         var N3 = trial3.length ;

         var Mean1 = sum1 / N1 ;
         var Mean2 = sum2 / N2 ; 
         var Mean3 = sum3 / N3 ; 

         var SumTot = sum1 + sum2 + sum3 ;
         var GrandMean = SumTot / (N1 + N2 + N3) ;         

         var SumSquaredTot = squareSum1 + squareSum2 + squareSum3 ;

         var SST = SumSquaredTot - ((SumTot * SumTot) / (N1 * 3));      

         var P = [] ;
         var index ;
         for(index = 0; index < trial1.length; ++index) 
         {
            P[index] = parseFloat(trial1[index]) + parseFloat(trial2[index]) + parseFloat(trial3[index]) ;
         }

         var SumPSquared = 0 ;   
         for(index = 0; index < P.length; ++index) 
         {
           SumPSquared = SumPSquared + (P[index] * P[index]) ;    
         } 

         var SSB = SumPSquared / 3 - (SumTot * SumTot) / (3 * N1) ;

         var MSW = (SST - SSB) / (2 * N1) ;
         var MSB = SSB / (N1 - 1) ;       

         var TEM = Math.sqrt(MSW) ; // TEM value  
         var TEM_Percent = TEM / GrandMean * 100 // % TEM value         
         var Correlation = ((MSB - MSW) / (MSB + 2 * MSW)) ;  // Correlation value   


         document.getElementById("tem_value").innerHTML = Math.round(TEM * 1000) / 1000 ;   
         document.getElementById("correlation").innerHTML = Math.round(Correlation * 1000) / 1000 ; 
         document.getElementById("tem_percent").innerHTML = Math.round(TEM_Percent * 1000) / 1000 ; 
    }
        
</script>    
</body>
</html>
