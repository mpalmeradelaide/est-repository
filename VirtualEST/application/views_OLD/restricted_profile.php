<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<link rel="stylesheet" type="text/css" href="https://corporate.aep.net.au/healthscreen//assets/css/health.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

.virtual {
 cursor: pointer;
 height: 36px;
 width: 120px;
 background: #065dcc;
 border: 0;
 color: #fff;
 border-radius: 2px;
 border: 2px solid #0253bb;
 box-shadow: 1px 2px 5px rgba(0,0,0,0.5);
 font-size: 14px;   
}
</style>
</head>
<body>
<div class="wrapper">
    <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/restricted_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?>  

    <div class="mid container">
        <table class="graph_table" width="100%">
          <tbody>
            <tr>
              <td colspan="3" align="center" class="name_field_td position_r">                
                <a class="strength_btns" id="exit"><img src="<?php echo "$base/$image"?>/power_icon.png" alt=""><p>Back</p></a>                 
                <a onclick="window.print(); return false;" class="strength_btns"><img src="<?php echo "$base/$image"?>/print_icon.png" alt=""><p>Print</p></a>   
                <h1 class="page_head">RESTRICTED profile</h1>   
              </td>
              </tr>
            <tr>
              <td align="center">
                <label for="name">Name</label>
                <input type="text" id="name" name="name" class="cus-input" value="<?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name'] ;?>"> 
              </td> 
			  
              <td align="center">
                <label>Gender</label>
                <input type="text" id="gender" name="gender" class="cus-input" value="<?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>"> 
              </td>             
              
              <td align="center">
                <label>Age (yr)</label>
                <input type="text" id="age" name="age" class="cus-input" value="<?php if(!empty($fieldData[0]->age)){echo $fieldData[0]->age;}else{echo $_SESSION['age'] ;}?>"> 
              </td>        
            </tr>
            
            <tr>
              <td colspan="3"> 
                <table>
                    <tr>
                      <td align="center" class="sml_text_field" width="336px">
                        <label>Height</label>
                        <input type="text" id="height" name="height" class="cus-input" value="<?php echo isset($fieldData[0]->height)?$fieldData[0]->height:""; ?>" style="width:180px;"> 
                        cm
                      </td>
                      
                      <td align="center" class="sml_text_field" width="324px">
                        <label>Body mass</label>
                        <input type="text" id="body_mass" name="body_mass" class="cus-input" value="<?php echo isset($fieldData[0]->body_mass)?$fieldData[0]->body_mass:""; ?>"> 
                        kg
                      </td>   
                      
                      <td align="center" class="sml_text_field" width="240px">
                        <button id="virtual_profile" name="virtual_profile" class="virtual">Virtual Profile</button>
                        <button id="clear_profile" name="clear_profile" class="virtual"  style="display:none;">Clear</button>
                      </td>
                      
                    </tr>                    
                </table>
              </td>                             
            </tr>
            
            <tr>                
                <td colspan="3" align="center"> 
                	<table width="90%">
                      <tbody>
                        <tr>
                          <td valign="top">
                          		<table width="100%" class="tabel_bord" cellspacing="0">
                                  	<thead>
                                    <tr>
                                      <td>Skinfolds</td>
                                      <td>mm</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td>Triceps</td>
                                      <td><input type="text" id="triceps" name="triceps" class="cus-input width_96" value="<?php echo isset($fieldData[0]->triceps)?$fieldData[0]->triceps:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Subscapular</td>
                                      <td><input type="text" id="subscapular" name="subscapular" class="cus-input width_96" value="<?php echo isset($fieldData[0]->subscapular)?$fieldData[0]->subscapular:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Biceps</td>
                                      <td><input type="text" id="biceps" name="biceps" class="cus-input width_96" value="<?php echo isset($fieldData[0]->biceps)?$fieldData[0]->biceps:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Iliac crest</td>
                                      <td><input type="text" id="iliac_crest" name="iliac_crest" class="cus-input width_96" value="<?php echo isset($fieldData[0]->iliac_crest)?$fieldData[0]->iliac_crest:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Supraspinale</td>
                                      <td><input type="text" id="supraspinale" name="supraspinale" class="cus-input width_96" value="<?php echo isset($fieldData[0]->supraspinale)?$fieldData[0]->supraspinale:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Abdominal</td>
                                      <td><input type="text" id="abdominal" name="abdominal" class="cus-input width_96" value="<?php echo isset($fieldData[0]->abdominal)?$fieldData[0]->abdominal:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Front thigh</td>
                                      <td><input type="text" id="thigh" name="thigh" class="cus-input width_96" value="<?php echo isset($fieldData[0]->thigh)?$fieldData[0]->thigh:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Medial calf</td>
                                      <td><input type="text" id="calf" name="calf" class="cus-input width_96" value="<?php echo isset($fieldData[0]->calf)?$fieldData[0]->calf:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Mid-axilla</td>
                                      <td><input type="text" id="mid_axilla" name="mid_axilla" class="cus-input width_96" value="<?php echo isset($fieldData[0]->mid_axilla)?$fieldData[0]->mid_axilla:""; ?>"> </td>
                                    </tr>                                
                                  </tbody>
                            </table>
                          </td>
                          <td valign="top">
                                <table width="100%" class="tabel_bord" cellspacing="0">
                                    <thead>                                  
                                    <tr>
                                      <td>Girths</td>
                                      <td>cm</td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <tr>
                                      <td>Arm (relaxed)</td>
                                      <td><input type="text" id="relArmG" name="relArmG" class="cus-input width_96" value="<?php echo isset($fieldData[0]->relArmG)?$fieldData[0]->relArmG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Arm (flexed)</td>
                                      <td><input type="text" id="flexArmG" name="flexArmG" class="cus-input width_96" value="<?php echo isset($fieldData[0]->flexArmG)?$fieldData[0]->flexArmG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Waist (minimum)</td>
                                      <td><input type="text" id="waistG" name="waistG" class="cus-input width_96" value="<?php echo isset($fieldData[0]->waistG)?$fieldData[0]->waistG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Gluteal (hips)</td>
                                      <td><input type="text" id="hipG" name="hipG" class="cus-input width_96" value="<?php echo isset($fieldData[0]->hipG)?$fieldData[0]->hipG:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Calf (maximum)</td>
                                      <td><input type="text" id="calfG" name="calfG" class="cus-input width_96" value="<?php echo isset($fieldData[0]->calfG)?$fieldData[0]->calfG:""; ?>"> </td>
                                    </tr>
                                  </tbody>
                                </table>

                          </td>
                          <td valign="top">
                          	<table width="100%" class="tabel_bord" cellspacing="0">
                                    <thead>
                                    <tr>
                                      <td>Breadths</td>
                                      <td>cm</td>
                                    </tr>
                                    </thead>
                                   <tbody>
                                    <tr>
                                      <td>Humerus</td>
                                      <td><input type="text" id="humerus" name="humerus" class="cus-input width_96" value="<?php echo isset($fieldData[0]->humerus)?$fieldData[0]->humerus:""; ?>"> </td>
                                    </tr>
                                    <tr>
                                      <td>Femur</td>
                                      <td><input type="text" id="femur" name="femur" class="cus-input width_96" value="<?php echo isset($fieldData[0]->femur)?$fieldData[0]->femur:""; ?>"> </td>
                                    </tr>
                                   </tbody>
                                </table>
                          </td>
                        </tr>
                      </tbody>
                    </table>
                </td>
            </tr>
            
            <tr>
              <td align="left">
              	<!--<a href="#" class="rest_bot_btns"><span>&nbsp;</span> imperial</a> 
                <a href="#" class="rest_bot_btns"><span><img src="<?php echo "$base/$image"?>/cross.png"></span> metric</a> -->
              </td> 
			  
              <td align="right" colspan="2">                  
		 <div class="bottom_icons" id="phantom" name="phantom" onclick="getPhantomZscore()"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                 <div class="bottom_icons" id="body_fat" name="body_fat" onclick="getBodyFat()"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                 <div class="bottom_icons" id="skinfold" name="skinfold" onclick="getSkinfolds()"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                 <div class="bottom_icons" id="somatotype" name="somatotype" onclick="getSomatotype()"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                 <div class="bottom_icons" id="anthropometry" name="anthropometry" onclick="getAnthropometry()"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <!--<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>-->
              </td>        
            </tr>
            <tr>
              <td align="left" colspan="3">&nbsp;</td>        
            </tr>
          </tbody>
        </table>
        
        <input type="hidden" id="zscore_Triceps" name="zscore_Triceps">
        <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular">
        <input type="hidden" id="zscore_Biceps" name="zscore_Biceps">
        <input type="hidden" id="zscore_Iliac" name="zscore_Iliac">
        <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale">
        <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal">
        <input type="hidden" id="zscore_Thigh" name="zscore_Thigh">
        <input type="hidden" id="zscore_Calf" name="zscore_Calf">
        <input type="hidden" id="zscore_RelArmG" name="zscore_RelArmG">
        <input type="hidden" id="zscore_FlexArmG" name="zscore_FlexArmG">
        <input type="hidden" id="zscore_WaistG" name="zscore_WaistG">
        <input type="hidden" id="zscore_HipG" name="zscore_HipG">
        <input type="hidden" id="zscore_CalfG" name="zscore_CalfG">
        <input type="hidden" id="zscore_Humerus" name="zscore_Humerus">
        <input type="hidden" id="zscore_Femur" name="zscore_Femur">      
               
    </div>

    <input type="hidden" id="exit_key" name="exit_key" value="">
    <input type="hidden" id="action_key" name="action_key" value="">
    <input type="hidden" id="profile" name="profile" value="restricted">
        
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
    
         $(window).bind("load", function() {
             
          if(document.getElementById("height").value !== "" && 
          document.getElementById("body_mass").value !== "" &&           
          document.getElementById("triceps").value !== "" &&    
          document.getElementById("subscapular").value !== "" &&    
          document.getElementById("biceps").value !== "" &&    
          document.getElementById("iliac_crest").value !== "" &&    
          document.getElementById("supraspinale").value !== "" &&    
          document.getElementById("abdominal").value !== "" &&    
          document.getElementById("thigh").value !== "" &&
          document.getElementById("calf").value !== "" &&    
          document.getElementById("mid_axilla").value !== "" &&    
          document.getElementById("relArmG").value !== "" &&    
          document.getElementById("flexArmG").value !== "" &&    
          document.getElementById("waistG").value !== "" &&    
          document.getElementById("hipG").value !== "" &&    
          document.getElementById("calfG").value !== "" &&    
          document.getElementById("humerus").value !== "" &&    
          document.getElementById("femur").value !== "")
          {      
           document.getElementById('clear_profile').style.display = "" ;
           document.getElementById('virtual_profile').style.display = "none" ;
          }          
          
         }); 

    	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
        });	

        document.getElementById("phantom").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("body_fat").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("skinfold").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("virtual_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("clear_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
     	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
	});
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','#virtual_profile', function(){ 
          
          document.getElementById("height").value = 179.3 ; 
          document.getElementById("body_mass").value = 74.84 ;             
          document.getElementById("triceps").value = 5 ;    
          document.getElementById("subscapular").value = 8.6 ;    
          document.getElementById("biceps").value = 3.5 ;    
          document.getElementById("iliac_crest").value = 6.5 ;    
          document.getElementById("supraspinale").value = 3.4 ;    
          document.getElementById("abdominal").value = 4.7 ;    
          document.getElementById("thigh").value = 9.2 ;
          document.getElementById("calf").value = 5.4 ;    
          document.getElementById("mid_axilla").value = 4.5 ;    
          document.getElementById("relArmG").value = 33.1 ;    
          document.getElementById("flexArmG").value = 35.5 ;    
          document.getElementById("waistG").value = 75.2 ;    
          document.getElementById("hipG").value = 98 ;    
          document.getElementById("calfG").value = 42 ;    
          document.getElementById("humerus").value = 7.43 ;    
          document.getElementById("femur").value = 10.97 ;    
          
          document.getElementById('clear_profile').style.display = "" ;
          document.getElementById('virtual_profile').style.display = "none" ; 
         
        });
        
        $(document).on('click','#clear_profile', function(){ 
          
          document.getElementById("height").value = "" ; 
          document.getElementById("body_mass").value = "" ;             
          document.getElementById("triceps").value = "" ;    
          document.getElementById("subscapular").value = "" ;    
          document.getElementById("biceps").value = "" ;    
          document.getElementById("iliac_crest").value = "" ;    
          document.getElementById("supraspinale").value = "" ;    
          document.getElementById("abdominal").value = "" ;    
          document.getElementById("thigh").value = "" ;
          document.getElementById("calf").value = "" ;    
          document.getElementById("mid_axilla").value = "" ;    
          document.getElementById("relArmG").value = "" ;    
          document.getElementById("flexArmG").value = "" ;    
          document.getElementById("waistG").value = "" ;    
          document.getElementById("hipG").value = "" ;    
          document.getElementById("calfG").value = "" ;    
          document.getElementById("humerus").value = "" ;    
          document.getElementById("femur").value = "" ;    
         
          document.getElementById('virtual_profile').style.display = "" ;
          document.getElementById('clear_profile').style.display = "none" ; 
        });
        
</script>  

<script>   
   //calculate phantom z-score value
    function getPhantomZscore()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

           if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || relArmG == "" || flexArmG == "" || waistG == "" || hipG == "" || calfG == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 1 ;
            document.forms["myform"].submit();	  
           }  
          else
          { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 1 ;
            document.forms["myform"].submit();	  
          }   
        }
    }
 
 
   //calculate Body Fat value
    function getBodyFat()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || relArmG == "" || flexArmG == "" || waistG == "" || hipG == "" || calfG == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 2 ;
            document.forms["myform"].submit();
           }  
          else
          { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 2 ;
            document.forms["myform"].submit();
          }  

        }
    }
 

 //calculate skinfolds value
    function getSkinfolds()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       
            var mid_axilla = document.getElementById("mid_axilla").value ;            
            
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 
            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || mid_axilla == "")
            {
               alert ("All Skinfolds values should be entered");
               //return false;
            }  
            else
            {
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur); 

               document.getElementById("exit_key").value = 0 ;
               document.getElementById("action_key").value = 3 ;
               document.forms["myform"].submit();              
            }        
        }
    }
    


 //calculate somatotype value
    function getSomatotype()
    {          
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       
            var mid_axilla = document.getElementById("mid_axilla").value ;            
            
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 
            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if (triceps == "" || subscapular == "" || supraspinale == "" || calf == "" || flexArmG == "" || calfG == "" || humerus == "" || femur == "")
            {
             alert ("Some values are missing");          
            }  
            else
            {
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur); 

               document.getElementById("exit_key").value = 0 ;
               document.getElementById("action_key").value = 4 ;
               document.forms["myform"].submit();              
            }        
        }
    } 
    
   
//calculate anthropometry value
    function getAnthropometry()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
        else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ; 
            var calfG = document.getElementById("calfG").value ; 

            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

           if (triceps == "" || subscapular == "" || thigh == "" || relArmG == "" || waistG == "" || hipG == "")
           {
             alert ("Some values are missing");          
           }  
           else
           { 
            phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur);                             

            document.getElementById("exit_key").value = 0 ;
            document.getElementById("action_key").value = 5 ;
            document.forms["myform"].submit();	  
           }   
        }
    }
    
   
     function phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,relArmG,flexArmG,waistG,hipG,calfG,humerus,femur)
     {
            ht = Math.round(parseFloat(ht) * 10) / 10 ;  //upto 2 decimal places  
            mass = Math.round(parseFloat(mass) * 100) / 100 ;  //upto 2 decimal places
              
            triceps = Math.round(parseFloat(triceps) * 10) / 10 ;
            subscapular = Math.round(parseFloat(subscapular) * 10) / 10 ;
            biceps = Math.round(parseFloat(biceps) * 10) / 10 ;
            iliac = Math.round(parseFloat(iliac) * 10) / 10 ;
            supraspinale = Math.round(parseFloat(supraspinale) * 10) / 10 ;
            abdominal = Math.round(parseFloat(abdominal) * 10) / 10 ;
            thigh = Math.round(parseFloat(thigh) * 10) / 10 ;
            calf = Math.round(parseFloat(calf) * 10) / 10 ;
            relArmG = Math.round(parseFloat(relArmG) * 10) / 10 ;
            flexArmG = Math.round(parseFloat(flexArmG) * 10) / 10 ;
            waistG = Math.round(parseFloat(waistG) * 10) / 10 ;
            hipG = Math.round(parseFloat(hipG) * 10) / 10 ;
            calfG = Math.round(parseFloat(calfG) * 10) / 10 ;
            humerus = Math.round(parseFloat(humerus) * 100) / 100 ; //upto 2 decimal places
            femur = Math.round(parseFloat(femur) * 100) / 100 ; //upto 2 decimal places
            
            
        // Skinfolds Phantom Zscores    
            var zscore_Triceps = Math.round([(parseFloat(triceps) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
            var zscore_Subscapular = Math.round([(parseFloat(subscapular) * (170.18 / parseFloat(ht)) - 17.2) / 5.07] * 10) / 10 ;
            var zscore_Biceps= Math.round([(parseFloat(biceps) * (170.18 / parseFloat(ht)) - 8) / 2] * 10) / 10 ;
            var zscore_Iliac = Math.round([(parseFloat(iliac) * (170.18 / parseFloat(ht)) - 22.4) / 6.8] * 10) / 10 ;
            var zscore_Supspinale = Math.round([(parseFloat(supraspinale) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
            var zscore_Abdominal = Math.round([(parseFloat(abdominal) * (170.18 / parseFloat(ht)) - 25.4) / 7.78] * 10) / 10 ;
            var zscore_Thigh = Math.round([(parseFloat(thigh) * (170.18 / parseFloat(ht)) - 27) / 8.33] * 10) / 10 ;
            var zscore_Calf = Math.round([(parseFloat(calf) * (170.18 / parseFloat(ht)) - 16) / 4.67] * 10) / 10 ;

        // Girths Phantom Zscores    
            var zscore_RelArmG = Math.round([(parseFloat(relArmG) * (170.18 / parseFloat(ht)) - 26.89) / 2.33] * 10) / 10 ;
            var zscore_FlexArmG = Math.round([(parseFloat(flexArmG) * (170.18 / parseFloat(ht)) - 29.41) / 2.37] * 10) / 10 ;
            var zscore_WaistG = Math.round([(parseFloat(waistG) * (170.18 / parseFloat(ht)) - 71.91) / 4.45] * 10) / 10 ;
            var zscore_HipG = Math.round([(parseFloat(hipG) * (170.18 / parseFloat(ht)) - 94.67) / 5.58] * 10) / 10 ;
            var zscore_CalfG = Math.round([(parseFloat(calfG) * (170.18 / parseFloat(ht)) - 35.25) / 2.3] * 10) / 10 ;

        // Breadths Phantom Zscores
            var zscore_Humerus = Math.round([(parseFloat(humerus) * (170.18 / parseFloat(ht)) - 6.48) / 0.35] * 10) / 10 ;
            var zscore_Femur = Math.round([(parseFloat(femur) * (170.18 / parseFloat(ht)) - 9.52) / 0.48] * 10) / 10 ;	  

            document.getElementById("zscore_Triceps").value = parseFloat(zscore_Triceps) ;
            document.getElementById("zscore_Subscapular").value = parseFloat(zscore_Subscapular) ;
            document.getElementById("zscore_Biceps").value = parseFloat(zscore_Biceps) ;
            document.getElementById("zscore_Iliac").value = parseFloat(zscore_Iliac) ;
            document.getElementById("zscore_Supspinale").value = parseFloat(zscore_Supspinale) ;
            document.getElementById("zscore_Abdominal").value = parseFloat(zscore_Abdominal) ;
            document.getElementById("zscore_Thigh").value = parseFloat(zscore_Thigh) ;
            document.getElementById("zscore_Calf").value = parseFloat(zscore_Calf) ;
            document.getElementById("zscore_RelArmG").value = parseFloat(zscore_RelArmG) ;
            document.getElementById("zscore_FlexArmG").value = parseFloat(zscore_FlexArmG) ;
            document.getElementById("zscore_WaistG").value = parseFloat(zscore_WaistG) ;
            document.getElementById("zscore_HipG").value = parseFloat(zscore_HipG) ;
            document.getElementById("zscore_CalfG").value = parseFloat(zscore_CalfG) ;
            document.getElementById("zscore_Humerus").value = parseFloat(zscore_Humerus) ;
            document.getElementById("zscore_Femur").value = parseFloat(zscore_Femur) ;
     }    
    
</script>
    
</body>
</html>
