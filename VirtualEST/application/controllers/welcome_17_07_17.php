<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Welcome extends CI_Controller {


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
    
   public $firstname ,$lastname ,$userid ,$dob,$country,$occupation,$gender;
    
        function __construct()
        {
            error_reporting(1);
             ini_set('memory_limit', '512M'); 
             parent::__construct();
             $this->load->helper('form');
             $this->load->helper('url');
             $this->load->model('client_model');
             $this->load->library('menu');
             session_start();
           

            
        }
	public function index($c =0)
	{
        error_reporting(0);
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $_SESSION['userid'] = 0;
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['gender'] = 'M';
        
        $data['projects'] = $this->client_model->getAllProjects();
        
       
        $this->load->view('client_detail', $data);

      
    }
        
    
    public function VirtualpersonGeneration()
    {
     $data['randomdata'] = $this->client_model->generateRandomValues();   
     error_reporting(0);
        $menu = new Menu;
        $data['menu'] = $menu->show_menu();
        $data['css'] = $this->config->item('css');
        $data['base'] = $this->config->item('base_url');
        $data['image'] = $this->config->item('images');
        $data['health_logo'] = $this->config->item('health_logo');
        $data['exercise_logo'] = $this->config->item('exercise_logo');
        $data['projects'] = $this->client_model->getAllProjects();
        $this->load->view('client_detail', $data);
    }
    
/*	public function clientDetail()
	{
            $code = $this->input->post('code');
            $check = $this->client_model->isCodeExist($code);
            if($check){
            $_SESSION['userid'] = 0;
            //  $this->session->set_userdata('userid', 0);

             $data['css'] = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['gender'] = 'M';
             $data['projects'] = $this->client_model->getAllProjects();
             $this->load->view('client_detail',$data);
            }else{
                $c=1;
                $this->index($c);        
               
            }
	}
*/        
        
        function saveClientInfo()
        {
        $this->load->library('form_validation');
                
        $this->form_validation->set_rules('first_name', 'First Name', 'required');
		$this->form_validation->set_rules('last_name', 'Last Name', 'required');		
		$this->form_validation->set_rules('daydropdown', 'Day of Birth', 'required|callback_daydropdown_check');
		$this->form_validation->set_rules('monthdropdown', 'Month of Birth', 'required|callback_monthdropdown_check');
		$this->form_validation->set_rules('yeardropdown', 'Year of Birth', 'required|callback_yeardropdown_check');		
		//$this->form_validation->set_rules('country', 'country', 'required|callback_country_check');
		//$this->form_validation->set_rules('occupation', 'occupation', 'required|callback_occupation_check');
		

		if ($this->form_validation->run() == TRUE)
		{
                      
			    $num_of_rows =$this->client_model->save();
                           
							$dob = $this->input->post('daydropdown')."/".$this->input->post('monthdropdown')."/".$this->input->post('yeardropdown');
						
						   if($num_of_rows>0){
                               $menu = new Menu;
                               $data['menu'] = $menu->show_menu();
                               $data['css']        = $this->config->item('css');
                               $data['base']       = $this->config->item('base_url');
							   $data['image']       = $this->config->item('images');
						       $data['health_logo']       = $this->config->item('health_logo');	
							   $data['exercise_logo']       = $this->config->item('exercise_logo');
                               $this->userid =  $num_of_rows;
                               $_SESSION['userid'] = $this->userid;
                               $data['id']       = $_SESSION['userid'];
                               
                          //  var_dump($_SESSION);
                         
                             //  $this->session->set_userdata('userid',$this->userid);
                              // $this->session->set_userdata('user_first_name', $this->input->post('first_name'));
                             //  $this->session->set_userdata('user_last_name', $this->input->post('last_name'));
                             //  $this->session->set_userdata('user_dob', $dob);
                            //   $this->session->set_userdata('user_gender', $dob);
                            //   $this->session->set_userdata('user_daydropdown', $this->input->post('daydropdown'));
                            //   $this->session->set_userdata('user_monthdropdown', $this->input->post('monthdropdown'));
                            //   $this->session->set_userdata('user_yeardropdown', $this->input->post('yeardropdown'));
                            //   $this->session->set_userdata('user_gender', $this->input->post('gender'));
                             //  $this->session->set_userdata('user_country', $this->input->post('country'));
                             //  $this->session->set_userdata('user_occupation', $this->input->post('occupation'));
                              $_SESSION['user_first_name']= $this->input->post('first_name');
                              $_SESSION['user_last_name']= $this->input->post('last_name');
                              $_SESSION['user_dob']= $dob;
                              $_SESSION['user_daydropdown']= $this->input->post('daydropdown');
                              $_SESSION['user_monthdropdown']= $this->input->post('monthdropdown');
                              $_SESSION['user_yeardropdown']= $this->input->post('yeardropdown');
                              $_SESSION['user_gender']= $this->input->post('gender');
                              $_SESSION['user_country']= $this->input->post('country');
                              $_SESSION['user_occupation']= $this->input->post('occupation');
                              $_SESSION['p_name']= $this->input->post('p_name');
                              $_SESSION['p_desc']= $this->input->post('p_desc');
                              $_SESSION['email']= $this->input->post('email');
                             
                              
                               $data['image']       = $this->config->item('images');
                               $data['firstName']       =$this->input->post('first_name');
                               $data['lastName']       = $this->input->post('last_name');
                               $this->firstname =$this->input->post('first_name');
                               $this->lastname =$this->input->post('last_name');
                                $this->dob =$dob;
                                $this->gender =$this->input->post('gender');
                                $this->country =$this->input->post('country');
                                $this->occupation =$this->input->post('occupation');
                                $this->p_name =$this->input->post('p_name');
                                $this->p_desc =$this->input->post('p_desc');
                                $this->email =$this->input->post('email');


                               $data['screening'] = $this->client_model->fetchScreeningInfo();
                               $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
    
                                if (isset($_POST['mysubmit1']))  
                               {
                                   $this->load->view('client_medical_history', $data);
                                }
                               elseif ( isset($_POST['mysubmit2']) )   
                               {
                                  $this->load->view('tests', $data);
                               }
							   elseif ( isset($_POST['mysubmit3']) )   
                               {
                                  $this->load->view('body_tests', $data);
                               }
                              

                            }else{
                                 echo "failure";
                            }
		}
		else
		{
            $data['css']        = $this->config->item('css');
            $data['base'] = $this->config->item('base_url');
            $data['image'] = $this->config->item('images');
            $data['health_logo'] = $this->config->item('health_logo');
            $data['exercise_logo'] = $this->config->item('exercise_logo');
            $data['firstName'] = $this->input->post('first_name');
            $data['lastName'] = $this->input->post('last_name');
            $data['gender'] = $this->input->post('gender');
            $data['country'] = $this->input->post('country');
            $data['occupation'] = $this->input->post('occupation');
            $data['daydropdown'] = $this->input->post('daydropdown');
            $data['monthdropdown'] = $this->input->post('monthdropdown');
            $data['yeardropdown'] = $this->input->post('yeardropdown');
            $data['dob'] = $this->input->post('dob');
            $data['p_name'] = $this->input->post('p_name');
            $data['p_desc'] = $this->input->post('p_desc');
            $data['email'] = $this->input->post('email');
            $data['projects'] = $this->client_model->getAllProjects();



            $this->load->view('client_detail',$data);
		}
                
                
            
              
        } 
        
        function clearClientInfo()
            {
						 $data['css']        = $this->config->item('css');
                         $data['base']       = $this->config->item('base_url');
                         $data['image']       = $this->config->item('images');
						 $data['health_logo']       = $this->config->item('health_logo');	
						 $data['exercise_logo']       = $this->config->item('exercise_logo');
                         $data['firstName']       = "";
                         $data['lastName']       = "";
                         $data['gender']       = "";
                         $data['country']       = "";
                         $data['occupation']       = "";
                         $data['daydropdown']       = "";
                         $data['monthdropdown']       = "";
                         $data['yeardropdown']       = "";
                         $data['dob']       = "";
                         $data['email']       = "";
                         $data['p_name']       = "";
                         $data['p_desc']       = "";
                        $this->load->view('client_detail',$data);
		}
   
         
         function saveClientMedicalInfo()
         {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->client_model->saveMedicalInfo();
           //  print_r($this->input->post());
      
            
                if($num_of_rows>0){
                 
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //  $data['id']       = $this->session->userdata('userid');
                    $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitMedical']))   {
                     //$num_of_rows =$this->client_model->saveMedicalInfo();
                  //    $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
               //    $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                  // $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                 //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                    $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                  
                   
                     $this->load->view('client_detail',$data);

                  } 
                    
                  
                   //$this->load->view('client_medical_history', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
        
        
        function saveClientPhysicalActivityInfo()
         {
            
           $num_of_rows =$this->client_model->savePhysicalActivityInfo();
      
                if($num_of_rows>0){
                   
                    
                   $menu = new Menu;
                   $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_risk_factor_info');
                    
                    
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                   //print_r($data); 
                   //die(__FILE__);
                   //$this->load->view('client_physical_activity', $data);
                    
                     if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitPhysicalActivity']))   {
                    // $num_of_rows =$this->client_model->savePhysicalActivityInfo();
                  //    $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
               //    $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
               //    $data['occupation']       = $this->session->userdata('user_occupation');
              //     $data['country']       = $this->session->userdata('user_country');
                   
                    
                   
                     $this->load->view('client_detail',$data);

                  } 
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function saveClientRiskFactorInfo()
         {
            // print_r($this->input->post());
          // die;
             $num_of_rows =$this->client_model->saveRiskFactorInfo();
         //$num_of_rows=1;
                if($num_of_rows>0){
                  
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                //  $data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                     $data['image']       = $this->config->item('images');
                      $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                    //$this->load->view('client_risk_factor', $data);
                   
                    if (isset($_POST['mysubmit1'])) {
                    $this->saveClientInfoLink();

                  } elseif (isset($_POST['mysubmit2'])) {
                     $this->saveClientMedicalInfoLink();

                  } elseif (isset($_POST['mysubmit3'])) {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif (isset($_POST['mysubmit4'])) {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif (isset($_POST['mysubmit5'])) {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6'])) {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitRiskFactor'])) {
                    
               //       $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                  // $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                //   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                   
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                   $data['projects'] = $this->client_model->getAllProjects();
                   
                   $this->load->view('client_detail',$data);

                  }                  

                }
                else
                {
                  echo "failure";
                }
     
        }
        
        function saveClientBodyCompositionInfo()
         {
            
            $num_of_rows =$this->client_model->saveBodyCompositionInfo();
            $num_of_rows=1;
                if($num_of_rows>0){
                   
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  //$data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                  $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                  // $this->load->view('client_body_composition', $data);
                    if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitBodyComposition']))   {
                    
                 //  $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                //   $data['dob']       = $this->session->userdata('user_dob');
                //   $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                //   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                  
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                      
                     $this->load->view('client_detail',$data);

                  } 
                  /*
                  elseif ( isset($_POST['mysubmit7']) )   {
                    $this->fetchClientInfoLink();

                  }
                  
                   */
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function saveClientMeditationInfo()
         {
           // $this->input->post['medicalConditions'];
            //print_r($this->input->post());
            //die;
             $num_of_rows =$this->client_model->saveMeditationInfo();
         //$num_of_rows=1;
                if($num_of_rows>0){
              
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                    $data['css']        = $this->config->item('css');
                    $data['base']       = $this->config->item('base_url');
                   // $data['id']       = $this->session->userdata('userid');
                    $data['id']       = $_SESSION['userid'];
                    $data['image']       = $this->config->item('images');
                    $data['firstName']       =$this->firstname;
                    $data['lastName']       = $this->lastname;
                  $data['screening'] = $this->client_model->fetchScreeningInfo();
               
                  // $this->load->view('client_medication', $data);
                  
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitMedication']))   {
                    
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                  // $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                 //  $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                 //  $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                //   $data['gender']       = $this->session->userdata('user_gender');
                //   $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                      
                      $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                   
                     $this->load->view('client_detail',$data);

                  }
                  

                }else{
                     echo "failure";
                }
     
        }
        
        function fetchScreenInfo()
         {
		 
			
             //$this->client_model->fetchScreeningInfo();
			
            // $num_of_rows =$this->client_model->saveRiskFactorInfo();
				$num_of_rows=1;
                if($num_of_rows>0)
				{
                  
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                  $data['id']       = $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
				   
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
				   //print_r($data);
				    
                 //  $data['screening'] = $this->client_model->fetchScreeningInfo();
                   if (isset($_POST['mysubmit1']))   {
                    $this->saveClientInfoLink();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )  
				  {
				 
                    $this->saveClientBodyCompositionInfoLink();
					 //echo "fetchScreeningInfo";die;

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }elseif (isset($_POST['submitScreening']))   {
                    
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                  // $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                  // $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                  // $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                  // $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                 //  $data['country']       = $this->session->userdata('user_country');
                   
                        $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
					
                     $this->load->view('client_detail',$data);

                  }
                   
              //$this->load->view('client_screening', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
        
   public function country_check()
    {
            if ($this->input->post('country') === 'selectcountry')  {
            $this->form_validation->set_message('country_check', 'Please choose your Country.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function occupation_check()
    {
            if ($this->input->post('occupation') === 'selectoccupation')  {
            $this->form_validation->set_message('occupation_check', 'Please choose your Occupation.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function daydropdown_check()
    {
            if ($this->input->post('daydropdown') === 'dd')  {
            $this->form_validation->set_message('daydropdown_check', 'Please choose your Day of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function monthdropdown_check()
    {
            if ($this->input->post('monthdropdown') === 'mm')  {
            $this->form_validation->set_message('monthdropdown_check', 'Please choose your Month of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
   public function yeardropdown_check()
    {
            if ($this->input->post('yeardropdown') === '0000')  {
            $this->form_validation->set_message('yeardropdown_check', 'Please choose your Year of Birth.');
            return FALSE;
        }
        else {
            return TRUE;
        }
    }
    
    function saveClientInfoLink()
        {
         
                               $data['css']        = $this->config->item('css');
                               $data['base']       = $this->config->item('base_url');
                              // $data['id']       =  $this->session->userdata('userid');
                               $data['id']       =  $_SESSION['userid'];
                               $data['image']       = $this->config->item('images');
                               $data['screening'] = $this->client_model->fetchScreeningInfo();
                               $data['fieldData'] = $this->client_model->fetchDetail('client_medical_info');
                               $this->load->view('client_medical_history', $data);
        }
        
    function saveClientMedicalInfoLink()
        {
         
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   //$data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                    $data['screening'] = $this->client_model->fetchScreeningInfo();
                    $data['fieldData'] = $this->client_model->fetchDetail('client_physical_activity_info');
                   $this->load->view('client_physical_activity', $data);
        } 
    function saveClientPhysicalActivityInfoLink()
         {
       
                   
                  
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 //  $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetail('client_risk_factor_info');
                   $this->load->view('client_risk_factor', $data);
         }   
         
      function saveClientRiskFactorInfoLink()
         {
              
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   //$data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['screening'] = $this->client_model->fetchScreeningInfo();  
                   $data['fieldData'] = $this->client_model->fetchDetail('client_body_composition_info');
                   $this->load->view('client_body_composition', $data);

     
        }   
        
         function saveClientBodyCompositionInfoLink()
         {
		
            
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                  // $data['id']       = $this->session->userdata('userid');;
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
				   
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetail('client_medication_info');
				  //print_r($data);die;
                   $this->load->view('client_medication', $data);
				    
				    

                
     
        }
        function fetchScreenInfoLink()
         {
          //  $this->client_model->fetchScreeningInfo();
            // $num_of_rows =$this->client_model->saveRiskFactorInfo();
         $num_of_rows=1;
                if($num_of_rows>0){
                   
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['myClass'] = $this;
                 
                   
                   $this->load->view('client_screening', $data);
                   

                }else{
                     echo "failure";
                }
     
        }
       
        
         function fetchClientInfoLink()
         {
                 $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                   $data['id']       = $this->session->userdata('userid');
                   $data['image']       = $this->config->item('images');
                   $data['firstName']       =$this->firstname;
                   $data['lastName']       = $this->lastname;
                   $data['screening'] = $this->client_model->fetchScreeningInfo();
                   $data['fieldData'] = $this->client_model->fetchDetailFromClientInfo('client_info');
                   $this->firstname =$this->input->post('first_name');
                   $this->lastname =$this->input->post('last_name');
                  /*
                   $data['firstName']       =$this->session->userdata('user_first_name');
                   $data['lastName']       = $this->session->userdata('user_last_name');
                   $data['dob']       = $this->session->userdata('user_dob');
                   $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                   $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                   $data['gender']       = $this->session->userdata('user_gender');
                   $data['occupation']       = $this->session->userdata('user_occupation');
                   $data['country']       = $this->session->userdata('user_country');
                  */ 
                     $this->load->view('client_detail',$data);
                 
        
             
         }
       
        function fetchClientInfo()
         {
            
             if (isset($_POST['submitMedical']))   {
                     $num_of_rows =$this->client_model->saveMedicalInfo();

                  } elseif ( isset($_POST['mysubmit2']) )   {
                     $this->saveClientMedicalInfoLink();

                  } elseif ( isset($_POST['mysubmit3']) )   {
                   $this->saveClientPhysicalActivityInfoLink();

                  } elseif ( isset($_POST['mysubmit4']) )   {
                    $this->saveClientRiskFactorInfoLink();

                  }elseif ( isset($_POST['mysubmit5']) )   {
                    $this->saveClientBodyCompositionInfoLink();

                  }elseif ( isset($_POST['mysubmit6']) )   {
                    $this->fetchScreenInfoLink();

                  }
            
                   
                    
               //       $menu = new Menu;
               //    $data['menu'] = $menu->show_menu();
                   $data['css']        = $this->config->item('css');
                   $data['base']       = $this->config->item('base_url');
                 // $data['id']       = $this->session->userdata('userid');
                    $data['id']       =  $_SESSION['userid'];
                   $data['image']       = $this->config->item('images');
                   $this->firstname =$this->input->post('first_name');
                   $this->lastname =$this->input->post('last_name');
                  
                  // $data['firstName']       =$this->session->userdata('user_first_name');
                 //  $data['lastName']       = $this->session->userdata('user_last_name');
                 //  $data['dob']       = $this->session->userdata('user_dob');
                 //  $data['daydropdown']       = $this->session->userdata('user_daydropdown');
                 //  $data['monthdropdown']       = $this->session->userdata('user_monthdropdown');
                //   $data['yeardropdown']       = $this->session->userdata('user_yeardropdown');
                 //  $data['gender']       = $this->session->userdata('user_gender');
                 //  $data['occupation']       = $this->session->userdata('user_occupation');
                //   $data['country']       = $this->session->userdata('user_country');
                   
                    $data['firstName']       =$_SESSION['user_first_name'];
                   $data['lastName']       =  $_SESSION['user_last_name'];
                   $data['dob']       = $_SESSION['user_dob'];
                   $data['daydropdown']       = $_SESSION['user_daydropdown'];
                   $data['monthdropdown']       = $_SESSION['user_monthdropdown'];
                   $data['yeardropdown']       =  $_SESSION['user_yeardropdown'];
                   $data['gender']       = $_SESSION['user_gender'];
                   $data['occupation']       = $_SESSION['user_occupation'];
                   $data['country']       = $_SESSION['user_country'];
                   $data['p_name']       = $_SESSION['p_name'];
                   $data['p_desc']       = $_SESSION['p_desc'];
                   $data['email']       = $_SESSION['email'];
                    $data['projects'] = $this->client_model->getAllProjects();
                   
                     $this->load->view('client_detail',$data);
                  // $data['screening'] = $this->client_model->fetchScreeningInfo();
                  
                   //print_r($data); 
                  //
          
                   

               
     
        }
        
/*
         function updateClientInfo($rf)
         {
            $this->client_model->updateRiskFactorInfo($rf) ;
         }
*/

  function updateClientInfo($rf,$variable)
         {
            $this->client_model->updateRiskFactorInfo($rf,$variable) ;
         }
    

function updateMedicationInfo()
         {		
	    $column=$_GET['column'];
	    $status=$_GET['val'];
		  
            $this->client_model->updateMedicationInfo($column,$status) ;
         }    
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */