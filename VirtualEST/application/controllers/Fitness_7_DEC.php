<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Fitness extends CI_Controller {

    public function __construct()
   {  
    // Call the Controller constructor
        
            error_reporting(1);
            ini_set('memory_limit', '512M');
            parent::__construct();
			$this->load->helper(array('form', 'url','common'));
			$this->load->library(array('session','form_validation'));  
            $this->load->model('fitness_model','',TRUE);
            $this->load->helper('text');               
	        $this->load->library('menu');
            session_start();
			
			 $date = str_replace('/', '-', $_SESSION['user_dob']);                      
			 $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
			 $dob = strtotime( $dateOfBirth);						 
			 $tdate = strtotime(date('Y-m-d'));			
			 $_SESSION['age'] = $this->getAge($dob, $tdate) ;
			 
			 if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
			 {
				 $_SESSION['age_range'] = '18-29' ;
			 }
			 elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
			 {
				 $_SESSION['age_range'] = '30-39' ;
			 }
			 elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
			 {
				 $_SESSION['age_range'] = '40-49' ;
			 }
			 elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
			 {
				 $_SESSION['age_range'] = '50-59' ;
			 }
			 elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
			 {
				 $_SESSION['age_range'] = '60-69' ;
			 }
			 elseif($_SESSION['age'] >=70)
			 {
				 $_SESSION['age_range'] = '70+' ;
			 }
			
  }  
	
	public function index() {   
	
			 $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
			 
             $this->load->view('tests',$data);	    
	 
	}
	
	function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                        ++$age;
                }
                return $age;
        }
        
        
        public function get_view_ajax_male()
        {          
            $sports_array = $this->fitness_model->all_sports_male();            
         
            echo "<option>Sports</option>";
            
            foreach($sports_array as $val)
             {
              echo "<option>".$val->sport."</option>";
             }
        }

         public function get_view_ajax_female()
        {           
            $sports_array = $this->fitness_model->all_sports_female();            
        
            echo "<option>Sports</option>";
            
            foreach($sports_array as $val)
              {
               echo "<option>".$val->sport."</option>";
              }
        }


        public function vertical_jump()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;			 
						 
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;                                    
                         $data['id']       = $_SESSION['userid'];  						 
                         $data['fieldData'] = $this->fitness_model->fetchDetail('verticaljump_test');
                         $this->load->view('vertical_jump',$data);   
         }
    
	public function flight_time()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges 
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
						 $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('flightcontact_test');                         
                         $this->load->view('flight_time',$data);	   
         }
	

         public function peak_power()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges 
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
                         $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('peakpower_test');
                         $this->load->view('peak_power',$data);	   
         }
	
        public function strength()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						 $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('strength');
                         
                         $this->load->view('strength',$data);	   
         }
	
       public function total_work_done_30s()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;
                         $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('total_work_30s');
                                            
                         $this->load->view('30s_total_work_done',$data);	   
         }
         
      public function submaximal_test()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						 $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('submaximal');
                                                  
                         $this->load->view('3x3min_submaximal_test',$data);	   
         }
	
       public function shuttle_test()
         {
                         $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;     
                         $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('shuttle_test');
                         $this->load->view('shuttle_test',$data);	   
         }
         
      
       public function v02max_test()
         {
                        $tests_array  = $this->fitness_model->all_tests(); // get all tests
                         $sports_array = $this->fitness_model->all_sports_male(); // get all comparison sports of male
                         $sports_array_female = $this->fitness_model->all_sports_female(); // get all comparison sports of females
                         $age_array  = $this->fitness_model->all_ages(); // get all age ranges                  
                         $prob_array  = $this->fitness_model->all_ranges(); // get all probability ranges  
                         //print_r(array_reverse($prob_array)); die;
                         
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         $data['tests_array'] = $tests_array ;
                         $data['sports_array'] = $sports_array ;
                         $data['sports_array_female'] = $sports_array_female ;
                         $data['age_array'] = $age_array ;                 
                         $data['prob_array'] = array_reverse($prob_array) ; // array of probability ranges  
                         $data['age_array'] = $age_array ;                                                   
                         $data['id']       = $_SESSION['userid'];          
                         $data['fieldData'] = $this->fitness_model->fetchDetail('vo2max_test');                
                         $this->load->view('v02max_test',$data);	   
         }
         
           public function lactate_threshold()
         {
                         $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                         
                         $this->load->view('lactate_threshold',$data);	   
         }
		 
		 
       function saveClientVo2MaxInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveVo2MaxInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                            
                        if($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }
                        elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }
        
        function saveClientVerticalJumpInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveVerticalJumpInfo();
                    
                if($num_of_rows>0){
					
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                 
                                           
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        }                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }
                        elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }                 




                     }else{
                          echo "failure";
                     }
     
        }
		

        function saveClientFlightContactInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->saveFlightContactInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                        
					     $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                            
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }
		
		
        function saveClientPeakPowerInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->savePeakPowerInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                                                  
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }						
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }		
		
        function saveClientShuttleInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->saveShuttleInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                        
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
                            
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }                        
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "30s_total_work_done")
                        {
                            $this->total_work_done_30s();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }


		function saveClientStrengthInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveStrengthInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						 
                        $this->load->view('tests', $data); 
						
                     }else{
                          echo "failure";
                     }
		}



		function saveClientSubmaximalInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
            
             //print_r($this->input->post()); die;
             $num_of_rows =$this->fitness_model->saveSubmaximalInfo();           
                
                if($num_of_rows>0){                    
                          
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						 
                        $this->load->view('tests', $data); 
						
                     }else{
                          echo "failure";
                     }
		}


        function saveClient30sTotalWorkInfo()
        {
// var_dump($this->session->all_userdata());
// die(__FILE__);
             $num_of_rows =$this->fitness_model->save30sTotalWorkInfo();
            // print_r($this->input->post()); die;
      
            
                if($num_of_rows>0){                    
                                                  
						 $data['css']  = $this->config->item('css');
                         $data['base'] = $this->config->item('base_url');
                         $data['image'] = $this->config->item('images');
						
                        if($this->input->post('test') == "v02max_test")
                        {
                            $this->v02max_test();
                        } 
						elseif($this->input->post('test') == "vertical_jump")
                        {
                            $this->vertical_jump();
                        }                                        
                        elseif($this->input->post('test') == "strength")
                        {
                            $this->strength();
                        }
                        elseif($this->input->post('test') == "shuttle_test")
                        {
                            $this->shuttle_test();
                        }
						elseif($this->input->post('test') == "flight_time")
                        {
                            $this->flight_time();
                        }						
                        elseif($this->input->post('test') == "lactate_threshold")
                        {
                            $this->lactate_threshold();
                        }                        
                        elseif($this->input->post('test') == "peak_power")
                        {
                            $this->peak_power();
                        } 
                        elseif($this->input->post('test') == "3x3min_submaximal_test")
                        {
                            $this->submaximal_test();
                        }
                        else
                        {
                            $this->load->view('tests', $data); 
                        }            

                     }else{
                          echo "failure";
                     }
     
        }				
         
}
