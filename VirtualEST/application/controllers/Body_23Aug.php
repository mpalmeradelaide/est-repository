<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Body extends CI_Controller {

   public function __construct()
   {  
    // Call the Controller constructor
        
            error_reporting(1);
            ini_set('memory_limit', '512M');
            parent::__construct();
            $this->load->helper(array('form', 'url','common'));
	    $this->load->library(array('session','form_validation'));  
            $this->load->model('body_model','',TRUE);
            $this->load->helper('text');               
            $this->load->library('menu');
            session_start();
            
            $date = str_replace('/', '-', $_SESSION['user_dob']);                      
	    $dateOfBirth=   date("Y-m-d", strtotime( $date) ); 
	    $dob = strtotime( $dateOfBirth);						 
	    $tdate = strtotime(date('Y-m-d'));			
	    $_SESSION['age'] = $this->getAge($dob, $tdate) ;
            
            if($_SESSION['age'] >= 18 && $_SESSION['age'] <=29)
             {
                 $_SESSION['age_range'] = '18-29' ;
             }
             elseif($_SESSION['age'] >= 30 && $_SESSION['age'] <=39)
             {
                 $_SESSION['age_range'] = '30-39' ;
             }
             elseif($_SESSION['age'] >= 40 && $_SESSION['age'] <=49)
             {
		 $_SESSION['age_range'] = '40-49' ;
             }
             elseif($_SESSION['age'] >= 50 && $_SESSION['age'] <=59)
             {
		 $_SESSION['age_range'] = '50-59' ;
             }
             elseif($_SESSION['age'] >= 60 && $_SESSION['age'] <=69)
             {
		 $_SESSION['age_range'] = '60-69' ;
             }
             elseif($_SESSION['age'] >=70)
             {
		 $_SESSION['age_range'] = '70+' ;
             }
    }  
 
    function getAge( $dob, $tdate )
        {
                $age = 0;
                while( $tdate > $dob = strtotime('+1 year', $dob))
                {
                  ++$age;
                }
                return $age;
        }
	
    public function index() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
			 
             $this->load->view('body_tests',$data);	    
	 
	}	
	
    public function restricted_profile() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('restricted_profile'); 
             $this->load->view('restricted_profile',$data);	    
	 
	}

    public function full_profile() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('full_profile'); 
             $this->load->view('full_profile',$data);	     
	 
	}			
	
    public function error_analysis() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
			 
             $this->load->view('error_analysis',$data);	    
	 
	}	
	
    public function error_analysis_confidence() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('error_confidence');              
			 
             $this->load->view('error_analysis_confidence',$data);	    
	 
	}
        
        
        
    public function error_real_change() {   
	
             $data['css']  = $this->config->item('css');
             $data['base'] = $this->config->item('base_url');
             $data['image'] = $this->config->item('images');
             $data['id']   = $_SESSION['userid'];          
             $data['fieldData'] = $this->body_model->fetchDetail('error_real_change');       
			 
             $this->load->view('error_real_change',$data);	    
	 
	}
        
         
    public function skinfold_Map() {   
	
            $data['css']  = $this->config->item('css');
            $data['base'] = $this->config->item('base_url');
            $data['image'] = $this->config->item('images');
             
            $triceps = $this->input->get("triceps") ;
            $subscapular = $this->input->get("subscapular") ;
            $illiac = $this->input->get("illiac") ;
            $thigh = $this->input->get("thigh") ;    
            $height = $this->input->get("height") ;    
            $body_mass = $this->input->get("body_mass") ;    
            
            $data['skinfolds'] = array("triceps"=>$triceps,"subscapular"=>$subscapular,"illiac"=>$illiac,"thigh"=>$thigh,"height"=>$height,"body_mass"=>$body_mass) ;             
            if($this->input->get("profile") == 'restricted') 
            {                
             $this->load->view('skinfold_Map_restricted',$data);	    
            }
            elseif($this->input->get("profile") == 'full') 
            {    
             $this->load->view('skinfold_Map_full',$data);	    
            }
	}	        



    public function restricted_actions() {   
	
           //Check if user exits the screen 
             if($this->input->post("exit_key") == 1)
             {
                 $num_of_rows =$this->body_model->saveRestrictedInfo();           
                
                 if($num_of_rows>0)
                  {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       $this->load->view('body_tests', $data);                     
                  }
                  else
                  {
                     echo "failure";
                  }            
                               	   
             }
             else
             {
                $num_of_rows =$this->body_model->saveRestrictedInfo(); 
                
                $zscores = $this->input->post() ;
                
                $data['css']  = $this->config->item('css');
                $data['base'] = $this->config->item('base_url');
                $data['image'] = $this->config->item('images');

              //Check button action  
                if($this->input->post("action_key") == 1) //Phantom ZScore
                { 
                  $data['phantom_Zscores'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"zscore_Triceps"=>$zscores["zscore_Triceps"], "zscore_Subscapular"=>$zscores["zscore_Subscapular"], "zscore_Biceps"=>$zscores["zscore_Biceps"], "zscore_Iliac"=>$zscores["zscore_Iliac"], "zscore_Supspinale"=>$zscores["zscore_Supspinale"], "zscore_Abdominal"=>$zscores["zscore_Abdominal"], "zscore_Thigh"=>$zscores["zscore_Thigh"], "zscore_Calf"=>$zscores["zscore_Calf"], "zscore_RelArmG"=>$zscores["zscore_RelArmG"], "zscore_FlexArmG"=>$zscores["zscore_FlexArmG"], "zscore_WaistG"=>$zscores["zscore_WaistG"], "zscore_HipG"=>$zscores["zscore_HipG"], "zscore_CalfG"=>$zscores["zscore_CalfG"], "zscore_Humerus"=>$zscores["zscore_Humerus"], "zscore_Femur"=>$zscores["zscore_Femur"])  ;                                  
                  $this->load->view('phantom_restricted',$data);	                 
                }
                elseif ($this->input->post("action_key") == 2) //Body Fat 
                {  
                  $data['bodyFat_values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"], "relArmG"=>$zscores["relArmG"], "flexArmG"=>$zscores["flexArmG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;                                    
                  $this->load->view('body_fat_restricted',$data);	  
                }
                elseif ($this->input->post("action_key") == 3) //Skinfolds
                {
                  $data['skinfolds_Values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"])  ;                
                  $this->load->view('skinfolds_restricted',$data);	  
                }      
                    
             }        
	 
	}
        
        
    public function full_actions() {   
        
             if($this->input->post("exit_key") == 1)
             {               
                 $num_of_rows =$this->body_model->saveFullInfo();           
                
                 if($num_of_rows>0)
                  {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       $this->load->view('body_tests', $data);                     
                  }
                  else
                  {
                     echo "failure";
                  }                      	   
            }             
            else
            {
                $num_of_rows =$this->body_model->saveFullInfo(); 
                
                $zscores = $this->input->post() ;
                //print_r($zscores); die;

                $data['css']  = $this->config->item('css');
                $data['base'] = $this->config->item('base_url');
                $data['image'] = $this->config->item('images');

              if($this->input->post("action_key") == 1) //Phantom ZScore
              {
                $data['phantom_Zscores'] = array("height"=>$zscores["height"],
                                                 "body_mass"=>$zscores["body_mass"],
                                                 "zscore_Triceps"=>$zscores["zscore_Triceps"],
                                                 "zscore_Subscapular"=>$zscores["zscore_Subscapular"], 
                                                 "zscore_Biceps"=>$zscores["zscore_Biceps"],
                                                 "zscore_Iliac"=>$zscores["zscore_Iliac"],
                                                 "zscore_Supspinale"=>$zscores["zscore_Supspinale"], 
                                                 "zscore_Abdominal"=>$zscores["zscore_Abdominal"],
                                                 "zscore_Thigh"=>$zscores["zscore_Thigh"],
                                                 "zscore_Calf"=>$zscores["zscore_Calf"],
                                                 "zscore_HeadG"=>$zscores["zscore_HeadG"],
                                                 "zscore_NeckG"=>$zscores["zscore_NeckG"], 
                                                 "zscore_RelArmG"=>$zscores["zscore_RelArmG"],
                                                 "zscore_FlexArmG"=>$zscores["zscore_FlexArmG"],
                                                 "zscore_ForearmG"=>$zscores["zscore_ForearmG"],
                                                 "zscore_WristG"=>$zscores["zscore_WristG"],
                                                 "zscore_ChestG"=>$zscores["zscore_ChestG"],
                                                 "zscore_WaistG"=>$zscores["zscore_WaistG"],
                                                 "zscore_HipG"=>$zscores["zscore_HipG"],
                                                 "zscore_ThighG"=>$zscores["zscore_ThighG"],
                                                 "zscore_MidThighG"=>$zscores["zscore_MidThighG"],                                                 
                                                 "zscore_CalfG"=>$zscores["zscore_CalfG"],
                                                 "zscore_AnkleG"=>$zscores["zscore_AnkleG"],
                                                 "zscore_AcRad"=>$zscores["zscore_AcRad"],
                                                 "zscore_RadStyl"=>$zscores["zscore_RadStyl"],
                                                 "zscore_midStylDact"=>$zscores["zscore_midStylDact"],
                                                 "zscore_Iliospinale"=>$zscores["zscore_Iliospinale"],
                                                 "zscore_Troch"=>$zscores["zscore_Troch"],
                                                 "zscore_TrochTib"=>$zscores["zscore_TrochTib"],                                                 
                                                 "zscore_TibLat"=>$zscores["zscore_TibLat"],
                                                 "zscore_TibMed"=>$zscores["zscore_TibMed"],
                                                 "zscore_Biac"=>$zscores["zscore_Biac"],
                                                 "zscore_Bideltoid"=>$zscores["zscore_Bideltoid"],
                                                 "zscore_Billio"=>$zscores["zscore_Billio"],
                                                 "zscore_Bitrochanteric"=>$zscores["zscore_Bitrochanteric"],
                                                 "zscore_Foot"=>$zscores["zscore_Foot"],
                                                 "zscore_Sitting"=>$zscores["zscore_Sitting"],
                                                 "zscore_TrChest"=>$zscores["zscore_TrChest"],                                                 
                                                 "zscore_ApChest"=>$zscores["zscore_ApChest"],
                                                 "zscore_ArmSpan"=>$zscores["zscore_ArmSpan"],                                       
                                                 "zscore_Humerus"=>$zscores["zscore_Humerus"],
                                                 "zscore_Femur"=>$zscores["zscore_Femur"])  ;

                $this->load->view('phantom_full',$data);	                 
              }
              elseif ($this->input->post("action_key") == 2) //Body Fat 
              {                  
                  $data['bodyFat_values'] = array("height"=>$zscores["height"],"body_mass"=>$zscores["body_mass"],"triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"], "relArmG"=>$zscores["relArmG"], "flexArmG"=>$zscores["flexArmG"], "forearmG"=>$zscores["forearmG"], "wristG"=>$zscores["wristG"], "waistG"=>$zscores["waistG"], "hipG"=>$zscores["hipG"], "calfG"=>$zscores["calfG"], "humerus"=>$zscores["humerus"], "femur"=>$zscores["femur"])  ;                                    
                  $this->load->view('body_fat_full',$data);	  
                }
              elseif ($this->input->post("action_key") == 3) //Skinfolds
              {
                $data['skinfolds_Values'] = array("triceps"=>$zscores["triceps"], "subscapular"=>$zscores["subscapular"], "biceps"=>$zscores["biceps"], "iliac_crest"=>$zscores["iliac_crest"], "supraspinale"=>$zscores["supraspinale"], "abdominal"=>$zscores["abdominal"], "thigh"=>$zscores["thigh"], "calf"=>$zscores["calf"], "mid_axilla"=>$zscores["mid_axilla"])  ;                
                $this->load->view('skinfolds_full',$data);	  
              }              
                    
            }       
	 
	}
                
        
        
      function saveClientErrorConfidenceInfo()
        {        
             $num_of_rows =$this->body_model->saveErrorConfidenceInfo(); 
             
             if($num_of_rows>0)
                {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       
                        if($this->input->post("exit_key") == "tem")
                        { 
                           $this->error_analysis() ;	                      
                        }
                        else if($this->input->post("exit_key") == "single_measure")
                        { 
                           $this->error_analysis_confidence() ;	                                     
                        }
                        else if($this->input->post("exit_key") == "real_change")
                        { 
                           $this->error_real_change() ;	                                     
                        }
                        else
                        {
                            $this->index();
                        }                   
                  }
                  else
                  {
                     echo "failure";
                  }   
        }
        
        
      function saveClientErrorRealChangeInfo()
        {        
             $num_of_rows =$this->body_model->savetErrorRealChangeInfo(); 
             
             if($num_of_rows>0)
                {                  
                       $data['css']  = $this->config->item('css');
                       $data['base'] = $this->config->item('base_url');
                       $data['image'] = $this->config->item('images');
                       
                       
                        if($this->input->post("exit_key") == "tem")
                        { 
                           $this->error_analysis() ;	                      
                        }
                        else if($this->input->post("exit_key") == "single_measure")
                        { 
                           $this->error_analysis_confidence() ;	                                     
                        }
                        else if($this->input->post("exit_key") == "real_change")
                        { 
                           $this->error_real_change() ;	                                     
                        }
                        else
                        {
                            $this->index() ;
                        }                  
                  }
                  else
                  {
                     echo "failure";
                  }   
        }        
        
         
}
