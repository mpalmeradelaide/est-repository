<?php
defined('BASEPATH') OR exit('No direct script access allowed');

// TODO this is likely temporary as a solution the problem of the pre-exercise screening not retaining values on post

// our problem is that on each reload, the body composition form loads values from _SESSION. Any values entered on the
// form are removed on each reload.

// in this code is an attempt to save any changed data from the form, thus retaining data from the form. This may not work.

// alternatives may be to use ajax to do the work instead - perhaps a controller is not the best solution

// TODO declare PreExScreening elsewhere

class PreExScreening extends CI_Controller {
	public function store_form_go_med() {
		store_form();
	}

	public function store_form_go_body() {
		store_form();
	}

	private function store_form() {
		// retrieve data from form
		$form_data = $this->input->post();

		// store each item into _SESSION data
		$_SESSION['option_height'] = $form_data['options_height'];
	}
}