<?php
	include 'ChromePhp.php';

	ChromePhp::log("ajax_submit_body_composition has been loaded and run");

	/**
	 * Saving any changes that may be made on the body composition form to the session
	 * unique to this particular form.
	 *
	 * @author: Matt Palmer
	 */

	//_SESSION['']=round($_POST[''],1);
	_SESSION['option_height']=round($_POST['option_height'],1);
	_SESSION['option_weight']=round($_POST['options_weight'],1);
	_SESSION['option_height_measured']=round($_POST['option_height_measured'],1);
	_SESSION['option_weight_measured']=round($_POST['option_weight_measured'],1);
	_SESSION['option_bmi']=round($_POST['option_bmi'],1);
	_SESSION['option_waist']=round($_POST['option_waist'],1);
	_SESSION['option_hip']=round($_POST['option_hip'],1);
	_SESSION['option_whr']=round($_POST['option_whr'],1);
	_SESSION['triceps']=round($_POST['option_triceps'],1);
	_SESSION['biceps']=round($_POST['option_biceps'],1);
	_SESSION['subscapular']=round($_POST['option_subscapular'],1);
	_SESSION['option_sos']=round($_POST['option_sos'],1);

?>