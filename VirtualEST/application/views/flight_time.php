<?php 
              if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
            $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
            $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
            $bodyfatdiv="block";    
            }
            else
            {
             $bodyfatdiv="none";    
            }
             
	$is_dynamic=$_SESSION['is_virtual'];		 
			 ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<script type="text/javascript">
	window.onload = function() {
	  var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	   {
	$("#vo2Max_value").val(<?php echo $_SESSION['FTCT'];?>);   	
	   }
	  changeMean();
	};	
	$(document).ready(function() {
	   var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		  var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	   {
		$(".v_person").fadeTo( "slow" , 1, function() {}); 
		$(".v_detail").toggle();
	   }	
	 });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	$(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	
	$(document).on('click','.menu_btn', function(){
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.vitual_btn', function(){
                  $("#vpflightgen").attr("action", "<?php echo base_url(); ?>index.php/Fitness/FitnessVirtualpersonGeneration");     
                  $("#vpflightgen").submit();       
       $(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	});
</script> 
<body>

<!-- insertion of header -->
<?=$header_insert; ?>

<div class="overlay">&nbsp;</div> 
<div class="info_block">
	<div class="info_block_head green_container">Flight: Contact time ratio</div>
	<p>The FLIGHT TIME : CONTACT TIME screen allows the user to plot a F:C time ratio for different age and gender categories as well as across different sporting populations. The F:C time ratio is measured using a jump mat [an electronic mat that times the contact time on the mat as well as the time the person spends 'in flight' off the mat]. </p>
	<p>The ratio is unitless, that is, it is a number or ratio without units. It provides a very important measure of relative power and is a specific indicator of plantar flexion [calf] power [which also reflects the proportion of fast twitch fibres that have the highest rate of force development]. Power athletes will score higher in this test while endurance-type athletes will demonstrate a lower F:C time ratio. It is also a useful test to determine training adaptations when undertaking training programs and to monitor athletes for recover following heavy training or competition where there may be some tissue soreness, inflammation or damage which will result in lower [than typical] performances.</p>
	<p>The methodology involves a small step off a 30 cm high box or stepper bench on to a timing mat. It should not be a jump and both feet should hit the mat at the same time and plantar flex rapidly. The client should try to minimise the contact time while maximising the flight time with forceful and rapid plantar flexion on the mat.</p>
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>  
</div>

<div class="video_box">
	<video controls> 
		<source src="<?php echo base_url('/assets/videos/Flighttimecontacttimeratiotest.mp4'); ?>" type="video/mp4">
	</video>
	<div class="video_done_button">
		&#x274C;
	</div>
</div>

<script>
	// show and hide video modal
	var is_vid_on = false;

	$(".video_icon").click(function() {
		$(".overlay").show();
		$(".video_box").show();
		is_vid_on = true;

		// hide those dots
		$(".blue_dot").css("opacity", "0");
		$(".red_dot").css("opacity", "0");
	});

	$(".video_done_button").click(function() {
		$(".overlay").hide();
		$(".video_box").hide();
		is_vid_on = false;
		$("video").each(function () { this.pause() });

		// resurrect dots
		$(".blue_dot").css("opacity", "1");
		$(".red_dot").css("opacity", "1");
	});

	$(".overlay").click(function() {
		if (is_vid_on == true) {
			$(".overlay").hide();
			$(".video_box").hide();
			is_vid_on = false;
			$("video").each(function () { this.pause() });

			// resurrect dots
			$(".blue_dot").css("opacity", "1");
			$(".red_dot").css("opacity", "1");
		}
	});
</script>

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientFlightContactInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	
	<div class="contain">
				
        <!--Start right -->         
        <div class="right-section right-section_new">
        	<div class="right-head">Flight : Contact time ratio</div>
        	<div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label>Test</label>
                    <select name="test" onchange="check_test_legitchoice()">
                        <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic strength and power</option>                   
                        <option value="vertical_jump">Vertical jump</option>                  
                        <option value="flight_time" selected="selected">Flight : Contact time </option> 
                        <option value="peak_power">Peak power [W] </option>
                        <option value="strength">Strength </option>
                        <option value="#"></option>
                        <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic capacity</option>                   
                        <option value="30s_total_work_done">30 s total work [kJ]</option>    
						<option value="maod_test">maximal accumulated oxygen deficit (MAOD)</option>  
						
                        <option value="#"></option>
                        <option value="#" style="font-weight:bold; color: #6D6D6D;">Aerobic fitness</option>                   
                        <option value="#" style="font-weight:bold;">VO&#x0032;&#x006D;&#x0061;&#x0078; [mL/kg/min]</option>                                     
                        <option value="3x3min_submaximal_test">Submaximal </option> 
				   	    <option value="#" style="font-weight:bold; color: #6D6D6D;">Maximal</option> 
						<option value="shuttle_test">20 m shuttle</option>
						<option value="bike_test">Predicting VO2max using maximal-effort treadmill or bike tests</option>
						
						
                        <option value="#"></option>
                        <option value="v02max_test">Measured VO&#x0032;&#x006D;&#x0061;&#x0078; </option> 
                        <option value="lactate_threshold" style="font-weight:bold;">Lactate threshold </option>
                    </select>

					<script>
						// refuse submit if 'test' value is '#'
						function check_test_legitchoice() {
							test_choice = $('select[name="test"]').children("option:selected").val();
							if (test_choice != "#") {
								$("#myform").submit();
							} else {
								$('select[name="test"]').val("flight_time");
							}
						}
					</script>

                </div>
                <div class="field_24">
                	<label>Gender</label>
                    <select id="gender" name="gender" onchange="changeMean();">
                        <option value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "Selected";}?>>male</option>
                        <option value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "Selected";}?>>female</option>
                    </select>
                    <script type="text/javascript">               
					   $("#gender").change(function ()
						{
							var selectedText = $(this).find("option:selected").text();
							var selectedValue = $(this).val();
						  
							 if(selectedValue=='Male')
							 {
									$.ajax({
									type:'POST',
									url: "<?php echo base_url('index.php/Fitness/get_view_ajax_male/');?>",                   
									 success: function (response) {
										document.getElementById("sport").innerHTML=response; 
									  }
									}); 
								 }else{
								   $.ajax({
									type:'POST',
									url: "<?php echo base_url('index.php/Fitness/get_view_ajax_female/');?>",                    
									success: function (response) {
										document.getElementById("sport").innerHTML=response; 
									  }
									}); 
							}
	
						});
			 
				   </script>
                </div>
                <div class="field_24">
                	<label>Age (yr)</label>
                    <select id="age" name="age" onchange="changeMean();">
                        <?php foreach($age_array as $age){ ?>
                        <option value="<?php echo $age->age ;?>" <?php if($fieldData[0]->age == $age->age || $_SESSION['age_range'] == $age->age){echo "Selected";}?>><?php echo $age->age ;?></option>                   
                       <?php } ?>
                    </select>
                </div>
                <div class="field_24">
                	<label>Comparison Sport</label>
                    <select name="sport" id="sport" onchange="changeSportMean()">                  
                            <option>Sports</option>
                             <?php  
                                foreach($sports_array as $sport){ ?>
                                <option value="<?php echo $sport->sports ;?>" <?php if($fieldData[0]->sports == $sport->sports){echo "Selected";}?>><?php echo $sport->sports ;?></option>              
                             <?php } ?>            
                    </select>
                </div>
        	</div>
            
            <div class="field_row verticle_field_row" style="border:0;"> 
                <div class="field_24">
                	<label for="value">Value</label>
                    <input class="cus-input" type="text" name="vo2Max_value" id="vo2Max_value" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" onKeyUp="getZscore()" value="<?php echo isset($fieldData[0]->value)?$fieldData[0]->value:""; ?>">
                </div>
                
                <div class="field_24">
                	<label for="percentile">Performance (%)</label>
                    <input type="text" name="percentile" id="percentile" readonly value="<?php echo isset($fieldData[0]->percentile)?$fieldData[0]->percentile:""; ?>">
                </div> 
                
                <input id="plot" name="plot" type="button" onclick="show()" class="lite_btn grey_btn f_right btn_green" value="Plot" style="margin-top:32px;"/>
                  <input id="scorePlot" name="scorePlot" type="hidden" value="<?php echo isset($fieldData[0]->scorePlot)?$fieldData[0]->scorePlot:""; ?>"/>  
                  <input id="sportPlot" name="sportPlot" type="hidden" value="<?php echo isset($fieldData[0]->sportPlot)?$fieldData[0]->sportPlot:""; ?>"/>  
                  <input id="sportMeanScore" name="sportMeanScore" type="hidden" value="<?php echo isset($fieldData[0]->sportMeanScore)?$fieldData[0]->sportMeanScore:""; ?>"/> 
            </div>
            
             <div class="field_row" style="border:0;">
             		<div class="graph flight_graph">
                	<ul class="verticle_lines">                        
                        <li><span id="class1">~3.93</span></li> 
                        <li><span id="class2">~3.79</span></li> 
                        <li><span id="class3">~3.64</span></li> 
                        <li><span id="class4">~3.59</span></li> 
                        <li><span id="class5">~3.46</span></li> 
                        <li><span id="class6">~3.4</span></li> 
                        <li><span id="class7">~3.35</span></li> 
                        <li><span id="class8">~3.26</span></li> 
                        <li><span id="class9">~3.19</span></li> 
                        <li><span id="class10">~3.1</span></li> 
                        <li><span id="class11">~3.02</span></li> 
                        <li><span id="class12">~2.93</span></li> 
                        <li><span id="class13">~2.8</span></li> 
                        <li><span id="class14">~2.68</span></li>
                        <li><span id="class15">~2.54</span></li>                        
                        <li>
                          <div class="graph_dashed_line"><div class="graph_dashed_line">&nbsp;</div> <span id="meanVal" class="graph_dash_text">2.28</span> <span>average</span>                            
                            <div class="custom1"><div id="plot16_1" class="blue_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_1" class="red_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_2" class="blue_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_2" class="red_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_3" class="blue_dot" style="display:none; left:80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_3" class="red_dot" style="display:none; left:80px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom2"><div id="plot17" class="blue_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom5"><div id="score17" class="red_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_8" class="blue_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_8" class="red_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_7" class="blue_dot" style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_7" class="red_dot"  style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_6" class="blue_dot" style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_6" class="red_dot"  style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_5" class="blue_dot" style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_5" class="red_dot"  style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_4" class="blue_dot" style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_4" class="red_dot"  style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_3" class="blue_dot" style="display:none; left:-110px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_3" class="red_dot" style="display:none; left:-110px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_2" class="blue_dot" style="display:none; left:-150px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_2" class="red_dot" style="display:none; left:-150px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_1" class="blue_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_1" class="red_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                          </div>
                        </li>                        
                        </ul>
                    
                        <ul class="bottom_values">
                            <?php $plot=1 ; foreach($prob_array as $val){?>
                            <li><span><?php echo $val->prob_range ;?></span> <div id="<?php echo "plot".$plot ; ?>" class="blue_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div><div id="<?php echo "score".$plot ; ?>" class="red_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></li>
                            <?php $plot++ ; }?>
                        </ul>
                            
                       <div class="score_box">
                            <div class="score_text">
                                    your<br/>score<br>
                                    <div style="margin-top: 10px;">Sport<br/>mean<br/><div id="sportMeanVal"></div></div> 
                            </div>                            
                            <!--<div class="red_dot"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div>-->
                        </div> 
                       
                       
                    <div class="graph_pulse_box">
                    	<span>frequency</span>
                    	<img src="<?php echo "$base/$image"?>/pulse.png" alt=""/> 
                    </div>
                </div>
             </div>
        </div>
    </div>    
    <?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>		
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">

	$(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
    });
	
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script> 


<script>

    function changeMean()
    { 
        var ageValue = document.getElementById("age").value;      
      
        if(document.getElementById('gender').value == "Male") {
                
        if(ageValue == "18-29")
        {
            var mean = 2.28 ;
            var sd = 0.319 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 2.17 ;
           var sd = 0.303 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 1.99 ;
           var sd = 0.279 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 1.73 ;
           var sd = 0.243 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 1.47 ;
           var sd = 0.221 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 1.22 ;
           var sd = 0.196 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 1.90 ;
            var sd = 0.266 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 1.81 ;
           var sd = 0.253 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 1.66 ;
           var sd = 0.241 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 1.44 ;
           var sd = 0.217 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 1.23 ;
           var sd = 0.190 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 1.02 ;
           var sd = 0.163 ;        
      }               
        
    }    
    
    var r1 = (5.2000 * sd) + mean ;
    var r2 = (4.7550 * sd) + mean ;
    var r3 = (4.2650 * sd) + mean ;
    var r4 = (4.1100 * sd) + mean ;
    var r5 = (3.7200 * sd) + mean ;
    var r6 = (3.5410 * sd) + mean ;
    var r7 = (3.3550 * sd) + mean ;
    var r8 = (3.0900 * sd) + mean ;
    var r9 = (2.8785 * sd) + mean ;
    var r10 = (2.5760 * sd) + mean ;
    var r11 = (2.3270 * sd) + mean ;
    var r12 = (2.0550 * sd) + mean ;
    var r13 = (1.6600 * sd) + mean ;
    var r14 = (1.2816 * sd) + mean ;
    var r15 = (0.8420 * sd) + mean ;
     
      document.getElementById('meanVal').innerHTML = parseFloat(mean) ;   
     
     
        document.getElementById('class1').innerHTML = "~"+Math.floor(parseFloat(r1) * 100) / 100 ;
        document.getElementById('class2').innerHTML = "~"+Math.floor(parseFloat(r2) * 100) / 100 ;
        document.getElementById('class3').innerHTML = "~"+Math.floor(parseFloat(r3) * 100) / 100 ;
        document.getElementById('class4').innerHTML = "~"+Math.floor(parseFloat(r4) * 100) / 100 ; 
        document.getElementById('class5').innerHTML = "~"+Math.floor(parseFloat(r5) * 100) / 100 ;
        document.getElementById('class6').innerHTML = "~"+Math.floor(parseFloat(r6) * 100) / 100 ;
        document.getElementById('class7').innerHTML = "~"+Math.floor(parseFloat(r7) * 100) / 100 ;
        document.getElementById('class8').innerHTML = "~"+Math.floor(parseFloat(r8) * 100) / 100 ;
        document.getElementById('class9').innerHTML = "~"+Math.floor(parseFloat(r9) * 100) / 100 ;
        document.getElementById('class10').innerHTML = "~"+Math.floor(parseFloat(r10) * 100) / 100;
        document.getElementById('class11').innerHTML = "~"+Math.floor(parseFloat(r11) * 100) / 100 ;
        document.getElementById('class12').innerHTML = "~"+Math.floor(parseFloat(r12) * 100) / 100 ;
        document.getElementById('class13').innerHTML = "~"+Math.floor(parseFloat(r13) * 100) / 100 ;
        document.getElementById('class14').innerHTML = "~"+Math.floor(parseFloat(r14) * 100) / 100 ;
        document.getElementById('class15').innerHTML = "~"+Math.floor(parseFloat(r15) * 100) / 100 ; 
   

     
     getZscore() ;
    // show() ;
      
  }
    
  //calculate z-score value
    function getZscore()
    { 
        var ageValue = document.getElementById("age").value;
        var vo2MAxVal = document.getElementById("vo2Max_value");
        var vo2Max = vo2MAxVal.value;
      
        
        var div1=document.getElementById('plot1');
        var div2=document.getElementById('plot2');
        var div3=document.getElementById('plot3');
        var div4=document.getElementById('plot4');
        var div5=document.getElementById('plot5');
        var div6=document.getElementById('plot6');
        var div7=document.getElementById('plot7');
        var div8=document.getElementById('plot8');
        var div9=document.getElementById('plot9');
        var div10=document.getElementById('plot10');
        var div11=document.getElementById('plot11');
        var div12=document.getElementById('plot12');
        var div13=document.getElementById('plot13');
        var div14=document.getElementById('plot14');
        var div15=document.getElementById('plot15');
        var div16_1=document.getElementById('plot16_1');
        var div16_2=document.getElementById('plot16_2');
        var div16_3=document.getElementById('plot16_3');
        var div17=document.getElementById('plot17');
        var div18_8=document.getElementById('plot18_8');
        var div18_7=document.getElementById('plot18_7');
        var div18_6=document.getElementById('plot18_6');
        var div18_5=document.getElementById('plot18_5');
        var div18_4=document.getElementById('plot18_4');
        var div18_3=document.getElementById('plot18_3');
        var div18_2=document.getElementById('plot18_2');
        var div18_1=document.getElementById('plot18_1');
        
         if(div1.style.display == "block")
        {
            div1.style.display = "none"
        }
        else if(div2.style.display == "block")
        {
            div2.style.display = "none"
        }
        else if(div3.style.display == "block")
        {
            div3.style.display = "none"
        }
        else if(div4.style.display == "block")
        {
            div4.style.display = "none"
        }
        else if(div5.style.display == "block")
        {
            div5.style.display = "none"
        }
        else if(div6.style.display == "block")
        {
            div6.style.display = "none"
        }
        else if(div7.style.display == "block")
        {
            div7.style.display = "none"
        }
        else if(div8.style.display == "block")
        {
            div8.style.display = "none"
        }
        else if(div9.style.display == "block")
        {
            div9.style.display = "none"
        }
        else if(div10.style.display == "block")
        {
            div10.style.display = "none"
        }
        else if(div11.style.display == "block")
        {
            div11.style.display = "none"
        }
        else if(div12.style.display == "block")
        {
            div12.style.display = "none"
        }
        else if(div13.style.display == "block")
        {
            div13.style.display = "none"
        }
        else if(div14.style.display == "block")
        {
            div14.style.display = "none"
        }
        else if(div15.style.display == "block")
        {
            div15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        } 
        
        
      
       if(document.getElementById('gender').value == "Male") {
                
        if(ageValue == "18-29")
        {
            var mean = 2.28 ;
            var sd = 0.319 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 2.17 ;
           var sd = 0.303 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 1.99 ;
           var sd = 0.279 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 1.73 ;
           var sd = 0.243 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 1.47 ;
           var sd = 0.221 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 1.22 ;
           var sd = 0.196 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 1.90 ;
            var sd = 0.266 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 1.81 ;
           var sd = 0.253 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 1.66 ;
           var sd = 0.241 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 1.44 ;
           var sd = 0.217 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 1.23 ;
           var sd = 0.190 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 1.02 ;
           var sd = 0.163 ;        
      }               
        
    }    
          
     var z_score = (parseFloat(vo2Max) - parseFloat(mean)) / parseFloat(sd) ;    
      if(isNaN(z_score)){            
            document.getElementById("percentile").value = "" ;  
        }
        else
        {
           z_score = Math.floor(parseFloat(z_score) * 1000) / 1000 ;
            
           // NORMDIST for z-score 
           var perform_percent = normalcdf(z_score);
           
            if(isNaN(parseFloat(perform_percent * 100)))
            {              
               document.getElementById("percentile").value = "" ;
             }
             else
             {                
                 document.getElementById("percentile").value = Math.floor(parseFloat(perform_percent * 100) * 1000000) / 1000000 ;
                 
             } 
           
           if(document.getElementById("percentile").value < 48)
           {  
             if(document.getElementById("percentile").value >= 0 && document.getElementById("percentile").value < 7)
                {              
                   var scorePlot = '1 in 1_1' ;
                }
             else if(document.getElementById("percentile").value >= 7 && document.getElementById("percentile").value < 14)
                {              
                   var scorePlot = '1 in 1_2' ;
                }  
             else if(document.getElementById("percentile").value >= 14 && document.getElementById("percentile").value < 20)
                {              
                   var scorePlot = '1 in 1_3' ;
                }  
             else if(document.getElementById("percentile").value >= 20 && document.getElementById("percentile").value < 30)
                {              
                   var scorePlot = '1 in 1_4' ;
                }  
             else if(document.getElementById("percentile").value >= 30 && document.getElementById("percentile").value < 35)
                {              
                   var scorePlot = '1 in 1_5' ;
                }  
             else if(document.getElementById("percentile").value >= 35 && document.getElementById("percentile").value < 40)
                {              
                   var scorePlot = '1 in 1_6' ;
                }  
             else if(document.getElementById("percentile").value >= 40 && document.getElementById("percentile").value < 44)
                {              
                   var scorePlot = '1 in 1_7' ;
                }  
             else if(document.getElementById("percentile").value >= 44 && document.getElementById("percentile").value < 48 )
                {              
                   var scorePlot = '1 in 1_8' ;
                }                
           }
           else if(document.getElementById("percentile").value >= 48 && document.getElementById("percentile").value < 52)
           {              
              var scorePlot = '1 in 2' ;
           }          
           else if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 80)
           {              
               if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 60)
               {
                 var scorePlot = '1 in 3_1' ;   
               }
               else if(document.getElementById("percentile").value > 60 && document.getElementById("percentile").value <= 70)
               {
                 var scorePlot = '1 in 3_2' ;   
               }
               else if(document.getElementById("percentile").value > 70 && document.getElementById("percentile").value <= 80)
               {
                 var scorePlot = '1 in 3_3' ;   
               }
                
           }  
           else if(z_score > 0.8100 && z_score <= 0.8420)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 0.8420 && z_score <= 1.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.0100 && z_score <= 1.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.1100 && z_score <= 1.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.2100 && z_score <= 1.2816)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.2816 && z_score <= 1.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.4100 && z_score <= 1.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.5100 && z_score <= 1.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.6100 && z_score <= 1.6600)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.6600 && z_score <= 1.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.8100 && z_score <= 1.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.9100 && z_score <= 2.0550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.0550 && z_score <= 2.1100)
           {
              var perform_percent = 0.98257082 ;
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.1100 && z_score <= 2.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.2100 && z_score <= 2.3270)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.3270 && z_score <= 2.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.4100 && z_score <= 2.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.5100 && z_score <= 2.5760)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.5760 && z_score <= 2.7100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.7100 && z_score <= 2.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.8100 && z_score <= 2.8785)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 2.8785 && z_score <= 3.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 3.0100 && z_score <= 3.0900)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.0900 && z_score <= 3.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.2100 && z_score <= 3.3550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.3550 && z_score <= 3.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.4100 && z_score <= 3.5410)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.5410 && z_score <= 3.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.6100 && z_score <= 3.7200)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.7200 && z_score <= 3.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.8100 && z_score <= 3.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.9100 && z_score <= 4.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 4.0100 && z_score <= 4.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50,000' ;
           }
           else if(z_score > 4.1100 && z_score <= 4.2650)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.2650 && z_score <= 4.3100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.3100 && z_score <= 4.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.4100 && z_score <= 4.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.5100 && z_score <= 4.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.6100 && z_score <= 4.7550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.7550 && z_score <= 4.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.8100 && z_score <= 4.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.9100 && z_score <= 5.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.0100 && z_score <= 5.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.1100 && z_score <= 5.2000)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000,000' ;
           }
           else if(z_score > 5.2000)
           {
              var perform_percent = normalcdf(z_score);	                      
              var scorePlot = '1 in 10,000,000' ;
           }
           
           document.getElementById("scorePlot").value = scorePlot ;   
           
          
            
        }     
       
         changeSportMean() ;
    }
    
    function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    }   
    
    
        function changeSportMean()
    {   
        var sportValue = document.getElementById("sport").value ;           
        
        var testmean1 = document.getElementById('class1').innerHTML.replace('~', '')   ;
        var testmean2 = document.getElementById('class2').innerHTML.replace('~', '')   ;
        var testmean3 = document.getElementById('class3').innerHTML.replace('~', '')   ;
        var testmean4 = document.getElementById('class4').innerHTML.replace('~', '')   ; 
        var testmean5 = document.getElementById('class5').innerHTML.replace('~', '')   ;
        var testmean6 = document.getElementById('class6').innerHTML.replace('~', '')   ;
        var testmean7 = document.getElementById('class7').innerHTML.replace('~', '')   ;
        var testmean8 = document.getElementById('class8').innerHTML.replace('~', '')   ;
        var testmean9 = document.getElementById('class9').innerHTML.replace('~', '')   ;
        var testmean10 = document.getElementById('class10').innerHTML.replace('~', '')  ;
        var testmean11 = document.getElementById('class11').innerHTML.replace('~', '')  ;
        var testmean12 = document.getElementById('class12').innerHTML.replace('~', '')  ;
        var testmean13 = document.getElementById('class13').innerHTML.replace('~', '')  ;
        var testmean14 = document.getElementById('class14').innerHTML.replace('~', '')  ;
        var testmean15 = document.getElementById('class15').innerHTML.replace('~', '')  ;        
        var testmean = document.getElementById('meanVal').innerHTML   ;
        
       
        var divsport1=document.getElementById('score1');
        var divsport2=document.getElementById('score2');
        var divsport3=document.getElementById('score3');
        var divsport4=document.getElementById('score4');
        var divsport5=document.getElementById('score5');
        var divsport6=document.getElementById('score6');
        var divsport7=document.getElementById('score7');
        var divsport8=document.getElementById('score8');
        var divsport9=document.getElementById('score9');
        var divsport10=document.getElementById('score10');
        var divsport11=document.getElementById('score11');
        var divsport12=document.getElementById('score12');
        var divsport13=document.getElementById('score13');
        var divsport14=document.getElementById('score14');
        var divsport15=document.getElementById('score15');
        var div16_1=document.getElementById('score16_1');
        var div16_2=document.getElementById('score16_2');
        var div16_3=document.getElementById('score16_3');
        var div17=document.getElementById('score17');
        var div18_8=document.getElementById('score18_8');
        var div18_7=document.getElementById('score18_7');
        var div18_6=document.getElementById('score18_6');
        var div18_5=document.getElementById('score18_5');
        var div18_4=document.getElementById('score18_4');
        var div18_3=document.getElementById('score18_3');
        var div18_2=document.getElementById('score18_2');
        var div18_1=document.getElementById('score18_1');
       
        
      if(sportValue != "") 
       {
        if(divsport1.style.display == "block")
        {
            divsport1.style.display = "none"
        }
        else if(divsport2.style.display == "block")
        {
            divsport2.style.display = "none"
        }
        else if(divsport3.style.display == "block")
        {
            divsport3.style.display = "none"
        }
        else if(divsport4.style.display == "block")
        {
            divsport4.style.display = "none"
        }
        else if(divsport5.style.display == "block")
        {
            divsport5.style.display = "none"
        }
        else if(divsport6.style.display == "block")
        {
            divsport6.style.display = "none"
        }
        else if(divsport7.style.display == "block")
        {
            divsport7.style.display = "none"
        }
        else if(divsport8.style.display == "block")
        {
            divsport8.style.display = "none"
        }
        else if(divsport9.style.display == "block")
        {
            divsport9.style.display = "none"
        }
        else if(divsport10.style.display == "block")
        {
            divsport10.style.display = "none"
        }
        else if(divsport11.style.display == "block")
        {
            divsport11.style.display = "none"
        }
        else if(divsport12.style.display == "block")
        {
            divsport12.style.display = "none"
        }
        else if(divsport13.style.display == "block")
        {
            divsport13.style.display = "none"
        }
        else if(divsport14.style.display == "block")
        {
            divsport14.style.display = "none"
        }
        else if(divsport15.style.display == "block")
        {
           divsport15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        }
    }
        
        
   // Male     
/*       if(document.getElementById('gender').value == "Male") {
                
        if(sportValue == "archery")
        {
            var mean = 2.72 ;
            var sd = 0.245 ;                  
            
        }else if(sportValue == "Australian football [AFL midfield]") {
        
           var mean = 2.83 ;
           var sd = 0.255 ;  
           
      }else if(sportValue == "Australian football [AFL]") {
        
           var mean = 2.96 ;
           var sd = 0.266 ;
           
      }else if(sportValue == "badminton") {
        
           var mean = 2.83 ;
           var sd = 0.255 ;        
           
      }else if(sportValue == "baseball") {
        
           var mean = 2.65 ;
           var sd = 0.239 ;    
           
      }else if(sportValue == "basketball") {
        
           var mean = 3.16 ;
           var sd = 0.284 ;     
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 2.09 ;
           var sd = 0.188 ;    
           
      }else if(sportValue == "boxing - general") {
        
           var mean = 2.78 ;
           var sd = 0.25 ;   
           
      }else if(sportValue == "boxing - heavyweight") {
        
           var mean = 2.71 ;
           var sd = 0.244 ;
           
      }else if(sportValue == "canoe polo") {
        
           var mean = 2.78 ;
           var sd = 0.25 ;   
           
      }else if(sportValue == "canoeing (Canadian)") {
        
           var mean = 2.73 ;
           var sd = 0.246 ; 
           
      }else if(sportValue == "cricket") {
        
           var mean = 2.71 ;
           var sd = 0.244 ;      
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 2.69 ;
           var sd = 0.242 ;      
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 2.64 ;
           var sd = 0.238 ; 
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 2.99 ;
           var sd = 0.269 ;   
           
      }else if(sportValue == "decathlon") {
        
           var mean = 2.86 ;
           var sd = 0.257 ; 
           
      }else if(sportValue == "discus") {
        
           var mean = 2.28 ;
           var sd = 0.205 ;   
           
      }else if(sportValue == "diving") {
        
           var mean = 2.47 ;
           var sd = 0.222 ;        
      }else if(sportValue == "fencing") {
        
           var mean = 2.66 ;
           var sd = 0.239 ;   
           
      }else if(sportValue == "golf") {
        
           var mean = 2.31 ;
           var sd = 0.208 ;        
           
      }else if(sportValue == "gymnastics") {
        
           var mean = 2.68 ;
           var sd = 0.241 ; 
           
      }else if(sportValue == "handball") {
        
           var mean = 3.02 ;
           var sd = 0.272 ;
           
      }else if(sportValue == "high jump") {
        
           var mean = 3.13 ;
           var sd = 0.282 ;  
           
      }else if(sportValue == "hockey (field)") {
        
           var mean = 2.81 ;
           var sd = 0.253 ;   
           
      }else if(sportValue == "hockey (ice)") {
        
           var mean = 2.7 ;
           var sd = 0.243 ; 
           
      }else if(sportValue == "hurdles") {
        
           var mean = 3.03 ;
           var sd = 0.273 ;  
           
      }else if(sportValue == "javelin") {
        
           var mean = 2.36 ;
           var sd = 0.212 ;  
           
      }else if(sportValue == "jockey") {
        
           var mean = 2.26 ;
           var sd = 0.203 ; 
           
      }else if(sportValue == "judo") {
        
           var mean = 2.5 ;
           var sd = 0.225 ;    
           
      }else if(sportValue == "karate") {
        
           var mean = 2.82 ;
           var sd = 0.254 ; 
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 2.85 ;
           var sd = 0.257 ; 
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 2.73 ;
           var sd = 0.246 ; 
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 2.86 ;
           var sd = 0.257 ;        
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 2.84 ;
           var sd = 0.256 ;  
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 2.82 ;
           var sd = 0.254 ;  
           
      }else if(sportValue == "long jump") {
        
           var mean = 3.07 ;
           var sd = 0.276 ;   
           
      }else if(sportValue == "orienteering") {
        
           var mean = 2.75 ;
           var sd = 0.248 ;  
           
      }else if(sportValue == "powerlifting") {
        
           var mean = 2.73 ;
           var sd = 0.246 ;        
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 2.64 ;
           var sd = 0.238 ;
           
      }else if(sportValue == "rollerskating") {
        
           var mean = 2.56 ;
           var sd = 0.23 ; 
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 2.96 ;
           var sd = 0.266 ;  
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 2.98 ;
           var sd = 0.268 ;   
           
      }else if(sportValue == "rugby League - backs") {
        
           var mean = 2.47 ;
           var sd = 0.222 ;  
           
      }else if(sportValue == "rugby League - forwards") {
        
           var mean = 2.41 ;
           var sd = 0.217 ;    
           
      }else if(sportValue == "rugby union") {
        
           var mean = 2.61 ;
           var sd = 0.235 ;       
           
      }else if(sportValue == "running - distance") {
        
           var mean = 2.63 ;
           var sd = 0.237 ; 
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 2.8 ;
           var sd = 0.252 ;       
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 3.01 ;
           var sd = 0.271 ;  
           
      }else if(sportValue == "sailing") {
        
           var mean = 2.68 ;
           var sd = 0.241 ;  
           
      }else if(sportValue == "shooting") {
        
           var mean = 2.72 ;
           var sd = 0.245 ;        
           
      }else if(sportValue == "shot put") {
        
           var mean = 2.62 ;
           var sd = 0.236 ; 
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 2.34 ;
           var sd = 0.211 ;        
           
      }else if(sportValue == "soccer") {
        
           var mean = 2.91 ;
           var sd = 0.262 ; 
           
      }else if(sportValue == "squash") {
        
           var mean = 2.79 ;
           var sd = 0.251 ;    
           
      }else if(sportValue == "sumo wrestling") {
        
           var mean = 2.01   ;
           var sd = 0.181 ;    
           
      }else if(sportValue == "surfing") {
        
           var mean = 2.69 ;
           var sd = 0.242 ;  
           
      }else if(sportValue == "swimming") {
        
           var mean = 2.66 ;
           var sd = 0.239 ; 
           
      }else if(sportValue == "table tennis") {
        
           var mean = 2.34 ;
           var sd = 0.211 ;
           
      }else if(sportValue == "tennis") {
        
           var mean = 2.94 ;
           var sd = 0.265 ;        
           
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 2.72 ;
           var sd = 0.245 ;      
           
      }else if(sportValue == "triathlon") {
        
           var mean = 2.67 ;
           var sd = 0.24 ; 
           
      }else if(sportValue == "volleyball") {
        
           var mean = 3.13 ;
           var sd = 0.282 ;  
           
      }else if(sportValue == "walking") {
        
           var mean = 2.61 ;
           var sd = 0.235 ;     
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 2.72 ;
           var sd = 0.245 ;        
      }else if(sportValue == "weightlifting") {
        
           var mean = 2.27 ;
           var sd = 0.204 ;        
      }else if(sportValue == "wrestling") {
        
          var mean = 2.48 ;
          var sd = 0.223 ;          
      }
             
    }
    
  // Female      
    else if(document.getElementById('gender').value == "Female") {
              
        if(sportValue == "archery")
        {
            var mean = 1.99 ;
            var sd = 0.179 ;                  
            
        }else if(sportValue == "badminton") {
        
           var mean = 2.41 ;
           var sd = 0.217 ; 
           
      }else if(sportValue == "basketball") {
        
           var mean = 2.56 ;
           var sd = 0.23 ;  
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 2.08 ;
           var sd = 0.187 ;     
           
      }else if(sportValue == "cricket") {
        
           var mean = 1.97 ;
           var sd = 0.177 ;
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 1.98 ;
           var sd = 0.178 ;  
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 1.98 ;
           var sd = 0.178 ;    
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 2.5 ;
           var sd = 0.225 ;   
           
      }else if(sportValue == "discus") {
        
           var mean = 1.88 ;
           var sd = 0.169 ;    
           
      }else if(sportValue == "diving") {
        
           var mean = 2.4 ;
           var sd = 0.216 ;
           
      }else if(sportValue == "fencing") {
        
           var mean = 1.94 ;
           var sd = 0.175 ; 
           
      }else if(sportValue == "golf") {
        
           var mean = 1.97 ;
           var sd = 0.177 ;   
           
      }else if(sportValue == "gymnastics") {
        
           var mean = 2.33 ;
           var sd = 0.21 ;    
           
      }else if(sportValue == "handball") {
        
           var mean = 2.05 ;
           var sd = 2.05 ;     
           
      }else if(sportValue == "heptathlon") {
        
           var mean = 2.14 ;
           var sd = 0.193 ; 
           
      }else if(sportValue == "high jump") {
        
           var mean = 2.64 ;
           var sd = 0.238 ;    
           
      }else if(sportValue == "hockey (field)") {
        
           var mean = 1.98 ;
           var sd = 0.178 ;        
           
      }else if(sportValue == "hurdles") {
        
           var mean = 2.11 ;
           var sd = 0.19 ; 
           
      }else if(sportValue == "javelin") {
        
           var mean = 2.15 ;
           var sd = 0.194 ;  
           
      }else if(sportValue == "judo") {
        
           var mean = 1.91 ;
           var sd = 0.172 ;        
           
      }else if(sportValue == "karate") {
        
           var mean = 2.35 ;
           var sd = 0.212 ;  
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 1.99 ;
           var sd = 0.179 ;   
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 2.07 ;
           var sd = 0.186 ;        
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 2.03 ;
           var sd = 0.183 ;    
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 2.12 ;
           var sd = 0.191 ;        
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 1.98 ;
           var sd = 0.178 ;    
           
      }else if(sportValue == "long jump") {
        
           var mean = 2.46 ;
           var sd = 0.221 ;   
           
      }else if(sportValue == "orienteering") {
        
           var mean = 2.01 ;
           var sd = 0.181 ;     
           
      }else if(sportValue == "netball") {
        
           var mean = 2.66 ;
           var sd = 0.239 ; 
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 2.19 ;
           var sd = 0.197 ;       
           
      }else if(sportValue == "rollerskating") {
        
           var mean = 2.1 ;
           var sd = 0.189 ;   
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 1.99 ;
           var sd = 0.179 ;        
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 2.1 ;
           var sd = 0.189 ;   
           
      }else if(sportValue == "rugby union") {
        
           var mean = 1.93 ;
           var sd = 0.174 ;      
           
      }else if(sportValue == "running - distance") {
        
           var mean = 2.02 ;
           var sd = 0.182 ;   
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 2.06 ;
           var sd = 0.185 ;  
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 2.12 ;
           var sd = 0.191 ;
           
      }else if(sportValue == "sailing") {
        
           var mean = 2.01 ;
           var sd = 0.181 ;  
           
      }else if(sportValue == "shooting") {
        
           var mean = 1.99 ;
           var sd = 0.179 ; 
           
      }else if(sportValue == "shot put") {
        
           var mean = 2.13   ;
           var sd = 0.192 ;        
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 2.01 ;
           var sd = 0.181 ;        
           
      }else if(sportValue == "soccer") {
        
           var mean = 2.02 ;
           var sd = 0.182 ; 
           
      }else if(sportValue == "softball") {
        
           var mean = 2.08 ;
           var sd = 0.187 ;        
           
      }else if(sportValue == "squash") {
        
           var mean = 2.02 ;
           var sd = 0.182 ;        
           
      }else if(sportValue == "surfing") {
        
           var mean = 1.99 ;
           var sd = 0.179 ;        
           
      }else if(sportValue == "swimming") {
        
           var mean = 2.05 ;
           var sd = 0.185 ;        
           
      }else if(sportValue == "synchronised swimming") {
        
           var mean = 2.01 ;
           var sd = 0.181 ;        
           
      }else if(sportValue == "table tennis") {
        
           var mean = 2.03 ;
           var sd = 0.183 ;        
           
      }else if(sportValue == "tennis") {
        
           var mean = 2.12 ;
           var sd = 0.191 ;        
           
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 1.99 ;
           var sd = 0.179 ;        
           
      }else if(sportValue == "triathlon") {
        
           var mean = 1.99 ;
           var sd = 0.179 ;        
           
      }else if(sportValue == "volleyball") {
        
           var mean = 2.54 ;
           var sd = 0.229 ;  
           
      }else if(sportValue == "walking") {
        
           var mean = 1.98 ;
           var sd = 0.178 ;        
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 2.06 ;
           var sd = 0.185 ;        
      }             
        
    } */
      
    
	
	 if(document.getElementById('gender').value == "Male") {
		var gender=	document.getElementById('gender').value;
		var meanget=undefined;
		var sdget=undefined;
		var mean=undefined;
		var sd=undefined;
		 $.ajax({
			type: "POST",
			url: '<?php echo base_url(); ?>index.php/Fitness/get_sports_fitness_norms',
			data:{'sportValue': sportValue,'gender' : gender },
			 dataType: 'json',
			success: function(data){
               console.log(data);
               meanget=data.flight_mean;
			   sdget=data.flight_sd;         
				
				 },
				 async: false // <- this turns it into synchronous 
			});	
	 if(meanget)
		{
		var mean=meanget;	
		}
		if(sdget)
		{
		var sd=sdget;  	
		}
	}
    
  // Female      

    else if(document.getElementById('gender').value == "Female") {
              
       var gender=	document.getElementById('gender').value;
		var meanget=undefined;
		var sdget=undefined;
		var mean=undefined;
		var sd=undefined;
		 $.ajax({
			type: "POST",
			url: '<?php echo base_url(); ?>index.php/Fitness/get_sports_fitness_norms',
			data:{'sportValue': sportValue,'gender' : gender },
			 dataType: 'json',
			success: function(data){
               console.log(data);
             meanget=data.flight_mean;
			 sdget=data.flight_sd;   
				 },
				 async: false // <- this turns it into synchronous 
			});	
        if(meanget)
		{
		var mean=meanget;	
		}
		if(sdget)
		{
		var sd=sdget;  	
		}
		
    }  
        console.log(mean);
	
	
	
	
	
        
          if(mean < testmean)
           {  
             if(mean < (parseFloat(testmean) / 2))
             {
              if(mean >= 0 && mean < ((parseFloat(testmean) / 2) - 0.6))
                {              
                   var sportPlot = '1 in 1_1' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) - 0.6) && mean < ((parseFloat(testmean) / 2) - 0.4))
                {              
                   var sportPlot = '1 in 1_2' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 0.4) && mean < ((parseFloat(testmean) / 2) - 0.2))
                {              
                   var sportPlot = '1 in 1_3' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 0.2) && mean < (parseFloat(testmean) / 2))
                {              
                   var sportPlot = '1 in 1_4' ;
                }
             }
             else{
                 
             if(mean >= (parseFloat(testmean) / 2) && mean < ((parseFloat(testmean) / 2) + 0.2))
                {              
                   var sportPlot = '1 in 1_5' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) + 0.2) && mean < ((parseFloat(testmean) / 2) + 0.4))
                {              
                  var sportPlot = '1 in 1_6' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 0.4) && mean < ((parseFloat(testmean) / 2) + 0.6))
                {              
                  var sportPlot = '1 in 1_7' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 0.6) && mean < testmean)
                {              
                   var sportPlot = '1 in 1_8' ;
                }        
             }
           }
           
           else if(mean >= testmean && mean < (parseFloat(testmean) + 0.05))
           {              
              var sportPlot = '1 in 2' ;
           }  
      
          else if(mean >= (parseFloat(testmean) + 0.05) && mean < testmean15)
           {           
               if(mean >= (parseFloat(testmean) + 0.05) && mean < (parseFloat(testmean) + 0.1))
               {
                 var sportPlot = '1 in 3_1' ;   
               }
               else if(mean >= (parseFloat(testmean) + 0.1) && mean < (parseFloat(testmean) + 0.15))
               {
                 var sportPlot = '1 in 3_2' ;   
               }
               else if(mean >= (parseFloat(testmean) + 0.15) && mean < testmean15)
               { 
                 var sportPlot = '1 in 3_3' ;   
               }
                
           }       
        
          else if(mean >= testmean15 && mean < testmean14)
           {              
              var sportPlot = '1 in 5' ;
           }           
           else if(mean >= testmean14 && mean < testmean13)
           {             
              var sportPlot = '1 in 10' ;
           }           
           else if(mean >= testmean13 && mean < testmean12)
           {             
              var sportPlot = '1 in 20' ;
           }          
           else if(mean >= testmean12 && mean < testmean11)
           {              
              var sportPlot = '1 in 50' ;
           }          
           else if(mean >= testmean11 && mean < testmean10)
           {              
              var sportPlot = '1 in 100' ;
           }          
           else if(mean >= testmean10 && mean < testmean9)
           {             	
              var sportPlot = '1 in 200' ;
           }          
           else if(mean >= testmean9 && mean < testmean8)
           {              
              var sportPlot = '1 in 500' ;
           }           
           else if(mean >= testmean8 && mean < testmean7)
           {              
              var sportPlot = '1 in 1000' ;
           }          
           else if(mean >= testmean7 && mean < testmean6)
           {              
              var sportPlot = '1 in 2,500' ;
           }          
           else if(mean >= testmean6 && mean < testmean5)
           {              
              var sportPlot = '1 in 5,000' ;
           }        
           else if(mean >= testmean5 && mean < testmean4)
           {              	
              var sportPlot = '1 in 10,000' ;
           }           
           else if(mean >= testmean4 && mean < testmean3)
           {             	
              var sportPlot = '1 in 50,000' ;
           }
           else if(mean >= testmean3 && mean < testmean2)
           {              	
              var sportPlot = '1 in 100,000' ;
           }           
           else if(mean >= testmean2 && mean < testmean1)
           {              
              var sportPlot = '1 in 1,000,000' ;
           }           
           else if(mean >= testmean1)
           {              
              var sportPlot = '1 in 10,000,000' ;
           }           
                               
           document.getElementById("sportPlot").value = sportPlot ;              
           
           document.getElementById("sportMeanScore").value = mean ; // store mean value of sport selected
           
           
  }
    
    
    
   function show()
    {    
        var score = document.getElementById("scorePlot").value ;      
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
      else
      { 
        if(score == '1 in 1_1')
        {
           var div=document.getElementById('plot18_1');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_2')
        {
           var div=document.getElementById('plot18_2');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_3')
        {
           var div=document.getElementById('plot18_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_4')
        {
           var div=document.getElementById('plot18_4');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_5')
        {
           var div=document.getElementById('plot18_5');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_6')
        {
           var div=document.getElementById('plot18_6');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_7')
        {
           var div=document.getElementById('plot18_7');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_8')
        {
           var div=document.getElementById('plot18_8');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 2')
        {
           var div=document.getElementById('plot17');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_1')
        {
           var div=document.getElementById('plot16_1');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_2')
        {
           var div=document.getElementById('plot16_2');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_3')
        {
           var div=document.getElementById('plot16_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 5')
        {
           var div=document.getElementById('plot15');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 10') {
        
           var div=document.getElementById('plot14');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 20') {
        
          var div=document.getElementById('plot13');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 50') {
        
           var div=document.getElementById('plot12');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 100') {
        
           var div=document.getElementById('plot11');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 200') {
        
           var div=document.getElementById('plot10');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 500') {
        
           var div=document.getElementById('plot9');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1000') {
        
           var div=document.getElementById('plot8');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 2,500') {
        
           var div=document.getElementById('plot7');
           div.style.display = "block" ; 
            
      }
      else if(score == '1 in 5,000') {
        
           var div=document.getElementById('plot6');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000') {
        
           var div=document.getElementById('plot5');
           div.style.display = "block" ;      
           
      }
      else if(score == '1 in 50,000') {
        
           var div=document.getElementById('plot4');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 100,000') {
        
           var div=document.getElementById('plot3');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1,000,000') {
        
           var div=document.getElementById('plot2');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000,000') {
        
          var div=document.getElementById('plot1');
          div.style.display = "block" ;     
      }
      
      
      
    }
      
        showSport() ;
  }
  
  
  function showSport()
    {       
        var sport = document.getElementById("sportPlot").value ; 
        var sportMean = document.getElementById('sportMeanScore').value ;
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
        else
      { 
          
        if(sport == '1 in 1_1')
        {
           var div=document.getElementById('score18_1');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_2')
        {
           var div=document.getElementById('score18_2');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_3')
        {
           var div=document.getElementById('score18_3');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_4')
        {
           var div=document.getElementById('score18_4');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_5')
        {
           var div=document.getElementById('score18_5');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_6')
        {
           var div=document.getElementById('score18_6');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_7')
        {
           var div=document.getElementById('score18_7');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_8')
        {
           var div=document.getElementById('score18_8');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 2')
        {
           var div=document.getElementById('score17');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_1')
        {
           var div=document.getElementById('score16_1');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_2')
        {
           var div=document.getElementById('score16_2');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_3')
        {
           var div=document.getElementById('score16_3');
           div.style.display = "block" ;
            
        }   
       
       else if(sport == '1 in 5')
        {
           var div=document.getElementById('score15');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 10') {
        
           var div=document.getElementById('score14');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 20') {
        
          var div=document.getElementById('score13');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 50') {
        
           var div=document.getElementById('score12');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 100') {
        
           var div=document.getElementById('score11');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 200') {
        
           var div=document.getElementById('score10');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 500') {
        
           var div=document.getElementById('score9');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1000') {
        
           var div=document.getElementById('score8');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 2,500') {
        
           var div=document.getElementById('score7');
           div.style.display = "block" ; 
            
      }
      else if(sport == '1 in 5,000') {
        
           var div=document.getElementById('score6');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000') {
        
           var div=document.getElementById('score5');
           div.style.display = "block" ;      
           
      }
      else if(sport == '1 in 50,000') {
        
           var div=document.getElementById('score4');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 100,000') {
        
           var div=document.getElementById('score3');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1,000,000') {
        
           var div=document.getElementById('score2');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000,000') {
        
          var div=document.getElementById('score1');
          div.style.display = "block" ;     
      }
      
    }
 
      
      if(sportMean == 'undefined')
      {   
        document.getElementById('sportMeanVal').innerHTML = "" ;
      }
      else
      {
          document.getElementById('sportMeanVal').innerHTML = "["+sportMean+"]" ;
      }
  }
  
</script>  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script>
</body>
</html>

