<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css" href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script src="https://raw.githubusercontent.com/caleb531/jcanvas/master/jcanvas.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>

<body>

<!-- insertion of header -->
<?=$header_insert; ?>

<div class="wrapper">
	
<!-- Form begins -->    
<?php 
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
<div class="contain">

        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Skinfold Map</div>
            
            <div class="skinfold_container">                
                <div class="skinfold_radio">
                    <input checked="checked" type="radio" id="4skinfolds" name="skinfolds" value="four_skinfolds" onclick="changeMethod(this.value)"> <label for="4skinfolds"> <span style="margin-right:8px; float:none;"></span> 4 skinfolds</label>
                    <input type="radio" id="6skinfolds" name="skinfolds" value="six_skinfolds" onclick="changeMethod(this.value)"> <label for="6skinfolds"> <span style="margin-right:8px; float:none;"></span> 6 skinfolds</label>
                </div>
                <div class="skmap_right">
                    <div class="skmap_rtxt1"><img src="<?php echo "$base/$image"?>/bold_line.jpg" alt=""> <?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</div>
                    <div class="skmap_rtxt2"><img src="<?php echo "$base/$image"?>/lite_line.jpg" alt=""> population, <?php echo $_SESSION['age_range'] ;?> yr [10, 50, 90 %'ile]</div>
                </div>
            </div>
            
            <table class="graph_table" width="100%">
          <tbody>                 
            
            <tr>
              <td colspan="3">	              	    
                    <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>">
                    <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                    <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){echo "Male" ;}elseif($_SESSION['user_gender'] == "F"){echo "Female" ;}?>">
                    <input type="hidden" id="height" name="height" value="<?php echo $skinfolds["height"];?>">
                    <input type="hidden" id="body_mass" name="body_mass" value="<?php echo $skinfolds["body_mass"];?>">
                
                    <input type="hidden" id="triceps" name="triceps" value="<?php echo $skinfolds["triceps"] ; ?>">
                    <input type="hidden" id="subscapular" name="subscapular" value="<?php echo $skinfolds["subscapular"] ; ?>">
                    <input type="hidden" id="illiac" name="illiac" value="<?php echo $skinfolds["illiac"] ; ?>">
                    <input type="hidden" id="thigh" name="thigh" value="<?php echo $skinfolds["thigh"] ; ?>">       
                    <input type="hidden" id="biceps" name="biceps" value="<?php echo $skinfolds["biceps"] ; ?>">
                    <input type="hidden" id="supraspinale" name="supraspinale" value="<?php echo $skinfolds["supraspinale"] ; ?>">
                    <input type="hidden" id="abdominal" name="abdominal" value="<?php echo $skinfolds["abdominal"] ; ?>">
                    <input type="hidden" id="calf" name="calf" value="<?php echo $skinfolds["calf"] ; ?>">
                            
                    <!-- 4 skinfolds -->          
              	    <div id="skinfold_4" class="lrg_circle">
                    	<div class="lrg_circle_in">
                            <div class="lrg_circle_inner">
                            <div class="lrg_circle_under">&nbsp;</div>
                            </div>
                        </div>
                        
                        <div class="sk_txt1">triceps</div>
                        <div class="sk_txt2">subscapular</div>
                        <div class="sk_txt3">front thigh</div>
                        <div class="sk_txt4">iliac crest</div>       
                                               
                        
                        <div class="hori_content">
                            <ul id="horizontal_scale">
                            	<li id="hor_scale1"></li>
                                <li id="hor_scale2"></li>
                                <li id="hor_scale3"></li>
                                <li id="hor_scale4"></li>
                                <li id="hor_scale5"></li>
                                <li id="hor_scale6"></li>
                                <li id="hor_scale7"></li>
                                <li id="hor_scale8"></li> 
                            </ul>
                        </div>
                        
                        <div class="vert_content">
                            <ul id="vertical_scale">
                            	<li id="ver_scale1"></li>
                                <li id="ver_scale2"></li>
                                <li id="ver_scale3"></li>
                                <li id="ver_scale4"></li>
                                <li id="ver_scale5"></li>
                                <li id="ver_scale6"></li>
                                <li id="ver_scale7"></li>
                                <li id="ver_scale8"></li>
                            </ul>
                        </div>
                        
                        <canvas id="myCanvas4" width="640" height="640" style="position: absolute; top: 0; left:0;">&nbsp;</canvas>                       
                    </div>
                    
                    
                 
                            
                    <!-- 6 skinfolds -->         
                    <div id="skinfold_6" class="lrg_circle" style="display:none;">
                    	<div class="lrg_circle_in">
                            <div class="lrg_circle_inner">
                            <div class="lrg_circle_under">&nbsp;</div>
                            </div>
                        </div>
                        
                        <div class="sk_txt1">triceps</div>
                        <div class="sk_txt2">biceps</div>
                        <div class="sk_txt3">abdominal</div>
                        <div class="sk_txt4">calf</div>
                                              
                        <div class="hori_content">
                            <ul id="6_horizontal_scale">
                            	<li id="6_hor_scale1"></li>
                                <li id="6_hor_scale2"></li>
                                <li id="6_hor_scale3"></li>
                                <li id="6_hor_scale4"></li>
                                <li id="6_hor_scale5"></li>
                                <li id="6_hor_scale6"></li>
                                <li id="6_hor_scale7"></li>
                                <li id="6_hor_scale8"></li> 
                            </ul>
                        </div>
                        
                        <div class="vert_content">
                            <ul id="6_vertical_scale">
                            	<li id="6_ver_scale1"></li>
                                <li id="6_ver_scale2"></li>
                                <li id="6_ver_scale3"></li>
                                <li id="6_ver_scale4"></li>
                                <li id="6_ver_scale5"></li>
                                <li id="6_ver_scale6"></li>
                                <li id="6_ver_scale7"></li>
                                <li id="6_ver_scale8"></li>
                            </ul>
                        </div>
                        
                        <canvas id="myCanvas6" width="640" height="640" style="position: absolute; top: 0; left:0;">&nbsp;</canvas>                        
                    </div>
                    
              </td>        
            </tr>             	
            
          </tbody>
        </table>
        </div>    
        
        <div style="display:none;">
        	<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>
        </div>
    </div>

    <input type="hidden" id="sumSites" name="sumSites" value="">

	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>

<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>

<script type="text/javascript">        
     
    $(window).bind("load", function() {              
       four_skinfolds_plot();        
    });
    
    function changeMethod(skinfold)
       { 
         if(skinfold == "four_skinfolds")
         {
           document.getElementById('skinfold_4').style.display = "" ;
           document.getElementById('skinfold_6').style.display = "none" ;
           four_skinfolds_plot() ;
         }
         else if(skinfold == "six_skinfolds")
         {
           document.getElementById('skinfold_6').style.display = "" ;
           document.getElementById('skinfold_4').style.display = "none" ;  
           six_skinfolds_plot() ;
         }

       } 
</script>
   
<script type="text/javascript">    
    
   function four_skinfolds_plot()
   {
        var triceps = document.getElementById("triceps").value ;        
        var subscapular = document.getElementById("subscapular").value ;         
        var illiac = document.getElementById("illiac").value ;          
        var thigh = document.getElementById("thigh").value ;  
        
        var height = document.getElementById("height").value ;  
        var body_mass = document.getElementById("body_mass").value ;  
        
        var skinfolds_arr = new Array();
        
        if(document.getElementById('gender').value == "Male")
        {
          var tricepSF = (triceps - 1.28) * 170.18 / height + 1.28 ;
          var subscapularSF = (subscapular - 2.07) * 170.18 / height + 2.07 ;
          var illiacSF = (illiac - 1.27) * 170.18 / height + 1.27 ;
          var thighSF = (thigh - 1.49) * 170.18 / height + 1.49 ;  
                    
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 6.49 ;
            var subscap10 = 9.47 ;
            var illiac10 = 8.23 ;
            var thigh10 = 6.98 ;
            
            var tricep50 = 11.35 ;
            var subscap50 = 16.24 ;
            var illiac50 = 20.96 ;
            var thigh50 = 12.16 ;
            
            var tricep90 = 23.55 ;
            var subscap90 = 30.72 ;
            var illiac90 = 38.02 ;
            var thigh90 = 25.18 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 7.36 ;
            var subscap10 = 11.65 ;
            var illiac10 = 11.41 ;
            var thigh10 = 7.66 ;
            
            var tricep50 = 12.24 ;
            var subscap50 = 19.94 ;
            var illiac50 = 25.64 ;
            var thigh50 = 13.13 ;
            
            var tricep90 = 24.91 ;
            var subscap90 = 32.71 ;
            var illiac90 = 38.45 ;
            var thigh90 = 26.73 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 7.7 ;
            var subscap10 = 13.59 ;
            var illiac10 = 13.98 ;
            var thigh10 = 7.91 ;
            
            var tricep50 = 12.76 ;
            var subscap50 = 21.34 ;
            var illiac50 = 26.1 ;
            var thigh50 = 13.19 ;
            
            var tricep90 = 24.42 ;
            var subscap90 = 34.09 ;
            var illiac90 = 38.75 ;
            var thigh90 = 27.35 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 8.13 ;
            var subscap10 = 13.61 ;
            var illiac10 = 13.86 ;
            var thigh10 = 8.16 ;
            
            var tricep50 =  12.96 ;
            var subscap50 = 21.77 ;
            var illiac50 = 24.39 ;
            var thigh50 = 13.27 ;
            
            var tricep90 = 25.39 ;
            var subscap90 = 33.42 ;
            var illiac90 = 36.8 ;
            var thigh90 = 28.98 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 8.63 ;
            var subscap10 = 14.33 ;
            var illiac10 = 15.74 ;
            var thigh10 = 9.12 ;
            
            var tricep50 = 12.98 ;
            var subscap50 = 22.27 ;
            var illiac50 = 23.5 ;
            var thigh50 = 13.06 ;
            
            var tricep90 = 25.45 ;
            var subscap90 = 35.1 ;
            var illiac90 = 36.67 ;
            var thigh90 = 27.76 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 8.09 ;
            var subscap10 = 12.19 ;
            var illiac10 = 9.98 ;
            var thigh10 = 7.96 ;
            
            var tricep50 = 12.47 ;
            var subscap50 = 19.46 ;
            var illiac50 = 18.14 ;
            var thigh50 = 13.58 ;
            
            var tricep90 = 23.36 ;
            var subscap90 = 30.82 ;
            var illiac90 = 31.79 ;
            var thigh90 = 28.77 ;
          }
        }
        else if(document.getElementById('gender').value == "Female")
        {
          var tricepSF = (triceps - 1.1) * 170.18 / height + 1.1 ;
          var subscapularSF = (subscapular - 1.74) * 170.18 / height + 1.74 ;
          var illiacSF = (illiac - 0.92) * 170.18 / height + 0.92 ;
          var thighSF = (thigh - 1.04) * 170.18 / height + 1.04 ;  
          
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 14.2 ;
            var subscap10 = 15.39 ;
            var illiac10 = 10.19 ;
            var thigh10 = 18.89 ;
            
            var tricep50 = 23.48 ;
            var subscap50 = 31.27 ;
            var illiac50 = 22.93 ;
            var thigh50 = 29.41 ;
            
            var tricep90 = 38.99 ;
            var subscap90 = 49.32 ;
            var illiac90 = 39.71 ;
            var thigh90 = 43.25 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 16.61 ;
            var subscap10 = 19.75 ;
            var illiac10 = 11.56 ;
            var thigh10 = 21.36 ;
            
            var tricep50 = 28.82 ;
            var subscap50 = 32.89 ;
            var illiac50 = 27.91 ;
            var thigh50 = 33.31 ;
            
            var tricep90 = 42.26 ;
            var subscap90 = 49.00 ;
            var illiac90 = 42.1 ;
            var thigh90 = 44.70 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 20.49 ;
            var subscap10 = 19.49 ;
            var illiac10 = 15.5 ;
            var thigh10 = 24.08 ;
            
            var tricep50 = 30.66 ;
            var subscap50 = 33.25 ;
            var illiac50 = 29.99 ;
            var thigh50 = 36.87 ;
            
            var tricep90 = 41.67 ;
            var subscap90 = 48.77 ;
            var illiac90 = 42.08 ;
            var thigh90 = 45.34 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 20.68 ;
            var subscap10 = 18.77 ;
            var illiac10 = 20.68 ;
            var thigh10 = 23.29 ;
            
            var tricep50 = 31.23 ;
            var subscap50 = 32.06 ;
            var illiac50 = 31.23 ;
            var thigh50 = 36.39 ;
            
            var tricep90 = 42.56 ;
            var subscap90 = 48.92 ;
            var illiac90 = 42.56 ;
            var thigh90 = 45.26 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 19.04 ;
            var subscap10 = 13.86 ;
            var illiac10 = 15.07 ;
            var thigh10 = 21.64 ;
            
            var tricep50 = 28.46 ;
            var subscap50 = 26.3 ;
            var illiac50 = 28.21 ;
            var thigh50 = 34.88 ;
            
            var tricep90 = 40.46 ;
            var subscap90 = 44.41 ;
            var illiac90 = 41.51 ;
            var thigh90 = 45.67 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 13.09 ;
            var subscap10 = 14.92 ;
            var illiac10 = 10.63 ;
            var thigh10 = 21.04 ;
            
            var tricep50 = 24.98 ;
            var subscap50 = 24.34 ;
            var illiac50 = 21.87 ;
            var thigh50 = 33.77 ;
            
            var tricep90 = 44.86 ;
            var subscap90 = 37.19 ;
            var illiac90 = 37.03 ;
            var thigh90 = 44.55 ;
          }
        }
    
        skinfolds_arr.push(tricepSF,subscapularSF,illiacSF,thighSF,tricep10,subscap10,illiac10,thigh10,tricep50,subscap50,illiac50,thigh50,tricep90,subscap90,illiac90,thigh90) ;
        
        var maxSkinfold = Math.max.apply(null, skinfolds_arr) ;
        
        
        document.getElementById("hor_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("hor_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("hor_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("hor_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("hor_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("hor_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("hor_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ; 
        document.getElementById("hor_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        
        document.getElementById("ver_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("ver_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("ver_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("ver_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("ver_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("ver_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("ver_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("ver_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ; 
                
        plot_4(maxSkinfold,tricep90,subscap90,thigh90,illiac90,tricep50,tricep10,subscap10,thigh10,illiac10,subscap50,thigh50,illiac50,tricepSF,subscapularSF,thighSF,illiacSF); 
   }
   
   
   
   function six_skinfolds_plot()
   {
        var triceps = document.getElementById("triceps").value ;        
        var biceps = document.getElementById("biceps").value ; 
        var subscapular = document.getElementById("subscapular").value ;                          
        var abdominal = document.getElementById("abdominal").value ; 
        var supraspinale = document.getElementById("supraspinale").value ; 
        var calf = document.getElementById("calf").value ;  

        var height = document.getElementById("height").value ;  
        var body_mass = document.getElementById("body_mass").value ;  
        
        var skinfolds_arr = new Array();
        
        if(document.getElementById('gender').value == "Male")
        {
          var tricepSF = (triceps - 1.28) * 170.18 / height + 1.28 ;
          var bicepSF = (biceps - 0.77) * 170.18 / height + 0.77 ;
          var subscapularSF = (subscapular - 2.07) * 170.18 / height + 2.07 ;
          var abdominalSF = (abdominal - 1.49) * 170.18 / height + 1.49 ;
          var supraspinaleSF = (supraspinale - 1.27) * 170.18 / height + 1.27 ;
          var calfSF = (calf - 0.89) * 170.18 / height + 0.89 ;
                    
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 6.49 ;
            var subscap10 = 9.47 ;
            var bicep10 = 3.04 ;            
            var abdominal10 = 8.4 ;
            var supras10 = 4.74 ;
            var calf10 = 5.3 ;
            
            var tricep50 = 11.35 ;
            var subscap50 = 16.24 ;
            var bicep50 = 4.77 ;            
            var abdominal50 = 23.06 ;
            var supras50 = 8.47 ;
            var calf50 = 9.01 ;
            
            var tricep90 = 23.55 ;
            var subscap90 = 30.72 ;
            var bicep90 = 11.37 ;            
            var abdominal90 = 39.15 ;
            var supras90 = 22.88 ;
            var calf90 = 17.83 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 7.36 ;
            var subscap10 = 11.65 ;
            var bicep10 = 3.87 ;            
            var abdominal10 = 18.14 ;
            var supras10 = 6.55 ;
            var calf10 = 6.26 ;
            
            var tricep50 = 12.24 ;
            var subscap50 = 19.94 ;
            var bicep50 = 5.71 ;            
            var abdominal50 = 29.48 ;
            var supras50 = 11.01 ;
            var calf50 = 9.98 ;
            
            var tricep90 = 24.91 ;
            var subscap90 = 32.71 ;
            var bicep90 = 11.3 ;            
            var abdominal90 = 39.06 ;
            var supras90 = 20.85 ;
            var calf90 = 16.8 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 7.7 ;
            var subscap10 = 13.59 ;
            var bicep10 = 4.28 ;            
            var abdominal10 = 18.62 ;
            var supras10 = 7.19 ;
            var calf10 = 7.04 ;
            
            var tricep50 = 12.76 ;
            var subscap50 = 21.34 ;
            var bicep50 = 6.45 ;            
            var abdominal50 = 31.58 ;
            var supras50 = 11.38 ;
            var calf50 = 11.27 ;
            
            var tricep90 = 24.42 ;
            var subscap90 = 34.09 ;
            var bicep90 = 12.58 ;            
            var abdominal90 = 40.9 ;
            var supras90 = 25.47 ;
            var calf90 = 19.62 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 8.13 ;
            var subscap10 = 13.61 ;
            var bicep10 = 4.32 ;            
            var abdominal10 = 17.15 ;
            var supras10 = 7 ;
            var calf10 = 6.54 ;
            
            var tricep50 =  12.96 ;
            var subscap50 = 21.77 ;
            var bicep50 = 6.69 ;            
            var abdominal50 = 29.97 ;
            var supras50 = 10.79 ;
            var calf50 = 10.9 ;
            
            var tricep90 = 25.39 ;
            var subscap90 = 33.42 ;
            var bicep90 = 11.35 ;            
            var abdominal90 = 40.09 ;
            var supras90 = 20.8 ;
            var calf90 = 17.49 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 8.63 ;
            var subscap10 = 14.33 ;
            var bicep10 = 4.44 ;            
            var abdominal10 = 19.4 ;
            var supras10 = 7.54 ;
            var calf10 = 6.7 ;
            
            var tricep50 = 12.98 ;
            var subscap50 = 22.27 ;
            var bicep50 = 6.58 ;            
            var abdominal50 = 27.46 ;
            var supras50 = 11.94 ;
            var calf50 = 10.07 ;
            
            var tricep90 = 25.45 ;
            var subscap90 = 35.1 ;
            var bicep90 = 13.1 ;            
            var abdominal90 = 39.61 ;
            var supras90 = 21.19 ;
            var calf90 = 18.24 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 8.09 ;
            var subscap10 = 12.19 ;
            var bicep10 = 3.88 ;            
            var abdominal10 = 13.91 ;
            var supras10 = 5.93 ;
            var calf10 = 5.56 ;
            
            var tricep50 = 12.47 ;
            var subscap50 = 19.46 ;
            var bicep50 = 6.18 ;            
            var abdominal50 = 23.04 ;
            var supras50 = 9.27 ;
            var calf50 = 8.47 ;
            
            var tricep90 = 23.36 ;
            var subscap90 = 30.82 ;
            var bicep90 = 9.22 ;            
            var abdominal90 = 31.92 ;
            var supras90 = 14.82 ;
            var calf90 = 16.52 ;
          }
        }
        else if(document.getElementById('gender').value == "Female")
        {
          var tricepSF = (triceps - 1.1) * 170.18 / height + 1.1 ;
          var bicepSF = (biceps - 0.49) * 170.18 / height + 0.49 ;
          var subscapularSF = (subscapular - 1.74) * 170.18 / height + 1.74 ;
          var abdominalSF = (abdominal - 1.04) * 170.18 / height + 1.04 ;
          var supraspinaleSF = (supraspinale - 0.92) * 170.18 / height + 0.92 ;
          var calfSF = (calf - 0.79) * 170.18 / height + 0.79 ;
          
          if(document.getElementById("age_range").value == "18-29")
          {
            var tricep10 = 14.12 ;
            var subscap10 = 15.39 ;
            var bicep10 = 6.27 ;            
            var abdominal10 = 17.41 ;
            var supras10 = 7.89 ;
            var calf10 = 14.95 ;
            
            var tricep50 = 23.48 ;
            var subscap50 = 31.27 ;
            var bicep50 = 9.99 ;            
            var abdominal50 = 29.36 ;
            var supras50 = 13.37 ;
            var calf50 = 23.21 ;
            
            var tricep90 = 38.99 ;
            var subscap90 = 49.32 ;
            var bicep90 = 25.1 ;            
            var abdominal90 = 41.13 ;
            var supras90 = 21.5 ;
            var calf90 = 40.42 ;
          }
          else if(document.getElementById("age_range").value == "30-39")
          {
            var tricep10 = 16.61 ;
            var subscap10 = 19.75 ;
            var bicep10 = 5.5 ;            
            var abdominal10 = 16.81 ;
            var supras10 = 6.58 ;
            var calf10 = 11.66 ;
            
            var tricep50 = 28.82 ;
            var subscap50 = 32.89 ;
            var bicep50 = 10.06 ;            
            var abdominal50 = 29.45 ;
            var supras50 = 12.12 ;
            var calf50 = 21.64 ;
            
            var tricep90 = 42.26 ;
            var subscap90 = 49.00 ;
            var bicep90 = 26.58 ;            
            var abdominal90 = 42.98 ;
            var supras90 = 28.75 ;
            var calf90 = 41.23 ;
          }
          else if(document.getElementById("age_range").value == "40-49")
          {
            var tricep10 = 20.49 ;
            var subscap10 = 19.49 ;
            var bicep10 = 7.01 ;            
            var abdominal10 = 21.9 ;
            var supras10 = 8.73 ;
            var calf10 = 16.62 ;
            
            var tricep50 = 30.66 ;
            var subscap50 = 33.25 ;
            var bicep50 = 14.49 ;            
            var abdominal50 = 39.36 ;
            var supras50 = 19.72 ;
            var calf50 = 29.08 ;
            
            var tricep90 = 41.67 ;
            var subscap90 = 48.77 ;
            var bicep90 = 31.93 ;            
            var abdominal90 = 43.76 ;
            var supras90 = 34.38 ;
            var calf90 = 42.31 ;
          }
          else if(document.getElementById("age_range").value == "50-59")
          {
            var tricep10 = 20.68 ;
            var subscap10 = 18.77 ;
            var bicep10 = 7.68 ;            
            var abdominal10 = 25.9 ;
            var supras10 = 11.24 ;
            var calf10 = 15.24 ;
            
            var tricep50 = 31.23 ;
            var subscap50 = 32.06 ;
            var bicep50 = 15.07 ;            
            var abdominal50 = 41.11 ;
            var supras50 = 20.96 ;
            var calf50 = 26.67 ;
            
            var tricep90 = 42.56 ;
            var subscap90 = 48.92 ;
            var bicep90 = 30.7 ;            
            var abdominal90 = 44.77 ;
            var supras90 = 32.76 ;
            var calf90 = 42.66 ;
          }
          else if(document.getElementById("age_range").value == "60-69")
          {
            var tricep10 = 19.04 ;
            var subscap10 = 13.86 ;
            var bicep10 = 8.8 ;            
            var abdominal10 = 30.15 ;
            var supras10 = 12.32 ;
            var calf10 = 16.04 ;
            
            var tricep50 = 28.46 ;
            var subscap50 = 26.3 ;
            var bicep50 = 12.72 ;            
            var abdominal50 = 40.85 ;
            var supras50 = 20.21 ;
            var calf50 = 28.18 ;
            
            var tricep90 = 40.46 ;
            var subscap90 = 44.41 ;
            var bicep90 = 24.72 ;            
            var abdominal90 = 45.91 ;
            var supras90 = 36.22 ;
            var calf90 = 42.58 ;
          }
          else if(document.getElementById("age_range").value == "70+")
          {
            var tricep10 = 13.09 ;
            var subscap10 = 14.92 ;
            var bicep10 = 8.12 ;            
            var abdominal10 = 28.34 ;
            var supras10 = 11.31 ;
            var calf10 = 17.61 ;
            
            var tricep50 = 24.98 ;
            var subscap50 = 24.34 ;
            var bicep50 = 12.44 ;            
            var abdominal50 = 41.79 ;
            var supras50 = 19.64 ;
            var calf50 = 28.79 ;
            
            var tricep90 = 44.86 ;
            var subscap90 = 37.19 ;
            var bicep90 = 24.75 ;            
            var abdominal90 = 44.25 ;
            var supras90 = 31.01 ;
            var calf90 = 43.83 ;
          }
        }
    
        skinfolds_arr.push(tricepSF,subscapularSF,bicepSF,abdominalSF,supraspinaleSF,calfSF,tricep10,subscap10,bicep10,abdominal10,supras10,calf10,tricep50,subscap50,bicep50,abdominal50,supras50,calf50,tricep90,subscap90,bicep90,abdominal90,supras90,calf90) ;
        
        var maxSkinfold = Math.max.apply(null, skinfolds_arr) ;        
        
        document.getElementById("6_hor_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("6_hor_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("6_hor_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("6_hor_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("6_hor_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("6_hor_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("6_hor_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ; 
        document.getElementById("6_hor_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        
        document.getElementById("6_ver_scale1").innerHTML = Math.round(maxSkinfold * 10) / 10 ;
        document.getElementById("6_ver_scale2").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("6_ver_scale3").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("6_ver_scale4").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("6_ver_scale5").innerHTML = Math.round((maxSkinfold * 0.25) * 10) / 10 ;
        document.getElementById("6_ver_scale6").innerHTML = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
        document.getElementById("6_ver_scale7").innerHTML = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
        document.getElementById("6_ver_scale8").innerHTML = Math.round(maxSkinfold * 10) / 10 ; 
                
        plot_6(maxSkinfold,tricep90,subscap90,bicep90,abdominal90,supras90,calf90,tricep50,subscap50,bicep50,abdominal50,supras50,calf50,tricep10,subscap10,bicep10,abdominal10,supras10,calf10,tricepSF,subscapularSF,bicepSF,abdominalSF,supraspinaleSF,calfSF);
   }
  
</script>        
    
<script src="<?php echo "$base/assets/js/"?>jCanvas.js"></script>

<script>

function plot_4(maxSkinfold,tricep90,subscap90,thigh90,illiac90,tricep50,tricep10,subscap10,thigh10,illiac10,subscap50,thigh50,illiac50,tricepSF,subscapularSF,thighSF,illiacSF){   
    
 var max1 = Math.round(maxSkinfold * 10) / 10 ;
 var max2 = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
 var max3 = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
 var max4 = Math.round((maxSkinfold * 0.25) * 10) / 10 ;

 // Plot tricep90       
 var x1_90 ; var y1_90 ;
 if(tricep90 >= max1)
 {
     x1_90 = 50 ; 
     y1_90 = 150 ;
 }
 else if(tricep90 < max1 && tricep90 > ((max1 + max2)/2))
 {
     x1_90 = 60 ; 
     y1_90 = 153 ;
 }
 else if(tricep90 == ((max1 + max2)/2))
 {
     x1_90 = 85 ; 
     y1_90 = 170 ;
 }
 else if(tricep90 < ((max1 + max2)/2) && tricep90 > max2)
 {
     x1_90 = 110 ; 
     y1_90 = 185 ;
 }
 else if(tricep90 == max2)
 {
     x1_90 = 115 ; 
     y1_90 = 190 ;
 }
 else if(tricep90 < max2 && tricep90 > ((max2 + max3)/2))
 {
     x1_90 = 130 ; 
     y1_90 = 200 ;
 }
 else if(tricep90 == ((max2 + max3)/2))
 {
     x1_90 = 155 ; 
     y1_90 = 220 ;
 }
 else if(tricep90 < ((max2 + max3)/2) && tricep90 > max3)
 {
     x1_90 = 175 ; 
     y1_90 = 230 ;
 }
 else if(tricep90 == max3)
 {
     x1_90 = 185 ; 
     y1_90 = 235 ;
 }
 else if(tricep90 < max3 && tricep90 > ((max3 + max4)/2))
 {
     x1_90 = 200 ; 
     y1_90 = 245 ;
 }
 else if(tricep90 == ((max3 + max4)/2))
 {
     x1_90 = 225 ; 
     y1_90 = 260 ;
 }
 else if(tricep90 < ((max3 + max4)/2) && tricep90 > max4)
 {
     x1_90 = 245 ; 
     y1_90 = 273 ;
 }
 else if(tricep90 == max4)
 {
     x1_90 = 250 ; 
     y1_90 = 275 ;
     
 }
 else if(tricep90 < max4 && tricep90 > (max4 / 2))
 {
     x1_90 = 255 ; 
     y1_90 = 282 ;
 }
 else if(tricep90 <= (max4 / 2))
 {
     x1_90 = 265 ; 
     y1_90 = 285 ;
 }


 // Plot subscap90       
 var x2_90 ; var y2_90 ;
 if(subscap90 >= max1)
 {
     x2_90 = 583 ; 
     y2_90 = 143 ;
 }
 else if(subscap90 < max1 && subscap90 > ((max1 + max2)/2))
 {
     x2_90 = 570 ; 
     y2_90 = 150 ;
 }
 else if(subscap90 == ((max1 + max2)/2))
 {
     x2_90 = 555 ; 
     y2_90 = 160 ;
 }
 else if(subscap90 < ((max1 + max2)/2) && subscap90 > max2)
 {
     x2_90 = 530 ; 
     y2_90 = 178 ;
 }
 else if(subscap90 == max2)
 {
     x2_90 = 520 ; 
     y2_90 = 186 ;
 }
 else if(subscap90 < max2 && subscap90 > ((max2 + max3)/2))
 {
     x2_90 = 508 ; 
     y2_90 = 193 ;
 }
 else if(subscap90 == ((max2 + max3)/2))
 {
     x2_90 = 485 ; 
     y2_90 = 210 ;
 }
 else if(subscap90 < ((max2 + max3)/2) && subscap90 > max3)
 {
     x2_90 = 465 ; 
     y2_90 = 223 ;
 }
 else if(subscap90 == max3)
 {
     x2_90 = 453 ; 
     y2_90 = 232 ;
 }
 else if(subscap90 < max3 && subscap90 > ((max3 + max4)/2))
 {
     x2_90 = 440 ; 
     y2_90 = 240 ;
 }
 else if(subscap90 == ((max3 + max4)/2))
 {
     x2_90 = 415 ; 
     y2_90 = 255 ;
 }
 else if(subscap90 < ((max3 + max4)/2) && subscap90 > max4)
 {
     x2_90 = 395 ; 
     y2_90 = 270 ;
 }
 else if(subscap90 == max4)
 {
     x2_90 = 385 ; 
     y2_90 = 278 ;
     
 }
 else if(subscap90 < max4 && subscap90 > (max4 / 2))
 {
     x2_90 = 380 ; 
     y2_90 = 280 ;
 }
 else if(subscap90 <= (max4 / 2))
 {
     x2_90 = 375 ; 
     y2_90 = 285 ;
 }
 
 // Plot thigh90      
 var x3_90 ; var y3_90 ;
 if(thigh90 >= max1)
 {
     x3_90 = 580 ; 
     y3_90 = 495 ;
 }
 else if(thigh90 < max1 && thigh90 > ((max1 + max2)/2))
 {
     x3_90 = 573 ; 
     y3_90 = 492 ;
 }
 else if(thigh90 == ((max1 + max2)/2))
 {
     x3_90 = 555 ; 
     y3_90 = 478 ;
 }
 else if(thigh90 < ((max1 + max2)/2) && thigh90 > max2)
 {
     x3_90 = 530 ; 
     y3_90 = 465 ;
 }
 else if(thigh90 == max2)
 {
     x3_90 = 515 ; 
     y3_90 = 453 ;
 }
 else if(thigh90 < max2 && thigh90 > ((max2 + max3)/2))
 {
     x3_90 = 505 ; 
     y3_90 = 446 ;
 }
 else if(thigh90 == ((max2 + max3)/2))
 {
     x3_90 = 485 ; 
     y3_90 = 432 ;
 }
 else if(thigh90 < ((max2 + max3)/2) && thigh90 > max3)
 {
     x3_90 = 463 ; 
     y3_90 = 416 ;
 }
 else if(thigh90 == max3)
 {
     x3_90 = 450 ; 
     y3_90 = 405 ;
 }
 else if(thigh90 < max3 && thigh90 > ((max3 + max4)/2))
 {
     x3_90 = 440 ; 
     y3_90 = 398 ;
 }
 else if(thigh90 == ((max3 + max4)/2))
 {
     x3_90 = 420 ; 
     y3_90 = 383 ;
 }
 else if(thigh90 < ((max3 + max4)/2) && thigh90 > max4)
 {
     x3_90 = 395 ; 
     y3_90 = 370 ;
 }
 else if(thigh90 == max4)
 {
     x3_90 = 385 ; 
     y3_90 = 363 ;
     
 }
 else if(thigh90 < max4 && thigh90 > (max4 / 2))
 {
     x3_90 = 380 ; 
     y3_90 = 360 ;
 }
 else if(thigh90 <= (max4 / 2))
 {
     x3_90 = 370 ; 
     y3_90 = 355 ;
 }
 
 
 // Plot illiac90      
 var x4_90 ; var y4_90 ;
 if(illiac90 >= max1)
 {
     x4_90 = 63 ; 
     y4_90 = 500 ;
 }
 else if(illiac90 < max1 && illiac90 > ((max1 + max2)/2))
 {
     x4_90 = 68 ; 
     y4_90 = 495 ;
 }
 else if(illiac90 == ((max1 + max2)/2))
 {
     x4_90 = 90 ; 
     y4_90 = 480 ;
 }
 else if(illiac90 < ((max1 + max2)/2) && illiac90 > max2)
 {
     x4_90 = 113 ; 
     y4_90 = 463 ;
 }
 else if(illiac90 == max2)
 {
     x4_90 = 128 ; 
     y4_90 = 453 ;
 }
 else if(illiac90 < max2 && illiac90 > ((max2 + max3)/2))
 {
     x4_90 = 133 ; 
     y4_90 = 450 ;
 }
 else if(illiac90 == ((max2 + max3)/2))
 {
     x4_90 = 155 ; 
     y4_90 = 435 ;
 }
 else if(illiac90 < ((max2 + max3)/2) && illiac90 > max3)
 {
     x4_90 = 180 ; 
     y4_90 = 418 ;
 }
 else if(illiac90 == max3)
 {
     x4_90 = 192 ; 
     y4_90 = 408 ;
 }
 else if(illiac90 < max3 && illiac90 > ((max3 + max4)/2))
 {
     x4_90 = 203 ; 
     y4_90 = 400 ;
 }
 else if(illiac90 == ((max3 + max4)/2))
 {
     x4_90 = 225 ; 
     y4_90 = 385 ;
 }
 else if(illiac90 < ((max3 + max4)/2) && illiac90 > max4)
 {
     x4_90 = 245 ; 
     y4_90 = 372 ;
 }
 else if(illiac90 == max4)
 {
     x4_90 = 255 ; 
     y4_90 = 365 ;
     
 }
 else if(illiac90 < max4 && illiac90 > (max4 / 2))
 {
     x4_90 = 260 ; 
     y4_90 = 360 ;
 }
 else if(illiac90 <= (max4 / 2))
 {
     x4_90 = 368 ; 
     y4_90 = 355 ;
 }
 
 

 // Plot tricep50       
 var x1_50 ; var y1_50 ;
 if(tricep50 >= max1)
 {
     x1_50 = 50 ; 
     y1_50 = 150 ;
 }
 else if(tricep50 < max1 && tricep50 > ((max1 + max2)/2))
 {
     x1_50 = 60 ; 
     y1_50 = 153 ;
 }
 else if(tricep50 == ((max1 + max2)/2))
 {
     x1_50 = 85 ; 
     y1_50 = 170 ;
 }
 else if(tricep50 < ((max1 + max2)/2) && tricep50 > max2)
 {
     x1_50 = 110 ; 
     y1_50 = 185 ;
 }
 else if(tricep50 == max2)
 {
     x1_50 = 115 ; 
     y1_50 = 190 ;
 }
 else if(tricep50 < max2 && tricep50 > ((max2 + max3)/2))
 {
     x1_50 = 130 ; 
     y1_50 = 200 ;
 }
 else if(tricep50 == ((max2 + max3)/2))
 {
     x1_50 = 155 ; 
     y1_50 = 220 ;
 }
 else if(tricep50 < ((max2 + max3)/2) && tricep50 > max3)
 {
     x1_50 = 175 ; 
     y1_50 = 230 ;
 }
 else if(tricep50 == max3)
 {
     x1_50 = 185 ; 
     y1_50 = 235 ;
 }
 else if(tricep50 < max3 && tricep50 > ((max3 + max4)/2))
 {
     x1_50 = 200 ; 
     y1_50 = 245 ;
 }
 else if(tricep50 == ((max3 + max4)/2))
 {
     x1_50 = 225 ; 
     y1_50 = 260 ;
 }
 else if(tricep50 < ((max3 + max4)/2) && tricep50 > max4)
 {
     x1_50 = 245 ; 
     y1_50 = 273 ;
 }
 else if(tricep50 == max4)
 {
     x1_50 = 250 ; 
     y1_50 = 275 ;
     
 }
 else if(tricep50 < max4 && tricep50 > (max4 / 2))
 {
     x1_50 = 255 ; 
     y1_50 = 282 ;
 }
 else if(tricep50 <= (max4 / 2))
 {
     x1_50 = 265 ; 
     y1_50 = 285 ;
 }


 // Plot subscap50       
 var x2_50 ; var y2_50 ;
 if(subscap50 >= max1)
 {
     x2_50 = 583 ; 
     y2_50 = 143 ;
 }
 else if(subscap50 < max1 && subscap50 > ((max1 + max2)/2))
 {
     x2_50 = 570 ; 
     y2_50 = 150 ;
 }
 else if(subscap50 == ((max1 + max2)/2))
 {
     x2_50 = 555 ; 
     y2_50 = 160 ;
 }
 else if(subscap50 < ((max1 + max2)/2) && subscap50 > max2)
 {
     x2_50 = 530 ; 
     y2_50 = 178 ;
 }
 else if(subscap50 == max2)
 {
     x2_50 = 520 ; 
     y2_50 = 186 ;
 }
 else if(subscap50 < max2 && subscap50 > ((max2 + max3)/2))
 {
     x2_50 = 508 ; 
     y2_50 = 193 ;
 }
 else if(subscap50 == ((max2 + max3)/2))
 {
     x2_50 = 485 ; 
     y2_50 = 210 ;
 }
 else if(subscap50 < ((max2 + max3)/2) && subscap50 > max3)
 {
     x2_50 = 465 ; 
     y2_50 = 223 ;
 }
 else if(subscap50 == max3)
 {
     x2_50 = 453 ; 
     y2_50 = 232 ;
 }
 else if(subscap50 < max3 && subscap50 > ((max3 + max4)/2))
 {
     x2_50 = 440 ; 
     y2_50 = 240 ;
 }
 else if(subscap50 == ((max3 + max4)/2))
 {
     x2_50 = 415 ; 
     y2_50 = 255 ;
 }
 else if(subscap50 < ((max3 + max4)/2) && subscap50 > max4)
 {
     x2_50 = 395 ; 
     y2_50 = 270 ;
 }
 else if(subscap50 == max4)
 {
     x2_50 = 385 ; 
     y2_50 = 278 ;
     
 }
 else if(subscap50 < max4 && subscap50 > (max4 / 2))
 {
     x2_50 = 380 ; 
     y2_50 = 280 ;
 }
 else if(subscap50 <= (max4 / 2))
 {
     x2_50 = 375 ; 
     y2_50 = 285 ;
 }
 
 // Plot thigh50      
 var x3_50 ; var y3_50 ;
 if(thigh50 >= max1)
 {
     x3_50 = 580 ; 
     y3_50 = 495 ;
 }
 else if(thigh50 < max1 && thigh50 > ((max1 + max2)/2))
 {
     x3_50 = 573 ; 
     y3_50 = 492 ;
 }
 else if(thigh50 == ((max1 + max2)/2))
 {
     x3_50 = 555 ; 
     y3_50 = 478 ;
 }
 else if(thigh50 < ((max1 + max2)/2) && thigh50 > max2)
 {
     x3_50 = 530 ; 
     y3_50 = 465 ;
 }
 else if(thigh50 == max2)
 {
     x3_50 = 515 ; 
     y3_50 = 453 ;
 }
 else if(thigh50 < max2 && thigh50 > ((max2 + max3)/2))
 {
     x3_50 = 505 ; 
     y3_50 = 446 ;
 }
 else if(thigh50 == ((max2 + max3)/2))
 {
     x3_50 = 485 ; 
     y3_50 = 432 ;
 }
 else if(thigh50 < ((max2 + max3)/2) && thigh50 > max3)
 {
     x3_50 = 463 ; 
     y3_50 = 416 ;
 }
 else if(thigh50 == max3)
 {
     x3_50 = 450 ; 
     y3_50 = 405 ;
 }
 else if(thigh50 < max3 && thigh50 > ((max3 + max4)/2))
 {
     x3_50 = 440 ; 
     y3_50 = 398 ;
 }
 else if(thigh50 == ((max3 + max4)/2))
 {
     x3_50 = 420 ; 
     y3_50 = 383 ;
 }
 else if(thigh50 < ((max3 + max4)/2) && thigh50 > max4)
 {
     x3_50 = 395 ; 
     y3_50 = 370 ;
 }
 else if(thigh50 == max4)
 {
     x3_50 = 385 ; 
     y3_50 = 363 ;
     
 }
 else if(thigh50 < max4 && thigh50 > (max4 / 2))
 {
     x3_50 = 380 ; 
     y3_50 = 360 ;
 }
 else if(thigh50 <= (max4 / 2))
 {
     x3_50 = 370 ; 
     y3_50 = 355 ;
 }
 
 
 // Plot illiac50      
 var x4_50 ; var y4_50 ;
 if(illiac50 >= max1)
 {
     x4_50 = 63 ; 
     y4_50 = 500 ;
 }
 else if(illiac50 < max1 && illiac50 > ((max1 + max2)/2))
 {
     x4_50 = 68 ; 
     y4_50 = 495 ;
 }
 else if(illiac50 == ((max1 + max2)/2))
 {
     x4_50 = 90 ; 
     y4_50 = 480 ;
 }
 else if(illiac50 < ((max1 + max2)/2) && illiac50 > max2)
 {
     x4_50 = 113 ; 
     y4_50 = 463 ;
 }
 else if(illiac50 == max2)
 {
     x4_50 = 128 ; 
     y4_50 = 453 ;
 }
 else if(illiac50 < max2 && illiac50 > ((max2 + max3)/2))
 {
     x4_50 = 133 ; 
     y4_50 = 450 ;
 }
 else if(illiac50 == ((max2 + max3)/2))
 {
     x4_50 = 155 ; 
     y4_50 = 435 ;
 }
 else if(illiac50 < ((max2 + max3)/2) && illiac50 > max3)
 {
     x4_50 = 180 ; 
     y4_50 = 418 ;
 }
 else if(illiac50 == max3)
 {
     x4_50 = 192 ; 
     y4_50 = 408 ;
 }
 else if(illiac50 < max3 && illiac50 > ((max3 + max4)/2))
 {
     x4_50 = 203 ; 
     y4_50 = 400 ;
 }
 else if(illiac50 == ((max3 + max4)/2))
 {
     x4_50 = 225 ; 
     y4_50 = 385 ;
 }
 else if(illiac50 < ((max3 + max4)/2) && illiac50 > max4)
 {
     x4_50 = 245 ; 
     y4_50 = 372 ;
 }
 else if(illiac50 == max4)
 {
     x4_50 = 255 ; 
     y4_50 = 365 ;
     
 }
 else if(illiac50 < max4 && illiac50 > (max4 / 2))
 {
     x4_50 = 260 ; 
     y4_50 = 360 ;
 }
 else if(illiac50 <= (max4 / 2))
 {
     x4_50 = 368 ; 
     y4_50 = 355 ;
 }
 
 
 // Plot tricep10       
 var x1_10 ; var y1_10 ;
 if(tricep10 >= max1)
 {
     x1_10 = 50 ; 
     y1_10 = 150 ;
 }
 else if(tricep10 < max1 && tricep10 > ((max1 + max2)/2))
 {
     x1_10 = 60 ; 
     y1_10 = 153 ;
 }
 else if(tricep10 == ((max1 + max2)/2))
 {
     x1_10 = 85 ; 
     y1_10 = 170 ;
 }
 else if(tricep10 < ((max1 + max2)/2) && tricep10 > max2)
 {
     x1_10 = 110 ; 
     y1_10 = 185 ;
 }
 else if(tricep10 == max2)
 {
     x1_10 = 115 ; 
     y1_10 = 190 ;
 }
 else if(tricep10 < max2 && tricep10 > ((max2 + max3)/2))
 {
     x1_10 = 130 ; 
     y1_10 = 200 ;
 }
 else if(tricep10 == ((max2 + max3)/2))
 {
     x1_10 = 155 ; 
     y1_10 = 220 ;
 }
 else if(tricep10 < ((max2 + max3)/2) && tricep10 > max3)
 {
     x1_10 = 175 ; 
     y1_10 = 230 ;
 }
 else if(tricep10 == max3)
 {
     x1_10 = 185 ; 
     y1_10 = 235 ;
 }
 else if(tricep10 < max3 && tricep10 > ((max3 + max4)/2))
 {
     x1_10 = 200 ; 
     y1_10 = 245 ;
 }
 else if(tricep10 == ((max3 + max4)/2))
 {
     x1_10 = 225 ; 
     y1_10 = 260 ;
 }
 else if(tricep10 < ((max3 + max4)/2) && tricep10 > max4)
 {
     x1_10 = 245 ; 
     y1_10 = 273 ;
 }
 else if(tricep10 == max4)
 {
     x1_10 = 250 ; 
     y1_10 = 275 ;
     
 }
 else if(tricep10 < max4 && tricep10 > (max4 / 2))
 {
     x1_10 = 255 ; 
     y1_10 = 282 ;
 }
 else if(tricep10 <= (max4 / 2))
 {
     x1_10 = 265 ; 
     y1_10 = 285 ;
 }


 // Plot subscap10       
 var x2_10 ; var y2_10 ;
 if(subscap10 >= max1)
 {
     x2_10 = 583 ; 
     y2_10 = 143 ;
 }
 else if(subscap10 < max1 && subscap10 > ((max1 + max2)/2))
 {
     x2_10 = 570 ; 
     y2_10 = 150 ;
 }
 else if(subscap10 == ((max1 + max2)/2))
 {
     x2_10 = 555 ; 
     y2_10 = 160 ;
 }
 else if(subscap10 < ((max1 + max2)/2) && subscap10 > max2)
 {
     x2_10 = 530 ; 
     y2_10 = 178 ;
 }
 else if(subscap10 == max2)
 {
     x2_10 = 520 ; 
     y2_10 = 186 ;
 }
 else if(subscap10 < max2 && subscap10 > ((max2 + max3)/2))
 {
     x2_10 = 508 ; 
     y2_10 = 193 ;
 }
 else if(subscap10 == ((max2 + max3)/2))
 {
     x2_10 = 485 ; 
     y2_10 = 210 ;
 }
 else if(subscap10 < ((max2 + max3)/2) && subscap10 > max3)
 {
     x2_10 = 465 ; 
     y2_10 = 223 ;
 }
 else if(subscap10 == max3)
 {
     x2_10 = 453 ; 
     y2_10 = 232 ;
 }
 else if(subscap10 < max3 && subscap10 > ((max3 + max4)/2))
 {
     x2_10 = 440 ; 
     y2_10 = 240 ;
 }
 else if(subscap10 == ((max3 + max4)/2))
 {
     x2_10 = 415 ; 
     y2_10 = 255 ;
 }
 else if(subscap10 < ((max3 + max4)/2) && subscap10 > max4)
 {
     x2_10 = 395 ; 
     y2_10 = 270 ;
 }
 else if(subscap10 == max4)
 {
     x2_10 = 385 ; 
     y2_10 = 278 ;
     
 }
 else if(subscap10 < max4 && subscap10 > (max4 / 2))
 {
     x2_10 = 380 ; 
     y2_10 = 280 ;
 }
 else if(subscap10 <= (max4 / 2))
 {
     x2_10 = 375 ; 
     y2_10 = 285 ;
 }
 
 // Plot thigh10      
 var x3_10 ; var y3_10 ;
 if(thigh10 >= max1)
 {
     x3_10 = 580 ; 
     y3_10 = 495 ;
 }
 else if(thigh10 < max1 && thigh10 > ((max1 + max2)/2))
 {
     x3_10 = 573 ; 
     y3_10 = 492 ;
 }
 else if(thigh10 == ((max1 + max2)/2))
 {
     x3_10 = 555 ; 
     y3_10 = 478 ;
 }
 else if(thigh10 < ((max1 + max2)/2) && thigh10 > max2)
 {
     x3_10 = 530 ; 
     y3_10 = 465 ;
 }
 else if(thigh10 == max2)
 {
     x3_10 = 515 ; 
     y3_10 = 453 ;
 }
 else if(thigh10 < max2 && thigh10 > ((max2 + max3)/2))
 {
     x3_10 = 505 ; 
     y3_10 = 446 ;
 }
 else if(thigh10 == ((max2 + max3)/2))
 {
     x3_10 = 485 ; 
     y3_10 = 432 ;
 }
 else if(thigh10 < ((max2 + max3)/2) && thigh10 > max3)
 {
     x3_10 = 463 ; 
     y3_10 = 416 ;
 }
 else if(thigh10 == max3)
 {
     x3_10 = 450 ; 
     y3_10 = 405 ;
 }
 else if(thigh10 < max3 && thigh10 > ((max3 + max4)/2))
 {
     x3_10 = 440 ; 
     y3_10 = 398 ;
 }
 else if(thigh10 == ((max3 + max4)/2))
 {
     x3_10 = 420 ; 
     y3_10 = 383 ;
 }
 else if(thigh10 < ((max3 + max4)/2) && thigh10 > max4)
 {
     x3_10 = 395 ; 
     y3_10 = 370 ;
 }
 else if(thigh10 == max4)
 {
     x3_10 = 385 ; 
     y3_10 = 363 ;
     
 }
 else if(thigh10 < max4 && thigh10 > (max4 / 2))
 {
     x3_10 = 380 ; 
     y3_10 = 360 ;
 }
 else if(thigh10 <= (max4 / 2))
 {
     x3_10 = 370 ; 
     y3_10 = 355 ;
 }
 
 
 // Plot illiac10      
 var x4_10 ; var y4_10 ;
 if(illiac10 >= max1)
 {
     x4_10 = 63 ; 
     y4_10 = 500 ;
 }
 else if(illiac10 < max1 && illiac10 > ((max1 + max2)/2))
 {
     x4_10 = 68 ; 
     y4_10 = 495 ;
 }
 else if(illiac10 == ((max1 + max2)/2))
 {
     x4_10 = 90 ; 
     y4_10 = 480 ;
 }
 else if(illiac10 < ((max1 + max2)/2) && illiac10 > max2)
 {
     x4_10 = 113 ; 
     y4_10 = 463 ;
 }
 else if(illiac10 == max2)
 {
     x4_10 = 128 ; 
     y4_10 = 453 ;
 }
 else if(illiac10 < max2 && illiac10 > ((max2 + max3)/2))
 {
     x4_10 = 133 ; 
     y4_10 = 450 ;
 }
 else if(illiac10 == ((max2 + max3)/2))
 {
     x4_10 = 155 ; 
     y4_10 = 435 ;
 }
 else if(illiac10 < ((max2 + max3)/2) && illiac10 > max3)
 {
     x4_10 = 180 ; 
     y4_10 = 418 ;
 }
 else if(illiac10 == max3)
 {
     x4_10 = 192 ; 
     y4_10 = 408 ;
 }
 else if(illiac10 < max3 && illiac10 > ((max3 + max4)/2))
 {
     x4_10 = 203 ; 
     y4_10 = 400 ;
 }
 else if(illiac10 == ((max3 + max4)/2))
 {
     x4_10 = 225 ; 
     y4_10 = 385 ;
 }
 else if(illiac10 < ((max3 + max4)/2) && illiac10 > max4)
 {
     x4_10 = 245 ; 
     y4_10 = 372 ;
 }
 else if(illiac10 == max4)
 {
     x4_10 = 255 ; 
     y4_10 = 365 ;
     
 }
 else if(illiac10 < max4 && illiac10 > (max4 / 2))
 {
     x4_10 = 260 ; 
     y4_10 = 360 ;
 }
 else if(illiac10 <= (max4 / 2))
 {
     x4_10 = 368 ; 
     y4_10 = 355 ;
 } 
 
 
       
 // Plot tricepSF       
 var x1 ; var y1 ;
 if(tricepSF >= max1)
 {
     x1 = 50 ; 
     y1 = 150 ;
 }
 else if(tricepSF < max1 && tricepSF > ((max1 + max2)/2))
 {
     x1 = 60 ; 
     y1 = 153 ;
 }
 else if(tricepSF == ((max1 + max2)/2))
 {
     x1 = 85 ; 
     y1 = 170 ;
 }
 else if(tricepSF < ((max1 + max2)/2) && tricepSF > max2)
 {
     x1 = 110 ; 
     y1 = 185 ;
 }
 else if(tricepSF == max2)
 {
     x1 = 115 ; 
     y1 = 190 ;
 }
 else if(tricepSF < max2 && tricepSF > ((max2 + max3)/2))
 {
     x1 = 130 ; 
     y1 = 200 ;
 }
 else if(tricepSF == ((max2 + max3)/2))
 {
     x1 = 155 ; 
     y1 = 220 ;
 }
 else if(tricepSF < ((max2 + max3)/2) && tricepSF > max3)
 {
     x1 = 175 ; 
     y1 = 230 ;
 }
 else if(tricepSF == max3)
 {
     x1 = 185 ; 
     y1 = 235 ;
 }
 else if(tricepSF < max3 && tricepSF > ((max3 + max4)/2))
 {
     x1 = 200 ; 
     y1 = 245 ;
 }
 else if(tricepSF == ((max3 + max4)/2))
 {
     x1 = 225 ; 
     y1 = 260 ;
 }
 else if(tricepSF < ((max3 + max4)/2) && tricepSF > max4)
 {
     x1 = 245 ; 
     y1 = 273 ;
 }
 else if(tricepSF == max4)
 {
     x1 = 250 ; 
     y1 = 275 ;
     
 }
 else if(tricepSF < max4 && tricepSF > (max4 / 2))
 {
     x1 = 255 ; 
     y1 = 282 ;
 }
 else if(tricepSF <= (max4 / 2))
 {
     x1 = 265 ; 
     y1 = 285 ;
 }


 // Plot subscapularSF       
 var x2 ; var y2 ;
 if(subscapularSF >= max1)
 {
     x2 = 583 ; 
     y2 = 143 ;
 }
 else if(subscapularSF < max1 && subscapularSF > ((max1 + max2)/2))
 {
     x2 = 570 ; 
     y2 = 150 ;
 }
 else if(subscapularSF == ((max1 + max2)/2))
 {
     x2 = 555 ; 
     y2 = 160 ;
 }
 else if(subscapularSF < ((max1 + max2)/2) && subscapularSF > max2)
 {
     x2 = 530 ; 
     y2 = 178 ;
 }
 else if(subscapularSF == max2)
 {
     x2 = 520 ; 
     y2 = 186 ;
 }
 else if(subscapularSF < max2 && subscapularSF > ((max2 + max3)/2))
 {
     x2 = 508 ; 
     y2 = 193 ;
 }
 else if(subscapularSF == ((max2 + max3)/2))
 {
     x2 = 485 ; 
     y2 = 210 ;
 }
 else if(subscapularSF < ((max2 + max3)/2) && subscapularSF > max3)
 {
     x2 = 465 ; 
     y2 = 223 ;
 }
 else if(subscapularSF == max3)
 {
     x2 = 453 ; 
     y2 = 232 ;
 }
 else if(subscapularSF < max3 && subscapularSF > ((max3 + max4)/2))
 {
     x2 = 440 ; 
     y2 = 240 ;
 }
 else if(subscapularSF == ((max3 + max4)/2))
 {
     x2 = 415 ; 
     y2 = 255 ;
 }
 else if(subscapularSF < ((max3 + max4)/2) && subscapularSF > max4)
 {
     x2 = 395 ; 
     y2 = 270 ;
 }
 else if(subscapularSF == max4)
 {
     x2 = 385 ; 
     y2 = 278 ;
     
 }
 else if(subscapularSF < max4 && subscapularSF > (max4 / 2))
 {
     x2 = 380 ; 
     y2 = 280 ;
 }
 else if(subscapularSF <= (max4 / 2))
 {
     x2 = 375 ; 
     y2 = 285 ;
 }
 
 // Plot thighSF       
 var x3 ; var y3 ;
 if(thighSF >= max1)
 {
     x3 = 580 ; 
     y3 = 495 ;
 }
 else if(thighSF < max1 && thighSF > ((max1 + max2)/2))
 {
     x3 = 573 ; 
     y3 = 492 ;
 }
 else if(thighSF == ((max1 + max2)/2))
 {
     x3 = 555 ; 
     y3 = 478 ;
 }
 else if(thighSF < ((max1 + max2)/2) && thighSF > max2)
 {
     x3 = 530 ; 
     y3 = 465 ;
 }
 else if(thighSF == max2)
 {
     x3 = 515 ; 
     y3 = 453 ;
 }
 else if(thighSF < max2 && thighSF > ((max2 + max3)/2))
 {
     x3 = 505 ; 
     y3 = 446 ;
 }
 else if(thighSF == ((max2 + max3)/2))
 {
     x3 = 485 ; 
     y3 = 432 ;
 }
 else if(thighSF < ((max2 + max3)/2) && thighSF > max3)
 {
     x3 = 463 ; 
     y3 = 416 ;
 }
 else if(thighSF == max3)
 {
     x3 = 450 ; 
     y3 = 405 ;
 }
 else if(thighSF < max3 && thighSF > ((max3 + max4)/2))
 {
     x3 = 440 ; 
     y3 = 398 ;
 }
 else if(thighSF == ((max3 + max4)/2))
 {
     x3 = 420 ; 
     y3 = 383 ;
 }
 else if(thighSF < ((max3 + max4)/2) && thighSF > max4)
 {
     x3 = 395 ; 
     y3 = 370 ;
 }
 else if(thighSF == max4)
 {
     x3 = 385 ; 
     y3 = 363 ;
     
 }
 else if(thighSF < max4 && thighSF > (max4 / 2))
 {
     x3 = 380 ; 
     y3 = 360 ;
 }
 else if(thighSF <= (max4 / 2))
 {
     x3 = 370 ; 
     y3 = 355 ;
 }
 
 
 // Plot illiacSF       
 var x4 ; var y4 ;
 if(illiacSF >= max1)
 {
     x4 = 63 ; 
     y4 = 500 ;
 }
 else if(illiacSF < max1 && illiacSF > ((max1 + max2)/2))
 {
     x4 = 68 ; 
     y4 = 495 ;
 }
 else if(illiacSF == ((max1 + max2)/2))
 {
     x4 = 90 ; 
     y4 = 480 ;
 }
 else if(illiacSF < ((max1 + max2)/2) && illiacSF > max2)
 {
     x4 = 113 ; 
     y4 = 463 ;
 }
 else if(illiacSF == max2)
 {
     x4 = 128 ; 
     y4 = 453 ;
 }
 else if(illiacSF < max2 && illiacSF > ((max2 + max3)/2))
 {
     x4 = 133 ; 
     y4 = 450 ;
 }
 else if(illiacSF == ((max2 + max3)/2))
 {
     x4 = 155 ; 
     y4 = 435 ;
 }
 else if(illiacSF < ((max2 + max3)/2) && illiacSF > max3)
 {
     x4 = 180 ; 
     y4 = 418 ;
 }
 else if(illiacSF == max3)
 {
     x4 = 192 ; 
     y4 = 408 ;
 }
 else if(illiacSF < max3 && illiacSF > ((max3 + max4)/2))
 {
     x4 = 203 ; 
     y4 = 400 ;
 }
 else if(illiacSF == ((max3 + max4)/2))
 {
     x4 = 225 ; 
     y4 = 385 ;
 }
 else if(illiacSF < ((max3 + max4)/2) && illiacSF > max4)
 {
     x4 = 245 ; 
     y4 = 372 ;
 }
 else if(illiacSF == max4)
 {
     x4 = 255 ; 
     y4 = 365 ;
     
 }
 else if(illiacSF < max4 && illiacSF > (max4 / 2))
 {
     x4 = 260 ; 
     y4 = 360 ;
 }
 else if(illiacSF <= (max4 / 2))
 {
     x4 = 268 ; 
     y4 = 355 ;
 }


    $('#myCanvas4').drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_90, y1: y1_90,
      x2: x2_90, y2: y2_90,
      x3: x3_90, y3: y3_90,
      x4: x4_90, y4: y4_90,
      x5: x1_90, y5: y1_90
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_50, y1: y1_50,
      x2: x2_50, y2: y2_50,
      x3: x3_50, y3: y3_50,
      x4: x4_50, y4: y4_50,
      x5: x1_50, y5: y1_50
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_10, y1: y1_10,
      x2: x2_10, y2: y2_10,
      x3: x3_10, y3: y3_10,
      x4: x4_10, y4: y4_10,
      x5: x1_10, y5: y1_10
    }).drawLine({   
      strokeStyle: '#906bd0',   
      strokeWidth: 4,   
      x1: x1, y1: y1, 
      x2: x2, y2: y2,
      x3: x3, y3: y3,
      x4: x4, y4: y4,
      x5: x1, y5: y1
    }); 
}


function plot_6(maxSkinfold,tricep90,subscap90,bicep90,abdominal90,supras90,calf90,tricep50,subscap50,bicep50,abdominal50,supras50,calf50,tricep10,subscap10,bicep10,abdominal10,supras10,calf10,tricepSF,subscapularSF,bicepSF,abdominalSF,supraspinaleSF,calfSF){   
    
 var max1 = Math.round(maxSkinfold * 10) / 10 ;
 var max2 = Math.round((maxSkinfold * 0.75) * 10) / 10 ;
 var max3 = Math.round((maxSkinfold * 0.5) * 10) / 10 ;
 var max4 = Math.round((maxSkinfold * 0.25) * 10) / 10 ;

 // Plot tricep90       
 var x1_90 ; var y1_90 ;
 if(tricep90 >= max1)
 {
     x1_90 = 50 ; 
     y1_90 = 150 ;
 }
 else if(tricep90 < max1 && tricep90 > ((max1 + max2)/2))
 {
     x1_90 = 60 ; 
     y1_90 = 153 ;
 }
 else if(tricep90 == ((max1 + max2)/2))
 {
     x1_90 = 85 ; 
     y1_90 = 170 ;
 }
 else if(tricep90 < ((max1 + max2)/2) && tricep90 > max2)
 {
     x1_90 = 110 ; 
     y1_90 = 185 ;
 }
 else if(tricep90 == max2)
 {
     x1_90 = 115 ; 
     y1_90 = 190 ;
 }
 else if(tricep90 < max2 && tricep90 > ((max2 + max3)/2))
 {
     x1_90 = 130 ; 
     y1_90 = 200 ;
 }
 else if(tricep90 == ((max2 + max3)/2))
 {
     x1_90 = 155 ; 
     y1_90 = 220 ;
 }
 else if(tricep90 < ((max2 + max3)/2) && tricep90 > max3)
 {
     x1_90 = 175 ; 
     y1_90 = 230 ;
 }
 else if(tricep90 == max3)
 {
     x1_90 = 185 ; 
     y1_90 = 235 ;
 }
 else if(tricep90 < max3 && tricep90 > ((max3 + max4)/2))
 {
     x1_90 = 200 ; 
     y1_90 = 245 ;
 }
 else if(tricep90 == ((max3 + max4)/2))
 {
     x1_90 = 225 ; 
     y1_90 = 260 ;
 }
 else if(tricep90 < ((max3 + max4)/2) && tricep90 > max4)
 {
     x1_90 = 245 ; 
     y1_90 = 273 ;
 }
 else if(tricep90 == max4)
 {
     x1_90 = 250 ; 
     y1_90 = 275 ;
     
 }
 else if(tricep90 < max4 && tricep90 > (max4 / 2))
 {
     x1_90 = 255 ; 
     y1_90 = 282 ;
 }
 else if(tricep90 <= (max4 / 2))
 {
     x1_90 = 265 ; 
     y1_90 = 285 ;
 }


 // Plot subscap90       
 var x2_90 ; var y2_90 ;
 if(subscap90 >= max1)
 {
     x2_90 = 320 ; 
     y2_90 = 0 ;
 }
 else if(subscap90 < max1 && subscap90 > ((max1 + max2)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 15 ;
 }
 else if(subscap90 == ((max1 + max2)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 35 ;
 }
 else if(subscap90 < ((max1 + max2)/2) && subscap90 > max2)
 {
     x2_90 = 320 ; 
     y2_90 = 60 ;
 }
 else if(subscap90 == max2)
 {
     x2_90 = 320 ; 
     y2_90 = 80 ;
 }
 else if(subscap90 < max2 && subscap90 > ((max2 + max3)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 95 ;
 }
 else if(subscap90 == ((max2 + max3)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 120 ;
 }
 else if(subscap90 < ((max2 + max3)/2) && subscap90 > max3)
 {
     x2_90 = 320 ; 
     y2_90 = 140 ;
 }
 else if(subscap90 == max3)
 {
     x2_90 = 320 ; 
     y2_90 = 160 ;
 }
 else if(subscap90 < max3 && subscap90 > ((max3 + max4)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 175 ;
 }
 else if(subscap90 == ((max3 + max4)/2))
 {
     x2_90 = 320 ; 
     y2_90 = 205 ;
 }
 else if(subscap90 < ((max3 + max4)/2) && subscap90 > max4)
 {
     x2_90 = 320 ; 
     y2_90 = 225 ;
 }
 else if(subscap90 == max4)
 {
     x2_90 = 320 ; 
     y2_90 = 240 ;
     
 }
 else if(subscap90 < max4 && subscap90 > (max4 / 2))
 {
     x2_90 = 320 ; 
     y2_90 = 250 ;
 }
 else if(subscap90 <= (max4 / 2))
 {
     x2_90 = 320 ; 
     y2_90 = 265 ;
 }
 
 
 // Plot bicep90       
 var x3_90 ; var y3_90 ;
 if(bicep90 >= max1)
 {
     x3_90 = 583 ; 
     y3_90 = 143 ;
 }
 else if(bicep90 < max1 && bicep90 > ((max1 + max2)/2))
 {
     x3_90 = 570 ; 
     y3_90 = 150 ;
 }
 else if(bicep90 == ((max1 + max2)/2))
 {
     x3_90 = 555 ; 
     y3_90 = 160 ;
 }
 else if(bicep90 < ((max1 + max2)/2) && bicep90 > max2)
 {
     x3_90 = 530 ; 
     y3_90 = 178 ;
 }
 else if(bicep90 == max2)
 {
     x3_90 = 520 ; 
     y3_90 = 186 ;
 }
 else if(bicep90 < max2 && bicep90 > ((max2 + max3)/2))
 {
     x3_90 = 508 ; 
     y3_90 = 193 ;
 }
 else if(bicep90 == ((max2 + max3)/2))
 {
     x3_90 = 485 ; 
     y3_90 = 210 ;
 }
 else if(bicep90 < ((max2 + max3)/2) && bicep90 > max3)
 {
     x3_90 = 465 ; 
     y3_90 = 223 ;
 }
 else if(bicep90 == max3)
 {
     x3_90 = 453 ; 
     y3_90 = 232 ;
 }
 else if(bicep90 < max3 && bicep90 > ((max3 + max4)/2))
 {
     x3_90 = 440 ; 
     y3_90 = 240 ;
 }
 else if(bicep90 == ((max3 + max4)/2))
 {
     x3_90 = 415 ; 
     y3_90 = 255 ;
 }
 else if(bicep90 < ((max3 + max4)/2) && bicep90 > max4)
 {
     x3_90 = 395 ; 
     y3_90 = 270 ;
 }
 else if(bicep90 == max4)
 {
     x3_90 = 385 ; 
     y3_90 = 278 ;
     
 }
 else if(bicep90 < max4 && bicep90 > (max4 / 2))
 {
     x3_90 = 380 ; 
     y3_90 = 280 ;
 }
 else if(bicep90 <= (max4 / 2))
 {
     x3_90 = 375 ; 
     y3_90 = 285 ;
 }
 
 // Plot abdominal90      
 var x4_90 ; var y4_90 ;
 if(abdominal90 >= max1)
 {
     x4_90 = 580 ; 
     y4_90 = 495 ;
 }
 else if(abdominal90 < max1 && abdominal90 > ((max1 + max2)/2))
 {
     x4_90 = 573 ; 
     y4_90 = 492 ;
 }
 else if(abdominal90 == ((max1 + max2)/2))
 {
     x4_90 = 555 ; 
     y4_90 = 478 ;
 }
 else if(abdominal90 < ((max1 + max2)/2) && abdominal90 > max2)
 {
     x4_90 = 530 ; 
     y4_90 = 465 ;
 }
 else if(abdominal90 == max2)
 {
     x4_90 = 515 ; 
     y4_90 = 453 ;
 }
 else if(abdominal90 < max2 && abdominal90 > ((max2 + max3)/2))
 {
     x4_90 = 505 ; 
     y4_90 = 446 ;
 }
 else if(abdominal90 == ((max2 + max3)/2))
 {
     x4_90 = 485 ; 
     y4_90 = 432 ;
 }
 else if(abdominal90 < ((max2 + max3)/2) && abdominal90 > max3)
 {
     x4_90 = 463 ; 
     y4_90 = 416 ;
 }
 else if(abdominal90 == max3)
 {
     x4_90 = 450 ; 
     y4_90 = 405 ;
 }
 else if(abdominal90 < max3 && abdominal90 > ((max3 + max4)/2))
 {
     x4_90 = 440 ; 
     y4_90 = 398 ;
 }
 else if(abdominal90 == ((max3 + max4)/2))
 {
     x4_90 = 420 ; 
     y4_90 = 383 ;
 }
 else if(abdominal90 < ((max3 + max4)/2) && abdominal90 > max4)
 {
     x4_90 = 395 ; 
     y4_90 = 370 ;
 }
 else if(abdominal90 == max4)
 {
     x4_90 = 385 ; 
     y4_90 = 363 ;
     
 }
 else if(abdominal90 < max4 && abdominal90 > (max4 / 2))
 {
     x4_90 = 380 ; 
     y4_90 = 360 ;
 }
 else if(abdominal90 <= (max4 / 2))
 {
     x4_90 = 370 ; 
     y4_90 = 355 ;
 }
 
 
// Plot supras90      
 var x5_90 ; var y5_90 ;
 if(supras90 >= max1)
 {
     x5_90 = 320 ; 
     y5_90 = 640 ;
 }
 else if(supras90 < max1 && supras90 > ((max1 + max2)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 630 ;
 }
 else if(supras90 == ((max1 + max2)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 610 ;
 }
 else if(supras90 < ((max1 + max2)/2) && supras90 > max2)
 {
     x5_90 = 320 ; 
     y5_90 = 570 ;
 }
 else if(supras90 == max2)
 {
     x5_90 = 320 ; 
     y5_90 = 560 ;
 }
 else if(supras90 < max2 && supras90 > ((max2 + max3)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 550 ;
 }
 else if(supras90 == ((max2 + max3)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 520 ;
 }
 else if(supras90 < ((max2 + max3)/2) && supras90 > max3)
 {
     x5_90 = 320 ; 
     y5_90 = 490 ;
 }
 else if(supras90 == max3)
 {
     x5_90 = 320 ; 
     y5_90 = 480 ;
 }
 else if(supras90 < max3 && supras90 > ((max3 + max4)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 465 ;
 }
 else if(supras90 == ((max3 + max4)/2))
 {
     x5_90 = 320 ; 
     y5_90 = 440 ;
 }
 else if(supras90 < ((max3 + max4)/2) && supras90 > max4)
 {
     x5_90 = 320 ; 
     y5_90 = 410 ;
 }
 else if(supras90 == max4)
 {
     x5_90 = 320 ; 
     y5_90 = 400 ;
     
 }
 else if(supras90 < max4 && supras90 > (max4 / 2))
 {
     x5_90 = 320 ; 
     y5_90 = 390 ;
 }
 else if(supras90 <= (max4 / 2))
 {
     x5_90 = 320 ; 
     y5_90 = 375 ;
 } 
 
 
 // Plot calf90      
 var x6_90 ; var y6_90 ;
 if(calf90 >= max1)
 {
     x6_90 = 63 ; 
     y6_90 = 500 ;
 }
 else if(calf90 < max1 && calf90 > ((max1 + max2)/2))
 {
     x6_90 = 68 ; 
     y6_90 = 495 ;
 }
 else if(calf90 == ((max1 + max2)/2))
 {
     x6_90 = 90 ; 
     y6_90 = 480 ;
 }
 else if(calf90 < ((max1 + max2)/2) && calf90 > max2)
 {
     x6_90 = 113 ; 
     y6_90 = 463 ;
 }
 else if(calf90 == max2)
 {
     x6_90 = 128 ; 
     y6_90 = 453 ;
 }
 else if(calf90 < max2 && calf90 > ((max2 + max3)/2))
 {
     x6_90 = 133 ; 
     y6_90 = 450 ;
 }
 else if(calf90 == ((max2 + max3)/2))
 {
     x6_90 = 155 ; 
     y6_90 = 435 ;
 }
 else if(calf90 < ((max2 + max3)/2) && calf90 > max3)
 {
     x6_90 = 180 ; 
     y6_90 = 418 ;
 }
 else if(calf90 == max3)
 {
     x6_90 = 192 ; 
     y6_90 = 408 ;
 }
 else if(calf90 < max3 && calf90 > ((max3 + max4)/2))
 {
     x6_90 = 203 ; 
     y6_90 = 400 ;
 }
 else if(calf90 == ((max3 + max4)/2))
 {
     x6_90 = 225 ; 
     y6_90 = 385 ;
 }
 else if(calf90 < ((max3 + max4)/2) && calf90 > max4)
 {
     x6_90 = 245 ; 
     y6_90 = 372 ;
 }
 else if(calf90 == max4)
 {
     x6_90 = 255 ; 
     y6_90 = 365 ;
     
 }
 else if(calf90 < max4 && calf90 > (max4 / 2))
 {
     x6_90 = 260 ; 
     y6_90 = 360 ;
 }
 else if(calf90 <= (max4 / 2))
 {
     x6_90 = 368 ; 
     y6_90 = 355 ;
 }
 
 

 // Plot tricep50       
 var x1_50 ; var y1_50 ;
 if(tricep50 >= max1)
 {
     x1_50 = 50 ; 
     y1_50 = 150 ;
 }
 else if(tricep50 < max1 && tricep50 > ((max1 + max2)/2))
 {
     x1_50 = 60 ; 
     y1_50 = 153 ;
 }
 else if(tricep50 == ((max1 + max2)/2))
 {
     x1_50 = 85 ; 
     y1_50 = 170 ;
 }
 else if(tricep50 < ((max1 + max2)/2) && tricep50 > max2)
 {
     x1_50 = 110 ; 
     y1_50 = 185 ;
 }
 else if(tricep50 == max2)
 {
     x1_50 = 115 ; 
     y1_50 = 190 ;
 }
 else if(tricep50 < max2 && tricep50 > ((max2 + max3)/2))
 {
     x1_50 = 130 ; 
     y1_50 = 200 ;
 }
 else if(tricep50 == ((max2 + max3)/2))
 {
     x1_50 = 155 ; 
     y1_50 = 220 ;
 }
 else if(tricep50 < ((max2 + max3)/2) && tricep50 > max3)
 {
     x1_50 = 175 ; 
     y1_50 = 230 ;
 }
 else if(tricep50 == max3)
 {
     x1_50 = 185 ; 
     y1_50 = 235 ;
 }
 else if(tricep50 < max3 && tricep50 > ((max3 + max4)/2))
 {
     x1_50 = 200 ; 
     y1_50 = 245 ;
 }
 else if(tricep50 == ((max3 + max4)/2))
 {
     x1_50 = 225 ; 
     y1_50 = 260 ;
 }
 else if(tricep50 < ((max3 + max4)/2) && tricep50 > max4)
 {
     x1_50 = 245 ; 
     y1_50 = 273 ;
 }
 else if(tricep50 == max4)
 {
     x1_50 = 250 ; 
     y1_50 = 275 ;
     
 }
 else if(tricep50 < max4 && tricep50 > (max4 / 2))
 {
     x1_50 = 255 ; 
     y1_50 = 282 ;
 }
 else if(tricep50 <= (max4 / 2))
 {
     x1_50 = 265 ; 
     y1_50 = 285 ;
 }


 // Plot subscap50       
 var x2_50 ; var y2_50 ;
 if(subscap50 >= max1)
 {
     x2_50 = 320 ; 
     y2_50 = 0 ;
 }
 else if(subscap50 < max1 && subscap50 > ((max1 + max2)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 15 ;
 }
 else if(subscap50 == ((max1 + max2)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 35 ;
 }
 else if(subscap50 < ((max1 + max2)/2) && subscap50 > max2)
 {
     x2_50 = 320 ; 
     y2_50 = 60 ;
 }
 else if(subscap50 == max2)
 {
     x2_50 = 320 ; 
     y2_50 = 80 ;
 }
 else if(subscap50 < max2 && subscap50 > ((max2 + max3)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 95 ;
 }
 else if(subscap50 == ((max2 + max3)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 120 ;
 }
 else if(subscap50 < ((max2 + max3)/2) && subscap50 > max3)
 {
     x2_50 = 320 ; 
     y2_50 = 140 ;
 }
 else if(subscap50 == max3)
 {
     x2_50 = 320 ; 
     y2_50 = 160 ;
 }
 else if(subscap50 < max3 && subscap50 > ((max3 + max4)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 175 ;
 }
 else if(subscap50 == ((max3 + max4)/2))
 {
     x2_50 = 320 ; 
     y2_50 = 205 ;
 }
 else if(subscap50 < ((max3 + max4)/2) && subscap50 > max4)
 {
     x2_50 = 320 ; 
     y2_50 = 225 ;
 }
 else if(subscap50 == max4)
 {
     x2_50 = 320 ; 
     y2_50 = 240 ;
     
 }
 else if(subscap50 < max4 && subscap50 > (max4 / 2))
 {
     x2_50 = 320 ; 
     y2_50 = 250 ;
 }
 else if(subscap50 <= (max4 / 2))
 {
     x2_50 = 320 ; 
     y2_50 = 265 ;
 }
 
 
 // Plot bicep50       
 var x3_50 ; var y3_50 ;
 if(bicep50 >= max1)
 {
     x3_50 = 583 ; 
     y3_50 = 143 ;
 }
 else if(bicep50 < max1 && bicep50 > ((max1 + max2)/2))
 {
     x3_50 = 570 ; 
     y3_50 = 150 ;
 }
 else if(bicep50 == ((max1 + max2)/2))
 {
     x3_50 = 555 ; 
     y3_50 = 160 ;
 }
 else if(bicep50 < ((max1 + max2)/2) && bicep50 > max2)
 {
     x3_50 = 530 ; 
     y3_50 = 178 ;
 }
 else if(bicep50 == max2)
 {
     x3_50 = 520 ; 
     y3_50 = 186 ;
 }
 else if(bicep50 < max2 && bicep50 > ((max2 + max3)/2))
 {
     x3_50 = 508 ; 
     y3_50 = 193 ;
 }
 else if(bicep50 == ((max2 + max3)/2))
 {
     x3_50 = 485 ; 
     y3_50 = 210 ;
 }
 else if(bicep50 < ((max2 + max3)/2) && bicep50 > max3)
 {
     x3_50 = 465 ; 
     y3_50 = 223 ;
 }
 else if(bicep50 == max3)
 {
     x3_50 = 453 ; 
     y3_50 = 232 ;
 }
 else if(bicep50 < max3 && bicep50 > ((max3 + max4)/2))
 {
     x3_50 = 440 ; 
     y3_50 = 240 ;
 }
 else if(bicep50 == ((max3 + max4)/2))
 {
     x3_50 = 415 ; 
     y3_50 = 255 ;
 }
 else if(bicep50 < ((max3 + max4)/2) && bicep50 > max4)
 {
     x3_50 = 395 ; 
     y3_50 = 270 ;
 }
 else if(bicep50 == max4)
 {
     x3_50 = 385 ; 
     y3_50 = 278 ;
     
 }
 else if(bicep50 < max4 && bicep50 > (max4 / 2))
 {
     x3_50 = 380 ; 
     y3_50 = 280 ;
 }
 else if(bicep50 <= (max4 / 2))
 {
     x3_50 = 375 ; 
     y3_50 = 285 ;
 }
 
 // Plot abdominal50      
 var x4_50 ; var y4_50 ;
 if(abdominal50 >= max1)
 {
     x4_50 = 580 ; 
     y4_50 = 495 ;
 }
 else if(abdominal50 < max1 && abdominal50 > ((max1 + max2)/2))
 {
     x4_50 = 573 ; 
     y4_50 = 492 ;
 }
 else if(abdominal50 == ((max1 + max2)/2))
 {
     x4_50 = 555 ; 
     y4_50 = 478 ;
 }
 else if(abdominal50 < ((max1 + max2)/2) && abdominal50 > max2)
 {
     x4_50 = 530 ; 
     y4_50 = 465 ;
 }
 else if(abdominal50 == max2)
 {
     x4_50 = 515 ; 
     y4_50 = 453 ;
 }
 else if(abdominal50 < max2 && abdominal50 > ((max2 + max3)/2))
 {
     x4_50 = 505 ; 
     y4_50 = 446 ;
 }
 else if(abdominal50 == ((max2 + max3)/2))
 {
     x4_50 = 485 ; 
     y4_50 = 432 ;
 }
 else if(abdominal50 < ((max2 + max3)/2) && abdominal50 > max3)
 {
     x4_50 = 463 ; 
     y4_50 = 416 ;
 }
 else if(abdominal50 == max3)
 {
     x4_50 = 450 ; 
     y4_50 = 405 ;
 }
 else if(abdominal50 < max3 && abdominal50 > ((max3 + max4)/2))
 {
     x4_50 = 440 ; 
     y4_50 = 398 ;
 }
 else if(abdominal50 == ((max3 + max4)/2))
 {
     x4_50 = 420 ; 
     y4_50 = 383 ;
 }
 else if(abdominal50 < ((max3 + max4)/2) && abdominal50 > max4)
 {
     x4_50 = 395 ; 
     y4_50 = 370 ;
 }
 else if(abdominal50 == max4)
 {
     x4_50 = 385 ; 
     y4_50 = 363 ;
     
 }
 else if(abdominal50 < max4 && abdominal50 > (max4 / 2))
 {
     x4_50 = 380 ; 
     y4_50 = 360 ;
 }
 else if(abdominal50 <= (max4 / 2))
 {
     x4_50 = 370 ; 
     y4_50 = 355 ;
 }
 
 
// Plot supras50      
 var x5_50 ; var y5_50 ;
 if(supras50 >= max1)
 {
     x5_50 = 320 ; 
     y5_50 = 640 ;
 }
 else if(supras50 < max1 && supras50 > ((max1 + max2)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 630 ;
 }
 else if(supras50 == ((max1 + max2)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 610 ;
 }
 else if(supras50 < ((max1 + max2)/2) && supras50 > max2)
 {
     x5_50 = 320 ; 
     y5_50 = 570 ;
 }
 else if(supras50 == max2)
 {
     x5_50 = 320 ; 
     y5_50 = 560 ;
 }
 else if(supras50 < max2 && supras50 > ((max2 + max3)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 550 ;
 }
 else if(supras50 == ((max2 + max3)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 520 ;
 }
 else if(supras50 < ((max2 + max3)/2) && supras50 > max3)
 {
     x5_50 = 320 ; 
     y5_50 = 490 ;
 }
 else if(supras50 == max3)
 {
     x5_50 = 320 ; 
     y5_50 = 480 ;
 }
 else if(supras50 < max3 && supras50 > ((max3 + max4)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 465 ;
 }
 else if(supras50 == ((max3 + max4)/2))
 {
     x5_50 = 320 ; 
     y5_50 = 440 ;
 }
 else if(supras50 < ((max3 + max4)/2) && supras50 > max4)
 {
     x5_50 = 320 ; 
     y5_50 = 410 ;
 }
 else if(supras50 == max4)
 {
     x5_50 = 320 ; 
     y5_50 = 400 ;
     
 }
 else if(supras50 < max4 && supras50 > (max4 / 2))
 {
     x5_50 = 320 ; 
     y5_50 = 390 ;
 }
 else if(supras50 <= (max4 / 2))
 {
     x5_50 = 320 ; 
     y5_50 = 375 ;
 } 
 
 
 // Plot calf50      
 var x6_50 ; var y6_50 ;
 if(calf50 >= max1)
 {
     x6_50 = 63 ; 
     y6_50 = 500 ;
 }
 else if(calf50 < max1 && calf50 > ((max1 + max2)/2))
 {
     x6_50 = 68 ; 
     y6_50 = 495 ;
 }
 else if(calf50 == ((max1 + max2)/2))
 {
     x6_50 = 90 ; 
     y6_50 = 480 ;
 }
 else if(calf50 < ((max1 + max2)/2) && calf50 > max2)
 {
     x6_50 = 113 ; 
     y6_50 = 463 ;
 }
 else if(calf50 == max2)
 {
     x6_50 = 128 ; 
     y6_50 = 453 ;
 }
 else if(calf50 < max2 && calf50 > ((max2 + max3)/2))
 {
     x6_50 = 133 ; 
     y6_50 = 450 ;
 }
 else if(calf50 == ((max2 + max3)/2))
 {
     x6_50 = 155 ; 
     y6_50 = 435 ;
 }
 else if(calf50 < ((max2 + max3)/2) && calf50 > max3)
 {
     x6_50 = 180 ; 
     y6_50 = 418 ;
 }
 else if(calf50 == max3)
 {
     x6_50 = 192 ; 
     y6_50 = 408 ;
 }
 else if(calf50 < max3 && calf50 > ((max3 + max4)/2))
 {
     x6_50 = 203 ; 
     y6_50 = 400 ;
 }
 else if(calf50 == ((max3 + max4)/2))
 {
     x6_50 = 225 ; 
     y6_50 = 385 ;
 }
 else if(calf50 < ((max3 + max4)/2) && calf50 > max4)
 {
     x6_50 = 245 ; 
     y6_50 = 372 ;
 }
 else if(calf50 == max4)
 {
     x6_50 = 255 ; 
     y6_50 = 365 ;
     
 }
 else if(calf50 < max4 && calf50 > (max4 / 2))
 {
     x6_50 = 260 ; 
     y6_50 = 360 ;
 }
 else if(calf50 <= (max4 / 2))
 {
     x6_50 = 368 ; 
     y6_50 = 355 ;
 }
 
 
 // Plot tricep10       
 var x1_10 ; var y1_10 ;
 if(tricep10 >= max1)
 {
     x1_10 = 50 ; 
     y1_10 = 150 ;
 }
 else if(tricep10 < max1 && tricep10 > ((max1 + max2)/2))
 {
     x1_10 = 60 ; 
     y1_10 = 153 ;
 }
 else if(tricep10 == ((max1 + max2)/2))
 {
     x1_10 = 85 ; 
     y1_10 = 170 ;
 }
 else if(tricep10 < ((max1 + max2)/2) && tricep10 > max2)
 {
     x1_10 = 110 ; 
     y1_10 = 185 ;
 }
 else if(tricep10 == max2)
 {
     x1_10 = 115 ; 
     y1_10 = 190 ;
 }
 else if(tricep10 < max2 && tricep10 > ((max2 + max3)/2))
 {
     x1_10 = 130 ; 
     y1_10 = 200 ;
 }
 else if(tricep10 == ((max2 + max3)/2))
 {
     x1_10 = 155 ; 
     y1_10 = 220 ;
 }
 else if(tricep10 < ((max2 + max3)/2) && tricep10 > max3)
 {
     x1_10 = 175 ; 
     y1_10 = 230 ;
 }
 else if(tricep10 == max3)
 {
     x1_10 = 185 ; 
     y1_10 = 235 ;
 }
 else if(tricep10 < max3 && tricep10 > ((max3 + max4)/2))
 {
     x1_10 = 200 ; 
     y1_10 = 245 ;
 }
 else if(tricep10 == ((max3 + max4)/2))
 {
     x1_10 = 225 ; 
     y1_10 = 260 ;
 }
 else if(tricep10 < ((max3 + max4)/2) && tricep10 > max4)
 {
     x1_10 = 245 ; 
     y1_10 = 273 ;
 }
 else if(tricep10 == max4)
 {
     x1_10 = 250 ; 
     y1_10 = 275 ;
     
 }
 else if(tricep10 < max4 && tricep10 > (max4 / 2))
 {
     x1_10 = 255 ; 
     y1_10 = 282 ;
 }
 else if(tricep10 <= (max4 / 2))
 {
     x1_10 = 265 ; 
     y1_10 = 285 ;
 }


  // Plot subscap10       
 var x2_10 ; var y2_10 ;
 if(subscap10 >= max1)
 {
     x2_10 = 320 ; 
     y2_10 = 0 ;
 }
 else if(subscap10 < max1 && subscap10 > ((max1 + max2)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 15 ;
 }
 else if(subscap10 == ((max1 + max2)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 35 ;
 }
 else if(subscap10 < ((max1 + max2)/2) && subscap10 > max2)
 {
     x2_10 = 320 ; 
     y2_10 = 60 ;
 }     
 else if(subscap10 == max2)
 {
     x2_10 = 320 ; 
     y2_10 = 80 ;
 }
 else if(subscap10 < max2 && subscap10 > ((max2 + max3)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 95 ;
 }
 else if(subscap10 == ((max2 + max3)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 120 ;
 }
 else if(subscap10 < ((max2 + max3)/2) && subscap10 > max3)
 {
     x2_10 = 320 ; 
     y2_10 = 140 ;
 }
 else if(subscap10 == max3)
 {
     x2_10 = 320 ; 
     y2_10 = 160 ;
 }
 else if(subscap10 < max3 && subscap10 > ((max3 + max4)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 175 ;
 }
 else if(subscap10 == ((max3 + max4)/2))
 {
     x2_10 = 320 ; 
     y2_10 = 205 ;
 }
 else if(subscap10 < ((max3 + max4)/2) && subscap10 > max4)
 {
     x2_10 = 320 ; 
     y2_10 = 225 ;
 }
 else if(subscap10 == max4)
 {
     x2_10 = 320 ; 
     y2_10 = 240 ;
     
 }
 else if(subscap10 < max4 && subscap10 > (max4 / 2))
 {
     x2_10 = 320 ; 
     y2_10 = 250 ;
 }
 else if(subscap10 <= (max4 / 2))
 {
     x2_10 = 320 ; 
     y2_10 = 265 ;
 }
 
 
 // Plot bicep10       
 var x3_10 ; var y3_10 ;
 if(bicep10 >= max1)
 {
     x3_10 = 583 ; 
     y3_10 = 143 ;
 }
 else if(bicep10 < max1 && bicep10 > ((max1 + max2)/2))
 {
     x3_10 = 570 ; 
     y3_10 = 150 ;
 }
 else if(bicep10 == ((max1 + max2)/2))
 {
     x3_10 = 555 ; 
     y3_10 = 160 ;
 }
 else if(bicep10 < ((max1 + max2)/2) && bicep10 > max2)
 {
     x3_10 = 530 ; 
     y3_10 = 178 ;
 }
 else if(bicep10 == max2)
 {
     x3_10 = 520 ; 
     y3_10 = 186 ;
 }
 else if(bicep10 < max2 && bicep10 > ((max2 + max3)/2))
 {
     x3_10 = 508 ; 
     y3_10 = 193 ;
 }
 else if(bicep10 == ((max2 + max3)/2))
 {
     x3_10 = 485 ; 
     y3_10 = 210 ;
 }
 else if(bicep10 < ((max2 + max3)/2) && bicep10 > max3)
 {
     x3_10 = 465 ; 
     y3_10 = 223 ;
 }
 else if(bicep10 == max3)
 {
     x3_10 = 453 ; 
     y3_10 = 232 ;
 }
 else if(bicep10 < max3 && bicep10 > ((max3 + max4)/2))
 {
     x3_10 = 440 ; 
     y3_10 = 240 ;
 }
 else if(bicep10 == ((max3 + max4)/2))
 {
     x3_10 = 415 ; 
     y3_10 = 255 ;
 }
 else if(bicep10 < ((max3 + max4)/2) && bicep10 > max4)
 {
     x3_10 = 395 ; 
     y3_10 = 270 ;
 }
 else if(bicep10 == max4)
 {
     x3_10 = 385 ; 
     y3_10 = 278 ;
     
 }
 else if(bicep10 < max4 && bicep10 > (max4 / 2))
 {
     x3_10 = 380 ; 
     y3_10 = 280 ;
 }
 else if(bicep10 <= (max4 / 2))
 {
     x3_10 = 375 ; 
     y3_10 = 285 ;
 }
 
 // Plot abdominal10      
 var x4_10 ; var y4_10 ;
 if(abdominal10 >= max1)
 {
     x4_10 = 580 ; 
     y4_10 = 495 ;
 }
 else if(abdominal10 < max1 && abdominal10 > ((max1 + max2)/2))
 {
     x4_10 = 573 ; 
     y4_10 = 492 ;
 }
 else if(abdominal10 == ((max1 + max2)/2))
 {
     x4_10 = 555 ; 
     y4_10 = 478 ;
 }
 else if(abdominal10 < ((max1 + max2)/2) && abdominal10 > max2)
 {
     x4_10 = 530 ; 
     y4_10 = 465 ;
 }
 else if(abdominal10 == max2)
 {
     x4_10 = 515 ; 
     y4_10 = 453 ;
 }
 else if(abdominal10 < max2 && abdominal10 > ((max2 + max3)/2))
 {
     x4_10 = 505 ; 
     y4_10 = 446 ;
 }
 else if(abdominal10 == ((max2 + max3)/2))
 {
     x4_10 = 485 ; 
     y4_10 = 432 ;
 }
 else if(abdominal10 < ((max2 + max3)/2) && abdominal10 > max3)
 {
     x4_10 = 463 ; 
     y4_10 = 416 ;
 }
 else if(abdominal10 == max3)
 {
     x4_10 = 450 ; 
     y4_10 = 405 ;
 }
 else if(abdominal10 < max3 && abdominal10 > ((max3 + max4)/2))
 {
     x4_10 = 440 ; 
     y4_10 = 398 ;
 }
 else if(abdominal10 == ((max3 + max4)/2))
 {
     x4_10 = 420 ; 
     y4_10 = 383 ;
 }
 else if(abdominal10 < ((max3 + max4)/2) && abdominal10 > max4)
 {
     x4_10 = 395 ; 
     y4_10 = 370 ;
 }
 else if(abdominal10 == max4)
 {
     x4_10 = 385 ; 
     y4_10 = 363 ;
     
 }
 else if(abdominal10 < max4 && abdominal10 > (max4 / 2))
 {
     x4_10 = 380 ; 
     y4_10 = 360 ;
 }
 else if(abdominal10 <= (max4 / 2))
 {
     x4_10 = 370 ; 
     y4_10 = 355 ;
 }
 
 
// Plot supras10      
 var x5_10 ; var y5_10 ;
 if(supras10 >= max1)
 {
     x5_10 = 320 ; 
     y5_10 = 640 ;
 }
 else if(supras10 < max1 && supras10 > ((max1 + max2)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 630 ;
 }
 else if(supras10 == ((max1 + max2)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 610 ;
 }
 else if(supras10 < ((max1 + max2)/2) && supras10 > max2)
 {
     x5_10 = 320 ; 
     y5_10 = 570 ;
 }
 else if(supras10 == max2)
 {
     x5_10 = 320 ; 
     y5_10 = 560 ;
 }
 else if(supras10 < max2 && supras10 > ((max2 + max3)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 550 ;
 }
 else if(supras10 == ((max2 + max3)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 520 ;
 }
 else if(supras10 < ((max2 + max3)/2) && supras10 > max3)
 {
     x5_10 = 320 ; 
     y5_10 = 490 ;
 }
 else if(supras10 == max3)
 {
     x5_10 = 320 ; 
     y5_10 = 480 ;
 }
 else if(supras10 < max3 && supras10 > ((max3 + max4)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 465 ;
 }
 else if(supras10 == ((max3 + max4)/2))
 {
     x5_10 = 320 ; 
     y5_10 = 440 ;
 }
 else if(supras10 < ((max3 + max4)/2) && supras10 > max4)
 {
     x5_10 = 320 ; 
     y5_10 = 410 ;
 }
 else if(supras10 == max4)
 {
     x5_10 = 320 ; 
     y5_10 = 400 ;
     
 }
 else if(supras10 < max4 && supras10 > (max4 / 2))
 {
     x5_10 = 320 ; 
     y5_10 = 390 ;
 }
 else if(supras10 <= (max4 / 2))
 {
     x5_10 = 320 ; 
     y5_10 = 375 ;
 } 
 
 
 // Plot calf10      
 var x6_10 ; var y6_10 ;
 if(calf10 >= max1)
 {
     x6_10 = 63 ; 
     y6_10 = 500 ;
 }
 else if(calf10 < max1 && calf10 > ((max1 + max2)/2))
 {
     x6_10 = 68 ; 
     y6_10 = 495 ;
 }
 else if(calf10 == ((max1 + max2)/2))
 {
     x6_10 = 90 ; 
     y6_10 = 480 ;
 }
 else if(calf10 < ((max1 + max2)/2) && calf10 > max2)
 {
     x6_10 = 113 ; 
     y6_10 = 463 ;
 }
 else if(calf10 == max2)
 {
     x6_10 = 128 ; 
     y6_10 = 453 ;
 }
 else if(calf10 < max2 && calf10 > ((max2 + max3)/2))
 {
     x6_10 = 133 ; 
     y6_10 = 450 ;
 }
 else if(calf10 == ((max2 + max3)/2))
 {
     x6_10 = 155 ; 
     y6_10 = 435 ;
 }
 else if(calf10 < ((max2 + max3)/2) && calf10 > max3)
 {
     x6_10 = 180 ; 
     y6_10 = 418 ;
 }
 else if(calf10 == max3)
 {
     x6_10 = 192 ; 
     y6_10 = 408 ;
 }
 else if(calf10 < max3 && calf10 > ((max3 + max4)/2))
 {
     x6_10 = 203 ; 
     y6_10 = 400 ;
 }
 else if(calf10 == ((max3 + max4)/2))
 {
     x6_10 = 225 ; 
     y6_10 = 385 ;
 }
 else if(calf10 < ((max3 + max4)/2) && calf10 > max4)
 {
     x6_10 = 245 ; 
     y6_10 = 372 ;
 }
 else if(calf10 == max4)
 {
     x6_10 = 255 ; 
     y6_10 = 365 ;
     
 }
 else if(calf10 < max4 && calf10 > (max4 / 2))
 {
     x6_10 = 260 ; 
     y6_10 = 360 ;
 }
 else if(calf10 <= (max4 / 2))
 {
     x6_10 = 368 ; 
     y6_10 = 355 ;
 } 
 
 
       
 // Plot tricepSF       
 var x1 ; var y1 ;
 if(tricepSF >= max1)
 {
     x1 = 50 ; 
     y1 = 150 ;
 }
 else if(tricepSF < max1 && tricepSF > ((max1 + max2)/2))
 {
     x1 = 60 ; 
     y1 = 153 ;
 }
 else if(tricepSF == ((max1 + max2)/2))
 {
     x1 = 85 ; 
     y1 = 170 ;
 }
 else if(tricepSF < ((max1 + max2)/2) && tricepSF > max2)
 {
     x1 = 110 ; 
     y1 = 185 ;
 }
 else if(tricepSF == max2)
 {
     x1 = 115 ; 
     y1 = 190 ;
 }
 else if(tricepSF < max2 && tricepSF > ((max2 + max3)/2))
 {
     x1 = 130 ; 
     y1 = 200 ;
 }
 else if(tricepSF == ((max2 + max3)/2))
 {
     x1 = 155 ; 
     y1 = 220 ;
 }
 else if(tricepSF < ((max2 + max3)/2) && tricepSF > max3)
 {
     x1 = 175 ; 
     y1 = 230 ;
 }
 else if(tricepSF == max3)
 {
     x1 = 185 ; 
     y1 = 235 ;
 }
 else if(tricepSF < max3 && tricepSF > ((max3 + max4)/2))
 {
     x1 = 200 ; 
     y1 = 245 ;
 }
 else if(tricepSF == ((max3 + max4)/2))
 {
     x1 = 225 ; 
     y1 = 260 ;
 }
 else if(tricepSF < ((max3 + max4)/2) && tricepSF > max4)
 {
     x1 = 245 ; 
     y1 = 273 ;
 }
 else if(tricepSF == max4)
 {
     x1 = 250 ; 
     y1 = 275 ;
     
 }
 else if(tricepSF < max4 && tricepSF > (max4 / 2))
 {
     x1 = 255 ; 
     y1 = 282 ;
 }
 else if(tricepSF <= (max4 / 2))
 {
     x1 = 265 ; 
     y1 = 285 ;
 }


// Plot subscapularSF       
 var x2 ; var y2 ;
 if(subscapularSF >= max1)
 {
     x2 = 320 ; 
     y2 = 0 ;
 }
 else if(subscapularSF < max1 && subscapularSF > ((max1 + max2)/2))
 {
     x2 = 320 ; 
     y2 = 15 ;
 }
 else if(subscapularSF == ((max1 + max2)/2))
 {
     x2 = 320 ; 
     y2 = 35 ;
 }
 else if(subscapularSF < ((max1 + max2)/2) && subscapularSF > max2)
 {
     x2 = 320 ; 
     y2 = 60 ;
 }     
 else if(subscapularSF == max2)
 {
     x2 = 320 ; 
     y2 = 80 ;
 }
 else if(subscapularSF < max2 && subscapularSF > ((max2 + max3)/2))
 {
     x2 = 320 ; 
     y2 = 95 ;
 }
 else if(subscapularSF == ((max2 + max3)/2))
 {
     x2 = 320 ; 
     y2 = 120 ;
 }
 else if(subscapularSF < ((max2 + max3)/2) && subscapularSF > max3)
 {
     x2 = 320 ; 
     y2 = 140 ;
 }
 else if(subscapularSF == max3)
 {
     x2 = 320 ; 
     y2 = 160 ;
 }
 else if(subscapularSF < max3 && subscapularSF > ((max3 + max4)/2))
 {
     x2 = 320 ; 
     y2 = 175 ;
 }
 else if(subscapularSF == ((max3 + max4)/2))
 {
     x2 = 320 ; 
     y2 = 205 ;
 }
 else if(subscapularSF < ((max3 + max4)/2) && subscapularSF > max4)
 {
     x2 = 320 ; 
     y2 = 225 ;
 }
 else if(subscapularSF == max4)
 {
     x2 = 320 ; 
     y2 = 240 ;
     
 }
 else if(subscapularSF < max4 && subscapularSF > (max4 / 2))
 {
     x2 = 320 ; 
     y2 = 250 ;
 }
 else if(subscapularSF <= (max4 / 2))
 {
     x2 = 320 ; 
     y2 = 265 ;
 }
 
 
 // Plot bicepSF       
 var x3 ; var y3 ;
 if(bicepSF >= max1)
 {
     x3 = 583 ; 
     y3 = 143 ;
 }
 else if(bicepSF < max1 && bicepSF > ((max1 + max2)/2))
 {
     x3 = 570 ; 
     y3 = 150 ;
 }
 else if(bicepSF == ((max1 + max2)/2))
 {
     x3 = 555 ; 
     y3 = 160 ;
 }
 else if(bicepSF < ((max1 + max2)/2) && bicepSF > max2)
 {
     x3 = 530 ; 
     y3 = 178 ;
 }
 else if(bicepSF == max2)
 {
     x3 = 520 ; 
     y3 = 186 ;
 }
 else if(bicepSF < max2 && bicepSF > ((max2 + max3)/2))
 {
     x3 = 508 ; 
     y3 = 193 ;
 }
 else if(bicepSF == ((max2 + max3)/2))
 {
     x3 = 485 ; 
     y3 = 210 ;
 }
 else if(bicepSF < ((max2 + max3)/2) && bicepSF > max3)
 {
     x3 = 465 ; 
     y3 = 223 ;
 }
 else if(bicepSF == max3)
 {
     x3 = 453 ; 
     y3 = 232 ;
 }
 else if(bicepSF < max3 && bicepSF > ((max3 + max4)/2))
 {
     x3 = 440 ; 
     y3 = 240 ;
 }
 else if(bicepSF == ((max3 + max4)/2))
 {
     x3 = 415 ; 
     y3 = 255 ;
 }
 else if(bicepSF < ((max3 + max4)/2) && bicepSF > max4)
 {
     x3 = 395 ; 
     y3 = 270 ;
 }
 else if(bicepSF == max4)
 {
     x3 = 385 ; 
     y3 = 278 ;
     
 }
 else if(bicepSF < max4 && bicepSF > (max4 / 2))
 {
     x3 = 380 ; 
     y3 = 280 ;
 }
 else if(bicepSF <= (max4 / 2))
 {
     x3 = 375 ; 
     y3 = 285 ;
 }
 
 // Plot abdominalSF      
 var x4 ; var y4 ;
 if(abdominalSF >= max1)
 {
     x4 = 580 ; 
     y4 = 495 ;
 }
 else if(abdominalSF < max1 && abdominalSF > ((max1 + max2)/2))
 {
     x4 = 573 ; 
     y4 = 492 ;
 }
 else if(abdominalSF == ((max1 + max2)/2))
 {
     x4 = 555 ; 
     y4 = 478 ;
 }
 else if(abdominalSF < ((max1 + max2)/2) && abdominalSF > max2)
 {
     x4 = 530 ; 
     y4 = 465 ;
 }
 else if(abdominalSF == max2)
 {
     x4 = 515 ; 
     y4 = 453 ;
 }
 else if(abdominalSF < max2 && abdominalSF > ((max2 + max3)/2))
 {
     x4 = 505 ; 
     y4 = 446 ;
 }
 else if(abdominalSF == ((max2 + max3)/2))
 {
     x4 = 485 ; 
     y4 = 432 ;
 }
 else if(abdominalSF < ((max2 + max3)/2) && abdominalSF > max3)
 {
     x4 = 463 ; 
     y4 = 416 ;
 }
 else if(abdominalSF == max3)
 {
     x4 = 450 ; 
     y4 = 405 ;
 }
 else if(abdominalSF < max3 && abdominalSF > ((max3 + max4)/2))
 {
     x4 = 440 ; 
     y4 = 398 ;
 }
 else if(abdominalSF == ((max3 + max4)/2))
 {
     x4 = 420 ; 
     y4 = 383 ;
 }
 else if(abdominalSF < ((max3 + max4)/2) && abdominalSF > max4)
 {
     x4 = 395 ; 
     y4 = 370 ;
 }
 else if(abdominalSF == max4)
 {
     x4 = 385 ; 
     y4 = 363 ;
     
 }
 else if(abdominalSF < max4 && abdominalSF > (max4 / 2))
 {
     x4 = 380 ; 
     y4 = 360 ;
 }
 else if(abdominalSF <= (max4 / 2))
 {
     x4 = 370 ; 
     y4 = 355 ;
 }
 
 
// Plot supraspinaleSF      
 var x5 ; var y5 ;
 if(supraspinaleSF >= max1)
 {
     x5 = 320 ; 
     y5 = 640 ;
 }
 else if(supraspinaleSF < max1 && supraspinaleSF > ((max1 + max2)/2))
 {
     x5 = 320 ; 
     y5 = 630 ;
 }
 else if(supraspinaleSF == ((max1 + max2)/2))
 {
     x5 = 320 ; 
     y5 = 610 ;
 }
 else if(supraspinaleSF < ((max1 + max2)/2) && supraspinaleSF > max2)
 {
     x5 = 320 ; 
     y5 = 570 ;
 }
 else if(supraspinaleSF == max2)
 {
     x5 = 320 ; 
     y5 = 560 ;
 }
 else if(supraspinaleSF < max2 && supraspinaleSF > ((max2 + max3)/2))
 {
     x5 = 320 ; 
     y5 = 550 ;
 }
 else if(supraspinaleSF == ((max2 + max3)/2))
 {
     x5 = 320 ; 
     y5 = 520 ;
 }
 else if(supraspinaleSF < ((max2 + max3)/2) && supraspinaleSF > max3)
 {
     x5 = 320 ; 
     y5 = 490 ;
 }
 else if(supraspinaleSF == max3)
 {
     x5 = 320 ; 
     y5 = 480 ;
 }
 else if(supraspinaleSF < max3 && supraspinaleSF > ((max3 + max4)/2))
 {
     x5 = 320 ; 
     y5 = 465 ;
 }
 else if(supraspinaleSF == ((max3 + max4)/2))
 {
     x5 = 320 ; 
     y5 = 440 ;
 }
 else if(supraspinaleSF < ((max3 + max4)/2) && supraspinaleSF > max4)
 {
     x5 = 320 ; 
     y5 = 410 ;
 }
 else if(supraspinaleSF == max4)
 {
     x5 = 320 ; 
     y5 = 400 ;
     
 }
 else if(supraspinaleSF < max4 && supraspinaleSF > (max4 / 2))
 {
     x5 = 320 ; 
     y5 = 390 ;
 }
 else if(supraspinaleSF <= (max4 / 2))
 {
     x5 = 320 ; 
     y5 = 375 ;
 }  
 
 
 // Plot calfSF      
 var x6 ; var y6 ;
 if(calfSF >= max1)
 {
     x6 = 63 ; 
     y6 = 500 ;
 }
 else if(calfSF < max1 && calfSF > ((max1 + max2)/2))
 {
     x6 = 68 ; 
     y6 = 495 ;
 }
 else if(calfSF == ((max1 + max2)/2))
 {
     x6 = 90 ; 
     y6 = 480 ;
 }
 else if(calfSF < ((max1 + max2)/2) && calfSF > max2)
 {
     x6 = 113 ; 
     y6 = 463 ;
 }
 else if(calfSF == max2)
 {
     x6 = 128 ; 
     y6 = 453 ;
 }
 else if(calfSF < max2 && calfSF > ((max2 + max3)/2))
 {
     x6 = 133 ; 
     y6 = 450 ;
 }
 else if(calfSF == ((max2 + max3)/2))
 {
     x6 = 155 ; 
     y6 = 435 ;
 }
 else if(calfSF < ((max2 + max3)/2) && calfSF > max3)
 {
     x6 = 180 ; 
     y6 = 418 ;
 }
 else if(calfSF == max3)
 {
     x6 = 192 ; 
     y6 = 408 ;
 }
 else if(calfSF < max3 && calfSF > ((max3 + max4)/2))
 {
     x6 = 203 ; 
     y6 = 400 ;
 }
 else if(calfSF == ((max3 + max4)/2))
 {
     x6 = 225 ; 
     y6 = 385 ;
 }
 else if(calfSF < ((max3 + max4)/2) && calfSF > max4)
 {
     x6 = 245 ; 
     y6 = 372 ;
 }
 else if(calfSF == max4)
 {
     x6 = 255 ; 
     y6 = 365 ;
     
 }
 else if(calfSF < max4 && calfSF > (max4 / 2))
 {
     x6 = 260 ; 
     y6 = 360 ;
 }
 else if(calfSF <= (max4 / 2))
 {
     x6 = 268 ; 
     y6 = 355 ;
 }


    $('#myCanvas6').drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_90, y1: y1_90,
      x2: x2_90, y2: y2_90,
      x3: x3_90, y3: y3_90,
      x4: x4_90, y4: y4_90,
      x5: x5_90, y5: y5_90,
      x6: x6_90, y6: y6_90,
      x7: x1_90, y7: y1_90
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_50, y1: y1_50,
      x2: x2_50, y2: y2_50,
      x3: x3_50, y3: y3_50,
      x4: x4_50, y4: y4_50,
      x5: x5_50, y5: y5_50,
      x6: x6_50, y6: y6_50,
      x7: x1_50, y7: y1_50
    }).drawLine({   
      strokeStyle: '#000',   
      strokeWidth: 1,   
      x1: x1_10, y1: y1_10,
      x2: x2_10, y2: y2_10,
      x3: x3_10, y3: y3_10,
      x4: x4_10, y4: y4_10,
      x5: x5_10, y5: y5_10,
      x6: x6_10, y6: y6_10,
      x7: x1_10, y7: y1_10
    }).drawLine({   
      strokeStyle: '#906bd0',   
      strokeWidth: 4,   
      x1: x1, y1: y1,
      x2: x2, y2: y2,
      x3: x3, y3: y3,
      x4: x4, y4: y4,
      x5: x5, y5: y5,
      x6: x6, y6: y6,
      x7: x1, y7: y1
    }); 
}
</script>
<script type="text/javascript">
        $(document).on('click','#exit', function(){
            document.forms["myform"].submit();
            //return false;
        });	
        
        $(document).on('click','#eight_Sites', function(){
            document.getElementById("sumSites").value = "8" ;   
        });
	
        $(document).on('click','.print_icon_toggle_btn', function(){
	    $(".print_icon_toggle").toggle();
	});
</script>  

</body>
</html>
