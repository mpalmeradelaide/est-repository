<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medical History</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script>
  $(function(){
     proceed = <?php  echo $fieldData[0]->option_8 ;  ?> 
            proceedTest = proceed.value
      if( proceedTest == 'proceed'  ){
      $("#proceed").prop('checked', true);
      /*
        if($(".yes:checked").length > 0) {
                   $("#guide").prop('checked', true);
                 }else{

                     // $("#proceed").removeAttr("checked");
                      $("#proceed").prop('checked', true);
                 } */
     // }else{
         
                   //   $("#proceed").prop('checked', true);
                 
          
      }else{
         $("#guide").prop('checked', true);
          
      }
      $(".yes").click(function(){

		if($(".yes:checked").length > 0) {
                    
			 $("#guide").prop('checked', true);
		}else{
                   
                    // $("#proceed").removeAttr("checked");
                     $("#proceed").prop('checked', true);
                } 
	});
      
      $(".no").click(function(){

		if($(".yes:checked").length > 0) {
                     
			 $("#guide").prop('checked', true);
		}else{
                 
                     
                    // $("#proceed").removeAttr("checked");
                     $("#proceed").prop('checked', true);
                } 
	});
      
	  
	   $(window).bind('scroll', function() {
		    
	   var navHeight = $( window ).height() - 400;
			 if ($(window).scrollTop() > 100) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});
	});
  
</script>
<!--For scroller End-->

</head>
<script type="text/javascript">
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
   <?php  //print_r($fieldData);
//var_dump($this->session->all_userdata());
?>
<body>
<?php
$hidden = array('userid' => $id  );
//print_r($hidden);
//echo "aaa";
//die;

  $attributes = array('id' => 'myform' , 'name'=>'myform');
 echo form_open('welcome/saveClientMedicalInfo',$attributes, $hidden); ?>
 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Medical History</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Medical History</div>
            <p>These questions require a simple ‘YES’ or ‘NO’. You will see that ‘NO’ is already selected as the default choice.
               If you are unsure then answer ‘YES’ and write additional information in the ‘Notes’ section at the bottom of the screen.
               The questions are part of a standard pre-exercise screening assessment and consist of general questions to determine 
               if a person has any major or uncontrolled cardiovascular, metabolic and respiratory diseases, signs and symptoms of disease, 
               or other medical issues that represent a substantial risk when beginning or upgrading their physical activity patterns.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>      
    </div>
</div>

<!--Start Wrapper --> 
<div class="wrapper">

<!--Start login --> 
<div class="login-cont" style="display:none;">
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="post" id="inputs"> -->
    <?php echo form_open('welcome/saveClientMedicalInfo',$attributes, $hidden); ?>
     <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required  value="<?php echo $_SESSION['user_first_name'] ;?>"  disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled"><input name ="submitMedical" type="submit" value="" title="edit client details" /></span>
     </div>
<!--	</form> -->
</div><!--End login --> 




   
<!--Start contain --> 
<div class="contain">
   
   <!--Start left --> 
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn active" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
     
       </div>
   <!--End Left --> 
 
 
 <?php //print_r($fieldData); ?>
   <!--Start right --> 
   <div class="right">
   		<div class="right-head" style="margin-bottom:0;">Medical History</div>
        
   	<div class="right-section page_medi">
    	<div class="field_row checkbox_title_row" style="border:0;">
      		<div class="field_85">&nbsp;</div>
            <div class="field_15">
            	<span>Yes</span>
            	<span>No</span>                
            </div>        
      </div>
        
      <div class="field_row">
      		<div class="field_85">
            	Has your doctor ever told you that you have a heart condition or have you ever suffered a stroke?
            </div>
            <div class="field_15">
            	<?php  $radio_is_checked = ($fieldData[0]->option_1 === 'Y')?"checked":"N";
                echo form_radio(array("name"=>"options_1","id"=>"options_1Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes')); ?> <label for="options_1Y"><span></span></label>
				<?php 
                echo form_radio(array("name"=>"options_1","id"=>"options_1N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"",'class'=>'no')); ?>  <label for="options_1N"><span></span></label>                
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	Do you ever  experience unexplained pains in your chest at rest or during physical activity/exercise?
            </div>
            <div class="field_15">
            	<?php $radio_is_checked = ($fieldData[0]->option_2 === 'Y')?"checked":"N";
               echo form_radio(array("name"=>"options_2","id"=>"options_2Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes' )); ?>  <label for="options_2Y"><span></span></label>
				<?php  
                 echo form_radio(array("name"=>"options_2","id"=>"options_2N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no')); ?> <label for="options_2N"><span></span></label>         		
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	Do you ever feel faint or have spells of dizziness during physical activity/exercise that causes you to lose balance?
            </div>
            <div class="field_15">
				<?php  $radio_is_checked = ($fieldData[0]->option_3 === 'Y')?"checked":"N";
        		echo form_radio(array("name"=>"options_3","id"=>"options_3Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes' )); ?>  <label for="options_3Y"><span></span></label>
				<?php
          		echo form_radio(array("name"=>"options_3","id"=>"options_3N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no')); ?> <label for="options_3N"><span></span></label>
         		
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	Have you had an asthma attack that required immediate medical attention at any time over the last 12 months?
            </div>
            <div class="field_15">
				<?php $radio_is_checked = ($fieldData[0]->option_4 === 'Y')?"checked":"N";
        		echo form_radio(array("name"=>"options_4","id"=>"options_4Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes' )); ?>  <label for="options_4Y"><span></span></label>
				<?php
          		echo form_radio(array("name"=>"options_4","id"=>"options_4N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no')); ?> <label for="options_4N"><span></span></label>
         		
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	If you have diabetes (type 1 or type 2) have you had trouble controlling your blood glucose in the last 3 months?
            </div>
            <div class="field_15">
				<?php $radio_is_checked = ($fieldData[0]->option_5 === 'Y')?"checked":"N";
				echo form_radio(array("name"=>"options_5","id"=>"options_5Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes')); ?>  <label for="options_5Y"><span></span></label>
				<?php 
           		echo form_radio(array("name"=>"options_5","id"=>"options_5N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no' )); ?> <label for="options_5N"><span></span></label>
         		
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	Do you have any diagnosed muscle, bone or joint problem that you have been told could be made worse by participating in physical activity/exercise?
            </div>
            <div class="field_15">
		    <?php $radio_is_checked = ($fieldData[0]->option_6 === 'Y')?"checked":"N";
            echo form_radio(array("name"=>"options_6","id"=>"options_6Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes' )); ?>  <label for="options_6Y"><span></span></label>
			<?php 
            echo form_radio(array("name"=>"options_6","id"=>"options_6N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no' )); ?> <label for="options_6N"><span></span></label>
         		
        	</div>        
      </div>
      
      <div class="field_row">
      		<div class="field_85">
            	Do you have any other medical conditions that may make it dangerous for you to participate in physical activity/ exercise?
            </div>
            <div class="field_15">
				<?php $radio_is_checked = ($fieldData[0]->option_7 === 'Y')?"checked":"N";
				echo form_radio(array("name"=>"options_7","id"=>"options_7Y","value"=>"Y",'checked'=>($radio_is_checked === "N")?"":"checked",'class'=>'yes' )); ?>  <label for="options_7Y"><span></span></label>
				<?php
           		echo form_radio(array("name"=>"options_7","id"=>"options_7N","value"=>"N",'checked' =>($radio_is_checked === "N")?"checked":"", 'class'=>'no')); ?> <label for="options_7N"><span></span></label>
         		
        	</div>        
      </div>
        
        
       <div class="field_row" style="border-bottom:0; margin-bottom:0;">
           <label>Notes:</label>
           <?php 
           $notes = $fieldData[0]->notes==''?'':$fieldData[0]->notes;
           $opts = 'placeholder="Notes", maxlength="200"';
           echo form_textarea('notes',set_value('description', $notes),$opts); ?>
       </div>
       
      
      <div class="field_row" style="border-bottom:0;">
      	<div class="field_85" style="width:50%;">What is your professional decision?</div>
        <div class="field_15" style="width:40%;">
			  <?php  $radio_is_checked = ($fieldData[0]->option_8 === 'proceed')?True:"";
              echo form_radio(array("name"=>"option_8","id"=>"proceed","value"=>"proceed")); ?> <label for="proceed" style="float:left; margin-right: 10px; margin-bottom:10px;"><span style="float:left; margin-right:8px;"></span> Proceed</label>
              <?php $radio_is_checked = ($fieldData[0]->option_8 === 'guide')?"checked":"";
              echo form_radio(array("name"=>"option_8","id"=>"guide","value"=>"guide","checked"=>$radio_is_checked )); ?> <label for="guide" style="float:left;"><span style="float:left; margin-right:8px;"></span> Refer for guidance</label>
          </div>
      </div>
      
   <?php echo form_submit('mysubmit2','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform2'");?>
                   
    <?php echo form_close(); ?>
      </div>
      	<!--End right section--> 
   </div>
   <!--End right --> 
   
   
</div><!--End contain -->
<!--
<div class="footer footer-top">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Wrapper --> 

</body>
</html>
