<?php 		
//To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
              $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
              $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
              $bodyfatdiv="block";    
            }
            else
            {
              $bodyfatdiv="none";    
            }
			 
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<style>

sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>

    
    
    
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		  $(".v_person").fadeTo( "slow" , 1, function() {});
		$(".v_detail").toggle();
		//Show Sports list with oz Ranking
		calculate_oz_rating();
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>    
    
    
    
    
    
    
    
    
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');  
        
        
         var body_fatarray = [];
        
// Code added

     var c_ht=document.getElementById("height_slider").value;            //alert(c_ht);
     var c_mass=document.getElementById("mass_slider").value;            //alert(c_mass);
     var c_body_fat=document.getElementById("body_fat_slider").value;    //alert(c_body_fat);
     var c_endomorph=document.getElementById("endomorph_slider").value;  //alert(c_endomorph);
     var c_mesomorph=document.getElementById("mesomorph_slider").value;  //alert(c_mesomorph);
     var c_ectomorph=document.getElementById("ectomorph_slider").value;  //alert(c_ectomorph);
   
       if(isNaN(c_endomorph))
          {
              c_endomorph=0;
          }
      if(isNaN(c_mesomorph))
          {
              c_mesomorph=0;
          }
      if(isNaN(c_ectomorph))
          {
              c_ectomorph=0;
          }
     var sport_arr= '<?php echo json_encode($fieldData); ?>' ;
         console.log(sport_arr);
     var htmlTable ='' ;
     var gender='<?php echo $_SESSION["gender"]; ?>'; 
     $.each($.parseJSON(sport_arr), function(key,value){
    if( gender == 'F' || gender == 'Female')    
             {
                 var gen="female";
             }
    else
             {
                 var gen="male";
             }
    if(value.gender == gen )    
            {        
		
		
		
		
                    //alert("SD Body fat"+value.body_fat_sd);
                    if(value.body_fat_sd == "" || value.body_fat_per == "" || value.endomorph == "" || value.endo_sd == "" || value.mesomorph == "" || value.meso_sd == "" || value.ectomorph == "" || value.ecto_sd == "" )
                        {
                            body_fatarray.push(value.sport);
                        }
                    else{
                    //z score    
					
                    var ZSportFat = ((c_body_fat - value.body_fat_per)/value.body_fat_sd) ;      // console.log("Fat"+ZSportFat);
                    var ZSportHt = ((c_ht - value.height)/value.height_sd) ;                     // console.log("Ht"+ZSportHt); 
                    var ZSportMass = ((c_mass - value.body_mass)/value.body_mass_sd) ;            //console.log("Mass"+ZSportMass); 
                    var ZSportEndo = ((c_endomorph - value.endomorph)/value.endo_sd) ;            //console.log("Endo"+ZSportEndo); 
                    var ZSportMeso = ((c_mesomorph - value.mesomorph)/value.meso_sd) ;            //console.log("Meso"+ZSportMeso); 
                    var ZSportEcto = ((c_ectomorph - value.ectomorph)/value.ecto_sd) ;            //console.log("Ecto"+ZSportEcto); 

                    // Probability    
                    var pFat = 2*normalcdf(ZSportFat)-1 ;    //console.log("Probfat"+pFat);
					var pHt = 2*normalcdf(ZSportHt)-1 ;     //console.log("Ht"+pHt);
                    var pMass = 2*normalcdf(ZSportMass)-1;   //console.log("Probfat"+pMass);
                    var pEndo = 2*normalcdf(ZSportEndo)-1 ;  //console.log("Probfat"+pEndo);
                    var pMeso = 2*normalcdf(ZSportMeso)-1 ;  //console.log("Probfat"+pMeso);
                    var pEcto = 2*normalcdf(ZSportEcto)-1  ;   //console.log("Probfat"+pEcto);
                   
                    //Distance = Squareroot(pFat^2+pHt^2+pMass^2+pEndo^2+pMeso^2+pEcto^2) 
                    //FINALOVERLAP  = 100-(Distance*100/2.45)     
        
                    var distance= Math.sqrt(Math.pow(pFat,2)+Math.pow(pHt,2)+Math.pow(pMass,2)+Math.pow(pEndo,2)+Math.pow(pMeso,2)+Math.pow(pEcto,2));   console.log("Distance"+distance);
                    var finaloverlap = Math.round ( (100-(distance*100/2.45)) * 10 ) / 10;   console.log("FinalOverlap"+finaloverlap);
                 }
                    //alert("Sport "+value.sport+"  finaloverlap"+finaloverlap);
                
                if(isNaN(finaloverlap))
                {
                    finaloverlap=0;
                }
                    htmlTable += '<tr>'+
                              '<td align="left">'+value.sport+'</td>'+
                              '<td align="right">'+finaloverlap+'</td>'+
                            '</tr>';
            }
}); // end of function

                    console.log("body_fat_array"+body_fatarray);
                    $('.body_sports_table').html(htmlTable);
        // Code added

    });
    
    
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
    

    
    
    
    
    
</head>
<body>
     <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],1);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="<?php echo $_SESSION['bodyfat']; ?>">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
    
    
    
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Sport Match</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        
         <div class="orng_box_btn f_right">
        	<form name="vpform" id="vprestrictedgen" method="post"> 
                <a href="#" class="virtual_btnrestricted"><img src="<?php echo "$base/assets/images/"?>virtual_icon.png" style="margin-top: 12px;"></a>
                <input type="hidden" name="vptype" value="sport_match">
            </form> 
        </div>
        
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Sport Match</div>
            <p>The morphological overlap between the client and athletes within each sport is calculated and shown in the table. The table shows how well the match is between the client and elite-level athlete. This is based on height and mass, % body fat, and calculated somatotype  [if all the necessary anthropometric variables have been entered]. A rating of 100 is a perfect match, overlap values above 80% are excellent matches, values between 0.61 and 0.80 represent substantial morphological overlap, 0.41 to 0.60 represent a moderate match, 0.10 to 0.40 suggest a poor to fair match, while values less than 0.1 show a very poor match.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/go_on_menu', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Sport Match</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">
					<div class="f_left width_70px"><label>Height (cm)</label><input type="text" id="height_text" name="height_text" value="<?php echo $_SESSION['HEIGHT'];?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="height_slider" type="range" value="<?php echo $_SESSION['HEIGHT'];?>" min="130" max="210" step="0.1" link-to="height_text" data-rangeslider id="height_slider">
						<ul class="range_numbers range_hori">
							<li>130</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>210</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>Mass (kg)</label><input type="text" id="mass_text" name="mass_text" value="<?php echo $_SESSION['MASS'];?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="mass_slider" type="range" value="<?php echo $_SESSION['MASS'];?>" min="30" max="160" step="0.1" link-to="mass_text" data-rangeslider id="mass_slider">
						<ul class="range_numbers range_hori range_hori2">
							<li>30</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>160</li>
						</ul>
					</div>	
																			
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>% body fat</label><input type="text" id="body_fat_per" name="body_fat_per" value="<?php echo $_SESSION['bodyfat'];?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="body_fat_slider" type="range"  value="<?php echo $_SESSION['bodyfat'];?>" min="3" max="40" step="0.1" link-to="body_fat_per" data-rangeslider id="body_fat_slider">
						<ul class="range_numbers range_hori range_hori2">
							<li>3</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>40</li>
						</ul>
					</div>																							
					
					<div class="clearfix">&nbsp;</div>
					<strong class="right_sp_head" style="margin: 20px 0;">Somatotype</strong>	
																																	
					<div class="f_left width_70px"><label>Endomorph</label><input type="text" id="endomorph_txt" name="endomorph_txt"  style="width:54px;" value="<?php echo $_SESSION['endo_score'] ;?>">  </div>
					<div class="range_div horiz_range_div">
						<input name="endomorph_slider" type="range"  min="0" max="10" step="0.1" link-to="endomorph_txt" data-rangeslider id="endomorph_slider" value="<?php echo $_SESSION['endo_score']; ?>">
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Mesomorph</label><input type="text" id="mesomorph_txt" name="mesomorph_txt"  style="width:54px;" value="<?php echo $_SESSION['meso_score'] ;?>"> </div>
					<div class="range_div horiz_range_div">
						<input name="mesomorph_slider" type="range"  min="0" max="10" step="0.1" link-to="mesomorph_txt" data-rangeslider id="mesomorph_slider" value="<?php echo $_SESSION['meso_score'] ;?>" >
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>		
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Ectomorph</label><input type="text" id="ectomorph_txt" name="ectomorph_txt"  style="width:54px;" value="<?php echo $_SESSION['ecto_score']; ?>"> </div>
					<div class="range_div horiz_range_div">
						<input name="ectomorph_slider" type="range"  min="0" max="10" step="0.1" link-to="ectomorph_txt" data-rangeslider id="ectomorph_slider" value="<?php echo $_SESSION['ecto_score']; ?>">
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>
					
					<div class="clearfix">&nbsp;</div>
					<button class="lite_btn f_left" type="button" style="margin-top:30px;">Recalculate</button>
				</div>     
				
				<div class="field_30 f_right">
					<strong class="right_sp_head color_vio">Sport Overlap</strong>
					
					<div class="sport_table">
						<table width="100%" border="0">
					   <thead>
					   		<tr>
							  <th align="left" height="35px">Sport</th>
							  <th align="right" height="35px">OZ rating (%)</th>
							</tr>					   	
					   </thead>
					  <tbody class="body_sports_table">
                          
                               <?php foreach($fieldData as $val)
                                                {
                                                echo "<tr>
                                                      <td align='left'>".$val->sport."</td>
                                                      <td align='right'>XX.X</td>
                                                    </tr>
                                                 " ; 
                                                }
                                              ?> 
                          
						
                                             
					  </tbody>
					</table>
					</div>
				</div>     
			</div>
        </div>
    </div>
      
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
  <script>
  var body_fatarray = [];
      
  $(document).on('click','#exit', function(){           
          document.forms["myform"].submit();
        //return false;
        });	
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
}); 
   

  $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
      var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
     $text_box.val(this.value); 
	calculate_oz_rating();
	});
  $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
});
      $text_box.val(this.value);   
	});
});    
//Sport Match 	
function calculate_oz_rating()
{
     var c_ht=document.getElementById("height_slider").value;            //alert(c_ht);
     var c_mass=document.getElementById("mass_slider").value;            //alert(c_mass);
     var c_body_fat=document.getElementById("body_fat_slider").value;    //alert(c_body_fat);
     var c_endomorph=document.getElementById("endomorph_slider").value;  //alert(c_endomorph);
     var c_mesomorph=document.getElementById("mesomorph_slider").value;  //alert(c_mesomorph);
     var c_ectomorph=document.getElementById("ectomorph_slider").value;  //alert(c_ectomorph);
      if(isNaN(c_endomorph))
          {
              c_endomorph=0;
          }
      if(isNaN(c_mesomorph))
          {
              c_mesomorph=0;
          }
      if(isNaN(c_ectomorph))
          {
              c_ectomorph=0;
          }
	var sport_arr= '<?php echo json_encode($fieldData); ?>' ;
    var objs = [];
	 var htmlTable ='' ;
     var gender='<?php echo $_SESSION["gender"]; ?>';  
    $.each($.parseJSON(sport_arr), function(key,value){
    if( gender == 'F' || gender == 'Female')    
             {
                 var gen="female";
             }
    else
             {
                 var gen="male";
             }
    if(value.gender == gen )    
            {           if(value.body_fat_sd == "")
                            {
                                body_fatarray.push(value.sport);
                            }    
                        else{
                       //z score    
                        var ZSportFat = parseFloat(((c_body_fat-value.body_fat_per)/(value.body_fat_sd)).toFixed(9));
                        //console.log("ZSportFat"+ZSportFat);
						var ZSportHt = parseFloat(((c_ht-value.height)/(value.height_sd)).toFixed(9));
						//console.log("ZSportHt"+ZSportHt);
						var ZSportMass = parseFloat(((c_mass-value.body_mass)/(value.body_mass_sd)).toFixed(9));
						//console.log("ZSportMass"+ZSportMass);
						var ZSportEndo =parseFloat(((c_endomorph-value.endomorph)/(value.endo_sd)).toFixed(9)); 
					    //console.log("ZSportEndo"+ZSportEndo);
						var ZSportMeso =parseFloat(((c_mesomorph-value.mesomorph)/(value.meso_sd)).toFixed(9));  
						//console.log("ZSportMeso"+ZSportMeso);
						var ZSportEcto = parseFloat(((c_ectomorph-value.ectomorph)/(value.ecto_sd)).toFixed(9));  
						//console.log("ZSportEcto"+ZSportEcto);
						// Probability    
                        var pFat = parseFloat((2*normalcdf(ZSportFat)-1).toFixed(9));
                        //console.log("pFat"+pFat);
						var pHt = parseFloat((2*normalcdf(ZSportHt)-1).toFixed(9));
                        //console.log("pHt"+pHt);
						var pMass = parseFloat((2*normalcdf(ZSportMass)-1).toFixed(9)); 
                        //console.log("pMass"+pMass);
						var pEndo = parseFloat((2*normalcdf(ZSportEndo)-1).toFixed(9));
                         //console.log("pEndo"+pEndo);
						var pMeso = parseFloat((2*normalcdf(ZSportMeso)-1).toFixed(9));
                         //console.log("pMeso"+pMeso);
						var pEcto = parseFloat((2*normalcdf(ZSportEcto)-1).toFixed(9)) ;  
						//console.log("pEcto"+pEcto);
						var distance= parseFloat((Math.sqrt(Math.pow(pFat,2)+Math.pow(pHt,2)+Math.pow(pMass,2)+Math.pow(pEndo,2)+Math.pow(pMeso,2)+Math.pow(pEcto,2)).toFixed(9)));   
						//console.log("distance"+distance);
						var finaloverlap = Math.round ( (100-(distance*100/2.45)) * 10 ) / 10;
						}
                        if(isNaN(finaloverlap))
                            {
                                finaloverlap=0;
                            }
			 objs.push({
				   'sport':value.sport,
				   'finaloverlap':finaloverlap
					});
		    }
  }); 
     objs.sort(compare);
	 $.each(objs,function(key,value){
			htmlTable += '<tr>'+
			'<td align="left">'+value.sport+'</td>'+
			'<td align="right">'+value.finaloverlap+'</td>'+
              '</tr>';
	}); 
	 
	 $('.body_sports_table').html(htmlTable);	
	} 
  //Sorting in Decending
  function compare(a,b) {
  if (a.finaloverlap > b.finaloverlap)
    return -1;
  if (a.finaloverlap < b.finaloverlap)
    return 1;
  return 0;
   }


  function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    } 
      
     
      
      
      
   $(document).on('click','.virtual_btnrestricted', function(){
    $("#vprestrictedgen").attr("action", "<?php echo base_url(); ?>index.php/Body/BCVirtualpersonGeneration");     
        $("#vprestrictedgen").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
       $(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 
      
      
      
 </script>  
</body>
</html>
