<!doctype html>
<html>
<head>
<meta charset="utf-8">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>overlay.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
#chartdiv .jqplot-axis.jqplot-yaxis {
    text-align: left;
    width: 190px !important; 
	text-transform: capitalize; color:#000;
}
#chartdiv .jqplot-xaxis:before {
    content: 'Smallest';
    position: absolute;
    left: 190px;
    top: 30px;
    font-size: 14px; color:#808080;
}
#chartdiv .jqplot-xaxis:after {
    content: 'Largest';
    position: absolute;
    right: 0;
    top: 30px;
    font-size: 14px; color:#808080;
}
#chartdiv .jqplot-yaxis .jqplot-yaxis-tick {
    right:auto; left:0;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay2").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay2").toggle();
	});  
</script>
</head> 
<body>
<!-- insertion of  header -->
<?=$header_insert; ?>

<div class="overlay2">&nbsp;</div> 
	<div class="info_block">
		<div class="info_block_head violet_container">Body Composition</div>
            <p> The Norms screen allows the user to compare a range of anthropometry measures
				against age- and sex-based norms for 5-year categories from a national database.
				An additional comparison can be made if the full profile is measured.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
<div class="wrapper">
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Body/skinfold_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?> 
	<div class="contain">
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Anthropometry Norms</div>
            
            <table class="graph_table" width="100%">
              <tbody>
                <tr>                
                    <td colspan="3" align="center">  
                        <div class="population_div_main">                             
                            <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>"> 
                            <input type="hidden" id="age_range" name="age_range" value="<?php echo $_SESSION['age_range'] ;?>">
                            <input type="hidden" id="height" name="height" value="<?php echo isset($anthropometry_Values["height"])?$anthropometry_Values["height"]:""; ?>"> 
                            <input type="hidden" id="body_mass" name="body_mass" value="<?php echo isset($anthropometry_Values["body_mass"])?$anthropometry_Values["body_mass"]:""; ?>"> 
                            <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>">                          
                        </div>
                                         
                        <div class="anthro_chart_main"> 
                            <div id="chartdiv" style="width:100%; height:750px;">&nbsp;</div>
                        </div>                      
                    </td>
                </tr>
              </tbody>
            </table>
        
        	<div style="display:none;">
                <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
                <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
                <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
                <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
                <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
                <!--<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>-->
            </div>
    	</div>
        
    </div>

             <input type="hidden" id="client_x" name="client_x" value="">
             <input type="hidden" id="client_y" name="client_y" value="">
             
             <input type="hidden" id="selectedSports_x" name="selectedSports_x">
             <input type="hidden" id="selectedSports_y" name="selectedSports_y">
             
             <input type="hidden" id="triceps" name="triceps" value="<?php echo isset($anthropometry_Values["triceps"])?$anthropometry_Values["triceps"]:""; ?>"> 
             <input type="hidden" id="biceps" name="biceps" value="<?php echo isset($anthropometry_Values["biceps"])?$anthropometry_Values["biceps"]:""; ?>"> 
             <input type="hidden" id="subscapular" name="subscapular" value="<?php echo isset($anthropometry_Values["subscapular"])?$anthropometry_Values["subscapular"]:""; ?>">              
             <input type="hidden" id="thigh" name="thigh" value="<?php echo isset($anthropometry_Values["thigh"])?$anthropometry_Values["thigh"]:""; ?>"> 
             <input type="hidden" id="relArmG" name="relArmG" value="<?php echo isset($anthropometry_Values["relArmG"])?$anthropometry_Values["relArmG"]:""; ?>"> 
             <input type="hidden" id="waistG" name="waistG" value="<?php echo isset($anthropometry_Values["waistG"])?$anthropometry_Values["waistG"]:""; ?>"> 
             <input type="hidden" id="hipG" name="hipG" value="<?php echo isset($anthropometry_Values["hipG"])?$anthropometry_Values["hipG"]:""; ?>"> 
             <!--<input type="hidden" id="thigh" name="thighG" value="<?php echo isset($anthropometry_Values["thighG"])?$anthropometry_Values["thighG"]:""; ?>">-->              
                                     
             <input type="hidden" id="exit_key" name="exit_key" value="">
             <input type="hidden" id="action_key" name="action_key" value="">
             
	<!-- <?php echo form_close(); ?>-->
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
	
	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
    }); 
	
	$(document).ready(function() {	  
		$(".overlay").overlay();
	});
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>overlay.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<link rel="stylesheet" type="text/css" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
 
 
  var anthroList ;
  var plotList ;
 
   $(window).bind("load", function() {
      
    var age = document.getElementById("age").value ; 
    var ht = document.getElementById("height").value ;
    var body_mass = document.getElementById("body_mass").value ;
    
    var triceps = document.getElementById("triceps").value ;            
    var subscapular = document.getElementById("subscapular").value ;   
    var biceps = document.getElementById("biceps").value ; 
    var thigh = document.getElementById("thigh").value ;  
    var relArmG = document.getElementById("relArmG").value ;            
    var waistG = document.getElementById("waistG").value ;    
    var hipG = document.getElementById("hipG").value ;  
    
    var pi = 3.1415962 ;    
        
   if(document.getElementById("gender").value == "Male")
   {    
    var TriAdj = (parseFloat(triceps) - 1.28) * 170.18 / ht + 1.28 ;
    var SubScapAdj = (parseFloat(subscapular) - 2.07) * 170.18 / ht + 2.07 ;
    var ThighAdj = (parseFloat(thigh) -1.49) * 170.18 / ht + 1.49 ;
   }    
   else if(document.getElementById("gender").value == "Female")
   {    
    var TriAdj = (parseFloat(triceps) - 1.1) * 170.18 / ht + 1.1 ;
    var SubScapAdj = (parseFloat(subscapular) - 1.74) * 170.18 / ht + 1.74 ;
    var ThighAdj = (parseFloat(thigh) - 1.04) * 170.18 / ht + 1.04 ;
   }
   
    var SFTotAdj = TriAdj + SubScapAdj + ThighAdj ;   
    var MassAdj = (parseFloat(body_mass)) * ((170.18/ht) * (170.18/ht) * (170.18/ht)) ;
    var BMI = parseFloat(body_mass)/((ht/100) * (ht/100)) ;
    var WHR = parseFloat(waistG)/parseFloat(hipG) ; 
    var RelArmGirthAdj = parseFloat(relArmG) * 170.18/ht ;
    var WaistGirthAdj = parseFloat(waistG) * 170.18/ht ;
    var HipGirthAdj = parseFloat(hipG) * 170.18/ht ;
    var Corrected_RelArmGirthAdj = (parseFloat(relArmG)  - pi * (parseFloat(biceps) + parseFloat(triceps))/20)*170.18/ht ;  
    //var Corrected_ThighGirthAdj = (parseFloat(thighG) - pi * (parseFloat(thigh)))/10)*170.18/ht ; 
    
   if(document.getElementById("gender").value == "Male")
   {     
     if(document.getElementById("age_range").value == "18-29")
     {
       var sfTotal_male = ['15.31','16.17','18.01','20.21','23.7','27.09','31','34.4','39.03','44.77','50.77','62.21','71.02','81.78','89.86','89.86'] ;
       var tricep_male = ['3.96','4.23','4.77','5.39','6.49','7.66','8.78','9.98','11.35','13.03','15.41','19.46','23.55','30.26','33.29','33.29'] ;
       var subscapular_male = ['6.32','6.77','7.44','8.24','9.47','10.81','12.33','13.94','16.24','18.76','22.09','26.94','30.72','34.13','36.8','36.8'] ;
       var thigh_male = ['4.11','4.49','5.06','5.72','6.98','8.02','9.11','10.49','12.16','13.92','16.27','20.33','25.18','30.18','34.04','34.04'] ;
       var mass_male = ['50.06','51.5','54.1','56.56','60.44','63.61','66.36','69.12','72.38','76.09','81.25','88.26','95.49','105.94','116.22','116.22'] ;
       var bmi_male = ['17.92','18.4','19.4','20.2','21.5','22.4','23.3','24.3','25.3','26.8','28.4','31.12','33.8','37.07','41.49','41.49'] ;
       var whr_male = ['0.77','0.7866','0.8','0.82','0.84','0.85','0.87','0.88','0.9','0.92','0.94','0.97','1','1.03','1.06','1.06'] ;
       var relaxArmGirth_male = ['24.22','24.8','25.93','27.03','28.36','29.33','30.23','31.09','32.09','33.01','34.38','36.46','38.22','40.16','42.23','42.23'] ;
       var waistGirth_male = ['65.51','66.46','68.76','70.98','74.37','77.21','80.03','82.8','85.78','89.31','93.59','100.3','105.97','114.21','121.21','121.21'] ;
       var hipGirth_male = ['79.34','81.09','82.95','84.82','87.68','89.81','91.7','93.71','95.55','97.95','100.99','104.82','110.04','115.73','122.85','122.85'] ;
       var corrected_relaxArmGirth_male = ['21.71','22.4','23.38','24.22','25.39','26.25','26.98','27.63','28.25','28.97','30.01','31.34','32.59','33.82','35.77','35.77'] ;
       var corrected_thighGirth_male = ['38.59','39.49','40.65','41.95','43.59','44.78','45.83','46.83','47.86','48.92','50.29','52.36','54.28','56.15','57.47','57.47'] ; 
     }
     else if(document.getElementById("age_range").value == "30-39")
     {
       var sfTotal_male = ['14.93','16.3','19.56','22.24','27.98','32.8','36.95','40.76','44.61','49.97','56.09','66.31','75.87','88.57','96.81','96.81'] ;
       var tricep_male = ['3.72','4.16','4.87','5.79','7.36','8.51','9.8','10.83','12.24','14.11','16.05','20.28','24.91','31.03','34.65','34.65'] ;
       var subscapular_male = ['6.81','7.14','8.07','9.22','11.65','13.64','15.67','17.96','19.94','22.38','25.22','29.48','32.71','37.88','40.9','40.9'] ;
       var thigh_male = ['3.73','4.25','5.25','6.17','7.66','8.84','10.13','11.57','13.13','14.65','17.58','22.06','26.73','32.21','36.37','36.37'] ;
       var mass_male = ['50.41','53.12','56','59.62','63.59','67.26','70.43','73.09','76.26','80','84.76','93.31','102.93','113.5','121.32','121.32'] ;
       var bmi_male = ['18.4','19.1','20.1','21.24','22.8','23.92','24.9','26','27','28.1','29.8','32.7','35.86','40.79','43.53','43.53'] ;
       var whr_male = ['0.79','0.81','0.83','0.86','0.88','0.9','0.92','0.93','0.95','0.97','0.99','1.02','1.04','1.079','1.1','1.1'] ;
       var relaxArmGirth_male = ['24.59','25.63','27.03','28.03','29.48','30.58','31.49','32.31','33.19','34.19','35.46','37.54','39.41','41.44','43.48','43.48'] ;
       var waistGirth_male = ['67.35','68.86','72.53','75.83','80.04','83.68','86.8','89.33','92.2','95.36','99.16','107.05','113.3','123.59','127.79','127.79'] ;
       var hipGirth_male = ['80','81.59','84.61','86.78','89.62','91.72','93.74','95.45','97.3','99.42','101.9','106.92','112.46','119.79','125.83','125.83'] ;
       var corrected_relaxArmGirth_male = ['22.45','23.07','24.16','25.1','26.31','27.05','27.89','28.53','29.26','30.02','30.86','32.28','33.56','34.65','36.07','36.07'] ;
       var corrected_thighGirth_male = ['38.04','39.21','40.89','42.08','43.71','44.88','45.85','46.88','47.81','49.08','50.37','52.06','54.23','56.7','58.19','58.19'] ; 
     }
     else if(document.getElementById("age_range").value == "40-49")
     {
       var sfTotal_male = ['15.14','16.7','20.37','24.84','31.35','35.28','38.27','42.68','46.54','51.22','57.25','67.02','76.69','83.9','90.71','90.71'] ;
       var tricep_male = ['3.85','4.21','5.27','6.46','7.7','7.9','10.25','11.47','12.76','14.29','16.63','21.33','24.42','28.98','31.64','31.64'] ;
       var subscapular_male = ['6.72','7.4','8.58','10.52','13.59','15.46','17.56','19.22','21.34','23.41','26.37','30.75','34.09','37.01','39.06','39.06'] ;
       var thigh_male = ['3.98','4.35','5.34','6.54','7.91','9.18','10.43','11.71','13.19','14.79','17.33','21.95','27.35','34.12','38.03','38.03'] ;
       var mass_male = ['52.6','53.87','58.07','61.71','66.58','70.14','72.98','75.91','79.26','83.69','89.31','96.68','104.72','113.24','123.01','123.01'] ;
       var bmi_male = ['18.9','19.5','20.8','22','23.7','24.8','25.8','26.8','27.8','29.4','31.4','34.1','36.5','40.3','42.9','42.9'] ;
       var whr_male = ['0.82','0.84','0.86','0.89','0.92','0.94','0.95','0.97','0.99','1','1.02','1.05','1.08','1.1','1.13','1.13'] ;
       var relaxArmGirth_male = ['25.3','26.11','27.23','28.53','29.98','30.95','31.86','32.67','33.45','34.66','35.89','37.81','39.12','41','42.76','42.76'] ;
       var waistGirth_male = ['70','72.14','75.73','79.93','84.55','88.23','90.9','93.73','96.37','100.35','104.79','111.71','117.4','124.68','131.8','131.8'] ;
       var hipGirth_male = ['80.72','82.25','84.98','87.6','90.98','93.18','94.78','96.53','98.65','100.77','103.76','108.21','114.29','120.23','126.41','126.41'] ;
       var corrected_relaxArmGirth_male = ['23.15','23.72','24.35','25.42','26.53','27.49','28.17','28.81','29.47','30.19','31.04','32.39','33.52','34.83','35.72','35.72'] ;
       var corrected_thighGirth_male = ['38.19','38.96','40.24','41.54','43.13','44.31','45.39','46.41','47.48','48.62','49.81','51.96','53.26','54.93','56.77','56.77'] ; 
     }
     else if(document.getElementById("age_range").value == "50-59")
     { 
       var sfTotal_male = ['15.49','18.09','23.6','26.58','31.5','36.53','40.7','44.39','47.96','52.07','58.06','66.69','74.92','88.89','97.52','97.52'] ;
       var tricep_male = ['4.42','4.87','5.88','6.69','8.13','9.32','10.39','11.74','12.96','14.43','16.8','21.23','25.39','31.99','35.13','35.13'] ;
       var subscapular_male = ['6.87','7.84','9.43','11.36','13.61','16.22','18.07','19.78','21.77','24.02','26.79','30.62','33.42','37.51','39.89','39.89'] ;
       var thigh_male = ['4.06','4.75','5.57','6.6','8.16','9.28','10.37','11.67','13.27','15.24','17.96','23.56','28.98','33.86','37.77','37.77'] ;
       var mass_male = ['53.26','54.65','58.38','61.9','67.25','70.38','73.45','76.55','79.84','83.99','88.71','95.31','103.41','116.39','124.22','124.22'] ;
       var bmi_male = ['18.8','19.5','20.6','22.1','23.9','25','26.2','27.3','28.2','29.6','31.1','33.6','36.7','41.4','44.4','44.4'] ;
       var whr_male = ['0.84','0.87','0.89','0.92','0.94','0.96','0.98','0.99','1','1.02','1.04','1.07','1.1','1.13','1.14','1.14'] ;
       var relaxArmGirth_male = ['24.93','25.6','26.66','27.94','29.51','30.81','31.78','32.56','33.47','34.4','35.57','37.25','38.7','41.9','44.01','44.01'] ;
       var waistGirth_male = ['71.68','75.69','79.37','83.33','87.44','90.62','93.31','95.67','99.13','102.45','107.38','112.85','118.31','125.69','132.57','132.57'] ;
       var hipGirth_male = ['82.51','83.6','85.79','88.12','91.55','93.66','95.16','96.72','98.86','101.15','103.48','108.32','112.56','119.52','126.38','126.38'] ;
       var corrected_relaxArmGirth_male = ['22.34','23.06','23.93','24.74','26.07','26.98','27.74','28.33','29.16','30.03','30.89','32.06','33','33.95','34.66','34.66'] ;
       var corrected_thighGirth_male = ['37.07','38.14','39.3','40.59','42.06','43.22','44.39','45.4','46.52','47.59','48.79','50.26','51.96','53.52','54.89','54.89'] ; 
     }
     else if(document.getElementById("age_range").value == "60-69")
     {
       var sfTotal_male = ['17.22','18.86','24.26','27.95','33','37.29','40.73','44.46','48.32','52.97','58.89','69.38','79.36','91.1','95.72','95.72'] ;
       var tricep_male = ['4.38','4.91','5.87','7.05','8.63','9.83','10.89','11.82','12.98','14.68','16.91','21.95','25.45','30.21','32.78','32.78'] ;
       var subscapular_male = ['6.68','7.57','9.58','11.63','14.33','16.48','18.28','20.31','22.27','24.44','27.46','31.36','35.1','38.02','39.81','39.81'] ;
       var thigh_male = ['4.44','5.11','5.83','6.62','7.91','9.12','10.34','11.74','13.06','15.14','18.08','22.71','27.76','34.61','38.22','38.22'] ;
       var mass_male = ['50.18','53.48','58.19','62.14','67.41','71.2','74.84','77.7','80.76','84.4','88.19','94.62','99.67','108.15','117.05','117.05'] ;
       var bmi_male = ['17.8','18.86','20.5','22.4','23.7','24.9','26.2','27.1','28.21','29.37','30.83','32.8','34.4','37.68','39.6','39.6'] ;
       var whr_male = ['0.86','0.876','0.91','0.93','0.96','0.98','1','1.01','1.03','1.04','1.06','1.09','1.11','1.14','1.175','1.175'] ;
       var relaxArmGirth_male = ['23.68','25.22','26.29','27.89','29.42','30.47','31.35','32.27','33.1','34.02','35.14','36.57','37.82','39.39','41.22','41.22'] ;
       var waistGirth_male = ['73.16','75.27','79.76','85.19','90.81','93.96','96.69','99.41','102.07','105.12','108.83','113.76','118.62','122.78','129.44','129.44'] ;
       var hipGirth_male = ['80.79','83.3','85.88','89.17','91.85','94.29','96.08','97.86','99.62','101.47','104.07','108.36','112.29','118.08','127.11','127.11'] ;
       var corrected_relaxArmGirth_male = ['21.48','22.09','23.78','24.8','25.94','26.73','27.36','27.99','28.66','29.46','30.27','31.47','32.48','33.58','34.54','34.54'] ;
       var corrected_thighGirth_male = ['35.17','36.17','38.5','39.85','41.5','42.71','43.71','44.57','45.56','46.53','47.77','49.34','51.11','52.56','53.84','53.84'] ; 
     }
     else if(document.getElementById("age_range").value == "70+")
     {
       var sfTotal_male = ['16.7','18.35','21.21','25.22','30.95','34.96','38.35','42.08','45.57','50.76','56.64','65.75','74.96','81.98','89.78','89.78'] ;
       var tricep_male = ['4.26','4.76','5.57','6.69','8.09','9.13','10.42','11.46','12.47','13.77','15.79','19.39','23.36','28.68','31.46','31.46'] ;
       var subscapular_male = ['6.12','7.1','8.07','9.64','12.19','14.27','16','17.72','19.46','21.4','23.71','27.35','30.82','34.59','36.77','36.77'] ;
       var thigh_male = ['4.34','4.68','5.46','6.52','7.96','9.21','10.52','11.95','13.58','16.02','18.91','23.99','28.77','34.3','37.24','37.24'] ;
       var mass_male = ['49.63','51.43','55.72','59.67','64.55','68.28','71.29','74.01','77.02','80.47','84.6','90.54','97.27','104.88','112.18','112.18'] ;
       var bmi_male = ['17.2','18','19.49','20.7','22.4','23.7','24.7','25.7','26.6','27.7','29.1','31.1','33.4','36.2','37.9','37.9'] ;
       var whr_male = ['0.846','0.87','0.89','0.92','0.95','0.97','0.99','1','1.02','1.03','1.05','1.08','1.11','1.14','1.18','1.18'] ;
       var relaxArmGirth_male = ['22.49','23.34','24.79','26.1','27.62','28.73','29.69','30.42','31.22','32.15','33.23','35.06','36.47','38.13','40.13','40.13'] ;
       var waistGirth_male = ['74.06','75.8','79.92','83.67','89.36','92.59','95.5','97.72','101.01','103.7','107.1','111.83','117.58','122.45','127.57','127.57'] ;
       var hipGirth_male = ['83.05','84.79','87.41','89.26','91.68','93.81','95.46','97.15','99.29','101.24','103.74','108.33','111.68','118.51','125.39','125.39'] ;
       var corrected_relaxArmGirth_male = ['19.97','21.19','22.18','23.28','24.48','25.27','25.94','26.54','27.19','27.89','28.81','29.9','30.88','32','32.91','32.91'] ;
       var corrected_thighGirth_male = ['33.81','35.01','36.78','38.07','39.61','40.68','41.63','42.49','43.46','44.44','45.64','47.39','48.93','50.7','51.88','51.88'] ; 
     }
     
     var rank_arr = ['0','1','2','5','10','20','30','40','50','60','70','80','90','95','98','99','100'] ;       
     var rankAnthro = new Array() ;
     var rankPlot = new Array() ;
     var diff ;
    
     if(Corrected_RelArmGirthAdj < corrected_relaxArmGirth_male[0])
    { 
      var rank_Corrected_RelArmG = 1 ;   
      rankAnthro.push(rank_Corrected_RelArmG) ;
    }  
    else if(Corrected_RelArmGirthAdj > corrected_relaxArmGirth_male[15])
    { 
      var rank_Corrected_RelArmG = 16 ;   
      rankAnthro.push(rank_Corrected_RelArmG) ;
    }         
    else if(Corrected_RelArmGirthAdj >= corrected_relaxArmGirth_male[0] && Corrected_RelArmGirthAdj <= corrected_relaxArmGirth_male[15])
    {  
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(Corrected_RelArmGirthAdj >= corrected_relaxArmGirth_male[i] && Corrected_RelArmGirthAdj < corrected_relaxArmGirth_male[i+1])
            {
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (corrected_relaxArmGirth_male[i+1] - corrected_relaxArmGirth_male[i]) / range_difference ; // each % point            
              var difference = Corrected_RelArmGirthAdj - corrected_relaxArmGirth_male[i] ; // sum greater than 0% value             
              var rank_difference = difference / percent_point ; 
              var rank_Corrected_RelArmG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_Corrected_RelArmG * 10)/10 ; rankAnthro.push(rankFinal);  
              var decimals = +rank_Corrected_RelArmG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Corrected_RelArmG - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ; 
              rank_Corrected_RelArmG = rank + diff ;                           
            }
        }
     }
     
     
    if(Corrected_RelArmGirthAdj < corrected_relaxArmGirth_male[0])
    { 
      var rank_Corrected_RelArmG = 1 ;   
      rankAnthro.push(rank_Corrected_RelArmG) ;
    }  
    else if(Corrected_RelArmGirthAdj > corrected_relaxArmGirth_male[15])
    { 
      var rank_Corrected_RelArmG = 16 ;   
      rankAnthro.push(rank_Corrected_RelArmG) ;
    }         
    else if(Corrected_RelArmGirthAdj >= corrected_relaxArmGirth_male[0] && Corrected_RelArmGirthAdj <= corrected_relaxArmGirth_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(RelArmGirthAdj >= relaxArmGirth_male[i] && RelArmGirthAdj < relaxArmGirth_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (relaxArmGirth_male[i+1] - relaxArmGirth_male[i]) / range_difference ; // each % point
              var difference = RelArmGirthAdj - relaxArmGirth_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_RelArmG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_RelArmG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_RelArmG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_RelArmG - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;       
              rank_RelArmG = rank + diff ;                        
            }
        }
     }
     
    
    if(WHR < whr_male[0])
    {     
      var rank_WHR = 1 ;  
      rankAnthro.push(rank_WHR); 
    }  
    else if(WHR > whr_male[15])
    {
      var rank_WHR = 16 ;    
      rankAnthro.push(rank_WHR); 
    }
    else if(WHR >= whr_male[0] && WHR <= whr_male[15])
    {          
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(WHR >= whr_male[i] && WHR < whr_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (whr_male[i+1] - whr_male[i]) / range_difference ; // each % point
              var difference = WHR - whr_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_WHR = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_WHR * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_WHR.toString().replace(/^[^\.]+/,'0') ; 
              var rank = rank_WHR - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_WHR = rank + diff ;                            
            }  
       } 
     }
     
     
    if(HipGirthAdj < hipGirth_male[0])
    {     
      var rank_HipG = 1 ;  
      rankAnthro.push(rank_HipG); 
    }  
    else if(HipGirthAdj > hipGirth_male[15])
    {
      var rank_HipG = 16 ;    
      rankAnthro.push(rank_HipG); 
    }
    else if(HipGirthAdj >= hipGirth_male[0] && HipGirthAdj <= hipGirth_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(HipGirthAdj >= hipGirth_male[i] && HipGirthAdj < hipGirth_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (hipGirth_male[i+1] - hipGirth_male[i]) / range_difference ; // each % point
              var difference = HipGirthAdj - hipGirth_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ; 
              var rank_HipG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_HipG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_HipG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_HipG - decimals ;     
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_HipG = rank + diff ;                              
            }
         }
     }  
     
     
    if(WaistGirthAdj < waistGirth_male[0])
    {     
      var rank_WaistG = 1 ;  
      rankAnthro.push(rank_WaistG); 
    }  
    else if(WaistGirthAdj > waistGirth_male[15])
    {
      var rank_WaistG = 16 ;    
      rankAnthro.push(rank_WaistG); 
    }
    else if(WaistGirthAdj >= waistGirth_male[0] && WaistGirthAdj <= waistGirth_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(WaistGirthAdj >= waistGirth_male[i] && WaistGirthAdj < waistGirth_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (waistGirth_male[i+1] - waistGirth_male[i]) / range_difference ; // each % point
              var difference = WaistGirthAdj - waistGirth_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_WaistG = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_WaistG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_WaistG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_WaistG - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;     
              rank_WaistG = rank + diff ;                        
            }
        }
     }   
     
	 
	 
	 
    if(ThighAdj < thigh_male[0])
    {     
      var rank_Thigh = 1 ;  
      rankAnthro.push(rank_Thigh); 
    }  
    else if(ThighAdj > thigh_male[15])
    {
      var rank_Thigh = 16 ;    
      rankAnthro.push(rank_Thigh); 
    }
    else if(ThighAdj >= thigh_male[0] && ThighAdj <= thigh_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(ThighAdj >= thigh_male[i] && ThighAdj < thigh_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (thigh_male[i+1] - thigh_male[i]) / range_difference ; // each % point
              var difference = ThighAdj - thigh_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Thigh = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_Thigh * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_Thigh.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Thigh - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Thigh = rank + diff ;                     
            }
        }
     }   
     
     
    if(SubScapAdj < subscapular_male[0])
    {     
      var rank_SubScap = 1 ;  
      rankAnthro.push(rank_SubScap); 
    }  
    else if(SubScapAdj > subscapular_male[15])
    {
      var rank_SubScap = 16 ;    
      rankAnthro.push(rank_SubScap); 
    }
    else if(SubScapAdj >= subscapular_male[0] && SubScapAdj <= subscapular_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(SubScapAdj >= subscapular_male[i] && SubScapAdj < subscapular_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (subscapular_male[i+1] - subscapular_male[i]) / range_difference ; // each % point
              var difference = SubScapAdj - subscapular_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_SubScap = parseInt(lower_rank) + rank_difference ;
              var rankFinal = Math.round(rank_SubScap * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_SubScap.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_SubScap - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_SubScap = rank + diff ;              
            }
        }    
     }   
     
     
    if(TriAdj < tricep_male[0])
    {     
      var rank_Tricep = 1 ;  
      rankAnthro.push(rank_Tricep); 
    }  
    else if(TriAdj > tricep_male[15])
    {
      var rank_Tricep = 16 ;    
      rankAnthro.push(rank_Tricep); 
    }
    else if(TriAdj >= tricep_male[0] && TriAdj <= tricep_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(TriAdj >= tricep_male[i] && TriAdj < tricep_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (tricep_male[i+1] - tricep_male[i]) / range_difference ; // each % point
              var difference = TriAdj - tricep_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Tricep = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_Tricep * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_Tricep.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Tricep - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Tricep = rank + diff ;                            
            }
       }         
     }   
     
     
    if(SFTotAdj < sfTotal_male[0])
    {     
      var rank_SFTot = 1 ;  
      rankAnthro.push(rank_SFTot); 
    }  
    else if(SFTotAdj > sfTotal_male[15])
    {
      var rank_SFTot = 16 ;    
      rankAnthro.push(rank_SFTot); 
    }
    else if(SFTotAdj >= sfTotal_male[0] && SFTotAdj <= sfTotal_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(SFTotAdj >= sfTotal_male[i] && SFTotAdj < sfTotal_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (sfTotal_male[i+1] - sfTotal_male[i]) / range_difference ; // each % point
              var difference = SFTotAdj - sfTotal_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_SFTot = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_SFTot * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_SFTot.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_SFTot - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_SFTot = rank + diff ;                         
            }
        }         
     }   
     
     
    if(BMI < bmi_male[0])
    {     
      var rank_BMI = 1 ;  
      rankAnthro.push(rank_BMI); 
    }  
    else if(BMI > bmi_male[15])
    {
      var rank_BMI = 16 ;    
      rankAnthro.push(rank_BMI); 
    }
    else if(BMI >= bmi_male[0] && BMI <= bmi_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(BMI >= bmi_male[i] && BMI < bmi_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (bmi_male[i+1] - bmi_male[i]) / range_difference ; // each % point
              var difference = BMI - bmi_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_BMI = parseInt(lower_rank) + rank_difference ;   
              var rankFinal = Math.round(rank_BMI * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_BMI.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_BMI - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_BMI = rank + diff ;                          
            }
        }          
     }   
     
     
    if(MassAdj < mass_male[0])
    {     
      var rank_Mass = 1 ;  
      rankAnthro.push(rank_Mass); 
    }  
    else if(MassAdj > mass_male[15])
    {
      var rank_Mass = 16 ;    
      rankAnthro.push(rank_Mass); 
    }
    else if(MassAdj >= mass_male[0] && MassAdj <= mass_male[15])
    {
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(MassAdj >= mass_male[i] && MassAdj < mass_male[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (mass_male[i+1] - mass_male[i]) / range_difference ; // each % point
              var difference = MassAdj - mass_male[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Mass = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_Mass * 10)/10 ;  rankAnthro.push(rankFinal); 
              var decimals = +rank_Mass.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Mass - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Mass = rank + diff ;                  
            }
        }           
     } 
     
       
    var Corrected_RelArmGirth_Anthro = Math.round(rank_Corrected_RelArmG * 10)/10 ; rankPlot.push(Corrected_RelArmGirth_Anthro); 
    var relArmG_Anthro = Math.round(rank_RelArmG * 10)/10 ; rankPlot.push(relArmG_Anthro); 
    var WHR_Anthro = Math.round(rank_WHR * 10)/10 ; rankPlot.push(WHR_Anthro);  
    var hipG_Anthro = Math.round(rank_HipG * 10)/10 ; rankPlot.push(hipG_Anthro);   
    var waistG_Anthro = Math.round(rank_WaistG * 10)/10 ; rankPlot.push(waistG_Anthro);   
    var thigh_Anthro = Math.round(rank_Thigh * 10)/10 ; rankPlot.push(thigh_Anthro);
    var subscapular_Anthro = Math.round(rank_SubScap * 10)/10 ; rankPlot.push(subscapular_Anthro);
    var triceps_Anthro = Math.round(rank_Tricep * 10)/10 ; rankPlot.push(triceps_Anthro);
    var SFTot_Anthro = Math.round(rank_SFTot * 10)/10 ; rankPlot.push(SFTot_Anthro);
    var BMI_Anthro = Math.round(rank_BMI * 10)/10 ; rankPlot.push(BMI_Anthro);
    var mass_Anthro = Math.round(rank_Mass * 10)/10 ; rankPlot.push(mass_Anthro);
    
    anthroList = rankAnthro ;
    plotList = rankPlot ;
    
    /*
     alert(Corrected_RelArmGirth_Anthro);
     alert(relArmG_Anthro);
     alert(WHR_Anthro);
     alert(hipG_Anthro);
     alert(waistG_Anthro);
     alert(thigh_Anthro);
     alert(subscapular_Anthro);
     alert(triceps_Anthro);
     alert(SFTot_Anthro);
     alert(BMI_Anthro);
     alert(mass_Anthro); */
     
     
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[Corrected_RelArmGirth_Anthro,2],[relArmG_Anthro,3],[WHR_Anthro,4],[hipG_Anthro,5],[waistG_Anthro,6],[thigh_Anthro,7],[subscapular_Anthro,8],[triceps_Anthro,9],[SFTot_Anthro,10],[BMI_Anthro,11],[mass_Anthro,12]];   
    var plot1 = $.jqplot('chartdiv', [chartData1], {
        seriesColors: ["#f17674"],        
        axes: {
        xaxis: {     
          label: 'Percentile Band',
          ticks: [[0,'0'],[1,'1'],[2,'2'],[3,'5'],[4,'10'],[5,'20'],[6,'30'],[7,'40'],[8,'50'],[9,'60'],[10,'70'],[11,'80'],[12,'90'],[13,'95'],[14,'98'],[15,'99'],[16,'100']] ,  
          tickOptions: {                        
             showMark: false           
          },
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer  
        },        
        yaxis: { 
                ticks: [[0],[1,'thigh girth (corrected)'],[2,'relaxed arm girth (corrected)'],[3,'relaxed arm girth'],[4,'waist : hip ratio'],[5,'hip girth'],[6,'waist girth'],[7,'thigh skinfold'],[8,'subscapular skinfold'],[9,'triceps skinfold'],[10,'Σ 3 skinfolds'],[11,'BMI'],[12,'body mass'],[13]],  
                tickOptions: {
                    showMark: false,
                    formatString: '%s' ,
                    angle: 30
                   }               
              }
        },
        grid:{           
            shadow:false,
            drawBorder:false,
            background: '#f2f2f2',
            shadowColor:'transparent'
        }, 
        //seriesColors: ["#faffbd"], 
        seriesColors: ["#3fc986"],
        seriesDefaults: {   
                     showLine:false,                     
                     showMarker: true,
                     markerRenderer: $.jqplot.MarkerRenderer,
                     markerOptions: {                                                                     
                        size: 22
                     },
                     shadow: false                    
                  },
        highlighter: {
                show: true,
                sizeAdjust: 3,
                tooltipContentEditor:tooltipContent             
            } 
         /* highlighter: {                
                sizeAdjust: 3,          
                tooltipAxes: 'x',
                //tooltipFormatString: '%.1f',
                //tooltipContentEditor:tooltipContentEditor
                formatString: "<div style='background:transparent; border:1px #ddd solid; width:100px; height:20px'>Rank% : %.1f</div>" 
             } */  
      });
      
    }    
   else if(document.getElementById("gender").value == "Female")
   {     
     if(document.getElementById("age_range").value == "18-29")
     {
       var sfTotal_female = ['27.36','30.25','34.57','39.6','45.95','52.7','58.81','64.37','70.36','78.02','87.12','98.23','107.81','117.21','122.81','122.81'] ;
       var tricep_female = ['7.54','8.79','9.97','11.85','14.12','16.49','18.75','20.94','23.48','26.64','30.58','35.79','38.99','41.69','43.83','43.83'] ;
       var subscapular_female = ['7.07','7.92','9.86','11.42','15.39','19.62','23.52','26.68','31.27','35.36','40.11','44.91','49.32','53.24','55.43','55.43'] ;
       var thigh_female = ['10.23','11.44','13.33','15.4','18.89','21.41','23.74','26.38','29.41','32.81','36.74','40.25','43.25','45.6','46.82','46.82'] ;
       var mass_female = ['50.56','52.15','54.92','57.96','62.42','65.98','69.51','73.12','77.38','83.3','90.96','102.41','114.48','129.34','138.93','138.93'] ;
       var bmi_female = ['16.9','17.5','18.3','19.2','20.5','21.6','22.7','23.8','25.29','27.2','29.7','33.5','37.23','42.26','44.91','44.91'] ;
       var whr_female = ['0.7','0.72','0.73','0.75','0.77','0.79','0.81','0.82','0.84','0.87','0.9','0.94','0.97','1.00','1.03','1.03'] ;
       var relaxArmGirth_female = ['22.76','23.72','24.18','25.27','26.71','27.79','28.83','29.88','31.19','32.91','34.84','38.05','40.84','44.3','47.32','47.32'] ;
       var waistGirth_female = ['65.49','66.51','69.12','71.25','74.82','77.67','80.9','84.47','88.99','94.43','100.64','108.76','117.32','127.11','133.55','133.55'] ;
       var hipGirth_female = ['84.7','86.67','89.01','91.66','94.57','97.26','99.73','102.29','105.19','109.06','114.39','122.24','128.2','139.85','144.93','144.93'] ;
       var corrected_relaxArmGirth_female = ['18.61','19.2','19.77','20.45','21.37','22.06','22.61','23.2','23.89','24.57','25.53','26.97','28.33','29.85','30.94','30.94'] ;
       var corrected_thighGirth_female = ['35.27','36.13','37.47','38.72','40','41.29','42.21','43.21','44.35','45.53','46.8','48.83','50.54','52.96','54.94','54.94'] ; 
     }
     else if(document.getElementById("age_range").value == "30-39")
     {
       var sfTotal_female = ['27.57','31.49','37.3','43.51','52.86','61.47','67.79','74.88','81.58','88.85','96.84','107.45','117.15','124.19','128.67','128.67'] ;
       var tricep_female = ['7.67','8.74','11.08','13.21','16.61','19.84','22.54','25.38','28.82','32.27','35.62','39.61','42.26','44.67','46.17','46.17'] ;
       var subscapular_female = ['8.61','9.6','11.69','14.88','19.75','23.21','26.49','29.37','32.89','36.63','40.51','45.4','49','53.55','56','56'] ;
       var thigh_female = ['9.36','11.7','14.14','17.26','21.36','24.67','27.25','30.23','33.31','36.72','39.35','42.68','44.7','46.54','47.7','47.7'] ;
       var mass_female = ['51.96','54.61','57.61','60.55','65.8','71.32','76.32','81.16','86.94','93.56','102.87','114.73','127.88','142.53','151.37','151.37'] ;
       var bmi_female = ['17.71','18.36','19.3','20.2','21.8','23.4','24.8','26.4','28.17','30.6','33.5','37.68','41.3','46.44','50.29','50.29'] ;
       var whr_female = ['0.7','0.72','0.75','0.77','0.8','0.82','0.84','0.85','0.88','0.9','0.92','0.96','0.99','1.02','1.05','1.05'] ;
       var relaxArmGirth_female = ['23.34','24.23','25.56','26.55','28.3','29.73','31.2','32.56','34.19','36.07','38.4','41.45','43.91','47.43','50.14','50.14'] ;
       var waistGirth_female = ['67.37','69.01','71.66','74.81','79.83','83.98','87.95','92.69','97.06','102.85','109.39','118.59','125.99','135.08','140.99','140.99'] ;
       var hipGirth_female = ['87.03','88.51','91','93.95','97.74','101.35','104.28','107.2','110.93','115.27','121.28','129.66','137.97','147.96','153.46','153.46'] ;
       var corrected_relaxArmGirth_female = ['19.11','19.56','20.38','21.15','22.11','22.84','23.49','24.33','25.1','25.88','27.01','28.65','30.22','32.26','33.01','33.01'] ;
       var corrected_thighGirth_female = ['34.84','35.72','37.46','38.57','40.19','41.2','42.23','43.4','44.35','45.66','47.04','49.73','51.83','54.79','55.67','55.67'] ; 
     }
     else if(document.getElementById("age_range").value == "40-49")
     {
       var sfTotal_female = ['29.31','35.28','44.75','52.98','63.04','71.87','77.97','83.78','90.15','96.05','103.68','112.89','118.29','124.14','130.18','130.18'] ;
       var tricep_female = ['8.97','10.64','13.26','16.57','20.49','23.31','25.62','27.82','30.66','33.34','35.88','39.16','41.67','43.8','45.93','45.93'] ;
       var subscapular_female = ['6.68','8.84','11.8','15.05','19.49','23.49','27.2','30.01','33.25','37.07','40.69','45.9','48.77','53.9','57.51','57.51'] ;
       var thigh_female = ['9.28','12.04','14.7','19.42','24.08','28.04','31.47','34.26','36.87','39.33','41.34','43.69','45.34','47.53','48.4','48.4'] ;
       var mass_female = ['53.17','56.71','60.02','64.2','69.93','74.92','79.36','85.26','90.23','96.1','102.67','116.15','128.84','147.82','156.72','156.72'] ;
       var bmi_female = ['18','18.58','20','21.2','23.1','24.6','26.1','27.7','29.4','31.1','33.5','37.7','41.1','48.2','51.68','51.68'] ;
       var whr_female = ['0.71','0.74','0.76','0.79','0.81','0.84','0.86','0.88','0.9','0.92','0.95','0.98','1.01','1.04','1.065','1.065'] ;
       var relaxArmGirth_female = ['24.12','25.06','26.51','27.84','29.59','31.01','32.31','33.83','35.14','36.6','38.6','41.32','44.58','48.34','52.09','52.09'] ;
       var waistGirth_female = ['69.87','71.36','75.15','78.81','84.31','89.1','93.06','97.2','100.58','105.94','111.36','119.62','127.59','137.74','143.64','143.64'] ;
       var hipGirth_female = ['88.28','90.48','93.51','96.12','100.48','103.69','106.55','109.6','112.6','116.28','121.31','129.63','139.08','151.84','157.79','157.79'] ;
       var corrected_relaxArmGirth_female = ['19.11','19.65','20.43','21.33','22.34','23.13','23.88','24.54','25.28','26.33','27.45','29.04','30.76','33.25','34.14','34.14'] ;
       var corrected_thighGirth_female = ['33.46','34.84','36.12','37.47','39.22','40.61','41.86','43.11','44.11','45.41','46.97','49.08','51.13','53.68','55.45','55.45'] ; 
     }
     else if(document.getElementById("age_range").value == "50-59")
     { 
       var sfTotal_female = ['29.17','33.77','43.1','51.04','64.01','70.9','77.96','83.28','89.89','95.18','103.84','112.4','119.71','128.41','132.72','132.72'] ;
       var tricep_female = ['8.09','11.19','14.22','16.7','20.68','23.43','26.19','28.63','31.23','33.41','36.15','39.61','42.56','44.81','46.09','46.09'] ;
       var subscapular_female = ['6.64','8.89','11.3','14.49','18.77','22.5','25.88','29.06','32.06','35.58','39.65','44.73','48.92','54.37','57.1','57.1'] ;
       var thigh_female = ['9.21','10.42','13.38','18.19','23.29','27.69','30.93','33.49','36.39','38.49','41.18','43.85','45.26','47.17','48.45','48.45'] ;
       var mass_female = ['52.76','55.17','60.38','65.4','71.87','76.57','80.98','86.44','92','99.17','106.35','118','127.72','138.83','146.47','146.47'] ;
       var bmi_female = ['17.25','18.25','19.95','21.5','23.4','25','26.5','28.3','29.9','31.8','34.4','37.9','41.4','45.76','48.86','48.86'] ;
       var whr_female = ['0.74','0.76','0.78','0.81','0.84','0.86','0.88','0.9','0.92','0.95','0.95','1.01','1.034','1.07','1.1','1.1'] ;
       var relaxArmGirth_female = ['23.8','24.95','26.87','27.97','30.07','31.01','32.96','34.41','35.79','37.45','39.36','42.4','46.11','49.23','50.9','50.9'] ;
       var waistGirth_female = ['71.93','74.36','77.42','81.59','87.58','92.4','96.63','100.69','105.12','110.09','115.29','122.3','129.05','139.74','142.48','142.48'] ;
       var hipGirth_female = ['86.83','89.16','92.85','96.54','100.62','103.76','106.69','109.85','113.2','117.81','123','132.32','139.72','147.44','152.8','152.8'] ;
       var corrected_relaxArmGirth_female = ['19.26','19.74','20.71','21.6','22.64','23.4','24.21','25.11','26.05','26.97','28.22','29.93','31.8','34.71','36.33','36.33'] ;
       var corrected_thighGirth_female = ['32.53','34.02','35.5','37.02','38.57','40.01','41.13','42.24','43.43','44.44','46.31','48.57','50.14','53.29','55.69','55.69'] ; 
     }
     else if(document.getElementById("age_range").value == "60-69")
     {
       var sfTotal_female = ['26.71','33.95','44.45','51.96','59.73','67.25','74.02','80.26','86.59','93.27','100.25','109.56','115.45','122.25','128.47','128.47'] ;
       var tricep_female = ['8.26','10.47','13.22','16.07','19.04','21.51','23.99','26.27','28.46','31.13','34.32','37.61','40.46','43.74','45.81','45.81'] ;
       var subscapular_female = ['6.25','6.75','8.81','10.71','13.86','17.13','20.07','23.11','26.3','29.46','33.37','39.62','44.41','49.74','52.32','52.32'] ;
       var thigh_female = ['7.37','9.15','12.78','16.3','21.64','25.63','29.71','32.41','34.88','37.85','40.42','43.9','45.67','48.03','48.92','48.92'] ;
       var mass_female = ['52.39','56.16','61.68','65.81','72.08','77.65','81.62','85.53','91.4','96.6','103.27','115.46','124.1','135.17','146.81','146.81'] ;
       var bmi_female = ['17.4','18.5','20.2','21.5','23.4','25','26.1','27.6','29.2','30.7','32.9','36.3','39.5','43.9','46.7','46.7'] ;
       var whr_female = ['0.75','0.77','0.79','0.82','0.86','0.89','0.91','0.93','0.95','0.97','0.99','1.03','1.06','1.1','1.13','1.13'] ;
       var relaxArmGirth_female = ['23.61','24.94','26.56','28.28','30.21','31.8','33.06','34.25','35.51','37.15','39.02','41.95','44.43','47.44','51.57','51.57'] ;
       var waistGirth_female = ['72.76','74.86','79.59','84.76','90.93','95.17','99.24','103.37','106.72','111.2','116.11','124.84','130.45','138.21','142.53','142.53'] ;
       var hipGirth_female = ['88.21','90.92','93.99','96.44','100.67','103.24','106.34','108.86','112.31','116.16','122.06','131.91','140.3','151.01','158.74','158.74'] ;
       var corrected_relaxArmGirth_female = ['19.59','20.28','21.13','22.01','23.21','24.13','24.99','25.84','26.54','27.38','28.49','30.41','32.2','34.58','36.37','36.37'] ;
       var corrected_thighGirth_female = ['32.52','33.76','35.58','36.86','38.53','39.84','41.01','42.13','43.23','44.34','45.91','48.18','50.32','52.66','55.4','55.4'] ; 
     }
     else if(document.getElementById("age_range").value == "70+")
     {
       var sfTotal_female = ['25.18','29.13','36.5','42.76','52.11','59.57','65.4','71.32','77.36','84.24','92.08','100.93','108.45','114.11','117.67','117.67'] ;
       var tricep_female = ['7.56','8.22','9.35','10.97','13.09','15.52','18.13','21.19','24.98','29.05','33.74','40.32','44.86','49.35','54.38','54.38'] ;
       var subscapular_female = ['6.13','7.61','9.76','12.03','14.92','17.4','19.72','21.88','24.34','27.01','29.99','33.84','37.19','40.96','42.67','42.67'] ;
       var thigh_female = ['7.8','10.19','13.37','16.6','21.04','24.51','28.17','31.04','33.77','36.23','39.53','42.81','44.55','46.95','48.24','48.24'] ;
       var mass_female = ['50.74','55.26','60.13','64.1','68.62','73.57','78.33','82.22','86.03','90.59','96.18','106.43','116.13','128.73','137.66','137.66'] ;
       var bmi_female = ['16.13','17.66','19.2','20.4','22','23.4','24.8','26','27.2','28.63','30.4','33.4','36.4','40.24','43.17','43.17'] ;
       var whr_female = ['0.74','0.78','0.81','0.83','0.86','0.89','0.91','0.93','0.95','0.97','0.99','1.02','1.05','1.08','1.13','1.13'] ;
       var relaxArmGirth_female = ['21.4','22.48','24.85','26.33','28.36','29.85','31.11','32.3','33.67','35.01','36.55','39.29','41.89','45.1','47.15','47.15'] ;
       var waistGirth_female = ['72.76','76.86','80.26','83.91','89.28','94.29','98.31','102.03','105.43','109.35','113.57','119.67','125.18','131.54','138.55','138.55'] ;
       var hipGirth_female = ['87.93','90.79','93.74','96.15','99.74','102.52','105.26','108.05','111.14','114.24','118.78','125.85','132.38','141.19','147.04','147.04'] ;
       var corrected_relaxArmGirth_female = ['18.68','19.63','20.69','21.61','22.8','23.7','24.43','25.19','25.97','26.8','27.9','29.57','31.22','33.53','34.99','34.99'] ;
       var corrected_thighGirth_female = ['32.88','33.58','35.42','36.6','38.09','39.23','40.32','41.09','42.22','43.34','44.82','47.05','49.25','51.74','52.93','52.93'] ; 
     }
     
     var rank_arr = ['0','1','2','5','10','20','30','40','50','60','70','80','90','95','98','99','100'] ;
     var rankAnthro = new Array() ;
     var rankPlot = new Array() ;
     var diff ;
    
     if(Corrected_RelArmGirthAdj < corrected_relaxArmGirth_female[0])
    {     
      var rank_Corrected_RelArmG = 1 ;  
      rankAnthro.push(rank_Corrected_RelArmG); 
    }  
    else if(Corrected_RelArmGirthAdj > corrected_relaxArmGirth_female[15])
    {
      var rank_Corrected_RelArmG = 16 ;    
      rankAnthro.push(rank_Corrected_RelArmG); 
    }
    else if(Corrected_RelArmGirthAdj >= corrected_relaxArmGirth_female[0] && Corrected_RelArmGirthAdj <= corrected_relaxArmGirth_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(Corrected_RelArmGirthAdj >= corrected_relaxArmGirth_female[i] && Corrected_RelArmGirthAdj < corrected_relaxArmGirth_female[i+1])
            {
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (corrected_relaxArmGirth_female[i+1] - corrected_relaxArmGirth_female[i]) / range_difference ; // each % point            
              var difference = Corrected_RelArmGirthAdj - corrected_relaxArmGirth_female[i] ; // sum greater than 0% value             
              var rank_difference = difference / percent_point ;  
              var rank_Corrected_RelArmG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_Corrected_RelArmG * 10)/10 ; rankAnthro.push(rankFinal);  
              var decimals = +rank_Corrected_RelArmG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Corrected_RelArmG - decimals ;      
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Corrected_RelArmG = rank + diff ;                           
            }
        }     
     } 
     
     
    if(RelArmGirthAdj < relaxArmGirth_female[0])
    {     
      var rank_RelArmG = 1 ;  
      rankAnthro.push(rank_RelArmG); 
    }  
    else if(RelArmGirthAdj > relaxArmGirth_female[15])
    {
      var rank_RelArmG = 16 ;    
      rankAnthro.push(rank_RelArmG); 
    }
    else if(RelArmGirthAdj >= relaxArmGirth_female[0] && RelArmGirthAdj <= relaxArmGirth_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(RelArmGirthAdj >= relaxArmGirth_female[i] && RelArmGirthAdj < relaxArmGirth_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (relaxArmGirth_female[i+1] - relaxArmGirth_female[i]) / range_difference ; // each % point
              var difference = RelArmGirthAdj - relaxArmGirth_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_RelArmG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_RelArmG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_RelArmG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_RelArmG - decimals ; 
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_RelArmG = rank + diff ;                        
            }
        }
     }
     
     
    if(WHR < whr_female[0])
    {     
      var rank_WHR = 1 ;  
      rankAnthro.push(rank_WHR); 
    }  
    else if(WHR > whr_female[15])
    {
      var rank_WHR = 16 ;    
      rankAnthro.push(rank_WHR); 
    }
    else if(WHR >= whr_female[0] && WHR <= whr_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(WHR >= whr_female[i] && WHR < whr_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (whr_female[i+1] - whr_female[i]) / range_difference ; // each % point
              var difference = WHR - whr_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_WHR = parseInt(lower_rank) + rank_difference ;
              var rankFinal = Math.round(rank_WHR * 10)/10 ; rankAnthro.push(rankFinal) ; 
              var decimals = +rank_WHR.toString().replace(/^[^\.]+/,'0') ; 
              var rank = rank_WHR - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_WHR = rank + diff ;                            
            }
        }     
     }   
     
     
    if(HipGirthAdj < hipGirth_female[0])
    {     
      var rank_HipG = 1 ;  
      rankAnthro.push(rank_HipG); 
    }  
    else if(HipGirthAdj > hipGirth_female[15])
    {
      var rank_HipG = 16 ;    
      rankAnthro.push(rank_HipG); 
    }
    else if(HipGirthAdj >= hipGirth_female[0] && HipGirthAdj <= hipGirth_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(HipGirthAdj >= hipGirth_female[i] && HipGirthAdj < hipGirth_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (hipGirth_female[i+1] - hipGirth_female[i]) / range_difference ; // each % point
              var difference = HipGirthAdj - hipGirth_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ; 
              var rank_HipG = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_HipG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_HipG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_HipG - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_HipG = rank + diff ;                             
            }
        }
     }  
     
     
    if(WaistGirthAdj < waistGirth_female[0])
    {     
      var rank_WaistG = 1 ;  
      rankAnthro.push(rank_WaistG); 
    }  
    else if(WaistGirthAdj > waistGirth_female[15])
    {
      var rank_WaistG = 16 ;    
      rankAnthro.push(rank_WaistG); 
    }
    else if(WaistGirthAdj >= waistGirth_female[0] && WaistGirthAdj <= waistGirth_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(WaistGirthAdj >= waistGirth_female[i] && WaistGirthAdj < waistGirth_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (waistGirth_female[i+1] - waistGirth_female[i]) / range_difference ; // each % point
              var difference = WaistGirthAdj - waistGirth_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_WaistG = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_WaistG * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_WaistG.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_WaistG - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_WaistG = rank + diff ;                        
            }
        }
     }   
     
     
    if(ThighAdj < thigh_female[0])
    {     
      var rank_Thigh = 1 ;  
      rankAnthro.push(rank_Thigh); 
    }  
    else if(ThighAdj > thigh_female[15])
    {
      var rank_Thigh = 16 ;    
      rankAnthro.push(rank_Thigh); 
    }
    else if(ThighAdj >= thigh_female[0] && ThighAdj <= thigh_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(ThighAdj >= thigh_female[i] && ThighAdj < thigh_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (thigh_female[i+1] - thigh_female[i]) / range_difference ; // each % point
              var difference = ThighAdj - thigh_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Thigh = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_Thigh * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_Thigh.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Thigh - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Thigh = rank + diff ;                     
            }
        } 
     }   
     
     
    if(SubScapAdj < subscapular_female[0])
    {     
      var rank_SubScap = 1 ;  
      rankAnthro.push(rank_SubScap); 
    }  
    else if(SubScapAdj > subscapular_female[15])
    {
      var rank_SubScap = 16 ;    
      rankAnthro.push(rank_SubScap); 
    }
    else if(SubScapAdj >= subscapular_female[0] && SubScapAdj <= subscapular_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(SubScapAdj >= subscapular_female[i] && SubScapAdj < subscapular_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (subscapular_female[i+1] - subscapular_female[i]) / range_difference ; // each % point
              var difference = SubScapAdj - subscapular_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_SubScap = parseInt(lower_rank) + rank_difference ;
              var rankFinal = Math.round(rank_SubScap * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_SubScap.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_SubScap - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_SubScap = rank + diff ;              
            }
        }
     }   
     
     
    if(TriAdj < tricep_female[0])
    {     
      var rank_Tricep = 1 ;  
      rankAnthro.push(rank_Tricep); 
    }  
    else if(TriAdj > tricep_female[15])
    {
      var rank_Tricep = 16 ;    
      rankAnthro.push(rank_Tricep); 
    }
    else if(TriAdj >= tricep_female[0] && TriAdj <= tricep_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(TriAdj >= tricep_female[i] && TriAdj < tricep_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (tricep_female[i+1] - tricep_female[i]) / range_difference ; // each % point
              var difference = TriAdj - tricep_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Tricep = parseInt(lower_rank) + rank_difference ; 
              var rankFinal = Math.round(rank_Tricep * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_Tricep.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Tricep - decimals ;    
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Tricep = rank + diff ;                            
            }
        }    
     }   
     
     
    if(SFTotAdj < sfTotal_female[0])
    {     
      var rank_SFTot = 1 ;  
      rankAnthro.push(rank_SFTot); 
    }  
    else if(SFTotAdj > sfTotal_female[15])
    {
      var rank_SFTot = 16 ;    
      rankAnthro.push(rank_SFTot); 
    }
    else if(SFTotAdj >= sfTotal_female[0] && SFTotAdj <= sfTotal_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(SFTotAdj >= sfTotal_female[i] && SFTotAdj < sfTotal_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (sfTotal_female[i+1] - sfTotal_female[i]) / range_difference ; // each % point
              var difference = SFTotAdj - sfTotal_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_SFTot = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_SFTot * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_SFTot.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_SFTot - decimals ;  
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_SFTot = rank + diff ;                         
            }
        }    
     }   
     
     
    if(BMI < bmi_female[0])
    {     
      var rank_BMI = 1 ;  
      rankAnthro.push(rank_BMI); 
    }  
    else if(BMI > bmi_female[15])
    {
      var rank_BMI = 16 ;    
      rankAnthro.push(rank_BMI); 
    }
    else if(BMI >= bmi_female[0] && BMI <= bmi_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(BMI >= bmi_female[i] && BMI < bmi_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (bmi_female[i+1] - bmi_female[i]) / range_difference ; // each % point
              var difference = BMI - bmi_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_BMI = parseInt(lower_rank) + rank_difference ;   
              var rankFinal = Math.round(rank_BMI * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_BMI.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_BMI - decimals ;              
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_BMI = rank + diff ;                          
            }
        }  
     }   
     
     
    if(MassAdj < mass_female[0])
    {     
      var rank_Mass = 1 ;  
      rankAnthro.push(rank_Mass); 
    }  
    else if(MassAdj > mass_female[15])
    {
      var rank_Mass = 16 ;    
      rankAnthro.push(rank_Mass); 
    }
    else if(MassAdj >= mass_female[0] && MassAdj <= mass_female[15])
    { 
     for(var i=0; i < (rank_arr.length); i++)
     {
       if(MassAdj >= mass_female[i] && MassAdj < mass_female[i+1])
            { 
              var lower_rank = rank_arr[i] ; // lower limit rank 
              var upper_rank = rank_arr[i+1] ; // upper limit rank 
              var range_difference = upper_rank - lower_rank  ; // difference between lower limit rank and upper limit rank
              var percent_point = (mass_female[i+1] - mass_female[i]) / range_difference ; // each % point
              var difference = MassAdj - mass_female[i] ; // sum greater than 0% value
              var rank_difference = difference / percent_point ;
              var rank_Mass = parseInt(lower_rank) + rank_difference ;  
              var rankFinal = Math.round(rank_Mass * 10)/10 ; rankAnthro.push(rankFinal); 
              var decimals = +rank_Mass.toString().replace(/^[^\.]+/,'0'); 
              var rank = rank_Mass - decimals ;
              
              if(rank >= 0 && rank < 1)
              {
                rank = 0 ; 
                diff = (1 - rankFinal) ;              
              }
              else if(rank >= 1 && rank < 2)
              {
                rank = 1 ; 
                diff = (2 - rankFinal) ;
              }
              else if(rank >= 2 && rank < 5)
              {
                rank = 2 ;
                diff = (5 - rankFinal)/10 ;
              }
              else if(rank >= 5 && rank < 10)
              {
                rank = 3 ;
                diff = (10 - rankFinal)/10 ;
              }
              else if(rank >= 10 && rank < 20)
              {
                rank = 4 ;  
                diff = (20 - rankFinal)/10 ;
              }
              else if(rank >= 20 && rank < 30)
              {
                rank = 5 ;
                diff = (30 - rankFinal)/10 ;
              }
              else if(rank >= 30 && rank < 40)
              {
                rank = 6 ;
                diff = (40 - rankFinal)/10 ;
              }
              else if(rank >= 40 && rank < 50)
              {
                rank = 7 ;
                diff = (50 - rankFinal)/10 ;
              }
              else if(rank >= 50 && rank < 60)
              {
                rank = 8 ;
                diff = (60 - rankFinal)/10 ;
              }
              else if(rank >= 60 && rank < 70)
              {
                rank = 9 ;
                diff = (70 - rankFinal)/10 ;
              }
              else if(rank >= 70 && rank < 80)
              {
                rank = 10 ;                 
                diff = (80 - rankFinal)/10 ;
              }
              else if(rank >= 80 && rank < 90)
              {
                rank = 11 ;                 
                diff = (90 - rankFinal)/10 ;
              }
              else if(rank >= 90 && rank < 95)
              {
                rank = 12 ;                 
                diff = (95 - rankFinal)/10 ;
              }
              else if(rank >= 95 && rank < 98)
              {
                rank = 13 ;
                diff = (98 - rankFinal)/10 ;
              }
              else if(rank >= 98 && rank < 99)
              {
                rank = 14 ;                 
                diff = (99 - rankFinal)/10 ;
              }
              else if(rank >= 99 && rank < 100)
              {
                rank = 15 ;                 
                diff = (100 - rankFinal)/10 ;
              }
              else if(rank >= 100)
              {
                rank = 16 ;  
                delete window.diff ;
              }
              
              diff = 1 - diff ;
              rank_Mass = rank + diff ;                     
            }
        } 
     }        
   
   
    
    var Corrected_RelArmGirth_Anthro = Math.round(rank_Corrected_RelArmG * 10)/10 ; rankPlot.push(Corrected_RelArmGirth_Anthro); 
    var relArmG_Anthro = Math.round(rank_RelArmG * 10)/10 ; rankPlot.push(relArmG_Anthro); 
    var WHR_Anthro = Math.round(rank_WHR * 10)/10 ; rankPlot.push(WHR_Anthro);  
    var hipG_Anthro = Math.round(rank_HipG * 10)/10 ; rankPlot.push(hipG_Anthro);   
    var waistG_Anthro = Math.round(rank_WaistG * 10)/10 ; rankPlot.push(waistG_Anthro);   
    var thigh_Anthro = Math.round(rank_Thigh * 10)/10 ; rankPlot.push(thigh_Anthro);
    var subscapular_Anthro = Math.round(rank_SubScap * 10)/10 ; rankPlot.push(subscapular_Anthro);
    var triceps_Anthro = Math.round(rank_Tricep * 10)/10 ; rankPlot.push(triceps_Anthro);
    var SFTot_Anthro = Math.round(rank_SFTot * 10)/10 ; rankPlot.push(SFTot_Anthro);
    var BMI_Anthro = Math.round(rank_BMI * 10)/10 ; rankPlot.push(BMI_Anthro);
    var mass_Anthro = Math.round(rank_Mass * 10)/10 ; rankPlot.push(mass_Anthro);
    
    anthroList = rankAnthro ;
    plotList = rankPlot ;
    
    /* alert(Corrected_ThighGirth_Anthro);
     alert(Corrected_RelArmGirth_Anthro);
     alert(relArmG_Anthro);
     alert(WHR_Anthro);
     alert(hipG_Anthro);
     alert(waistG_Anthro);
     alert(thigh_Anthro);
     alert(subscapular_Anthro);
     alert(triceps_Anthro);
     alert(SFTot_Anthro);
     alert(BMI_Anthro);
     alert(mass_Anthro); */
     
     
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[Corrected_RelArmGirth_Anthro,2],[relArmG_Anthro,3],[WHR_Anthro,4],[hipG_Anthro,5],[waistG_Anthro,6],[thigh_Anthro,7],[subscapular_Anthro,8],[triceps_Anthro,9],[SFTot_Anthro,10],[BMI_Anthro,11],[mass_Anthro,12]];   
    var plot1 = $.jqplot('chartdiv', [chartData1], {
        seriesColors: ["#f17674"],        
        axes: {
        xaxis: {     
          label: 'Percentile Band',
          ticks: [[0,'0'],[1,'1'],[2,'2'],[3,'5'],[4,'10'],[5,'20'],[6,'30'],[7,'40'],[8,'50'],[9,'60'],[10,'70'],[11,'80'],[12,'90'],[13,'95'],[14,'98'],[15,'99'],[16,'100']] ,  
          tickOptions: {                        
             showMark: false           
          },
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer  
        },        
        yaxis: { 
                ticks: [[0],[1,'thigh girth (corrected)'],[2,'relaxed arm girth (corrected)'],[3,'relaxed arm girth'],[4,'waist : hip ratio'],[5,'hip girth'],[6,'waist girth'],[7,'thigh skinfold'],[8,'subscapular skinfold'],[9,'triceps skinfold'],[10,'Σ 3 skinfolds'],[11,'BMI'],[12,'body mass'],[13]],  
                tickOptions: {
                    showMark: false,
                    formatString: '%s' ,
                    angle: 30
                   }               
              }
        },
        grid:{           
            shadow:false,
            drawBorder:false,
            background: '#f2f2f2',
            shadowColor:'transparent'
        }, 
        //seriesColors: ["#faffbd"], 
        seriesColors: ["#3fc986"],
        seriesDefaults: {   
                     showLine:false,                     
                     showMarker: true,
                     markerRenderer: $.jqplot.MarkerRenderer,
                     markerOptions: {                                                                     
                        size: 22
                     },
                     shadow: false                    
                  },
         highlighter: {
                show: true,
                sizeAdjust: 3,
                tooltipContentEditor:tooltipContent             
            } 
         /* highlighter: {                
                sizeAdjust: 3,          
                tooltipAxes: 'x',
                //tooltipFormatString: '%.1f',
                //tooltipContentEditor:tooltipContentEditor
                formatString: "<div style='background:transparent; border:1px #ddd solid; width:100px; height:20px'>Rank% : %.1f</div>" 
             } */    
      });
      
  }    
    
}); 
        
        function tooltipContent(str, seriesIndex, pointIndex, plot)
        {   
            var rank = plot.series[seriesIndex].data[pointIndex][0];    
            
            var rankAnthro = anthroList ;   
            var rankPlot = plotList ;   
           
            
            var length = rankAnthro.length ; var pos ;           
            for(var i=0; i < length; i++)
            {
               if(rankPlot[i] == rank)
               {
                  pos = i ; break ;
               }
            }
                            
            var html = "<div style='background:transparent; border:1px #ddd solid; width:50px; height:20px'>" ;
            html += rankAnthro[pos] ;
            html += " %</div>" ;
            rank = "" ;
            return html;
        }
      
</script>
</body>
</html>
