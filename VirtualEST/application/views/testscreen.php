<?php

#pragma mark - Interpolation calculation

-(void)getBenchPressValue:(float)bpKG :(float)bpResp{
    
    if(bpKG > 0.0 && bpResp > 0.0){
        
        float bprm = bpKG*2.2/(1.0278-(bpResp*0.0278));
        float BPRMkg =  bprm/2.2;
        str_pred1.text = [NSString stringWithFormat:@"%.1f", BPRMkg];
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        float BPratio  =  BPRMkg/clientWeight;
        float adjBPratio  =  ageCorrection*BPratio;
        
        if(isnan(adjBPratio)){
            str_sw1.text = @"";
        }
        else if(isinf(adjBPratio)){
            str_sw1.text = @"";
        }
        else if(adjBPratio == (float) 0.0){
            str_sw1.text = @"";
        }
        else{
            str_sw1.text = [NSString stringWithFormat:@"%.2f", adjBPratio];
        }
        
        //adjBPratio = [str_sw1.text floatValue];
        adjBPratio = [[NSString stringWithFormat:@"%.3f", adjBPratio] floatValue];
        
        [upElementArray replaceObjectAtIndex:133 withObject:str_sw1.text];

        
        NSInteger per_rank1 = 2222;
       
        if(clientGender == 0){
            //male
            
            if(adjBPratio > 0){
                
                if(adjBPratio <= (float) 1.75 && adjBPratio >  (float) 1.55){
                    
                    if (adjBPratio > (float) 1.55 && adjBPratio <=  (float) 1.57) {
                        per_rank1 = 91;
                    }
                    else if (adjBPratio > (float) 1.57 && adjBPratio <=  (float) 1.59) {
                        
                        per_rank1 = 92;
                    }
                    else if (adjBPratio > (float) 1.59 && adjBPratio <=  (float) 1.61) {
                        
                        per_rank1 = 93;
                    }
                    else if (adjBPratio > (float) 1.61 && adjBPratio <=  (float) 1.63) {
                        per_rank1 = 94;
                    }
                    else if (adjBPratio >  (float) 1.63 && adjBPratio <=  (float) 1.65) {
                        per_rank1 = 95;
                        
                    }
                    else if (adjBPratio >  (float) 1.65 && adjBPratio <=  (float) 1.67) {
                        per_rank1 = 96;
                    }
                    else if (adjBPratio >  (float) 1.67 && adjBPratio <=  (float) 1.69) {
                        per_rank1 = 97;
                        
                    }
                    else if (adjBPratio >  (float) 1.69 && adjBPratio <=  (float) 1.71) {
                        per_rank1 = 98;
                        
                    }
                    else if (adjBPratio >  (float) 1.71 && adjBPratio <=  (float) 1.73) {
                        per_rank1 = 99;
                    }
                    
                    else if (adjBPratio >  (float) 1.73 && adjBPratio <=  (float) 1.75) {
                        per_rank1 = 100;
                    }
                    else  {
                        per_rank1 = 100;
                    }
                    
                }
                
                if(adjBPratio <= (float) 1.55 && adjBPratio >  (float) 1.40){
                    
                    if (adjBPratio > (float) 1.400 && adjBPratio <=  (float) 1.415) {
                        per_rank1 = 81;
                    }
                    else if (adjBPratio > (float) 1.415 && adjBPratio <=  (float) 1.430) {
                        
                        per_rank1 = 82;
                    }
                    else if (adjBPratio > (float) 1.430 && adjBPratio <=  (float) 1.445) {
                        
                        per_rank1 = 83;
                    }
                    else if (adjBPratio > (float) 1.445 && adjBPratio <=  (float) 1.460) {
                        per_rank1 = 84;
                    }
                    else if (adjBPratio >  (float) 1.460 && adjBPratio <=  (float) 1.475) {
                        per_rank1 = 85;
                        
                    }
                    else if (adjBPratio >  (float) 1.475 && adjBPratio <=  (float) 1.490) {
                        per_rank1 = 86;
                    }
                    else if (adjBPratio >  (float) 1.490 && adjBPratio <=  (float) 1.505) {
                        per_rank1 = 87;
                        
                    }
                    else if (adjBPratio >  (float) 1.505 && adjBPratio <=  (float) 1.520) {
                        per_rank1 = 88;
                        
                    }
                    else if (adjBPratio >  (float) 1.520 && adjBPratio <=  (float) 1.535) {
                        per_rank1 = 89;
                    }
                    
                    else if (adjBPratio >  (float) 1.535 && adjBPratio <=  (float) 1.55) {
                        per_rank1 = 90;
                    }
                    else  {
                        per_rank1 = 90;
                    }
                    
                }
                
                else if(adjBPratio <= (float) 1.40  && adjBPratio >  (float) 1.30){
                    
                    if (adjBPratio > (float) 1.30 && adjBPratio <=  (float) 1.31) {
                        per_rank1 = 71;
                    }
                    else if (adjBPratio > (float) 1.31 && adjBPratio <=  (float) 1.32) {
                        per_rank1 = 72;
                    }
                    else if (adjBPratio > (float) 1.32 && adjBPratio <=  (float) 1.33) {
                        per_rank1 = 73;
                    }
                    else if (adjBPratio > (float) 1.33 && adjBPratio <=  (float) 1.34) {
                        per_rank1 = 74;
                    }
                    else if (adjBPratio > (float) 1.34 && adjBPratio <=  (float) 1.35) {
                        per_rank1 = 75;
                    }
                    else if (adjBPratio > (float) 1.35 && adjBPratio <=  (float) 1.36) {
                        per_rank1 = 76;
                    }
                    else if (adjBPratio > (float) 1.36 && adjBPratio <=  (float) 1.37) {
                        per_rank1 = 77;
                    }
                    else if (adjBPratio > (float) 1.37 && adjBPratio <=  (float) 1.38) {
                        per_rank1 = 78;
                    }
                    else if (adjBPratio > (float) 1.38 && adjBPratio <=  (float) 1.39) {
                        per_rank1 = 79;
                    }
                    else  {
                        per_rank1 = 80;
                    }
                    
                    
                }
                else if(adjBPratio <= (float) 1.3 && adjBPratio >  (float) 1.2){
                    
                   
                    if (adjBPratio > (float) 1.20 && adjBPratio <=  (float) 1.21) {
                        per_rank1 = 61;
                    }
                    else if (adjBPratio > (float) 1.21 && adjBPratio <=  (float) 1.22) {
                        per_rank1 = 62;
                    }
                    else if (adjBPratio > (float) 1.22 && adjBPratio <=  (float) 1.23) {
                        per_rank1 = 63;
                    }
                    else if (adjBPratio > (float) 1.23 && adjBPratio <=  (float) 1.24) {
                        per_rank1 = 64;
                    }
                    else if (adjBPratio > (float) 1.24 && adjBPratio <=  (float) 1.25) {
                        per_rank1 = 65;
                    }
                    else if (adjBPratio > (float) 1.25 && adjBPratio <=  (float) 1.26) {
                        per_rank1 = 66;
                    }
                    else if (adjBPratio > (float) 1.26 && adjBPratio <=  (float) 1.27) {
                        per_rank1 = 67;
                    }
                    else if (adjBPratio > (float) 1.27 && adjBPratio <=  (float) 1.28) {
                        per_rank1 = 68;
                    }
                    else if (adjBPratio > (float) 1.28 && adjBPratio <=  (float) 1.29) {
                        per_rank1 = 69;
                    }
                    else  {
                        per_rank1 = 70;
                    }
                 
                }
                else if(adjBPratio <= (float) 1.20 && adjBPratio >  (float) 1.10){
                    
                    if (adjBPratio > (float) 1.10 && adjBPratio <=  (float) 1.11) {
                        per_rank1 = 51;
                    }
                    else if (adjBPratio > (float) 1.11 && adjBPratio <=  (float) 1.12) {
                        per_rank1 = 52;
                    }
                    else if (adjBPratio > (float) 1.12 && adjBPratio <=  (float) 1.13) {
                        per_rank1 = 53;
                    }
                    else if (adjBPratio > (float) 1.13 && adjBPratio <=  (float) 1.14) {
                        per_rank1 = 54;
                    }
                    else if (adjBPratio > (float) 1.14 && adjBPratio <=  (float) 1.15) {
                        per_rank1 = 55;
                    }
                    else if (adjBPratio > (float) 1.15 && adjBPratio <=  (float) 1.16) {
                        per_rank1 = 56;
                    }
                    else if (adjBPratio > (float) 1.16 && adjBPratio <=  (float) 1.17) {
                        per_rank1 = 57;
                    }
                    else if (adjBPratio > (float) 1.17 && adjBPratio <=  (float) 1.18) {
                        per_rank1 = 58;
                    }
                    else if (adjBPratio > (float) 1.18 && adjBPratio <=  (float) 1.19) {
                        per_rank1 = 59;
                    }
                    else  {
                        per_rank1 = 60;
                    }
                   
                }
                else if(adjBPratio <= (float) 1.1 && adjBPratio >  (float) 1.0){
                    
                    if (adjBPratio > (float) 1.00 && adjBPratio <=  (float) 1.01) {
                        per_rank1 = 41;
                    }
                    else if (adjBPratio > (float) 1.01 && adjBPratio <=  (float) 1.02) {
                        per_rank1 = 42;
                    }
                    else if (adjBPratio > (float) 1.02 && adjBPratio <=  (float) 1.03) {
                        per_rank1 = 43;
                    }
                    else if (adjBPratio > (float) 1.03 && adjBPratio <=  (float) 1.04) {
                        per_rank1 = 44;
                    }
                    else if (adjBPratio > (float) 1.04 && adjBPratio <=  (float) 1.05) {
                        per_rank1 = 45;
                    }
                    else if (adjBPratio > (float) 1.05 && adjBPratio <=  (float) 1.06) {
                        per_rank1 = 46;
                    }
                    else if (adjBPratio > (float) 1.06 && adjBPratio <=  (float) 1.07) {
                        per_rank1 = 47;
                    }
                    else if (adjBPratio > (float) 1.07 && adjBPratio <=  (float) 1.08) {
                        per_rank1 = 48;
                    }
                    else if (adjBPratio > (float) 1.08 && adjBPratio <=  (float) 1.09) {
                        per_rank1 = 49;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                   
                }
                else if(adjBPratio <= (float) 1.00  && adjBPratio > (float) 0.90){
                    
                    if (adjBPratio > (float) 0.90 && adjBPratio <=  (float) 0.91) {
                        per_rank1 = 31;
                    }
                    else if (adjBPratio > (float) 0.91 && adjBPratio <=  (float) 0.92) {
                        per_rank1 = 32;
                    }
                    else if (adjBPratio > (float) 0.92 && adjBPratio <=  (float) 0.93) {
                        per_rank1 = 33;
                    }
                    else if (adjBPratio > (float) 0.93 && adjBPratio <=  (float) 0.94) {
                        per_rank1 = 34;
                    }
                    else if (adjBPratio > (float) 0.94 && adjBPratio <=  (float) 0.95) {
                        per_rank1 = 35;
                    }
                    else if (adjBPratio > (float) 0.95 && adjBPratio <=  (float) 0.96) {
                        per_rank1 = 36;
                    }
                    else if (adjBPratio > (float) 0.96 && adjBPratio <=  (float) 0.97) {
                        per_rank1 = 37;
                    }
                    else if (adjBPratio > (float) 0.97 && adjBPratio <=  (float) 0.98) {
                        per_rank1 = 38;
                    }
                    else if (adjBPratio > (float) 0.98 && adjBPratio <=  (float) 0.99) {
                        per_rank1 = 39;
                    }
                    else  {
                        per_rank1 = 40;
                    }
                   
                }
                else if(adjBPratio <= (float) 0.90  && adjBPratio > (float) 0.75){
                    
                    
                    if (adjBPratio > (float) 0.750 && adjBPratio <=  (float) 0.765) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio > (float) 0.765 && adjBPratio <=  (float) 0.780) {
                        
                        per_rank1 = 22;
                    }
                    else if (adjBPratio > (float) 0.780 && adjBPratio <=  (float) 0.795) {
                        
                        per_rank1 = 23;
                    }
                    else if (adjBPratio > (float) 0.795 && adjBPratio <=  (float) 0.810) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio >  (float) 0.810 && adjBPratio <=  (float) 0.825) {
                        per_rank1 = 25;
                        
                    }
                    else if (adjBPratio >  (float) 0.825 && adjBPratio <=  (float) 0.840) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio >  (float) 0.840 && adjBPratio <=  (float) 0.855) {
                        per_rank1 = 27;
                        
                    }
                    else if (adjBPratio >  (float) 0.855 && adjBPratio <=  (float) 0.870) {
                        per_rank1 = 28;
                        
                    }
                    else if (adjBPratio >  (float) 0.870 && adjBPratio <=  (float) 0.885) {
                        per_rank1 = 29;
                    }
                    
                    else if (adjBPratio >  (float) 0.885 && adjBPratio <=  (float) 0.90) {
                        per_rank1 = 30;
                    }
                    else  {
                        per_rank1 = 30;
                    }
                    
                }
                else if(adjBPratio <= (float) 0.75  && adjBPratio > (float) 0.60){
                    
                    
                    if (adjBPratio > (float) 0.60 && adjBPratio <=  (float) 0.615) {
                        per_rank1 = 11;
                    }
                    else if (adjBPratio > (float) 0.615 && adjBPratio <=  (float) 0.630) {
                        
                        per_rank1 = 12;
                    }
                    else if (adjBPratio > (float) 0.630 && adjBPratio <=  (float) 0.645) {
                        
                        per_rank1 = 13;
                    }
                    else if (adjBPratio > (float) 0.645 && adjBPratio <=  (float) 0.660) {
                        per_rank1 = 14;
                    }
                    else if (adjBPratio >  (float) 0.660 && adjBPratio <=  (float) 0.675) {
                        per_rank1 = 15;
                        
                    }
                    else if (adjBPratio >  (float) 0.675 && adjBPratio <=  (float) 0.680) {
                        per_rank1 = 16;
                    }
                    else if (adjBPratio >  (float) 0.680 && adjBPratio <=  (float) 0.695) {
                        per_rank1 = 17;
                        
                    }
                    else if (adjBPratio >  (float) 0.695 && adjBPratio <=  (float) 0.710) {
                        per_rank1 = 18;
                        
                    }
                    else if (adjBPratio >  (float) 0.710 && adjBPratio <=  (float) 0.725) {
                        per_rank1 = 19;
                    }
                    
                    else if (adjBPratio >  (float) 0.725 && adjBPratio <=  (float) 0.740) {
                        per_rank1 = 20;
                    }
                    else  {
                        per_rank1 = 20;
                    }
                    
                }
                else if(adjBPratio <= (float) 0.60  && adjBPratio > (float) 0.50){
                    

                    if (adjBPratio > (float) 0.50 && adjBPratio <=  (float) 0.51) {
                        per_rank1 = 1;
                    }
                    else if (adjBPratio > (float) 0.51 && adjBPratio <=  (float) 0.52) {
                        per_rank1 = 2;
                    }
                    else if (adjBPratio > (float) 0.52 && adjBPratio <=  (float) 0.53) {
                        per_rank1 = 3;
                    }
                    else if (adjBPratio > (float) 0.53 && adjBPratio <=  (float) 0.54) {
                        per_rank1 = 4;
                    }
                    else if (adjBPratio > (float) 0.54 && adjBPratio <=  (float) 0.55) {
                        per_rank1 = 5;
                    }
                    else if (adjBPratio > (float) 0.55 && adjBPratio <=  (float) 0.56) {
                        per_rank1 = 6;
                    }
                    else if (adjBPratio > (float) 0.56 && adjBPratio <=  (float) 0.57) {
                        per_rank1 = 7;
                    }
                    else if (adjBPratio > (float) 0.57 && adjBPratio <=  (float) 0.58) {
                        per_rank1 = 8;
                    }
                    else if (adjBPratio > (float) 0.58 && adjBPratio <=  (float) 0.59) {
                        per_rank1 = 9;
                    }
                    else  {
                        per_rank1 = 10;
                    }
                    
                    
                }
                else if(adjBPratio > (float) 1.75){
                    per_rank1 = 100;
                }
                else if(adjBPratio <=  (float) 0.50){
                    per_rank1 = 0;
                }
                if(per_rank1 == 2222){
                    str_rank1.text = @"--";
                }
                else{
                    str_rank1.text = [NSString stringWithFormat:@"%d", per_rank1];
                }
            }
            else{
                per_rank1 = 2222;
                str_rank1.text = @"--";
            }
        } // end for male
        
        else{
            //female
            
            if(adjBPratio >  0){
                
                if(adjBPratio <= (float) 0.90 && adjBPratio >  (float) 0.85){
                    
                    if (adjBPratio > (float) 0.850 && adjBPratio <= (float) 0.855) {
                        per_rank1 = 91;
                    }
                    else if (adjBPratio > (float) 0.855 && adjBPratio <= (float) 0.860) {
                        per_rank1 = 92;
                    }
                    else if (adjBPratio > (float) 0.860 && adjBPratio <= (float) 0.865) {
                        per_rank1 = 93;
                    }
                    else if (adjBPratio > (float) 0.865 && adjBPratio <= (float) 0.870) {
                        per_rank1 = 94;
                    }
                    else if (adjBPratio > (float) 0.870 && adjBPratio <= (float) 0.875) {
                        per_rank1 = 95;
                    }
                    else if (adjBPratio > (float) 0.875 && adjBPratio <= (float) 0.880) {
                        per_rank1 = 96;
                    }
                    else if (adjBPratio > (float) 0.880 && adjBPratio <= (float) 0.885) {
                        per_rank1 = 97;
                    }
                    else if (adjBPratio > (float) 0.885 && adjBPratio <= (float) 0.890) {
                        per_rank1 = 98;
                    }
                    else if (adjBPratio > (float) 0.890 && adjBPratio <= (float) 0.895) {
                        per_rank1 = 99;
                    }
                    else  {
                        per_rank1 = 100;
                    }
                }
                else if(adjBPratio <= (float) 0.85  && adjBPratio >  (float) 0.80){
                    
                    if (adjBPratio > (float) 0.800 && adjBPratio <= (float) 0.805) {
                        per_rank1 = 81;
                    }
                    else if (adjBPratio > (float) 0.805 && adjBPratio <= (float) 0.810) {
                        per_rank1 = 82;
                    }
                    else if (adjBPratio > (float) 0.810 && adjBPratio <= (float) 0.815) {
                        per_rank1 = 83;
                    }
                    else if (adjBPratio > (float) 0.815 && adjBPratio <= (float) 0.820) {
                        per_rank1 = 84;
                    }
                    else if (adjBPratio > (float) 0.820 && adjBPratio <= (float) 0.825) {
                        per_rank1 = 85;
                    }
                    else if (adjBPratio > (float) 0.825 && adjBPratio <= (float) 0.830) {
                        per_rank1 = 86;
                    }
                    else if (adjBPratio > (float) 0.830 && adjBPratio <= (float) 0.835) {
                        per_rank1 = 87;
                    }
                    else if (adjBPratio > (float) 0.835 && adjBPratio <= (float) 0.840) {
                        per_rank1 = 88;
                    }
                    else if (adjBPratio > (float) 0.840 && adjBPratio <= (float) 0.845) {
                        per_rank1 = 89;
                    }
                    else  {
                        per_rank1 = 90;
                    }
                }
                else if(adjBPratio <= (float) 0.80 && adjBPratio >  (float) 0.75){
                    
                    if (adjBPratio > (float) 0.750 && adjBPratio <= (float) 0.755) {
                        per_rank1 = 71;
                    }
                    else if (adjBPratio > (float) 0.755 && adjBPratio <= (float) 0.760) {
                        per_rank1 = 72;
                    }
                    else if (adjBPratio > (float) 0.760 && adjBPratio <= (float) 0.765) {
                        per_rank1 = 73;
                    }
                    else if (adjBPratio > (float) 0.765 && adjBPratio <= (float) 0.770) {
                        per_rank1 = 74;
                    }
                    else if (adjBPratio > (float) 0.770 && adjBPratio <= (float) 0.775) {
                        per_rank1 = 75;
                    }
                    else if (adjBPratio > (float) 0.775 && adjBPratio <= (float) 0.780) {
                        per_rank1 = 76;
                    }
                    else if (adjBPratio > (float) 0.780 && adjBPratio <= (float) 0.785) {
                        per_rank1 = 77;
                    }
                    else if (adjBPratio > (float) 0.785 && adjBPratio <= (float) 0.790) {
                        per_rank1 = 78;
                    }
                    else if (adjBPratio > (float) 0.790 && adjBPratio <= (float) 0.795) {
                        per_rank1 = 89;
                    }
                    else  {
                        per_rank1 = 80;
                    }
                }
                else if(adjBPratio <= (float) 0.75 && adjBPratio > (float)  0.70){
                    
                    if (adjBPratio > (float) 0.700 && adjBPratio <= (float) 0.705) {
                        per_rank1 = 61;
                    }
                    else if (adjBPratio > (float) 0.705 && adjBPratio <= (float) 0.710) {
                        per_rank1 = 62;
                    }
                    else if (adjBPratio > (float) 0.710 && adjBPratio <= (float) 0.715) {
                        per_rank1 = 63;
                    }
                    else if (adjBPratio > (float) 0.715 && adjBPratio <= (float) 0.720) {
                        per_rank1 = 64;
                    }
                    else if (adjBPratio > (float) 0.720 && adjBPratio <= (float) 0.725) {
                        per_rank1 = 65;
                    }
                    else if (adjBPratio > (float) 0.725 && adjBPratio <= (float) 0.730) {
                        per_rank1 = 66;
                    }
                    else if (adjBPratio > (float) 0.730 && adjBPratio <= (float) 0.735) {
                        per_rank1 = 67;
                    }
                    else if (adjBPratio > (float) 0.735 && adjBPratio <= (float) 0.740) {
                        per_rank1 = 68;
                    }
                    else if (adjBPratio > (float) 0.740 && adjBPratio <= (float) 0.745) {
                        per_rank1 = 69;
                    }
                    else  {
                        per_rank1 = 70;
                    }
                }
                else if(adjBPratio <= (float) 0.70 && adjBPratio >  (float) 0.65){
                    
                    if (adjBPratio > (float) 0.650 && adjBPratio <= (float) 0.655) {
                        per_rank1 = 51;
                    }
                    else if (adjBPratio > (float) 0.655 && adjBPratio <= (float) 0.660) {
                        per_rank1 = 52;
                    }
                    else if (adjBPratio > (float) 0.660 && adjBPratio <= (float) 0.665) {
                        per_rank1 = 53;
                    }
                    else if (adjBPratio > (float) 0.665 && adjBPratio <= (float) 0.670) {
                        per_rank1 = 54;
                    }
                    else if (adjBPratio > (float) 0.670 && adjBPratio <= (float) 0.675) {
                        per_rank1 = 55;
                    }
                    else if (adjBPratio > (float) 0.675 && adjBPratio <= (float) 0.680) {
                        per_rank1 = 56;
                    }
                    else if (adjBPratio > (float) 0.680 && adjBPratio <= (float) 0.685) {
                        per_rank1 = 57;
                    }
                    else if (adjBPratio > (float) 0.685 && adjBPratio <= (float) 0.690) {
                        per_rank1 = 58;
                    }
                    else if (adjBPratio > (float) 0.690 && adjBPratio <= (float) 0.695) {
                        per_rank1 = 59;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                }
                else if(adjBPratio <= (float) 0.65  && adjBPratio > (float) 0.60){
                    
                    if (adjBPratio > (float) 0.600 && adjBPratio <= (float) 0.605) {
                        per_rank1 = 41;
                    }
                    else if (adjBPratio > (float) 0.605 && adjBPratio <= (float) 0.610) {
                        per_rank1 = 42;
                    }
                    else if (adjBPratio > (float) 0.610 && adjBPratio <= (float) 0.615) {
                        per_rank1 = 43;
                    }
                    else if (adjBPratio > (float) 0.615 && adjBPratio <= (float) 0.620) {
                        per_rank1 = 44;
                    }
                    else if (adjBPratio > (float) 0.620 && adjBPratio <= (float) 0.625) {
                        per_rank1 = 45;
                    }
                    else if (adjBPratio > (float) 0.625 && adjBPratio <= (float) 0.630) {
                        per_rank1 = 46;
                    }
                    else if (adjBPratio > (float) 0.630 && adjBPratio <= (float) 0.635) {
                        per_rank1 = 47;
                    }
                    else if (adjBPratio > (float) 0.635 && adjBPratio <= (float) 0.640) {
                        per_rank1 = 48;
                    }
                    else if (adjBPratio > (float) 0.640 && adjBPratio <= (float) 0.645) {
                        per_rank1 = 49;
                    }
                    else  {
                        per_rank1 = 50;
                    }
                }
                else if(adjBPratio <= (float) 0.60  && adjBPratio > (float) 0.55){
                    
                    if (adjBPratio > (float) 0.550 && adjBPratio <= (float) 0.555) {
                        per_rank1 = 31;
                    }
                    else if (adjBPratio > (float) 0.555 && adjBPratio <= (float) 0.560) {
                        per_rank1 = 32;
                    }
                    else if (adjBPratio > (float) 0.560 && adjBPratio <= (float) 0.565) {
                        per_rank1 = 33;
                    }
                    else if (adjBPratio > (float) 0.565 && adjBPratio <= (float) 0.570) {
                        per_rank1 = 34;
                    }
                    else if (adjBPratio > (float) 0.570 && adjBPratio <= (float) 0.575) {
                        per_rank1 = 35;
                    }
                    else if (adjBPratio > (float) 0.575 && adjBPratio <= (float) 0.580) {
                        per_rank1 = 36;
                    }
                    else if (adjBPratio > (float) 0.580 && adjBPratio <= (float) 0.585) {
                        per_rank1 = 37;
                    }
                    else if (adjBPratio > (float) 0.585 && adjBPratio <= (float) 0.590) {
                        per_rank1 = 38;
                    }
                    else if (adjBPratio > (float) 0.590 && adjBPratio <= (float) 0.595) {
                        per_rank1 = 39;
                    }
                    else  {
                        per_rank1 = 40;
                    }
                }
                else if(adjBPratio <= (float) 0.550  && adjBPratio > (float) 0.500){
                    
                    if (adjBPratio > (float) 0.500 && adjBPratio <= (float) 0.505) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio > (float) 0.505 && adjBPratio <= (float) 0.510) {
                        per_rank1 = 22;
                    }
                    else if (adjBPratio > (float) 0.510 && adjBPratio <= (float) 0.515) {
                        per_rank1 = 23;
                    }
                    else if (adjBPratio > (float) 0.515 && adjBPratio <= (float) 0.520) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio > (float) 0.520 && adjBPratio <= (float) 0.525) {
                        per_rank1 = 25;
                    }
                    else if (adjBPratio > (float) 0.525 && adjBPratio <= (float) 0.530) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio > (float) 0.530 && adjBPratio <= (float) 0.535) {
                        per_rank1 = 27;
                    }
                    else if (adjBPratio > (float) 0.535 && adjBPratio <= (float) 0.540) {
                        per_rank1 = 28;
                    }
                    else if (adjBPratio > (float) 0.540 && adjBPratio <= (float) 0.545) {
                        per_rank1 = 29;
                    }
                    else  {
                        per_rank1 = 30;
                    }
                }
                else if(adjBPratio <= (float) 0.50  && adjBPratio > (float) 0.45){

                    if (adjBPratio > (float) 0.450 && adjBPratio <= (float) 0.455) {
                        per_rank1 = 11;
                    }
                    else if (adjBPratio > (float) 0.455 && adjBPratio <= (float) 0.460) {
                        per_rank1 = 12;
                    }
                    else if (adjBPratio > (float) 0.460 && adjBPratio <= (float) 0.465) {
                        per_rank1 = 13;
                    }
                    else if (adjBPratio > (float) 0.465 && adjBPratio <= (float) 0.470) {
                        per_rank1 = 14;
                    }
                    else if (adjBPratio > (float) 0.470 && adjBPratio <= (float) 0.475) {
                        per_rank1 = 15;
                    }
                    else if (adjBPratio > (float) 0.475 && adjBPratio <= (float) 0.480) {
                        per_rank1 = 16;
                    }
                    else if (adjBPratio > (float) 0.480 && adjBPratio <= (float) 0.485) {
                        per_rank1 = 17;
                    }
                    else if (adjBPratio > (float) 0.485 && adjBPratio <= (float) 0.490) {
                        per_rank1 = 18;
                    }
                    else if (adjBPratio > (float) 0.490 && adjBPratio <= (float) 0.495) {
                        per_rank1 = 19;
                    }
                    else  {
                        per_rank1 = 20;
                    }
                }
                else if(adjBPratio  <= (float) 0.450  && adjBPratio > (float) 0.400){

                    if (adjBPratio > (float) 0.400 && adjBPratio <= (float) 0.405) {
                        per_rank1 = 21;
                    }
                    else if (adjBPratio > (float) 0.405 && adjBPratio <= (float) 0.410) {
                        per_rank1 = 22;
                    }
                    else if (adjBPratio > (float) 0.410 && adjBPratio <= (float) 0.415) {
                        per_rank1 = 23;
                    }
                    else if (adjBPratio > (float) 0.415 && adjBPratio <= (float) 0.420) {
                        per_rank1 = 24;
                    }
                    else if (adjBPratio > (float) 0.420 && adjBPratio <= (float) 0.425) {
                        per_rank1 = 25;
                    }
                    else if (adjBPratio > (float) 0.425 && adjBPratio <= (float) 0.430) {
                        per_rank1 = 26;
                    }
                    else if (adjBPratio > (float) 0.430 && adjBPratio <= (float) 0.435) {
                        per_rank1 = 27;
                    }
                    else if (adjBPratio > (float) 0.435 && adjBPratio <= (float) 0.440) {
                        per_rank1 = 28;
                    }
                    else if (adjBPratio > (float) 0.440 && adjBPratio <= (float) 0.445) {
                        per_rank1 = 29;
                    }
                    else  {
                        per_rank1 = 30;
                    }

                }
                else if (adjBPratio <=  (float) 0.40) {
                    per_rank1 = 0;
                }
                else if(adjBPratio > (float) 0.90){
                    per_rank1 = 100;
                }
                
                if(per_rank1 == 2222){
                    str_rank1.text = @"--";
                }
                else{
                    str_rank1.text = [NSString stringWithFormat:@"%d", per_rank1];
                }
                
            }
            else{
                per_rank1 = 2222;
                str_rank1.text = @"--";
            }
            
        }
        
        strg_per_rank1 = per_rank1;
                
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;//(100 - total_percent_rank);
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    // end of if
    
    else{
        str_pred1.text = @"";
        str_sw1.text = @"";
        str_rank1.text  = @"--";
        
        [upElementArray replaceObjectAtIndex:134 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;//(100 - total_percent_rank);
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
}


// Arm Curl

-(void)getArmCurlValue:(float)bpKG :(float)bpResp{
    
    if(bpKG > 0.0 && bpResp > 0.0){
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        float bprm2 = bpKG*2.2/(1.0278-(bpResp*0.0278));
        float BPRMkg2 =  bprm2/2.2;
        str_pred2.text = [NSString stringWithFormat:@"%.1f", BPRMkg2];
        
        float BPratio2  =  BPRMkg2/clientWeight;
        float adjBPratio2  =  ageCorrection*BPratio2;
        
        if(isnan(adjBPratio2)){
            str_sw2.text = @"";
        }
        else if(isinf(adjBPratio2)){
            str_sw2.text = @"";
        }
        else if(adjBPratio2 == 0.0){
            str_sw2.text = @"";
        }
        else{
            str_sw2.text = [NSString stringWithFormat:@"%.2f", adjBPratio2];
        }
        adjBPratio2 = [[NSString stringWithFormat:@"%.3f", adjBPratio2] floatValue];
        
        [upElementArray replaceObjectAtIndex:135 withObject:str_sw2.text];
        
        NSInteger per_rank2 = 2222;
        
        if(clientGender == 0){
            //male
            
            if(adjBPratio2 > 0){
                
                if(adjBPratio2 <= (float) 0.800 && adjBPratio2 >  (float) 0.650){
                    
                    if (adjBPratio2 > (float) 0.650 && adjBPratio2 <=  (float) 0.665) {
                        per_rank2 = 91;
                    }
                    else if (adjBPratio2 > (float) 0.665 && adjBPratio2 <=  (float) 0.680) {
                        per_rank2 = 92;
                    }
                    else if (adjBPratio2 > (float) 0.680 && adjBPratio2 <=  (float) 0.695) {
                        per_rank2 = 93;
                    }
                    else if (adjBPratio2 > (float) 0.695 && adjBPratio2 <=  (float) 0.710) {
                        per_rank2 = 94;
                    }
                    else if (adjBPratio2 >  (float) 0.710 && adjBPratio2 <=  (float) 0.725) {
                        per_rank2 = 95;
                    }
                    else if (adjBPratio2 > (float) 0.725 && adjBPratio2 <=  (float) 0.740) {
                        per_rank2 = 96;
                    }
                    else if (adjBPratio2 > (float) 0.740 && adjBPratio2 <=  (float) 0.755) {
                        per_rank2 = 97;
                    }
                    else if (adjBPratio2 > (float) 0.755 && adjBPratio2 <=  (float) 0.770) {
                        per_rank2 = 98;
                    }
                    else if (adjBPratio2 >  (float) 0.770 && adjBPratio2 <=  (float) 0.785) {
                        per_rank2 = 99;
                    }
                    else {
                        per_rank2 = 100;
                    }
                }
                else if(adjBPratio2 <= (float) 0.65  && adjBPratio2 >  (float) 0.60){
                    
                    if (adjBPratio2 > (float) 0.600 && adjBPratio2 <= (float) 0.605) {
                        per_rank2 = 81;
                    }
                    else if (adjBPratio2 > (float) 0.605 && adjBPratio2 <= (float) 0.610) {
                        per_rank2 = 82;
                    }
                    else if (adjBPratio2 > (float) 0.610 && adjBPratio2 <= (float) 0.615) {
                        per_rank2 = 83;
                    }
                    else if (adjBPratio2 > (float) 0.615 && adjBPratio2 <= (float) 0.620) {
                        per_rank2 = 84;
                    }
                    else if (adjBPratio2 > (float) 0.620 && adjBPratio2 <= (float) 0.625) {
                        per_rank2 = 85;
                    }
                    else if (adjBPratio2 > (float) 0.625 && adjBPratio2 <= (float) 0.630) {
                        per_rank2 = 86;
                    }
                    else if (adjBPratio2 > (float) 0.630 && adjBPratio2 <= (float) 0.635) {
                        per_rank2 = 87;
                    }
                    else if (adjBPratio2 > (float) 0.635 && adjBPratio2 <= (float) 0.640) {
                        per_rank2 = 88;
                    }
                    else if (adjBPratio2 > (float) 0.640 && adjBPratio2 <= (float) 0.645) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 90;
                    }

                }
                else if(adjBPratio2 <= (float) 0.60 && adjBPratio2 >  (float) 0.55){
                    
                    if (adjBPratio2 > (float) 0.550 && adjBPratio2 <= (float) 0.555) {
                        per_rank2 = 71;
                    }
                    else if (adjBPratio2 > (float) 0.555 && adjBPratio2 <= (float) 0.560) {
                        per_rank2 = 72;
                    }
                    else if (adjBPratio2 > (float) 0.560 && adjBPratio2 <= (float) 0.565) {
                        per_rank2 = 73;
                    }
                    else if (adjBPratio2 > (float) 0.565 && adjBPratio2 <= (float) 0.570) {
                        per_rank2 = 74;
                    }
                    else if (adjBPratio2 > (float) 0.570 && adjBPratio2 <= (float) 0.575) {
                        per_rank2 = 75;
                    }
                    else if (adjBPratio2 > (float) 0.575 && adjBPratio2 <= (float) 0.580) {
                        per_rank2 = 76;
                    }
                    else if (adjBPratio2 > (float) 0.580 && adjBPratio2 <= (float) 0.585) {
                        per_rank2 = 77;
                    }
                    else if (adjBPratio2 > (float) 0.585 && adjBPratio2 <= (float) 0.590) {
                        per_rank2 = 78;
                    }
                    else if (adjBPratio2 > (float) 0.590 && adjBPratio2 <= (float) 0.595) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 80;
                    }
                    
                }
                else if(adjBPratio2 <= (float) 0.55 && adjBPratio2 >  (float) 0.50){
                    
                    if (adjBPratio2 > (float) 0.500 && adjBPratio2 <= (float) 0.505) {
                        per_rank2 = 61;
                    }
                    else if (adjBPratio2 > (float) 0.505 && adjBPratio2 <= (float) 0.510) {
                        per_rank2 = 62;
                    }
                    else if (adjBPratio2 > (float) 0.510 && adjBPratio2 <= (float) 0.515) {
                        per_rank2 = 63;
                    }
                    else if (adjBPratio2 > (float) 0.515 && adjBPratio2 <= (float) 0.520) {
                        per_rank2 = 64;
                    }
                    else if (adjBPratio2 > (float) 0.520 && adjBPratio2 <= (float) 0.525) {
                        per_rank2 = 65;
                    }
                    else if (adjBPratio2 > (float) 0.525 && adjBPratio2 <= (float) 0.530) {
                        per_rank2 = 66;
                    }
                    else if (adjBPratio2 > (float) 0.530 && adjBPratio2 <= (float) 0.535) {
                        per_rank2 = 67;
                    }
                    else if (adjBPratio2 > (float) 0.535 && adjBPratio2 <= (float) 0.540) {
                        per_rank2 = 68;
                    }
                    else if (adjBPratio2 > (float) 0.540 && adjBPratio2 <= (float) 0.545) {
                        per_rank2 = 69;
                    }
                    else  {
                        per_rank2 = 70;
                    }

                }
                else if(adjBPratio2 <= (float) 0.500 && adjBPratio2 >  (float) 0.450){
                    
                    if (adjBPratio2 > (float) 0.450 && adjBPratio2 <= (float) 0.455) {
                        per_rank2 = 51;
                    }
                    else if (adjBPratio2 > (float) 0.455 && adjBPratio2 <= (float) 0.460) {
                        per_rank2 = 52;
                    }
                    else if (adjBPratio2 > (float) 0.460 && adjBPratio2 <= (float) 0.465) {
                        per_rank2 = 53;
                    }
                    else if (adjBPratio2 > (float) 0.465 && adjBPratio2 <= (float) 0.470) {
                        per_rank2 = 54;
                    }
                    else if (adjBPratio2 > (float) 0.470 && adjBPratio2 <= (float) 0.475) {
                        per_rank2 = 55;
                    }
                    else if (adjBPratio2 > (float) 0.475 && adjBPratio2 <= (float) 0.480) {
                        per_rank2 = 56;
                    }
                    else if (adjBPratio2 > (float) 0.480 && adjBPratio2 <= (float) 0.485) {
                        per_rank2 = 57;
                    }
                    else if (adjBPratio2 > (float) 0.485 && adjBPratio2 <= (float) 0.490) {
                        per_rank2 = 58;
                    }
                    else if (adjBPratio2 > (float) 0.490 && adjBPratio2 <= (float) 0.495) {
                        per_rank2 = 59;
                    }
                    else  {
                        per_rank2 = 60;
                    }

                    
                }
                else if(adjBPratio2 <= (float) 0.45  && adjBPratio2 > (float) 0.40){
                    
                    
                    if (adjBPratio2 > (float) 0.400 && adjBPratio2 <= (float) 0.405) {
                        per_rank2 = 41;
                    }
                    else if (adjBPratio2 > (float) 0.405 && adjBPratio2 <= (float) 0.410) {
                        per_rank2 = 42;
                    }
                    else if (adjBPratio2 > (float) 0.410 && adjBPratio2 <= (float) 0.415) {
                        per_rank2 = 43;
                    }
                    else if (adjBPratio2 > (float) 0.415 && adjBPratio2 <= (float) 0.420) {
                        per_rank2 = 44;
                    }
                    else if (adjBPratio2 > (float) 0.420 && adjBPratio2 <= (float) 0.425) {
                        per_rank2 = 45;
                    }
                    else if (adjBPratio2 > (float) 0.425 && adjBPratio2 <= (float) 0.430) {
                        per_rank2 = 46;
                    }
                    else if (adjBPratio2 > (float) 0.430 && adjBPratio2 <= (float) 0.435) {
                        per_rank2 = 47;
                    }
                    else if (adjBPratio2 > (float) 0.435 && adjBPratio2 <= (float) 0.440) {
                        per_rank2 = 48;
                    }
                    else if (adjBPratio2 > (float) 0.440 && adjBPratio2 <= (float) 0.445) {
                        per_rank2 = 49;
                    }
                    else  {
                        per_rank2 = 50;
                    }
                }
                else if(adjBPratio2 <= (float) 0.400  && adjBPratio2 > (float) 0.350){
                    
                    
                    if (adjBPratio2 > (float) 0.350 && adjBPratio2 <= (float) 0.355) {
                        per_rank2 = 31;
                    }
                    else if (adjBPratio2 > (float) 0.355 && adjBPratio2 <= (float) 0.360) {
                        per_rank2 = 32;
                    }
                    else if (adjBPratio2 > (float) 0.360 && adjBPratio2 <= (float) 0.365) {
                        per_rank2 = 33;
                    }
                    else if (adjBPratio2 > (float) 0.365 && adjBPratio2 <= (float) 0.370) {
                        per_rank2 = 34;
                    }
                    else if (adjBPratio2 > (float) 0.370 && adjBPratio2 <= (float) 0.375) {
                        per_rank2 = 35;
                    }
                    else if (adjBPratio2 > (float) 0.375 && adjBPratio2 <= (float) 0.380) {
                        per_rank2 = 36;
                    }
                    else if (adjBPratio2 > (float) 0.380 && adjBPratio2 <= (float) 0.385) {
                        per_rank2 = 37;
                    }
                    else if (adjBPratio2 > (float) 0.385 && adjBPratio2 <= (float) 0.390) {
                        per_rank2 = 38;
                    }
                    else if (adjBPratio2 > (float) 0.390 && adjBPratio2 <= (float) 0.395) {
                        per_rank2 = 39;
                    }
                    else  {
                        per_rank2 = 40;
                    }

                }
                else if(adjBPratio2 <= (float) 0.35  && adjBPratio2 > (float) 0.30){
                    
                    
                    if (adjBPratio2 > (float) 0.300 && adjBPratio2 <= (float) 0.305) {
                        per_rank2 = 21;
                    }
                    else if (adjBPratio2 > (float) 0.305 && adjBPratio2 <= (float) 0.310) {
                        per_rank2 = 22;
                    }
                    else if (adjBPratio2 > (float) 0.310 && adjBPratio2 <= (float) 0.315) {
                        per_rank2 = 23;
                    }
                    else if (adjBPratio2 > (float) 0.315 && adjBPratio2 <= (float) 0.320) {
                        per_rank2 = 24;
                    }
                    else if (adjBPratio2 > (float) 0.320 && adjBPratio2 <= (float) 0.325) {
                        per_rank2 = 25;
                    }
                    else if (adjBPratio2 > (float) 0.325 && adjBPratio2 <= (float) 0.330) {
                        per_rank2 = 26;
                    }
                    else if (adjBPratio2 > (float) 0.330 && adjBPratio2 <= (float) 0.335) {
                        per_rank2 = 27;
                    }
                    else if (adjBPratio2 > (float) 0.335 && adjBPratio2 <= (float) 0.340) {
                        per_rank2 = 28;
                    }
                    else if (adjBPratio2 > (float) 0.340 && adjBPratio2 <= (float) 0.345) {
                        per_rank2 = 29;
                    }
                    else  {
                        per_rank2 = 30;
                    }
                }
                else if(adjBPratio2 <= (float) 0.30  && adjBPratio2 > (float) 0.20){
                    
                    if (adjBPratio2 > (float) 0.20 && adjBPratio2 <=  (float) 0.21) {
                        per_rank2 = 11;
                    }
                    else if (adjBPratio2 > (float) 0.21 && adjBPratio2 <=  (float) 0.22) {
                        per_rank2 = 12;
                    }
                    else if (adjBPratio2 > (float) 0.22 && adjBPratio2 <=  (float) 0.23) {
                        per_rank2 = 13;
                    }
                    else if (adjBPratio2 > (float) 0.23 && adjBPratio2 <=  (float) 0.24) {
                        per_rank2 = 14;
                    }
                    else if (adjBPratio2 >  (float) 0.24 && adjBPratio2 <=  (float) 0.25) {
                        per_rank2 = 15;
                    }
                    else if (adjBPratio2 > (float) 0.25 && adjBPratio2 <=  (float) 0.26) {
                        per_rank2 = 16;
                    }
                    else if (adjBPratio2 > (float) 0.26 && adjBPratio2 <=  (float) 0.27) {
                        per_rank2 = 17;
                    }
                    else if (adjBPratio2 > (float) 0.27 && adjBPratio2 <=  (float) 0.28) {
                        per_rank2 = 18;
                    }
                    else if (adjBPratio2 >  (float) 0.28 && adjBPratio2 <=  (float) 0.29) {
                        per_rank2 = 19;
                    }
                    else {
                        per_rank2 = 10;
                    }
                  
                }
                else if(adjBPratio2 <= (float) 0.20  && adjBPratio2 > (float) 0.10){
                    
                    if (adjBPratio2 > (float) 0.10 && adjBPratio2 <=  (float) 0.11) {
                        per_rank2 = 1;
                    }
                    else if (adjBPratio2 > (float) 0.11 && adjBPratio2 <=  (float) 0.12) {
                        per_rank2 = 2;
                    }
                    else if (adjBPratio2 > (float) 0.12 && adjBPratio2 <=  (float) 0.13) {
                        per_rank2 = 3;
                    }
                    else if (adjBPratio2 > (float) 0.13 && adjBPratio2 <=  (float) 0.14) {
                        per_rank2 = 4;
                    }
                    else if (adjBPratio2 >  (float) 0.14 && adjBPratio2 <=  (float) 0.15) {
                        per_rank2 = 5;
                    }
                    else if (adjBPratio2 > (float) 0.15 && adjBPratio2 <=  (float) 0.16) {
                        per_rank2 = 6;
                    }
                    else if (adjBPratio2 > (float) 0.16 && adjBPratio2 <=  (float) 0.17) {
                        per_rank2 = 7;
                    }
                    else if (adjBPratio2 > (float) 0.17 && adjBPratio2 <=  (float) 0.18) {
                        per_rank2 = 8;
                    }
                    else if (adjBPratio2 >  (float) 0.18 && adjBPratio2 <=  (float) 0.19) {
                        per_rank2 = 9;
                    }
                    else {
                        per_rank2 = 10;
                    }
                }
                else if(adjBPratio2 <= (float) 0.10){
                    per_rank2 = 0;
                }
                else if(adjBPratio2 >  (float) 0.800){
                    per_rank2 = 100;
                }
                if(per_rank2 == 2222){
                    str_rank2.text = @"--";
                }
                else{
                    str_rank2.text = [NSString stringWithFormat:@"%d", per_rank2];
                }
            }
            else{
                per_rank2 = 2222;
                str_rank2.text = @"--";
            }
        }
        else{
            //female
            adjBPratio2 = [[NSString stringWithFormat:@"%.2f", adjBPratio2] floatValue];
            
            if(adjBPratio2 > 0){
                
                if(adjBPratio2 <= (float) 0.50 && adjBPratio2 >  (float) 0.46){
                    
                    
                    if (adjBPratio2 > (float) 0.46 && adjBPratio2 <= (float) 0.464) {
                        per_rank2 = 91;
                    }
                    else if (adjBPratio2 > (float) 0.464 && adjBPratio2 <= (float) 0.468) {
                        per_rank2 = 92;
                    }
                    else if (adjBPratio2 > (float) 0.468 && adjBPratio2 <= (float) 0.472) {
                        per_rank2 = 93;
                    }
                    else if (adjBPratio2 > (float) 0.472 && adjBPratio2 <= (float) 0.476) {
                        per_rank2 = 94;
                    }
                    else if (adjBPratio2 > (float) 0.476 && adjBPratio2 <= (float) 0.480) {
                        per_rank2 = 95;
                    }
                    else if (adjBPratio2 > (float) 0.480 && adjBPratio2 <= (float) 0.484) {
                        per_rank2 = 96;
                    }
                    else if (adjBPratio2 > (float) 0.484 && adjBPratio2 <= (float) 0.488) {
                        per_rank2 = 97;
                    }
                    else if (adjBPratio2 > (float) 0.488 && adjBPratio2 <= (float) 0.492) {
                        per_rank2 = 98;
                    }
                    else if (adjBPratio2 > (float) 0.492 && adjBPratio2 <= (float) 0.496) {
                        per_rank2 = 99;
                    }
                    else  {
                        per_rank2 = 100;
                    }
                    
                }
                else if(adjBPratio2 <= (float) 0.460 && adjBPratio2 >  (float) 0.420){
                    
                    
                    if (adjBPratio2 > (float) 0.420 && adjBPratio2 <= (float) 0.424) {
                        per_rank2 = 81;
                    }
                    else if (adjBPratio2 > (float) 0.424 && adjBPratio2 <= (float) 0.428) {
                        per_rank2 = 82;
                    }
                    else if (adjBPratio2 > (float) 0.428 && adjBPratio2 <= (float) 0.432) {
                        per_rank2 = 83;
                    }
                    else if (adjBPratio2 > (float) 0.432 && adjBPratio2 <= (float) 0.436) {
                        per_rank2 = 84;
                    }
                    else if (adjBPratio2 > (float) 0.436 && adjBPratio2 <= (float) 0.440) {
                        per_rank2 = 85;
                    }
                    else if (adjBPratio2 > (float) 0.440 && adjBPratio2 <= (float) 0.444) {
                        per_rank2 = 86;
                    }
                    else if (adjBPratio2 > (float) 0.444 && adjBPratio2 <= (float) 0.448) {
                        per_rank2 = 87;
                    }
                    else if (adjBPratio2 > (float) 0.448 && adjBPratio2 <= (float) 0.452) {
                        per_rank2 = 88;
                    }
                    else if (adjBPratio2 > (float) 0.452 && adjBPratio2 <= (float) 0.456) {
                        per_rank2 = 89;
                    }
                    else  {
                        per_rank2 = 90;
                    }

                    
                }////starts from here
                else if(adjBPratio2 <= (float) 0.42 && adjBPratio2 >  (float) 0.38){
                    
                    if (adjBPratio2 > (float) 0.38 && adjBPratio2 <= (float) 0.384) {
                        per_rank2 = 71;
                    }
                    else if (adjBPratio2 > (float) 0.384 && adjBPratio2 <= (float) 0.388) {
                        per_rank2 = 72;
                    }
                    else if (adjBPratio2 > (float) 0.388 && adjBPratio2 <= (float) 0.392) {
                        per_rank2 = 73;
                    }
                    else if (adjBPratio2 > (float) 0.392 && adjBPratio2 <= (float) 0.396) {
                        per_rank2 = 74;
                    }
                    else if (adjBPratio2 > (float) 0.396 && adjBPratio2 <= (float) 0.400) {
                        per_rank2 = 75;
                    }
                    else if (adjBPratio2 > (float) 0.400 && adjBPratio2 <= (float) 0.404) {
                        per_rank2 = 76;
                    }
                    else if (adjBPratio2 > (float) 0.404 && adjBPratio2 <= (float) 0.408) {
                        per_rank2 = 77;
                    }
                    else if (adjBPratio2 > (float) 0.408 && adjBPratio2 <= (float) 0.412) {
                        per_rank2 = 78;
                    }
                    else if (adjBPratio2 > (float) 0.412 && adjBPratio2 <= (float) 0.416) {
                        per_rank2 = 79;
                    }
                    else  {
                        per_rank2 = 80;
                    }
                    
                }
                else if(adjBPratio2 <= (float) 0.38 && adjBPratio2 >  (float) 0.34){
                    
                    if (adjBPratio2 > (float) 0.34 && adjBPratio2 <= (float) 0.344) {
                        per_rank2 = 61;
                    }
                    else if (adjBPratio2 > (float) 0.344 && adjBPratio2 <= (float) 0.348) {
                        per_rank2 = 62;
                    }
                    else if (adjBPratio2 > (float) 0.348 && adjBPratio2 <= (float) 0.352) {
                        per_rank2 = 63;
                    }
                    else if (adjBPratio2 > (float) 0.352 && adjBPratio2 <= (float) 0.356) {
                        per_rank2 = 64;
                    }
                    else if (adjBPratio2 > (float) 0.356 && adjBPratio2 <= (float) 0.360) {
                        per_rank2 = 65;
                    }
                    else if (adjBPratio2 > (float) 0.360 && adjBPratio2 <= (float) 0.364) {
                        per_rank2 = 66;
                    }
                    else if (adjBPratio2 > (float) 0.364 && adjBPratio2 <= (float) 0.368) {
                        per_rank2 = 67;
                    }
                    else if (adjBPratio2 > (float) 0.368 && adjBPratio2 <= (float) 0.372) {
                        per_rank2 = 68;
                    }
                    else if (adjBPratio2 > (float) 0.372 && adjBPratio2 <= (float) 0.376) {
                        per_rank2 = 69;
                    }
                    else  {
                        per_rank2 = 70;
                    }
                    
                }
                else if(adjBPratio2 <= (float) 0.340 && adjBPratio2 >  (float) 0.300){
                    
                    if (adjBPratio2 > (float) 0.300 && adjBPratio2 <= (float) 0.304) {
                        per_rank2 = 51;
                    }
                    else if (adjBPratio2 > (float) 0.304 && adjBPratio2 <= (float) 0.308) {
                        per_rank2 = 52;
                    }
                    else if (adjBPratio2 > (float) 0.308 && adjBPratio2 <= (float) 0.312) {
                        per_rank2 = 53;
                    }
                    else if (adjBPratio2 > (float) 0.312 && adjBPratio2 <= (float) 0.316) {
                        per_rank2 = 54;
                    }
                    else if (adjBPratio2 > (float) 0.316 && adjBPratio2 <= (float) 0.320) {
                        per_rank2 = 55;
                    }
                    else if (adjBPratio2 > (float) 0.320 && adjBPratio2 <= (float) 0.324) {
                        per_rank2 = 56;
                    }
                    else if (adjBPratio2 > (float) 0.324 && adjBPratio2 <= (float) 0.328) {
                        per_rank2 = 57;
                    }
                    else if (adjBPratio2 > (float) 0.328 && adjBPratio2 <= (float) 0.332) {
                        per_rank2 = 58;
                    }
                    else if (adjBPratio2 > (float) 0.332 && adjBPratio2 <= (float) 0.336) {
                        per_rank2 = 59;
                    }
                    else  {
                        per_rank2 = 60;
                    }

                }
                else if(adjBPratio2 <= (float) 0.30 && adjBPratio2 >  (float) 0.26){
                    
                    
                    if (adjBPratio2 > (float) 0.260 && adjBPratio2 <= (float) 0.264) {
                        per_rank2 = 41;
                    }
                    else if (adjBPratio2 > (float) 0.264 && adjBPratio2 <= (float) 0.268) {
                        per_rank2 = 42;
                    }
                    else if (adjBPratio2 > (float) 0.268 && adjBPratio2 <= (float) 0.272) {
                        per_rank2 = 43;
                    }
                    else if (adjBPratio2 > (float) 0.272 && adjBPratio2 <= (float) 0.276) {
                        per_rank2 = 44;
                    }
                    else if (adjBPratio2 > (float) 0.276 && adjBPratio2 <= (float) 0.280) {
                        per_rank2 = 45;
                    }
                    else if (adjBPratio2 > (float) 0.280 && adjBPratio2 <= (float) 0.284) {
                        per_rank2 = 46;
                    }
                    else if (adjBPratio2 > (float) 0.284 && adjBPratio2 <= (float) 0.288) {
                        per_rank2 = 47;
                    }
                    else if (adjBPratio2 > (float) 0.288 && adjBPratio2 <= (float) 0.292) {
                        per_rank2 = 48;
                    }
                    else if (adjBPratio2 > (float) 0.292 && adjBPratio2 <= (float) 0.296) {
                        per_rank2 = 49;
                    }
                    else  {
                        per_rank2 = 50;
                    }
    
                }
                else if(adjBPratio2 <= (float) 0.26  && adjBPratio2 > (float) 0.22){
                    
                    
                    if (adjBPratio2 > (float) 0.220 && adjBPratio2 <= (float) 0.224) {
                        per_rank2 = 31;
                    }
                    else if (adjBPratio2 > (float) 0.224 && adjBPratio2 <= (float) 0.228) {
                        per_rank2 = 32;
                    }
                    else if (adjBPratio2 > (float) 0.228 && adjBPratio2 <= (float) 0.232) {
                        per_rank2 = 33;
                    }
                    else if (adjBPratio2 > (float) 0.232 && adjBPratio2 <= (float) 0.236) {
                        per_rank2 = 34;
                    }
                    else if (adjBPratio2 > (float) 0.236 && adjBPratio2 <= (float) 0.240) {
                        per_rank2 = 35;
                    }
                    else if (adjBPratio2 > (float) 0.240 && adjBPratio2 <= (float) 0.244) {
                        per_rank2 = 36;
                    }
                    else if (adjBPratio2 > (float) 0.244 && adjBPratio2 <= (float) 0.248) {
                        per_rank2 = 37;
                    }
                    else if (adjBPratio2 > (float) 0.248 && adjBPratio2 <= (float) 0.252) {
                        per_rank2 = 38;
                    }
                    else if (adjBPratio2 > (float) 0.252 && adjBPratio2 <= (float) 0.256) {
                        per_rank2 = 39;
                    }
                    else  {
                        per_rank2 = 40;
                    }
                    
                }
                else if(adjBPratio2 <= (float) 0.22  && adjBPratio2 > (float) 0.18){
                    
                    if (adjBPratio2 > (float) 0.18 && adjBPratio2 <= (float) 0.184) {
                        per_rank2 = 21;
                    }
                    else if (adjBPratio2 > (float) 0.184 && adjBPratio2 <= (float) 0.188) {
                        per_rank2 = 22;
                    }
                    else if (adjBPratio2 > (float) 0.188 && adjBPratio2 <= (float) 0.192) {
                        per_rank2 = 23;
                    }
                    else if (adjBPratio2 > (float) 0.192 && adjBPratio2 <= (float) 0.196) {
                        per_rank2 = 24;
                    }
                    else if (adjBPratio2 > (float) 0.196 && adjBPratio2 <= (float) 0.200) {
                        per_rank2 = 25;
                    }
                    else if (adjBPratio2 > (float) 0.200 && adjBPratio2 <= (float) 0.204) {
                        per_rank2 = 26;
                    }
                    else if (adjBPratio2 > (float) 0.204 && adjBPratio2 <= (float) 0.208) {
                        per_rank2 = 27;
                    }
                    else if (adjBPratio2 > (float) 0.208 && adjBPratio2 <= (float) 0.212) {
                        per_rank2 = 28;
                    }
                    else if (adjBPratio2 > (float) 0.212 && adjBPratio2 <= (float) 0.216) {
                        per_rank2 = 29;
                    }
                    else  {
                        per_rank2 = 30;
                    }

                }
                else if(adjBPratio2 <= (float) 0.18  && adjBPratio2 > (float) 0.14){
                    
                    
                    if (adjBPratio2 > (float) 0.14 && adjBPratio2 <= (float) 0.144) {
                        per_rank2 = 11;
                    }
                    else if (adjBPratio2 > (float) 0.144 && adjBPratio2 <= (float) 0.148) {
                        per_rank2 = 12;
                    }
                    else if (adjBPratio2 > (float) 0.148 && adjBPratio2 <= (float) 0.152) {
                        per_rank2 = 13;
                    }
                    else if (adjBPratio2 > (float) 0.152 && adjBPratio2 <= (float) 0.156) {
                        per_rank2 = 14;
                    }
                    else if (adjBPratio2 > (float) 0.156 && adjBPratio2 <= (float) 0.160) {
                        per_rank2 = 15;
                    }
                    else if (adjBPratio2 > (float) 0.160 && adjBPratio2 <= (float) 0.164) {
                        per_rank2 = 16;
                    }
                    else if (adjBPratio2 > (float) 0.164 && adjBPratio2 <= (float) 0.168) {
                        per_rank2 = 17;
                    }
                    else if (adjBPratio2 > (float) 0.168 && adjBPratio2 <= (float) 0.172) {
                        per_rank2 = 18;
                    }
                    else if (adjBPratio2 > (float) 0.172 && adjBPratio2 <= (float) 0.176) {
                        per_rank2 = 19;
                    }
                    else  {
                        per_rank2 = 20;
                    }

                }
                else if(adjBPratio2 <= (float) 0.14  && adjBPratio2 > (float) 0.10){
                    
                    if (adjBPratio2 > (float) 0.100 && adjBPratio2 <= (float) 0.104) {
                        per_rank2 = 1;
                    }
                    else if (adjBPratio2 > (float) 0.104 && adjBPratio2 <= (float) 0.108) {
                        per_rank2 = 2;
                    }
                    else if (adjBPratio2 > (float) 0.108 && adjBPratio2 <= (float) 0.112) {
                        per_rank2 = 3;
                    }
                    else if (adjBPratio2 > (float) 0.112 && adjBPratio2 <= (float) 0.116) {
                        per_rank2 = 4;
                    }
                    else if (adjBPratio2 > (float) 0.116 && adjBPratio2 <= (float) 0.120) {
                        per_rank2 = 5;
                    }
                    else if (adjBPratio2 > (float) 0.120 && adjBPratio2 <= (float) 0.124) {
                        per_rank2 = 6;
                    }
                    else if (adjBPratio2 > (float) 0.124 && adjBPratio2 <= (float) 0.128) {
                        per_rank2 = 7;
                    }
                    else if (adjBPratio2 > (float) 0.128 && adjBPratio2 <= (float) 0.132) {
                        per_rank2 = 8;
                    }
                    else if (adjBPratio2 > (float) 0.132 && adjBPratio2 <= (float) 0.136) {
                        per_rank2 = 9;
                    }
                    else  {
                        per_rank2 = 10;
                    }

                }
                else if(adjBPratio2 <= (float) 0.10){
                    per_rank2 = 0;
                }
                else if(adjBPratio2 >  (float) 0.50){
                    per_rank2 = 100;
                }
                if(per_rank2 == 2222){
                    str_rank2.text = @"--";
                }
                else{
                    str_rank2.text = [NSString stringWithFormat:@"%d", per_rank2];
                }
                
            }
            else{
                per_rank2 = 2222;
                str_rank2.text = @"--";
            }
        }
        
        strg_per_rank2 = per_rank2;
        
        if([str_rank2.text isEqualToString:@"--"]){
            [upElementArray replaceObjectAtIndex:136 withObject:@"0"];
        }
        else{
            [upElementArray replaceObjectAtIndex:136 withObject:[NSString stringWithFormat:@"%.0f",strg_per_rank2]];
        }
        
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        //    if(total_percent_rank != 0.0)
        //        str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rank];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;//(100 - total_percent_rank);
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    else{
        str_pred2.text = @"";
        str_sw2.text = @"";
        str_rank2.text  = @"--";
        [upElementArray replaceObjectAtIndex:136 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;//(100 - total_percent_rank);
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    
}

-(void)getLatPullValue:(float)kgValue :(float)respValue{
    
    if(kgValue > 0.0 && respValue > 0.0){
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        float bprm3 = kgValue*2.2/(1.0278-(respValue*0.0278));
        float BPRMkg3 =  bprm3/2.2;
        str_pred3.text = [NSString stringWithFormat:@"%.1f", BPRMkg3];
        
        float BPratio3  =  BPRMkg3/clientWeight;
        float adjBPratio3  =  ageCorrection*BPratio3;
        
        if(isnan(adjBPratio3)){
            str_sw3.text = @"";
        }
        else if(isinf(adjBPratio3)){
            str_sw3.text = @"";
        }
        else if(adjBPratio3 == 0.0){
            str_sw3.text = @"";
        }
        else{
            str_sw3.text = [NSString stringWithFormat:@"%.2f", adjBPratio3];
        }
        adjBPratio3 = [[NSString stringWithFormat:@"%.3f", adjBPratio3] floatValue];
        
        [upElementArray replaceObjectAtIndex:137 withObject:str_sw3.text];
        
        NSInteger per_rank3 = 2222;
        
        if (clientGender == 0) {
            if(adjBPratio3 > 0){
                
                if(adjBPratio3 <= (float) 1.35 && adjBPratio3 >  (float) 1.25){
                    
                    
                    if (adjBPratio3 > (float) 1.25 && adjBPratio3 <=  (float) 1.26) {
                        per_rank3 = 91;
                    }
                    else if (adjBPratio3 >  (float) 1.26 && adjBPratio3 <=  (float) 1.27) {
                        per_rank3 = 92;
                    }
                    else if (adjBPratio3 >  (float) 1.27 && adjBPratio3 <=  (float) 1.28) {
                        per_rank3 = 93;
                    }
                    else if (adjBPratio3 >  (float) 1.28 && adjBPratio3 <=  (float) 1.29) {
                        per_rank3 = 94;
                    }
                    else if (adjBPratio3 >  (float) 1.29 && adjBPratio3 <=  (float) 1.30) {
                        per_rank3 = 95;
                    }
                    else if (adjBPratio3 >  (float) 1.30 && adjBPratio3 <=  (float) 1.31) {
                        per_rank3 = 96;
                    }
                    else if (adjBPratio3 >  (float) 1.31 && adjBPratio3 <=  (float) 1.32) {
                        per_rank3 = 97;
                    }
                    else if (adjBPratio3 >  (float) 1.32 && adjBPratio3 <=  (float) 1.33) {
                        per_rank3 = 98;
                    }
                    else if (adjBPratio3 >  (float) 1.33 && adjBPratio3 <=  (float) 1.34) {
                        per_rank3 = 99;
                    }
                    else {
                        per_rank3 = 100;
                    }
                    
                }
                else if(adjBPratio3 <= (float) 1.25  && adjBPratio3 >  (float) 1.20){
                    
                    
                    if (adjBPratio3 > (float) 1.20 && adjBPratio3 <=  (float) 1.205) {
                        per_rank3 = 81;
                    }
                    else if (adjBPratio3 >  (float) 1.205 && adjBPratio3 <=  (float) 1.210) {
                        per_rank3 = 82;
                    }
                    else if (adjBPratio3 >  (float) 1.210 && adjBPratio3 <=  (float) 1.215) {
                        per_rank3 = 83;
                    }
                    else if (adjBPratio3 >  (float) 1.215 && adjBPratio3 <=  (float) 1.220) {
                        per_rank3 = 84;
                    }
                    else if (adjBPratio3 >  (float) 1.220 && adjBPratio3 <=  (float) 1.225) {
                        per_rank3 = 85;
                    }
                    else if (adjBPratio3 >  (float) 1.225 && adjBPratio3 <=  (float) 1.230) {
                        per_rank3 = 86;
                    }
                    else if (adjBPratio3 >  (float) 1.230 && adjBPratio3 <=  (float) 1.235) {
                        per_rank3 = 87;
                    }
                    else if (adjBPratio3 >  (float) 1.235 && adjBPratio3 <=  (float) 1.240) {
                        per_rank3 = 88;
                    }
                    else if (adjBPratio3 >  (float) 1.240 && adjBPratio3 <=  (float) 1.245) {
                        per_rank3 = 89;
                    }
                    else {
                        per_rank3 = 90;
                    }
                    
                }
                else if(adjBPratio3 <= (float) 1.20 && adjBPratio3 >  (float) 1.15){
                    
                    
                    if (adjBPratio3 > (float) 1.150 && adjBPratio3 <=  (float) 1.155) {
                        per_rank3 = 71;
                    }
                    else if (adjBPratio3 >  (float) 1.155 && adjBPratio3 <=  (float) 1.160) {
                        per_rank3 = 72;
                    }
                    else if (adjBPratio3 >  (float) 1.160 && adjBPratio3 <=  (float) 1.165) {
                        per_rank3 = 73;
                    }
                    else if (adjBPratio3 >  (float) 1.165 && adjBPratio3 <=  (float) 1.170) {
                        per_rank3 = 74;
                    }
                    else if (adjBPratio3 >  (float) 1.170 && adjBPratio3 <=  (float) 1.175) {
                        per_rank3 = 75;
                    }
                    else if (adjBPratio3 >  (float) 1.175 && adjBPratio3 <=  (float) 1.180) {
                        per_rank3 = 76;
                    }
                    else if (adjBPratio3 >  (float) 1.180 && adjBPratio3 <=  (float) 1.185) {
                        per_rank3 = 77;
                    }
                    else if (adjBPratio3 >  (float) 1.185 && adjBPratio3 <=  (float) 1.190) {
                        per_rank3 = 78;
                    }
                    else if (adjBPratio3 >  (float) 1.190 && adjBPratio3 <=  (float) 1.195) {
                        per_rank3 = 79;
                    }
                    else {
                        per_rank3 = 80;
                    }
                }
                else if(adjBPratio3 <= (float) 1.15 && adjBPratio3 >  (float) 1.10){
                    
                    
                    if (adjBPratio3 > (float) 1.10 && adjBPratio3 <=  (float) 1.105) {
                        per_rank3 = 61;
                    }
                    else if (adjBPratio3 >  (float) 1.105 && adjBPratio3 <=  (float) 1.110) {
                        per_rank3 = 62;
                    }
                    else if (adjBPratio3 >  (float) 1.110 && adjBPratio3 <=  (float) 1.115) {
                        per_rank3 = 63;
                    }
                    else if (adjBPratio3 >  (float) 1.115 && adjBPratio3 <=  (float) 1.120) {
                        per_rank3 = 64;
                    }
                    else if (adjBPratio3 >  (float) 1.120 && adjBPratio3 <=  (float) 1.125) {
                        per_rank3 = 65;
                    }
                    else if (adjBPratio3 >  (float) 1.125 && adjBPratio3 <=  (float) 1.130) {
                        per_rank3 = 66;
                    }
                    else if (adjBPratio3 >  (float) 1.130 && adjBPratio3 <=  (float) 1.135) {
                        per_rank3 = 67;
                    }
                    else if (adjBPratio3 >  (float) 1.135 && adjBPratio3 <=  (float) 1.140) {
                        per_rank3 = 68;
                    }
                    else if (adjBPratio3 >  (float) 1.140 && adjBPratio3 <=  (float) 1.145) {
                        per_rank3 = 69;
                    }
                    else {
                        per_rank3 = 70;
                    }
                }
                else if(adjBPratio3 <= (float) 1.10 && adjBPratio3 >  (float) 1.05){
                    
                    
                    if (adjBPratio3 > (float) 1.05 && adjBPratio3 <=  (float) 1.010) {
                        per_rank3 = 51;
                    }
                    else if (adjBPratio3 >  (float) 1.010 && adjBPratio3 <=  (float) 1.020) {
                        per_rank3 = 52;
                    }
                    else if (adjBPratio3 >  (float) 1.020 && adjBPratio3 <=  (float) 1.030) {
                        per_rank3 = 53;
                    }
                    else if (adjBPratio3 >  (float) 1.030 && adjBPratio3 <=  (float) 1.040) {
                        per_rank3 = 54;
                    }
                    else if (adjBPratio3 >  (float) 1.040 && adjBPratio3 <=  (float) 1.050) {
                        per_rank3 = 55;
                    }
                    else if (adjBPratio3 >  (float) 1.050 && adjBPratio3 <=  (float) 1.060) {
                        per_rank3 = 56;
                    }
                    else if (adjBPratio3 >  (float) 1.060 && adjBPratio3 <=  (float) 1.070) {
                        per_rank3 = 57;
                    }
                    else if (adjBPratio3 >  (float) 1.070 && adjBPratio3 <=  (float) 1.080) {
                        per_rank3 = 58;
                    }
                    else if (adjBPratio3 >  (float) 1.080 && adjBPratio3 <=  (float) 1.090) {
                        per_rank3 = 59;
                    }
                    else {
                        per_rank3 = 60;
                    }
                }
                else if(adjBPratio3 <= (float) 1.050  && adjBPratio3 > (float) 1.000){
                    
                    if (adjBPratio3 > (float) 1.00 && adjBPratio3 <=  (float) 1.005) {
                        per_rank3 = 41;
                    }
                    else if (adjBPratio3 >  (float) 1.005 && adjBPratio3 <=  (float) 1.010) {
                        per_rank3 = 42;
                    }
                    else if (adjBPratio3 >  (float) 1.010 && adjBPratio3 <=  (float) 1.015) {
                        per_rank3 = 43;
                    }
                    else if (adjBPratio3 >  (float) 1.015 && adjBPratio3 <=  (float) 1.020) {
                        per_rank3 = 44;
                    }
                    else if (adjBPratio3 >  (float) 1.020 && adjBPratio3 <=  (float) 1.025) {
                        per_rank3 = 45;
                    }
                    else if (adjBPratio3 >  (float) 1.025 && adjBPratio3 <=  (float) 1.030) {
                        per_rank3 = 46;
                    }
                    else if (adjBPratio3 >  (float) 1.030 && adjBPratio3 <=  (float) 1.035) {
                        per_rank3 = 47;
                    }
                    else if (adjBPratio3 >  (float) 1.035 && adjBPratio3 <=  (float) 1.040) {
                        per_rank3 = 48;
                    }
                    else if (adjBPratio3 >  (float) 1.040 && adjBPratio3 <=  (float) 1.045) {
                        per_rank3 = 49;
                    }
                    else {
                        per_rank3 = 50;
                    }
                    
                }
                else if(adjBPratio3 <= (float) 1.000  && adjBPratio3 > (float) 0.950){
                    
                    if (adjBPratio3 > (float) 0.950 && adjBPratio3 <=  (float) 0.955) {
                        per_rank3 = 31;
                    }
                    else if (adjBPratio3 >  (float) 0.955 && adjBPratio3 <=  (float) 0.960) {
                        per_rank3 = 32;
                    }
                    else if (adjBPratio3 >  (float) 0.960 && adjBPratio3 <=  (float) 0.965) {
                        per_rank3 = 33;
                    }
                    else if (adjBPratio3 >  (float) 0.965 && adjBPratio3 <=  (float) 0.970) {
                        per_rank3 = 34;
                    }
                    else if (adjBPratio3 >  (float) 0.970 && adjBPratio3 <=  (float) 0.975) {
                        per_rank3 = 35;
                    }
                    else if (adjBPratio3 >  (float) 0.975 && adjBPratio3 <=  (float) 0.980) {
                        per_rank3 = 36;
                    }
                    else if (adjBPratio3 >  (float) 0.980 && adjBPratio3 <=  (float) 0.985) {
                        per_rank3 = 37;
                    }
                    else if (adjBPratio3 >  (float) 0.985 && adjBPratio3 <=  (float) 0.990) {
                        per_rank3 = 38;
                    }
                    else if (adjBPratio3 >  (float) 0.990 && adjBPratio3 <=  (float) 0.995) {
                        per_rank3 = 39;
                    }
                    else {
                        per_rank3 = 40;
                    }
                }
                else if(adjBPratio3 <= (float) 0.950  && adjBPratio3 > (float) 0.900){
                    
                    if (adjBPratio3 > (float) 0.900 && adjBPratio3 <=  (float) 0.905) {
                        per_rank3 = 21;
                    }
                    else if (adjBPratio3 >  (float) 0.905 && adjBPratio3 <=  (float) 0.910) {
                        per_rank3 = 22;
                    }
                    else if (adjBPratio3 >  (float) 0.910 && adjBPratio3 <=  (float) 0.915) {
                        per_rank3 = 23;
                    }
                    else if (adjBPratio3 >  (float) 0.915 && adjBPratio3 <=  (float) 0.920) {
                        per_rank3 = 24;
                    }
                    else if (adjBPratio3 >  (float) 0.920 && adjBPratio3 <=  (float) 0.925) {
                        per_rank3 = 25;
                    }
                    else if (adjBPratio3 >  (float) 0.925 && adjBPratio3 <=  (float) 0.930) {
                        per_rank3 = 26;
                    }
                    else if (adjBPratio3 >  (float) 0.930 && adjBPratio3 <=  (float) 0.935) {
                        per_rank3 = 27;
                    }
                    else if (adjBPratio3 >  (float) 0.935 && adjBPratio3 <=  (float) 0.940) {
                        per_rank3 = 28;
                    }
                    else if (adjBPratio3 >  (float) 0.940 && adjBPratio3 <=  (float) 0.945) {
                        per_rank3 = 29;
                    }
                    else {
                        per_rank3 = 30;
                    }
                    
                }
                else if(adjBPratio3 <= (float) 0.900  && adjBPratio3 > (float) 0.750){
                    
                    if (adjBPratio3 > (float) 0.750 && adjBPratio3 <=  (float) 0.765) {
                        per_rank3 = 11;
                    }
                    else if (adjBPratio3 >  (float) 0.765 && adjBPratio3 <=  (float) 0.780) {
                        per_rank3 = 12;
                    }
                    else if (adjBPratio3 >  (float) 0.780 && adjBPratio3 <=  (float) 0.795) {
                        per_rank3 = 13;
                    }
                    else if (adjBPratio3 >  (float) 0.795 && adjBPratio3 <=  (float) 0.810) {
                        per_rank3 = 14;
                    }
                    else if (adjBPratio3 >  (float) 0.810 && adjBPratio3 <=  (float) 0.825) {
                        per_rank3 = 15;
                    }
                    else if (adjBPratio3 >  (float) 0.825 && adjBPratio3 <=  (float) 0.840) {
                        per_rank3 = 16;
                    }
                    else if (adjBPratio3 >  (float) 0.840 && adjBPratio3 <=  (float) 0.855) {
                        per_rank3 = 17;
                    }
                    else if (adjBPratio3 >  (float) 0.855 && adjBPratio3 <=  (float) 0.970) {
                        per_rank3 = 18;
                    }
                    else if (adjBPratio3 >  (float) 0.970 && adjBPratio3 <=  (float) 0.985) {
                        per_rank3 = 19;
                    }
                    else {
                        per_rank3 = 20;
                    }
                    
                }
                else if(adjBPratio3 <= (float) 0.75  && adjBPratio3 > (float) 0.65){
                    
                    if (adjBPratio3 > (float) 0.65 && adjBPratio3 <=  (float) 0.66) {
                        per_rank3 = 1;
                    }
                    else if (adjBPratio3 >  (float) 0.66 && adjBPratio3 <=  (float) 0.67) {
                        per_rank3 = 2;
                    }
                    else if (adjBPratio3 >  (float) 0.67 && adjBPratio3 <=  (float) 0.68) {
                        per_rank3 = 3;
                    }
                    else if (adjBPratio3 >  (float) 0.68 && adjBPratio3 <=  (float) 0.69) {
                        per_rank3 = 4;
                    }
                    else if (adjBPratio3 >  (float) 0.69 && adjBPratio3 <=  (float) 0.70) {
                        per_rank3 = 5;
                    }
                    else if (adjBPratio3 >  (float) 0.70 && adjBPratio3 <=  (float) 0.71) {
                        per_rank3 = 6;
                    }
                    else if (adjBPratio3 >  (float) 0.71 && adjBPratio3 <=  (float) 0.72) {
                        per_rank3 = 7;
                    }
                    else if (adjBPratio3 >  (float) 0.72 && adjBPratio3 <=  (float) 0.73) {
                        per_rank3 = 8;
                    }
                    else if (adjBPratio3 >  (float) 0.73 && adjBPratio3 <=  (float) 0.74) {
                        per_rank3 = 9;
                    }
                    else {
                        per_rank3 = 10;
                    }
                }
                
                else if(adjBPratio3 > (float) 1.35){
                    per_rank3 = 100;
                }
                else if(adjBPratio3 <= (float) 0.65){
                    per_rank3 = 0;
                }
                
                if(per_rank3 == 2222){
                    str_rank3.text = @"--";
                }
                else{
                    str_rank3.text = [NSString stringWithFormat:@"%d", per_rank3];
                }
            }
            else{
                per_rank3 = 2222;
                str_rank3.text = @"--";
            }
        }
        else{
            //female
            
            
            if(adjBPratio3 > 0){
                
                if(adjBPratio3 <= (float) 0.85 && adjBPratio3 >  (float) 0.80){
                    
                    if (adjBPratio3 > (float) 0.800 && adjBPratio3 <=  (float) 0.805) {
                        per_rank3 = 91;
                    }
                    else if (adjBPratio3 >  (float) 0.805 && adjBPratio3 <=  (float) 0.810) {
                        per_rank3 = 92;
                    }
                    else if (adjBPratio3 >  (float) 0.810 && adjBPratio3 <=  (float) 0.815) {
                        per_rank3 = 93;
                    }
                    else if (adjBPratio3 >  (float) 0.815 && adjBPratio3 <=  (float) 0.820) {
                        per_rank3 = 94;
                    }
                    else if (adjBPratio3 >  (float) 0.820 && adjBPratio3 <=  (float) 0.825) {
                        per_rank3 = 95;
                    }
                    else if (adjBPratio3 >  (float) 0.825 && adjBPratio3 <=  (float) 0.830) {
                        per_rank3 = 96;
                    }
                    else if (adjBPratio3 >  (float) 0.830 && adjBPratio3 <=  (float) 0.835) {
                        per_rank3 = 97;
                    }
                    else if (adjBPratio3 >  (float) 0.835 && adjBPratio3 <=  (float) 0.840) {
                        per_rank3 = 98;
                    }
                    else if (adjBPratio3 >  (float) 0.840 && adjBPratio3 <=  (float) 0.845) {
                        per_rank3 = 99;
                    }
                    else {
                        per_rank3 = 100;
                    }
                }
                else if(adjBPratio3 <= (float) 0.80  && adjBPratio3 >  (float) 0.75){
                    
                    if (adjBPratio3 > (float) 0.750 && adjBPratio3 <=  (float) 0.755) {
                        per_rank3 = 81;
                    }
                    else if (adjBPratio3 >  (float) 0.755 && adjBPratio3 <=  (float) 0.760) {
                        per_rank3 = 82;
                    }
                    else if (adjBPratio3 >  (float) 0.760 && adjBPratio3 <=  (float) 0.765) {
                        per_rank3 = 83;
                    }
                    else if (adjBPratio3 >  (float) 0.765 && adjBPratio3 <=  (float) 0.770) {
                        per_rank3 = 84;
                    }
                    else if (adjBPratio3 >  (float) 0.770 && adjBPratio3 <=  (float) 0.775) {
                        per_rank3 = 85;
                    }
                    else if (adjBPratio3 >  (float) 0.775 && adjBPratio3 <=  (float) 0.780) {
                        per_rank3 = 86;
                    }
                    else if (adjBPratio3 >  (float) 0.780 && adjBPratio3 <=  (float) 0.785) {
                        per_rank3 = 87;
                    }
                    else if (adjBPratio3 >  (float) 0.785 && adjBPratio3 <=  (float) 0.790) {
                        per_rank3 = 88;
                    }
                    else if (adjBPratio3 >  (float) 0.790 && adjBPratio3 <=  (float) 0.795) {
                        per_rank3 = 89;
                    }
                    else {
                        per_rank3 = 90;
                    }
                }
                else if(adjBPratio3 <= (float) 0.75 && adjBPratio3 >  (float) 0.70){
                    
                    
                    if (adjBPratio3 > (float) 0.700 && adjBPratio3 <=  (float) 0.705) {
                        per_rank3 = 71;
                    }
                    else if (adjBPratio3 >  (float) 0.705 && adjBPratio3 <=  (float) 0.710) {
                        per_rank3 = 72;
                    }
                    else if (adjBPratio3 >  (float) 0.710 && adjBPratio3 <=  (float) 0.715) {
                        per_rank3 = 73;
                    }
                    else if (adjBPratio3 >  (float) 0.715 && adjBPratio3 <=  (float) 0.720) {
                        per_rank3 = 74;
                    }
                    else if (adjBPratio3 >  (float) 0.720 && adjBPratio3 <=  (float) 0.725) {
                        per_rank3 = 75;
                    }
                    else if (adjBPratio3 >  (float) 0.725 && adjBPratio3 <=  (float) 0.730) {
                        per_rank3 = 76;
                    }
                    else if (adjBPratio3 >  (float) 0.730 && adjBPratio3 <=  (float) 0.735) {
                        per_rank3 = 77;
                    }
                    else if (adjBPratio3 >  (float) 0.735 && adjBPratio3 <=  (float) 0.740) {
                        per_rank3 = 78;
                    }
                    else if (adjBPratio3 >  (float) 0.740 && adjBPratio3 <=  (float) 0.745) {
                        per_rank3 = 79;
                    }
                    else {
                        per_rank3 = 80;
                    }

                }
                else if(adjBPratio3 <= (float) 0.70 && adjBPratio3 >  (float) 0.65){
                    
                    
                    if (adjBPratio3 > (float) 0.650 && adjBPratio3 <=  (float) 0.655) {
                        per_rank3 = 61;
                    }
                    else if (adjBPratio3 >  (float) 0.655 && adjBPratio3 <=  (float) 0.660) {
                        per_rank3 = 62;
                    }
                    else if (adjBPratio3 >  (float) 0.660 && adjBPratio3 <=  (float) 0.665) {
                        per_rank3 = 63;
                    }
                    else if (adjBPratio3 >  (float) 0.665 && adjBPratio3 <=  (float) 0.670) {
                        per_rank3 = 64;
                    }
                    else if (adjBPratio3 >  (float) 0.670 && adjBPratio3 <=  (float) 0.675) {
                        per_rank3 = 65;
                    }
                    else if (adjBPratio3 >  (float) 0.675 && adjBPratio3 <=  (float) 0.680) {
                        per_rank3 = 66;
                    }
                    else if (adjBPratio3 >  (float) 0.680 && adjBPratio3 <=  (float) 0.685) {
                        per_rank3 = 67;
                    }
                    else if (adjBPratio3 >  (float) 0.685 && adjBPratio3 <=  (float) 0.690) {
                        per_rank3 = 68;
                    }
                    else if (adjBPratio3 >  (float) 0.690 && adjBPratio3 <=  (float) 0.695) {
                        per_rank3 = 69;
                    }
                    else {
                        per_rank3 = 70;
                    }
                }
                else if(adjBPratio3 <= (float) 0.65 && adjBPratio3 >  (float) 0.60){
                    
                    if (adjBPratio3 > (float) 0.600 && adjBPratio3 <=  (float) 0.605) {
                        per_rank3 = 51;
                    }
                    else if (adjBPratio3 >  (float) 0.605 && adjBPratio3 <=  (float) 0.610) {
                        per_rank3 = 52;
                    }
                    else if (adjBPratio3 >  (float) 0.610 && adjBPratio3 <=  (float) 0.615) {
                        per_rank3 = 53;
                    }
                    else if (adjBPratio3 >  (float) 0.615 && adjBPratio3 <=  (float) 0.620) {
                        per_rank3 = 54;
                    }
                    else if (adjBPratio3 >  (float) 0.620 && adjBPratio3 <=  (float) 0.625) {
                        per_rank3 = 55;
                    }
                    else if (adjBPratio3 >  (float) 0.625 && adjBPratio3 <=  (float) 0.630) {
                        per_rank3 = 56;
                    }
                    else if (adjBPratio3 >  (float) 0.630 && adjBPratio3 <=  (float) 0.635) {
                        per_rank3 = 57;
                    }
                    else if (adjBPratio3 >  (float) 0.635 && adjBPratio3 <=  (float) 0.640) {
                        per_rank3 = 58;
                    }
                    else if (adjBPratio3 >  (float) 0.640 && adjBPratio3 <=  (float) 0.645) {
                        per_rank3 = 59;
                    }
                    else {
                        per_rank3 = 60;
                    }
                }
                else if(adjBPratio3 <= (float) 0.60  && adjBPratio3 > (float) 0.55){
                    
                    if (adjBPratio3 > (float) 0.550 && adjBPratio3 <=  (float) 0.555) {
                        per_rank3 = 41;
                    }
                    else if (adjBPratio3 >  (float) 0.555 && adjBPratio3 <=  (float) 0.560) {
                        per_rank3 = 42;
                    }
                    else if (adjBPratio3 >  (float) 0.560 && adjBPratio3 <=  (float) 0.565) {
                        per_rank3 = 43;
                    }
                    else if (adjBPratio3 >  (float) 0.565 && adjBPratio3 <=  (float) 0.570) {
                        per_rank3 = 44;
                    }
                    else if (adjBPratio3 >  (float) 0.570 && adjBPratio3 <=  (float) 0.575) {
                        per_rank3 = 45;
                    }
                    else if (adjBPratio3 >  (float) 0.575 && adjBPratio3 <=  (float) 0.580) {
                        per_rank3 = 46;
                    }
                    else if (adjBPratio3 >  (float) 0.580 && adjBPratio3 <=  (float) 0.585) {
                        per_rank3 = 47;
                    }
                    else if (adjBPratio3 >  (float) 0.585 && adjBPratio3 <=  (float) 0.590) {
                        per_rank3 = 48;
                    }
                    else if (adjBPratio3 >  (float) 0.590 && adjBPratio3 <=  (float) 0.595) {
                        per_rank3 = 49;
                    }
                    else {
                        per_rank3 = 50;
                    }
                }
                else if(adjBPratio3 <= (float) 0.55  && adjBPratio3 > (float) 0.50){
                    
                    if (adjBPratio3 > (float) 0.500 && adjBPratio3 <=  (float) 0.505) {
                        per_rank3 = 31;
                    }
                    else if (adjBPratio3 >  (float) 0.505 && adjBPratio3 <=  (float) 0.510) {
                        per_rank3 = 32;
                    }
                    else if (adjBPratio3 >  (float) 0.510 && adjBPratio3 <=  (float) 0.515) {
                        per_rank3 = 33;
                    }
                    else if (adjBPratio3 >  (float) 0.515 && adjBPratio3 <=  (float) 0.520) {
                        per_rank3 = 34;
                    }
                    else if (adjBPratio3 >  (float) 0.520 && adjBPratio3 <=  (float) 0.525) {
                        per_rank3 = 35;
                    }
                    else if (adjBPratio3 >  (float) 0.525 && adjBPratio3 <=  (float) 0.530) {
                        per_rank3 = 36;
                    }
                    else if (adjBPratio3 >  (float) 0.530 && adjBPratio3 <=  (float) 0.535) {
                        per_rank3 = 37;
                    }
                    else if (adjBPratio3 >  (float) 0.535 && adjBPratio3 <=  (float) 0.540) {
                        per_rank3 = 38;
                    }
                    else if (adjBPratio3 >  (float) 0.540 && adjBPratio3 <=  (float) 0.545) {
                        per_rank3 = 39;
                    }
                    else {
                        per_rank3 = 40;
                    }

                }
                else if(adjBPratio3 <= (float) 0.50  && adjBPratio3 > (float) 0.45){
                    
                    if (adjBPratio3 > (float) 0.450 && adjBPratio3 <=  (float) 0.455) {
                        per_rank3 = 21;
                    }
                    else if (adjBPratio3 >  (float) 0.455 && adjBPratio3 <=  (float) 0.460) {
                        per_rank3 = 22;
                    }
                    else if (adjBPratio3 >  (float) 0.460 && adjBPratio3 <=  (float) 0.465) {
                        per_rank3 = 23;
                    }
                    else if (adjBPratio3 >  (float) 0.465 && adjBPratio3 <=  (float) 0.470) {
                        per_rank3 = 24;
                    }
                    else if (adjBPratio3 >  (float) 0.470 && adjBPratio3 <=  (float) 0.475) {
                        per_rank3 = 25;
                    }
                    else if (adjBPratio3 >  (float) 0.475 && adjBPratio3 <=  (float) 0.480) {
                        per_rank3 = 26;
                    }
                    else if (adjBPratio3 >  (float) 0.480 && adjBPratio3 <=  (float) 0.485) {
                        per_rank3 = 27;
                    }
                    else if (adjBPratio3 >  (float) 0.485 && adjBPratio3 <=  (float) 0.490) {
                        per_rank3 = 28;
                    }
                    else if (adjBPratio3 >  (float) 0.490 && adjBPratio3 <=  (float) 0.495) {
                        per_rank3 = 29;
                    }
                    else {
                        per_rank3 = 30;
                    }
                }
                else if(adjBPratio3 <= (float) 0.45  && adjBPratio3 >(float) 0.40){
                    
                    if (adjBPratio3 > (float) 0.400 && adjBPratio3 <=  (float) 0.405) {
                        per_rank3 = 11;
                    }
                    else if (adjBPratio3 >  (float) 0.405 && adjBPratio3 <=  (float) 0.410) {
                        per_rank3 = 12;
                    }
                    else if (adjBPratio3 >  (float) 0.410 && adjBPratio3 <=  (float) 0.415) {
                        per_rank3 = 13;
                    }
                    else if (adjBPratio3 >  (float) 0.415 && adjBPratio3 <=  (float) 0.420) {
                        per_rank3 = 14;
                    }
                    else if (adjBPratio3 >  (float) 0.420 && adjBPratio3 <=  (float) 0.425) {
                        per_rank3 = 15;
                    }
                    else if (adjBPratio3 >  (float) 0.425 && adjBPratio3 <=  (float) 0.430) {
                        per_rank3 = 16;
                    }
                    else if (adjBPratio3 >  (float) 0.430 && adjBPratio3 <=  (float) 0.435) {
                        per_rank3 = 17;
                    }
                    else if (adjBPratio3 >  (float) 0.435 && adjBPratio3 <=  (float) 0.440) {
                        per_rank3 = 18;
                    }
                    else if (adjBPratio3 >  (float) 0.440 && adjBPratio3 <=  (float) 0.445) {
                        per_rank3 = 19;
                    }
                    else {
                        per_rank3 = 20;
                    }

                }
                else if(adjBPratio3 <= (float) 0.40  && adjBPratio3 > (float) 0.35){
                    
                    if (adjBPratio3 > (float) 0.350 && adjBPratio3 <=  (float) 0.355) {
                        per_rank3 = 1;
                    }
                    else if (adjBPratio3 >  (float) 0.355 && adjBPratio3 <=  (float) 0.360) {
                        per_rank3 = 2;
                    }
                    else if (adjBPratio3 >  (float) 0.360 && adjBPratio3 <=  (float) 0.365) {
                        per_rank3 = 3;
                    }
                    else if (adjBPratio3 >  (float) 0.365 && adjBPratio3 <=  (float) 0.370) {
                        per_rank3 = 4;
                    }
                    else if (adjBPratio3 >  (float) 0.370 && adjBPratio3 <=  (float) 0.375) {
                        per_rank3 = 5;
                    }
                    else if (adjBPratio3 >  (float) 0.375 && adjBPratio3 <=  (float) 0.380) {
                        per_rank3 = 6;
                    }
                    else if (adjBPratio3 >  (float) 0.380 && adjBPratio3 <=  (float) 0.385) {
                        per_rank3 = 7;
                    }
                    else if (adjBPratio3 >  (float) 0.385 && adjBPratio3 <=  (float) 0.390) {
                        per_rank3 = 8;
                    }
                    else if (adjBPratio3 >  (float) 0.390 && adjBPratio3 <=  (float) 0.395) {
                        per_rank3 = 9;
                    }
                    else {
                        per_rank3 = 10;
                    }
                }
                else if(adjBPratio3 > (float) 0.85){
                    per_rank3 = 100;
                }
                else if(adjBPratio3 <= (float) 0.35){
                    per_rank3 = 0;
                }
                
                if(per_rank3 == 2222){
                    str_rank3.text = @"--";
                }
                else{
                    str_rank3.text = [NSString stringWithFormat:@"%d", per_rank3];
                }
                
            }
            else{
                per_rank3 = 2222;
                str_rank3.text = @"--";
            }
           
        }

        
        
        strg_per_rank3 = per_rank3;
        
        if([str_rank3.text isEqualToString:@"--"]){
            [upElementArray replaceObjectAtIndex:138 withObject:@"0"];
        }
        else{
            [upElementArray replaceObjectAtIndex:138 withObject:[NSString stringWithFormat:@"%.0f",strg_per_rank3]];
        }
        
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;//(100 - total_percent_rank);
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    else{
        str_pred3.text = @"";
        str_sw3.text = @"";
        str_rank3.text  = @"--";
        [upElementArray replaceObjectAtIndex:138 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    
}
-(void)getLegPressValue:(float)kgValue :(float)respValue{
    
    if(kgValue > 0.0 && respValue > 0.0){
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        float bprm4 = kgValue*2.2/(1.0278-(respValue*0.0278));
        float BPRMkg4 =  bprm4/2.2;
        str_pred4.text = [NSString stringWithFormat:@"%.1f", BPRMkg4];
        
        float BPratio4  =  BPRMkg4/clientWeight;
        float adjBPratio4  =  ageCorrection*BPratio4;
        
        if(isnan(adjBPratio4)){
            str_sw4.text = @"";
        }
        else if(isinf(adjBPratio4)){
            str_sw4.text = @"";
        }
        else if(adjBPratio4 == 0.0){
            str_sw4.text = @"";
        }
        else{
            str_sw4.text = [NSString stringWithFormat:@"%.2f", adjBPratio4];
        }
        //adjBPratio4 = [str_sw4.text floatValue];
        adjBPratio4 = [[NSString stringWithFormat:@"%.3f", adjBPratio4] floatValue];
        [upElementArray replaceObjectAtIndex:139 withObject:str_sw4.text];
        NSInteger per_rank4 = 2222;
        
        
        if (clientGender==0) {
            //male
            
            if(adjBPratio4 > 0){
                
                if(adjBPratio4 <= (float) 3.50 && adjBPratio4 >  (float) 3.20){
                    
                    if (adjBPratio4 > (float) 3.20 && adjBPratio4 <= (float) 3.23) {
                        per_rank4 = 91;
                    }
                    else if (adjBPratio4 > (float) 3.23 && adjBPratio4 <= (float) 3.26) {
                        per_rank4 = 92;
                    }
                    else if (adjBPratio4 > (float) 3.26 && adjBPratio4 <= (float) 3.29) {
                        per_rank4 = 93;
                    }
                    else if (adjBPratio4 > (float) 3.29 && adjBPratio4 <= (float) 3.32) {
                        per_rank4 = 94;
                    }
                    else if (adjBPratio4 > (float) 3.32 && adjBPratio4 <= (float) 3.35) {
                        per_rank4 = 95;
                    }
                    else if (adjBPratio4 > (float) 3.35 && adjBPratio4 <= (float) 3.38) {
                        per_rank4 = 96;
                    }
                    else if (adjBPratio4 > (float) 3.38 && adjBPratio4 <= (float) 3.41) {
                        per_rank4 = 97;
                    }
                    else if (adjBPratio4 > (float) 3.41 && adjBPratio4 <= (float) 3.44) {
                        per_rank4 = 98;
                    }
                    else if (adjBPratio4 > (float) 3.44 && adjBPratio4 <= (float) 3.47) {
                        per_rank4 = 99;
                    }
                    else  {
                        per_rank4 = 100;
                    }
                }
                else if(adjBPratio4 <= (float) 3.20  && adjBPratio4 >  (float) 2.90){
                    
                    if (adjBPratio4 > (float) 2.90 && adjBPratio4 <= (float) 2.93) {
                        per_rank4 = 81;
                    }
                    else if (adjBPratio4 > (float) 2.93 && adjBPratio4 <= (float) 2.96) {
                        per_rank4 = 82;
                    }
                    else if (adjBPratio4 > (float) 2.96 && adjBPratio4 <= (float) 2.99) {
                        per_rank4 = 83;
                    }
                    else if (adjBPratio4 > (float) 2.99 && adjBPratio4 <= (float) 3.02) {
                        per_rank4 = 84;
                    }
                    else if (adjBPratio4 > (float) 3.02 && adjBPratio4 <= (float) 3.05) {
                        per_rank4 = 85;
                    }
                    else if (adjBPratio4 > (float) 3.05 && adjBPratio4 <= (float) 3.08) {
                        per_rank4 = 86;
                    }
                    else if (adjBPratio4 > (float) 3.08 && adjBPratio4 <= (float) 3.11) {
                        per_rank4 = 87;
                    }
                    else if (adjBPratio4 > (float) 3.11 && adjBPratio4 <= (float) 3.14) {
                        per_rank4 = 88;
                    }
                    else if (adjBPratio4 > (float) 3.14 && adjBPratio4 <= (float) 3.17) {
                        per_rank4 = 89;
                    }
                    else if (adjBPratio4 > (float) 3.17 && adjBPratio4 <= (float) 3.20) {
                        per_rank4 = 90;
                    }
                    else  {
                        per_rank4 = 100;
                    }
                }
                else if(adjBPratio4 <= (float) 2.90 && adjBPratio4 >  (float) 2.60){
                    
                    if (adjBPratio4 > (float) 2.60 && adjBPratio4 <= (float) 2.63) {
                        per_rank4 = 71;
                    }
                    else if (adjBPratio4 > (float) 2.63 && adjBPratio4 <= (float) 2.66) {
                        per_rank4 = 72;
                    }
                    else if (adjBPratio4 > (float) 2.66 && adjBPratio4 <= (float) 2.69) {
                        per_rank4 = 73;
                    }
                    else if (adjBPratio4 > (float) 2.69 && adjBPratio4 <= (float) 2.72) {
                        per_rank4 = 74;
                    }
                    else if (adjBPratio4 > (float) 2.72 && adjBPratio4 <= (float) 2.75) {
                        per_rank4 = 75;
                    }
                    else if (adjBPratio4 > (float) 2.75 && adjBPratio4 <= (float) 2.78) {
                        per_rank4 = 76;
                    }
                    else if (adjBPratio4 > (float) 2.78 && adjBPratio4 <= (float) 2.81) {
                        per_rank4 = 77;
                    }
                    else if (adjBPratio4 > (float) 2.81 && adjBPratio4 <= (float) 2.84) {
                        per_rank4 = 78;
                    }
                    else if (adjBPratio4 > (float) 2.84 && adjBPratio4 <= (float) 2.87) {
                        per_rank4 = 79;
                    }
                    else if (adjBPratio4 > (float) 2.87 && adjBPratio4 <= (float) 2.90) {
                        per_rank4 = 80;
                    }
                    else  {
                        per_rank4 = 80;
                    }
                }
                else if(adjBPratio4 <= (float) 2.60 && adjBPratio4 >  (float) 2.40){
                    
                    if (adjBPratio4 > (float) 2.40 && adjBPratio4 <= (float) 2.42) {
                        per_rank4 = 61;
                    }
                    else if (adjBPratio4 > (float) 2.42 && adjBPratio4 <= (float) 2.44) {
                        per_rank4 = 62;
                    }
                    else if (adjBPratio4 > (float) 2.44 && adjBPratio4 <= (float) 2.46) {
                        per_rank4 = 63;
                    }
                    else if (adjBPratio4 > (float) 2.46 && adjBPratio4 <= (float) 2.48) {
                        per_rank4 = 64;
                    }
                    else if (adjBPratio4 > (float) 2.48 && adjBPratio4 <= (float) 2.50) {
                        per_rank4 = 65;
                    }
                    else if (adjBPratio4 > (float) 2.50 && adjBPratio4 <= (float) 2.52) {
                        per_rank4 = 66;
                    }
                    else if (adjBPratio4 > (float) 2.52 && adjBPratio4 <= (float) 2.54) {
                        per_rank4 = 67;
                    }
                    else if (adjBPratio4 > (float) 2.54 && adjBPratio4 <= (float) 2.56) {
                        per_rank4 = 68;
                    }
                    else if (adjBPratio4 > (float) 2.56 && adjBPratio4 <= (float) 2.58) {
                        per_rank4 = 69;
                    }
                    else if (adjBPratio4 > (float) 2.58 && adjBPratio4 <= (float) 2.60) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjBPratio4 <= (float) 2.40 && adjBPratio4 >  (float) 2.20){
                    
                    if (adjBPratio4 > (float) 2.20 && adjBPratio4 <= (float) 2.22) {
                        per_rank4 = 51;
                    }
                    else if (adjBPratio4 > (float) 2.22 && adjBPratio4 <= (float) 2.24) {
                        per_rank4 = 52;
                    }
                    else if (adjBPratio4 > (float) 2.24 && adjBPratio4 <= (float) 2.26) {
                        per_rank4 = 53;
                    }
                    else if (adjBPratio4 > (float) 2.26 && adjBPratio4 <= (float) 2.28) {
                        per_rank4 = 54;
                    }
                    else if (adjBPratio4 > (float) 2.28 && adjBPratio4 <= (float) 2.30) {
                        per_rank4 = 55;
                    }
                    else if (adjBPratio4 > (float) 2.30 && adjBPratio4 <= (float) 2.32) {
                        per_rank4 = 56;
                    }
                    else if (adjBPratio4 > (float) 2.32 && adjBPratio4 <= (float) 2.34) {
                        per_rank4 = 57;
                    }
                    else if (adjBPratio4 > (float) 2.34 && adjBPratio4 <= (float) 2.36) {
                        per_rank4 = 58;
                    }
                    else if (adjBPratio4 > (float) 2.36 && adjBPratio4 <= (float) 2.38) {
                        per_rank4 = 59;
                    }
                    else if (adjBPratio4 > (float) 2.38 && adjBPratio4 <= (float) 2.30) {
                        per_rank4 = 60;
                    }
                    else  {
                        per_rank4 = 60;
                    }
                }
                else if(adjBPratio4 <= (float) 2.20 && adjBPratio4 >  (float) 2.00){
                    
                    if (adjBPratio4 > (float) 2.00 && adjBPratio4 <= (float) 2.02) {
                        per_rank4 = 41;
                    }
                    else if (adjBPratio4 > (float) 2.02 && adjBPratio4 <= (float) 2.04) {
                        per_rank4 = 42;
                    }
                    else if (adjBPratio4 > (float) 2.04 && adjBPratio4 <= (float) 2.06) {
                        per_rank4 = 43;
                    }
                    else if (adjBPratio4 > (float) 2.06 && adjBPratio4 <= (float) 2.08) {
                        per_rank4 = 44;
                    }
                    else if (adjBPratio4 > (float) 2.08 && adjBPratio4 <= (float) 2.00) {
                        per_rank4 = 45;
                    }
                    else if (adjBPratio4 > (float) 2.00 && adjBPratio4 <= (float) 2.12) {
                        per_rank4 = 46;
                    }
                    else if (adjBPratio4 > (float) 2.12 && adjBPratio4 <= (float) 2.14) {
                        per_rank4 = 47;
                    }
                    else if (adjBPratio4 > (float) 2.14 && adjBPratio4 <= (float) 2.16) {
                        per_rank4 = 48;
                    }
                    else if (adjBPratio4 > (float) 2.16 && adjBPratio4 <= (float) 2.18) {
                        per_rank4 = 49;
                    }
                    else if (adjBPratio4 > (float) 2.18 && adjBPratio4 <= (float) 2.10) {
                        per_rank4 = 50;
                    }
                    else  {
                        per_rank4 = 50;
                    }
                }
                else if(adjBPratio4 <= (float) 2.00  && adjBPratio4 > (float) 1.80){
                    
                    if (adjBPratio4 > (float) 1.80 && adjBPratio4 <= (float) 1.82) {
                        per_rank4 = 31;
                    }
                    else if (adjBPratio4 > (float) 1.82 && adjBPratio4 <= (float) 1.84) {
                        per_rank4 = 32;
                    }
                    else if (adjBPratio4 > (float) 1.84 && adjBPratio4 <= (float) 1.86) {
                        per_rank4 = 33;
                    }
                    else if (adjBPratio4 > (float) 1.86 && adjBPratio4 <= (float) 1.88) {
                        per_rank4 = 34;
                    }
                    else if (adjBPratio4 > (float) 1.88 && adjBPratio4 <= (float) 1.90) {
                        per_rank4 = 35;
                    }
                    else if (adjBPratio4 > (float) 1.90 && adjBPratio4 <= (float) 1.92) {
                        per_rank4 = 36;
                    }
                    else if (adjBPratio4 > (float) 1.92 && adjBPratio4 <= (float) 1.94) {
                        per_rank4 = 37;
                    }
                    else if (adjBPratio4 > (float) 1.94 && adjBPratio4 <= (float) 1.96) {
                        per_rank4 = 38;
                    }
                    else if (adjBPratio4 > (float) 1.96 && adjBPratio4 <= (float) 1.98) {
                        per_rank4 = 39;
                    }
                    else if (adjBPratio4 > (float) 1.98 && adjBPratio4 <= (float) 2.00) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjBPratio4 <= (float) 1.80  && adjBPratio4 > (float) 1.50){
                    
                    if (adjBPratio4 > (float) 1.50 && adjBPratio4 <= (float) 1.53) {
                        per_rank4 = 21;
                    }
                    else if (adjBPratio4 > (float) 1.53 && adjBPratio4 <= (float) 1.56) {
                        per_rank4 = 22;
                    }
                    else if (adjBPratio4 > (float) 1.56 && adjBPratio4 <= (float) 1.59) {
                        per_rank4 = 23;
                    }
                    else if (adjBPratio4 > (float) 1.59 && adjBPratio4 <= (float) 1.62) {
                        per_rank4 = 24;
                    }
                    else if (adjBPratio4 > (float) 1.62 && adjBPratio4 <= (float) 1.65) {
                        per_rank4 = 25;
                    }
                    else if (adjBPratio4 > (float) 1.65 && adjBPratio4 <= (float) 1.68) {
                        per_rank4 = 26;
                    }
                    else if (adjBPratio4 > (float) 1.68 && adjBPratio4 <= (float) 1.71) {
                        per_rank4 = 27;
                    }
                    else if (adjBPratio4 > (float) 1.71 && adjBPratio4 <= (float) 1.74) {
                        per_rank4 = 28;
                    }
                    else if (adjBPratio4 > (float) 1.74 && adjBPratio4 <= (float) 1.77) {
                        per_rank4 = 29;
                    }
                    else if (adjBPratio4 > (float) 1.77 && adjBPratio4 <= (float) 2.80) {
                        per_rank4 = 30;
                    }
                    else  {
                        per_rank4 = 30;
                    }
                }
                else if(adjBPratio4 <= (float) 1.50  && adjBPratio4 > (float) 1.20){
                    
                    if (adjBPratio4 > (float) 1.20 && adjBPratio4 <= (float) 1.23) {
                        per_rank4 = 11;
                    }
                    else if (adjBPratio4 > (float) 1.23 && adjBPratio4 <= (float) 1.26) {
                        per_rank4 = 12;
                    }
                    else if (adjBPratio4 > (float) 1.26 && adjBPratio4 <= (float) 1.29) {
                        per_rank4 = 13;
                    }
                    else if (adjBPratio4 > (float) 1.29 && adjBPratio4 <= (float) 1.32) {
                        per_rank4 = 14;
                    }
                    else if (adjBPratio4 > (float) 1.32 && adjBPratio4 <= (float) 1.35) {
                        per_rank4 = 15;
                    }
                    else if (adjBPratio4 > (float) 1.35 && adjBPratio4 <= (float) 1.38) {
                        per_rank4 = 16;
                    }
                    else if (adjBPratio4 > (float) 1.38 && adjBPratio4 <= (float) 1.41) {
                        per_rank4 = 17;
                    }
                    else if (adjBPratio4 > (float) 1.41 && adjBPratio4 <= (float) 1.44) {
                        per_rank4 = 18;
                    }
                    else if (adjBPratio4 > (float) 1.44 && adjBPratio4 <= (float) 1.47) {
                        per_rank4 = 19;
                    }
                    else if (adjBPratio4 > (float) 1.47 && adjBPratio4 <= (float) 2.50) {
                        per_rank4 = 20;
                    }
                    else  {
                        per_rank4 = 20;
                    }
                }
                else if(adjBPratio4 <= (float) 1.20  && adjBPratio4 > (float) 1.00){
                    
                    if (adjBPratio4 > (float) 1.00 && adjBPratio4 <= (float) 1.02) {
                        per_rank4 = 1;
                    }
                    else if (adjBPratio4 > (float) 1.02 && adjBPratio4 <= (float) 1.04) {
                        per_rank4 = 2;
                    }
                    else if (adjBPratio4 > (float) 1.04 && adjBPratio4 <= (float) 1.06) {
                        per_rank4 = 3;
                    }
                    else if (adjBPratio4 > (float) 1.06 && adjBPratio4 <= (float) 1.08) {
                        per_rank4 = 4;
                    }
                    else if (adjBPratio4 > (float) 1.08 && adjBPratio4 <= (float) 1.10) {
                        per_rank4 = 5;
                    }
                    else if (adjBPratio4 > (float) 1.10 && adjBPratio4 <= (float) 1.12) {
                        per_rank4 = 6;
                    }
                    else if (adjBPratio4 > (float) 1.12 && adjBPratio4 <= (float) 1.14) {
                        per_rank4 = 7;
                    }
                    else if (adjBPratio4 > (float) 1.14 && adjBPratio4 <= (float) 1.16) {
                        per_rank4 = 8;
                    }
                    else if (adjBPratio4 > (float) 1.16 && adjBPratio4 <= (float) 1.18) {
                        per_rank4 = 9;
                    }
                    else if (adjBPratio4 > (float) 1.18 && adjBPratio4 <= (float) 2.20) {
                        per_rank4 = 10;
                    }
                }
                else if(adjBPratio4 <= (float) 1.00){
                    per_rank4 = 0;
                }
                else if(adjBPratio4 > (float) 3.50){
                    per_rank4 = 100;
                }
                
                if(per_rank4 == 2222){
                    str_rank4.text = @"--";
                }
                else{
                    str_rank4.text = [NSString stringWithFormat:@"%d", per_rank4];
                }
            }
            else{
                per_rank4 = 2222;
                str_rank4.text = @"--";
            }
        }
        else{
            //female
           
            if(adjBPratio4 > 0){
                
                if(adjBPratio4 <= (float) 3.10 && adjBPratio4 >  (float) 2.80){
                    
                    if (adjBPratio4 > (float) 2.80 && adjBPratio4 <= (float) 2.83) {
                        per_rank4 = 91;
                    }
                    else if (adjBPratio4 > (float) 2.83 && adjBPratio4 <= (float) 2.86) {
                        per_rank4 = 92;
                    }
                    else if (adjBPratio4 > (float) 2.86 && adjBPratio4 <= (float) 2.89) {
                        per_rank4 = 93;
                    }
                    else if (adjBPratio4 > (float) 2.89 && adjBPratio4 <= (float) 2.92) {
                        per_rank4 = 94;
                    }
                    else if (adjBPratio4 > (float) 2.92 && adjBPratio4 <= (float) 2.95) {
                        per_rank4 = 95;
                    }
                    else if (adjBPratio4 > (float) 2.95 && adjBPratio4 <= (float) 2.98) {
                        per_rank4 = 96;
                    }
                    else if (adjBPratio4 > (float) 2.98 && adjBPratio4 <= (float) 3.01) {
                        per_rank4 = 97;
                    }
                    else if (adjBPratio4 > (float) 3.01 && adjBPratio4 <= (float) 3.04) {
                        per_rank4 = 98;
                    }
                    else if (adjBPratio4 > (float) 3.04 && adjBPratio4 <= (float) 3.07) {
                        per_rank4 = 99;
                    }
                    else if (adjBPratio4 > (float) 3.07 && adjBPratio4 <= (float) 3.10) {
                        per_rank4 = 100;
                    }
                    else{
                        per_rank4 = 100;
                    }
                }
                else if(adjBPratio4 <= (float) 2.80  && adjBPratio4 >  (float) 2.60){
                    
                    if (adjBPratio4 > (float) 2.60 && adjBPratio4 <= (float) 2.62) {
                        per_rank4 = 81;
                    }
                    else if (adjBPratio4 > (float) 2.62 && adjBPratio4 <= (float) 2.64) {
                        per_rank4 = 82;
                    }
                    else if (adjBPratio4 > (float) 2.64 && adjBPratio4 <= (float) 2.66) {
                        per_rank4 = 83;
                    }
                    else if (adjBPratio4 > (float) 2.66 && adjBPratio4 <= (float) 2.68) {
                        per_rank4 = 84;
                    }
                    else if (adjBPratio4 > (float) 2.68 && adjBPratio4 <= (float) 2.70) {
                        per_rank4 = 85;
                    }
                    else if (adjBPratio4 > (float) 2.70 && adjBPratio4 <= (float) 2.72) {
                        per_rank4 = 86;
                    }
                    else if (adjBPratio4 > (float) 2.72 && adjBPratio4 <= (float) 2.74) {
                        per_rank4 = 87;
                    }
                    else if (adjBPratio4 > (float) 2.74 && adjBPratio4 <= (float) 2.76) {
                        per_rank4 = 88;
                    }
                    else if (adjBPratio4 > (float) 2.76 && adjBPratio4 <= (float) 2.78) {
                        per_rank4 = 89;
                    }
                    else if (adjBPratio4 > (float) 2.78 && adjBPratio4 <= (float) 2.80) {
                        per_rank4 = 90;
                    }
                    else  {
                        per_rank4 = 90;
                    }
                }
                else if(adjBPratio4 <= (float) 2.60  && adjBPratio4 >  (float) 2.40){
                    
                    if (adjBPratio4 > (float) 2.40 && adjBPratio4 <= (float) 2.42) {
                        per_rank4 = 71;
                    }
                    else if (adjBPratio4 > (float) 2.42 && adjBPratio4 <= (float) 2.44) {
                        per_rank4 = 72;
                    }
                    else if (adjBPratio4 > (float) 2.44 && adjBPratio4 <= (float) 2.46) {
                        per_rank4 = 73;
                    }
                    else if (adjBPratio4 > (float) 2.46 && adjBPratio4 <= (float) 2.48) {
                        per_rank4 = 74;
                    }
                    else if (adjBPratio4 > (float) 2.48 && adjBPratio4 <= (float) 2.50) {
                        per_rank4 = 75;
                    }
                    else if (adjBPratio4 > (float) 2.50 && adjBPratio4 <= (float) 2.52) {
                        per_rank4 = 76;
                    }
                    else if (adjBPratio4 > (float) 2.52 && adjBPratio4 <= (float) 2.54) {
                        per_rank4 = 77;
                    }
                    else if (adjBPratio4 > (float) 2.54 && adjBPratio4 <= (float) 2.56) {
                        per_rank4 = 78;
                    }
                    else if (adjBPratio4 > (float) 2.56 && adjBPratio4 <= (float) 2.58) {
                        per_rank4 = 79;
                    }
                    else if (adjBPratio4 > (float) 2.58 && adjBPratio4 <= (float) 2.60) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjBPratio4 <= (float) 2.40 && adjBPratio4 >  (float) 2.20){
                    
                    if (adjBPratio4 > (float) 2.2 && adjBPratio4 <= (float) 2.22) {
                        per_rank4 = 61;
                    }
                    else if (adjBPratio4 > (float) 2.22 && adjBPratio4 <= (float) 2.24) {
                        per_rank4 = 62;
                    }
                    else if (adjBPratio4 > (float) 2.24 && adjBPratio4 <= (float) 2.26) {
                        per_rank4 = 63;
                    }
                    else if (adjBPratio4 > (float) 2.26 && adjBPratio4 <= (float) 2.28) {
                        per_rank4 = 64;
                    }
                    else if (adjBPratio4 > (float) 2.28 && adjBPratio4 <= (float) 2.30) {
                        per_rank4 = 65;
                    }
                    else if (adjBPratio4 > (float) 2.30 && adjBPratio4 <= (float) 2.32) {
                        per_rank4 = 66;
                    }
                    else if (adjBPratio4 > (float) 2.32 && adjBPratio4 <= (float) 2.34) {
                        per_rank4 = 67;
                    }
                    else if (adjBPratio4 > (float) 2.34 && adjBPratio4 <= (float) 2.36) {
                        per_rank4 = 68;
                    }
                    else if (adjBPratio4 > (float) 2.36 && adjBPratio4 <= (float) 2.38) {
                        per_rank4 = 69;
                    }
                    else if (adjBPratio4 > (float) 2.38 && adjBPratio4 <= (float) 2.40) {
                        per_rank4 = 70;
                    }
                    else  {
                        per_rank4 = 70;
                    }
                }
                else if(adjBPratio4 <= (float) 2.20 && adjBPratio4 >  (float) 2.00){
                    
                    if (adjBPratio4 > (float) 2.00 && adjBPratio4 <= (float) 2.02) {
                        per_rank4 = 51;
                    }
                    else if (adjBPratio4 > (float) 2.02 && adjBPratio4 <= (float) 2.04) {
                        per_rank4 = 52;
                    }
                    else if (adjBPratio4 > (float) 2.04 && adjBPratio4 <= (float) 2.06) {
                        per_rank4 = 53;
                    }
                    else if (adjBPratio4 > (float) 2.06 && adjBPratio4 <= (float) 2.08) {
                        per_rank4 = 54;
                    }
                    else if (adjBPratio4 > (float) 2.08 && adjBPratio4 <= (float) 2.10) {
                        per_rank4 = 55;
                    }
                    else if (adjBPratio4 > (float) 2.10 && adjBPratio4 <= (float) 2.12) {
                        per_rank4 = 56;
                    }
                    else if (adjBPratio4 > (float) 2.12 && adjBPratio4 <= (float) 2.14) {
                        per_rank4 = 57;
                    }
                    else if (adjBPratio4 > (float) 2.14 && adjBPratio4 <= (float) 2.16) {
                        per_rank4 = 58;
                    }
                    else if (adjBPratio4 > (float) 2.16 && adjBPratio4 <= (float) 2.18) {
                        per_rank4 = 59;
                    }
                    else if (adjBPratio4 > (float) 2.18 && adjBPratio4 <= (float) 2.20) {
                        per_rank4 = 60;
                    }
                    else  {
                        per_rank4 = 60;
                    }
                  
                }
                else if(adjBPratio4 <= (float) 2.00  && adjBPratio4 > (float) 1.80){
                    
                    if (adjBPratio4 > (float) 1.80 && adjBPratio4 <= (float) 1.82) {
                        per_rank4 = 41;
                    }
                    else if (adjBPratio4 > (float) 1.82 && adjBPratio4 <= (float) 1.84) {
                        per_rank4 = 42;
                    }
                    else if (adjBPratio4 > (float) 1.84 && adjBPratio4 <= (float) 1.86) {
                        per_rank4 = 43;
                    }
                    else if (adjBPratio4 > (float) 1.86 && adjBPratio4 <= (float) 1.88) {
                        per_rank4 = 44;
                    }
                    else if (adjBPratio4 > (float) 1.88 && adjBPratio4 <= (float) 1.90) {
                        per_rank4 = 45;
                    }
                    else if (adjBPratio4 > (float) 1.90 && adjBPratio4 <= (float) 1.92) {
                        per_rank4 = 46;
                    }
                    else if (adjBPratio4 > (float) 1.92 && adjBPratio4 <= (float) 1.94) {
                        per_rank4 = 47;
                    }
                    else if (adjBPratio4 > (float) 1.94 && adjBPratio4 <= (float) 1.96) {
                        per_rank4 = 48;
                    }
                    else if (adjBPratio4 > (float) 1.96 && adjBPratio4 <= (float) 1.98) {
                        per_rank4 = 49;
                    }
                    else if (adjBPratio4 > (float) 1.98 && adjBPratio4 <= (float) 2.00) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjBPratio4 <= (float) 1.80  && adjBPratio4 > (float) 1.60){
                    
                    if (adjBPratio4 > (float) 1.60 && adjBPratio4 <=1.62) {
                        per_rank4 = 31;
                    }
                    else if (adjBPratio4 > (float) 1.62 && adjBPratio4 <= (float) 1.64) {
                        per_rank4 = 32;
                    }
                    else if (adjBPratio4 > (float) 1.64 && adjBPratio4 <= (float) 1.66) {
                        per_rank4 = 33;
                    }
                    else if (adjBPratio4 > (float) 1.66 && adjBPratio4 <= (float) 1.68) {
                        per_rank4 = 34;
                    }
                    else if (adjBPratio4 > (float) 1.68 && adjBPratio4 <= (float) 1.70) {
                        per_rank4 = 35;
                    }
                    else if (adjBPratio4 > (float) 1.70 && adjBPratio4 <= (float) 1.72) {
                        per_rank4 = 36;
                    }
                    else if (adjBPratio4 > (float) 1.72 && adjBPratio4 <= (float) 1.74) {
                        per_rank4 = 37;
                    }
                    else if (adjBPratio4 > (float) 1.74 && adjBPratio4 <= (float) 1.76) {
                        per_rank4 = 38;
                    }
                    else if (adjBPratio4 > (float) 1.76 && adjBPratio4 <= (float) 1.78) {
                        per_rank4 = 39;
                    }
                    else if (adjBPratio4 > (float) 1.78 && adjBPratio4 <= (float) 1.80) {
                        per_rank4 = 40;
                    }
                    else  {
                        per_rank4 = 40;
                    }
                }
                else if(adjBPratio4 <= (float) 1.60  && adjBPratio4 > (float) 1.30){
                    
                    if (adjBPratio4 > (float) 1.30 && adjBPratio4 <= (float) 1.33) {
                        per_rank4 = 21;
                    }
                    else if (adjBPratio4 > (float) 1.33 && adjBPratio4 <= (float) 1.36) {
                        per_rank4 = 22;
                    }
                    else if (adjBPratio4 > (float) 1.36 && adjBPratio4 <= (float) 1.39) {
                        per_rank4 = 23;
                    }
                    else if (adjBPratio4 > (float) 1.39 && adjBPratio4 <= (float) 1.42) {
                        per_rank4 = 24;
                    }
                    else if (adjBPratio4 > (float) 1.42 && adjBPratio4 <= (float) 1.45) {
                        per_rank4 = 25;
                    }
                    else if (adjBPratio4 > (float) 1.45 && adjBPratio4 <= (float) 1.48) {
                        per_rank4 = 26;
                    }
                    else if (adjBPratio4 > (float) 1.48 && adjBPratio4 <= (float) 1.51) {
                        per_rank4 = 27;
                    }
                    else if (adjBPratio4 > (float) 1.51 && adjBPratio4 <= (float) 1.54) {
                        per_rank4 = 28;
                    }
                    else if (adjBPratio4 > (float) 1.54 && adjBPratio4 <= (float) 1.57) {
                        per_rank4 = 29;
                    }
                    else if (adjBPratio4 > (float) 1.57 && adjBPratio4 <= (float) 1.60) {
                        per_rank4 = 30;
                    }
                    else  {
                        per_rank4 = 30;
                    }
                }
                else if(adjBPratio4 <= (float) 1.30  && adjBPratio4 > (float) 0.90){
                    
                    if (adjBPratio4 > (float) 0.90 && adjBPratio4 <= (float) 0.94) {
                        per_rank4 = 11;
                    }
                    else if (adjBPratio4 > (float) 0.94 && adjBPratio4 <= (float) 0.98) {
                        per_rank4 = 12;
                    }
                    else if (adjBPratio4 > (float) 0.98 && adjBPratio4 <= (float) 1.02) {
                        per_rank4 = 13;
                    }
                    else if (adjBPratio4 > (float) 1.02 && adjBPratio4 <= (float) 1.06) {
                        per_rank4 = 14;
                    }
                    else if (adjBPratio4 > (float) 1.06 && adjBPratio4 <= (float) 1.10) {
                        per_rank4 = 15;
                    }
                    else if (adjBPratio4 > (float) 1.10 && adjBPratio4 <= (float) 1.14) {
                        per_rank4 = 16;
                    }
                    else if (adjBPratio4 > (float) 1.14 && adjBPratio4 <= (float) 1.18) {
                        per_rank4 = 17;
                    }
                    else if (adjBPratio4 > (float) 1.18 && adjBPratio4 <= (float) 1.22) {
                        per_rank4 = 18;
                    }
                    else if (adjBPratio4 > (float) 1.22 && adjBPratio4 <= (float) 1.26) {
                        per_rank4 = 19;
                    }
                    else if (adjBPratio4 > (float) 1.26 && adjBPratio4 <= (float) 1.30) {
                        per_rank4 = 20;
                    }
                    else  {
                        per_rank4 = 20;
                    }
                }
                else if(adjBPratio4 <= (float) 0.90  && adjBPratio4 > (float) 0.70){
                    
                    if (adjBPratio4 > (float) 0.70 && adjBPratio4 <= (float) 0.72) {
                        per_rank4 = 1;
                    }
                    else if (adjBPratio4 > (float) 0.72 && adjBPratio4 <= (float) 0.74) {
                        per_rank4 = 2;
                    }
                    else if (adjBPratio4 > (float) 0.74 && adjBPratio4 <= (float) 0.76) {
                        per_rank4 = 3;
                    }
                    else if (adjBPratio4 > (float) 0.76 && adjBPratio4 <= (float) 0.78) {
                        per_rank4 = 4;
                    }
                    else if (adjBPratio4 > (float) 0.78 && adjBPratio4 <= (float) 0.80) {
                        per_rank4 = 5;
                    }
                    else if (adjBPratio4 > (float) 0.80 && adjBPratio4 <= (float) 0.82) {
                        per_rank4 = 6;
                    }
                    else if (adjBPratio4 > (float) 0.82 && adjBPratio4 <= (float) 0.84) {
                        per_rank4 = 7;
                    }
                    else if (adjBPratio4 > (float) 0.84 && adjBPratio4 <= (float) 0.86) {
                        per_rank4 = 8;
                    }
                    else if (adjBPratio4 > (float) 0.86 && adjBPratio4 <= (float) 0.88) {
                        per_rank4 = 9;
                    }
                    else if (adjBPratio4 > (float) 0.88 && adjBPratio4 <= (float) 0.90) {
                        per_rank4 = 10;
                    }
                }
                else if(adjBPratio4 <= (float) 0.70){
                    per_rank4 = 0;
                }
                else if(adjBPratio4 > (float) 3.10){
                    per_rank4 = 100;
                }
                if(per_rank4 == 2222){
                    str_rank4.text = @"--";
                }
                else{
                    str_rank4.text = [NSString stringWithFormat:@"%d", per_rank4];
                }
            }
            else{
                per_rank4 = 2222;
                str_rank4.text = @"--";
            }
            
        }

        
        
        strg_per_rank4 = per_rank4;
        
        if([str_rank4.text isEqualToString:@"--"]){
            [upElementArray replaceObjectAtIndex:140 withObject:@"0"];
        }
        else{
            [upElementArray replaceObjectAtIndex:140 withObject:[NSString stringWithFormat:@"%.0f",strg_per_rank4]];
        }
        
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    else{
        str_pred4.text = @"";
        str_sw4.text = @"";
        str_rank4.text  = @"--";
        [upElementArray replaceObjectAtIndex:140 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    
}

-(void)getLegExtValue:(float)kgValue :(float)respValue{
    
    if(kgValue > 0.0 && respValue > 0.0){
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        
        float bprm5 = kgValue*2.2/(1.0278-(respValue*0.0278));
        float BPRMkg5 =  bprm5/2.2;
        str_pred5.text = [NSString stringWithFormat:@"%.1f", BPRMkg5];
        
        float BPratio5  =  BPRMkg5/clientWeight;
        float adjBPratio5  =  ageCorrection*BPratio5;
        
        if(isnan(adjBPratio5)){
            str_sw5.text = @"";
        }
        else if(isinf(adjBPratio5)){
            str_sw5.text = @"";
        }
        else if(adjBPratio5 == 0.0){
            str_sw5.text = @"";
        }
        else{
            str_sw5.text = [NSString stringWithFormat:@"%.2f", adjBPratio5];
        }
        adjBPratio5 = [[NSString stringWithFormat:@"%.3f", adjBPratio5] floatValue];
        [upElementArray replaceObjectAtIndex:141 withObject:str_sw5.text];
        NSInteger per_rank5 = 2222;
        
        
        if (clientGender==0) {
            //male
            
            if(adjBPratio5 > 0){
                
                
                if(adjBPratio5 <= (float) 1.350 && adjBPratio5 >  (float) 1.100){
                    
                    if (adjBPratio5 > (float) 1.100 && adjBPratio5 <= (float) 1.125) {
                        per_rank5 = 91;
                    }
                    else if (adjBPratio5 > (float) 1.125 && adjBPratio5 <= (float) 1.150) {
                        per_rank5 = 92;
                    }
                    else if (adjBPratio5 > (float) 1.150 && adjBPratio5 <= (float) 1.175) {
                        per_rank5 = 93;
                    }
                    else if (adjBPratio5 > (float) 1.175 && adjBPratio5 <= (float) 1.200) {
                        per_rank5 = 94;
                    }
                    else if (adjBPratio5 > (float) 1.200 && adjBPratio5 <= (float) 1.225) {
                        per_rank5 = 95;
                    }
                    else if (adjBPratio5 > (float) 1.225 && adjBPratio5 <= (float) 1.250) {
                        per_rank5 = 96;
                    }
                    else if (adjBPratio5 > (float) 1.250 && adjBPratio5 <= (float) 1.275) {
                        per_rank5 = 97;
                    }
                    else if (adjBPratio5 > (float) 1.275 && adjBPratio5 <= (float) 1.300) {
                        per_rank5 = 98;
                    }
                    else if (adjBPratio5 > (float) 1.300 && adjBPratio5 <= (float) 1.325) {
                        per_rank5 = 99;
                    }
                    else  {
                        per_rank5 = 100;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 1.10 && adjBPratio5 >  (float) 0.90){
                    
                    if (adjBPratio5 > (float) 0.90 && adjBPratio5 <= (float) 0.92) {
                        per_rank5 = 81;
                    }
                    else if (adjBPratio5 > (float) 0.92 && adjBPratio5 <= (float) 0.94) {
                        per_rank5 = 82;
                    }
                    else if (adjBPratio5 > (float) 0.94 && adjBPratio5 <= (float) 0.96) {
                        per_rank5 = 83;
                    }
                    else if (adjBPratio5 > (float) 0.96 && adjBPratio5 <= (float) 0.98) {
                        per_rank5 = 84;
                    }
                    else if (adjBPratio5 > (float) 0.98 && adjBPratio5 <= (float) 1.00) {
                        per_rank5 = 85;
                    }
                    else if (adjBPratio5 > (float) 1.00 && adjBPratio5 <= (float) 1.02) {
                        per_rank5 = 86;
                    }
                    else if (adjBPratio5 > (float) 1.02 && adjBPratio5 <= (float) 1.04) {
                        per_rank5 = 87;
                    }
                    else if (adjBPratio5 > (float) 1.04 && adjBPratio5 <= (float) 1.06) {
                        per_rank5 = 88;
                    }
                    else if (adjBPratio5 > (float) 1.06 && adjBPratio5 <= (float) 1.08) {
                        per_rank5 = 89;
                    }
                    else  {
                        per_rank5 = 90;
                    }
                    
                    
                }
                else if(adjBPratio5 <= (float) 0.90 && adjBPratio5 >  (float) 0.80){
                    
                    if (adjBPratio5 > (float) 0.80 && adjBPratio5 <= (float) 0.81) {
                        per_rank5 = 71;
                    }
                    else if (adjBPratio5 > (float) 0.81 && adjBPratio5 <= (float) 0.82) {
                        per_rank5 = 72;
                    }
                    else if (adjBPratio5 > (float) 0.82 && adjBPratio5 <= (float) 0.83) {
                        per_rank5 = 73;
                    }
                    else if (adjBPratio5 > (float) 0.83 && adjBPratio5 <= (float) 0.84) {
                        per_rank5 = 74;
                    }
                    else if (adjBPratio5 > (float) 0.84 && adjBPratio5 <= (float) 0.85) {
                        per_rank5 = 75;
                    }
                    else if (adjBPratio5 > (float) 0.85 && adjBPratio5 <= (float) 0.86) {
                        per_rank5 = 76;
                    }
                    else if (adjBPratio5 > (float) 0.86 && adjBPratio5 <= (float) 0.87) {
                        per_rank5 = 77;
                    }
                    else if (adjBPratio5 > (float) 0.87 && adjBPratio5 <= (float) 0.88) {
                        per_rank5 = 78;
                    }
                    else if (adjBPratio5 > (float) 0.88 && adjBPratio5 <= (float) 0.89) {
                        per_rank5 = 79;
                    }
                    else  {
                        per_rank5 = 80;
                    }
                    
                    
                }//
                else if(adjBPratio5 <= (float) 0.80  && adjBPratio5 >  (float) 0.70){
                    
                    
                    if (adjBPratio5 > (float) 0.70 && adjBPratio5 <= (float) 0.71) {
                        per_rank5 = 61;
                    }
                    else if (adjBPratio5 > (float) 0.71 && adjBPratio5 <= (float) 0.72) {
                        per_rank5 = 62;
                    }
                    else if (adjBPratio5 > (float) 0.72 && adjBPratio5 <= (float) 0.73) {
                        per_rank5 = 63;
                    }
                    else if (adjBPratio5 > (float) 0.73 && adjBPratio5 <= (float) 0.74) {
                        per_rank5 = 64;
                    }
                    else if (adjBPratio5 > (float) 0.74 && adjBPratio5 <= (float) 0.75) {
                        per_rank5 = 65;
                    }
                    else if (adjBPratio5 > (float) 0.75 && adjBPratio5 <= (float) 0.76) {
                        per_rank5 = 66;
                    }
                    else if (adjBPratio5 > (float) 0.76 && adjBPratio5 <= (float) 0.77) {
                        per_rank5 = 67;
                    }
                    else if (adjBPratio5 > (float) 0.77 && adjBPratio5 <= (float) 0.78) {
                        per_rank5 = 68;
                    }
                    else if (adjBPratio5 > (float) 0.78 && adjBPratio5 <= (float) 0.79) {
                        per_rank5 = 69;
                    }
                    else  {
                        per_rank5 = 70;
                    }
                    
                    
                }//
                else if(adjBPratio5 <= (float) 0.700 && adjBPratio5 >  (float) 0.650){
                    
                    if (adjBPratio5 > (float) 0.650 && adjBPratio5 <= (float) 0.655) {
                        per_rank5 = 51;
                    }
                    else if (adjBPratio5 > (float) 0.655 && adjBPratio5 <= (float) 0.660) {
                        per_rank5 = 52;
                    }
                    else if (adjBPratio5 > (float) 0.660 && adjBPratio5 <= (float) 0.665) {
                        per_rank5 = 53;
                    }
                    else if (adjBPratio5 > (float) 0.665 && adjBPratio5 <= (float) 0.670) {
                        per_rank5 = 54;
                    }
                    else if (adjBPratio5 > (float) 0.670 && adjBPratio5 <= (float) 0.675) {
                        per_rank5 = 55;
                    }
                    else if (adjBPratio5 > (float) 0.675 && adjBPratio5 <= (float) 0.680) {
                        per_rank5 = 56;
                    }
                    else if (adjBPratio5 > (float) 0.680 && adjBPratio5 <= (float) 0.685) {
                        per_rank5 = 57;
                    }
                    else if (adjBPratio5 > (float) 0.685 && adjBPratio5 <= (float) 0.690) {
                        per_rank5 = 58;
                    }
                    else if (adjBPratio5 > (float) 0.690 && adjBPratio5 <= (float) 0.695) {
                        per_rank5 = 59;
                    }
                    else  {
                        per_rank5 = 60;
                    }
                    
                    
                }//
                else if(adjBPratio5 <= (float) 0.650 && adjBPratio5 >  (float) 0.600){
                    
                    if (adjBPratio5 > (float) 0.600 && adjBPratio5 <= (float) 0.605) {
                        per_rank5 = 41;
                    }
                    else if (adjBPratio5 > (float) 0.605 && adjBPratio5 <= (float) 0.610) {
                        per_rank5 = 42;
                    }
                    else if (adjBPratio5 > (float) 0.610 && adjBPratio5 <= (float) 0.615) {
                        per_rank5 = 43;
                    }
                    else if (adjBPratio5 > (float) 0.615 && adjBPratio5 <= (float) 0.620) {
                        per_rank5 = 44;
                    }
                    else if (adjBPratio5 > (float) 0.620 && adjBPratio5 <= (float) 0.625) {
                        per_rank5 = 45;
                    }
                    else if (adjBPratio5 > (float) 0.625 && adjBPratio5 <= (float) 0.630) {
                        per_rank5 = 46;
                    }
                    else if (adjBPratio5 > (float) 0.630 && adjBPratio5 <= (float) 0.635) {
                        per_rank5 = 47;
                    }
                    else if (adjBPratio5 > (float) 0.635 && adjBPratio5 <= (float) 0.640) {
                        per_rank5 = 48;
                    }
                    else if (adjBPratio5 > (float) 0.640 && adjBPratio5 <= (float) 0.645) {
                        per_rank5 = 49;
                    }
                    else  {
                        per_rank5 = 50;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 0.600 && adjBPratio5 >  (float) 0.550){
                    
                    if (adjBPratio5 > (float) 0.550 && adjBPratio5 <= (float) 0.555) {
                        per_rank5 = 31;
                    }
                    else if (adjBPratio5 > (float) 0.555 && adjBPratio5 <= (float) 0.560) {
                        per_rank5 = 32;
                    }
                    else if (adjBPratio5 > (float) 0.560 && adjBPratio5 <= (float) 0.565) {
                        per_rank5 = 33;
                    }
                    else if (adjBPratio5 > (float) 0.565 && adjBPratio5 <= (float) 0.570) {
                        per_rank5 = 34;
                    }
                    else if (adjBPratio5 > (float) 0.570 && adjBPratio5 <= (float) 0.575) {
                        per_rank5 = 35;
                    }
                    else if (adjBPratio5 > (float) 0.575 && adjBPratio5 <= (float) 0.580) {
                        per_rank5 = 36;
                    }
                    else if (adjBPratio5 > (float) 0.580 && adjBPratio5 <= (float) 0.585) {
                        per_rank5 = 37;
                    }
                    else if (adjBPratio5 > (float) 0.585 && adjBPratio5 <= (float) 0.590) {
                        per_rank5 = 38;
                    }
                    else if (adjBPratio5 > (float) 0.590 && adjBPratio5 <= (float) 0.595) {
                        per_rank5 = 39;
                    }
                    else  {
                        per_rank5 = 40;
                    }
                    
                    
                }
                else if(adjBPratio5 <= (float) 0.55 && adjBPratio5 >  (float) 0.45){
                    
                    if (adjBPratio5 > (float) 0.45 && adjBPratio5 <= (float) 0.46) {
                        per_rank5 = 21;
                    }
                    else if (adjBPratio5 > (float) 0.46 && adjBPratio5 <= (float) 0.47) {
                        per_rank5 = 22;
                    }
                    else if (adjBPratio5 > (float) 0.47 && adjBPratio5 <= (float) 0.48) {
                        per_rank5 = 23;
                    }
                    else if (adjBPratio5 > (float) 0.48 && adjBPratio5 <= (float) 0.49) {
                        per_rank5 = 24;
                    }
                    else if (adjBPratio5 > (float) 0.49 && adjBPratio5 <= (float) 0.50) {
                        per_rank5 = 25;
                    }
                    else if (adjBPratio5 > (float) 0.50 && adjBPratio5 <= (float) 0.51) {
                        per_rank5 = 26;
                    }
                    else if (adjBPratio5 > (float) 0.51 && adjBPratio5 <= (float) 0.52) {
                        per_rank5 = 27;
                    }
                    else if (adjBPratio5 > (float) 0.52 && adjBPratio5 <= (float) 0.53) {
                        per_rank5 = 28;
                    }
                    else if (adjBPratio5 > (float) 0.53 && adjBPratio5 <= (float) 0.54) {
                        per_rank5 = 29;
                    }
                    else  {
                        per_rank5 = 30;
                    }
                }
                else if(adjBPratio5 <= (float) 0.450 && adjBPratio5 >  (float) 0.300){
                    
                    if (adjBPratio5 > (float) 0.300 && adjBPratio5<= (float) 0.315) {
                        per_rank5 = 11;
                    }
                    else if (adjBPratio5 > (float) 0.315 && adjBPratio5 <= (float) 0.330) {
                        per_rank5 = 12;
                    }
                    else if (adjBPratio5 > (float) 0.330 && adjBPratio5 <= (float) 0.345) {
                        per_rank5 = 13;
                    }
                    else if (adjBPratio5 > (float) 0.345 && adjBPratio5 <= (float) 0.360) {
                        per_rank5 = 14;
                    }
                    else if (adjBPratio5 > (float) 0.360 && adjBPratio5 <= (float) 0.375) {
                        per_rank5 = 15;
                    }
                    else if (adjBPratio5 > (float) 0.375 && adjBPratio5 <= (float) 0.390) {
                        per_rank5 = 16;
                    }
                    else if (adjBPratio5 > (float) 0.390 && adjBPratio5 <= (float) 0.405) {
                        per_rank5 = 17;
                    }
                    else if (adjBPratio5 > (float) 0.405 && adjBPratio5 <= (float) 0.420) {
                        per_rank5 = 18;
                    }
                    else if (adjBPratio5 > (float) 0.420 && adjBPratio5 <= (float) 0.435) {
                        per_rank5 = 19;
                    }
                    else  {
                        per_rank5 = 20;
                    }
                }
                else if(adjBPratio5 <= (float) 0.30  && adjBPratio5 > (float) 0.10){
                    
                    
                    if (adjBPratio5 > (float) 0.10 && adjBPratio5 <= (float) 0.12) {
                        per_rank5 = 1;
                    }
                    else if (adjBPratio5 > (float) 0.12 && adjBPratio5 <= (float) 0.14) {
                        per_rank5 = 2;
                    }
                    else if (adjBPratio5 > (float) 0.14 && adjBPratio5 <= (float) 0.16) {
                        per_rank5 = 3;
                    }
                    else if (adjBPratio5 > (float) 0.16 && adjBPratio5 <= (float) 0.18) {
                        per_rank5 = 4;
                    }
                    else if (adjBPratio5 > (float) 0.18 && adjBPratio5 <= (float) 0.20) {
                        per_rank5 = 5;
                    }
                    else if (adjBPratio5 > (float) 0.20 && adjBPratio5 <= (float) 0.22) {
                        per_rank5 = 6;
                    }
                    else if (adjBPratio5 > (float) 0.22 && adjBPratio5 <= (float) 0.24) {
                        per_rank5 = 7;
                    }
                    else if (adjBPratio5 > (float) 0.24 && adjBPratio5 <= (float) 0.26) {
                        per_rank5 = 8;
                    }
                    else if (adjBPratio5 > (float) 0.26 && adjBPratio5 <= (float) 0.28) {
                        per_rank5 = 9;
                    }
                    else  {
                        per_rank5 = 10;
                    }

                }
                else if(adjBPratio5 <=  (float) 0.10 ){
                    per_rank5 = 0;
                }
                else if(adjBPratio5 >  (float) 1.350){
                    per_rank5 = 100;
                }
                
                if(per_rank5 == 2222){
                    str_rank5.text = @"--";
                }
                else{
                    str_rank5.text = [NSString stringWithFormat:@"%d", per_rank5];
                }
            }
            else{
                per_rank5 = 2222;
                str_rank5.text = @"--";
            }
        }
        else{
            //female
            
            if(adjBPratio5 > 0){
                
                if(adjBPratio5 <= (float) 1.200 && adjBPratio5 >  (float) 0.950){
                    
                    
                    if (adjBPratio5 > (float) 0.950 && adjBPratio5 <= (float) 0.975) {
                        per_rank5 = 91;
                    }
                    else if (adjBPratio5 > (float) 0.975 && adjBPratio5 <= (float) 1.000) {
                        per_rank5 = 92;
                    }
                    else if (adjBPratio5 > (float) 1.000 && adjBPratio5 <= (float) 1.025) {
                        per_rank5 = 93;
                    }
                    else if (adjBPratio5 > (float) 1.025 && adjBPratio5 <= (float) 1.050) {
                        per_rank5 = 94;
                    }
                    else if (adjBPratio5 > (float) 1.050 && adjBPratio5 <= (float) 1.075) {
                        per_rank5 = 95;
                    }
                    else if (adjBPratio5 > (float) 1.075 && adjBPratio5 <= (float) 1.100) {
                        per_rank5 = 96;
                    }
                    else if (adjBPratio5 > (float) 1.100 && adjBPratio5 <= (float) 1.125) {
                        per_rank5 = 97;
                    }
                    else if (adjBPratio5 > (float) 1.125 && adjBPratio5 <= (float) 1.150) {
                        per_rank5 = 98;
                    }
                    else if (adjBPratio5 > (float) 1.150 && adjBPratio5 <= (float) 1.175) {
                        per_rank5 = 99;
                    }
                    else  {
                        per_rank5 = 100;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 0.950 && adjBPratio5 >  (float) 0.800){
                    
                    if (adjBPratio5 > (float) 0.800 && adjBPratio5 <= (float) 0.815) {
                        per_rank5 = 81;
                    }
                    else if (adjBPratio5 > (float) 0.815 && adjBPratio5 <= (float) 0.830) {
                        per_rank5 = 82;
                    }
                    else if (adjBPratio5 > (float) 0.830 && adjBPratio5 <= (float) 0.845) {
                        per_rank5 = 83;
                    }
                    else if (adjBPratio5 > (float) 0.845 && adjBPratio5 <= (float) 0.860) {
                        per_rank5 = 84;
                    }
                    else if (adjBPratio5 > (float) 0.860 && adjBPratio5 <= (float) 0.875) {
                        per_rank5 = 85;
                    }
                    else if (adjBPratio5 > (float) 0.875 && adjBPratio5 <= (float) 0.890) {
                        per_rank5 = 86;
                    }
                    else if (adjBPratio5 > (float) 0.890 && adjBPratio5 <= (float) 0.905) {
                        per_rank5 = 87;
                    }
                    else if (adjBPratio5 > (float) 0.905 && adjBPratio5 <= (float) 0.920) {
                        per_rank5 = 88;
                    }
                    else if (adjBPratio5 > (float) 0.920 && adjBPratio5 <= (float) 0.935) {
                        per_rank5 = 89;
                    }
                    else  {
                        per_rank5 = 90;
                    }
                    
                    
                }
                
                else if(adjBPratio5 <= 0.80  && adjBPratio5 > 0.70){
                    
                    if (adjBPratio5 > (float) 0.70 && adjBPratio5 <= (float) 0.71) {
                        per_rank5 = 71;
                    }
                    else if (adjBPratio5 > (float) 0.71 && adjBPratio5 <= (float) 0.72) {
                        per_rank5 = 72;
                    }
                    else if (adjBPratio5 > (float) 0.72 && adjBPratio5 <= (float) 0.73) {
                        per_rank5 = 73;
                    }
                    else if (adjBPratio5 > (float) 0.73 && adjBPratio5 <= (float) 0.74) {
                        per_rank5 = 74;
                    }
                    else if (adjBPratio5 > (float) 0.74 && adjBPratio5 <= (float) 0.75) {
                        per_rank5 = 75;
                    }
                    else if (adjBPratio5 > (float) 0.75 && adjBPratio5 <= (float) 0.76) {
                        per_rank5 = 76;
                    }
                    else if (adjBPratio5 > (float) 0.76 && adjBPratio5 <= (float) 0.77) {
                        per_rank5 = 77;
                    }
                    else if (adjBPratio5 > (float) 0.77 && adjBPratio5 <= (float) 0.78) {
                        per_rank5 = 78;
                    }
                    else if (adjBPratio5 > (float) 0.78 && adjBPratio5 <= (float) 0.79) {
                        per_rank5 = 79;
                    }
                    else  {
                        per_rank5 = 80;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 0.70  && adjBPratio5 >  (float) 0.60){
                    
                    
                    if (adjBPratio5 > (float) 0.60 && adjBPratio5 <= (float) 0.61) {
                        per_rank5 = 61;
                    }
                    else if (adjBPratio5 > (float) 0.61 && adjBPratio5 <= (float) 0.62) {
                        per_rank5 = 62;
                    }
                    else if (adjBPratio5 > (float) 0.62 && adjBPratio5 <= (float) 0.63) {
                        per_rank5 = 63;
                    }
                    else if (adjBPratio5 > (float) 0.63 && adjBPratio5 <= (float) 0.64) {
                        per_rank5 = 64;
                    }
                    else if (adjBPratio5 > (float) 0.64 && adjBPratio5 <= (float) 0.65) {
                        per_rank5 = 65;
                    }
                    else if (adjBPratio5 > (float) 0.65 && adjBPratio5 <= (float) 0.66) {
                        per_rank5 = 66;
                    }
                    else if (adjBPratio5 > (float) 0.66 && adjBPratio5 <= (float) 0.67) {
                        per_rank5 = 67;
                    }
                    else if (adjBPratio5 > (float) 0.67 && adjBPratio5 <= (float) 0.68) {
                        per_rank5 = 68;
                    }
                    else if (adjBPratio5 > (float) 0.68 && adjBPratio5 <= (float) 0.69) {
                        per_rank5 = 69;
                    }
                    else  {
                        per_rank5 = 70;
                    }
                    
                    
                }//
                else if(adjBPratio5 <= (float) 0.60 && adjBPratio5 >  (float) 0.50){
                    
                    if (adjBPratio5 > (float) 0.50 && adjBPratio5 <= (float) 0.51) {
                        per_rank5 = 51;
                    }
                    else if (adjBPratio5 > (float) 0.51 && adjBPratio5 <= (float) 0.52) {
                        per_rank5 = 52;
                    }
                    else if (adjBPratio5 > (float) 0.52 && adjBPratio5 <= (float) 0.53) {
                        per_rank5 = 53;
                    }
                    else if (adjBPratio5 > (float) 0.53 && adjBPratio5 <= (float) 0.54) {
                        per_rank5 = 54;
                    }
                    else if (adjBPratio5 > (float) 0.54 && adjBPratio5 <= (float) 0.55) {
                        per_rank5 = 55;
                    }
                    else if (adjBPratio5 > (float) 0.55 && adjBPratio5 <= (float) 0.56) {
                        per_rank5 = 56;
                    }
                    else if (adjBPratio5 > (float) 0.56 && adjBPratio5 <= (float) 0.57) {
                        per_rank5 = 57;
                    }
                    else if (adjBPratio5 > (float) 0.57 && adjBPratio5 <= (float) 0.58) {
                        per_rank5 = 58;
                    }
                    else if (adjBPratio5 > (float) 0.58 && adjBPratio5 <= (float) 0.59) {
                        per_rank5 = 59;
                    }
                    else  {
                        per_rank5 = 60;
                    }
                    
                    
                }//
                else if(adjBPratio5 <= (float) 0.50 && adjBPratio5 >  (float) 0.45){
                    
                    if (adjBPratio5 > (float) 0.450 && adjBPratio5 <= (float) 0.455) {
                        per_rank5 = 41;
                    }
                    else if (adjBPratio5 > (float) 0.455 && adjBPratio5 <= (float) 0.460) {
                        per_rank5 = 42;
                    }
                    else if (adjBPratio5 > (float) 0.460 && adjBPratio5 <= (float) 0.465) {
                        per_rank5 = 43;
                    }
                    else if (adjBPratio5 > (float) 0.465 && adjBPratio5 <= (float) 0.470) {
                        per_rank5 = 44;
                    }
                    else if (adjBPratio5 > (float) 0.470 && adjBPratio5 <= (float) 0.475) {
                        per_rank5 = 45;
                    }
                    else if (adjBPratio5 > (float) 0.475 && adjBPratio5 <= (float) 0.480) {
                        per_rank5 = 46;
                    }
                    else if (adjBPratio5 > (float) 0.480 && adjBPratio5 <= (float) 0.485) {
                        per_rank5 = 47;
                    }
                    else if (adjBPratio5 > (float) 0.485 && adjBPratio5 <= (float) 0.490) {
                        per_rank5 = 48;
                    }
                    else if (adjBPratio5 > (float) 0.490 && adjBPratio5 <= (float) 0.495) {
                        per_rank5 = 49;
                    }
                    else  {
                        per_rank5 = 50;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 0.45  && adjBPratio5 > (float) 0.40){
                    
                    
                    if (adjBPratio5 > (float) 0.400 && adjBPratio5 <= (float) 0.405) {
                        per_rank5 = 31;
                    }
                    else if (adjBPratio5 > (float) 0.405 && adjBPratio5 <= (float) 0.410) {
                        per_rank5 = 32;
                    }
                    else if (adjBPratio5 > (float) 0.410 && adjBPratio5 <= (float) 0.415) {
                        per_rank5 = 33;
                    }
                    else if (adjBPratio5 > (float) 0.415 && adjBPratio5 <= (float) 0.420) {
                        per_rank5 = 34;
                    }
                    else if (adjBPratio5 > (float) 0.420 && adjBPratio5 <= (float) 0.425) {
                        per_rank5 = 35;
                    }
                    else if (adjBPratio5 > (float) 0.425 && adjBPratio5 <= (float) 0.430) {
                        per_rank5 = 36;
                    }
                    else if (adjBPratio5 > (float) 0.430 && adjBPratio5 <= (float) 0.435) {
                        per_rank5 = 37;
                    }
                    else if (adjBPratio5 > (float) 0.435 && adjBPratio5 <= (float) 0.440) {
                        per_rank5 = 38;
                    }
                    else if (adjBPratio5 > (float) 0.440 && adjBPratio5 <= (float) 0.445) {
                        per_rank5 = 39;
                    }
                    else  {
                        per_rank5 = 40;
                    }

                }
                else if(adjBPratio5 <= (float) 0.40  && adjBPratio5 > (float) 0.35){
                    
                    if (adjBPratio5 > (float) 0.350 && adjBPratio5 <= (float) 0.355) {
                        per_rank5 = 21;
                    }
                    else if (adjBPratio5 > (float) 0.355 && adjBPratio5 <= (float) 0.360) {
                        per_rank5 = 22;
                    }
                    else if (adjBPratio5 > (float) 0.360 && adjBPratio5 <= (float) 0.365) {
                        per_rank5 = 23;
                    }
                    else if (adjBPratio5 > (float) 0.365 && adjBPratio5 <= (float) 0.370) {
                        per_rank5 = 24;
                    }
                    else if (adjBPratio5 > (float) 0.370 && adjBPratio5 <= (float) 0.375) {
                        per_rank5 = 25;
                    }
                    else if (adjBPratio5 > (float) 0.375 && adjBPratio5 <= (float) 0.380) {
                        per_rank5 = 26;
                    }
                    else if (adjBPratio5 > (float) 0.380 && adjBPratio5 <= (float) 0.385) {
                        per_rank5 = 27;
                    }
                    else if (adjBPratio5 > (float) 0.385 && adjBPratio5 <= (float) 0.390) {
                        per_rank5 = 28;
                    }
                    else if (adjBPratio5 > (float) 0.390 && adjBPratio5 <= (float) 0.395) {
                        per_rank5 = 29;
                    }
                    else  {
                        per_rank5 = 30;
                    }
                    
                }
                else if(adjBPratio5 <= (float) 0.35  && adjBPratio5 > (float) 0.25){
                    
                    if (adjBPratio5 > (float) 0.50 && adjBPratio5 <= (float) 0.51) {
                        per_rank5 = 11;
                    }
                    else if (adjBPratio5 > (float) 0.51 && adjBPratio5 <= (float) 0.52) {
                        per_rank5 = 12;
                    }
                    else if (adjBPratio5 > (float) 0.52 && adjBPratio5 <= (float) 0.53) {
                        per_rank5 = 13;
                    }
                    else if (adjBPratio5 > (float) 0.53 && adjBPratio5 <= (float) 0.54) {
                        per_rank5 = 14;
                    }
                    else if (adjBPratio5 > (float) 0.54 && adjBPratio5 <= (float) 0.55) {
                        per_rank5 = 15;
                    }
                    else if (adjBPratio5 > (float) 0.55 && adjBPratio5 <= (float) 0.56) {
                        per_rank5 = 16;
                    }
                    else if (adjBPratio5 > (float) 0.56 && adjBPratio5 <= (float) 0.57) {
                        per_rank5 = 17;
                    }
                    else if (adjBPratio5 > (float) 0.57 && adjBPratio5 <= (float) 0.58) {
                        per_rank5 = 18;
                    }
                    else if (adjBPratio5 > (float) 0.58 && adjBPratio5 <= (float) 0.59) {
                        per_rank5 = 19;
                    }
                    else  {
                        per_rank5 = 20;
                    }
                    
                }
                
                else if(adjBPratio5 <= (float) 0.25  && adjBPratio5 > (float) 0.20){
                    
                    if (adjBPratio5 > (float) 0.200 && adjBPratio5 <= (float) 0.205) {
                        per_rank5 = 1;
                    }
                    else if (adjBPratio5 > (float) 0.205 && adjBPratio5 <= (float) 0.210) {
                        per_rank5 = 2;
                    }
                    else if (adjBPratio5 > (float) 0.210 && adjBPratio5 <= (float) 0.215) {
                        per_rank5 = 3;
                    }
                    else if (adjBPratio5 > (float) 0.215 && adjBPratio5 <= (float) 0.220) {
                        per_rank5 = 4;
                    }
                    else if (adjBPratio5 > (float) 0.220 && adjBPratio5 <= (float) 0.225) {
                        per_rank5 = 5;
                    }
                    else if (adjBPratio5 > (float) 0.225 && adjBPratio5 <= (float) 0.230) {
                        per_rank5 = 6;
                    }
                    else if (adjBPratio5 > (float) 0.230 && adjBPratio5 <= (float) 0.235) {
                        per_rank5 = 7;
                    }
                    else if (adjBPratio5 > (float) 0.235 && adjBPratio5 <= (float) 0.240) {
                        per_rank5 = 8;
                    }
                    else if (adjBPratio5 > (float) 0.240 && adjBPratio5 <= (float) 0.245) {
                        per_rank5 = 9;
                    }
                    else  {
                        per_rank5 = 10;
                    }

                }
                else if(adjBPratio5 <= (float) 0.20){
                    per_rank5 = 0;
                }
                else if(adjBPratio5 >  (float) 1.200){
                    per_rank5 = 100;
                }
                
                if(per_rank5 == 2222){
                    str_rank5.text = @"--";
                }
                else{
                    str_rank5.text = [NSString stringWithFormat:@"%d", per_rank5];
                }
            }
            else{
                per_rank5 = 2222;
                str_rank5.text = @"--";
            }
        }
        
        
        strg_per_rank5 = per_rank5;
        
        if([str_rank5.text isEqualToString:@"--"]){
            [upElementArray replaceObjectAtIndex:142 withObject:@"0"];
        }
        else{
            [upElementArray replaceObjectAtIndex:142 withObject:[NSString stringWithFormat:@"%.0f",strg_per_rank5]];
        }
        
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    else{
        str_pred5.text = @"";
        str_sw5.text = @"";
        str_rank5.text  = @"--";
        [upElementArray replaceObjectAtIndex:142 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
}
-(void)getLegCurlValue:(float)kgValue :(float)respValue{
    
    if(kgValue > 0.0 && respValue > 0.0){
        
        float ageCorrection = 0.0;
        if(clientCurrentAge > 25){
            ageCorrection = ((clientCurrentAge-25)*0.01025)+1;
        }
        else{
            ageCorrection = 1.0;
        }
        
        float bprm6 = kgValue*2.2/(1.0278-(respValue*0.0278));
        float BPRMkg6 =  bprm6/2.2;
        str_pred6.text = [NSString stringWithFormat:@"%.1f", BPRMkg6];
        
        float BPratio6  =  BPRMkg6/clientWeight;
        float adjBPratio6  =  ageCorrection*BPratio6;
        
        if(isnan(adjBPratio6)){
            str_sw6.text = @"";
        }
        else if(isinf(adjBPratio6)){
            str_sw6.text = @"";
        }
        else if(adjBPratio6 == 0.0){
            str_sw6.text = @"";
        }
        else{
            str_sw6.text = [NSString stringWithFormat:@"%.2f", adjBPratio6];
        }
        //float bench_press_mean_rank = [[dic_mean objectForKey:@"bench press"] floatValue];
        //float bench_press_sd_rank = [[dic_mean objectForKey:@"bench press"] floatValue];
        adjBPratio6 = [[NSString stringWithFormat:@"%.3f", adjBPratio6] floatValue];
        [upElementArray replaceObjectAtIndex:143 withObject:str_sw6.text];
        NSInteger per_rank6 = 2222;
        
        
        if(clientGender == 0){
            //male
            
            if(adjBPratio6 > 0){
                
                
                if(adjBPratio6 <= (float) 0.80  && adjBPratio6 >  (float) 0.70){
                    
                    
                    if (adjBPratio6 > (float) 0.70 && adjBPratio6 <= (float) 0.71) {
                        per_rank6 = 91;
                    }
                    else if (adjBPratio6 > (float) 0.71 && adjBPratio6 <= (float) 0.72) {
                        per_rank6 = 92;
                    }
                    else if (adjBPratio6 > (float) 0.72 && adjBPratio6 <= (float) 0.73) {
                        per_rank6 = 93;
                    }
                    else if (adjBPratio6 > (float) 0.73 && adjBPratio6 <= (float) 0.74) {
                        per_rank6 = 94;
                    }
                    else if (adjBPratio6 > (float) 0.74 && adjBPratio6 <= (float) 0.75) {
                        per_rank6 = 95;
                    }
                    else if (adjBPratio6 > (float) 0.75 && adjBPratio6 <= (float) 0.76) {
                        per_rank6 = 96;
                    }
                    else if (adjBPratio6 > (float) 0.76 && adjBPratio6 <= (float) 0.77) {
                        per_rank6 = 97;
                    }
                    else if (adjBPratio6 > (float) 0.77 && adjBPratio6 <= (float) 0.78) {
                        per_rank6 = 98;
                    }
                    else if (adjBPratio6 > (float) 0.78 && adjBPratio6 <= (float) 0.79) {
                        per_rank6 = 99;
                    }
                    else  {
                        per_rank6 = 100;
                    }
                }
                else if(adjBPratio6 <= (float) 0.700 && adjBPratio6 >  (float) 0.650){
                    
                    
                    if (adjBPratio6 > (float) 0.650 && adjBPratio6 <= (float) 0.655) {
                        per_rank6 = 81;
                    }
                    else if (adjBPratio6 > (float) 0.655 && adjBPratio6 <= (float) 0.660) {
                        per_rank6 = 82;
                    }
                    else if (adjBPratio6 > (float) 0.660 && adjBPratio6 <= (float) 0.665) {
                        per_rank6 = 83;
                    }
                    else if (adjBPratio6 > (float) 0.665 && adjBPratio6 <= (float) 0.670) {
                        per_rank6 = 84;
                    }
                    else if (adjBPratio6 > (float) 0.670 && adjBPratio6 <= (float) 0.675) {
                        per_rank6 = 85;
                    }
                    else if (adjBPratio6 > (float) 0.675 && adjBPratio6 <= (float) 0.680) {
                        per_rank6 = 86;
                    }
                    else if (adjBPratio6 > (float) 0.680 && adjBPratio6 <= (float) 0.685) {
                        per_rank6 = 87;
                    }
                    else if (adjBPratio6 > (float) 0.685 && adjBPratio6 <= (float) 0.690) {
                        per_rank6 = 88;
                    }
                    else if (adjBPratio6 > (float) 0.690 && adjBPratio6 <= (float) 0.695) {
                        per_rank6 = 89;
                    }
                    else  {
                        per_rank6 = 90;
                    }

                }
                else if(adjBPratio6 <= (float) 0.65  && adjBPratio6 >  (float) 0.60){
                    
                    
                    if (adjBPratio6 > (float) 0.600 && adjBPratio6 <= (float) 0.605) {
                        per_rank6 = 71;
                    }
                    else if (adjBPratio6 > (float) 0.605 && adjBPratio6 <= (float) 0.610) {
                        per_rank6 = 72;
                    }
                    else if (adjBPratio6 > (float) 0.610 && adjBPratio6 <= (float) 0.615) {
                        per_rank6 = 73;
                    }
                    else if (adjBPratio6 > (float) 0.615 && adjBPratio6 <= (float) 0.620) {
                        per_rank6 = 74;
                    }
                    else if (adjBPratio6 > (float) 0.620 && adjBPratio6 <= (float) 0.625) {
                        per_rank6 = 75;
                    }
                    else if (adjBPratio6 > (float) 0.625 && adjBPratio6 <= (float) 0.630) {
                        per_rank6 = 76;
                    }
                    else if (adjBPratio6 > (float) 0.630 && adjBPratio6 <= (float) 0.635) {
                        per_rank6 = 77;
                    }
                    else if (adjBPratio6 > (float) 0.635 && adjBPratio6 <= (float) 0.640) {
                        per_rank6 = 78;
                    }
                    else if (adjBPratio6 > (float) 0.640 && adjBPratio6 <= (float) 0.645) {
                        per_rank6 = 79;
                    }
                    else  {
                        per_rank6 = 80;
                    }
                }
                else if(adjBPratio6 <= (float) 0.60 && adjBPratio6 >  (float) 0.55){
                    
                    if (adjBPratio6 > (float) 0.550 && adjBPratio6 <= (float) 0.555) {
                        per_rank6 = 61;
                    }
                    else if (adjBPratio6 > (float) 0.555 && adjBPratio6 <= (float) 0.560) {
                        per_rank6 = 62;
                    }
                    else if (adjBPratio6 > (float) 0.560 && adjBPratio6 <= (float) 0.565) {
                        per_rank6 = 63;
                    }
                    else if (adjBPratio6 > (float) 0.565 && adjBPratio6 <= (float) 0.570) {
                        per_rank6 = 64;
                    }
                    else if (adjBPratio6 > (float) 0.570 && adjBPratio6 <= (float) 0.575) {
                        per_rank6 = 65;
                    }
                    else if (adjBPratio6 > (float) 0.575 && adjBPratio6 <= (float) 0.580) {
                        per_rank6 = 66;
                    }
                    else if (adjBPratio6 > (float) 0.580 && adjBPratio6 <= (float) 0.585) {
                        per_rank6 = 67;
                    }
                    else if (adjBPratio6 > (float) 0.585 && adjBPratio6 <= (float) 0.590) {
                        per_rank6 = 68;
                    }
                    else if (adjBPratio6 > (float) 0.590 && adjBPratio6 <= (float) 0.595) {
                        per_rank6 = 69;
                    }
                    else  {
                        per_rank6 = 70;
                    }
                }
                else if(adjBPratio6 <= (float) 0.55 && adjBPratio6 >  (float) 0.50){
                    
                    if (adjBPratio6 > (float) 0.500 && adjBPratio6 <= (float) 0.505) {
                        per_rank6 = 51;
                    }
                    else if (adjBPratio6 > (float) 0.505 && adjBPratio6 <= (float) 0.510) {
                        per_rank6 = 52;
                    }
                    else if (adjBPratio6 > (float) 0.510 && adjBPratio6 <= (float) 0.515) {
                        per_rank6 = 53;
                    }
                    else if (adjBPratio6 > (float) 0.515 && adjBPratio6 <= (float) 0.520) {
                        per_rank6 = 54;
                    }
                    else if (adjBPratio6 > (float) 0.520 && adjBPratio6 <= (float) 0.525) {
                        per_rank6 = 55;
                    }
                    else if (adjBPratio6 > (float) 0.525 && adjBPratio6 <= (float) 0.530) {
                        per_rank6 = 56;
                    }
                    else if (adjBPratio6 > (float) 0.530 && adjBPratio6 <= (float) 0.535) {
                        per_rank6 = 57;
                    }
                    else if (adjBPratio6 > (float) 0.535 && adjBPratio6 <= (float) 0.540) {
                        per_rank6 = 58;
                    }
                    else if (adjBPratio6 > (float) 0.540 && adjBPratio6 <= (float) 0.545) {
                        per_rank6 = 59;
                    }
                    else  {
                        per_rank6 = 60;
                    }

                }
                else if(adjBPratio6 <= (float) 0.50 && adjBPratio6 >  (float) 0.45){
                    
                    if (adjBPratio6 > (float) 0.450 && adjBPratio6 <= (float) 0.455) {
                        per_rank6 = 41;
                    }
                    else if (adjBPratio6 > (float) 0.455 && adjBPratio6 <= (float) 0.460) {
                        per_rank6 = 42;
                    }
                    else if (adjBPratio6 > (float) 0.460 && adjBPratio6 <= (float) 0.465) {
                        per_rank6 = 43;
                    }
                    else if (adjBPratio6 > (float) 0.465 && adjBPratio6 <= (float) 0.470) {
                        per_rank6 = 44;
                    }
                    else if (adjBPratio6 > (float) 0.470 && adjBPratio6 <= (float) 0.475) {
                        per_rank6 = 45;
                    }
                    else if (adjBPratio6 > (float) 0.475 && adjBPratio6 <= (float) 0.480) {
                        per_rank6 = 46;
                    }
                    else if (adjBPratio6 > (float) 0.480 && adjBPratio6 <= (float) 0.485) {
                        per_rank6 = 47;
                    }
                    else if (adjBPratio6 > (float) 0.485 && adjBPratio6 <= (float) 0.490) {
                        per_rank6 = 48;
                    }
                    else if (adjBPratio6 > (float) 0.490 && adjBPratio6 <= (float) 0.495) {
                        per_rank6 = 49;
                    }
                    else  {
                        per_rank6 = 50;
                    }
                }
                else if(adjBPratio6 <= (float) 0.45  && adjBPratio6 > (float) 0.40){
                    
                    
                    if (adjBPratio6 > (float) 0.400 && adjBPratio6 <= (float) 0.405) {
                        per_rank6 = 31;
                    }
                    else if (adjBPratio6 > (float) 0.405 && adjBPratio6 <= (float) 0.410) {
                        per_rank6 = 32;
                    }
                    else if (adjBPratio6 > (float) 0.410 && adjBPratio6 <= (float) 0.415) {
                        per_rank6 = 33;
                    }
                    else if (adjBPratio6 > (float) 0.415 && adjBPratio6 <= (float) 0.420) {
                        per_rank6 = 34;
                    }
                    else if (adjBPratio6 > (float) 0.420 && adjBPratio6 <= (float) 0.425) {
                        per_rank6 = 35;
                    }
                    else if (adjBPratio6 > (float) 0.425 && adjBPratio6 <= (float) 0.430) {
                        per_rank6 = 36;
                    }
                    else if (adjBPratio6 > (float) 0.430 && adjBPratio6 <= (float) 0.435) {
                        per_rank6 = 37;
                    }
                    else if (adjBPratio6 > (float) 0.435 && adjBPratio6 <= (float) 0.440) {
                        per_rank6 = 38;
                    }
                    else if (adjBPratio6 > (float) 0.440 && adjBPratio6 <= (float) 0.445) {
                        per_rank6 = 39;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                }
                else if(adjBPratio6 <= (float) 0.40  && adjBPratio6 >  (float) 0.35){
                    
                    if (adjBPratio6 > (float) 0.350 && adjBPratio6 <= (float) 0.355) {
                        per_rank6 = 21;
                    }
                    else if (adjBPratio6 > (float) 0.355 && adjBPratio6 <= (float) 0.360) {
                        per_rank6 = 22;
                    }
                    else if (adjBPratio6 > (float) 0.360 && adjBPratio6 <= (float) 0.365) {
                        per_rank6 = 23;
                    }
                    else if (adjBPratio6 > (float) 0.365 && adjBPratio6 <= (float) 0.370) {
                        per_rank6 = 24;
                    }
                    else if (adjBPratio6 > (float) 0.370 && adjBPratio6 <= (float) 0.375) {
                        per_rank6 = 25;
                    }
                    else if (adjBPratio6 > (float) 0.375 && adjBPratio6 <= (float) 0.380) {
                        per_rank6 = 26;
                    }
                    else if (adjBPratio6 > (float) 0.380 && adjBPratio6 <= (float) 0.385) {
                        per_rank6 = 27;
                    }
                    else if (adjBPratio6 > (float) 0.385 && adjBPratio6 <= (float) 0.390) {
                        per_rank6 = 28;
                    }
                    else if (adjBPratio6 > (float) 0.390 && adjBPratio6 <= (float) 0.395) {
                        per_rank6 = 29;
                    }
                    else  {
                        per_rank6 = 30;
                    }
                }
                else if(adjBPratio6 <= (float) 0.35  && adjBPratio6 > (float) 0.30){
                    
                    
                    if (adjBPratio6 > (float) 0.300 && adjBPratio6 <= (float) 0.305) {
                        per_rank6 = 11;
                    }
                    else if (adjBPratio6 > (float) 0.305 && adjBPratio6 <= (float) 0.310) {
                        per_rank6 = 12;
                    }
                    else if (adjBPratio6 > (float) 0.310 && adjBPratio6 <= (float) 0.315) {
                        per_rank6 = 13;
                    }
                    else if (adjBPratio6 > (float) 0.315 && adjBPratio6 <= (float) 0.320) {
                        per_rank6 = 14;
                    }
                    else if (adjBPratio6 > (float) 0.320 && adjBPratio6 <= (float) 0.325) {
                        per_rank6 = 15;
                    }
                    else if (adjBPratio6 > (float) 0.325 && adjBPratio6 <= (float) 0.330) {
                        per_rank6 = 16;
                    }
                    else if (adjBPratio6 > (float) 0.330 && adjBPratio6 <= (float) 0.335) {
                        per_rank6 = 17;
                    }
                    else if (adjBPratio6 > (float) 0.335 && adjBPratio6 <= (float) 0.340) {
                        per_rank6 = 18;
                    }
                    else if (adjBPratio6 > (float) 0.340 && adjBPratio6 <= (float) 0.345) {
                        per_rank6 = 19;
                    }
                    else  {
                        per_rank6 = 20;
                    }

                }
                else if(adjBPratio6 <= (float) 0.30  && adjBPratio6 > (float) 0.25){
                    
                    
                    if (adjBPratio6 > (float) 0.250 && adjBPratio6 <= (float) 0.255) {
                        per_rank6 = 1;
                    }
                    else if (adjBPratio6 > (float) 0.255 && adjBPratio6 <= (float) 0.260) {
                        per_rank6 = 2;
                    }
                    else if (adjBPratio6 > (float) 0.260 && adjBPratio6 <= (float) 0.265) {
                        per_rank6 = 3;
                    }
                    else if (adjBPratio6 > (float) 0.265 && adjBPratio6 <= (float) 0.270) {
                        per_rank6 = 4;
                    }
                    else if (adjBPratio6 > (float) 0.270 && adjBPratio6 <= (float) 0.275) {
                        per_rank6 = 5;
                    }
                    else if (adjBPratio6 > (float) 0.275 && adjBPratio6 <= (float) 0.280) {
                        per_rank6 = 6;
                    }
                    else if (adjBPratio6 > (float) 0.280 && adjBPratio6 <= (float) 0.285) {
                        per_rank6 = 7;
                    }
                    else if (adjBPratio6 > (float) 0.285 && adjBPratio6 <= (float) 0.290) {
                        per_rank6 = 8;
                    }
                    else if (adjBPratio6 > (float) 0.290 && adjBPratio6 <= (float) 0.295) {
                        per_rank6 = 9;
                    }
                    else  {
                        per_rank6 = 10;
                    }
                }
                else if(adjBPratio6 <= (float) 0.25){
                    per_rank6 = 0;
                }
                else if(adjBPratio6 >  (float) 0.800){
                    per_rank6 = 100;
                }
                
                if(per_rank6 == 2222){
                    str_rank6.text = @"--";
                }
                else{
                    str_rank6.text = [NSString stringWithFormat:@"%d", per_rank6];
                }
            }
            else{
                per_rank6 = 2222;
                str_rank6.text = @"--";
            }
        }
        else{
            //female
            
            if(adjBPratio6 > 0){
                
                
                if(adjBPratio6 <= (float) 0.75  && adjBPratio6 >  (float) 0.65){
                    
                    if (adjBPratio6 > (float) 0.65 && adjBPratio6 <= (float) 0.66) {
                        per_rank6 = 91;
                    }
                    else if (adjBPratio6 > (float) 0.66 && adjBPratio6 <= (float) 0.67) {
                        per_rank6 = 92;
                    }
                    else if (adjBPratio6 > (float) 0.67 && adjBPratio6 <= (float) 0.68) {
                        per_rank6 = 93;
                    }
                    else if (adjBPratio6 > (float) 0.68 && adjBPratio6 <= (float) 0.69) {
                        per_rank6 = 94;
                    }
                    else if (adjBPratio6 > (float) 0.69 && adjBPratio6 <= (float) 0.70) {
                        per_rank6 = 95;
                    }
                    else if (adjBPratio6 > (float) 0.70 && adjBPratio6 <= (float) 0.71) {
                        per_rank6 = 96;
                    }
                    else if (adjBPratio6 > (float) 0.71 && adjBPratio6 <= (float) 0.72) {
                        per_rank6 = 97;
                    }
                    else if (adjBPratio6 > (float) 0.72 && adjBPratio6 <= (float) 0.73) {
                        per_rank6 = 98;
                    }
                    else if (adjBPratio6 > (float) 0.73 && adjBPratio6 <= (float) 0.74) {
                        per_rank6 = 99;
                    }
                    else  {
                        per_rank6 = 100;
                    }
                }
                if(adjBPratio6 <= (float) 0.65  && adjBPratio6 >  (float) 0.60){
                    
                    if (adjBPratio6 > (float) 0.60 && adjBPratio6 <= (float) 0.605) {
                        per_rank6 = 81;
                    }
                    else if (adjBPratio6 > (float) 0.605 && adjBPratio6 <= (float) 0.610) {
                        per_rank6 = 82;
                    }
                    else if (adjBPratio6 > (float) 0.610 && adjBPratio6 <= (float) 0.615) {
                        per_rank6 = 83;
                    }
                    else if (adjBPratio6 > (float) 0.615 && adjBPratio6 <= (float) 0.620) {
                        per_rank6 = 84;
                    }
                    else if (adjBPratio6 > (float) 0.620 && adjBPratio6 <= (float) 0.625) {
                        per_rank6 = 85;
                    }
                    else if (adjBPratio6 > (float) 0.625 && adjBPratio6 <= (float) 0.630) {
                        per_rank6 = 96;
                    }
                    else if (adjBPratio6 > (float) 0.630 && adjBPratio6 <= (float) 0.635) {
                        per_rank6 = 87;
                    }
                    else if (adjBPratio6 > (float) 0.635 && adjBPratio6 <= (float) 0.640) {
                        per_rank6 = 88;
                    }
                    else if (adjBPratio6 > (float) 0.640 && adjBPratio6 <= (float) 0.645) {
                        per_rank6 = 89;
                    }
                    else  {
                        per_rank6 = 90;
                    }
                }
                if(adjBPratio6 <= (float) 0.60  && adjBPratio6 >  (float) 0.55){
                    
                    if (adjBPratio6 > (float) 0.550 && adjBPratio6 <= (float) 0.555) {
                        per_rank6 = 71;
                    }
                    else if (adjBPratio6 > (float) 0.555 && adjBPratio6 <= (float) 0.560) {
                        per_rank6 = 72;
                    }
                    else if (adjBPratio6 > (float) 0.560 && adjBPratio6 <= (float) 0.565) {
                        per_rank6 = 73;
                    }
                    else if (adjBPratio6 > (float) 0.565 && adjBPratio6 <= (float) 0.570) {
                        per_rank6 = 74;
                    }
                    else if (adjBPratio6 > (float) 0.570 && adjBPratio6 <= (float) 0.575) {
                        per_rank6 = 75;
                    }
                    else if (adjBPratio6 > (float) 0.575 && adjBPratio6 <= (float) 0.580) {
                        per_rank6 = 76;
                    }
                    else if (adjBPratio6 > (float) 0.580 && adjBPratio6 <= (float) 0.585) {
                        per_rank6 = 77;
                    }
                    else if (adjBPratio6 > (float) 0.585 && adjBPratio6 <= (float) 0.590) {
                        per_rank6 = 78;
                    }
                    else if (adjBPratio6 > (float) 0.590 && adjBPratio6 <= (float) 0.595) {
                        per_rank6 = 79;
                    }
                    else  {
                        per_rank6 = 80;
                    }
                }
                else if(adjBPratio6 <= (float) 0.55  && adjBPratio6 >  (float) 0.50){
                    
                    
                    if (adjBPratio6 > (float) 0.50 && adjBPratio6 <= (float) 0.505) {
                        per_rank6 = 61;
                    }
                    else if (adjBPratio6 > (float) 0.505 && adjBPratio6 <= (float) 0.510) {
                        per_rank6 = 62;
                    }
                    else if (adjBPratio6 > (float) 0.510 && adjBPratio6 <= (float) 0.515) {
                        per_rank6 = 63;
                    }
                    else if (adjBPratio6 > (float) 0.515 && adjBPratio6 <= (float) 0.520) {
                        per_rank6 = 64;
                    }
                    else if (adjBPratio6 > (float) 0.520 && adjBPratio6 <= (float) 0.525) {
                        per_rank6 = 65;
                    }
                    else if (adjBPratio6 > (float) 0.525 && adjBPratio6 <= (float) 0.530) {
                        per_rank6 = 66;
                    }
                    else if (adjBPratio6 > (float) 0.530 && adjBPratio6 <= (float) 0.535) {
                        per_rank6 = 67;
                    }
                    else if (adjBPratio6 > (float) 0.535 && adjBPratio6 <= (float) 0.540) {
                        per_rank6 = 68;
                    }
                    else if (adjBPratio6 > (float) 0.540 && adjBPratio6 <= (float) 0.545) {
                        per_rank6 = 69;
                    }
                    else  {
                        per_rank6 = 70;
                    }
                }
                else if(adjBPratio6 <= (float) 0.50 && adjBPratio6 >  (float) 0.45){
                    
                    if (adjBPratio6 > (float) 0.450 && adjBPratio6 <= (float) 0.455) {
                        per_rank6 = 51;
                    }
                    else if (adjBPratio6 > (float) 0.455 && adjBPratio6 <= (float) 0.460) {
                        per_rank6 = 52;
                    }
                    else if (adjBPratio6 > (float) 0.460 && adjBPratio6 <= (float) 0.465) {
                        per_rank6 = 53;
                    }
                    else if (adjBPratio6 > (float) 0.465 && adjBPratio6 <= (float) 0.470) {
                        per_rank6 = 54;
                    }
                    else if (adjBPratio6 > (float) 0.470 && adjBPratio6 <= (float) 0.475) {
                        per_rank6 = 55;
                    }
                    else if (adjBPratio6 > (float) 0.475 && adjBPratio6 <= (float) 0.480) {
                        per_rank6 = 56;
                    }
                    else if (adjBPratio6 > (float) 0.480 && adjBPratio6 <= (float) 0.485) {
                        per_rank6 = 57;
                    }
                    else if (adjBPratio6 > (float) 0.485 && adjBPratio6 <= (float) 0.490) {
                        per_rank6 = 58;
                    }
                    else if (adjBPratio6 > (float) 0.490 && adjBPratio6 <= (float) 0.495) {
                        per_rank6 = 59;
                    }
                    else  {
                        per_rank6 = 60;
                    }
                }
                else if(adjBPratio6 <= (float) 0.45 && adjBPratio6 >  (float) 0.40){
                    
                    if (adjBPratio6 > (float) 0.40 && adjBPratio6 <= (float) 0.405) {
                        per_rank6 = 41;
                    }
                    else if (adjBPratio6 > (float) 0.405 && adjBPratio6 <= (float) 0.410) {
                        per_rank6 = 42;
                    }
                    else if (adjBPratio6 > (float) 0.410 && adjBPratio6 <= (float) 0.415) {
                        per_rank6 = 43;
                    }
                    else if (adjBPratio6 > (float) 0.415 && adjBPratio6 <= (float) 0.420) {
                        per_rank6 = 44;
                    }
                    else if (adjBPratio6 > (float) 0.420 && adjBPratio6 <= (float) 0.425) {
                        per_rank6 = 45;
                    }
                    else if (adjBPratio6 > (float) 0.425 && adjBPratio6 <= (float) 0.430) {
                        per_rank6 = 46;
                    }
                    else if (adjBPratio6 > (float) 0.430 && adjBPratio6 <= (float) 0.435) {
                        per_rank6 = 47;
                    }
                    else if (adjBPratio6 > (float) 0.435 && adjBPratio6 <= (float) 0.440) {
                        per_rank6 = 48;
                    }
                    else if (adjBPratio6 > (float) 0.440 && adjBPratio6 <= (float) 0.445) {
                        per_rank6 = 49;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                }
                else if(adjBPratio6 <= (float) 0.40 && adjBPratio6 >  (float) 0.35){
                    
                    if (adjBPratio6 > (float) 0.35 && adjBPratio6 <= (float) 0.355) {
                        per_rank6 = 31;
                    }
                    else if (adjBPratio6 > (float) 0.355 && adjBPratio6 <= (float) 0.360) {
                        per_rank6 = 32;
                    }
                    else if (adjBPratio6 > (float) 0.360 && adjBPratio6 <= (float) 0.365) {
                        per_rank6 = 33;
                    }
                    else if (adjBPratio6 > (float) 0.365 && adjBPratio6 <= (float) 0.370) {
                        per_rank6 = 34;
                    }
                    else if (adjBPratio6 > (float) 0.370 && adjBPratio6 <= (float) 0.375) {
                        per_rank6 = 35;
                    }
                    else if (adjBPratio6 > (float) 0.375 && adjBPratio6 <= (float) 0.380) {
                        per_rank6 = 36;
                    }
                    else if (adjBPratio6 > (float) 0.380 && adjBPratio6 <= (float) 0.385) {
                        per_rank6 = 37;
                    }
                    else if (adjBPratio6 > (float) 0.385 && adjBPratio6 <= (float) 0.390) {
                        per_rank6 = 38;
                    }
                    else if (adjBPratio6 > (float) 0.390 && adjBPratio6 <= (float) 0.395) {
                        per_rank6 = 39;
                    }
                    else  {
                        per_rank6 = 40;
                    }
                    
                }
                else if(adjBPratio6 <= (float) 0.35 && adjBPratio6 >  (float) 0.25){
                    
                    if (adjBPratio6 > (float) 0.25 && adjBPratio6 <= (float) 0.26) {
                        per_rank6 = 21;
                    }
                    else if (adjBPratio6 > (float) 0.26 && adjBPratio6 <= (float) 0.27) {
                        per_rank6 = 22;
                    }
                    else if (adjBPratio6 > (float) 0.27 && adjBPratio6 <= (float) 0.28) {
                        per_rank6 = 23;
                    }
                    else if (adjBPratio6 > (float) 0.28 && adjBPratio6 <= (float) 0.29) {
                        per_rank6 = 24;
                    }
                    else if (adjBPratio6 > (float) 0.29 && adjBPratio6 <= (float) 0.30) {
                        per_rank6 = 25;
                    }
                    else if (adjBPratio6 > (float) 0.30 && adjBPratio6 <= (float) 0.31) {
                        per_rank6 = 26;
                    }
                    else if (adjBPratio6 > (float) 0.31 && adjBPratio6 <= (float) 0.32) {
                        per_rank6 = 27;
                    }
                    else if (adjBPratio6 > (float) 0.32 && adjBPratio6 <= (float) 0.33) {
                        per_rank6 = 28;
                    }
                    else if (adjBPratio6 > (float) 0.33 && adjBPratio6 <= (float) 0.34) {
                        per_rank6 = 29;
                    }
                    else  {
                        per_rank6 = 30;
                    }
                    
                }
                else if(adjBPratio6 <= (float) 0.25 && adjBPratio6 >  (float) 0.15){
                    
                    if (adjBPratio6 > (float) 0.15 && adjBPratio6 <= (float) 0.16) {
                        per_rank6 = 11;
                    }
                    else if (adjBPratio6 > (float) 0.16 && adjBPratio6 <= (float) 0.17) {
                        per_rank6 = 12;
                    }
                    else if (adjBPratio6 > (float) 0.17 && adjBPratio6 <= (float) 0.18) {
                        per_rank6 = 13;
                    }
                    else if (adjBPratio6 > (float) 0.18 && adjBPratio6 <= (float) 0.19) {
                        per_rank6 = 14;
                    }
                    else if (adjBPratio6 > (float) 0.19 && adjBPratio6 <= (float) 0.20) {
                        per_rank6 = 15;
                    }
                    else if (adjBPratio6 > (float) 0.20 && adjBPratio6 <= (float) 0.21) {
                        per_rank6 = 16;
                    }
                    else if (adjBPratio6 > (float) 0.21 && adjBPratio6 <= (float) 0.22) {
                        per_rank6 = 17;
                    }
                    else if (adjBPratio6 > (float) 0.22 && adjBPratio6 <= (float) 0.23) {
                        per_rank6 = 18;
                    }
                    else if (adjBPratio6 > (float) 0.23 && adjBPratio6 <= (float) 0.24) {
                        per_rank6 = 19;
                    }
                    else  {
                        per_rank6 = 20;
                    }
                    
                }
                else if(adjBPratio6 <= (float) 0.15 && adjBPratio6 >  (float) 0.0){
                    
                    if (adjBPratio6 > (float) 0.0 && adjBPratio6 <= (float) 0.015) {
                        per_rank6 = 1;
                    }
                    else if (adjBPratio6 > (float) 0.015 && adjBPratio6 <= (float) 0.030) {
                        per_rank6 = 2;
                    }
                    else if (adjBPratio6 > (float) 0.030 && adjBPratio6 <= (float) 0.045) {
                        per_rank6 = 3;
                    }
                    else if (adjBPratio6 > (float) 0.045 && adjBPratio6 <= (float) 0.060) {
                        per_rank6 = 4;
                    }
                    else if (adjBPratio6 > (float) 0.060 && adjBPratio6 <= (float) 0.075) {
                        per_rank6 = 5;
                    }
                    else if (adjBPratio6 > (float) 0.075 && adjBPratio6 <= (float) 0.090) {
                        per_rank6 = 6;
                    }
                    else if (adjBPratio6 > (float) 0.090 && adjBPratio6 <= (float) 0.105) {
                        per_rank6 = 7;
                    }
                    else if (adjBPratio6 > (float) 0.105 && adjBPratio6 <= (float) 0.120) {
                        per_rank6 = 8;
                    }
                    else if (adjBPratio6 > (float) 0.120 && adjBPratio6 <= (float) 0.135) {
                        per_rank6 = 9;
                    }
                    else  {
                        per_rank6 = 10;
                    }
                    
                }
                else if(adjBPratio6 <= (float) 0.0){
                    per_rank6 = 0;
                }
                else if(adjBPratio6 >  (float) 0.750){
                    per_rank6 = 100;
                }
                if(per_rank6 == 2222){
                    str_rank6.text = @"--";
                }
                else{
                    str_rank6.text = [NSString stringWithFormat:@"%d", per_rank6];
                }
                
            }
            else{
                per_rank6 = 2222;
                str_rank6.text = @"--";
            }
        }
        
        strg_per_rank6 = per_rank6;
        
        if([str_rank6.text isEqualToString:@"--"]){
            [upElementArray replaceObjectAtIndex:144 withObject:@"0"];
        }
        else{
            [upElementArray replaceObjectAtIndex:144 withObject:[NSString stringWithFormat:@"%.0f",strg_per_rank6]];
        }
        
        
        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
    else{
        str_pred6.text = @"";
        str_sw6.text = @"";
        str_rank6.text  = @"--";
        [upElementArray replaceObjectAtIndex:144 withObject:@"0"];

        float total_percent_rank = [self getPercent_Rank:strg_per_rank1 :strg_per_rank2 :strg_per_rank3 :strg_per_rank4 :strg_per_rank5 :strg_per_rank6 :avgGrip_percent];
        
        if(total_percent_rank > 0.0){
            
            float total_percent_rankTEMP = total_percent_rank;
            str_avgGripPercent.text = [NSString stringWithFormat:@"%.1f", total_percent_rankTEMP];
        }
    }
}

?>