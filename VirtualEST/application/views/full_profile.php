
<?php

// To show Hide Additional Info

if (isset($_SESSION['risk_factor']))
	{
	$riskdiv = "block";
	}
  else
	{
	$riskdiv = "none";
	}

if (isset($_SESSION['VO2max']))
	{
	$vo2maxdiv = "block";
	}
  else
	{
	$vo2maxdiv = "none";
	}

if (isset($_SESSION['bodyfat']))
	{
	$bodyfatdiv = "block";
	}
  else
	{
	$bodyfatdiv = "none";
	}

$isvp = $_SESSION['is_virtual'];
?>

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php
echo "$base/$css" ?>">
<link rel="stylesheet" type="text/css" href="<?php
echo "$base/assets/css/" ?>style.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		var isvp='<?php
echo $isvp; ?>';
        if(isvp == 1)
        {
		 $(".v_person").fadeTo( "slow" , 1, function() {});
		 $(".v_detail").toggle();
		} 
		
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".overlay").show();
		$(".info_block").show();		
	});  
	$(document).on('click','.close', function(){
		$(".info_block").hide();
		$(".overlay").hide();
	}); 
	
	
	  $(document).on('click', '.info_block_new_btn', function () {
                var info_id = this.id;
                 $.ajax({
                  url:'<?php
echo site_url('Body/get_info_details'); ?>',  
                   type:'POST',
                   data:{info_id:info_id},
                   dataType:'json',
                   success:function(data)
                    {
                    $(".info_block_head").text(data.heading);
                    $("#headings").text(data.heading);       
                    $("#content").html(data.head_content);
                    $("#method").text(data.method_content);
                    var info_images=data.images; 
		    var ajax_image = "<img src='http://" + window.location.hostname + "/VirtualEST/assets/images/est_anthrop/"+info_images+"'/>";
                    $(".info_img").html(ajax_image);
                    }
                 })   
                $(".info_block_new").toggle();
                $(".overlay").toggle();
            });
	$(document).on('click','.close_new', function(){
		$(".info_block_new").toggle();
		$(".overlay").toggle();
	}); 
</script>
    
<!-- FOR BODY COMPOSITION -->    
    
<script>
$(document).on('click','#restricted_profile', function(){


        // alert('done');


		document.forms["myform"].submit();	  
        $("#restricted_profile").attr("href", "<?php
echo site_url('Body/restricted_profile'); ?>");
	});  
    
    
$(document).on('click','#full_profile', function(){


         // alert('done doen');


		document.forms["myform"].submit();	  
        $("#full_profile").attr("href", "<?php
echo site_url('Body/full_profile'); ?>");
	});  
    
$(document).on('click','#error_analysis', function(){
		document.forms["myform"].submit();	  
        $("#error_analysis").attr("href", "<?php
echo site_url('Body/error_analysis'); ?>");
	});  

$(document).on('click','#changing_body_mass', function(){
		document.forms["myform"].submit();	  
        $("#changing_body_mass").attr("href", "<?php
echo site_url('Body/changing_body_mass'); ?>");
	});	
</script>    
        
    
    
    
    
</head>
<body>
	<!-- insertion of  header -->
	<?php
echo $header_insert; ?>

<div class="overlay">&nbsp;</div>
<div class="info_block">
            <div class="info_block_head_top violet_container">Full Profile</div>
            <p>The Full Profile allows the user to enter up to 9 skinfolds, 13 girths, and 19 bone lengths and breadths in addition to height and body mass. 
			The Full Profile allows the user to explore a range of analyses including: phantom z-scores for each anthropometry variable, % body fat using an 
			extensive range of prediction equations, skinfold sums and skinfold maps, somatotype, fractionation of body mass,  and plot variables against national 
			age-based and sex-based norms. Click the 'Virtual Profile' button to  generate a 'virtual person' where plausible values are generated in the program to
			be used in the various analyses. These can also be changed as the user explores the various analyses and outputs.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>         

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array(
	'userid' => $id
);

// $attributes = array('id' => 'myform' , 'name'=>'myform');

echo form_open('Body/full_actions', array(
	'id' => 'myform',
	'name' => 'myform'
) , $hidden); ?> 
	
    <div class="contain">
		
        <!--Start right --> 

        <div class="right-section right-section_new">

		<div class="top_tabs">
             	 <div class="lite_btn" id="phantom" name="phantom" onclick="getPhantomZscore()">Phantom</div>
                 <div class="lite_btn" id="body_fat" name="body_fat" onclick="getBodyFat()">% Body fat</div>
                 <div class="lite_btn" id="skinfold" name="skinfold" onclick="getSkinfolds()">Skinfolds</div>
                 <div class="lite_btn" id="somatotype" name="somatotype" onclick="getSomatotype()">Somatotype</div>
                 <div class="lite_btn" id="anthropometry" name="anthropometry" onclick="getAnthropometry()">Norms</div>
                 <div class="lite_btn" id="fractionation" name="fractionation" onclick="getFractionation()">Fractionation</div>
             </div>

        	<div class="right-head">Full Profile</div>
            
            <div class="field_row verticle_field_row" style="border-bottom:0; padding-bottom:0;"> 
                <div class="field_24">
                    <label>Gender</label>
                    <!--input type="text" id="gender" name="gender" value="<?php //if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}

?>" --> 
                    <input type="text" id="gender" name="gender" value="<?php

if (isset($_SESSION['user_gender']))
	{
	if ($_SESSION['user_gender'] === "M")
		{
		echo "Male";
		}
	  else
		{
		echo "Female";
		}
	}
  else
	{
	if ($fieldData[0]->gender == "Male")
		{
		echo "Male";
		}
	  else
		{
		echo "Female";
		}
	}

?>" tabindex="1"> 
                </div>
                <div class="field_24">
                	<label>Age (yr)</label>
                	<input type="text" id="age" name="age" value="<?php

if (!empty($fieldData[0]->age))
	{
	echo $fieldData[0]->age;
	}
  else
	{
	echo $_SESSION['age'];
	} ?>" tabindex="2"> 
                </div>
                <div class="field_24">
                    <label>Height [cm]</label>
                    <input type="text" id="height" name="height" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php

if (isset($fieldData[0]->height))
	{
	echo $fieldData[0]->height;
	}
  else
	{
	echo round($_SESSION['HEIGHT'], 1);
	} ?>" tabindex="3">                  
                </div>
                <div class="field_24">
                	<label>Body mass [kg]</label>
                    <input type="text" id="body_mass" name="body_mass" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php

if (isset($fieldData[0]->body_mass))
	{
	echo $fieldData[0]->body_mass;
	}
  else
	{
	echo $_SESSION['MASS'];
	} ?>" tabindex="4"> 
                </div>
             </div>
             
             <div class="field_row verticle_field_row"> 
                 <div class="field_24">
                    <!--label>Create virtual profile</label-->                    
                    <button id="virtual_profile" name="virtual_profile" class="lite_btn btn_virtual">Virtual Profile</button>
                    <button id="clear_profile" name="clear_profile" class="lite_btn btn_virtual"  style="display:none;">Clear</button>
                </div>
            </div>
            
            <div class="field_row verticle_field_row" style="border:0;"> 
            	<div class="field_24">
					<strong class="color_vio">Skinfolds (mm)</strong>
                    
                    <div class="resp_field">
                    	<label>Triceps <a href="#" class="info_block_new_btn" id="1"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="triceps" name="triceps" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php

if (isset($fieldData[0]->triceps))
	{
	echo $fieldData[0]->triceps;
	}
  else
	{
	echo $_SESSION['triceps'];
	} ?>" tabindex="5">
                    </div>  
                    <div class="resp_field">
                    	 <label>Subscapular <a href="#" class="info_block_new_btn" id="2"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="subscapular" name="subscapular" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->subscapular) ? $fieldData[0]->subscapular : $_SESSION['subscapular']; ?>" tabindex="6">
                    </div>  
                    <div class="resp_field">
                    	<label>Biceps <a href="#" class="info_block_new_btn" id="3"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>                       
						<input type="text" id="biceps" name="biceps" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->biceps) ? $fieldData[0]->biceps : $_SESSION['biceps']; ?>" tabindex="7">
                    </div>  
                    <div class="resp_field">
                    	<label>Iliac crest <a href="#" class="info_block_new_btn" id="4"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>                        <input type="text" id="iliac_crest" name="iliac_crest" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->iliac_crest) ? $fieldData[0]->iliac_crest : $_SESSION['iliac_crest']; ?>" tabindex="8">
                    </div>  
                    <div class="resp_field">
                    	<label>Supraspinale <a href="#" class="info_block_new_btn" id="5"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>                        <input type="text" id="supraspinale" name="supraspinale" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->supraspinale) ? $fieldData[0]->supraspinale : $_SESSION['supraspinale']; ?>" tabindex="9">
                    </div>  
                    <div class="resp_field">
                    	<label>Abdominal <a href="#" class="info_block_new_btn" id="6"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>                       <input type="text" id="abdominal" name="abdominal" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->abdominal) ? $fieldData[0]->abdominal : $_SESSION['abdominal']; ?>" tabindex="10">
                    </div>  
                    <div class="resp_field">
                    	<label>Front thigh <a href="#" class="info_block_new_btn" id="7"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="thigh" name="thigh" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->thigh) ? $fieldData[0]->thigh : $_SESSION['thigh']; ?>" tabindex="11">
                    </div>  
                    <div class="resp_field">
                    	 <label>Medial calf <a href="#" class="info_block_new_btn" id="8"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="calf" name="calf" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->calf) ? $fieldData[0]->calf : $_SESSION['calf']; ?>" tabindex="12">
                    </div>  
                    <div class="resp_field">
                    	 <label>Mid-axilla <a href="#" class="info_block_new_btn" id="9"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="mid_axilla" name="mid_axilla" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->mid_axilla) ? $fieldData[0]->mid_axilla : $_SESSION['mid_axilla']; ?>" tabindex="13">
                    </div>    
                </div>
                
                <div class="field_24">
                	<strong class="color_vio">Girths (cm)</strong>
                    
                    <div class="resp_field">
                    	<label>Head <a href="#" class="info_block_new_btn" id="10"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="headG" name="headG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->headG) ? $fieldData[0]->headG : $_SESSION['headG']; ?>" tabindex="14">
                    </div>  
                    <div class="resp_field">
                    	<label>Neck <a href="#" class="info_block_new_btn" id="11"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="neckG" name="neckG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->neckG) ? $fieldData[0]->neckG : $_SESSION['neckG']; ?>" tabindex="15">
                    </div>  
                    <div class="resp_field">
                    	<label>Arm (relaxed)<a href="#" class="info_block_new_btn" id="12"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="relArmG" name="relArmG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->relArmG) ? $fieldData[0]->relArmG : $_SESSION['relArmG']; ?>" tabindex="16">
                    </div>  
                    <div class="resp_field">
                    	<label>Arm (flexed)<a href="#" class="info_block_new_btn" id="13"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="flexArmG" name="flexArmG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->flexArmG) ? $fieldData[0]->flexArmG : $_SESSION['flexArmG']; ?>" tabindex="17">
                    </div>  
                    <div class="resp_field">
                    	<label>Forearm (maximum)<a href="#" class="info_block_new_btn" id="14"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="forearmG" name="forearmG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->forearmG) ? $fieldData[0]->forearmG : $_SESSION['forearmG']; ?>" tabindex="18">
                    </div>  
                    <div class="resp_field">
                    	<label>Wrist (distal styloids)<a href="#" class="info_block_new_btn" id="15"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="wristG" name="wristG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->wristG) ? $fieldData[0]->wristG : $_SESSION['wristG']; ?>" tabindex="19">
                    </div>  
                    <div class="resp_field">
                    	<label>Chest (mesosternale)<a href="#" class="info_block_new_btn" id="16"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="chestG" name="chestG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->chestG) ? $fieldData[0]->chestG : $_SESSION['chestG']; ?>" tabindex="20">
                    </div>  
                    <div class="resp_field">
                    	<label>Waist (minimum)<a href="#" class="info_block_new_btn" id="17"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="waistG" name="waistG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->waistG) ? $fieldData[0]->waistG : $_SESSION['waistG']; ?>" tabindex="21">
                    </div>  
                    <div class="resp_field">
                    	<label>Gluteal (hips)<a href="#" class="info_block_new_btn" id="18"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="hipG" name="hipG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->hipG) ? $fieldData[0]->hipG : $_SESSION['hipG']; ?>" tabindex="22">
                    </div>  
                    <div class="resp_field">
                    	<label>Thigh (1 cm distal)<a href="#" class="info_block_new_btn" id="19"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="thighG" name="thighG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->thighG) ? $fieldData[0]->thighG : $_SESSION['thighG']; ?>" tabindex="23">
                    </div>  
                    <div class="resp_field">
                    	<label>Thigh (mid)<a href="#" class="info_block_new_btn" id="20"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="midThighG" name="midThighG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->midThighG) ? $fieldData[0]->midThighG : $_SESSION['midThighG']; ?>" tabindex="24">
                    </div>  
                    <div class="resp_field">
                    	<label>Calf (maximum)<a href="#" class="info_block_new_btn" id="21"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="calfG" name="calfG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->calfG) ? $fieldData[0]->calfG : $_SESSION['calfG']; ?>" tabindex="25">
                    </div>  
                    <div class="resp_field">
                    	<label>Ankle (minimum)<a href="#" class="info_block_new_btn" id="22"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="ankleG" name="ankleG" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->ankleG) ? $fieldData[0]->ankleG : $_SESSION['ankleG']; ?>" tabindex="26">
                    </div>  
                </div>
                
                <div class="field_24">
                	<strong class="color_vio">Lengths (cm)</strong>
                    
                    <div class="resp_field">
                    	<label>Acromiale-radiale<a href="#" class="info_block_new_btn" id="23"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="acRad" name="acRad" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->acRad) ? $fieldData[0]->acRad : $_SESSION['acRad']; ?>" tabindex="27">
                    </div>  
                    <div class="resp_field">
                    	<label>Radiale-styl<a href="#" class="info_block_new_btn" id="24"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="radStyl" name="radStyl" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->radStyl) ? $fieldData[0]->radStyl : $_SESSION['radStyl']; ?>" tabindex="28">
                    </div>  
                    <div class="resp_field">
                    	<label>Midstyl-dacty<a href="#" class="info_block_new_btn" id="25"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="midStylDact" name="midStylDact" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->midStylDact) ? $fieldData[0]->midStylDact : $_SESSION['midStylDact']; ?>" tabindex="29">
                    </div>  
                    <div class="resp_field">
                    	<label>Iliospinale<a href="#" class="info_block_new_btn" id="26"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="iliospinale" name="iliospinale" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->iliospinale) ? $fieldData[0]->iliospinale : $_SESSION['iliospinale']; ?>" tabindex="30">
                    </div>  <div class="resp_field">
                    	<label>Trochanterion<a href="#" class="info_block_new_btn" id="27"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="troch" name="troch" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->troch) ? $fieldData[0]->troch : $_SESSION['troch']; ?>" tabindex="31">
                    </div>  
                    <div class="resp_field">
                    	<label>Trochant-tibiale<a href="#" class="info_block_new_btn" id="28"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="trochTib" name="trochTib" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->trochTib) ? $fieldData[0]->trochTib : $_SESSION['trochTib']; ?>" tabindex="32">
                    </div>  
                    <div class="resp_field">
                    	<label>Tibiale laterale<a href="#" class="info_block_new_btn" id="29"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="tibLat" name="tibLat" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->tibLat) ? $fieldData[0]->tibLat : $_SESSION['tibLat']; ?>" tabindex="33">
                    </div>  
                    <div class="resp_field">
                    	<label>Tib med-sphy tib<a href="#" class="info_block_new_btn" id="30"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="tibMed" name="tibMed" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->tibMed) ? $fieldData[0]->tibMed : $_SESSION['tibMed']; ?>" tabindex="34">
                    </div>  
                </div>
                
                <div class="field_24">
                	<strong class="color_vio">Breadths (cm)</strong>
                    
                    <div class="resp_field">
                    	<label>Biacromial<a href="#" class="info_block_new_btn" id="31"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="biac" name="biac" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->biac) ? $fieldData[0]->biac : $_SESSION['biac']; ?>" tabindex="35">
                    </div>  
                    <div class="resp_field">
                    	<label>Bideltoid<a href="#" class="info_block_new_btn" id="32"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="bideltoid" name="bideltoid" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->bideltoid) ? $fieldData[0]->bideltoid : $_SESSION['bideltoid']; ?>" tabindex="36">
                    </div>  
                    <div class="resp_field">
                    	<label>Biiliocristal<a href="#" class="info_block_new_btn" id="33"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="billio" name="billio" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->billio) ? $fieldData[0]->billio : $_SESSION['billio']; ?>" tabindex="37">
                    </div>  
                    <div class="resp_field">
                    	<label>Bitrochanteric<a href="#" class="info_block_new_btn" id="34"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="bitrochanteric" name="bitrochanteric" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->bitrochanteric) ? $fieldData[0]->bitrochanteric : $_SESSION['bitrochanteric']; ?>" tabindex="38">
                    </div>  
                    <div class="resp_field">
                    	<label>Foot length<a href="#" class="info_block_new_btn" id="35"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="foot" name="foot" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->foot) ? $fieldData[0]->foot : $_SESSION['foot']; ?>" tabindex="39">
                    </div>  
                    <div class="resp_field">
                    	<label>Sitting height<a href="#" class="info_block_new_btn" id="36"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="sitting" name="sitting" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->sitting) ? $fieldData[0]->sitting : $_SESSION['sitting']; ?>" tabindex="40">
                    </div>  
                    <div class="resp_field">
                    	<label>Transverse chest<a href="#" class="info_block_new_btn" id="37"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="trChest" name="trChest" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->trChest) ? $fieldData[0]->trChest : $_SESSION['trChest']; ?>" tabindex="41">
                    </div>  
                    <div class="resp_field">
                    	<label>A-P chest<a href="#" class="info_block_new_btn" id="38"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="apChest" name="apChest" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->apChest) ? $fieldData[0]->apChest : $_SESSION['apChest']; ?>" tabindex="42">
                    </div>  
                    <div class="resp_field">
                    	<label>Arm span<a href="#" class="info_block_new_btn" id="39"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="armSpan" name="armSpan" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->armSpan) ? $fieldData[0]->armSpan : $_SESSION['armSpan']; ?>" tabindex="43">
                    </div>                                      
                    <div class="resp_field">
                    	<label>Humerus (bi-epi)<a href="#" class="info_block_new_btn" id="40"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="humerus" name="humerus" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->humerus) ? $fieldData[0]->humerus : $_SESSION['humerus']; ?>" tabindex="44">
                    </div>  
                    <div class="resp_field">
                    	<label>Femur (bi-epi)<a href="#" class="info_block_new_btn" id="41"><img src="<?php
echo "$base/assets/images/" ?>info4.png" class="info_icon"></a></label>
                        <input type="text" id="femur" name="femur" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php
echo isset($fieldData[0]->femur) ? $fieldData[0]->femur : $_SESSION['femur']; ?>" tabindex="45">
                    </div>  
                </div>
            </div>
            <!-- Block for Information Popup-->
			
            <div class="info_block_new">
                        <div class="info_block_head" style="background: #906bd0;">Full Profile</div>
                        <div class="info_content">
                            <div class="info_img">
                               
                            </div>
                            <div class="info_txt">
                                <h3 id="headings"></h3>	
                                <p id="content"></p>
                                <div class="clearfix">&nbsp;</div>
                                <h3>Method</h3>
                                <p id="method"></p>
                            </div>
                        </div>
                        <div class="info_block_foot">
                            <a href="#" class="lite_btn grey_btn f_right close_new">Close</a>
                        </div>                
                    </div>
            
            
             
             <input type="hidden" id="zscore_Triceps" name="zscore_Triceps">
            <input type="hidden" id="zscore_Subscapular" name="zscore_Subscapular">
            <input type="hidden" id="zscore_Biceps" name="zscore_Biceps">
            <input type="hidden" id="zscore_Iliac" name="zscore_Iliac">
            <input type="hidden" id="zscore_Supspinale" name="zscore_Supspinale">
            <input type="hidden" id="zscore_Abdominal" name="zscore_Abdominal">
            <input type="hidden" id="zscore_Thigh" name="zscore_Thigh">
            <input type="hidden" id="zscore_Calf" name="zscore_Calf">
            
            <input type="hidden" id="zscore_HeadG" name="zscore_HeadG">
            <input type="hidden" id="zscore_NeckG" name="zscore_NeckG">
            <input type="hidden" id="zscore_RelArmG" name="zscore_RelArmG">
            <input type="hidden" id="zscore_FlexArmG" name="zscore_FlexArmG">
            <input type="hidden" id="zscore_ForearmG" name="zscore_ForearmG">
            <input type="hidden" id="zscore_WristG" name="zscore_WristG">
            <input type="hidden" id="zscore_ChestG" name="zscore_ChestG">
            <input type="hidden" id="zscore_WaistG" name="zscore_WaistG">
            <input type="hidden" id="zscore_HipG" name="zscore_HipG">
            <input type="hidden" id="zscore_ThighG" name="zscore_ThighG">
            <input type="hidden" id="zscore_MidThighG" name="zscore_MidThighG">
            <input type="hidden" id="zscore_CalfG" name="zscore_CalfG">
            <input type="hidden" id="zscore_AnkleG" name="zscore_AnkleG">
            
            <input type="hidden" id="zscore_AcRad" name="zscore_AcRad">
            <input type="hidden" id="zscore_RadStyl" name="zscore_RadStyl">
            <input type="hidden" id="zscore_midStylDact" name="zscore_midStylDact">
            <input type="hidden" id="zscore_Iliospinale" name="zscore_Iliospinale">
            <input type="hidden" id="zscore_Troch" name="zscore_Troch">
            <input type="hidden" id="zscore_TrochTib" name="zscore_TrochTib">
            <input type="hidden" id="zscore_TibLat" name="zscore_TibLat">
            <input type="hidden" id="zscore_TibMed" name="zscore_TibMed">
            
            <input type="hidden" id="zscore_Biac" name="zscore_Biac">
            <input type="hidden" id="zscore_Bideltoid" name="zscore_Bideltoid">
            <input type="hidden" id="zscore_Billio" name="zscore_Billio">
            <input type="hidden" id="zscore_Bitrochanteric" name="zscore_Bitrochanteric">
            <input type="hidden" id="zscore_Foot" name="zscore_Foot">
            <input type="hidden" id="zscore_Sitting" name="zscore_Sitting">
            <input type="hidden" id="zscore_TrChest" name="zscore_TrChest">
            <input type="hidden" id="zscore_ApChest" name="zscore_ApChest"> 
            <input type="hidden" id="zscore_ArmSpan" name="zscore_ArmSpan"> 
            <input type="hidden" id="zscore_Humerus" name="zscore_Humerus">
            <input type="hidden" id="zscore_Femur" name="zscore_Femur">
        </div>
        
    </div>    

        <input type="hidden" id="exit_key" name="exit_key" value="">
		<input type="hidden" id="action_key" name="action_key" value="">

	<?php
echo form_close(); ?>
<!-- Form ends -->
	
</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
    
        $(window).bind("load", function() {
             
          if(document.getElementById("height").value !== "" && 
          document.getElementById("body_mass").value !== "" &&             
          document.getElementById("triceps").value !== "" &&    
          document.getElementById("subscapular").value !== "" &&    
          document.getElementById("biceps").value !== "" &&   
          document.getElementById("iliac_crest").value !== "" &&    
          document.getElementById("supraspinale").value !== "" &&    
          document.getElementById("abdominal").value !== "" &&    
          document.getElementById("thigh").value !== "" &&
          document.getElementById("calf").value !== "" &&    
          document.getElementById("mid_axilla").value !== "" &&
          document.getElementById("headG").value !== "" && 
          document.getElementById("neckG").value !== "" && 
          document.getElementById("relArmG").value !== "" &&    
          document.getElementById("flexArmG").value !== "" &&
          document.getElementById("forearmG").value !== "" &&    
          document.getElementById("wristG").value !== "" &&    
          document.getElementById("chestG").value !== "" &&
          document.getElementById("waistG").value !== "" &&
          document.getElementById("hipG").value !== "" &&
          document.getElementById("thighG").value !== "" &&   
          document.getElementById("midThighG").value !== "" &&             
          document.getElementById("calfG").value !== "" &&    
          document.getElementById("ankleG").value !== "" &&
          document.getElementById("acRad").value !== "" &&   
          document.getElementById("radStyl").value !== "" &&
          document.getElementById("midStylDact").value !== "" &&   
          document.getElementById("iliospinale").value !== "" &&
          document.getElementById("troch").value !== "" &&    
          document.getElementById("trochTib").value !== "" &&  
          document.getElementById("tibLat").value !== "" &&  
          document.getElementById("tibMed").value !== "" && 
          document.getElementById("biac").value !== "" &&              
          document.getElementById("bideltoid").value !== "" &&    
          document.getElementById("billio").value !== "" &&
          document.getElementById("bitrochanteric").value !== "" &&  
          document.getElementById("foot").value !== "" &&  
          document.getElementById("sitting").value !== "" &&    
          document.getElementById("trChest").value !== "" &&
          document.getElementById("apChest").value !== "" &&
          document.getElementById("armSpan").value !== "" &&  
          document.getElementById("humerus").value !== "" &&    
          document.getElementById("femur").value !== "")
          {      
           document.getElementById('clear_profile').style.display = "" ;
           document.getElementById('virtual_profile').style.display = "none" ;
          }          
          
         }); 
        
        $(document).on('click','#exit', function(){
           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();


        // return false;


        });	

        document.getElementById("phantom").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("body_fat").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
        
        document.getElementById("skinfold").addEventListener("click", function(event){
            event.preventDefault() ;
        }); 
	
        document.getElementById("virtual_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        document.getElementById("clear_profile").addEventListener("click", function(event){
            event.preventDefault() ;
        });
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
        $(document).on('click','#virtual_profile', function(){ 
          
          document.getElementById("height").value = 179.3 ; 
          document.getElementById("body_mass").value = 74.84 ;             
          document.getElementById("triceps").value = 5 ;    
          document.getElementById("subscapular").value = 8.6 ;    
          document.getElementById("biceps").value = 3.5 ;    
          document.getElementById("iliac_crest").value = 6.5 ;    
          document.getElementById("supraspinale").value = 3.4 ;    
          document.getElementById("abdominal").value = 4.7 ;    
          document.getElementById("thigh").value = 9.2 ;
          document.getElementById("calf").value = 5.4 ;    
          document.getElementById("mid_axilla").value = 4.5 ;
          document.getElementById("headG").value = 56 ; 
          document.getElementById("neckG").value = 37.3 ; 
          document.getElementById("relArmG").value = 33.1 ;    
          document.getElementById("flexArmG").value = 35.5 ;
          document.getElementById("forearmG").value = 28 ;    
          document.getElementById("wristG").value = 17.1 ;    
          document.getElementById("chestG").value = 93.7 ; 
          document.getElementById("waistG").value = 75.2 ;
          document.getElementById("hipG").value = 98 ;    
          document.getElementById("thighG").value = 56.4 ;    
          document.getElementById("midThighG").value = 45.2 ;              
          document.getElementById("calfG").value = 42 ;    
          document.getElementById("ankleG").value = 23.3 ;
          document.getElementById("acRad").value = 35 ;    
          document.getElementById("radStyl").value = 27.2 ;  
          document.getElementById("midStylDact").value = 19.1 ;    
          document.getElementById("iliospinale").value = 97 ;  
          document.getElementById("troch").value = 88.3 ;    
          document.getElementById("trochTib").value = 43.5 ;  
          document.getElementById("tibLat").value = 44.87 ;    
          document.getElementById("tibMed").value = 39.2 ;  
          document.getElementById("biac").value = 41.8 ;              
          document.getElementById("bideltoid").value = 44 ;    
          document.getElementById("billio").value = 29.5 ;
          document.getElementById("bitrochanteric").value = 32 ;    
          document.getElementById("foot").value = 26.4 ;  
          document.getElementById("sitting").value = 96.2 ;    
          document.getElementById("trChest").value = 29.6 ;  
          document.getElementById("apChest").value = 17.6 ;    
          document.getElementById("armSpan").value = 182 ;  
          document.getElementById("humerus").value = 7.43 ;    
          document.getElementById("femur").value = 10.97 ;
          
          document.getElementById('clear_profile').style.display = "" ;
          document.getElementById('virtual_profile').style.display = "none" ;
         
        });
        
        $(document).on('click','#clear_profile', function(){ 
          
          document.getElementById("height").value = "" ; 
          document.getElementById("body_mass").value = "" ;              
          document.getElementById("triceps").value = "" ;    
          document.getElementById("subscapular").value = "" ;   
          document.getElementById("biceps").value = "" ;     
          document.getElementById("iliac_crest").value = "" ;    
          document.getElementById("supraspinale").value = "" ;     
          document.getElementById("abdominal").value = "" ;     
          document.getElementById("thigh").value = "" ; 
          document.getElementById("calf").value = "" ;    
          document.getElementById("mid_axilla").value = "" ; 
          document.getElementById("headG").value = "" ; 
          document.getElementById("neckG").value = "" ; 
          document.getElementById("relArmG").value = "" ;   
          document.getElementById("flexArmG").value = "" ; 
          document.getElementById("forearmG").value = "" ;    
          document.getElementById("wristG").value = "" ;     
          document.getElementById("chestG").value = "" ; 
          document.getElementById("waistG").value = "" ; 
          document.getElementById("hipG").value = "" ;   
          document.getElementById("thighG").value = "" ;    
          document.getElementById("midThighG").value = "" ;              
          document.getElementById("calfG").value = "" ;  
          document.getElementById("ankleG").value = "" ; 
          document.getElementById("acRad").value = "" ;    
          document.getElementById("radStyl").value = "" ;   
          document.getElementById("midStylDact").value = "" ;     
          document.getElementById("iliospinale").value = "" ; 
          document.getElementById("troch").value = "" ;     
          document.getElementById("trochTib").value = "" ;  
          document.getElementById("tibLat").value = "" ;     
          document.getElementById("tibMed").value = "" ;  
          document.getElementById("biac").value = "" ;               
          document.getElementById("bideltoid").value = "" ;    
          document.getElementById("billio").value = "" ; 
          document.getElementById("bitrochanteric").value = "" ;     
          document.getElementById("foot").value = "" ;  
          document.getElementById("sitting").value = "" ; 
          document.getElementById("trChest").value = "" ;   
          document.getElementById("apChest").value = "" ;     
          document.getElementById("armSpan").value = "" ;   
          document.getElementById("humerus").value = "" ;     
          document.getElementById("femur").value = "" ; 
          
          document.getElementById('virtual_profile').style.display = "none" ;
          document.getElementById('clear_profile').style.display = "block" ; 
        });
        
</script>  

<script>


    
  // calculate phantom z-score value   


    function getPhantomZscore()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
         {
           alert ("Height and Body mass values should be entered");
           return false;
         }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 

            if(triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == ""
               || thigh == "" || calf == "" || headG == "" || neckG == "" || relArmG == "" || flexArmG == "" ||
               forearmG == "" || wristG == "" || chestG == "" || waistG == "" || hipG == "" || thighG == "" || 
               midThighG == "" || calfG == "" || ankleG == "" || acRad == "" || radStyl == "" || midStylDact == "" 
               || iliospinale == "" || troch == "" || trochTib == "" || tibLat == "" || tibMed == "" || biac == "" || 
               bideltoid == "" || billio == "" || bitrochanteric == "" || foot == "" || sitting == "" || trChest == ""
               || apChest == "" || armSpan == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 1 ;
                document.forms["myform"].submit();
           }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 1 ;
                document.forms["myform"].submit();
          }
        }
    }


 
 
   // calculate Body Fat value


    function getBodyFat()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
         {
           alert ("Height and Body mass values should be entered");
           return false;
         }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ;       

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 


          if(triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == ""
               || thigh == "" || calf == "" || headG == "" || neckG == "" || relArmG == "" || flexArmG == "" ||
               forearmG == "" || wristG == "" || chestG == "" || waistG == "" || hipG == "" || thighG == "" || 
               midThighG == "" || calfG == "" || ankleG == "" || acRad == "" || radStyl == "" || midStylDact == "" 
               || iliospinale == "" || troch == "" || trochTib == "" || tibLat == "" || tibMed == "" || biac == "" || 
               bideltoid == "" || billio == "" || bitrochanteric == "" || foot == "" || sitting == "" || trChest == ""
               || apChest == "" || armSpan == "" || humerus == "" || femur == "")
           {
            alert ("Note: not all measurements have been entered. Only the measurements that have been entered can be used in subsequent calculations.");    
             
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 2 ;
                document.forms["myform"].submit();
           }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 2 ;
                document.forms["myform"].submit();
          }
        }
    }


    
 
 // calculate skinfolds value


    function getSkinfolds()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ; 


          if (triceps == "" || subscapular == "" || biceps == "" || iliac == "" || supraspinale == "" || abdominal == "" || thigh == "" || calf == "" || mid_axilla == "")
          {
             alert ("All Skinfolds values should be entered");          
          }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 3 ;
                document.forms["myform"].submit();
          }        
        }
    }


    
    
 // calculate somatotype value


    function getSomatotype()
    { 
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || subscapular == "" || supraspinale == "" || calf == "" || flexArmG == "" || calfG == "" || humerus == "" || femur == "")
          {
            alert ("Some values are missing");          
          }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 4 ;
                document.forms["myform"].submit();
          }        
        }
    }    


    
 
// calculate anthropometry value


    function getAnthropometry()
    {  
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || biceps == ""  || subscapular == "" || thigh == "" || relArmG == "" || waistG == "" || hipG == "" || thighG == "")
          {
            alert ("Some values are missing");          
          }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 5 ;
                document.forms["myform"].submit();
          }        
        }
    } 




 
// calculate fractionation value


    function getFractionation()
    {  
      var ht = document.getElementById("height").value ;   
      var mass = document.getElementById("body_mass").value ;
  
        if (ht == "" || mass == "")
        {
           alert ("Height and Body mass values should be entered");
           return false;
        }
            else
        {  
            var triceps = document.getElementById("triceps").value ;            
            var subscapular = document.getElementById("subscapular").value ;
            var biceps = document.getElementById("biceps").value ;  
            var iliac = document.getElementById("iliac_crest").value ;  
            var supraspinale = document.getElementById("supraspinale").value ;  
            var abdominal = document.getElementById("abdominal").value ;  
            var thigh = document.getElementById("thigh").value ;  
            var calf = document.getElementById("calf").value ; 
            var mid_axilla = document.getElementById("mid_axilla").value ; 

            var headG = document.getElementById("headG").value ; 
            var neckG = document.getElementById("neckG").value ;
            var relArmG = document.getElementById("relArmG").value ; 
            var flexArmG = document.getElementById("flexArmG").value ; 
            var forearmG = document.getElementById("forearmG").value ;
            var wristG = document.getElementById("wristG").value ;
            var chestG = document.getElementById("chestG").value ;
            var waistG = document.getElementById("waistG").value ; 
            var hipG = document.getElementById("hipG").value ;             
            var thighG = document.getElementById("thighG").value ;
            var midThighG = document.getElementById("midThighG").value ;
            var calfG = document.getElementById("calfG").value ; 
            var ankleG = document.getElementById("ankleG").value ;
            
            var acRad = document.getElementById("acRad").value ; 
            var radStyl = document.getElementById("radStyl").value ;
            var midStylDact = document.getElementById("midStylDact").value ; 
            var iliospinale = document.getElementById("iliospinale").value ; 
            var troch = document.getElementById("troch").value ;
            var trochTib = document.getElementById("trochTib").value ;
            var tibLat = document.getElementById("tibLat").value ;
            var tibMed = document.getElementById("tibMed").value ; 
                        
            var biac = document.getElementById("biac").value ;             
            var bideltoid = document.getElementById("bideltoid").value ;
            var billio = document.getElementById("billio").value ;
            var bitrochanteric = document.getElementById("bitrochanteric").value ; 
            var foot = document.getElementById("foot").value ;
            var sitting = document.getElementById("sitting").value ;
            var trChest = document.getElementById("trChest").value ;
            var apChest = document.getElementById("apChest").value ;
            var armSpan = document.getElementById("armSpan").value ;            
            var humerus = document.getElementById("humerus").value ; 
            var femur = document.getElementById("femur").value ;  


          if (triceps == "" || subscapular == "" || thigh == "" || calf == "" || relArmG == "" || chestG == "" || thighG == "" || calfG == "")
          {
            alert ("Some values are missing");          
          }  
              else
          { 
               phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur);                             

                document.getElementById("exit_key").value = 0 ;
                document.getElementById("action_key").value = 6 ;
                document.forms["myform"].submit();
          }        
        }
    } 

 
      function phantomScores(ht,mass,triceps,subscapular,biceps,iliac,supraspinale,abdominal,thigh,calf,headG,neckG,relArmG,flexArmG,
               forearmG,wristG,chestG,waistG,hipG,thighG, 
               midThighG,calfG,ankleG,acRad,radStyl,midStylDact,
               iliospinale,troch,trochTib,tibLat,tibMed,biac, 
               bideltoid,billio,bitrochanteric,foot,sitting,trChest,
               apChest,armSpan,humerus,femur)
     {
              ht = Math.round(parseFloat(ht) * 10) / 10 ;  //upto 2 decimal places  
              mass = Math.round(parseFloat(mass) * 100) / 100 ;  //upto 2 decimal places

              triceps = Math.round(parseFloat(triceps) * 10) / 10 ;
              subscapular = Math.round(parseFloat(subscapular) * 10) / 10 ;
              biceps = Math.round(parseFloat(biceps) * 10) / 10 ;
              iliac = Math.round(parseFloat(iliac) * 10) / 10 ;
              supraspinale = Math.round(parseFloat(supraspinale) * 10) / 10 ;
              abdominal = Math.round(parseFloat(abdominal) * 10) / 10 ;
              thigh = Math.round(parseFloat(thigh) * 10) / 10 ;
              calf = Math.round(parseFloat(calf) * 10) / 10 ;
              
              headG = Math.round(parseFloat(headG) * 10) / 10 ;
              neckG = Math.round(parseFloat(neckG) * 10) / 10 ;
              relArmG = Math.round(parseFloat(relArmG) * 10) / 10 ;
              flexArmG = Math.round(parseFloat(flexArmG) * 10) / 10 ;
              forearmG = Math.round(parseFloat(forearmG) * 10) / 10 ;
              wristG = Math.round(parseFloat(wristG) * 10) / 10 ;
              chestG = Math.round(parseFloat(chestG) * 10) / 10 ;
              waistG = Math.round(parseFloat(waistG) * 10) / 10 ;
              hipG = Math.round(parseFloat(hipG) * 10) / 10 ;
              thighG = Math.round(parseFloat(thighG) * 10) / 10 ;
              midThighG = Math.round(parseFloat(midThighG) * 10) / 10 ;
              calfG = Math.round(parseFloat(calfG) * 10) / 10 ;
              ankleG = Math.round(parseFloat(ankleG) * 10) / 10 ;              
              
              acRad = Math.round(parseFloat(acRad) * 10) / 10 ;
              radStyl = Math.round(parseFloat(radStyl) * 10) / 10 ;
              midStylDact = Math.round(parseFloat(midStylDact) * 10) / 10 ;
              iliospinale = Math.round(parseFloat(iliospinale) * 10) / 10 ;
              troch = Math.round(parseFloat(troch) * 10) / 10 ;
              trochTib = Math.round(parseFloat(trochTib) * 10) / 10 ;
              tibLat = Math.round(parseFloat(tibLat) * 10) / 10 ;
              tibMed = Math.round(parseFloat(tibMed) * 10) / 10 ;
              
              
              biac = Math.round(parseFloat(biac) * 10) / 10 ;
              bideltoid = Math.round(parseFloat(bideltoid) * 10) / 10 ;
              billio = Math.round(parseFloat(billio) * 10) / 10 ;
              bitrochanteric = Math.round(parseFloat(bitrochanteric) * 10) / 10 ;
              foot = Math.round(parseFloat(foot) * 10) / 10 ;
              sitting = Math.round(parseFloat(sitting) * 10) / 10 ;
              trChest = Math.round(parseFloat(trChest) * 10) / 10 ;
              apChest = Math.round(parseFloat(apChest) * 10) / 10 ;
              armSpan = Math.round(parseFloat(armSpan) * 10) / 10 ;
              humerus = Math.round(parseFloat(humerus) * 100) / 100 ; //upto 2 decimal places
              femur = Math.round(parseFloat(femur) * 100) / 100 ; //upto 2 decimal places




          // Skinfolds Phantom Zscores    


              var zscore_Triceps = Math.round([(parseFloat(triceps) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
              var zscore_Subscapular = Math.round([(parseFloat(subscapular) * (170.18 / parseFloat(ht)) - 17.2) / 5.07] * 10) / 10 ;
              var zscore_Biceps= Math.round([(parseFloat(biceps) * (170.18 / parseFloat(ht)) - 8) / 2] * 10) / 10 ;
              var zscore_Iliac = Math.round([(parseFloat(iliac) * (170.18 / parseFloat(ht)) - 22.4) / 6.8] * 10) / 10 ;
              var zscore_Supspinale = Math.round([(parseFloat(supraspinale) * (170.18 / parseFloat(ht)) - 15.4) / 4.47] * 10) / 10 ;
              var zscore_Abdominal = Math.round([(parseFloat(abdominal) * (170.18 / parseFloat(ht)) - 25.4) / 7.78] * 10) / 10 ;
              var zscore_Thigh = Math.round([(parseFloat(thigh) * (170.18 / parseFloat(ht)) - 27) / 8.33] * 10) / 10 ;
              var zscore_Calf = Math.round([(parseFloat(calf) * (170.18 / parseFloat(ht)) - 16) / 4.67] * 10) / 10 ;



          // Girths Phantom Zscores  


              var zscore_HeadG = Math.round([(parseFloat(headG) * (170.18 / parseFloat(ht)) - 56) / 1.44] * 10) / 10 ;
              var zscore_NeckG = Math.round([(parseFloat(neckG) * (170.18 / parseFloat(ht)) - 34.91) / 1.73] * 10) / 10 ;
              var zscore_RelArmG = Math.round([(parseFloat(relArmG) * (170.18 / parseFloat(ht)) - 26.89) / 2.33] * 10) / 10 ;
              var zscore_FlexArmG = Math.round([(parseFloat(flexArmG) * (170.18 / parseFloat(ht)) - 29.41) / 2.37] * 10) / 10 ;
              var zscore_ForearmG= Math.round([(parseFloat(forearmG) * (170.18 / parseFloat(ht)) - 25.13) / 1.41] * 10) / 10 ;
              var zscore_WristG = Math.round([(parseFloat(wristG) * (170.18 / parseFloat(ht)) - 16.35) / 0.72] * 10) / 10 ;
              var zscore_ChestG = Math.round([(parseFloat(chestG) * (170.18 / parseFloat(ht)) - 87.86) / 5.18] * 10) / 10 ;
              var zscore_WaistG = Math.round([(parseFloat(waistG) * (170.18 / parseFloat(ht)) - 71.91) / 4.45] * 10) / 10 ;
              var zscore_HipG = Math.round([(parseFloat(hipG) * (170.18 / parseFloat(ht)) - 94.67) / 5.58] * 10) / 10 ;              
              var zscore_ThighG = Math.round([(parseFloat(thighG) * (170.18 / parseFloat(ht)) - 55.82) / 4.23] * 10) / 10 ;
              var zscore_MidThighG = Math.round([(parseFloat(midThighG) * (170.18 / parseFloat(ht)) - 51.84) / 3.44] * 10) / 10 ;
              var zscore_CalfG = Math.round([(parseFloat(calfG) * (170.18 / parseFloat(ht)) - 35.25) / 2.3] * 10) / 10 ;
              var zscore_AnkleG = Math.round([(parseFloat(ankleG) * (170.18 / parseFloat(ht)) - 21.71) / 1.33] * 10) / 10 ;          



          // Lengths Phantom Zscores      


              var zscore_AcRad = Math.round([(parseFloat(acRad) * (170.18 / parseFloat(ht)) - 32.53) / 1.77] * 10) / 10 ;
              var zscore_RadStyl = Math.round([(parseFloat(radStyl) * (170.18 / parseFloat(ht)) - 24.57) / 1.37] * 10) / 10 ;
              var zscore_midStylDact = Math.round([(parseFloat(midStylDact) * (170.18 / parseFloat(ht)) - 18.85) / 0.85] * 10) / 10 ;
              var zscore_Iliospinale = Math.round([(parseFloat(iliospinale) * (170.18 / parseFloat(ht)) - 94.11) / 4.71] * 10) / 10 ;
              var zscore_Troch = Math.round([(parseFloat(troch) * (170.18 / parseFloat(ht)) - 86.4) / 4.32] * 10) / 10 ;
              var zscore_TrochTib = Math.round([(parseFloat(trochTib) * (170.18 / parseFloat(ht)) - 41.37) / 2.48] * 10) / 10 ;
              var zscore_TibLat = Math.round([(parseFloat(tibLat) * (170.18 / parseFloat(ht)) - 44.82) / 2.56] * 10) / 10 ;
              var zscore_TibMed = Math.round([(parseFloat(tibMed) * (170.18 / parseFloat(ht)) - 36.81) / 2.1] * 10) / 10 ;


              
          // Breadths Phantom Zscores              


              var zscore_Biac = Math.round([(parseFloat(biac) * (170.18 / parseFloat(ht)) - 38.04) / 1.92] * 10) / 10 ;
              var zscore_Bideltoid = Math.round([(parseFloat(bideltoid) * (170.18 / parseFloat(ht)) - 43.5) / 2.40] * 10) / 10 ;
              var zscore_Billio = Math.round([(parseFloat(billio) * (170.18 / parseFloat(ht)) - 28.84) / 1.75] * 10) / 10 ;
              var zscore_Bitrochanteric = Math.round([(parseFloat(bitrochanteric) * (170.18 / parseFloat(ht)) - 32.66) / 1.8] * 10) / 10 ;
              var zscore_Foot = Math.round([(parseFloat(foot) * (170.18 / parseFloat(ht)) - 25.5) / 1.16] * 10) / 10 ;
              var zscore_Sitting = Math.round([(parseFloat(sitting) * (170.18 / parseFloat(ht)) - 89.92) / 4.5] * 10) / 10 ;
              var zscore_TrChest = Math.round([(parseFloat(trChest) * (170.18 / parseFloat(ht)) - 27.92) / 1.74] * 10) / 10 ;
              var zscore_ApChest = Math.round([(parseFloat(apChest) * (170.18 / parseFloat(ht)) - 17.5) / 1.38] * 10) / 10 ;
              var zscore_ArmSpan = Math.round([(parseFloat(armSpan) * (170.18 / parseFloat(ht)) - 172.35) / 7.41] * 10) / 10 ;
              var zscore_Humerus = Math.round([(parseFloat(humerus) * (170.18 / parseFloat(ht)) - 6.48) / 0.35] * 10) / 10 ;
              var zscore_Femur = Math.round([(parseFloat(femur) * (170.18 / parseFloat(ht)) - 9.52) / 0.48] * 10) / 10 ;	  


              document.getElementById("zscore_Triceps").value = parseFloat(zscore_Triceps) ;
              document.getElementById("zscore_Subscapular").value = parseFloat(zscore_Subscapular) ;
              document.getElementById("zscore_Biceps").value = parseFloat(zscore_Biceps) ;
              document.getElementById("zscore_Iliac").value = parseFloat(zscore_Iliac) ;
              document.getElementById("zscore_Supspinale").value = parseFloat(zscore_Supspinale) ;
              document.getElementById("zscore_Abdominal").value = parseFloat(zscore_Abdominal) ;
              document.getElementById("zscore_Thigh").value = parseFloat(zscore_Thigh) ;
              document.getElementById("zscore_Calf").value = parseFloat(zscore_Calf) ;
              
              document.getElementById("zscore_HeadG").value = parseFloat(zscore_HeadG) ;
              document.getElementById("zscore_NeckG").value = parseFloat(zscore_NeckG) ;
              document.getElementById("zscore_RelArmG").value = parseFloat(zscore_RelArmG) ;
              document.getElementById("zscore_FlexArmG").value = parseFloat(zscore_FlexArmG) ;
              document.getElementById("zscore_ForearmG").value = parseFloat(zscore_ForearmG) ;
              document.getElementById("zscore_WristG").value = parseFloat(zscore_WristG) ;
              document.getElementById("zscore_ChestG").value = parseFloat(zscore_ChestG) ;
              document.getElementById("zscore_WaistG").value = parseFloat(zscore_WaistG) ;
              document.getElementById("zscore_HipG").value = parseFloat(zscore_HipG) ;            
              document.getElementById("zscore_ThighG").value = parseFloat(zscore_ThighG) ;
              document.getElementById("zscore_MidThighG").value = parseFloat(zscore_MidThighG) ;
              document.getElementById("zscore_CalfG").value = parseFloat(zscore_CalfG) ;
              document.getElementById("zscore_AnkleG").value = parseFloat(zscore_AnkleG) ;      
              
              document.getElementById("zscore_AcRad").value = parseFloat(zscore_AcRad) ;
              document.getElementById("zscore_RadStyl").value = parseFloat(zscore_RadStyl) ;
              document.getElementById("zscore_midStylDact").value = parseFloat(zscore_midStylDact) ;
              document.getElementById("zscore_Iliospinale").value = parseFloat(zscore_Iliospinale) ;
              document.getElementById("zscore_Troch").value = parseFloat(zscore_Troch) ;
              document.getElementById("zscore_TrochTib").value = parseFloat(zscore_TrochTib) ;
              document.getElementById("zscore_TibLat").value = parseFloat(zscore_TibLat) ;
              document.getElementById("zscore_TibMed").value = parseFloat(zscore_TibMed) ;
              
              document.getElementById("zscore_Biac").value = parseFloat(zscore_Biac) ;
              document.getElementById("zscore_Bideltoid").value = parseFloat(zscore_Bideltoid) ;
              document.getElementById("zscore_Billio").value = parseFloat(zscore_Billio) ;
              document.getElementById("zscore_Bitrochanteric").value = parseFloat(zscore_Bitrochanteric) ;
              document.getElementById("zscore_Foot").value = parseFloat(zscore_Foot) ;
              document.getElementById("zscore_Sitting").value = parseFloat(zscore_Sitting) ;
              document.getElementById("zscore_TrChest").value = parseFloat(zscore_TrChest) ;
              document.getElementById("zscore_ApChest").value = parseFloat(zscore_ApChest) ;
              document.getElementById("zscore_ArmSpan").value = parseFloat(zscore_ArmSpan) ;              
              document.getElementById("zscore_Humerus").value = parseFloat(zscore_Humerus) ;
              document.getElementById("zscore_Femur").value = parseFloat(zscore_Femur) ;                             

              /*document.getElementById("exit_key").value = 0 ;
              document.forms["myform"].submit();*/
     }


	 
	 
	 
	 
// Kritika 
// 10 october 


    $(document).on('click','.virtual_btnfullprofile', function(){
    $("#vpfullprofilegen").attr("action", "<?php
echo base_url(); ?>index.php/Body/BCVirtualpersonGeneration");     
        $("#vpfullprofilegen").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
       $(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		
		window.location.href = "<?php
echo site_url('welcome/destroy_VP'); ?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 
    
</script>
   <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script> 
</body>
</html>