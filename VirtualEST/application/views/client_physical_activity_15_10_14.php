<!--

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Physical Information</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">

</head>
<body>

<div id="container"  style=" float:left ;width:90%">
<?
$hidden = array('id' => $id);

  
 echo form_open('welcome/saveClientPhysicalActivityInfo','', $hidden); ?>

<? echo "In the last week, how many times have u walked continously for atleat 10 minutes , for recreation , exercise or to go and from places ?"; ?>:
<? echo form_input('options_1'); ?>
</br>

<? echo "Estimate the total time that you spent walking in this way in the last week ?"; ?>:
<? echo form_input('options_2'); ?>min
</br>

<? echo "In the last week, how many times did you do any vigorous physical activity which made you breathe harder or puff and pant?(Excluding household chores and gardening)"; ?>:
<? echo form_input('options_3'); ?>
</br>

<? echo "Estimate the total time that you spent doing vigorous physical activity in the last week?"; ?>:
<? echo form_input('options_4'); ?>min
</br>


<? echo "In the last week, how many times did you do any other more moderate physical activities that you have not already  mentioned"; ?>:
<? echo form_input('options_5'); ?>
</br>

<? echo "Estimate the total time that you spent doing these moderate activities in the last week?"; ?>:
<? echo form_input('options_6'); ?>min
</br>

SEDENTARY ACTIVITY
</br>


<? echo "On average how many hours per day do you spend sitting , watching TV, videos, DVDs or playing video or computer games or surfing the internet for plaesure ?"; ?>:
<? echo form_input('options_7'); ?>hr
</br>

<? echo "On average how many hours per day do you spend driving or being driven?"; ?>:
<? echo form_input('options_8'); ?>hr
</br>

<? echo "Does your job require sitting for long periods ?"; ?>:
<? echo form_radio(array("name"=>"options_9","id"=>"no","value"=>"no" )); ?>:NO
<? echo form_radio(array("name"=>"options_9","id"=>"yes","value"=>"yes" )); ?>:YES
</br>


<? echo form_submit('mysubmit','Submit!');  ?>
<? echo form_close(); ?>



</div>
    <div style="float:left ; width:9%">
        <?php echo $menu; ?>
        
    </div>
</body>
</html>
-->

<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Physical Activity</title>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<style>
.fixed {
	position: fixed; 
	top: 0; 
	height: 70px; 
	z-index: 1;
}
</style>
<script>
    
    
    
var CUSTOM_ALERT = function(){//Namespace pattern
 var alertBox = null, titleEl = null, messageEl = null;
 return {
  hide : function(){
   alertBox.style.visibility = 'hidden';
  },
  alert : function(aTitle, aMessage){
   if(!alertBox) alertBox = document.getElementById("CustomAlert");
   //if(!titleEl) titleEl = document.getElementById("CustomAlertTitle");
   if(!messageEl) messageEl = document.getElementById("CustomAlertMessage");
   //titleEl.innerHTML = aTitle;
   messageEl.innerHTML = aMessage;
   thisText = aMessage.length;
   //if (aTitle.length > aMessage.length){ thisText = aTitle.length;}
   aWidth = Math.min(Math.max(thisText*5+80, 150), 350);
   aHeight = (thisText>610) ? 290 : (thisText>550) ? 270 : (thisText > 490) ? 250 : (thisText > 420) ? 230 : (thisText > 360) ? 210 : (thisText > 300) ? 190 : (thisText > 240) ? 170 : (thisText > 180) ? 150 : (thisText > 120) ? 130 : (thisText > 60)? 120 : 100;
   alertBox.style.width = aWidth + 'px';
   alertBox.style.height = aHeight + 'px';
   var left = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
   var top = window.innerHeight || document.documentElement.clientHeight || document.body.clientHeight;
   var hScroll = window.pageXOffset || document.body.scrollLeft || document.documentElement.scrollLeft || 0;
   var vScroll = window.pageYOffset || document.body.scrollTop || document.documentElement.scrollTop || 0;
   alertBox.style.left = hScroll + (left - aWidth)/2 + 'px';
   alertBox.style.top =  vScroll + (top - aHeight)/2 + 'px';
   alertBox.style.visibility = 'visible';
  }
 };
}();

CUSTOM_ALERT.alert('Your Alert Message Goes Here<br>hello');
</script>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
<script type="text/javascript">

    
var specialKeys = new Array();
        specialKeys.push(8,37, 38, 39, 40); //Backspace
        function IsNumeric(e) {
            
            var keyCode = e.which ? e.which : e.keyCode
           
            var ret = ((keyCode >= 48 && keyCode <= 57) || specialKeys.indexOf(keyCode) != -1 );
        if(!ret)  { 
        CUSTOM_ALERT.alert('','Number Only');
        }
            return ret;
        }

  $(function() {
	
	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
	  // console.log(navHeight);
			 if ($(window).scrollTop() > navHeight) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
			// console.log($('.left').hasClass('fixed');
		});
                
                
          $('.one-decimal').keyup(function(){
        if($(this).val().indexOf('.')!=-1){         
            if($(this).val().split(".")[1].length > 1){                
                if( isNaN( parseFloat( this.value ) ) ) return;
                this.value = parseFloat(this.value).toFixed(1);
            }  
         }            
         return this; //for chaining
      });        
	
}); 




</script>
</head>

<body>
<!--Start Wrapper --> 
<div class="wrapper">
    <div id="CustomAlert">
 <div id="CustomAlertMessage"></div>
 <input type="button" value="OK" onClick="CUSTOM_ALERT.hide();" id="CustomAlertSampleokButton">
</div>

  <div class="logo" id="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="930" height="56" alt="Adult Pre-exercise Screening System Logo"></div>



<!--Start login --> 
<div class="login-cont">
	<!--<form action="<?php echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
       <?php echo  form_open('welcome/saveClientPhysicalActivityInfo'); ?> 
    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php echo $this->session->userdata('user_first_name') ;?>" disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required  value="<?php echo $this->session->userdata('user_last_name') ;?>" disabled="disabled"><input name="submitPhysicalActivity" type="submit" value="" title="edit client details" /></span>
     </div>
<!--	</form> -->
</div><!--End login --> 




 <!--Start Mid --> 
  <div class="mid-phsi">
 
   
<!--Start contain --> 
<div class="contain">
   <?php //echo  form_open('welcome/saveClientPhysicalActivityInfo'); ?> 
   <!--Start left --> 
   <div class="left">
   		   		<div class="btn">
        <?php echo form_submit('mysubmit1','',"class='client_submit_form1' , 'id' = 'myform1'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit2','',"class='client_submit_form22' , 'id' = 'myform2'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
        </div>
        
        <div class="btn">
        <?php echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
        </div>
        
   
   </div><!--End Left --> 
 
 
  <?php //print_r($fieldData); ?>
   <!--Start right --> 
   <div class="right">
   		<div class="right-head">Physical Activity</div>
   		<div class="right-section">
     <?php // $hidden = array('userid' => $id);
            $option_1 = $fieldData[0]->option_1==''?'':$fieldData[0]->option_1;
            
                  
                     $attrib = array(
                        'name'        => 'options_1',
                        'id'          => 'options_1',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_1,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
                         'type'=>'number',
                         'min'=>'0'
                         
                      );
      ?>
      <span class="phy"><ol class="gen"><li>In the last week how many times have you walked continuously for at least 10 minutes, for recreation, exercise or to get to and from places?</li></ol><?php echo form_input($attrib); ?></span>
       <?php 
       $option_2 = $fieldData[0]->option_2==''?'':$fieldData[0]->option_2;
                     $attrib = array(
                        'name'        => 'options_2',
                        'id'          => 'options_2',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_2,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>
      
      <span class="phy"><ol class="gen" start="2"><li>Estimate the total time that you  spent walking in this way in the last week?</li></ol><?php echo form_input($attrib); ?><div class="value-physi">min</div></span>
        <?php 
        $option_3 = $fieldData[0]->option_3==''?'':$fieldData[0]->option_3;
                     $attrib = array(
                        'name'        => 'options_3',
                        'id'          => 'options_3',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_3,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
		         'type'=>'number',
                         'min'=>'0'
                      );
       ?>
      <span class="phy"><ol class="gen" start="3"><li>In the last week, how many times did you do any vigorous physical activity which made you breathe harder or puff and pant? (excluding household chores and gardening)</li></ol><?php echo form_input($attrib); ?></span>
       <?php 
        $option_4 = $fieldData[0]->option_4==''?'':$fieldData[0]->option_4;
                     $attrib = array(
                        'name'        => 'options_4',
                        'id'          => 'options_4',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_4,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>
      <span class="phy"><ol class="gen" start="4"><li>Estimate the total time that you  spent doing this vigorous physical activity in the last week?</li></ol><?php echo form_input($attrib); ?><div class="value-physi">min</div></span>
      <?php 
       $option_5 = $fieldData[0]->option_5==''?'':$fieldData[0]->option_5;
                     $attrib = array(
                        'name'        => 'options_5',
                        'id'          => 'options_5',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_5,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>
       <span class="phy"><ol class="gen" start="5"><li>In the last week, how many times did you do any other more moderate physical activities that you have not already mentioned?</li></ol><?php echo form_input($attrib); ?></span>
        <?php 
        $option_6 = $fieldData[0]->option_6==''?'':$fieldData[0]->option_6;
                     $attrib = array(
                        'name'        => 'options_6',
                        'id'          => 'options_6',
                        'class'       => 'physi-text',
                         'size'=>'20',
                         'value'=>$option_6,
                         'onkeypress'=>'return IsNumeric(event)',
                         'onpaste'=>'return false',
                         'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0'
                      );
       ?>
       <span class="phy"><ol class="gen" start="6"><li>Estimate the total time that you spent doing these moderate activities in the last week?</li></ol><?php echo form_input($attrib); ?><div class="value-physi">min</div></span>
       
       <span>
       <p><strong>SEDENTARY ACTIVITY</strong></p>
       </span>
       <?php 
       $option_7 = $fieldData[0]->option_7==''?'':$fieldData[0]->option_7;
                     $attrib = array(
                        'name'        => 'options_7',
                        'id'          => 'options_7',
                        'class'       => 'physi-text  one-decimal ',
                         'size'=>'20',
                         'value'=>$option_7,
                         //'onkeypress'=>'return IsNumeric(event)',
                        // 'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
			 'type'=>'number',
                         'min'=>'0',
                          'step'=>'any'
                      );
       ?>
       <span class="phy"><ol class="gen" start="7"><li>On average how many hours per day do you spend sitting watching TV, videos, DVDs or playing video or computer games  or surfing the internet for pleasure?</li></ol><?php echo form_input($attrib); ?><div class="value-physi">hr</div></span>
       <?php 
        $option_8 = $fieldData[0]->option_8==''?'':$fieldData[0]->option_8;
                     $attrib = array(
                        'name'        => 'options_8',
                        'id'          => 'options_8',
                        'class'       => 'physi-text  one-decimal ',
                         'size'=>'20',
                         'value'=>$option_8,
                        // 'onkeypress'=>'return IsNumeric(event)',
                       //  'onpaste'=>'return false',
                        // 'ondrop'=>'return false',
			'type'=>'number',
                         'min'=>'0',
                          'step'=>'any'
                      );
       ?>
       <span class="phy"><ol class="gen" start="8"><li>On average how many hours per day do you spend driving or being driven?</li></ol><?php echo form_input($attrib); ?><div class="value-physi">hr</div></span>
       
       <span class="phy"><ol class="gen" start="9"><li>Does your job require sitting for long periods?</li></ol>
           <?php  $radio_is_checked = ($fieldData[0]->option_9 === 'N')?"checked":"Y";
           echo form_radio(array("name"=>"options_9","id"=>"options_9","value"=>"N" ,'class'=>'gender',"checked"=>$radio_is_checked )); ?>No&nbsp;&nbsp;
               <?php $radio_is_checked = ($radio_is_checked === 'Y')?"checked":"";
               echo form_radio(array("name"=>"options_9","id"=>"options_9","value"=>"Y",'class'=>'gender',"checked"=>$radio_is_checked  )); ?>Yes</span>
       

       
      <!--
       <input type="submit" name="mysubmit" value="Submit!" class="client_submit"></form>
       -->
       
      <?php echo form_close(); ?>

      </div><!--End right section--> 
  
   </div><!--End right -->
  
</div><!--End contain -->

<!--
<div class="footer footer-top">&copy; Copyrights Reserved by Health Screen Pro</div>-->


</div><!--End Mid --> 
</div><!--End Wrapper --> 

</body>
</html>
