
<!doctype html>
<?php

if (count($randomdata) > 0)
	{
	$firstName = $randomdata['firstname'];
	$lastName = $randomdata['lastname'];
	if ($randomdata['gender'] == 'Male')
		{
		$gender = 'M';
		}
	  else
		{
		$gender = 'F';
		}

	$daydropdown = $randomdata['daydropdown'];
	$monthdropdown = $randomdata['monthdropdown'];
	$yeardropdown = $randomdata['yeardropdown'];
	$occupation = $randomdata['occupation'];
	$country = $randomdata['country']['countrykey'];
	$subpopulation = $randomdata['sub_population'];
	$age_category = $randomdata['age_category'];
	$_SESSION['HEIGHT'] = $randomdata['HEIGHT']; // added
	$_SESSION['MASS'] = $randomdata['MASS']; // added
	$_SESSION['BMI'] = $randomdata['BMI']; // added
	$_SESSION['vp_age'] = $randomdata['vp_age'];
	}
  else
	{
	$firstName = $_SESSION['user_first_name'];
	$lastName = $_SESSION['user_last_name'];
	$gender = $_SESSION['user_gender'];
	$daydropdown = $_SESSION['user_daydropdown'];
	$monthdropdown = $_SESSION['user_monthdropdown'];
	$yeardropdown = $_SESSION['user_yeardropdown'];
	$occupation = $_SESSION['user_occupation'];
	$country = $_SESSION['user_country'];
	$subpopulation = $_SESSION['subpopulation'];
	$age_category = $_SESSION['age_category'];
	}

if (isset($_SESSION['is_virtual']))
	{
	$new = 0;
	}
  else
	{
	$new = 1;
	}

// To show Hide Additional Info

if (isset($_SESSION['risk_factor']))
	{
	$riskdiv = "block";
	}
  else
	{
	$riskdiv = "none";
	}

if (isset($_SESSION['VO2max']))
	{
	$vo2maxdiv = "block";
	}
  else
	{
	$vo2maxdiv = "none";
	}

if (isset($_SESSION['bodyfat']))
	{
	$bodyfatdiv = "block";
	}
  else
	{
	$bodyfatdiv = "none";
	}

?>


<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta charset="utf-8">
<title>Client Details</title>
<link href='<?php
echo "$base/$css"; ?>' rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="<?php
echo "$base/assets/css/" ?>style.css">
<link rel="stylesheet" type="text/css" href="http://fitnessriskmanagement.com.au/screening-tool/assets/css/jquery.datetimepicker.css"/>
<script type="text/javascript" src="<?php
echo base_url() . 'assets/'; ?>js/jquery.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<!-- todo including jquerysession -->
<!--<script type="text/javascript" src="<?php
echo base_url() . 'assets/'; ?>js/jquerysession.js"></script>-->

<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script>
  <script type="text/javascript">
	$(document).ready(function() {
		
		var isnew='<?php
echo $new; ?>';
        if(isnew==0)
        {
		 $("#subpopulationid").removeClass("cus-input");
		 $("#agerangeid").removeClass("cus-input");
		 $(".v_person").fadeTo( "slow" , 1, function() {});
		$(".v_detail").toggle();
		
		} 
		
		
        /* var age='<?php
echo $_SESSION['vp_age']; ?>';
        if(age > 0)
        {
        $(".v_person").fadeTo( "slow" , 1, function() {});
		$(".v_detail").toggle();
        }    */
    });
    
    $(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php
echo site_url('welcome/destroy_VP'); ?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	});
    </script> 

<script>
  $(function(){ 
     gender = '<?php
$gender; ?>'; 
       genderTest = gender.value            
      if( genderTest == 'M' ){   
       $("#male").prop('checked', true);
      }      
    });
	
	$(document).on("click",".btn_generate", function(){

		// obtain 'do not ask me again' choice from previous VP gen
		var stopmodel_flag = sessionStorage.getItem("_mc_stopmodel");

		// check if anything has been entered into the fields on VP gen page
		// if so, set flag to allow the caution overwrite question. If form
		// is blank, the caution question will not be prompted.

		var changes = false;

		// check if text areas are empty

		if (!($("#firstname").val().trim() == "")) {
			changes = true;
		}

		if (!($("#lastname").val().trim() == "")) {
			changes = true;
		}

		if (!($("#emailid").val().trim() == "")) {
			changes = true;
		}

		if (!($("#notenew_id").val().trim() == "")) {
			changes = true;
		}

		if (!($("#accesscode_id").val().trim() == "")) {
			changes = true;
		}

		// check if drop downs are empty

		if (!($("#daydropdown").val() == "dd" || $("#daydropdown").val() == null)) {
			changes = true;
		}

		if (!($("#monthdropdown").val() == "mm" || $("#monthdropdown").val() == null)) {
			changes = true;
		}
		
		if (!($("#yeardropdown").val() == "0000" || $("#yeardropdown").val() == null)) {
			changes = true;
		}

		if (!($("#agerangeid").val() == "selectage" || $("#agerangeid").val() == null)) {
			changes = true;
		}

		if (!($("#occupationid").val() == "selectoccupation" || $("#occupationid").val() == null)) {
			changes = true;
		}

		if (!($("#subpopulationid").val() == "selectoccupation" || $("#subpopulationid").val() == null)) {
			changes = true;
		}

		if (!($("#country_id").val() == "selectcountry" || $("#country_id").val() == null)) {
			changes = true;
		}

		if (!($("#projectname_id").val() == "0" || $("#projectname_id").val() == null)) {
			changes = true;
		}

		if ($("#male")[0].checked || $("#female")[0].checked) {
			changes = true;
		}

		// if form is empty AND 'do not ask me again' hasn't been set, display the overwrite question
		if(stopmodel_flag != 'true' && changes == true) {
			// show dialog
			$('#makechanges_dialog').show();
			// of yes, generate
			$('#mc-yes').click(function() {
				var mcisChecked = $('#mc-donot:checked').val()?true:false;
				sessionStorage.setItem("_mc_stopmodel", mcisChecked);

				//passValue("<?php echo site_url('welcome/VirtualpersonGeneration'); ?>"); // TODO passes values fine but doesn't autogenerate for unknown reasons
				window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration'); ?>";
			});
			// if no, hide dialog and go back
			$('#mc-no').click(function() {
				var mcisChecked = $('#mc-donot:checked').val()?true:false;
				sessionStorage.setItem("_mc_stopmodel", mcisChecked);
				$('#makechanges_dialog').hide();
			});
		} else {
			// just generate if blank form or 'do not ask me again' is not set
			//passValue("<?php echo site_url('welcome/VirtualpersonGeneration'); ?>");
			window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration'); ?>";
		}

		// pass values via post data from this jquery to welcome.php
		function passValue(loc) {
			// TODO for testing
			console.log("location to post to: " + loc);
			//$.session.set("test_session", "testing session...");

			// get all relevant field values and pack them ready to go
				data = {
					gen: $("input[name='gender']:checked").val(),
					age: $("#age_id").val(),
					firstname: $("#firstname").val(),
					lastname: $("#lastname").val(),
					emailid: $("#emailid").val(),
					day: $("#daydropdown").val(),
					month: $("#monthdropdown").val(),
					year: $("#yeardropdown").val(),
					agerange: $("#agerangeid").children("option:selected").val(), // the select dropdown is not giving up it's treasure!
					country: $("#country").children("option:selected").val() // ditto
				}
			// Put in post and call function in controller...
			$.post(loc, data);
		}
	}); 
</script>

    
<script type="text/javascript">

/**
 * determine age in years from date of birth dropdowns then set age category appropriately
 *
 * @author Matt Palmer (JAC 2018)
 */

 // set change events on all date of birth drop down boxes
 $(document).ready(function(){
	$("#yeardropdown").change(calculateAgeCategory);
	$("#monthdropdown").change(calculateAgeCategory);	
	$("#daydropdown").change(calculateAgeCategory);
 });

 //function to calculate years between DOB and current date
 function calculateAgeCategory() {
	// obtain values from relevant drop downs
	year = $('#yeardropdown option:selected').text();
	month = $('#monthdropdown option:selected').text();
	day = $('#daydropdown option:selected').text();

	// change month to number
	switch(month) {
		case "JAN":
			month = 1;
			break;
		case "FEB":
			month = 2;
			break;
		case "MAR":
			month = 3;
			break;
		case "APR":
			month = 4;
			break;
		case "MAY":
			month = 5;
			break;
		case "JUN":
			month = 6;
			break;
		case "JUL":
			month = 7;
			break;
		case "AUG":
			month = 8;
			break;
		case "SEP":
			month = 9;
			break;
		case "OCT":
			month = 10;
			break;
		case "NOV":
			month = 11;
			break;
		case "DEC":
			month = 12;
			break;
		default:
			month = 0;
	}
	
	// check if all fields are valid
	if (year != "YYYY" && month != 0 && day != "DD") {
		// obtain date and break into day, month an year
		var date = new Date();

		var currentMonth = date.getMonth()+1;
		var currentDay = date.getDate();
		var currentYear = date.getYear()+1900;
		
		// logic to calculate year
		if(month < currentMonth) {
			yearDifference = currentYear - year;
		} else if (month > currentMonth) {
			yearDifference = currentYear - year - 1;
		} else {
			if (day <= currentDay) {
				yearDifference = currentYear - year;
			} else {
				yearDifference = currentYear - year - 1;
			}
		}

		// select correct age group in dropdown using age in years
		if (yearDifference >= 18 && yearDifference <= 29) {
			$("#agerangeid option[value=18-29]").attr('selected', 'selected');
		} else if (yearDifference >= 30 && yearDifference <= 39) {
			$("#agerangeid option[value=30-39]").attr('selected', 'selected');
		} else if (yearDifference >= 40 && yearDifference <= 49) {
			$("#agerangeid option[value=40-49]").attr('selected', 'selected');
		} else if (yearDifference >= 50 && yearDifference <= 59) {
			$("#agerangeid option[value=50-59]").attr('selected', 'selected');
		} else if (yearDifference >= 60 && yearDifference <= 69) {
			$("#agerangeid option[value=60-69]").attr('selected', 'selected');
		} else if (yearDifference >= 70 && yearDifference <= 79) {
			$("#agerangeid option[value=70-79]").attr('selected', 'selected');
		} else if (yearDifference >= 80 && yearDifference <= 84) {
			$("#agerangeid option[value=80-84]").attr('selected', 'selected');
		} else if (yearDifference > 84) {
			$("#agerangeid option[value='84+']").attr('selected', 'selected');
		}
	}
 }
  
/* TODO
 * 
 * activated whenever gender is altered
 * - overwrites name
 * - age
 * - keeps on going to female... why?
 *
 * tried remove this code a it seems to do nothing other than make the user experience of generating a VP difficult
 */
   $(document).on("click",".genderclass", function(){
        var isnew='<?php echo $new;?>';
        if(isnew==0)
        {
	   var gender=$(this).attr('value');
       //alert(gender);
       var emailid=$('#emailid').attr('value');
       var montharr=["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
       var day = $('#daydropdown').find(":selected").text();
       //alert(day);
       //var month = $('#monthdropdown').find(":selected").text();
       //month=inArray(month,montharr);
       var month=$("#monthdropdown option:selected").val();
       var mnthname=montharr[month-1];
       //alert(mnthname);
       var year= $('#yeardropdown').find(":selected").text();
       var lastname=$('#lastname').val();
       var firstname=$('#firstname').val();
       //alert(lastname);
       //alert(firstname);
       var subpopulation="";
       var dateofBirth = year+'/'+month+'/'+day;
       //alert(dateofBirth);
       var vp_age=getAge(dateofBirth);
       //alert(vp_age);  
       
       var agerange=getAgeRange(vp_age);
       //alert(agerange);
       //alert(country);
       $('#agerangeid').val(agerange);
       //window.location.href = "<?php //echo site_url('welcome/VirtualpersonGeneration?gen=');?>"+gender;    
        //window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration?gen=');?>"+gender+"&age="+vp_age+"&firstname="+firstname+"&lastname="+lastname+"&email="+emailid+"&day="+day+"&month="+mnthname+"&year="+year+"&agerange="+agerange;    
        window.location.href = "<?php echo site_url('welcome/VirtualpersonGeneration_change?gen=');?>"+gender+"&age="+vp_age;    
		}
		}); 
		
    
	
 // Sub Population     OLD

/* $(document).on("change",".subpopulationclass",function(){
       var gender=document.querySelector('input[name = "gender"]:checked').value;
       var day = $('#daydropdown').find(":selected").text();
       var month=$("#monthdropdown option:selected").val();
       var year= $('#yeardropdown').find(":selected").text();
    
       var subpop = $('#subpopulationid').find(":selected").text();
       var dateofBirth = year+'-'+month+'-'+day;
       var vp_age=getAge(dateofBirth);
    
       /*var optionValue  = localStorage.getItem("select1");
       $("#subpopulationid").val(optionValue).find("option[value=" + $_GET['subpop'] +"]").attr('selected', true);*/
    
   /*    window.location.href = "<?php
echo site_url('welcome/VirtualpersonGeneration?gen='); ?>"+gender+"&age="+vp_age+"&subpop="+subpop;    
      
}); */









 // Sub Population    

$(document).on("change",".subpopulationclass",function(){
	 var isnew='<?php
echo $new; ?>';
        if(isnew==0)
        {
       var gender=document.querySelector('input[name = "gender"]:checked').value;
       var subpop = $('#subpopulationid').find(":selected").text();

    
       // alert(subpop);


       var vp_age="";
       var age_range="";
       var dateofBirth="";
       if(subpop == 'Athlete')
           {
  		       <?php
$today_date = date('Y-m-d');
$start = strtotime("$today_date -40 year");
$end = strtotime("$today_date -18 year");
$int = mt_rand($start, $end);
$between_date = date('Y-m-d', $int);
$get_date_values = explode("-", $between_date);
$valueyears = $get_date_values[0];
$valuemonth = $get_date_values[1];
$valuedate = $get_date_values[2];
$dateofBirt = $valueyears . '/' . $valuemonth . '/' . $valuedate;
?> 
			   var dob='<?php
echo $dateofBirt; ?>';  // alert(dob);
			   var res = dob.split("/");
			   var year=res[0];
			   var month=res[1];
			   var day=res[2];
				
			    var dateofBirth = year+'/'+month+'/'+day;
                vp_age=getAge(dateofBirth); 
                age_range=getAgeRange(vp_age);  // Age range less than 40 is athlete
               
                $('#daydropdown').val(day);
                $('#monthdropdown').val(month);
                $('#yeardropdown').val(year);
                $('#agerangeid').val(age_range);
                
                $('#age').val(vp_age);
                $('#subpopdropdown').val(subpop);

				
				// $("div.field_50 select").val("Athlete/coach");

				
                
                $.ajax({
                        type: "POST",
                        url: '<?php
echo base_url(); ?>index.php/welcome/file_exist_age',
                        data:{'age': vp_age,'gender' : gender },
                        success: function(data){
                                var source='<?php
echo "$base" ?>'+data;
                                $('#imgsrc').attr("src",source);
                             }
			          });    
               
               
           }
         else
           {
               
               var day = $('#daydropdown').find(":selected").text();      //alert(day);
               var month=$("#monthdropdown option:selected").val();       //alert(month);
               var year= $('#yeardropdown').find(":selected").text();     //alert(year);
               
               dateofBirth = year+'/'+month+'/'+day;
               console.log(dateofBirth);
               vp_age=getAge(dateofBirth); // Get age

               // alert(vp_age);

               age_range=getAgeRange(vp_age);   // get Age range
               $('#age').val(vp_age);
               $('#subpopdropdown').val(subpop);
               
               
               $.ajax({
                        type: "POST",
                        url: '<?php
echo base_url(); ?>index.php/welcome/file_exist_age',
                        data:{'age': vp_age,'gender' : gender },
                        success: function(data){
                                var source='<?php
echo "$base" ?>'+data;
                                $('#imgsrc').attr("src",source);
                             }
			          }); 
               
               
           }
      
    
     window.location.href = "<?php
echo site_url('welcome/VirtualpersonGeneration_change?gen=') ?>"+gender+"&age="+vp_age+"&subpop="+subpop+"&case=third"+"&day="+day+"&month="+month+"&year="+year+"&range="+age_range;    
	}      
});









    
    
$(document).on("change",".categoryclass",function(){ 
    
     var isnew='<?php
echo $new; ?>';
        if(isnew==0)
        {
	var selval=$("#agerangeid option:selected").val();
    var catval=$("#agerangeid option:selected").text();
    
    if(selval == '84+')
	{
		var getrandval=getRandomInt(85,100);
	}
    else{
        var res = catval.split("-");
        var val1=res[0];
        var val2=res[1];
        var getrandval=getRandomInt(val1,val2);
      }
    
        var d = new Date();
		var n = d.getFullYear();
		var calcyear=n-getrandval;
		var randdate=getRandomInt(1,28);

		
		// month calc

		var month=getRandomInt(0,11);

    
        // values set

        $('#daydropdown').val(randdate);
		$('#monthdropdown').val(month+1);
		$('#yeardropdown').val(calcyear);
    
		}   
});
    
    
function getRandomInt(min, max) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min)) + min; //The maximum is exclusive and the minimum is inclusive
}   
    
    
    
    

/*    
  OLD
   $(document).on("change",".dateclass",function(){ 
   var selval=$("#monthdropdown option:selected").val();
   var month=$("#monthdropdown option:selected").text();
   var day = $('#daydropdown').find(":selected").text();  
   var year= $('#yeardropdown').find(":selected").text();  
   var dateofBirth = year+'-'+selval+'-'+day;
   var vp_age=getAge(dateofBirth);
   
   var agerange=getAgeRange(vp_age);
   $('#agerangeid').val(agerange);
}); */



/* 

OLD
$(document).on("change",".dateclass",function(){ 
    
   var gender=document.querySelector('input[name = "gender"]:checked').value;    

    
    // alert(gender);

    
   var selval=$("#monthdropdown option:selected").val();  
   var month=$("#monthdropdown option:selected").text();
   var day = $('#daydropdown').find(":selected").text();  
   var year= $('#yeardropdown').find(":selected").text();  
   var dateofBirth = year+'/'+selval+'/'+day;
   var vp_age=getAge(dateofBirth);

    
// alert(vp_age);

   var agerange=getAgeRange(vp_age);

    
    // alert(agerange);

    
   $('#agerangeid').val(agerange);
   $('#age').val(vp_age);
   var subpop = $('#subpopulationid').find(":selected").text();    

  // window.location.href = "<?php //echo site_url('welcome/VirtualpersonGeneration_change?gen=')

 ?>"+gender+"&age="+vp_age; 
    $.ajax({
			type: "POST",
			url: '<?php
echo base_url(); ?>index.php/welcome/file_exist_age',
			data:{'age': vp_age,'gender' : gender },
			success: function(data){
                    var source='<?php
echo "$base" ?>'+data;
                    $('#imgsrc').attr("src",source);
                 }
			});	
    
});  */






/* TODO temporarily remove for testing purposes
 * This (undocumented) code is causing problems with user experience, resetting VP fields every time a year is set. */
$(document).on("change",".dateclass",function(){     
   var isnew='<?php
echo $new; ?>';
        if(isnew==0)
        {
   var gender=document.querySelector('input[name = "gender"]:checked').value;    
   var selval=$("#monthdropdown option:selected").val();  
   var month=$("#monthdropdown option:selected").text();
   var day = $('#daydropdown').find(":selected").text();  
   var year= $('#yeardropdown').find(":selected").text();  
   var dateofBirth = year+'/'+selval+'/'+day;
   var vp_age=getAge(dateofBirth);    

   // alert(vp_age);

   var agerange=getAgeRange(vp_age);

    // alert(agerange);

   $('#agerangeid').val(agerange);
   $('#age').val(vp_age);
   var subpop = $('#subpopulationid').find(":selected").text(); 
    
  /*  
     $.ajax({
			type: "POST",
			url: '<?php //echo base_url();
 ?>index.php/welcome/VP_age',
			data:{'day': day,'month' : month,'year':year ,'case':"gender"},
			success: function(data){
                    $('#age').val(data);
                 }
			});	*/
  
  
    var dob="dob";
    window.location.href = "<?php
echo site_url('welcome/VirtualpersonGeneration_change?gen=') ?>"+gender+"&age="+vp_age+'&day='+day+'&month='+selval+'&year='+year+'&case='+dob; 
    $.ajax({
			type: "POST",
			url: '<?php
echo base_url(); ?>index.php/welcome/file_exist_age',
			data:{'age': vp_age,'gender' : gender },
			success: function(data){
                    var source='<?php
echo "$base" ?>'+data;
                    $('#imgsrc').attr("src",source);
                 }
			});	
		}   
});




  // GET AGE RANGE

       function getAgeRange(age)
       {
       	     var agerange="";

            // alert("age"+age) ;

             if(age >=1 && age <=17)
             {
                 agerange='18-29';
             }
               else if(age >= 18 && age <=29)
			 {
				 agerange = '18-29' ;
			 }
          	   else if(age >= 30 && age <=39)
			 {
				 agerange = '30-39' ;
			 }
			   else if(age >= 40 && age <=49)
			 {
				 agerange = '40-49' ;
			 }
			   else if(age >= 50 && age <=59)
			 {
				 agerange = '50-59' ;
			 }
			   else if(age >= 60 && age <=69)
			 {
				 agerange = '60-69' ;
			 }
			   else if(age >=70 && age <=79)
			 {
				 agerange = '70-79' ;
			 }
               else if(age >=80 && age <=84)
			 {
				 agerange = '80-84' ;
			 }
               else if(age >84)
			 {
				 agerange = '84+' ;
			 }
                               
            return agerange;
        }  
    
    
 function getAge(dateString) {
                var today = new Date();
                var birthDate = new Date(dateString);
                var age = today.getFullYear() - birthDate.getFullYear();
                var m = today.getMonth() - birthDate.getMonth();
                if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                    age--;
                }
                return age;
             }
    
function inArray(needle,haystack)
{
    var count=haystack.length;
    for(var i=1;i<=count;i++)
    {
        if(haystack[i]===needle){return i+1;}
    }
    return false;
}       
   
    
/*function reverse_birthday( $years ){
return date('Y-m-d', strtotime($years . ' years ago'));
}  */  
    
</script>    
    

    
    
<script>
     function edValueKeyPress()
    {
       var edValue = document.getElementById("firstname");
        var s = edValue.value;
        $("#edValue").text(s);    
    }
    
     function edValuelnamePress()
    {
        var lnameValue = document.getElementById("last_name");
        var s = lnameValue.value;
        $("#lnameValue").text(s);    
    }
 </script>
    
    

<style type="text/css">
    .vie_table {
    position: absolute;
    width: 250px;
    left: 50px;
    top: 100px;
    text-align: center;
}
 .vie_table td{padding: 20px;}
</style>

<script type="text/javascript">
	$(document).on('click','#info_icon_btn', function(){
		$("#info_block").show();
		$(".overlay").show();
	});
	
	$(document).on('click','#linda_info_icon_btn', function(){
		$("#linda_info_block").show();
		$(".overlay").show();
	}); 
	$(document).on('click','.close', function(){
		$(".info_block").hide();
		$(".overlay").hide();
	}); 
	
	
	$(document).on('click','.head_logo', function(){
		$(".tooltiptext").toggle();
	}); 
	 
</script> 

</head>

<body>

	<!-- insertion of header -->
	
	 <div id="makechanges_dialog" title="Overwrite?">
		<div id="makechanges-overlay">
			<div id="makechanges-content">
				<h4 class="makechanges-text">Are you sure you want to generate a new person?</h4>
				<p class="center">All previous data will be lost.</p>
				<div class="ta-center">
					<div class="makechanges-btn" id="mc-yes">
						<a>Yes</a>
					</div>
					<div class="makechanges-btn" id="mc-no">
						<a>No</a>
					</div>
				</div>
				<div class="mc-donotask">					
					</p><input type="checkbox" name="mc-donot" id="mc-donot">Do not ask me again.</p>
				</div>
			</div>			
		</div>
	 </div>

     <div class="v_person">
	 <a href="#" class="discart" title="Exit virtual person">x</a>
	 <div class="v_image">
	     
             <img src="<?php
echo "$base/$filename" ?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name </label>
		<input type="text" name="first_name" value="<?php
echo "$firstName $lastName"; ?>">
	  </div>
		<div class="field_row gen">
		<label>Age </label>
		<input id="age_id" type="text" name="age" value="<?php
echo round($_SESSION['vp_age']); ?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] </label><input type="text" name="height" value="<?php
echo round($_SESSION['HEIGHT'], 2); ?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] </label><input type="text" name="body_mass" value="<?php
echo round($_SESSION['MASS'], 2); ?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI </label><input type="text" name="BMI" value="<?php
echo round($_SESSION['BMI'], 2); ?>">                  
			</div>

			<div class="field_50" style="display:<?php
echo $bodyfatdiv; ?>">
				<label>Avg % body fat</label><input type="text" name="body_fat" value="">                  
			</div>
		</div>
	  
	  <div class="field_row" style="display: none;">
		  <label>Sub-population</label>
		  <?php
$subpop_array = array(
	'selectoccupation' => 'Select',
	'Active' => 'Active',
	'General' => 'General',
	'Sedentary' => 'Sedentary',
	'Athlete' => 'Athlete'
);
echo form_dropdown('subpopulation', $subpop_array, $subpopulation, 'id="subpopdropdown"');
?>  
		</div>
		<div class="field_row" style="display:<?php
echo $riskdiv; ?>">
			<div class="field_50">
				<label>Risk factor score</label><input type="text" name="risk_fact_sc" value="<?php
echo $_SESSION['risk_factor']; ?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group</label><input type="text" name="risk_group" value="<?php
echo $_SESSION['risk_group']; ?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php
echo $vo2maxdiv; ?>">
		<label>VO2max [mL/kg/min]</label>
		<input type="text" name="VO2max" value="<?php
echo $_SESSION['VO2max']; ?>">
	  </div>
		
	 </div>
 </div>
 
<div class="header">
	<div class="wrapper">
		<div class="head_left">
			<a href="https://www.unisa.edu.au/" target="_blank">
				<img id="unisa_logo_id" src="<?php echo base_url('/assets/images/') ."/unisa-logo.png"; ?>">
			</a>
		</div>
    	<div class="head_left">
       		<a href="javascript:void(0);" class="head_logo">Exercise Science Toolkit</a> 
       		<span class="tooltiptext">Norton KI & Norton LH (2018) Exercise Science Toolkit. Computer program, University of South Australia, Adelaide, Australia. URL: <a href="https://est-demo.jamesanthony.consulting" target="_blank">est-demo.jamesanthony.consulting</a></span>
       </div>
        <div class="head_right">
        <a onclick="window.print(); return false;"><img src="<?php
echo "$base/assets/images/" ?>print_blk.jpg" style="width: 20px; margin-top: 13px; margin-left: 30px;"></a>
        <span id="edValue"><?php
echo $firstName; ?></span> <span id="lnameValue"><?php
echo $lastName; ?></span>  <img src="<?php
echo "$base/assets/images/" ?>user_icon.png">
        
        </div>
    </div>
</div>
    <div class="vie_table">
        <!--<table border="1" cellpadding="0" cellspacing="0" width="100%">
            <tr>
                <td>Random Probability for Log BMI</td>
                <td><?php
echo $randomdata['probability_LOG_BMI']; ?></td>
            </tr>
            <tr>
                <td>Random_generated_log_BMI</td>
                <td><?php
echo $randomdata['Random_generated_log_BMI']; ?></td>
            </tr>
             <tr>
                <td>MASS</td>
                <td><?php
echo $randomdata['MASS']; ?></td>
            </tr>
             <tr>
                <td>Random Probability of BMI</td>
                <td><?php
echo $randomdata['probability_random_BMI']; ?></td>
            </tr>
            
            <tr>
                <td>Random generated BMI</td>
                <td><?php
echo $randomdata['BMI']; ?></td>
            </tr>
            <tr>
                <td>HEIGHT</td>
                <td><?php
echo $randomdata['HEIGHT']; ?></td>
            </tr>
            <tr>
                <td>m_BMI</td>
                <td><?php
echo $randomdata['m_BMI']; ?></td>
            </tr>
            <tr>
                <td>b_BMI</td>
                <td><?php
echo $randomdata['b_BMI']; ?></td>
            </tr>
            <tr>
                <td>s_BMI</td>
                <td><?php
echo $randomdata['s_BMI']; ?></td>
            </tr>
            <tr>
                <td>mean_log_Mass</td>
                <td><?php
echo $randomdata['mean_log_Mass']; ?></td>
            </tr>
            <tr>
                <td>sd_log_Mass</td>
                <td><?php
echo $randomdata['sd_log_Mass']; ?></td>
            </tr>
        </table>-->
    </div>
<!--Start Wrapper --> 
<div class="wrapper">
	<div class="welc_form">
    	 <?php
$attributes = array(
	'id' => 'clientInfo'
);
echo form_open('welcome/saveClientInfo', $attributes); ?>
       	
  	    <div class="welc_form_row">
             <h4 id="vp_top_heading">Enter your details or generate a virtual person <a href="#" class="btn_generate f_right" title="Generate new VP profile">Generate Virtual Person</a></h4>			 
            <p>Fields marked with an asterisk * are mandatory.</p>
            
              <div id="validate">
                <?php
echo validation_errors(); ?>
              </div>
                  
                  <?php
$firstName = $firstName != '' ? $firstName : '';
$lastName = $lastName === '' ? '' : $lastName;
$email = $email === '' ? '' : $email;
$p_desc = $p_desc === '' ? '' : $p_desc;
$gender = $gender === 'M' ? 'M' : $gender;
$genderM = $gender === 'M' ? 'Checked' : '';
$genderF = $gender === 'F' ? 'Checked' : '';
$dobirth = $dob === '' ? '' : $dob;
$occupation = $occupation === '' ? 'selectoccupation' : $occupation;
$country = $country === '' ? 'Select' : $country;
$p_name = $p_name === '' ? 'Select' : $p_name;
$daydropdown = $daydropdown === '' ? 'Select' : $daydropdown;
$monthdropdown = $monthdropdown === '' ? 'Select' : $monthdropdown;
$yeardropdown = $yeardropdown === '' ? 'Select' : $yeardropdown;
$subpopulation = $subpopulation === '' ? 'Select' : $subpopulation;
$age_category = $age_category === '' ? 'Select' : $age_category;
?>
                          
              <div class="field_row">
                  <div class="field_25_left light-right-margin">
                    <label>First name *</label><?php
echo form_input('first_name', $firstName, "class='' id='firstname' onKeyPress='edValueKeyPress()' onKeyUp='edValueKeyPress()' "); ?>
                  </div>
                  
                  <div class="field_25_left">
                    <label>Last name *</label><?php
echo form_input('last_name', $lastName, "class='' id='lastname'  onKeyPress='edValuelnamePress()' onKeyUp='edValuelnamePress()' "); ?>
                  </div>

				  <div class="field_50 gen">
                    <label>Gender *</label>
					
                    <?php
						echo form_radio(array(
							"name" => "gender",
							"id" => "male",
							"class" => "genderclass",
							"value" => "M",
							"checked" => $genderM
						)); ?> <label for="male"><span class="radio_circle" style="margin-right:8px;"></span>Male</label>
						
                    <?php
						echo form_radio(array(
							"name" => "gender",
							"id" => "female",
							"class" => "genderclass",
							"value" => "F",
							"checked" => $genderF
						)); ?>  <label for="female"><span class="radio_circle" style="margin-right:8px;"></span>Female</label>
                  </div>      

              </div>

              <div class="field_row">
                  <div class="field_50">
                    <label>Email</label><?php
						echo form_input('email', $email, "id='emailid'"); ?>
                  </div>                  

				  <?php
					$dob = array(
						'name' => 'datetimepicker2',
						'id' => 'datetimepicker2',
						'placeholder' => "mm/dd/yyyy",
						'value' => $dobirth,
						'type' => 'numb',
						'class' => 'webDate'
					);
					?>
                  <div class="field_50 dob">
                    <label>Date of birth *</label>                    
                    <span><?php
						$options = array(
							'dd' => 'DD',
							'1' => '1',
							'2' => '2',
							'3' => '3',
							'4' => '4',
							'5' => '5',
							'6' => '6',
							'7' => '7',
							'8' => '8',
							'9' => '9',
							'10' => '10',
							'11' => '11',
							'12' => '12',
							'13' => '13',
							'14' => '14',
							'15' => '15',
							'16' => '16',
							'17' => '17',
							'18' => '18',
							'19' => '19',
							'20' => '20',
							'21' => '21',
							'22' => '22',
							'23' => '23',
							'24' => '24',
							'25' => '25',
							'26' => '26',
							'27' => '27',
							'28' => '28',
							'29' => '29',
							'30' => '30',
							'31' => '31'
						);
						$js = 'id="daydropdown" ';
						echo form_dropdown('daydropdown', $options, $daydropdown, 'class="" id="daydropdown"', $js);
						?></span>
                    <span><?php
						$options = array(
							'mm' => 'MM',
							'1' => 'JAN',
							'2' => 'FEB',
							'3' => 'MAR',
							'4' => 'APR',
							'5' => 'MAY',
							'6' => 'JUN',
							'7' => 'JUL',
							'8' => 'AUG',
							'9' => 'SEP',
							'10' => 'OCT',
							'11' => 'NOV',
							'12' => 'DEC'
						);
						$js = 'id="monthdropdown"';
						echo form_dropdown('monthdropdown', $options, $monthdropdown, 'class="" id="monthdropdown"', $js);
?></span>            
                    <span><?php

//    $options = array('0' => 'YYYY','1910'=>'1910','1910'=>"1910", '1911'=>"1911", '1912'=>"1912", '1913'=>"1913", '1914'=>"1914", '1915'=>"1915", '1916'=>"1916", '1917'=>"1917", '1918'=>"1918", '1919'=>"1919", '1920'=>"1920", '1921'=>"1921", '1922'=>"1922", '1923'=>"1923", '1924'=>"1924", '1925'=>"1925", '1926'=>"1926", '1927'=>"1927", '1928'=>"1928", '1929'=>"1929", '1930'=>"1930", '1931'=>"1931", '1932'=>"1932", '1933'=>"1933", '1934'=>"1934", '1935'=>"1935", '1936'=>"1936", '1937'=>"1937", '1938'=>"1938", '1939'=>"1939", '1940'=>"1940", '1941'=>"1941", '1942'=>"1942", '1943'=>"1943", '1944'=>"1944", '1945'=>"1945", '1946'=>"1946", '1947'=>"1947", '1948'=>"1948", '1949'=>"1949", '1950'=>"1950", '1951'=>"1951", '1952'=>"1952", '1953'=>"1953", '1954'=>"1954", '1955'=>"1955", '1956'=>"1956", '1957'=>"1957", '1958'=>"1958", '1959'=>"1959", '1960'=>"1960", '1961'=>"1961", '1962'=>"1962", '1963'=>"1963", '1964'=>"1964", '1965'=>"1965", '1966'=>"1966", '1967'=>"1967", '1968'=>"1968", '1969'=>"1969", '1970'=>"1970", '1971'=>"1971", '1972'=>"1972", '1973'=>"1973", '1974'=>"1974", '1975'=>"1975", '1976'=>"1976", '1977'=>"1977", '1978'=>"1978", '1979'=>"1979", '1980'=>"1980", '1981'=>"1981", '1982'=>"1982", '1983'=>"1983", '1984'=>"1984", '1985'=>"1985", '1986'=>"1986", '1987'=>"1987", '1988'=>"1988", '1989'=>"1989", '1990'=>"1990", '1991'=>"1991", '1992'=>"1992", '1993'=>"1993", '1994'=>"1994", '1995'=>"1995", '1996'=>"1996", '1997'=>"1997", '1998'=>"1998", '1999'=>"1999", '2000'=>"2000", '2001'=>"2001", '2002'=>"2002", '2003'=>"2003", '2004'=>"2004", '2005'=>"2005", '2006'=>"2006", '2007'=>"2007", '2008'=>"2008", '2009'=>'2009', '2010'=>'2010', '2011'=>'2011', '2012'=>'2012', '2013'=>'2013', '2014'=>'2014');
// rsort($options  , SORT_DESC);

					$options = array(
						'0000' => 'YYYY',
						'2010' => '2010',
						'2009' => '2009',
						'2008' => '2008',
						'2007' => '2007',
						'2006' => '2006',
						'2005' => '2005',
						'2004' => '2004',
						'2003' => '2003',
						'2002' => '2002',
						'2001' => '2001',
						'2000' => '2000',
						'1999' => '1999',
						'1998' => '1998',
						'1997' => '1997',
						'1996' => '1996',
						'1995' => '1995',
						'1994' => '1994',
						'1993' => '1993',
						'1992' => '1992',
						'1991' => '1991',
						'1990' => '1990',
						'1989' => '1989',
						'1988' => '1988',
						'1987' => '1987',
						'1986' => '1986',
						'1985' => '1985',
						'1984' => '1984',
						'1983' => '1983',
						'1982' => '1982',
						'1981' => '1981',
						'1980' => '1980',
						'1979' => '1979',
						'1978' => '1978',
						'1977' => '1977',
						'1976' => '1976',
						'1975' => '1975',
						'1974' => '1974',
						'1973' => '1973',
						'1972' => '1972',
						'1971' => '1971',
						'1970' => '1970',
						'1969' => '1969',
						'1968' => '1968',
						'1967' => '1967',
						'1966' => '1966',
						'1965' => '1965',
						'1964' => '1964',
						'1963' => '1963',
						'1962' => '1962',
						'1961' => '1961',
						'1960' => '1960',
						'1959' => '1959',
						'1958' => '1958',
						'1957' => '1957',
						'1956' => '1956',
						'1955' => '1955',
						'1954' => '1954',
						'1953' => '1953',
						'1952' => '1952',
						'1951' => '1951',
						'1950' => '1950',
						'1949' => '1949',
						'1948' => '1948',
						'1947' => '1947',
						'1946' => '1946',
						'1945' => '1945',
						'1944' => '1944',
						'1943' => '1943',
						'1942' => '1942',
						'1941' => '1941',
						'1940' => '1940',
						'1939' => '1939',
						'1938' => '1938',
						'1937' => '1937',
						'1936' => '1936',
						'1935' => '1935',
						'1934' => '1934',
						'1933' => '1933',
						'1932' => '1932',
						'1931' => '1931',
						'1930' => '1930',
						'1929' => '1929',
						'1928' => '1928',
						'1927' => '1927',
						'1926' => '1926',
						'1925' => '1925',
						'1924' => '1924',
						'1923' => '1923',
						'1922' => '1922',
						'1921' => '1921',
						'1920' => '1920',
						'1919' => '1919',
						'1918' => '1918',
						'1917' => '1917',
						'1916' => '1916',
						'1915' => '1915',
						'1914' => '1914',
						'1913' => '1913',
						'1912' => '1912',
						'1911' => '1911',
						'1910' => '1910',
						'1910' => '1910'
					);
					$js = 'id="yeardropdown" ';
echo form_dropdown('yeardropdown', $options, $yeardropdown, 'class="dateclass" id="yeardropdown"', $js);
?></span>
                  </div>
                  <!--
                  <div class="field_50">	
                  <label>Age categories</label>
					
                  <?php
						$age_cate = array(
							'selectage' => 'Select',
							'18-29' => '18-29',
							'30-39' => '30-39',
							'40-49' => '40-49',
							'50-59' => '50-59',
							'60-69' => '60-69',
							'70-79' => '70-79',
							'80-84' => '80-84',
							'84+' => '84+'
						);
						echo form_dropdown('age_cate', $age_cate, $age_category, 'class="ageclass categoryclass cus-input" id="agerangeid" style="display: none;" ');
						?>
                  </div>
				  -->
              </div>
                
              <div class="field_row">  
                <div class="field_50">	
                  <label>Occupation</label>
                  <?php
$options = array(
	'selectoccupation' => 'Select',
	'Accounting' => 'Accounting',
	'Administrative/offic' => 'Administrative/office',
	'Advertising' => 'Advertising',
	'Animal care' => 'Animal care',
	'Architect' => 'Architect',
	'Art-creative' => 'Art-creative',
	'Athlete/coach' => 'Athlete/coach',
	'Building/surveying' => 'Building/surveying',
	'Customer service' => 'Customer service',
	'Design' => 'Design',
	'Emergency services' => 'Emergency services',
	'Education' => 'Education',
	'Engineering' => 'Engineering',
	'Entertainer' => 'Entertainer',
	'Farmer/primary production' => 'Farmer/primary production',
	'Finance' => 'Finance',
	'Garden care' => 'Garden care',
	'General business' => 'General business',
	'Health care' => 'Health care',
	'Human resources' => 'Human resources',
	'Information technology' => 'Information technology',
	'Legal' => 'Legal',
	'Management' => 'Management',
	'Manufacturing' => 'Manufacturing',
	'Manufacturing' => 'Manufacturing',
	'Medical/dental' => 'Medical/dental',
	'Nurse' => 'Nurse',
	'Pilot' => 'Pilot',
	'Production' => 'Production',
	'Public service' => 'Public service',
	'Research' => 'Research'
);
echo form_dropdown('occupation', $options, $occupation, 'id="occupationid"'); ?>
                  </div>
                 
                   <div class="field_50">	
                  <label>Sub-population</label>
		<?php
$subpop_array = array(
	'selectoccupation' => 'Select',
	'Active' => 'Active',
	'General' => 'General',
	'Sedentary' => 'Sedentary',
	'Athlete' => 'Athlete'
);
echo form_dropdown('subpopulation', $subpop_array, $subpopulation, 'class="subpopulationclass  cus-input" id="subpopulationid"');
?>  
                  </div>
                  
                            
              </div> 
              
               <div class="field_row">  
                   <div class="field_50">	
                  <label>Country</label>
                  <?php
$options = array(
	'selectcountry' => 'Select',
	'AF' => 'Afghanistan',
	'AX' => 'Aland Islands',
	'AL' => 'Albania',
	'DZ' => 'Algeria',
	'AS' => 'American Samoa',
	'AD' => 'Andorra',
	'AO' => 'Angola',
	'AI' => 'Anguilla',
	'AQ' => 'Antarctica',
	'AG' => 'Antigua and Barbuda',
	'AR' => 'Argentina',
	'AM' => 'Armenia',
	'AW' => 'Aruba',
	'AU' => 'Australia',
	'AT' => 'Austria',
	'AZ' => 'Azerbaijan',
	'BS' => 'Bahamas',
	'BH' => 'Bahrain',
	'BD' => 'Bangladesh',
	'BB' => 'Barbados',
	'BY' => 'Belarus',
	'BE' => 'Belgium',
	'BZ' => 'Belize',
	'BJ' => 'Benin',
	'BM' => 'Bermuda',
	'BT' => 'Bhutan',
	'BO' => 'Bolivia',
	'BQ' => 'Bonaire, Sint Eustatius and Saba',
	'BA' => 'Bosnia and Herzegovina',
	'BW' => 'Botswana',
	'BR' => 'Brazil',
	'IO' => 'British Indian Ocean Territory',
	'BN' => 'Brunei Darussalam',
	'BG' => 'Bulgaria',
	'BF' => 'Burkina Faso',
	'BI' => 'Burundi',
	'KH' => 'Cambodia',
	'CM' => 'Cameroon',
	'CA' => 'Canada',
	'CV' => 'Cape Verde',
	'KY' => 'Cayman Islands',
	'CF' => 'Central African Republic',
	'TD' => 'Chad',
	'CL' => 'Chile',
	'CN' => 'China',
	'CX' => 'Christmas Island',
	'CC' => 'Cocos (Keeling) Islands',
	'CO' => 'Colombia',
	'KM' => 'Comoros',
	'CG' => 'Congo',
	'CD' => 'Congo, The Democratic Republic of the',
	'CK' => 'Cook Islands',
	'CR' => 'Costa Rica',
	'CI' => 'Cote d Ivoire',
	'HR' => 'Croatia',
	'CU' => 'Cuba',
	'CW' => 'Curaçao',
	'CY' => 'Cyprus',
	'CZ' => 'Czech Republic',
	'DK' => 'Denmark',
	'DJ' => 'Djibouti',
	'DM' => 'Dominica',
	'DO' => 'Dominican Republic',
	'EC' => 'Ecuador',
	'EG' => 'Egypt',
	'SV' => 'El Salvador',
	'GQ' => 'Equatorial Guinea',
	'ER' => 'Eritrea',
	'EE' => 'Estonia',
	'ET' => 'Ethiopia',
	'FK' => 'Falkland Islands(Malvinas)',
	'FO' => 'Faroe Islands',
	'FJ' => 'Fiji',
	'FI' => 'Finland',
	'FR' => 'France',
	'GF' => 'French Guiana',
	'PF' => 'French Polynesia',
	'TF' => 'French Southern Territories',
	'GA' => 'Gabon',
	'GM' => 'Gambia',
	'GE' => 'Georgia',
	'DE' => 'Germany',
	'GH' => 'Ghana',
	'GI' => 'Gibraltar',
	'GR' => 'Greece',
	'GL' => 'Greenland',
	'GD' => 'Grenada',
	'GP' => 'Guadeloupe',
	'GU' => 'Guam',
	'GT' => 'Guatemala',
	'GG' => 'Guernsey',
	'GN' => 'Guinea',
	'GW' => 'Guinea-Bissau',
	'GY' => 'Guyana',
	'HT' => 'Haiti',
	'HM' => 'Heard Island and McDonald Islands',
	'VA' => 'Holy See (Vatican City State)',
	'HN' => 'Honduras',
	'HK' => 'Hong Kong',
	'HU' => 'Hungary',
	'IS' => 'Iceland',
	'IN' => 'India',
	'ID' => 'Indonesia',
	'XZ' => 'Installations in International Waters',
	'IR' => 'Iran, Islamic Republic of',
	'IQ' => 'Iraq',
	'IE' => 'Ireland',
	'IM' => 'Isle of Man',
	'IL' => 'Israel',
	'IT' => 'Italy',
	'JM' => 'Jamaica',
	'JP' => 'Japan',
	'JE' => 'Jersey',
	'JO' => 'Jordan',
	'KZ' => 'Kazakhstan',
	'KE' => 'Kenya',
	'KI' => 'Kiribati',
	'KP' => 'Korea, Democratic People s Republic of',
	'KR' => 'Korea, Republic of',
	'KW' => 'Kuwait',
	'KG' => 'Kyrgyzstan',
	'LA' => 'Lao People s Democratic Republic',
	'LV' => 'Latvia',
	'LB' => 'Lebanon',
	'LS' => 'Lesotho',
	'LR' => 'Liberia',
	'LY' => 'Libya',
	'LI' => 'Liechtenstein',
	'LT' => 'Lithuania',
	'LU' => 'Luxembourg',
	'MO' => 'Macao',
	'MK' => 'Macedonia, The former Yugoslav Republic of',
	'MG' => 'Madagascar',
	'MW' => 'Malawi',
	'MY' => 'Malaysia',
	'MV' => 'Maldives',
	'ML' => 'Mali',
	'MT' => 'Malta',
	'MH' => 'Marshall Islands',
	'MQ' => 'Martinique',
	'MR' => 'Mauritania',
	'MU' => 'Mauritius',
	'YT' => 'Mayotte',
	'MX' => 'Mexico',
	'FM' => 'Micronesia, Federated States of',
	'MD' => 'Moldova, Republic of',
	'MC' => 'Monaco',
	'MN' => 'Mongolia',
	'ME' => 'Montenegro',
	'MS' => 'Montserrat',
	'MA' => 'Morocco',
	'MZ' => 'Mozambique',
	'MM' => 'Myanmar',
	'NA' => 'Namibia',
	'NR' => 'Nauru',
	'NP' => 'Nepal',
	'NL' => 'Netherlands',
	'NC' => 'New Caledonia',
	'NZ' => 'New Zealand',
	'NI' => 'Nicaragua',
	'NE' => 'Niger',
	'NG' => 'Nigeria',
	'NU' => 'Niue',
	'NF' => 'Norfolk Island',
	'MP' => 'Northern Mariana Islands',
	'NO' => 'Norway',
	'OM' => 'Oman',
	'PK' => 'Pakistan',
	'PW' => 'Palau',
	'PS' => 'Palestine, State of',
	'PA' => 'Panama',
	'PG' => 'Papua New Guinea',
	'PY' => 'Paraguay',
	'PE' => 'Peru',
	'PH' => 'Philippines',
	'PN' => 'Pitcairn',
	'PL' => 'Poland',
	'PT' => 'Portugal',
	'PR' => 'Puerto Rico',
	'QA' => 'Qatar',
	'RE' => 'Reunion',
	'RO' => 'Romania',
	'RU' => 'Russian Federation',
	'RW' => 'Rwanda',
	'BL' => 'Saint Barthelemy',
	'SH' => 'Saint Helena, Ascension and Tristan Da Cunha',
	'KN' => 'Saint Kitts and Nevis',
	'LC' => 'Saint Lucia',
	'MF' => 'Saint Martin (French Part)',
	'PM' => 'Saint Pierre and Miquelon',
	'VC' => 'Saint Vincent and the Grenadines',
	'WS' => 'Samoa',
	'SM' => 'San Marino',
	'ST' => 'Sao Tome and Principe',
	'SA' => 'Saudi Arabia',
	'SN' => 'Senegal',
	'RS' => 'Serbia',
	'SC' => 'Seychelles',
	'SL' => 'Sierra Leone',
	'SG' => 'Singapore',
	'SX' => 'Sint Maarten (Dutch Part)',
	'SK' => 'Slovakia',
	'SI' => 'Slovenia',
	'SB' => 'Solomon Islands',
	'SO' => 'Somalia',
	'ZA' => 'South Africa',
	'GS' => 'South Georgia and the South Sandwich Islands',
	'SS' => 'South Sudan',
	'ES' => 'Spain',
	'LK' => 'Sri Lanka',
	'SD' => 'Sudan',
	'SR' => 'Suriname',
	'SJ' => 'Svalbard and Jan Mayen',
	'SZ' => 'Swaziland',
	'SE' => 'Sweden',
	'CH' => 'Switzerland',
	'SY' => 'Syrian Arab Republic',
	'TW' => 'Taiwan, Province of China',
	'TJ' => 'Tajikistan',
	'TZ' => 'Tanzania, United Republic of',
	'TH' => 'Thailand',
	'TL' => 'Timor-Leste',
	'TG' => 'Togo',
	'TK' => 'Tokelau',
	'TO' => 'Tonga',
	'TT' => 'Trinidad and Tobago',
	'TN' => 'Tunisia',
	'TR' => 'Turkey',
	'TM' => 'Turkmenistan',
	'TC' => 'Turks and Caicos Islands',
	'TV' => 'Tuvalu',
	'UG' => 'Uganda',
	'UA' => 'Ukraine',
	'AE' => 'United Arab Emirates',
	'GB' => 'United Kingdom',
	'US' => 'United States',
	'UM' => 'United States Minor Outlying Islands',
	'UY' => 'Uruguay',
	'UZ' => 'Uzbekistan',
	'VU' => 'Vanuatu',
	'VE' => 'Venezuela',
	'VN' => 'Viet Nam',
	'VG' => 'Virgin Islands, British',
	'VI' => 'Virgin Islands, U.S.',
	'WF' => 'Wallis and Futuna',
	'EH' => 'Western Sahara',
	'YE' => 'Yemen',
	'ZM' => 'Zambia',
	'ZW' => 'Zimbabwe'
);
echo form_dropdown('country', $options, $country, "id='country_id'");
?>
                    
                    <label style="margin-top: 10px;">Project</label>
						<?php
$p_options = ['Select'];
$p_options = array_merge($p_options, $projects);
?> 
						<?php
echo form_dropdown('projectname', $p_options, $p_name, "id='projectname_id'"); ?>
                  </div>
                    
                    
                   <div class="field_50">
                    <label>Notes</label>
                    <?php
echo form_textarea('p_desc', $p_desc, "class='notenew' id='notenew_id'"); ?>
				   </div>
                   
                    
              		
               </div>
               
               <div class="field_row">  	
						
                    <div class="field_50">
                    <div><?php
echo $msg; ?></div>
                    <label>Access code (if applicable)</label>
                    <input id="accesscode_id" type="text" name="code" value="" style="width:150px;">
                    </div>

					<?php
						echo form_submit('submit_dashboard', 'Save and Continue', "class='lite_btn program_btn active clr_blue save_continue'");
					?>

              
                    <input type="hidden" name="Random_generated_log_BMI" value="<?php
echo $randomdata['Random_generated_log_BMI'] ?>">
                    <input type="hidden" name="MASS" value="<?php
echo $randomdata['MASS']; ?>"> 
                    <input type="hidden" name="BMI" value="<?php
echo $randomdata['BMI']; ?>">
                     <input type="hidden" name="HEIGHT" value="<?php
echo $randomdata['HEIGHT']; ?>">
                      <input type="hidden" name="vp_age" value="<?php
echo $randomdata['vp_age']; ?>">
					    <input type="hidden" value="<?php
echo $filename; ?>" id="hiddenval" name="profile_img">
               
               </div> 
              
              <!-- <span>         
                  <?php
echo form_submit('mysubmit1', 'Pre-exercise screening'); ?> 
                  <?php
echo form_submit('mysubmit2', 'Fitness testing', "class='start-btn fitness_test_btn'"); ?>
                  <?php
echo form_submit('mysubmit3', 'Body composition', "class='start-btn body_comp_btn'"); ?>
               </span>
              
               <button class="lite_btn f_right">Continue</button>
              --> 
                      
        </div>
        
        <div class="welc_form_row">
            <!--
			<h4>Select module</h4>            
           
            <?php
echo form_submit('mysubmit1', 'Pre-exercise Screening', "class='lite_btn program_btn active'"); ?> 
            <?php
echo form_submit('mysubmit2', 'Fitness Testing', "class='lite_btn program_btn active clr_grn'"); ?>
            <?php
echo form_submit('mysubmit3', 'Body Composition', "class='lite_btn program_btn active clr_vio'"); ?>
            <?php
echo form_submit('mysubmit4', 'Sport Match', "class='lite_btn program_btn active clr_cyan'"); ?>
		     <?php
echo form_submit('mysubmit5', 'Virtual Population', "class='lite_btn program_btn active clr_grey mr_0'"); ?>
			 <?php
echo form_submit('mysubmit6', 'Blood Biomarkers', "class='lite_btn program_btn active clr_blue'"); ?>
-->
		</div>
		<?php
echo form_close(); ?>
  </div>
  
 
</div><!--End Wrapper --> 

<div class="overlay">&nbsp;</div>
<div class="info_block" id="info_block">
	<div class="info_block_head">Professor Kevin Norton</div>
	<img src="<?php
echo "$base/assets/images/" ?>kevin.jpg" style="float:left; height: 200px; margin-right: 20px;">
	<p>Dr Kevin NORTON is a Professor in Exercise Science at the University of South Australia. His research focuses on the evolution of sports performance, health screening, physical activity promotion, and the limits of human work capacity. He has worked with professional sporting bodies including the Australian Football League, National Rugby League, Super League, Australian Rugby Union and the International Rugby Board. His research has been supported by numerous organisations including the International Olympic Committee, Australian Sports Commission as well as NHMRC, Australian Research Council and DFAT. Kevin has had extensive sports industry involvement with secondments to Sport Knowledge Australia and Human Kinetics [USA] to develop sports science software, and a sports science role with the Adelaide Football Club in the AFL.</p>    
	 <div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	 </div>                
</div> 

<div class="info_block" id="linda_info_block">
	<div class="info_block_head">Dr Lynda NORTON</div>
	<img src="<?php
echo "$base/assets/images/" ?>lynda.jpg" style="float:left; height: 200px; margin-right: 20px;">
	<p>Dr Lynda NORTON is a Lecturer at Flinders University. She is a registered nurse who moved into public health research following her MPH and PhD. She has considerable experience with database management having worked for both the Australian New Zealand Intensive Care Society and the Australian Research Centre for Injury Studies. She has led several multi-year intervention studies on physical activity and health. Her teaching is focused on healthy lifestyles and associated health risk factors for health and exercise science students. Current research interests include trends in adult health and fitness, relationships between exercise and health outcomes and mindfulness and stress modification in emergency services personnel.</p>    
	 <div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	 </div>                
</div>        

<div class="footer"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">&copy; <a href="javaScript:void(0);" id="info_icon_btn">Professor Kevin Norton</a>, <a href="javaScript:void(0);" id="linda_info_icon_btn">Dr Lynda Norton</a> and The University of South Australia</p> 
        <p style="text-align: right; line-height: 50px;"><img src="<?php
echo "$base/assets/images/" ?>university_logo.png" style="height:35px !important; float:right; margin:12px 20px 12px 15px;"></p>
    </div> 
</div>
</body>
</html>
  

 
