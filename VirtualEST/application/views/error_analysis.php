<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<!-- custom scrollbar stylesheet -->
<link rel="stylesheet" href="<?php echo "$base/assets/css/"?>jquery.mCustomScrollbar.css">
<style>
 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
.mCSB_scrollTools .mCSB_buttonUp,.mCSB_scrollTools .mCSB_buttonDown{display:none !important;}
</style>
<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>
<body>

<!-- insertion of header -->
<?=$header_insert; ?>

<div class="overlay">&nbsp;</div>
<div class="info_block">
	<div class="info_block_head violet_container">Error Analysis</div>
	<p>The Error Analysis allows the user to perform several analytical tasks including: Calculating Technical Error of Measurement [TEM] from raw data, calculating the 95% confidence intervals around a single measure,
	   and determining if a real change has ocurred between two measures on the same person.
	</p>    
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>                
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Fitness/saveClientVo2MaxInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	
    <div class="contain">
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Error Analysis</div>
            
            <div class="field_row verticle_field_row"> 
                <div class="half_container f_left">
                	<p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis'); ?>';" id="tem" name="error_radio" value="tem" checked="checked">
                    <label for="tem"> <span style="margin-right:8px;"></span>Calculate TEM from raw data</label></p>
                    <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_analysis_confidence'); ?>';" id="single_measure" name="error_radio" value="single_measure"> 
                    <label for="single_measure"> <span style="margin-right:8px;"></span>95% CI around a single measure</label></p><!--<img src="<?php echo "$base/$image"?>/red_dot.png">-->
                    <p><input type="radio" onclick="window.location.href = '<?php  echo site_url('Body/error_real_change'); ?>';" id="real_change" name="error_radio" value="real_change"> 
                    <label for="real_change"> <span style="margin-right:8px;"></span>Has a real change occurred?</label></p>
                </div>
                <div class="half_container f_right">
                	<div class="note_text">
                        <p>Enter the raw data from Trial 1 in the first field, and the matching data from Trial 2 in the second field. Use the 'tab' key to move from cell to cell. There must be an equal number of pairs. There is an option to calculate your TEM using 3 datasets in which case enter raw data from Trial 3 in the third field. Click on the COMPUTE button to show the calculated TEM results.</p>
                    </div>
                </div>
            </div>
            
            <div class="field_row verticle_field_row" style="border-bottom:0;"> 
            	<div class="half_container f_left error_select">
                	<div class="select_div">
                    	<p>Trial 1 <span id="list1_items"></span></p>
                        <div class="Scroll_content">
                        <ul id="ulscroller1">
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="1"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="4"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="7"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="10"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="13"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="16"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="19"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="22"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="25"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="28"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="31"></li>
                            <li><input type="text"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="34"></li>
                            <li id="37"><input type="text"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="37"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">
                    	<p>Trial 2 <span id="list2_items"></span></p>
                        <div class="Scroll_content">
                    	<ul id="ulscroller2">
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="2"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="5"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="8"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="11"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="14"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="17"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="20"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="23"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="26"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="29"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="32"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="35"></li>
                            <li id="38"><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="38"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">
                    	<p>Trial 3 <span id="list3_items"></span></p>
                        <div class="Scroll_content">
                    	<ul id="ulscroller3">
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="3"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="6"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="9"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="12"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="15"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="18"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="21"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="24"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="27"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="30"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="33"></li>
                            <li><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="36"></li>
                            <li id="39"><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*"  tabindex="39"></li>
                        </ul> 
                        </div>
                    </div>                	
                </div>
                
                <div class="half_container f_right">
                	<div class="calculation_box" style="float: right; width: 160px; background: #f2f2f2; padding:10px 20px; margin:0; margin-left:80px; margin-top: 48px; line-height: 30px">
                        <p>Correlation = <label id="correlation" style="display:inline-block; margin-left:5px; margin-bottom:0;"></label></p>
                        <p>TEM = <label id="tem_value" style="display:inline-block; margin-left:5px; margin-bottom:0;"></label></p>
                        <p>TEM% = <label id="tem_percent" style="display:inline-block; margin-left:5px; margin-bottom:0;"></label></p> 
                    </div>
                    <button id="compute" name="compute" class="lite_btn f_left btn_vio" style="margin-top:48px;">Compute</button>
                    <button id="clear" name="clear" onclick="clear_columns();" class="lite_btn btn_virtual" style="margin-top:294px;">Clear</button>
                </div>
            </div>            
    </div>
	
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<!-- custom scrollbar plugin -->
<script src="<?php echo "$base/assets/js/"?>jquery.mCustomScrollbar.concat.min.js"></script>

<script>
	(function($){
		$(window).load(function(){
			
			$(".Scroll_content").mCustomScrollbar({
				snapAmount:40,
				scrollButtons:{enable:true},
				keyboard:{scrollAmount:40},
				mouseWheel:{deltaFactor:40},
				scrollInertia:400
			});
			
		});
	})(jQuery);
</script>
<script type="text/javascript">

        $(document).on('click','#exit', function(){
          window.location.href = "<?php  echo site_url('Body/index'); ?>" ;
            //return false;
        });	
	
        
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
        
     $(document).on('focusin', '#ulscroller1 li:last-child', function(){     
       var listId_1 = this.id ;     
       var tabVal_1 = parseInt(listId_1) + 3 ;       
       $("#ulscroller1").append('<li id='+tabVal_1+'><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" tabIndex='+tabVal_1+'></li>');         
    });
        
    $(document).on('focusin', '#ulscroller2 li:last-child', function(){
       var listId_2 = this.id ;     
       var tabVal_2 = parseInt(listId_2) + 3 ;  
       $("#ulscroller2").append('<li id='+tabVal_2+'><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" tabIndex='+tabVal_2+'></li>');         
    });
        
    $(document).on('focusin', '#ulscroller3 li:last-child', function(){
       var listId_3 = this.id ;     
       var tabVal_3 = parseInt(listId_3) + 3 ;  
       $("#ulscroller3").append('<li id='+tabVal_3+'><input type="text" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" tabIndex='+tabVal_3+'></li>');         
    }); 



  
  // Get counts of all values for each trial columns

        $(document).on('keyup', '#ulscroller1 li input', function(){
          var items1 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var values1 = $(this).val();	
            if(values1 !== "")
              { 
                  items1.push(values1);
              }
            }); 
            
            document.getElementById("list1_items").innerHTML = "["+items1.length+"]" ;
        });


        $(document).on('keyup', '#ulscroller2 li input', function(){
          var items2 = []; 
            
            $("#ulscroller2 li input").each(function() {
            var values2 = $(this).val();	
            if(values2 !== "")
              { 
                  items2.push(values2);
              }
            });
            
             document.getElementById("list2_items").innerHTML = "["+items2.length+"]" ;
        });
        
        $(document).on('keyup', '#ulscroller3 li input', function(){
          var items3 = []; 
            
            $("#ulscroller3 li input").each(function() {
            var values3 = $(this).val();	
            if(values3 !== "")
              { 
                  items3.push(values3);
              }
            }); 
            
            document.getElementById("list3_items").innerHTML = "["+items3.length+"]" ;
        });
        
        
               
// Check for equal number of Pairs for all the 3 Trials

        $(document).on('click','#compute', function(){
            
            var trial1 = []; var trial2 = []; var trial3 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var list1 = $(this).val();	
            if(list1 !== "")
              { 
                  trial1.push(list1);
              }
            });
            
            $("#ulscroller2 li input").each(function() {
            var list2 = $(this).val();	
            if(list2 !== "")
              { 
                  trial2.push(list2);
              }
            });
                        
            
            $("#ulscroller3 li input").each(function() {
            var list3 = $(this).val();	
            if(list3 !== "")
              { 
                  trial3.push(list3);
              }
            });
            
            //if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
      
      
         if(trial3.length == 0)
         {   
            if(trial1.length == trial2.length && trial1.length !== 0  && trial2.length !== 0)
            {
                //alert("All equal Pairs");
                get_2tem() ;
            }
            else
            {
                if(trial1.length == 0  && trial2.length == 0)
                {
                    alert("Please enter Trial values");
                }
                else
                {
                    alert("You must have equal number of pairs");
                }
                
            }
        }
        else
        {
            if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
               {
                   //alert("All equal Pairs");
                   get_3tem() ;
               }
               else
               {
                   if(trial1.length == 0  && trial2.length == 0 && trial3.length == 0)
                   {
                       alert("Please enter Trial values");
                   }
                   else
                   {
                       alert("You must have equal number of pairs");
                   }

               }   
        }
            
        });
  
  
    function clear_columns()
    {
        var trial1 = []; var trial2 = []; var trial3 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var list1 = $(this).val();	
            if(list1 !== "")
              { 
                  trial1.push(list1);
              }
            });
            
            $("#ulscroller2 li input").each(function() {
            var list2 = $(this).val();	
            if(list2 !== "")
              { 
                  trial2.push(list2);
              }
            });
                        
            
            $("#ulscroller3 li input").each(function() {
            var list3 = $(this).val();	
            if(list3 !== "")
              { 
                  trial3.push(list3);
              }
            });
        
            var index ;        
            for(index = 1; index <= trial1.length ; index +=1)
            {
                $("#ulscroller1 li input").val("");               
            }

            for(index = 1; index <= trial2.length ; index +=1)
            {
                $("#ulscroller2 li input").val("");               
            }

            for(index = 1; index <= trial3.length ; index +=1)
            {
                $("#ulscroller3 li input").val("");                
            }
            
              
            $('#list1_items').empty();
            $('#list2_items').empty();
            $('#list3_items').empty();
            $('#tem_value').empty(); 
            $('#correlation').empty();
            $('#tem_percent').empty();
    }

  // TEM calculation with 2 trial Columns
    function get_2tem()
    {
       var trial1 = [] ; // Trial1 values
       var trial2 = [] ; // Trial2 values

      // Sum of Trial1 values  
       var sum1 = 0;
       var squareSum1 = 0 ;
        $("#ulscroller1 li input").each(function() {
          var items1 = $(this).val();	
          if(items1 !== "")
          { 
            sum1 = sum1 + parseFloat(items1) ;
            squareSum1 = squareSum1 + (parseFloat(items1) * parseFloat(items1)) ;
            trial1.push(items1);  
          }
         }); 


      // Sum of Trial2 values      
       var sum2 = 0;    
       var squareSum2 = 0 ;
        $("#ulscroller2 li input").each(function() {
          var items2 = $(this).val();	
          if(items2 !== "")
          { 
            sum2 = sum2 + parseFloat(items2) ;
            squareSum2 = squareSum2 + (parseFloat(items2) * parseFloat(items2)) ;
            trial2.push(items2);  
          }
         }); 

         var N1 = trial1.length ;
         var N2 = trial2.length ;     

         var Mean1 = sum1 / N1 ;
         var Mean2 = sum2 / N2 ;     

         var SumTot = sum1 + sum2 ;
         var GrandMean = SumTot / (N1 + N2) ;         

         var SumSquaredTot = squareSum1 + squareSum2 ;

         var SST = SumSquaredTot - ((SumTot * SumTot) / (N1 * 2));      

         var P = [] ;
         var index ;
         for(index = 0; index < trial1.length; ++index) 
         {
            P[index] = parseFloat(trial1[index]) + parseFloat(trial2[index]) ;
         }

         var SumPSquared = 0 ;   
         for(index = 0; index < P.length; ++index) 
         {
           SumPSquared = SumPSquared + (P[index] * P[index]) ;    
         } 

         var SSB = SumPSquared / 2 - (SumTot * SumTot) / (2 * N1) ;

         var MSW = (SST - SSB) / N1 ;
         var MSB = SSB / (N1 - 1) ;       

         var TEM = Math.sqrt(MSW) ; // TEM value  
         var TEM_Percent = TEM / GrandMean * 100 // % TEM value         
         var Correlation = ((MSB - MSW) / (MSB + MSW)) ;  // Correlation value   


         document.getElementById("tem_value").innerHTML = Math.round(TEM * 1000) / 1000 ;   
         document.getElementById("correlation").innerHTML = Math.round(Correlation * 1000) / 1000 ; 
         document.getElementById("tem_percent").innerHTML = Math.round(TEM_Percent * 1000) / 1000 ; 
    }




  // TEM calculation with 3 trial Columns
    function get_3tem()
    {
       var trial1 = [] ; // Trial1 values
       var trial2 = [] ; // Trial2 values
       var trial3 = [] ; // Trial3 values

      // Sum of Trial1 values  
       var sum1 = 0;
       var squareSum1 = 0 ;
        $("#ulscroller1 li input").each(function() {
          var items1 = $(this).val();	
          if(items1 !== "")
          { 
            sum1 = sum1 + parseFloat(items1) ;
            squareSum1 = squareSum1 + (parseFloat(items1) * parseFloat(items1)) ;
            trial1.push(items1);  
          }
         }); 


     // Sum of Trial2 values      
       var sum2 = 0;    
       var squareSum2 = 0 ;
        $("#ulscroller2 li input").each(function() {
          var items2 = $(this).val();	
          if(items2 !== "")
          { 
            sum2 = sum2 + parseFloat(items2) ;
            squareSum2 = squareSum2 + (parseFloat(items2) * parseFloat(items2)) ;
            trial2.push(items2);  
          }
         });
         

     // Sum of Trial3 values      
       var sum3 = 0;    
       var squareSum3 = 0 ;
        $("#ulscroller3 li input").each(function() {
          var items3 = $(this).val();	
          if(items3 !== "")
          { 
            sum3 = sum3 + parseFloat(items3) ;
            squareSum3 = squareSum3 + (parseFloat(items3) * parseFloat(items3)) ;
            trial3.push(items3);  
          }
         });

         var N1 = trial1.length ;
         var N2 = trial2.length ;
         var N3 = trial3.length ;

         var Mean1 = sum1 / N1 ;
         var Mean2 = sum2 / N2 ; 
         var Mean3 = sum3 / N3 ; 

         var SumTot = sum1 + sum2 + sum3 ;
         var GrandMean = SumTot / (N1 + N2 + N3) ;         

         var SumSquaredTot = squareSum1 + squareSum2 + squareSum3 ;

         var SST = SumSquaredTot - ((SumTot * SumTot) / (N1 * 3));      

         var P = [] ;
         var index ;
         for(index = 0; index < trial1.length; ++index) 
         {
            P[index] = parseFloat(trial1[index]) + parseFloat(trial2[index]) + parseFloat(trial3[index]) ;
         }

         var SumPSquared = 0 ;   
         for(index = 0; index < P.length; ++index) 
         {
           SumPSquared = SumPSquared + (P[index] * P[index]) ;    
         } 

         var SSB = SumPSquared / 3 - (SumTot * SumTot) / (3 * N1) ;

         var MSW = (SST - SSB) / (2 * N1) ;
         var MSB = SSB / (N1 - 1) ;       

         var TEM = Math.sqrt(MSW) ; // TEM value  
         var TEM_Percent = TEM / GrandMean * 100 // % TEM value         
         var Correlation = ((MSB - MSW) / (MSB + 2 * MSW)) ;  // Correlation value   


         document.getElementById("tem_value").innerHTML = Math.round(TEM * 1000) / 1000 ;   
         document.getElementById("correlation").innerHTML = Math.round(Correlation * 1000) / 1000 ; 
         document.getElementById("tem_percent").innerHTML = Math.round(TEM_Percent * 1000) / 1000 ; 
    }
        
</script>    
</body>
</html>
