<?php 
		//To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
              $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
              $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
              $bodyfatdiv="block";    
            }
            else
            {
              $bodyfatdiv="none";    
            }

            $isvp=$_SESSION['is_virtual'];
            //print_r($_SESSION);  

            if(isset($_SESSION["gender"]))
            {
                $gender=$_SESSION["gender"];
            }
            else
            {
                $gender=$_SESSION["user_gender"];
            }
             
            $isvp=$_SESSION['is_virtual'];
            //print_r($fieldData);
			 
?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<style>

sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>

    
    
    
<script type="text/javascript">
	$(document).ready(function() {
        
         var isvp='<?php echo $isvp;?>';
        
        var divHeight = $('.contain').height(); 
		 $('.side_menu').css('height', divHeight+'px');
		if(isvp == 1)
        {
          $(".v_person").fadeTo( "slow" , 1, function() {});
		  $(".v_detail").toggle();
		  calculate_oz_rating();
		}
		//Show Sports list with oz Ranking
		
      
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>    


</head>
<body>

<!-- insertion of header -->
<?=$header_insert; ?>

<div class="overlay">&nbsp;</div> 
<div class="info_block">
            <div class="info_block_head" style="background-color: #16becf;">Sport Match</div>
            <p>The morphological overlap between the client and athletes within each sport is calculated and shown in the table. The table shows how well the match is between the client and elite-level athlete. This is based on height and mass, % body fat, and calculated somatotype  [if all the necessary anthropometric variables have been entered]. A rating of 100 is a perfect match, overlap values above 80% are excellent matches, values between 0.61 and 0.80 represent substantial morphological overlap, 0.41 to 0.60 represent a moderate match, 0.10 to 0.40 suggest a poor to fair match, while values less than 0.1 show a very poor match.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/go_on_menu', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<!--Start right --> 
        <div class="right-section right-section_new" style="width: 965px;">
        	<div class="right-head">Sport Match</div>
            
            <div class="field_row" style="border: 0;">
				<div class="field_70 f_left">

					<script>
						// on mouse move, update numbers next to sliders
						$(".field_70.f_left").mousemove(function() {
							$("#height_text").val(document.getElementById("height_slider").value);
							$("#mass_text").val(document.getElementById("mass_slider").value);
							$("#body_fat_per").val(document.getElementById("body_fat_slider").value);
							$("#endomorph_txt").val(document.getElementById("endomorph_slider").value);
							$("#mesomorph_txt").val(document.getElementById("mesomorph_slider").value);
							$("#ectomorph_txt").val(document.getElementById("ectomorph_slider").value);
						});
					</script>

					<div class="f_left width_70px"><label>Height (cm)</label><input type="text" id="height_text" name="height_text" value="<?php 
                                                                        if(isset($_SESSION['HEIGHT']))
                                                                        {
                                                                          echo $_SESSION['HEIGHT'];  
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "130";
                                                                        }?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="height_slider" type="range" value="<?php if(isset($_SESSION['HEIGHT']))
                                                                        {
                                                                          echo $_SESSION['HEIGHT'];  
                                                                        }
                                                                        else
                                                                        {
                                                                            echo "130";
                                                                        }?>" min="130" max="210" step="0.1" link-to="height_text" data-rangeslider id="height_slider">
						<ul class="range_numbers range_hori">
							<li>130</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>210</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>Mass (kg)</label><input type="text" id="mass_text" name="mass_text" value="<?php if(isset($_SESSION['MASS']))
                                                                        {
                                                                          echo $_SESSION['MASS'];  
                                                                        }
                                                                        else
                                                                        {
                                                                          echo "30";
                                                                        }?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="mass_slider" type="range" value="<?php if(isset($_SESSION['MASS']))
                                                                        {
                                                                          echo $_SESSION['MASS'];  
                                                                        }
                                                                        else
                                                                        {
                                                                          echo "30";
                                                                        }?>" min="30" max="160" step="0.1" link-to="mass_text" data-rangeslider id="mass_slider">
						<ul class="range_numbers range_hori range_hori2">
							<li>30</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>160</li>
						</ul>
					</div>	
																			
					<div class="clearfix">&nbsp;</div>
					
					<div class="f_left width_70px"><label>% body fat</label><input type="text" id="body_fat_per" name="body_fat_per" value="<?php  if(isset($_SESSION['bodyfat']))
                                                                                                        {
                                                                                                          echo $_SESSION['bodyfat'];  
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            echo "3";
                                                                                                        }?>" style="width:54px;"> </div>
					<div class="range_div horiz_range_div">
						<input name="body_fat_slider" type="range"  value="<?php  if(isset($_SESSION['bodyfat']))
                                                                                                        {
                                                                                                          echo $_SESSION['bodyfat'];  
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            echo "3";
                                                                                                        }
                                                                           ?>" min="3" max="40" step="0.1" link-to="body_fat_per" data-rangeslider id="body_fat_slider">
						<ul class="range_numbers range_hori range_hori2">
							<li>3</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>40</li>
						</ul>
					</div>																							
					
					<div class="clearfix">&nbsp;</div>
					<strong class="right_sp_head" style="margin: 20px 0;">Somatotype</strong>	
																																	
					<div class="f_left width_70px"><label>Endomorph</label><input type="text" id="endomorph_txt" name="endomorph_txt"  style="width:54px;" value="<?php  if(isset($_SESSION['endo_score']))
                                {
                                  echo $_SESSION['endo_score'];  
                                }
                                else
                                {
                                    echo "0";
                                }?>">  </div>
					<div class="range_div horiz_range_div">
						<input name="endomorph_slider" type="range"  min="0" max="10" step="0.01" link-to="endomorph_txt" data-rangeslider id="endomorph_slider" value="<?php  if(isset($_SESSION['endo_score']))
                                {
                                  echo $_SESSION['endo_score'];  
                                }
                                else
                                {
                                    echo "0";
                                }?>">
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>	
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Mesomorph</label><input type="text" id="mesomorph_txt" name="mesomorph_txt"  style="width:54px;" value="<?php if(isset($_SESSION['meso_score']))
                                {
                                  echo $_SESSION['meso_score'];  
                                }
                                else
                                {
                                    echo "0";
                                }?>"> </div>
					<div class="range_div horiz_range_div">
						<input name="mesomorph_slider" type="range"  min="0" max="10" step="0.01" link-to="mesomorph_txt" data-rangeslider id="mesomorph_slider" value="<?php if(isset($_SESSION['meso_score']))
                                {
                                  echo $_SESSION['meso_score'];  
                                }
                                else
                                {
                                    echo "0";
                                }?>" >
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>		
					
					<div class="clearfix">&nbsp;</div>	
					<div class="f_left width_70px"><label>Ectomorph</label><input type="text" id="ectomorph_txt" name="ectomorph_txt"  style="width:54px;" value="<?php  if(isset($_SESSION['ecto_score']))
                                {
                                  echo $_SESSION['ecto_score'];
                                }
                                else
                                {
                                    echo "0";
                                }  ?>"> </div>
					<div class="range_div horiz_range_div">
						<input name="ectomorph_slider" type="range"  min="0" max="10" step="0.01" link-to="ectomorph_txt" data-rangeslider id="ectomorph_slider" value="<?php  if(isset($_SESSION['ecto_score']))
                                {
                                  echo $_SESSION['ecto_score'];
                                }
                                else
                                {
                                    echo "0";
                                } ?>">
						<ul class="range_numbers range_hori">
							<li>0</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>&nbsp;</li>
							<li>10</li>
						</ul>
					</div>
					
					<div class="clearfix">&nbsp;</div>
					<!--button class="lite_btn f_left" type="button" style="margin-top:30px;">Recalculate</button-->
				</div>     
				
				<div class="field_30 f_right">
					<strong class="right_sp_head color_cyan">Sport Overlap</strong>
					
					<div class="sport_table">
						<table width="100%" border="0" id="mytable">
					   <thead>
					   		<tr>
							  <th align="left" height="35px">Sport</th>
							  <th align="right" height="35px">OZ rating (%)</th>
							</tr>					   	
					   </thead>
					  <tbody class="body_sports_table">
                          
                               <?php foreach($fieldData as $val)
                                                {
                                                echo "<tr>
                                                      <td align='left'>".$val->sport."</td>
                                                      <td align='right'>XX.X</td>
                                                    </tr>
                                                 " ; 
                                                }
                                              ?> 
                          
						
                                             
					  </tbody>
					</table>
					</div>
				</div>     
			</div>
        </div>
    </div>
      
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>	
	
  <script>
  var body_fatarray = [];
/*      
  $(document).on('click','#exit', function(){           
          document.forms["myform"].submit();
        //return false;
        });*/	
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
}); 
   

 $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
      var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
             $text_box.val(this.value); 
            calculate_oz_rating();
	});
  $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
});
      $text_box.val(this.value);   
	});
}); 

//Sport Match 	
function calculate_oz_rating()
{
     var c_ht=document.getElementById("height_slider").value;            //alert(c_ht);
     var c_mass=document.getElementById("mass_slider").value;            //alert(c_mass);
     var c_body_fat=document.getElementById("body_fat_slider").value;    //alert(c_body_fat);
     var c_endomorph=document.getElementById("endomorph_slider").value;  //alert(c_endomorph);
     var c_mesomorph=document.getElementById("mesomorph_slider").value;  //alert(c_mesomorph);
     var c_ectomorph=document.getElementById("ectomorph_slider").value;  //alert(c_ectomorph);
      if(isNaN(c_endomorph))
          {
              c_endomorph=0;
          }
      if(isNaN(c_mesomorph))
          {
              c_mesomorph=0;
          }
      if(isNaN(c_ectomorph))
          {
              c_ectomorph=0;
          }
	var sport_arr= '<?php echo json_encode($fieldData); ?>' ;      console.log(sport_arr);
    var objs = [];
	var htmlTable ='' ;
    var gender='<?php echo $gender;  ?>'; 
    
    $.each($.parseJSON(sport_arr), function(key,value){
    if( gender == 'F' || gender == 'Female')    
             {
                 var gen="female";
             }
    else
             {
                 var gen="male";
             }
    if(value.gender == gen )    
            {           if(value.body_fat_sd == "")
                            {
                                body_fatarray.push(value.sport);
                            }    
                        else{
                       //z score    
                        var ZSportFat = parseFloat(((c_body_fat-value.body_fat_per)/(value.body_fat_sd)).toFixed(9));
                        //console.log("ZSportFat"+ZSportFat);
						var ZSportHt = parseFloat(((c_ht-value.height)/(value.height_sd)).toFixed(9));
						//console.log("ZSportHt"+ZSportHt);
						var ZSportMass = parseFloat(((c_mass-value.body_mass)/(value.body_mass_sd)).toFixed(9));
						//console.log("ZSportMass"+ZSportMass);
						var ZSportEndo =parseFloat(((c_endomorph-value.endomorph)/(value.endo_sd)).toFixed(9)); 
					    //console.log("ZSportEndo"+ZSportEndo);
						var ZSportMeso =parseFloat(((c_mesomorph-value.mesomorph)/(value.meso_sd)).toFixed(9));  
						//console.log("ZSportMeso"+ZSportMeso);
						var ZSportEcto = parseFloat(((c_ectomorph-value.ectomorph)/(value.ecto_sd)).toFixed(9));  
						//console.log("ZSportEcto"+ZSportEcto);
						// Probability    
                        var pFat = parseFloat((2*normalcdf(ZSportFat)-1).toFixed(9));
                        //console.log("pFat"+pFat);
						var pHt = parseFloat((2*normalcdf(ZSportHt)-1).toFixed(9));
                        //console.log("pHt"+pHt);
						var pMass = parseFloat((2*normalcdf(ZSportMass)-1).toFixed(9)); 
                        //console.log("pMass"+pMass);
						var pEndo = parseFloat((2*normalcdf(ZSportEndo)-1).toFixed(9));
                         //console.log("pEndo"+pEndo);
						var pMeso = parseFloat((2*normalcdf(ZSportMeso)-1).toFixed(9));
                         //console.log("pMeso"+pMeso);
						var pEcto = parseFloat((2*normalcdf(ZSportEcto)-1).toFixed(9)) ;  
						//console.log("pEcto"+pEcto);
						var distance= parseFloat((Math.sqrt(Math.pow(pFat,2)+Math.pow(pHt,2)+Math.pow(pMass,2)+Math.pow(pEndo,2)+Math.pow(pMeso,2)+Math.pow(pEcto,2)).toFixed(9)));   
						//console.log("distance"+distance);
						var finaloverlap = Math.round ( (100-(distance*100/2.45)) * 10 ) / 10;
						}
                        if(isNaN(finaloverlap))
                            {
                                finaloverlap=0;
                            }
			 objs.push({
				   'sport':value.sport,
				   'finaloverlap':finaloverlap
					});
		    }
  }); 
    
    
    
    console.log(objs);
    
    
     objs.sort(compare);
	 $.each(objs,function(key,value){
			htmlTable += '<tr>'+
			'<td align="left">'+value.sport+'</td>'+
			'<td align="right">'+value.finaloverlap+'</td>'+
              '</tr>';
	}); 
	 
	 $('.body_sports_table').html(htmlTable);	
	} 
  function compare(a,b) {
  if (a.finaloverlap > b.finaloverlap)
    return -1;
  if (a.finaloverlap < b.finaloverlap)
    return 1;
  return 0;
   }

  function normalcdf(x){ 
    var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
	return Prob ;
    } 
      
    $(document).on('click','.virtual_btnrestricted', function(){
    $("#vprestrictedgen").attr("action", "<?php echo base_url(); ?>index.php/Body/BCVirtualpersonGeneration");     
        $("#vprestrictedgen").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
       $(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 
      
      
      
 </script>  
 
 
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script> 
 
</body>
</html>
