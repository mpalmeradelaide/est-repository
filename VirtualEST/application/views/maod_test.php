<?php 
//To show Hide Additional Info
         if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
            $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
            $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
            $bodyfatdiv="block";    
            }
            else
            {
             $bodyfatdiv="none";    
            }
 $is_dynamic=$_SESSION['is_virtual'];
 
            ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script src='https://d3js.org/d3.v3.min.js'></script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>chart.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>frame.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>graph.css">

<script type="text/javascript">	
	window.onload = function() {
         var is_dynamic='<?php echo $is_dynamic;?>';
		  $("#bodymass").val('<?php echo $_SESSION['MASS'];?>');
         $("#name").val('<?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?>');
         var vo2max='<?php echo $_SESSION['VO2max'];?>';
		 var gender='<?php echo $_SESSION['gender'];?>';
       if(gender==='Male' || gender==='M')   
      $("#male").prop("checked", true);
       else
       $("#female").prop("checked", true);    
		 
		 if(is_dynamic == 1)
	    {
		
       if(vo2max < 20)
       {
      alert('A MAOD test is NOT recommended for this person due to poor fitness, age or both.');
       }
       else
       {
       $("#stage1_workload").val('<?php echo $_SESSION['stage1_workload'];?>');
       $("#stage2_workload").val('<?php echo $_SESSION['stage2_workload'];?>');
       $("#stage3_workload").val('<?php echo $_SESSION['stage3_workload'];?>');
       $("#stage4_workload").val('<?php echo $_SESSION['stage4_workload'];?>');
       $("#stage5_workload").val('<?php echo $_SESSION['stage5_workload'];?>');
       $("#stage6_workload").val('<?php echo $_SESSION['stage6_workload'];?>');
       $("#stage7_workload").val('<?php echo $_SESSION['stage7_workload'];?>');
       $("#stage8_workload").val('<?php echo $_SESSION['stage8_workload'];?>');
       $("#stage9_workload").val('<?php echo $_SESSION['stage9_workload'];?>');
       $("#stage10_workload").val('<?php echo $_SESSION['stage10_workload'];?>');
       
       $("#stage1_maod").val('<?php echo $_SESSION['stage1_maod'];?>');
       $("#stage2_maod").val('<?php echo $_SESSION['stage2_maod'];?>');
       $("#stage3_maod").val('<?php echo $_SESSION['stage3_maod'];?>');
       $("#stage4_maod").val('<?php echo $_SESSION['stage4_maod'];?>');
       $("#stage5_maod").val('<?php echo $_SESSION['stage5_maod'];?>');
       $("#stage6_maod").val('<?php echo $_SESSION['stage6_maod'];?>');
       $("#stage7_maod").val('<?php echo $_SESSION['stage7_maod'];?>');
       $("#stage8_maod").val('<?php echo $_SESSION['stage8_maod'];?>');
       $("#stage9_maod").val('<?php echo $_SESSION['stage9_maod'];?>');
       $("#stage10_maod").val('<?php echo $_SESSION['stage10_maod'];?>');
      }
		}	
	  };
       	
	$(document).ready(function() {
		
		var is_dynamic='<?php echo $is_dynamic;?>';
	    var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		
		if(is_dynamic == 1)
	    { 
		$(".v_person").fadeTo( "slow" , 1, function() {});
		  $(".v_detail").toggle();
	    }

		$("#nxtbtn").click(function(){
		$("#Vmax").val('<?php echo $_SESSION['VO2maxLM'];?>');
		reqSpeed();
            
		$("#stage1_vo2").val('<?php echo $_SESSION['stage1_vo2'];?>'); 
        $("#stage2_vo2").val('<?php echo $_SESSION['stage2_vo2'];?>');
        $("#stage3_vo2").val('<?php echo $_SESSION['stage3_vo2'];?>');
        $("#stage4_vo2").val('<?php echo $_SESSION['stage4_vo2'];?>');
        $("#stage5_vo2").val('<?php echo $_SESSION['stage5_vo2'];?>');
        $("#stage6_vo2").val('<?php echo $_SESSION['stage6_vo2'];?>');
        $("#stage7_vo2").val('<?php echo $_SESSION['stage7_vo2'];?>');
        $("#stage8_vo2").val('<?php echo $_SESSION['stage8_vo2'];?>'); 
        $("#stage9_vo2").val('<?php echo $_SESSION['stage9_vo2'];?>');
        $("#stage10_vo2").val('<?php echo $_SESSION['stage10_vo2'];?>');
        $("#stage11_vo2").val('<?php echo $_SESSION['stage11_vo2'];?>');
        $("#stage12_vo2").val('<?php echo $_SESSION['stage12_vo2'];?>');
        $("#stage13_vo2").val('<?php echo $_SESSION['stage13_vo2'];?>');
        $("#stage14_vo2").val('<?php echo $_SESSION['stage14_vo2'];?>');
        $("#stage15_vo2").val('<?php echo $_SESSION['stage15_vo2'];?>'); 
        $("#stage16_vo2").val('<?php echo $_SESSION['stage16_vo2'];?>');
        $("#stage17_vo2").val('<?php echo $_SESSION['stage17_vo2'];?>');
        $("#stage18_vo2").val('<?php echo $_SESSION['stage18_vo2'];?>');
	
		var val = $("#tints").val();
        adjInterval(val);
				  
        }); 
    });
	$(document).on('click','.vitual_btn', function(){
    $("#vpmaodtest").attr("action", "<?php echo base_url(); ?>index.php/Fitness/FitnessVirtualpersonGeneration");     
        $("#vpmaodtest").submit();
	$(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
		window.location.href = "<?php echo site_url('welcome/destroy_VP');?>";
	});
        $(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
       });   
	   
	   
	   $(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});

	// toggle info box
		
	$(document).on('click', '.info_block_new_btn', toggle_info_overlay() ); 
	$(document).on('click', '.close', toggle_info_overlay() );

	function toggle_info_overlay() {
		$(".info_block").toggle();
		$(".overlay").toggle();
	}

</script>
</head>

<body class="view_maod_test">

	<!-- insertion of header -->
	<?=$header_insert; ?>

<div class="overlay">&nbsp;</div> 
        
<div class="info_block">
	<div class="info_block_head green_container">Maximal Accumulated Oxygen Deficit [MAOD]</div>
	<p>
	A method of estimating the maximal anaerobic capacity is to use the method proposed by Medbø and colleagues in the 1980’s.The method is known as the oxygen deficit method. The maximal accumulated oxygen deficit [MAOD] is defined as the difference between the energy-demand of an exhausting bout of supramaximal exercise [usually expressed as an oxygen equivalent] and the oxygen uptake during the exercise. The Medbø MAOD method involves the following steps:
	</p>
	<br />
	<p>
	[1] a regression equation between measured oxygen uptake and power output [workrate] is established from a number of sub-anaerobic threshold work rates. This is done by having the athlete work at a number of steady-state submaximal workrates, and generating a line of best fit to the data. The users needs to enter the work stages [in km per hour or power output in Watts in the appropriate fields] and then click ‘plot’. The regression between work rate [speed or power] and VO2 is established on screen one of the MAOD test. The energy demand is in oxygen equivalents and can be determined for any level of external work, for example, running speed or power on a bike using the regression equation established [and assuming a linear relationship even up to supramaximal intensity levels].
	</p>
	<br />
	<p>
	[2] the subject then undergoes exercise to exhaustion at a supramaximal workrate, usually ~110 -125% of VO2max. The regression line established on screen one is used automatically to predict the energy demand of exercise [in oxygen-equivalents] at the chosen supramaximal workrate. The supramaximal exercise intensity should be estimated to result in exhaustion within about 70 – 180 seconds. During the supramaximal exercise VO2 is measured in intervals of between 10 – 30 s [preferably 10 s]. The measured VO2 values are entered on screen two of the MAOD test. The oxygen deficit is then determined as the total oxygen demand less the total measured oxygen uptake. It is reported as mL O2 equivalents / kg body mass. Typical values for elite athletes are also displayed and are based on published scientific reports.
	</p>
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>  
</div>     

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform'); 
echo form_open('Fitness/saveClientVerticalJumpInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?> 
	
    <div class="contain">

        <!--Start right --> 
       <!--Start right, Screen 1 -->
            <div id="screen1" style="display: inline" class="right-section right-section_new">
                <div class="right-head">Maximal Accumulated Oxygen Deficit [MAOD]</div>
                <div id="input-vals">
                    
                    <!-- Inputs for Bodymass, Name, and Sex -->
                    <label for="bodymass"><strong>Body mass (kg)</strong></label>
                    <label for="name" style="margin-left: 180px"><strong>Name</strong></label>
                    <label for="" style="margin-left: 225px;"><strong>Gender</strong></label>
                    <br />
                    <br />
                    <input type="text" id="bodymass" style="width: 190px;" tabindex=1 maxlength=6 required autofocus oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />
                    <input type="text" id="name" style="width: 190px; margin-left: 30px; margin-right: 20px;" tabindex=2 ng-model="yourName" />

                    <div style="display: inline;">
                        <input type="radio" style="display: none;" id="male" name="sex" value="male"><label for="male"><span style="float: none; margin-right: 8px;"></span><strong>Male</strong></label>
                        <input type="radio" style="display: none;" id="female" name="sex" value="female"><label for="female"><span style="float: none; margin-right: 8px; margin-left: 5px;"></span><strong>Female</strong></label>
                    </div>

                </div>

                <br>
                
                <!-- Input table for Workload and VO2max data pairs, plotted in firstGraph -->
                <div class="field_row" style="border-bottom: none">
                    <ul class="field_24" id="workload" style="position: absolute; margin-top: 10px;">
                        <label style="margin-bottom: 0; padding-bottom: 21px; text-align: center"><strong>Workload</strong></label>

                        <li><input class="x" id="stage1_workload" type="text" tabindex=2 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage2_workload" type="text" tabindex=4 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage3_workload" type="text" tabindex=6 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage4_workload" type="text" tabindex=8 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage5_workload" type="text" tabindex=10 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage6_workload" type="text" tabindex=12 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage7_workload" type="text" tabindex=14 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage8_workload" type="text" tabindex=16 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage9_workload" type="text" tabindex=18 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="stage10_workload" type="text" tabindex=20 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="" type="text" tabindex=22 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="x" id="" type="text" tabindex=24 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li></li>
                        <li><input type="radio" style="display: none" name="workloadUnits" id="power" value="power" checked="true"><label for="power" style="margin-top: 10px;"><span style="float: none; margin-right: 8px;"></span><strong>Watts</strong></label></li>
                        <li><input type="radio" style="display: none" name="workloadUnits" id="speed" value="speed"><label for="speed"><span style="float: none; margin-right: 8px;"></span><strong>kph</strong></label></li>
                    </ul>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <ul id="vo2" class="field_24" style="position: absolute; margin-left: 80px; margin-top: 10px;">
                        <label style="margin: 0; padding: 0; text-align: center">
                            <strong>
                                VO<sub>2</sub><br />
                                (L/min)
                            </strong>
                        </label>
                        <li><input class="y" id="stage1_maod" type="text" tabindex=3 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage2_maod" type="text" tabindex=5 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage3_maod" type="text" tabindex=7 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage4_maod" type="text" tabindex=9 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage5_maod" type="text" tabindex=11 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage6_maod" type="text" tabindex=13 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage7_maod" type="text" tabindex=15 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage8_maod" type="text" tabindex=17 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage9_maod" type="text" tabindex=19 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="stage10_maod" type="text" tabindex=21 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="" type="text" tabindex=23 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y" id="" type="text" tabindex=25 oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                    </ul>

                    <!-- SVG graph element using D3 to create and plot graph, and axis labels -->
                    <div id="results" style="text-align: center; margin-right: 130px; margin-top: -20px; opacity: 0.0"><strong><div style="margin-left: -73px;">Y = </div></strong><div style="margin-left: -120px;"><strong>R&sup2;= </strong></div></div>
                    <p id="yAxisLabel" class="axis text" style="position: absolute; margin-left: 210px; margin-top: 330px; transform: rotate(270deg);"></p>
                    <svg id="graphS1" width="700" height="700" style="margin-left: 250px; margin-top: -%5;"></svg>
                    <p id="xAxisLabel" class="axis text" style="text-align: center; margin-left: 345px; opacity: 0.0;">Workload (kph)</p>
                    <br />  
                    
                    <!-- Navigation buttons for plotting graph, clearing data, and moving to next screen -->
                    <input class="lite_btn btn_white screen1-buttons" onclick="s1Input()" name="plot" value="Plot" type="button" style="position: absolute; margin-top: 40px; margin-left: 0px; width: 40px" />
                    <input class="lite_btn btn_white screen1-buttons" onclick="clearGraph('graphS1', true)" id="clearbtn" name="clear" value="Clear" type="button" style="position: absolute; margin-top: 40px; margin-left: 350px; width: 45px; visibility: hidden;" />
                    <input class="lite_btn btn_gray screen1-buttons" id="nxtbtn" name="move" onclick="changeScreens()" value="Next Screen" type="button" style="position: absolute; margin-top: 40px; margin-left: 738px; width: 140px; visibility: hidden;" />

                </div>
            </div>
            <div><br /></div>
            
            <!-- Screen 2 -->
            <div id="screen2" name="screen2" style="display: none" class="right-section right-section_new">
                <div class="right-head">Maximal Accumulated Oxygen Deficit [MAOD]</div>
                
                <!-- Input values for Time Intervals (drop-down list), Estimated VO2max, and Supramaximal Workrate -->
                <div id="input-vals">

                    <div style="display: inline">
                        <label for="value"><strong>Time interval (s)</strong></label>
                        <label for="Vmax" style="margin-left: 48px"><strong>Estimated V0<sub>2max</sub> (L/min)</strong></label>
                        <label for="supramaximal" style="margin-left: 48px"><strong>Supramaximal workrate (%max)</strong></label>
                        <label for="tints" style="margin-left: 45px" id="reqWUnits"><p id=""></p></label>
                    </div>

                    <br />
                    <br />
                    <select name="tints" id="tints" style="width: 110px; float: left;" onkeypress="adjIntervalTable(this)" onkeydown="adjIntervalTable(this)" onChange="adjIntervalTable(this)">
                        <option value="5">5</option>
                        <option value="10" selected="true">10</option>
                        <option value="15">15</option>
                        <option value="20">20</option>
                        <option value="30">30</option>
                        <option value="45">45</option>
                        <option value="60">60</option>
                    </select>
                    <input type="text" id="timeInterval" required style="width: 40px; visibility: hidden; display: none;" maxlength="3" onkeypress="adjIntervalTable(this)" onkeydown="adjIntervalTable(this)" onChange="adjIntervalTable(this)" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" />


					<input type="text" id="Vmax" style="width: 110px; margin-left: 43px" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="reqSpeed()"/>
					<input type="text" id="supramaximal" value="115" style="width: 150px; margin-left: 40px;" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" onkeyup="reqSpeed()" />

                    <input type="text" disabled id="reqworkload" style="width: 150px; display: inline-block; margin-left: 42px;" />

                </div>

                <br />

                <!-- Input table for VO2max, plotted in secondGraph -->
                <div class="field_row" style="border-bottom: none;">
                    <ul class="field_24" id="interval" style="position: absolute; padding-bottom: 10px; margin-top: 20px;">
                        <label style="margin: 0; padding: 0; text-align: center;"><strong>Interval</strong></label>
                        <label style="margin: 0; padding: 0; text-align: center;"><strong>(s)</strong></label>
                        <li><input disabled class="x2" value="10" /></li>
                        <li><input disabled class="x2" value="20" /></li>
                        <li><input disabled class="x2" value="30" /></li>
                        <li><input disabled class="x2" value="40" /></li>
                        <li><input disabled class="x2" value="50" /></li>
                        <li><input disabled class="x2" value="60" /></li>
                        <li><input disabled class="x2" value="70" /></li>
                        <li><input disabled class="x2" value="80" /></li>
                        <li><input disabled class="x2" value="90" /></li>
                        <li><input disabled class="x2" value="100" /></li>
                        <li><input disabled class="x2" value="110" /></li>
                        <li><input disabled class="x2" value="120" /></li>
                        <li><input disabled class="x2" value="130" /></li>
                        <li><input disabled class="x2" value="140" /></li>
                        <li><input disabled class="x2" value="150" /></li>
                        <li><input disabled class="x2" value="160" /></li>
                        <li><input disabled class="x2" value="170" /></li>
                        <li><input disabled class="x2" value="180" /></li>
                    </ul>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <ul id="vo2" class="field_24" style="position: absolute; margin-left: 80px; padding-bottom: 10px; margin-top: 20px;">
                        <label style="margin: 0; padding: 0; text-align: center;"><strong>V0<sub>2</sub></strong></label>
                        <label style="margin: 0; padding: 0; text-align: center;"><strong>(L/min)</strong></label>
                        <li><input class="y2" id="stage1_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage2_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage3_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage4_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage5_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage6_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage7_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage8_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage9_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage10_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage11_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage12_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage13_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage14_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2"  id="stage15_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage16_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage17_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                        <li><input class="y2" id="stage18_vo2" oninput="this.value = this.value.replace(/[^0-9.]/g, '').replace(/(\..*)\./g, '$1');" /></li>
                    </ul>

                    <!-- SVG graph element using D3 to create and plot graph, and axis labels -->                    
                    <div id="results2" style="text-align: center; background-color:#a5e2c5; margin-left: 300px; margin-top: -30px; margin-right: -35px; padding: 20px; opacity: 0.0; font-size: 25px;">Result</div>
                    <div id="percent" style="position: absolute; text-align: center; margin-left: 968px; margin-top: -40px; font-size: 25px; opacity: 0.0;">99.9%</div>
					<div id="areaTotals"></div>
					
                    <p id="yAxisLabel3" class="text" style="position: absolute; margin-left: 910px; margin-top: 340px; transform: rotate(270deg);"></p>
                    <svg id="graphS3" width="200" height="700" style="position: absolute; margin-left: 900px;"></svg>

                    <p id="yAxisLabel2" class="text" style="position: absolute; margin-left: 200px; margin-top: 340px; transform: rotate(270deg);"></p>
                    <svg id="graphS2" width="700" height="700" style="position: relative; margin-left: 250px; "></svg>
                    <p id="xAxisLabel2" class="axis text" style="text-align: center; margin-left: 345px; padding-bottom: 220px; opacity: 0.0;">Graph</p>
                </div>
                
                <!-- Navigation buttons for plotting graph, clearing data, and moving to previous screen -->
                <div style="position: relative;">
                    <input class="lite_btn btn_white plotscreen2" onclick="s2Input()" name="plot" value="Plot" type="button" style="position: absolute; width: 40px;" />
                    <input class="lite_btn btn_white clearscreen2" onclick="clearGraph('graphS2', true)" name="clear" value="Clear" type="button" style="position: absolute; margin-left: 350px;   width: 45px" />
                    <input class="lite_btn btn_white prevscreen2" id="move" name="move" onclick="changeScreens()" value="Next Screen" type="button" style="position: absolute; margin-left: 738px; width: 160px;" />
                </div>
            </div>

    </div>
    
</div>

<?php echo form_close(); ?>
<!-- Form ends -->	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>
	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>


<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
<script src="<?php echo base_url(); ?>assets/js/jquery.ui.touch-punch.min.js"></script>
   
</body>
</html>
