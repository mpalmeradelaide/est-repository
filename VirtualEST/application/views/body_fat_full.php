<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Result Screen
    </title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">
    </script>
    <link rel="stylesheet" type="text/css"   href="<?php
echo "$base/$css";
?>">
    <link rel="stylesheet" type="text/css" href="<?php
echo "$base/assets/css/";
?>style.css">
    <style>
      sub {
        font-size: 75%;
        line-height: 0;
        position: relative;
        vertical-align: baseline;
      }
      sub {
        bottom: -0.25em;
      }
    </style>
    <script type="text/javascript">
      $(document).ready(function() {
        var divHeight = $('.contain').height();
        $('.side_menu').css('height', divHeight+'px');
      }
                       );
      $(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
        $(".sub_menu").hide();
        $(this).next(".sub_menu").toggle().animate({
          left: '274px', opacity:'1'}
                                                  );
      }
                    );
      $(document).on('click','#VO2max', function(){
        $(".inner_sub_menu").slideUp();
        $(this).next(".inner_sub_menu").toggle().animate({
          left: '274px', opacity:'1'}
                                                        );
      }
                    );
      $(document).on('click','.menu_btn', function(){
        $(this).toggleClass("active");
        $(".drop_main").toggle().animate({
          left: '76px', opacity:'1'}
                                        );
      }
                    );
      $(document).on('click','.info_icon_btn', function(){
        $(".info_block").toggle();
        $(".overlay").toggle();
      }
                    );
      $(document).on('click','.close', function(){
        $(".info_block").toggle();
        $(".overlay").toggle();
      }
                    );
    </script>
  </head> 
  <body>

	<!-- insertion of header -->
	<?= $header_insert; ?>

	<div class="overlay">&nbsp;</div> 
	<div class="info_block">
          <div class="info_block_head violet_container">Body Composition / % Body Fat
          </div>
          <p> The % body fat screen is a tool that allows the automatic calculation of % body fat from the
            anthropometry data in either the restricted or full profiles. It has 7 % body fat prediction
            equations for men and 11 for women when using the restricted profile data.
            For full anthropometric profiles there are 7 equations for men and 14 for women.
            Also provided are the overall mean % body fat ± SD values as well as the % body fat range
            that is common to all the prediction equations. The individual predicted % body fat values
            are plotted on a graph together with the overall mean ± SD. A separate graph has a large
            number of % body fat values obtained from elite athletes in a range of sports.
          </p>    
          <div class="info_block_foot">
            <a href="#" class="lite_btn grey_btn f_right close">Close
            </a>
          </div>                
        </div> 	
    <div class="wrapper">
      <!-- Form begins -->    
      <?php
$hidden = array(
				'userid' => $id
);
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Body/skinfold_actions', array('id'=>'myform','name'=>'myform'), $hidden); 
?> 
      <?php
$attributes = array(
				'id' => 'myform',
				'name' => 'myform'
);
echo form_open('Body/saveBodyFat', $attributes);
?>
      <div class="contain">
        <!--Start right --> 
        <div class="right-section right-section_new">
          <!-- graph is here -->
          <div class="bfc-graph-wrapper">
            <div class="half_container f_left">
              <h3>your profile
              </h3>
              <div class="population_div"> 
                <div id="chartdiv">&nbsp;
                </div>
              </div>
              <div class="user_prof_txt">The range of % body fat values common to all prediction equations is between 
                <span id="max_Lower" style="color:#ef3832; margin:0; float:none;">
                </span> and 
                <span id="min_Upper" style="color:#ef3832; margin:0; float:none;">
                </span>%
              </div>
            </div> 
            <div class="half_container f_right">
              <div class="points_div">
                <ul>
                  <li>40
                  </li> 
                  <li>35
                  </li> 
                  <li>30
                  </li> 
                  <li>25 
                  </li> 
                  <li>20
                  </li> 
                  <li>15
                  </li> 
                  <li>10
                  </li> 
                  <li>5
                  </li> 
                  <li>0
                  </li>
                </ul>
              </div>                    
              <div class="sport_group_main">
                <h3>sport groups
                </h3>  
                <div class="population_div"> 
                  <div id="sport_group_div">
                  </div> 
                </div>
                <label id="sportData">
                </label>
              </div>
            </div>
          </div>
          <!-- end graph --> 
          <div class="right-head rh-block">% Body Fat
          </div>
          <div class="field_row verticle_field_row"> 
            <div class="user_prof_val_div_cont">
              <h3>your profile
              </h3>
              <table width="100%" class="tabel_bord tabel_bord_sml body_tble" cellspacing="0" style="width: 310px; border:0;">
                <thead>
                  <tr>
                    <td>Equation
                    </td>
                    <td>% body fat
                    </td>
                  </tr>
                </thead>
                <tbody>
                  <?php
if ($_SESSION['user_gender'] == "M") {
?>                             
                  <tr>
                    <td id="eqn1">Withers 1987
                    </td>
                    <td id="perBF1">
                    </td> 
                  </tr> 
                  <tr>
                    <td id="eqn2">Wilmore & Behnke 1969
                    </td>
                    <td id="perBF2">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn3">Sloan 1967
                    </td>
                    <td id="perBF3">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn4">Katch & McArdle 1973
                    </td>
                    <td id="perBF4">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn5">Durnin & Womersley 1974
                    </td>
                    <td id="perBF5">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn6">Thorland 1984
                    </td>
                    <td id="perBF6">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn7">Forsyth & Sinning 1973
                    </td>
                    <td id="perBF7">
                    </td> 
                  </tr>                             
                  <?php
} else {
?>                            
                  <tr>
                    <td id="eqn1">Withers 1987
                    </td>
                    <td id="perBF1">
                    </td> 
                  </tr> 
                  <tr>
                    <td id="eqn2">Withers 1987
                    </td>
                    <td id="perBF2">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn3">Wilmore & Behnke 1970
                    </td>
                    <td id="perBF3">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn4">Thorland 1984
                    </td>
                    <td id="perBF4">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn5">Sloan 1962
                    </td>
                    <td id="perBF5">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn6">Jackson 1980
                    </td>
                    <td id="perBF6">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn7">Jackson 1980
                    </td>
                    <td id="perBF7">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn8">Durnin & Womersley 1974
                    </td>
                    <td id="perBF8">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn9">Pollock 1975
                    </td>
                    <td id="perBF9">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn10">Katch & Michael  1968
                    </td>
                    <td id="perBF10">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn11">Lewis 1978
                    </td>
                    <td id="perBF11">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn12">Katch & McArdle 1973
                    </td>
                    <td id="perBF12">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn13">Withers 1987
                    </td>
                    <td id="perBF13">
                    </td> 
                  </tr>
                  <tr>
                    <td id="eqn14">Pollock 1975
                    </td>
                    <td id="perBF14">
                    </td> 
                  </tr>
                  <?php
}
?>  
                  <tr>
                    <td colspan="2">
                      <div style="color:#c60003;" id="meanBF">
                      </div>
                    </td>
                  </tr>
                </tbody>
              </table>   
            </div>
          </div>
          <input type="hidden" id="age" name="age" value="<?php
echo $_SESSION['age'];
?>"> 
          <input type="hidden" id="height" name="height" value="<?php
echo isset($bodyFat_values["height"]) ? $bodyFat_values["height"] : "";
?>"> 
          <input type="hidden" id="gender" name="gender" value="<?php
if ($_SESSION['user_gender'] == "M") {
				echo "Male";
} else {
				echo "Female";
}
?>">   
          <div class="field_row" style="border:0;">
          </div>           
        </div>    
        <div style="display:none;">
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/phantom.png" alt="">Phantom
          </div>
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/body_fat.png" alt="">% body fat
          </div>
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/skinfolds.png" alt="">Skinfolds
          </div>
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/somatotype.png" alt="">Somatotype
          </div>
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/norms.png" alt="">Norms
          </div>
          <div class="bottom_icons" onclick="window.location.href = '<?php
echo site_url('Body/full_profile');
?>';">
            <img src="<?php
echo "$base/$image";
?>/fractionation.png" alt="">Fractionation
          </div>
        </div>
      </div>
      <input type="hidden" id="triceps" name="triceps" value="<?php
echo isset($bodyFat_values["triceps"]) ? $bodyFat_values["triceps"] : "";
?>"> 
      <input type="hidden" id="subscapular" name="subscapular" value="<?php
echo isset($bodyFat_values["subscapular"]) ? $bodyFat_values["subscapular"] : "";
?>"> 
      <input type="hidden" id="biceps" name="biceps" value="<?php
echo isset($bodyFat_values["biceps"]) ? $bodyFat_values["biceps"] : "";
?>"> 
      <input type="hidden" id="iliac_crest" name="iliac_crest" value="<?php
echo isset($bodyFat_values["iliac_crest"]) ? $bodyFat_values["iliac_crest"] : "";
?>"> 
      <input type="hidden" id="supraspinale" name="supraspinale" value="<?php
echo isset($bodyFat_values["supraspinale"]) ? $bodyFat_values["supraspinale"] : "";
?>"> 
      <input type="hidden" id="abdominal" name="abdominal" value="<?php
echo isset($bodyFat_values["abdominal"]) ? $bodyFat_values["abdominal"] : "";
?>"> 
      <input type="hidden" id="thigh" name="thigh" value="<?php
echo isset($bodyFat_values["thigh"]) ? $bodyFat_values["thigh"] : "";
?>"> 
      <input type="hidden" id="calf" name="calf" value="<?php
echo isset($bodyFat_values["calf"]) ? $bodyFat_values["calf"] : "";
?>"> 
      <input type="hidden" id="mid_axilla" name="mid_axilla" value="<?php
echo isset($bodyFat_values["mid_axilla"]) ? $bodyFat_values["mid_axilla"] : "";
?>"> 
      <input type="hidden" id="relArmG" name="relArmG" value="<?php
echo isset($bodyFat_values["relArmG"]) ? $bodyFat_values["relArmG"] : "";
?>"> 
      <input type="hidden" id="flexArmG" name="flexArmG" value="<?php
echo isset($bodyFat_values["flexArmG"]) ? $bodyFat_values["flexArmG"] : "";
?>"> 
      <input type="hidden" id="forearmG" name="forearmG" value="<?php
echo isset($bodyFat_values["forearmG"]) ? $bodyFat_values["forearmG"] : "";
?>">
      <input type="hidden" id="wristG" name="wristG" value="<?php
echo isset($bodyFat_values["wristG"]) ? $bodyFat_values["wristG"] : "";
?>"> 
      <input type="hidden" id="waistG" name="waistG" value="<?php
echo isset($bodyFat_values["waistG"]) ? $bodyFat_values["waistG"] : "";
?>"> 
      <input type="hidden" id="hipG" name="hipG" value="<?php
echo isset($bodyFat_values["hipG"]) ? $bodyFat_values["hipG"] : "";
?>"> 
      <input type="hidden" id="thighG" name="thighG" value="<?php
echo isset($bodyFat_values["thighG"]) ? $bodyFat_values["thighG"] : "";
?>"> 
      <input type="hidden" id="calfG" name="calfG" value="<?php
echo isset($bodyFat_values["calfG"]) ? $bodyFat_values["calfG"] : "";
?>"> 
      <input type="hidden" id="humerus" name="humerus" value="<?php
echo isset($bodyFat_values["humerus"]) ? $bodyFat_values["humerus"] : "";
?>"> 
      <input type="hidden" id="femur" name="femur" value="<?php
echo isset($bodyFat_values["femur"]) ? $bodyFat_values["femur"] : "";
?>">         
      <input type="hidden" id="exit_key" name="exit_key" value="">
      <input type="hidden" id="action_key" name="action_key" value="">
      <input type="hidden" id="meanbodyfat" name="meanbodyfat" value="">
      <input type="hidden" id="sdbodyfat" name="sdbodyfat" value="">
      <?php
echo form_close();
?>
      <!-- Form ends -->
    </div>
    <div class="footer" style="display: none;"> 
      <div class="wrapper">
        <p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton
        </p>
      </div> 
    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js">
    </script>
    <script type="text/javascript">
      $(document).on('click','.print_icon_toggle_btn', function(){
        $(".print_icon_toggle").toggle();
      }
                    );
      $(document).on('click','#exit', function(){
        document.getElementById("exit_key").value = 1 ;
        document.forms["myform"].submit();
        //return false;
      }
                    );
      $(document).on('click','#show_hide_table', function(){
        $(this).text(function(i, v){
          return v === 'Show Table' ? 'Hide Table' : 'Show Table'
        }
                    );
        $(".population_div_cont").toggle();
        $(".user_prof_val_div_cont").toggle();
      }
                    );
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>jquery.jqplot.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>plugins/jqplot.canvasTextRenderer.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>plugins/jqplot.canvasAxisLabelRenderer.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>plugins/jqplot.canvasAxisTickRenderer.min.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>plugins/jqplot.categoryAxisRenderer.min.js">
    </script>
    <script type="text/javascript" src="<?php
echo "$base/assets/dist/";
?>plugins/jqplot.highlighter.js">
    </script>
    <link rel="stylesheet" type="text/css" src="<?php
echo "$base/assets/dist/";
?>jquery.jqplot.css" />
    <script>   
      $(window).bind("load", function() {
        var age = document.getElementById("age").value ;
        var ht = document.getElementById("height").value ;
        var triceps = document.getElementById("triceps").value ;
        var subscapular = document.getElementById("subscapular").value ;
        var biceps = document.getElementById("biceps").value ;
        var iliac = document.getElementById("iliac_crest").value ;
        var supraspinale = document.getElementById("supraspinale").value ;
        var abdominal = document.getElementById("abdominal").value ;
        var thigh = document.getElementById("thigh").value ;
        var calf = document.getElementById("calf").value ;
        var mid_axilla = document.getElementById("mid_axilla").value ;
        var relArmG = document.getElementById("relArmG").value ;
        var flexArmG = document.getElementById("flexArmG").value ;
        var forearmG = document.getElementById("forearmG").value ;
        var wristG = document.getElementById("wristG").value ;
        var waistG = document.getElementById("waistG").value ;
        var hipG = document.getElementById("hipG").value ;
        var thighG = document.getElementById("thighG").value ;
        var calfG = document.getElementById("calfG").value ;
        var humerus = document.getElementById("humerus").value ;
        var femur = document.getElementById("femur").value ;
        if(document.getElementById("gender").value == "Male")
        {
          var n = 0 ;
          // Toatal number of %BF  
          var totalBF = 0 ;
          // Total %BF
          var sumSquares = 0 ;
          // Sum of squares of %BF
          if(triceps !== "" && subscapular !== "" && biceps !== "" && supraspinale !== "" && abdominal !== "" && thigh !== "" && calf !== "") 
          {
            var BDa = 1.0988 - 0.0004 * (parseFloat(triceps) + parseFloat(subscapular) + parseFloat(biceps) + parseFloat(supraspinale) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf)) ;
            var per_BFa = 495 / BDa - 450 ;
            // %Body Fat 1 
            var lower_BFa = Math.round((per_BFa - 4.8) * 10) / 10 ;
            // Lower Limit 1  
            var upper_BFa = Math.round((per_BFa + 4.8) * 10) / 10 ;
            // Upper Limit 1  
            totalBF = totalBF + parseFloat(per_BFa) ;
            sumSquares = sumSquares + (parseFloat(per_BFa) * parseFloat(per_BFa)) ;
            n = n+1 ;
          }
          if(abdominal !== "" && thigh !== "") 
          {
            var BDb = 1.08543 - 0.000886 * (parseFloat(abdominal)) - 0.0004 * (parseFloat(thigh)) ;
            var per_BFb = 495 / BDb - 450 ;
            // %Body Fat 2 
            var lower_BFb = Math.round((per_BFb - 6.6) * 10) / 10 ;
            // Lower Limit 2  
            var upper_BFb = Math.round((per_BFb + 6.6) * 10) / 10 ;
            // Upper Limit 2  
            totalBF = totalBF + parseFloat(per_BFb) ;
            sumSquares = sumSquares + (parseFloat(per_BFb) * parseFloat(per_BFb)) ;
            n = n+1 ;
          }
          if(subscapular !== "" && thigh !== "") 
          {
            var BDc = 1.1043 - 0.001327 * parseFloat(thigh) - 0.00131 * parseFloat(subscapular) ;
            var per_BFc = 495 / BDc - 450 ;
            // %Body Fat 3  
            var lower_BFc = Math.round((per_BFc - 5.1) * 10) / 10 ;
            // Lower Limit 3  
            var upper_BFc = Math.round((per_BFc + 5.1) * 10) / 10 ;
            // Upper Limit 3
            totalBF = totalBF + parseFloat(per_BFc) ;
            sumSquares = sumSquares + (parseFloat(per_BFc) * parseFloat(per_BFc)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && abdominal !== "") 
          {
            var BDd = 1.09665 - 0.00103 * parseFloat(triceps) - 0.00056 * parseFloat(subscapular) - 0.00054 * parseFloat(abdominal) ;
            var per_BFd = 495 / BDd - 450 ;
            // %Body Fat 4
            var lower_BFd = Math.round((per_BFd - 6.2) * 10) / 10 ;
            // Lower Limit 4  
            var upper_BFd = Math.round((per_BFd + 6.2) * 10) / 10 ;
            // Upper Limit 4
            totalBF = totalBF + parseFloat(per_BFd) ;
            sumSquares = sumSquares + (parseFloat(per_BFd) * parseFloat(per_BFd)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && biceps !== "" && iliac !== "") 
          {
            var BDe = 1.1765 - 0.0744 * Math.log10(parseFloat(triceps) + parseFloat(biceps) + parseFloat(subscapular) + parseFloat(iliac))  ;
            var per_BFe = 495 / BDe - 450 ;
            // %Body Fat 5
            var lower_BFe = Math.round((per_BFe - 9.1) * 10) / 10 ;
            // Lower Limit 5  
            var upper_BFe = Math.round((per_BFe + 9.1) * 10) / 10 ;
            // Upper Limit 5 
            totalBF = totalBF + parseFloat(per_BFe) ;
            sumSquares = sumSquares + (parseFloat(per_BFe) * parseFloat(per_BFe)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && iliac !== "" && abdominal !== "" && thigh !== "" && calf !== "" && mid_axilla !== "") 
          {
            var BDf = 1.1091 - 0.00052 * (parseFloat(triceps) + parseFloat(subscapular) + parseFloat(mid_axilla) + parseFloat(iliac) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf)) 
            + 0.00000032 * [(parseFloat(triceps) + parseFloat(subscapular) + parseFloat(mid_axilla) + parseFloat(iliac) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf)) * (parseFloat(triceps) + parseFloat(subscapular) + parseFloat(mid_axilla) + parseFloat(iliac) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf))]  ;
            var per_BFf = 495 / BDf - 450 ;
            // %Body Fat 6
            var lower_BFf = Math.round((per_BFf - 4.6) * 10) / 10 ;
            // Lower Limit 6  
            var upper_BFf = Math.round((per_BFf + 4.6) * 10) / 10 ;
            // Upper Limit 6  
            totalBF = totalBF + parseFloat(per_BFf) ;
            sumSquares = sumSquares + (parseFloat(per_BFf) * parseFloat(per_BFf)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && abdominal !== "" && mid_axilla !== "") 
          {
            var BDg = 1.10647 - 0.00162 * parseFloat(subscapular) - 0.00144 * parseFloat(abdominal) - 0.00077 * parseFloat(triceps) + 0.00071 * parseFloat(mid_axilla) ;
            var per_BFg = 495 / BDg - 450 ;
            // %Body Fat 7
            var lower_BFg = Math.round((per_BFg - 5) * 10) / 10 ;
            // Lower Limit 7  
            var upper_BFg = Math.round((per_BFg + 5) * 10) / 10 ;
            // Upper Limit 7 
            totalBF = totalBF + parseFloat(per_BFg) ;
            sumSquares = sumSquares + (parseFloat(per_BFg) * parseFloat(per_BFg)) ;
            n = n+1 ;
          }
          var max_arr = new Array();
          var min_arr = new Array();
          if(lower_BFa !== undefined && upper_BFa !== undefined)
          {
            max_arr.push(lower_BFa);
            min_arr.push(upper_BFa);
          }
          if(lower_BFb !== undefined && upper_BFb !== undefined)
          {
            max_arr.push(lower_BFb);
            min_arr.push(upper_BFb);
          }
          if(lower_BFc !== undefined && upper_BFc !== undefined)
          {
            max_arr.push(lower_BFc);
            min_arr.push(upper_BFc);
          }
          if(lower_BFd !== undefined && upper_BFd !== undefined)
          {
            max_arr.push(lower_BFd);
            min_arr.push(upper_BFd);
          }
          if(lower_BFe !== undefined && upper_BFe !== undefined)
          {
            max_arr.push(lower_BFe);
            min_arr.push(upper_BFe);
          }
          if(lower_BFf !== undefined && upper_BFf !== undefined)
          {
            max_arr.push(lower_BFf);
            min_arr.push(upper_BFf);
          }
          if(lower_BFg !== undefined && upper_BFg !== undefined)
          {
            max_arr.push(lower_BFg);
            min_arr.push(upper_BFg);
          }
          var max_lower = Math.max.apply(null, max_arr) ;
          document.getElementById("max_Lower").innerHTML = parseFloat(max_lower);
          var min_upper = Math.min.apply(null, min_arr) ;
          document.getElementById("min_Upper").innerHTML = parseFloat(min_upper);
          var meanBF = totalBF / n ;
          // Mean value       
          var variance = (sumSquares - (totalBF * totalBF) / n) / n ;
          // Variance
          var sd = Math.sqrt(variance) ;
          // Standard Deviation          
          // Store the values 
          document.getElementById('meanbodyfat').value=meanBF;
          document.getElementById('sdbodyfat').value=sd;
          if(per_BFa !== undefined){
            document.getElementById('perBF1').innerHTML = Math.round(parseFloat(per_BFa) * 10) / 10+" %" ;
          }
          if(per_BFb !== undefined){
            document.getElementById('perBF2').innerHTML = Math.round(parseFloat(per_BFb) * 10) / 10+" %" ;
          }
          if(per_BFc !== undefined){
            document.getElementById('perBF3').innerHTML = Math.round(parseFloat(per_BFc) * 10) / 10+" %" ;
          }
          if(per_BFd !== undefined){
            document.getElementById('perBF4').innerHTML = Math.round(parseFloat(per_BFd) * 10) / 10+" %" ;
          }
          if(per_BFe !== undefined){
            document.getElementById('perBF5').innerHTML = Math.round(parseFloat(per_BFe) * 10) / 10+" %" ;
          }
          if(per_BFf !== undefined){
            document.getElementById('perBF6').innerHTML = Math.round(parseFloat(per_BFf) * 10) / 10+" %" ;
          }
          if(per_BFg !== undefined){
            document.getElementById('perBF7').innerHTML = Math.round(parseFloat(per_BFg) * 10) / 10+" %" ;
          }
          var fat_values = [per_BFa,per_BFb,per_BFc,per_BFd,per_BFe,per_BFf,per_BFg] ;
          //fat_values.sort(); 
          fat_values.sort(function(a,b) {
            return (a - b) ;
          }
                         );
          $.jqplot.config.enablePlugins = true;
          var chartData1 = [[1,parseFloat(fat_values[0])], [2,parseFloat(fat_values[1])], [3,parseFloat(fat_values[2])], [5,parseFloat(fat_values[3])], [6,parseFloat(fat_values[4])], [7,parseFloat(fat_values[5])], [8,parseFloat(fat_values[6])]] ;
          // %Bosy Fat Values   
          var chartData2 = [[4,parseFloat(meanBF)]] ;
          var chartData3 = [[4,parseFloat(meanBF)], [4,((Math.round(parseFloat(meanBF) * 10) / 10) + parseFloat(sd))]] ;
          var chartData4 = [[4,parseFloat(meanBF)], [4,((Math.round(parseFloat(meanBF) * 10) / 10) - parseFloat(sd))]] ;
          var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
            axes: {
              xaxis: {
                ticks : ['0', '1', '2', '3', '4', '5', '6', '7', '8','9'],
                showTicks: false,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions: {
                  show: false            
                }
                ,              
                tickOptions: {
                  showGridline: false,
                  showMark: false
                }
              }
              ,        
              yaxis: {
                ticks:['0', '5', '10', '15', '20', '25', '30', '35', '40'],
                showTicks: false,              
                tickOptions: {
                  formatString: '%.1f' ,
                  angle: 30
                }
              }
            }
            ,
            grid:{
              shadow:false,
              drawBorder:false,
              background: '#f2f2f2',
              shadowColor:'transparent'
            }
            ,        
            seriesDefaults: {
              showLine:false,                     
              showMarker: true,
              markerRenderer: $.jqplot.MarkerRenderer,
              markerOptions: {
                size: 12
              }
              ,
              shadow: false                    
            }
            ,
            seriesColors: ["#906bd0", "#906bd0", "#906bd0", "#906bd0"],           
            series: [            
              {
                showLine:false,                     
                showMarker: true,
                markerRenderer: $.jqplot.MarkerRenderer,
                markerOptions: {
                  size: 12
                }
                ,
                shadow: false                    
              }
              ,
              {
                markerRenderer: $.jqplot.MarkerRenderer,
                markerOptions: {
                  size: 20}
              }
              ,
              {
                showLine:true,                     
                showMarker: false,             
              }
              ,
              {
                showLine:true,                     
                showMarker: false,             
              }
            ],
            highlighter: {
              sizeAdjust: 3,          
              tooltipAxes: 'y',
              tooltipFormatString: '%.1f'            
            }
          }
                              );
          //sportBF.sort(function(a,b) { return a.val - b.val; }); 
          //sportBF.sort();
          var sportBF = sportsList() ;
          var length = sportBF.length;
          var sportData = new Array() ;
          var sportArr = new Array() ;
          for(var i=0; i < length; i++)
          {
            sportData.push(sportBF[i]["val"]) ;
            sportArr.push(sportBF[i]["sport"]) ;
          }
          var plot2 = $.jqplot('sport_group_div', [sportData], {
            seriesColors: ["#3fc986"],        
            axes: {
              xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: sportArr,
                showTicks: false,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions: {
                  show: false            
                }
                ,   
                tickOptions: {
                  showGridline: false,
                  showMark: false           
                }
              }
              ,        
              yaxis: {
                ticks:['0', '5', '10', '15', '20', '25', '30', '35', '40'],
                showTicks: false,              
                tickOptions: {
                  formatString: '%.1f' ,
                  angle: 30
                }
              }
            }
            ,
            grid:{
              shadow:false,
              drawBorder:false,
              background: '#f2f2f2',
              shadowColor:'transparent'
            }
            ,        
            seriesDefaults: {
              showLine:false,                     
              showMarker: true,
              markerRenderer: $.jqplot.MarkerRenderer,
              markerOptions: {
                size: 12
              }
              ,
              shadow: false                    
            }
            ,                          
            highlighter: {
              show: true,
              sizeAdjust: 3,               
              tooltipContentEditor:tooltipContentEditor
            }
          }
                              );
        }
        else if(document.getElementById("gender").value == "Female")
        {
          var n = 0 ;
          // Toatal number of %BF  
          var totalBF = 0 ;
          // Total %BF
          var sumSquares = 0 ;
          // Sum of squares of %BF  
          if(triceps !== "" && subscapular !== "" && supraspinale !== "" && abdominal !== "" && thigh !== "" && calf !== "") 
          {
            var BDa = 1.20953 - 0.08294 * Math.log10(parseFloat(triceps) + parseFloat(subscapular) + parseFloat(supraspinale) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf)) ;
            var per_BFa = 495 / BDa - 450 ;
            // %Body Fat 1
            var lower_BFa = Math.round((per_BFa - 5.6) * 10) / 10 ;
            // Lower Limit 1  
            var upper_BFa = Math.round((per_BFa + 5.6) * 10) / 10 ;
            // Upper Limit 1  
            totalBF = totalBF + parseFloat(per_BFa) ;
            sumSquares = sumSquares + (parseFloat(per_BFa) * parseFloat(per_BFa)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && supraspinale !== "" && calf !== "") 
          {
            var BDb = 1.17484 - 0.07229 * Math.log10(parseFloat(triceps) + parseFloat(subscapular) + parseFloat(supraspinale) + parseFloat(calf)) ;
            var per_BFb = 495 / BDb - 450 ;
            // %Body Fat 2 
            var lower_BFb = Math.round((per_BFb - 5.6) * 10) / 10 ;
            // Lower Limit 2  
            var upper_BFb = Math.round((per_BFb + 5.6) * 10) / 10 ;
            // Upper Limit 2  
            totalBF = totalBF + parseFloat(per_BFb) ;
            sumSquares = sumSquares + (parseFloat(per_BFb) * parseFloat(per_BFb)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && thigh !== "") 
          {
            var BDc = 1.06234 - 0.000688 * parseFloat(subscapular) - 0.00039 * parseFloat(triceps) - 0.00025 * parseFloat(thigh) ;
            var per_BFc = 495 / BDc - 450 ;
            // %Body Fat 3
            var lower_BFc = Math.round((per_BFc - 5.6) * 10) / 10 ;
            // Lower Limit 3  
            var upper_BFc = Math.round((per_BFc + 5.6) * 10) / 10 ;
            // Upper Limit 3  
            totalBF = totalBF + parseFloat(per_BFc) ;
            sumSquares = sumSquares + (parseFloat(per_BFc) * parseFloat(per_BFc)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && iliac !== "") 
          {
            var BDd =1.0987 - 0.00122 * (parseFloat(triceps) + parseFloat(subscapular) + parseFloat(iliac)) 
            + 0.00000263 * [(parseFloat(triceps) + parseFloat(subscapular) + parseFloat(iliac)) * (parseFloat(triceps) + parseFloat(subscapular) + parseFloat(iliac))]  ;
            var per_BFd = 495 / BDd - 450 ;
            // %Body Fat 4
            var lower_BFd = Math.round((per_BFd - 5.2) * 10) / 10 ;
            // Lower Limit 4  
            var upper_BFd = Math.round((per_BFd + 5.2) * 10) / 10 ;
            // Upper Limit 4  
            totalBF = totalBF + parseFloat(per_BFd) ;
            sumSquares = sumSquares + (parseFloat(per_BFd) * parseFloat(per_BFd)) ;
            n = n+1 ;
          }
          if(triceps !== "" && iliac !== "") 
          {
            var BDe = 1.0764 - 0.00081 * parseFloat(iliac) - 0.00088 * parseFloat(triceps)   ;
            var per_BFe = 495 / BDe - 450 ;
            // %Body Fat 5
            var lower_BFe = Math.round((per_BFe - 7.2) * 10) / 10 ;
            // Lower Limit 5  
            var upper_BFe = Math.round((per_BFe + 7.2) * 10) / 10 ;
            // Upper Limit 5  
            totalBF = totalBF + parseFloat(per_BFe) ;
            sumSquares = sumSquares + (parseFloat(per_BFe) * parseFloat(per_BFe)) ;
            n = n+1 ;
          }
          if(triceps !== "" && abdominal !== "" && thigh !== "" && iliac !== "" && hipG !== "") 
          {
            var BDf = 1.24374 - 0.03162 * Math.log(parseFloat(triceps) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(iliac)) - 0.00066 * parseFloat(hipG)
            var per_BFf = 495 / BDf - 450 ;
            // %Body Fat 6
            var lower_BFf = Math.round((per_BFf - 7.2) * 10) / 10 ;
            // Lower Limit 6  
            var upper_BFf = Math.round((per_BFf + 7.2) * 10) / 10 ;
            // Upper Limit 6  
            totalBF = totalBF + parseFloat(per_BFf) ;
            sumSquares = sumSquares + (parseFloat(per_BFf) * parseFloat(per_BFf)) ;
            n = n+1 ;
          }
          if(triceps !== "" && thigh !== "" && iliac !== "") 
          {
            var BDg = 1.21389 - 0.04057 * Math.log(parseFloat(triceps) + parseFloat(thigh) + parseFloat(iliac)) - 0.00016 * parseFloat(age) ;
            var per_BFg = 495 / BDg - 450 ;
            // %Body Fat 7
            var lower_BFg = Math.round((per_BFg - 7.8) * 10) / 10 ;
            // Lower Limit 7  
            var upper_BFg = Math.round((per_BFg + 7.8) * 10) / 10 ;
            // Upper Limit 7   
            totalBF = totalBF + parseFloat(per_BFg) ;
            sumSquares = sumSquares + (parseFloat(per_BFg) * parseFloat(per_BFg)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && biceps !== "" && iliac !== "") 
          {
            var BDh = 1.1567 - 0.0717 * Math.log10(parseFloat(triceps)+ parseFloat(biceps) + parseFloat(subscapular) + parseFloat(iliac)) ;
            var per_BFh = 495 / BDh - 450 ;
            // %Body Fat 8
            var lower_BFh = Math.round((per_BFh - 10.8) * 10) / 10 ;
            // Lower Limit 8  
            var upper_BFh = Math.round((per_BFh + 10.8) * 10) / 10 ;
            // Upper Limit 8 
            totalBF = totalBF + parseFloat(per_BFh) ;
            sumSquares = sumSquares + (parseFloat(per_BFh) * parseFloat(per_BFh)) ;
            n = n+1 ;
          }
          if(thigh !== "" && iliac !== "") 
          {
            var BDi = 1.0852 - 0.0008 * parseFloat(iliac) - 0.0011 * parseFloat(thigh)  ;
            var per_BFi = 495 / BDi - 450 ;
            // %Body Fat 9
            var lower_BFi = Math.round((per_BFi - 8.2) * 10) / 10 ;
            // Lower Limit 9  
            var upper_BFi = Math.round((per_BFi + 8.2) * 10) / 10 ;
            // Upper Limit 9 
            totalBF = totalBF + parseFloat(per_BFi) ;
            sumSquares = sumSquares + (parseFloat(per_BFi) * parseFloat(per_BFi)) ;
            n = n+1 ;
          }
          if(triceps !== "" && flexArmG !== "" && subscapular !== "" && hipG !== "") 
          {
            var BDj = 1.12569 - 0.001835 * parseFloat(triceps) - 0.002779 / 2.54 * parseFloat(hipG) + 0.005419 / 2.54 * parseFloat(flexArmG) - 0.0007167 * parseFloat(subscapular)  ;
            var per_BFj = 495 / BDj - 450 ;
            // %Body Fat 10
            var lower_BFj = Math.round((per_BFj - 7.2) * 10) / 10 ;
            // Lower Limit 10  
            var upper_BFj = Math.round((per_BFj + 7.2) * 10) / 10 ;
            // Upper Limit 10
            totalBF = totalBF + parseFloat(per_BFj) ;
            sumSquares = sumSquares + (parseFloat(per_BFj) * parseFloat(per_BFj)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && relArmG !== "") 
          {
            var BDk = 0.97845 - 0.0002 * parseFloat(triceps) + 0.00088 * parseFloat(ht) - 0.00122 * parseFloat(subscapular) - 0.00234 * parseFloat(relArmG)  ;
            var per_BFk = 495 / BDk - 450 ;
            // %Body Fat 11
            var lower_BFk = Math.round((per_BFk - 7) * 10) / 10 ;
            // Lower Limit 11  
            var upper_BFk = Math.round((per_BFk + 7) * 10) / 10 ;
            // Upper Limit 11   
            totalBF = totalBF + parseFloat(per_BFk) ;
            sumSquares = sumSquares + (parseFloat(per_BFk) * parseFloat(per_BFk)) ;
            n = n+1 ;
          }
          if(iliac !== "" && subscapular !== "" && thighG !== "" && humerus !== "") 
          {
            var BDl = 1.09246 - 0.00049 * parseFloat(subscapular) - 0.00075 * parseFloat(iliac) + 0.0071 * parseFloat(humerus) - 0.00121 * parseFloat(thighG)  ;
            var per_BFl = 495 / BDl - 450 ;
            // %Body Fat 12
            var lower_BFl = Math.round((per_BFl - 7.8) * 10) / 10 ;
            // Lower Limit 12  
            var upper_BFl = Math.round((per_BFl + 7.8) * 10) / 10 ;
            // Upper Limit 12  
            totalBF = totalBF + parseFloat(per_BFl) ;
            sumSquares = sumSquares + (parseFloat(per_BFl) * parseFloat(per_BFl)) ;
            n = n+1 ;
          }
          if(triceps !== "" && subscapular !== "" && supraspinale !== "" && abdominal !== "" && thigh !== "" && calf !== "" && hipG !== "" && forearmG !== "" && humerus !== "") 
          {
            var BDm = 1.16957 - 0.06447 *  Math.log10(parseFloat(triceps) + parseFloat(subscapular) + parseFloat(supraspinale) + parseFloat(abdominal) + parseFloat(thigh) + parseFloat(calf)) - 0.000806 * parseFloat(hipG) + 0.0017 * parseFloat(forearmG) + 0.00606 * parseFloat(humerus) ;
            var per_BFm = 495 / BDm - 450 ;
            // %Body Fat 13
            var lower_BFm = Math.round((per_BFm - 5) * 10) / 10 ;
            // Lower Limit 13  
            var upper_BFm = Math.round((per_BFm + 5) * 10) / 10 ;
            // Upper Limit 13  
            totalBF = totalBF + parseFloat(per_BFm) ;
            sumSquares = sumSquares + (parseFloat(per_BFm) * parseFloat(per_BFm)) ;
            n = n+1 ;
          }
          if(iliac !== "" && wristG !== "" && femur !== "" && thigh !== "") 
          {
            var BDn = 1.0836 - 0.0007 * parseFloat(iliac) - 0.0007 * parseFloat(thigh) + 0.0048 * parseFloat(wristG) - 0.0088 * parseFloat(femur) ;
            var per_BFn = 495 / BDn - 450 ;
            // %Body Fat 14
            var lower_BFn = Math.round((per_BFn - 7.4) * 10) / 10 ;
            // Lower Limit 14  
            var upper_BFn = Math.round((per_BFn + 7.4) * 10) / 10 ;
            // Upper Limit 14 
            totalBF = totalBF + parseFloat(per_BFn) ;
            sumSquares = sumSquares + (parseFloat(per_BFn) * parseFloat(per_BFn)) ;
            n = n+1 ;
          }
          var max_arr = new Array();
          var min_arr = new Array();
          if(lower_BFa !== undefined && upper_BFa !== undefined)
          {
            max_arr.push(lower_BFa);
            min_arr.push(upper_BFa);
          }
          if(lower_BFb !== undefined && upper_BFb !== undefined)
          {
            max_arr.push(lower_BFb);
            min_arr.push(upper_BFb);
          }
          if(lower_BFc !== undefined && upper_BFc !== undefined)
          {
            max_arr.push(lower_BFc);
            min_arr.push(upper_BFc);
          }
          if(lower_BFd !== undefined && upper_BFd !== undefined)
          {
            max_arr.push(lower_BFd);
            min_arr.push(upper_BFd);
          }
          if(lower_BFe !== undefined && upper_BFe !== undefined)
          {
            max_arr.push(lower_BFe);
            min_arr.push(upper_BFe);
          }
          if(lower_BFf !== undefined && upper_BFf !== undefined)
          {
            max_arr.push(lower_BFf);
            min_arr.push(upper_BFf);
          }
          if(lower_BFg !== undefined && upper_BFg !== undefined)
          {
            max_arr.push(lower_BFg);
            min_arr.push(upper_BFg);
          }
          if(lower_BFh !== undefined && upper_BFh !== undefined)
          {
            max_arr.push(lower_BFh);
            min_arr.push(upper_BFh);
          }
          if(lower_BFi !== undefined && upper_BFi !== undefined)
          {
            max_arr.push(lower_BFi);
            min_arr.push(upper_BFi);
          }
          if(lower_BFj !== undefined && upper_BFj !== undefined)
          {
            max_arr.push(lower_BFj);
            min_arr.push(upper_BFj);
          }
          if(lower_BFk !== undefined && upper_BFk !== undefined)
          {
            max_arr.push(lower_BFk);
            min_arr.push(upper_BFk);
          }
          if(lower_BFl !== undefined && upper_BFl !== undefined)
          {
            max_arr.push(lower_BFl);
            min_arr.push(upper_BFl);
          }
          if(lower_BFm !== undefined && upper_BFm !== undefined)
          {
            max_arr.push(lower_BFm);
            min_arr.push(upper_BFm);
          }
          if(lower_BFn !== undefined && upper_BFn !== undefined)
          {
            max_arr.push(lower_BFn);
            min_arr.push(upper_BFn);
          }
          var max_lower = Math.max.apply(null, max_arr) ;
          document.getElementById("max_Lower").innerHTML = parseFloat(max_lower);
          var min_upper = Math.min.apply(null, min_arr) ;
          document.getElementById("min_Upper").innerHTML = parseFloat(min_upper);
          var meanBF = totalBF / n ;
          // Mean value       
          var variance = (sumSquares - (totalBF * totalBF) / n) / n ;
          // Variance
          var sd = Math.sqrt(variance) ;
          // Standard Deviation          
          // Store the values 
          document.getElementById('meanbodyfat').value=meanBF;
          document.getElementById('sdbodyfat').value=sd;
          if(per_BFa !== undefined){
            document.getElementById('perBF1').innerHTML = Math.round(parseFloat(per_BFa) * 10) / 10+" %" ;
          }
          if(per_BFb !== undefined){
            document.getElementById('perBF2').innerHTML = Math.round(parseFloat(per_BFb) * 10) / 10+" %" ;
          }
          if(per_BFc !== undefined){
            document.getElementById('perBF3').innerHTML = Math.round(parseFloat(per_BFc) * 10) / 10+" %" ;
          }
          if(per_BFd !== undefined){
            document.getElementById('perBF4').innerHTML = Math.round(parseFloat(per_BFd) * 10) / 10+" %" ;
          }
          if(per_BFe !== undefined){
            document.getElementById('perBF5').innerHTML = Math.round(parseFloat(per_BFe) * 10) / 10+" %" ;
          }
          if(per_BFf !== undefined){
            document.getElementById('perBF6').innerHTML = Math.round(parseFloat(per_BFf) * 10) / 10+" %" ;
          }
          if(per_BFg !== undefined){
            document.getElementById('perBF7').innerHTML = Math.round(parseFloat(per_BFg) * 10) / 10+" %" ;
          }
          if(per_BFh !== undefined){
            document.getElementById('perBF8').innerHTML = Math.round(parseFloat(per_BFh) * 10) / 10+" %" ;
          }
          if(per_BFi !== undefined){
            document.getElementById('perBF9').innerHTML = Math.round(parseFloat(per_BFi) * 10) / 10+" %" ;
          }
          if(per_BFj !== undefined){
            document.getElementById('perBF10').innerHTML = Math.round(parseFloat(per_BFj) * 10) / 10+" %" ;
          }
          if(per_BFk !== undefined){
            document.getElementById('perBF11').innerHTML = Math.round(parseFloat(per_BFk) * 10) / 10+" %" ;
          }
          if(per_BFl !== undefined){
            document.getElementById('perBF12').innerHTML = Math.round(parseFloat(per_BFl) * 10) / 10+" %" ;
          }
          if(per_BFm !== undefined){
            document.getElementById('perBF13').innerHTML = Math.round(parseFloat(per_BFm) * 10) / 10+" %" ;
          }
          if(per_BFn !== undefined){
            document.getElementById('perBF14').innerHTML = Math.round(parseFloat(per_BFn) * 10) / 10+" %" ;
          }
          var fat_values = [per_BFa,per_BFb,per_BFc,per_BFd,per_BFe,per_BFf,per_BFg,per_BFh,per_BFi,per_BFj,per_BFk,per_BFl,per_BFm,per_BFn] ;
          //fat_values.sort(); 
          fat_values.sort(function(a,b) {
            return (a - b) ;
          }
                         );
          $.jqplot.config.enablePlugins = true;
          var chartData1 = [[1,parseFloat(fat_values[0])], [2,parseFloat(fat_values[1])], [3,parseFloat(fat_values[2])], [4,parseFloat(fat_values[3])], [5,parseFloat(fat_values[4])], [6,parseFloat(fat_values[5])], [7,parseFloat(fat_values[6])], [9,parseFloat(fat_values[7])], [10,parseFloat(fat_values[8])], [11,parseFloat(fat_values[9])], [12,parseFloat(fat_values[10])], [13,parseFloat(fat_values[11])], [14,parseFloat(fat_values[12])], [15,parseFloat(fat_values[13])]] ;
          // %Bosy Fat Values   
          var chartData2 = [[8,parseFloat(meanBF)]] ;
          var chartData3 = [[8,parseFloat(meanBF)], [8,((Math.round(parseFloat(meanBF) * 10) / 10) + parseFloat(sd))]] ;
          var chartData4 = [[8,parseFloat(meanBF)], [8,((Math.round(parseFloat(meanBF) * 10) / 10) - parseFloat(sd))]] ;
          var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
            axes: {
              xaxis: {
                ticks : ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '10', '11', '12','13','14','15','16'],
                showTicks: false,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions: {
                  show: false            
                }
                ,              
                tickOptions: {
                  showGridline: false,
                  showMark: false
                }
              }
              ,        
              yaxis: {
                ticks:['0', '5', '10', '15', '20', '25', '30', '35', '40'],
                showTicks: false,              
                tickOptions: {
                  formatString: '%.1f' ,
                  angle: 30
                }
              }
            }
            ,
            grid:{
              shadow:false,
              drawBorder:false,
              background: '#f2f2f2',
              shadowColor:'transparent'
            }
            ,        
            seriesDefaults: {
              showLine:false,                     
              showMarker: true,
              markerRenderer: $.jqplot.MarkerRenderer,
              markerOptions: {
                size: 12
              }
              ,
              shadow: false                    
            }
            ,
            seriesColors: ["#906bd0", "#906bd0", "#906bd0", "#906bd0"],           
            series: [            
              {
                showLine:false,                     
                showMarker: true,
                markerRenderer: $.jqplot.MarkerRenderer,
                markerOptions: {
                  size: 12
                }
                ,
                shadow: false                    
              }
              ,
              {
                markerRenderer: $.jqplot.MarkerRenderer,
                markerOptions: {
                  size: 20}
              }
              ,
              {
                showLine:true,                     
                showMarker: false,             
              }
              ,
              {
                showLine:true,                     
                showMarker: false,             
              }
            ],
            highlighter: {
              sizeAdjust: 3,          
              tooltipAxes: 'y',
              tooltipFormatString: '%.1f'            
            }
          }
                              );
          //sportBF.sort(function(a,b) { return a.val - b.val; }); 
          //sportBF.sort();
          var sportBF = sportsList() ;
          var length = sportBF.length;
          var sportData = new Array() ;
          var sportArr = new Array() ;
          for(var i=0; i < length; i++)
          {
            sportData.push(sportBF[i]["val"]) ;
            sportArr.push(sportBF[i]["sport"]) ;
          }
          var plot2 = $.jqplot('sport_group_div', [sportData], {
            seriesColors: ["#3fc986"],        
            axes: {
              xaxis: {
                renderer: $.jqplot.CategoryAxisRenderer,
                ticks: sportArr,
                showTicks: false,
                labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
                labelOptions: {
                  show: false            
                }
                ,     
                tickOptions: {
                  formatString:'%s',
                  showGridline: false,
                  showMark: false           
                }
              }
              ,        
              yaxis: {
                ticks:['0', '5', '10', '15', '20', '25', '30', '35', '40'],
                showTicks: false,              
                tickOptions: {
                  formatString: '%.1f' ,
                  angle: 30
                }
              }
            }
            ,
            grid:{
              shadow:false,
              drawBorder:false,
              background: '#f2f2f2',
              shadowColor:'transparent'
            }
            ,        
            seriesDefaults: {
              showLine:false,                     
              showMarker: true,
              markerRenderer: $.jqplot.MarkerRenderer,
              markerOptions: {
                size: 12
              }
              ,
              shadow: false                    
            }
            ,                          
            highlighter: {
              show: true,
              sizeAdjust: 3,               
              tooltipContentEditor:tooltipContentEditor
            }
          }
                              );
        }
        document.getElementById('meanBF').innerHTML = (Math.round(parseFloat(meanBF) * 10) / 10)+" ± "+(Math.round(parseFloat(sd) * 10) / 10)+" % body fat" ;
      }
                    );
      function tooltipContentEditor(str, seriesIndex, pointIndex, plot) {
        var sportBF = sportsList() ;
        var length = sportBF.length;
        for(var i=0; i < length; i++)
        {
          if(i == pointIndex)
          {
            var data = "<div style='background-color:white; border:1px #ddd solid; width:auto; height:20px'>"
            data += sportBF[i]["sport"]+" : "+sportBF[i]["val"] ;
            data += " %</div>" ;
            document.getElementById('sportData').innerHTML = sportBF[i]["sport"]+" : "+sportBF[i]["val"]+" %" ;
          }
        }
        return data ;
      }
      function sportsList()
      {
        /* if(document.getElementById("gender").value == "Male")
   {    
       var sportBF = [
                        { sport: "American football [running back]", val: 7.3 },
						{ sport: "American football [linemen]", val: 22.5 },
						{ sport: "American football [quarterback]", val: 14.6 },
						{ sport: "American football [wide receiver]", val: 8.1 },
						{ sport: "Australian football [midfield]", val: 8.4 },
                        { sport: "Australian football [general]", val: 9.20 },
                        { sport: "Badminton", val: 10.0 },                        
                        { sport: "Baseball", val: 13.80 },
                        { sport: "Basketball [guards]", val: 9.9 },
                        { sport: "Basketball [forwards]", val: 10.1 },
                        { sport: "Basketball [centres]", val: 14.4 },
                        { sport: "Bobsleigh", val: 12.5 },
                        { sport: "Bodybuilding", val: 5.90 },
                        { sport: "Boxing [general]", val: 14.50 },
                        { sport: "Boxing [heavyweight]", val: 17.40 },
                        { sport: "Canoe polo", val: 12.30 },
                        { sport: "Canoeing [Canadian]", val: 11.80 },
                        { sport: "Cricket [general]", val: 12.65 },
                        { sport: "Cricket [fast bowler]", val: 11.10 },
                        { sport: "Cycling [track sprint]", val: 9.10 },                        
                        { sport: "Cycling - off-road", val: 5.9 },
                        { sport: "Cycling - road", val: 6.0 },
                        { sport: "Dance [standard]", val: 11.20 },
                        { sport: "Dance [ten dance]", val: 14.90 },
                        { sport: "Dance [ballet]", val: 6.90 },
                        { sport: "Dance [sport]", val: 11.70 },
                        { sport: "Decathlon", val: 8.05 },
                        { sport: "Discus", val: 17.90 },
                        { sport: "Diving", val: 7.53 },
                        { sport: "Fencing", val: 11.06 },
                        { sport: "Golf", val: 15.66 },
                        { sport: "Handball [goalkeeper]", val: 20.20 },
                        { sport: "Handball [back]", val: 12.40 },
                        { sport: "Handball [center]", val: 13.40 },
                        { sport: "Handball [wing]", val: 15.1 },
                        { sport: "Gymnastics", val: 8.85 },
                        { sport: "Hockey (field)", val: 11.40 },
                        { sport: "Hockey (ice)", val: 13.17 },
                        { sport: "High jump", val: 10.1 },
                        { sport: "Jockey", val: 8.26 },
                        { sport: "Judo", val: 9.32 },
                        { sport: "Jumping - general", val: 7.0 },
                        { sport: "Karate", val: 16.6 },
                        { sport: "Kayak [sprint]", val: 10.50 },
                        { sport: "Kayak [slalom]", val: 10.0 },
                        { sport: "Kayak [marathon]", val: 9.1 },
                        { sport: "Kayak [general]", val: 10.14 },
                        { sport: "Lacrosse", val: 12.30 },
                        { sport: "Orienteering", val: 9.10 },
                        { sport: "Parachuting", val: 11.31 },
                        { sport: "Pentathlon", val: 12.3 },
                        { sport: "Powerlifting lightweight", val: 9.20 },
                        { sport: "Powerlifting middleweight", val: 10.10 },
                        { sport: "Powerlifting heavyweight", val: 12.80 },
                        { sport: "Rockclimbing", val: 8.70 },
                        { sport: "Rollerskating", val: 10.33 },
                        { sport: "Rowing - heavyweight", val: 11.20 },
                        { sport: "Rowing - lightweight", val: 8.80 },
                        { sport: "Rugby League [backs]", val: 11.10 },
                        { sport: "Rugby League [forwards]", val: 13.50 },
                        { sport: "Rugby Union [forwards]", val: 18.38 },
                        { sport: "Rugby Union [backs]", val: 11.85 },
                        { sport: "Running [sprint]", val: 5.60 },
                        { sport: "Running [middle distance]", val: 8.15 },
                        { sport: "Running [distance]", val: 7.49 },
                        { sport: "Sailing", val: 8.98 },
                        { sport: "Shot put", val: 19.80 },
                        { sport: "Skating [figure]", val: 11.7 },
                        { sport: "Skiing [cross-country]", val: 9.66 },
                        { sport: "Skiing [downhill]", val: 14.01 },
                        { sport: "Skijumping", val: 13.3 },
                        { sport: "Soccer", val: 9.70 },
                        { sport: "Squash", val: 11.20 },
                        { sport: "Surfing", val: 11.89 },
                        { sport: "Swimming", val: 10.69 },
                        { sport: "Table tennis", val: 14.10 },
                        { sport: "Tennis", val: 11.7 },
                        { sport: "Throwing - general", val: 18.4 },
                        { sport: "Ttriathlon", val: 7.34 },
                        { sport: "Volleyball", val: 9.76 },
                        { sport: "Walking", val: 7.3 },
                        { sport: "Waterpolo", val: 10.73 },
                        { sport: "Weightlifting", val: 13.60 },
                        { sport: "Wrestling", val: 7.40 }
                  ] ;
    }
    else if(document.getElementById("gender").value == "Female")
    {
        var sportBF = [                        
                        { sport: "Badminton", val: 13.4 },                   
                        { sport: "Basketball [guards]", val: 18.2 },
                        { sport: "Basketball [forwards]", val: 18.8 },
                        { sport: "Basketball [centres]", val: 23.5 },
                        { sport: "Bobsleigh", val: 17.4 },
                        { sport: "Bodybuilding", val: 10.90 },                        
                        { sport: "Cricket", val: 18.9 },                        
                        { sport: "Cycling [track sprint]", val: 14.9 },                        
                        { sport: "Cycling - off-road", val: 13.20 },
                        { sport: "Cycling - road", val: 13.10 },
                        { sport: "Dance [standard]", val: 20.90 },
                        { sport: "Dance [ten dance]", val: 22.60 },
                        { sport: "Dance [ballet]", val: 12.90 },
                        { sport: "Dance [sport]", val: 18.90 },                        
                        { sport: "Discus", val: 28.70 },
                        { sport: "Diving", val: 14.6 },
                        { sport: "Fencing", val: 16.13 },
                        { sport: "Golf", val: 28.8 },
                        { sport: "Gymnastics [artistic]", val: 12.36 },
                        { sport: "Gymnastics [rhythmic", val: 14.94 },
                        { sport: "Handball [goalkeeper]", val: 23.2 },
                        { sport: "Handball [back]", val: 19.4 },
                        { sport: "Handball [center]", val: 20.6 },
                        { sport: "Handball [wing]", val: 21.8 },
                        { sport: "Heptathlon", val: 14.4 },
                        { sport: "Hockey (field)", val: 17.29 },
                        { sport: "Hockey (ice)", val: 24.33 },
                        { sport: "High jump", val: 14.5 },
                        { sport: "Hurdles", val: 15.3 },
                        { sport: "Javelin", val: 17.40 },
                        { sport: "Judo", val: 20.6 },
                        { sport: "Jumping - general", val: 14.6 },
                        { sport: "Karate", val: 20.7 },
                        { sport: "Kayak [sprint]", val: 19.60 },
                        { sport: "Kayak [slalom]", val: 21.20 },
                        { sport: "Kayak [marathon]", val: 16.8 },
                        { sport: "Kayak [general]", val: 17.4 },
                        { sport: "Lacrosse", val: 19.30 },
                        { sport: "Long jump", val: 16.1 },
                        { sport: "Netball", val: 17.80 },
                        { sport: "Orienteering", val: 16.70 },                        
                        { sport: "Pentathlon", val: 18.1 },                       
                        { sport: "Rockclimbing", val: 19.20 },
                        { sport: "Rollerskating", val: 20.20 },
                        { sport: "Rowing - heavyweight", val: 17.7 },
                        { sport: "Rowing - lightweight", val: 12.99 },
                        { sport: "Rugby League [backs]", val: 22.88 },
                        { sport: "Rugby League [forwards]", val: 27.63 },
                        { sport: "Rugby Union [forwards]", val: 23.26 },
                        { sport: "Rugby Union [backs]", val: 20.71 },
                        { sport: "Running [sprint]", val: 15.5 },
                        { sport: "Running [middle distance]", val: 16.2 },
                        { sport: "Running [distance]", val: 13.20 },
                        { sport: "Sailing", val: 17.74 },
                        { sport: "Shot put", val: 27.30 },                        
                        { sport: "Skiing [cross-country]", val: 16.75 },
                        { sport: "Skiing [downhill]", val: 22.40 },                        
                        { sport: "Soccer", val: 19.80 },
                        { sport: "Softball", val: 23.42 },
                        { sport: "Squash", val: 21.40 },
                        { sport: "Surfing", val: 22.00 },
                        { sport: "Swimming", val: 14.4 },
                        { sport: "Synchronised swimming", val: 21.70 },
                        { sport: "Table tennis", val: 22.80 },
                        { sport: "Tennis", val: 19.8 },
                        { sport: "Throwing - general", val: 27.40 },
                        { sport: "Touch", val: 17.9 },
                        { sport: "Ttriathlon", val: 14.09 },
                        { sport: "Volleyball", val: 19.11 },                        
                        { sport: "Waterpolo", val: 22.11 }                        
                  ];
    } */
        if(document.getElementById("gender").value == "Male")
        {
          var sportBF = [
            {
              sport: "American football [running back]", val: 7.3 }
            ,
            {
              sport: "American football [linemen]", val: 22.5 }
            ,
            {
              sport: "American football [quarterback]", val: 14.6 }
            ,
            {
              sport: "American football [wide receiver]", val: 8.1 }
            ,
            {
              sport: "American football [Archery]", val:17.5 }
            ,
            {
              sport: "Australian football [midfield]", val: 8.4 }
            ,
            {
              sport: "Australian football [general]", val: 9.20 }
            ,
            {
              sport: "Australian football [ruckmen]", val: 11.1 }
            ,
            {
              sport: "Badminton", val: 10.0 }
            ,                        
            {
              sport: "Baseball", val: 13.80 }
            ,
            {
              sport: "Basketball [guards]", val: 9.9 }
            ,
            {
              sport: "Basketball [forwards]", val: 10.1 }
            ,
            {
              sport: "Basketball [centres]", val: 14.4 }
            ,
            {
              sport: "Bobsleigh", val: 12.5 }
            ,
            {
              sport: "Bodybuilding", val: 5.90 }
            ,
            {
              sport: "Boxing [general]", val: 14.50 }
            ,
            {
              sport: "Boxing [heavyweight]", val: 17.40 }
            ,
            {
              sport: "Canoe polo", val: 12.30 }
            ,
            {
              sport: "Canoeing [Canadian]", val: 11.80 }
            ,
            {
              sport: "Cricket [general]", val: 12.65 }
            ,
            {
              sport: "Cricket [fast bowler]", val: 11.10 }
            ,
            {
              sport: "Cycling [track sprint]", val: 9.10 }
            ,                        
            {
              sport: "Cycling - off-road", val: 5.9 }
            ,
            {
              sport: "Cycling - road", val: 6.0 }
            ,
            {
              sport: "Dance [standard]", val: 11.20 }
            ,
            {
              sport: "Dance [ten dance]", val: 14.90 }
            ,
            {
              sport: "Dance [ballet]", val: 6.90 }
            ,
            {
              sport: "Dance [sport]", val: 11.70 }
            ,
            {
              sport: "Decathlon", val: 8.05 }
            ,
            {
              sport: "Discus", val: 17.90 }
            ,
            {
              sport: "Diving", val: 7.53 }
            ,
            {
              sport: "Fencing", val: 11.06 }
            ,
            {
              sport: "Golf", val: 15.66 }
            ,
            {
              sport: "Handball [goalkeeper]", val: 20.20 }
            ,
            {
              sport: "Handball [back]", val: 12.40 }
            ,
            {
              sport: "Handball [center]", val: 13.40 }
            ,
            {
              sport: "Handball [wing]", val: 15.1 }
            ,
            {
              sport: "Gymnastics", val: 8.85 }
            ,
            {
              sport: "Hockey (field)", val: 11.40 }
            ,
            {
              sport: "Hockey (ice)", val: 13.17 }
            ,
            {
              sport: "High jump", val: 8.4 }
            ,
            {
              sport: "Jockey", val: 8.26 }
            ,
            {
              sport: "Judo", val: 9.32 }
            ,
            {
              sport: "Jumping - general", val: 7.0 }
            ,
            {
              sport: "Karate", val: 16.6 }
            ,
            {
              sport: "Kayak [sprint]", val: 10.50 }
            ,
            {
              sport: "Kayak [slalom]", val: 10.0 }
            ,
            {
              sport: "Kayak [marathon]", val: 9.1 }
            ,
            {
              sport: "Kayak [general]", val: 10.14 }
            ,
            {
              sport: "Lacrosse", val: 12.30 }
            ,
            {
              sport: "Orienteering", val: 9.10 }
            ,
            {
              sport: "Parachuting", val: 11.31 }
            ,
            {
              sport: "Pentathlon", val: 12.3 }
            ,
            {
              sport: "Powerlifting lightweight", val: 9.20 }
            ,
            {
              sport: "Powerlifting middleweight", val: 10.10 }
            ,
            {
              sport: "Powerlifting heavyweight", val: 12.80 }
            ,
            {
              sport: "Rockclimbing", val: 8.70 }
            ,
            {
              sport: "Rollerskating", val: 10.33 }
            ,
            {
              sport: "Rowing - heavyweight", val: 11.20 }
            ,
            {
              sport: "Rowing - lightweight", val: 8.80 }
            ,
            {
              sport: "Rugby League [backs]", val: 11.10 }
            ,
            {
              sport: "Rugby League [forwards]", val: 13.50 }
            ,
            {
              sport: "Rugby Union [forwards]", val: 18.38 }
            ,
            {
              sport: "Rugby Union [backs]", val: 11.85 }
            ,
            {
              sport: "Running [sprint]", val: 5.60 }
            ,
            {
              sport: "Running [middle distance]", val: 8.15 }
            ,
            {
              sport: "Running [distance]", val: 7.49 }
            ,
            {
              sport: "Sailing", val: 8.98 }
            ,
            {
              sport: "Shot put", val: 19.80 }
            ,
            {
              sport: "Skating [figure]", val: 11.7 }
            ,
            {
              sport: "Skiing [cross-country]", val: 9.66 }
            ,
            {
              sport: "Skiing [downhill]", val: 14.01 }
            ,
            {
              sport: "Skijumping", val: 13.3 }
            ,
            {
              sport: "Soccer", val: 9.70 }
            ,
            {
              sport: "Squash", val: 11.20 }
            ,
            {
              sport: "Surfing", val: 11.89 }
            ,
            {
              sport: "Swimming", val: 10.69 }
            ,
            {
              sport: "Table tennis", val: 14.10 }
            ,
            {
              sport: "Tennis", val: 11.7 }
            ,
            {
              sport: "Throwing - general", val: 18.4 }
            ,
            {
              sport: "Ttriathlon", val: 7.34 }
            ,
            {
              sport: "Volleyball", val: 9.76 }
            ,
            {
              sport: "Walking", val: 7.3 }
            ,
            {
              sport: "Waterpolo", val: 10.73 }
            ,
            {
              sport: "Weightlifting", val: 13.60 }
            ,
            {
              sport: "Wrestling", val: 7.40 }
          ] ;
        }
        else if(document.getElementById("gender").value == "Female")
        {
          var sportBF = [                        
            {
              sport: "Archery", val: 26 }
            ,  
            {
              sport: "Badminton", val: 13.4 }
            ,                   
            {
              sport: "Basketball [guards]", val: 18.2 }
            ,
            {
              sport: "Basketball [forwards]", val: 18.8 }
            ,
            {
              sport: "Basketball [centres]", val: 23.5 }
            ,
            {
              sport: "Bobsleigh", val: 17.4 }
            ,
            {
              sport: "Bodybuilding", val: 10.90 }
            ,                        
            {
              sport: "Cricket", val: 18.9 }
            ,                        
            {
              sport: "Cycling [track sprint]", val: 14.9 }
            ,                        
            {
              sport: "Cycling - off-road", val: 13.20 }
            ,
            {
              sport: "Cycling - road", val: 13.10 }
            ,
            {
              sport: "Dance [standard]", val: 20.90 }
            ,
            {
              sport: "Dance [ten dance]", val: 22.60 }
            ,
            {
              sport: "Dance [ballet]", val: 12.90 }
            ,
            {
              sport: "Dance [sport]", val: 18.90 }
            ,                        
            {
              sport: "Discus", val: 28.70 }
            ,
            {
              sport: "Diving", val: 14.6 }
            ,
            {
              sport: "Fencing", val: 16.13 }
            ,
            {
              sport: "Golf", val: 28.8 }
            ,
            {
              sport: "Gymnastics [artistic]", val: 12.36 }
            ,
            {
              sport: "Gymnastics [rhythmic", val: 14.94 }
            ,
            {
              sport: "Handball [goalkeeper]", val: 23.2 }
            ,
            {
              sport: "Handball [back]", val: 19.4 }
            ,
            {
              sport: "Handball [center]", val: 20.6 }
            ,
            {
              sport: "Handball [wing]", val: 21.8 }
            ,
            {
              sport: "Heptathlon", val: 14.4 }
            ,
            {
              sport: "Hockey (field)", val: 17.29 }
            ,
            {
              sport: "Hockey (ice)", val: 24.33 }
            ,
            {
              sport: "High jump", val: 14.5 }
            ,
            {
              sport: "Hurdles", val: 15.3 }
            ,
            {
              sport: "Javelin", val: 17.40 }
            ,
            {
              sport: "Judo", val: 20.6 }
            ,
            {
              sport: "Jumping - general", val: 14.6 }
            ,
            {
              sport: "Karate", val: 20.7 }
            ,
            {
              sport: "Kayak [sprint]", val: 19.60 }
            ,
            {
              sport: "Kayak [slalom]", val: 21.20 }
            ,
            {
              sport: "Kayak [marathon]", val: 16.8 }
            ,
            {
              sport: "Kayak [general]", val: 17.4 }
            ,
            {
              sport: "Lacrosse", val: 19.30 }
            ,
            {
              sport: "Long jump", val: 16.1 }
            ,
            {
              sport: "Netball", val: 17.80 }
            ,
            {
              sport: "Orienteering", val: 16.70 }
            ,                        
            {
              sport: "Pentathlon", val: 18.1 }
            ,                       
            {
              sport: "Rockclimbing", val: 19.20 }
            ,
            {
              sport: "Rollerskating", val: 20.20 }
            ,
            {
              sport: "Rowing - heavyweight", val: 17.7 }
            ,
            {
              sport: "Rowing - lightweight", val: 12.99 }
            ,
            {
              sport: "Rugby League [backs]", val: 22.88 }
            ,
            {
              sport: "Rugby League [forwards]", val: 27.63 }
            ,
            {
              sport: "Rugby Union [forwards]", val: 23.26 }
            ,
            {
              sport: "Rugby Union [backs]", val: 20.71 }
            ,
            {
              sport: "Running [sprint]", val: 15.5 }
            ,
            {
              sport: "Running [middle distance]", val: 16.2 }
            ,
            {
              sport: "Running [distance]", val: 13.20 }
            ,
            {
              sport: "Sailing", val: 17.74 }
            ,
            {
              sport: "Shot put", val: 27.30 }
            ,                        
            {
              sport: "Skiing [cross-country]", val: 16.75 }
            ,
            {
              sport: "Skiing [downhill]", val: 22.40 }
            ,                        
            {
              sport: "Soccer", val: 19.80 }
            ,
            {
              sport: "Softball", val: 23.42 }
            ,
            {
              sport: "Squash", val: 21.40 }
            ,
            {
              sport: "Surfing", val: 22.00 }
            ,
            {
              sport: "Swimming", val: 14.4 }
            ,
            {
              sport: "Synchronised swimming", val: 21.70 }
            ,
            {
              sport: "Table tennis", val: 22.80 }
            ,
            {
              sport: "Tennis", val: 19.8 }
            ,
            {
              sport: "Throwing - general", val: 27.40 }
            ,
            {
              sport: "Touch", val: 17.9 }
            ,
            {
              sport: "Ttriathlon", val: 14.09 }
            ,
            {
              sport: "Volleyball", val: 19.11 }
            ,                        
            {
              sport: "Waterpolo", val: 22.11 }
          ];
        }
        return sportBF ;
      }
    </script>
  </body>
</html>
