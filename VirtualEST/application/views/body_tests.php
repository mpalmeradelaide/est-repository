<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<script type="text/javascript">
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
</head>

<body>
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>           
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        <div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Body Composition</div>
            <p>There are four sections of the Body Composition module: the (1) Restricted Profile analysis, (2) Full Profile analysis, (3) Error Analysis, and (4) Changing Body Mass tools.
			</br></br>
The Restricted Profile allows the user to enter up to 9 skinfolds, 5 girths and 2 bone breadths plus height and body mass and calculate a range of variables including: phantom z-scores for each anthropometry variable, % body fat using a range of prediction equations, skinfold sums and skinfold maps, somatotype, and plot variables against national age- and sex-based norms. There is also an option to analyse a ‘virtual person’ where plausible values are generated in the program to be used in the various analyses.
</br></br>
				The Full Profile allows the user to enter up to 9 skinfolds, 13 girths, and 19 bone lengths and breadths in addition to height and body mass. The Full Profile allows the user to explore a range of analyses including: phantom z-scores for each anthropometry variable, % body fat using an extensive range of prediction equations, skinfold sums and skinfold maps, somatotype, fractionation of body mass, and plot variables against national age- and sex-based norms. There is also an option to analyse a ‘virtual person’ where plausible values are generated in the program to be used in the various analyses.<br></br>
				The Error Analysis allows the user to perform several analytical tasks including: Calculating Technical Error of Measurement [TEM] from raw data, calculating 95% confidence intervals around a single measure, and determining if a real change has occurred between two measures on the same person.
				</br></br>
				The Changing Body Mass tool allows the user to manipulate the desired % body fat and investigate the change in body mass required.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div> 
    
    </div>
</div>
<div class="wrapper">
    <div class="drop_main">
        <ul>
            <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
            <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
            <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
			<li><a href="<?php echo site_url('Body/changing_body_mass'); ?>" id="changing_body_mass"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Changing Body Mass</a></li>
			<!--<li><a href="<?php echo site_url('Body/sport_match'); ?>" id="sport_match"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Sport Match</a></li>-->

        </ul>
    </div>
</div>
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>	
</body>
</html>
