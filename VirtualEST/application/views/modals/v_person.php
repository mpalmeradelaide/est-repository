<?php

/**
 * contains VP preview for header dropdown
 *
 * modification of original code
 *
 * @author Matt Palmer JAC
 *
 */

?>
<div class="vp_gen_wrapper">
	<div id="validate">
	</div>

	<form method="post" action="<?php echo site_url('/Vpgenerate/submit_smallvp'); ?>">

		<div class="vp_v_detail" style="display: block !important; ">
			<div>	 
				<div class="v_image">         
					<div class="vp_image_wrapper">
						<img  class="vp_dropdown_image" src="<?php
							echo base_url('/') ."/$filename";
							?>">								
					</div>
					<!--<a href="#" class="vp_dropdown_discard discart">Generate New Virtual Profile</a>-->
					<a href="#" class="gen_new_profile">Generate New Profile</a>
				</div>
			</div>
			<div class="field_row"> 
			</div>
			<div class="field_row">
				<div class="field_50">
					<label>First Name</label>
					<input required class="firstname_input" type="text" name="first_name" value="<?php
						echo "$_SESSION[user_first_name]";
					?>">
				</div>
				<div class="field_50">
					<label>Last Name</label>
					<input required class="lastname_input" type="text" name="last_name" value="<?php
						echo "$_SESSION[user_last_name]";
					?>">
				</div>
			</div>
			<div class="field_row">
				<label>Age</label>
				<input required class="age_input" type="number" name="age" id="age_id" value="<?php
					echo round($_SESSION['vp_age']);
					?>">
			</div>
			<div class="field_row">
				<div class="field_50">
					<label>Height [cm]</label><input required class="height_input" type="number" name="height" value="<?php
						echo round($_SESSION['HEIGHT'], 2);
						?>">                  
				</div>
				<div class="field_50">
					<label>Body mass [kg]</label><input required class="weight_input" type="number" name="body_mass" value="<?php
						echo round($_SESSION['MASS'], 2);
					?>">                  
				</div>
			</div>        
			<div class="field_row">
				<div>
					<label>BMI</label><input readonly="readonly" class="bmi_input" type="text" name="BMI" value="<?php
						echo round($_SESSION['BMI'], 2);
						?>">                  
				</div>
					<div class="field_50  vp-extra-content" style="display:<?php
						echo $bodyfatdiv;
						?>">
							<label>Avg % body fat</label><input disabled="disabled" type="text" name="body_fat" class="body_fat_input" value="">                  
					</div>
				</div>
			<div class="field_row vp-extra-content">
				<label>Sub-Population</label>
					<?php
						$subpop_array = array(
							'selectoccupation' => 'Select',
							'Active' => 'Active',
							'General' => 'General',
							'Sedentary' => 'Sedentary',
							'Athlete' => 'Athlete'
							);

						echo form_dropdown('subpopulation', $subpop_array, $_SESSION['subpopulation'], 'class="subpopulation_input"');
						?>  
			</div>
			<div class="field_row vp-extra-content" style="display:<?php
				echo $riskdiv;
				?>">
				<div class="field_50">
				<label>Risk factor score</label><input disabled="disabled" type="text" name="risk_fact_sc" value="<?php
					echo $_SESSION['risk_factor'];
					?>">                  
				</div>

				<div class="field_50 vp-extra-content">
					<label>Risk group</label><input disabled="disabled" type="text" name="risk_group" value="<?php
						echo $_SESSION['risk_group'];
					?>">                  
				</div>
			</div>
			<div class="field_row vp-extra-content" style="display:<?php
				echo $vo2maxdiv;
				?>">   
				<label>VO2max [mL/kg/min]</label>
				<input disabled="disabled" type="text" name="VO2max" value="<?php
					echo $_SESSION['VO2max'];
				?>">
			</div>
			<div class="vp_save_button" id="vp_save_button">
				Save
			</div>

			<?php
			/*<div class="field_row" style="display:<?php
				echo $riskdiv;
				?>">
				<button type="submit" class="save_vpsmall_btn">Save Details</button>
			</div>*/
			?>
		</div>
	</form>
</div>

<script>	
	$(document).ready(function() {
		// calculate BMI on the fly
		$(".height_input").on('input', function () {calc_bmi_vpmodal()});
		$(".weight_input").on('input', function () {calc_bmi_vpmodal()});

		function calc_bmi_vpmodal()
		{
			console.log("function executed: calc_bmi_vpmodal()");

			var height_validity = true;
			var weight_validity = true;

			if (isNaN($(".height_input").val()))
			{
				height_validity = false;
			}

			if (isNaN($(".weight_input").val()))
			{
				weight_validity = false;
			}

			if(height_validity == true && weight_validity == true) {
				var height_input_m = ($(".height_input").val() / 100);
				console.log("height_input_m: " + height_input_m);
				var height_squared = height_input_m * height_input_m;
				console.log("height_squared_m: " + height_squared);
				console.log("'weight_input': " + $(".weight_input").val());
				var bmi = $(".weight_input").val() / height_squared;
				console.log("bmi: " + bmi);
				$('.bmi_input').val(Math.round(bmi*100)/100);
			}
		}
		
		// generate a new VP from the VP dropdown
		$(".gen_new_profile").click(function() {
			$.ajax({
				url: "<?=site_url("welcome/VirtualpersonGenerationAsync")?>",
				type: "post", // To protect sensitive data
				data: {
					ajax:true,
						},
				success:function(response){
					// Handle the response object					
				},
				complete:function(data){
					// parse JSON string in data
					var randomdata = JSON.parse(data.responseText);
					
					// load randomly generated stats into vp dropdown modal
					$(".firstname_input").val(randomdata["firstname"]);
					$(".lastname_input").val(randomdata["lastname"]);
					console.log("FULL NAME: " + randomdata["firstname"] + " " + randomdata["lastname"])
					$(".vpdd_name").text(randomdata["firstname"] + " " + randomdata["lastname"]);
					$(".height_input").val(Math.round(randomdata["HEIGHT"]*100)/100);
					$(".weight_input").val(randomdata["MASS"]);
					$(".subpopulation_input").val(randomdata["sub_population"]);
					$(".bmi_input").val(Math.round(randomdata["BMI"]*100)/100);
					$(".age_input").val(randomdata["vp_age"]);
					calc_bmi_vpmodal();
					 
					// load image
					site_url = "<?php echo base_url(); ?>";
					$(".vp_dropdown_image").attr('src', site_url + randomdata["filename"]);
					
					// TODO also, are we going to have a 'are you sure?' popup for VP gen in dropdown modal as well?
				}
			});
		});

		// save data entered into vp dropdown
		$(".vp_save_button").click(function() {
			$.ajax({
				url: "<?=site_url("welcome/VPSaveDataAsync")?>",
				type: "post",
				data: {
					ajax : true,
					firstname_input : $(".firstname_input").val(),
					lastname_input : $(".lastname_input").val(),
					age_input : $(".age_input").val(),
					height_input : $(".height_input").val(),
					weight_input : $(".weight_input").val(),
					bmi_input : $(".bmi_input").val()
				},
				success:function(response){
					console.log("VP data saved successfully.");
					// do nothing
				},
				complete:function() {
					console.log("VP data saved complete.");
					// do nothing
				}
			});
		});
	});

	

</script>