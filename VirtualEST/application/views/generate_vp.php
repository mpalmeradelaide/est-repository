<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<!--script src="<?php echo "$base/assets/js/"?>jquery.validate.min.js"></script-->
<!-- <script src="http://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script> -->
<script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}


#loading img {
    position: absolute;
    top: 50%;
    left: 50%;
    margin-top: -64px;
    margin-left: -64px;
}

#loading {
    height: 100%;
    width: 100%;
    position: fixed;
    z-index: 5000;
    margin-top: 0px;
    top: 0px;
    left: 0px;
    overflow: hidden;
    display: none;
    background: #fff;
	opacity:0.6;
}


</style>
<script>
   $(function(){     
   $('#vpgenerate').validate({ 
    rules:{    
      no_of_vp  :  {required:true},
       "sel_itm2[]": "required"
     },    
     messages:{ 
      no_of_vp  : {required:"Please enter a number."},
       "sel_itm2[]": "Please add some items."
     }
     });     
    });
</script>
<script>
$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});    
</script>
</head>
<body>

	<!-- insertion of header -->
	<?=$header_insert; ?>

<div id="loading"> <img src="<?php echo "$base/assets/images/loader.gif"?>"> </div>

<div class="overlay">&nbsp;</div>
<div class="info_block">
	<div class="info_block_head green_container">Generate Virtual Person</div>
	<p>The Virtual Person Generator is a standard Toolkit. In which user can genarated the Virtual person profile with the given number of count with its selected test , User can also select gender and sub-population for generating VP.</p>    
	<div class="info_block_foot">
        <a href="#" class="lite_btn grey_btn f_right close">Close</a>
    </div>                
</div> 

<div class="wrapper">
<!-- Form begins -->    
 <?php 
        $attributes = array('id' => 'vpgenerate','name'=>'vpgenerate');
       echo form_open('Vpgenerate/VirtualpersonGeneration',$attributes); ?>
    <div class="contain">
    <!--Start right -->         
        <div class="right-section right-section_new section_121">
            <div class="right-head">VP GENERATOR</div>
            <?php if($upload_count>0){
				echo "<script type='text/javascript' > 
			                  $('#loading').hide();
                     </script>
					 ";
					 
			   echo "<div class='form_message success' id='msg'>You have successfully generated $upload_count profiles. please click 'Export' to download.</div>";
             }
			?>
            
            <div class="field_row verticle_field_row"> 			
                <div class="field_50" style="float: left;">
                    <label style="margin-top: 10px;">Generate up to 5000 profiles with the following characteristics:</label>
                </div>
                <div class="field_24">
                    <input type="number" name="no_of_vp" id="no_of_vp" value="">
                </div>
                <div class="field_24" style="margin-top: 10px;">
                    (number of people)
                </div>
            </div>

            <div class="field_row verticle_field_row"> 
                <div class="field_24">
                    <label for="value">Gender</label>
                    <select id="gender" name="gender">
                        <option value="">Select</option>                   
                        <option value="M">Male</option>                   
                        <option value="F">Female</option>                   
                    </select>
                </div>

                <div class="field_24">
                    <label for="percentile">Sub-population</label>
                    <?php 
                //$subpop_array=array(''=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
				$subpop_array=array('Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete', 'All' => 'All');
                			
                  echo form_dropdown('subpopulation',$subpop_array); 
                ?>  
                </div>

                <input type="button" class="lite_btn grey_btn f_right btn_green" id="Generate" value="Generate" style="margin-top:32px;"/>            
            </div>

            <div class="field_row verticle_field_row"> 
                <div class="field_24">
                    <label>Include these items:</label>
                    <select id="sel_itm" name="" multiple style="height:205px;">
                         <?php foreach($test_var as $val)
                        echo  "<option value='$val->option_val'>$val->option_text</option>";   
                         ?>         
                     </select>
                </div>

                <div class="field_24" style="text-align: center; margin-top: 58px;">
                    <input type="button" class="lite_btn grey_btn  btn_green" id="itm_all_add_btn" value="Add all >>" style="height: 30px;line-height: 30px !important;"/>            
                    <input type="button" class="lite_btn grey_btn  btn_green" id="itm_add_btn" value="Add >>" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>            
                    <input type="button" class="lite_btn grey_btn btn_green" id="itm_remove_btn" value=" << Remove" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>   
                    <input type="button" class="lite_btn grey_btn btn_green" id="itm_remove_all_btn" value=" << Remove all" style="margin-top:12px; height: 30px;line-height: 30px !important;"/>   
                </div>

                <div class="field_24">
                    <select id="sel_itm2" name="sel_itm2[]" multiple style="height: 205px; margin-top: 31px;">
					 <?php 
                        if($selected_var!="")
                        {
                         $selected_array=explode(",",$selected_var);    
                         foreach($selected_array as $val)
                         {
                         echo "<option value='$val'>$val</option>";         
                         }
                        }
                      ?> 
                    </select>
                </div> 
                <div class="field_24 f_right">
                    <input type="hidden" name="exportnum" value="<?php echo $upload_count;?>" id="exportnum">
					<input type="hidden" name="selected_var" value="<?php echo $selected_var;?>" id="selected_var">
                    <input type="button" class="lite_btn grey_btn f_right btn_green" value="Stop" style="display: none;"/>            
                    <input type="button" class="lite_btn grey_btn f_right btn_green" value="Export" id="export" style="margin-top:72px;"/>            
                    <a href="<?php  echo site_url('/'); ?>" style="margin-top:32px;" class="lite_btn grey_btn f_right btn_green" id="exit">Done</a>

					
					<!--<input type="button" class="lite_btn grey_btn f_right btn_green" value="Done" style="margin-top:32px;"/> -->     
                </div>      
            </div>

            <div class="field_row verticle_field_row" style="border: 0;"></div>



        </div> 
    </div>

<?php echo form_close(); ?>
<!-- Form ends -->

</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>		
<script type="text/javascript">
	$(document).ready(function() {
		// remove selection box list - may be remnants left over from a previous generation
		$('#sel_itm2').children().remove().end();
	});

	$(document).on('click','#exit', function(){
		document.forms["myform"].submit();
        //return false;
    });

	// Export VPs
	$(document).on('click','#export', function(){
		if($(".form_message.success").is(":visible")) {
			$("#msg").hide();
			$('#sel_itm2 option').remove();
			$("#vpgenerate").attr("action", "<?php echo base_url(); ?>/index.php/Vpgenerate/exportVP");     
			$("#vpgenerate")[0].submit();     
			//return false;
		} else {
			alert("Please generate a population to export.")
		}
    }); 
        
	// Generate VPs
	$(document).on('click','#Generate', function(){
		$('#vpgenerate').validate();
		if ($("#vpgenerate").valid())
		{
	        var num=document.getElementById('no_of_vp').value;
			if(num < 1 || num > 5000)
	            {
					alert("Number of users should be between 1 and 5000.");
				}
			else
	            {
					$('#loading').show();
					$("#vpgenerate").attr("action", "<?php echo base_url(); ?>/index.php/Vpgenerate/VirtualpersonGeneration");     
					$("#vpgenerate").submit();					
				}
		}
    }); 

    $(document).on('click','#itm_add_btn', function(){
		return !$('#sel_itm option:selected').remove().appendTo('#sel_itm2');  		
    }); 
	$(document).on('click','#itm_all_add_btn', function(){
		return !$('#sel_itm option').remove().appendTo('#sel_itm2').prop('selected', true);  		
    }); 
	$('#itm_remove_btn').click(function() {  
		return !$('#sel_itm2 option:selected').remove().appendTo('#sel_itm');  
	 }); 
	$('#itm_remove_all_btn').click(function() {  
		return !$('#sel_itm2 option').remove().appendTo('#sel_itm').prop('selected', false);  
	 }); 
	
</script> 
</body>
</html>
