<!doctype html> 
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>overlay.css">

<!-- custom scroll -->
<script src="http://115.112.118.252/healthscreen//assets/js/jquery.mCustomScrollbar.concat.min.js"></script>
<script>
 (function($){
  $(window).load(function(){
   
   $(".Scroll_content").mCustomScrollbar({
    snapAmount:40,
    scrollButtons:{enable:true},
    keyboard:{scrollAmount:40},
    mouseWheel:{deltaFactor:40},
    scrollInertia:400
   });   
  });
 })(jQuery);
 
 	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
    });
	
 	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay2").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay2").toggle();
	}); 
	
	$(document).on('click','#somato_clear',function(){
		document.getElementById("endomorph1").value = "" ; 
		document.getElementById("mesomorph1").value = "" ; 
		document.getElementById("ectomorph1").value = "" ; 
			
	});
	
	$(document).on('click','#somato_clear2',function(){
		document.getElementById("endomorph2").value = "" ; 
		document.getElementById("mesomorph2").value = "" ; 
		document.getElementById("ectomorph2").value = "" ; 			
	});	
	
	
	
	// To Clear SDI screen
	
	
	$(document).on('click','#sdiclear', function(){            
            $("#ulscroller1 li input").each(function() {
            $(this).val('');	
            });
            $("#ulscroller2 li input").each(function() {
            $(this).val('');	
            });
			
            $("#ulscroller3 li input").each(function() {
            $(this).val('');	
            });
	});
</script>

<link rel="stylesheet" href="http://115.112.118.252/healthscreen//assets/css/jquery.mCustomScrollbar.css">
<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
.mCSB_scrollTools .mCSB_buttonDown,.mCSB_scrollTools .mCSB_buttonUp{display:none !important;}
.error_select .select_div .Scroll_content{max-height: 334px;}
</style>
</head> 
<body>

<!-- insertion of  header -->
<?=$header_insert; ?>

<div class="overlay2">&nbsp;</div>
        
<div class="info_block">
	<div class="info_block_head violet_container">Body Composition / Somatotype</div>
	<p> A somatotype is a method of describing physique or shape (not size) and was originally proposed in 1940 by Sheldon (Varieties of human physique). Somatotype is expressed as a three-number rating representing endomorphy, mesomorphy and ectomorphy components, respectively (always in that order). Endomorphy is the relative fatness, Mesomorphy is the relative musculo-skeletal robustness, and Ectomorphy is the relative linearity or slenderness of a physique.
	</p>    
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>                
</div> 

<div class="wrapper">
	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
//echo form_open('Body/skinfold_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?> 

<?php 
  
    	 $attributes = array('id' => 'myform' , 'name'=>'myform');
         echo form_open('Body/savesomatotype',$attributes); ?>


<div class="overlay" id="popUp1" style="visibility: hidden;">
            <div class="modal">
                    <div class="modal_head"><h3>Calculating somatodistance between two somatotypes [SAD]:</h3></div>                    
                    
                    <div id="somato1" class="calculation_box">                        
                        <table width="100%" style="margin-top: 20px;" cellspacing="10">
                          <tbody>
                          	<tr>
                            	<td colspan="3" align="left">
                                	Find the distance between somatotype 1:
                                </td>
                            </tr>
                            <tr>
                              <td align="left"><label>Endomorph:</label> <input type="text" id="endomorph1" name="endomorph1" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                              <td align="left"><label>Mesomorph:</label> <input type="text" id="mesomorph1" name="mesomorph1" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                              <td align="left"><label>Ectomorph:</label> <input type="text" id="ectomorph1" name="ectomorph1" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td align="left" colspan="2">
							      <button id="somato_clear" name="somato_clear" class="lite_btn f_right" onclick="" style="margin-top:110px; margin-left:10px;">Clear</button>
                                  <button id="somato_res1" name="somato_res1" class="lite_btn f_right" onclick="" style="margin-top:110px; margin-left:10px;">OK</button>
                                  <button class="lite_btn f_right" onclick="$('.overlay#popUp1').trigger('hide');" style="margin-top:110px;">Cancel</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>
                    
                    <div id="somato2" class="calculation_box" style="display:none;">                        
                        <table width="100%" style="margin-top: 20px;" cellspacing="10">
                          <tbody>
                          	<tr>
                            	<td colspan="3" align="left">
                                	And somatotype 2:
                                </td>
                            </tr>
                            <tr>
                              <td align="left"><label>Endomorph:</label> <input type="text" id="endomorph2" name="endomorph2" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                              <td align="left"><label>Mesomorph:</label> <input type="text" id="mesomorph2" name="mesomorph2" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                              <td align="left"><label>Ectomorph:</label> <input type="text" id="ectomorph2" name="ectomorph2" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" value=""></td>
                            </tr>
                            <tr>
                              <td>&nbsp;</td>
                              <td align="left" colspan="2">
							      <button id="somato_clear2" name="somato_clear2" class="lite_btn f_right" onclick="" style="margin-top:110px; margin-left:10px;">Clear</button>
                                  <button id="somato_res2" name="somato_res2" class="lite_btn f_right" style="margin-top:110px; margin-left:10px;">OK</button>
                                  <button class="lite_btn f_right" onclick="$('.overlay#popUp1').trigger('hide');" style="margin-top:110px;">Cancel</button>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                    </div>            
                
                    <div id="distance" class="calculation_box"  style="display:none; margin-top: 40px;">                                           
                        <p>2-dimentional distance(SDD) = <label id="sdd_val"></label></p>
                        <p>3-dimentional distance(SAD) = <label id="sad_val"></label></p> 
                        <button id="output" name="output" class="lite_btn" style="margin-top:150px;">OK</button>
                    </div>
            </div> 
</div>

<div class="overlay" id="popUp2" style="visibility: hidden;">
    <div class="modal">
         <div class="modal_head"> <h3>Calculating the Somatotype Dispersion Index [SDI] for a group of somatotype values :</h3></div>  
            
         <table id="sdiData" class="graph_table" style="margin: 0 20px; width: auto;">
          <tbody>                  
            <tr>            	
                <td width="46%" class="error_select">
                    <div class="select_div">  
                        <p style="color:#000;">Endomorph</p>
                    	<!--<p id="list1_items" style="color:#c60003;"></p>-->                       
                        <div class="Scroll_content">
                        <ul id="ulscroller1">
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="1"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="4"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="7"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="10"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="13"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="16"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="19"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="22"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="25"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="28"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="31"></li>
                            <li><input type="text"  onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="34"></li>
                            <li id="37"><input type="text"  onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="37"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">   
                        <p style="color:#000;">Mesomorph</p>
                    <!-- <p id="list2_items" style="color:#c60003;"></p>-->                       
                        <div class="Scroll_content">
                    	<ul id="ulscroller2">
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="2"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="5"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="8"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="11"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="14"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="17"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="20"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="23"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="26"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="29"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="32"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="35"></li>
                            <li id="38"><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="38"></li>
                        </ul> 
                        </div>
                    </div>
                    <div class="select_div">
                        <p style="color:#000;">Ectomorph</p>
                    <!-- <p id="list1_items" style="color:#c60003;"></p>-->
                        <div class="Scroll_content">
                    	<ul id="ulscroller3">
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="3"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="6"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="9"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="12"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="15"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="18"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="21"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="24"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="27"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="30"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="33"></li>
                            <li><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="36"></li>
                            <li id="39"><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*"  tabindex="39"></li>
                        </ul> 
                        </div>
                    </div>
					<button id="sdiclear" name="sdiclear" class="lite_btn f_right" style="margin-top:25px; margin-left:10px;">Clear</button>
                    <button id="compute" name="compute" class="lite_btn f_right" style="margin-top:25px; margin-left:10px;">Compute</button>
                    <button class="lite_btn f_right" style="margin-top:25px;" onclick="$('.overlay#popUp2').trigger('hide');">Cancel</button>
                </td>               
            </tr>     
           
          </tbody>
        </table>       
        
        <div id="sdi_result" class="calculation_box"  style="display:none; margin-top: 40px;">                       
            <p>Mean Somatotype = <label id="sdi_mean"></label></p>
            <p>Dispersion index = <label id="sdi"></label></p>
            <p>Variance = <label id="sdv"></label></p>
            <button id="sdi_output" name="sdi_output" class="lite_btn" style="margin-top:120px;">OK</button>
        </div>
         
    </div>
</div>

<script type="text/javascript">
    
        document.getElementById("somato_res1").addEventListener("click", function(event){             
        document.getElementById('somato2').style.display = "" ;
        document.getElementById('somato1').style.display = "none" ;
    });  
    
    document.getElementById("somato_res2").addEventListener("click", function(event){   
        
        var endomorph_score1 = document.getElementById("endomorph1").value ;        
        var mesomorph_score1 = document.getElementById("mesomorph1").value ; 
        var ectomorph_score1 = document.getElementById("ectomorph1").value ; 
                  
        var x1 = (ectomorph_score1 - endomorph_score1) ; 
        var y1 = 2 * mesomorph_score1 - endomorph_score1 - ectomorph_score1 ;
        
        var endomorph_score2 = document.getElementById("endomorph2").value ;        
        var mesomorph_score2 = document.getElementById("mesomorph2").value ; 
        var ectomorph_score2 = document.getElementById("ectomorph2").value ; 
                  
        var x2 = (ectomorph_score2 - endomorph_score2) ; 
        var y2 = 2 * mesomorph_score2 - endomorph_score2 - ectomorph_score2 ;        
        
        var sad = Math.sqrt(((endomorph_score1 - endomorph_score2) * (endomorph_score1 - endomorph_score2)) + ((mesomorph_score1 - mesomorph_score2) * (mesomorph_score1 - mesomorph_score2)) + ((ectomorph_score1 - ectomorph_score2) * (ectomorph_score1 - ectomorph_score2))) ;
        var sdd = Math.sqrt(3 * ((x1 - x2) * (x1 - x2)) + ((y1 - y2) * (y1 - y2))) ;
          
        document.getElementById('sdd_val').innerHTML = Math.round(parseFloat(sdd) * 100) / 100 ;
        document.getElementById('sad_val').innerHTML = Math.round(parseFloat(sad) * 100) / 100 ;
                
        document.getElementById('somato2').style.display = "none" ;
        document.getElementById('distance').style.display = "" ;
        
    }); 
    
    document.getElementById("output").addEventListener("click", function(event){ 
        $('.overlay#popUp1').trigger('hide');
        document.getElementById('somato1').style.display = "" ;
        document.getElementById('somato2').style.display = "none" ;
        document.getElementById('distance').style.display = "none" ;        
    }); 
    
    
    document.getElementById("sdi_output").addEventListener("click", function(event){ 
        $('.overlay#popUp2').trigger('hide');
        document.getElementById('sdiData').style.display = "" ;
        document.getElementById('sdi_result').style.display = "none" ;            
    }); 
    
    
    $(document).on('focusin', '#ulscroller1 li:last-child', function(){     
        var tabElement = this.find('input').attr("tabIndex");
        alert(tabElement); 
       //var listId_1 = this.id ;     
       //var tabVal_1 = parseInt(listId_1) + 3 ;       
       //$("#ulscroller1").append('<li id='+tabVal_1+'><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" tabIndex='+tabVal_1+'></li>');         
    });
        
    $(document).on('focusin', '#ulscroller2 li:last-child', function(){
       var listId_2 = this.id ;     
       var tabVal_2 = parseInt(listId_2) + 3 ;  
       $("#ulscroller2").append('<li id='+tabVal_2+'><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" tabIndex='+tabVal_2+'></li>');         
    });
        
    $(document).on('focusin', '#ulscroller3 li:last-child', function(){
       var listId_3 = this.id ;     
       var tabVal_3 = parseInt(listId_3) + 3 ;  
       $("#ulscroller3").append('<li id='+tabVal_3+'><input type="text" onkeypress="return event.charCode > 47 && event.charCode < 58 || event.charCode == 46;" pattern="[0-9]*" tabIndex='+tabVal_3+'></li>');         
    }); 
    
    
    // Get counts of all values for each trial columns

        $(document).on('keyup', '#ulscroller1 li input', function(){
          var items1 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var values1 = $(this).val();	
            if(values1 !== "")
              { 
                  items1.push(values1);
              }
            }); 
            
           // document.getElementById("list1_items").innerHTML = "["+items1.length+"]" ;
        });


        $(document).on('keyup', '#ulscroller2 li input', function(){
          var items2 = []; 
            
            $("#ulscroller2 li input").each(function() {
            var values2 = $(this).val();	
            if(values2 !== "")
              { 
                  items2.push(values2);
              }
            });
            
            // document.getElementById("list2_items").innerHTML = "["+items2.length+"]" ;
        });
        
        $(document).on('keyup', '#ulscroller3 li input', function(){
          var items3 = []; 
            
            $("#ulscroller3 li input").each(function() {
            var values3 = $(this).val();	
            if(values3 !== "")
              { 
                  items3.push(values3);
              }
            }); 
            
          //  document.getElementById("list3_items").innerHTML = "["+items3.length+"]" ;
        });
        
        
               
// Check for equal number of Pairs for all the 3 Trials

        $(document).on('click','#compute', function(){
            
            var trial1 = []; var trial2 = []; var trial3 = []; 
            
            $("#ulscroller1 li input").each(function() {
            var list1 = $(this).val();	
            if(list1 !== "")
              { 
                  trial1.push(list1);
              }
            });
            
            $("#ulscroller2 li input").each(function() {
            var list2 = $(this).val();	
            if(list2 !== "")
              { 
                  trial2.push(list2);
              }
            });
                        
            
            $("#ulscroller3 li input").each(function() {
            var list3 = $(this).val();	
            if(list3 !== "")
              { 
                  trial3.push(list3);
              }
            });
            
            //if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
      
      
               if(trial1.length == trial2.length && trial1.length == trial3.length && trial2.length == trial3.length && trial1.length !== 0  && trial2.length !== 0  && trial3.length !== 0)
               {                 
                   get_3tem() ;
               }
               else
               {
                   if(trial1.length == 0  && trial2.length == 0 && trial3.length == 0)
                   {
                       alert("Please enter Somatotype values");
                   }
                   else
                   {
                       alert("You must have equal number of pairs");
                   }

               }   
            
        });
        
</script>

	<div class="contain">
    	<!--
		<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
				<li><a href="<?php echo site_url('Body/changing_body_mass'); ?>" id="changing_body_mass"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Changing Body Mass</a></li>
            </ul>
        </div>
		-->
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Somatotype</div>
            <div class="field_row verticle_field_row" style="border:0; margin:0;"> 
            	<div class="field_24">
                    <p><input type="radio" id="cal_sad" name="error_radio" value="sad" onclick="$('.overlay#popUp1').trigger('show');"> <label for="cal_sad"><span style="margin:0; margin-right:8px;"></span>Calculate SAD</label></p>
                    <p><input type="radio" id="cal_sdi" name="error_radio" value="sdi" onclick="$('.overlay#popUp2').trigger('show');"> <label for="cal_sdi"><span style="margin:0; margin-right:8px;"></span>Calculate SDI</label></p>
                </div>
                    <input type="hidden" id="age" name="age" value="<?php echo $_SESSION['age'] ;?>"> 
                    <input type="hidden" id="height" name="height" value="<?php echo isset($somatotype_Values["height"])?$somatotype_Values["height"]:""; ?>"> 
                    <input type="hidden" id="body_mass" name="body_mass" value="<?php echo isset($somatotype_Values["body_mass"])?$somatotype_Values["body_mass"]:""; ?>"> 
                    <input type="hidden" id="gender" name="gender" value="<?php if($_SESSION['user_gender'] == "M"){ echo "Male" ;}else{ echo "Female" ;}?>">                   
                <div class="field_24">
                	<p><?php echo $_SESSION['user_first_name']." ".$_SESSION['user_last_name']." ".$_SESSION['age'] ;?> yr</p>
                    <p id="somato"></p>
                </div>
                
                <div class="field_24 f_right">
                    <select id="sports" class="drop_select" onchange="sportPlot()">
                    	<option>Select Sport</option>                      
                    </select>
                </div>
            </div>
            
            <div class="field_row verticle_field_row" style="border:0; margin:0;">
            	<div class="tri_shart_main"> 
                    <div id="chartdiv" style="width:100%; height:750px;">&nbsp;</div>
                </div> 
            </div>
		</div>
        <div style="display:none">
        	<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/phantom.png" alt="">Phantom</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/body_fat.png" alt="">% body fat</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/skinfolds.png" alt="">Skinfolds</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/somatotype.png" alt="">Somatotype</div>
             <div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/restricted_profile'); ?>';"><img src="<?php echo "$base/$image"?>/norms.png" alt="">Norms</div>
             <!--<div class="bottom_icons" onclick="window.location.href = '<?php  echo site_url('Body/full_profile'); ?>';"><img src="<?php echo "$base/$image"?>/fractionation.png" alt="">Fractionation</div>-->
        </div>
    </div>

             <input type="hidden" id="client_x" name="client_x" value="">
             <input type="hidden" id="client_y" name="client_y" value="">
             
             <input type="hidden" id="selectedSports_x" name="selectedSports_x">
             <input type="hidden" id="selectedSports_y" name="selectedSports_y">
             
             <input type="hidden" id="triceps" name="triceps" value="<?php echo isset($somatotype_Values["triceps"])?$somatotype_Values["triceps"]:""; ?>"> 
             <input type="hidden" id="subscapular" name="subscapular" value="<?php echo isset($somatotype_Values["subscapular"])?$somatotype_Values["subscapular"]:""; ?>">              
             <input type="hidden" id="supraspinale" name="supraspinale" value="<?php echo isset($somatotype_Values["supraspinale"])?$somatotype_Values["supraspinale"]:""; ?>"> 
             <input type="hidden" id="calf" name="calf" value="<?php echo isset($somatotype_Values["calf"])?$somatotype_Values["calf"]:""; ?>">  
             <input type="hidden" id="flexArmG" name="flexArmG" value="<?php echo isset($somatotype_Values["flexArmG"])?$somatotype_Values["flexArmG"]:""; ?>">             
             <input type="hidden" id="calfG" name="calfG" value="<?php echo isset($somatotype_Values["calfG"])?$somatotype_Values["calfG"]:""; ?>"> 
             <input type="hidden" id="humerus" name="humerus" value="<?php echo isset($somatotype_Values["humerus"])?$somatotype_Values["humerus"]:""; ?>"> 
             <input type="hidden" id="femur" name="femur" value="<?php echo isset($somatotype_Values["femur"])?$somatotype_Values["femur"]:""; ?>">         
                                     
             <input type="hidden" id="exit_key" name="exit_key" value="">
             <input type="hidden" id="action_key" name="action_key" value="">
			 
			 
			 
			 
			  <input type="hidden" id="post_endomorph" name="post_endomorph" value="">
              <input type="hidden" id="post_ectomorph" name="post_ectomorph" value="">
              <input type="hidden" id="post_mesomorph" name="post_mesomorph" value="">
			 
			 
             
	 <?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by: Professor Kevin Norton, Dr Lynda Norton</p>
    </div> 
</div>	
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
  $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
	
	$(document).on('click','#exit', function(){           
          document.getElementById("exit_key").value = 1 ;    
          document.forms["myform"].submit();
        //return false;
    }); 
	
	$(document).ready(function() {	  
		$(".overlay").overlay();
	});
</script>
<script type="text/javascript" src="<?php echo "$base/assets/js/"?>overlay.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.trendline.min.js"></script>
<link rel="stylesheet" type="text/css" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
 
   $(window).bind("load", function() {
    
    var age = document.getElementById("age").value ; 
    var ht = document.getElementById("height").value ;
    var body_mass = document.getElementById("body_mass").value ;
    
    var triceps = document.getElementById("triceps").value ;            
    var subscapular = document.getElementById("subscapular").value ;    
    var supraspinale = document.getElementById("supraspinale").value ;  
    var calf = document.getElementById("calf").value ;
    var flexArmG = document.getElementById("flexArmG").value ;  
    var calfG = document.getElementById("calfG").value ;  
    var humerus = document.getElementById("humerus").value ;
    var femur = document.getElementById("femur").value ;          
     

     var sportBF ='<?php echo json_encode($fieldData);?>';
   // alert(sportBF);   
    var options;
    var length = sportBF.length;
       
    $.each($.parseJSON(sportBF), function(key,value){
        options += '<option value="'+ value.sport +'">'+ value.sport +'</option>';        
    });


	 
    /* var sportBF = sportsList() ;
    var options;
    var length = sportBF.length;
    for(var i=0; i < length; i++)
    {
        options += '<option value="'+ sportBF[i]["sport"] +'">'+ sportBF[i]["sport"] +'</option>';
    } */
	
    $('#sports').append(options);  
    
    var total = parseFloat(triceps) + parseFloat(supraspinale) + parseFloat(subscapular) ;
    
    var endomorph_score = (-0.7182 + 0.1451 * (total) - 0.00068 * (total * total) + 0.0000014 * (total * total * total)) ;  
    var mesomorph_score = 0.858 * parseFloat(humerus) + 0.601 * parseFloat(femur) + 0.188 * (parseFloat(flexArmG) - (parseFloat(triceps) / 10)) + 0.161 * (parseFloat(calfG) - (parseFloat(calf) / 10)) - 0.131 * parseFloat(ht) + 4.5 ;
    
	
	// Additional Calculation  , Dt: 11 oct 17
    //if HWR > 38.25 and HWR <40.75 then ECTOMORPH score = .463* HWR -17.63  
	
    var hwr = parseFloat(ht) / (Math.cbrt(parseFloat(body_mass))) ; 
    
    if(parseFloat(hwr) >= 40.75)
    {
     var ectomorph_score = 0.732 * parseFloat(hwr) - 28.58 ;  
    }
    /* 
	OLD CODE
	else if(parseFloat(hwr) > 38.25 && parseFloat(hwr) < 40.75)
    {
     var ectomorph_score = 463 * parseFloat(hwr) - 17.63 ;    
    }  */
    else if(parseFloat(hwr) < 38.25)
    {
     var ectomorph_score = 0.1 ;       
    } 
    else if(parseFloat(hwr) > 38.25 && parseFloat(hwr) < 40.75 )  // added condition    
    {
     var ectomorph_score = .463 * parseFloat(hwr) - 17.63 ; 
    }    
	
	
	
	document.getElementById("post_endomorph").value=(Math.round(endomorph_score * 100) / 100);
	document.getElementById("post_ectomorph").value=(Math.round(ectomorph_score * 100) / 100);
	document.getElementById("post_mesomorph").value=(Math.round(mesomorph_score * 100) / 100);
	
	
	
    
    document.getElementById("somato").innerHTML = (Math.round(endomorph_score * 100) / 100)+", "+(Math.round(mesomorph_score * 100) / 100)+", "+(Math.round(ectomorph_score * 100) / 100) ;    
   
    var client_x = (ectomorph_score - endomorph_score) ; 
    var client_y = 2 * mesomorph_score - (endomorph_score + ectomorph_score) ;
  
    document.getElementById("client_x").value = parseFloat(client_x) ;
    document.getElementById("client_y").value = parseFloat(client_y) ;  
    
    $.jqplot.config.enablePlugins = true;      
    var chartData1 = [[client_x,client_y]] ;      
    var plot1 = $.jqplot('chartdiv', [chartData1], {
          
        axes: {
        xaxis: {            
          ticks : ['-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],           
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,           
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['-10',  '-8', '-6', '-4','-2', '0', '2', '4', '6', '8', '10', '12', '14', '16'],        
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{           
             shadow:false,            
             drawBorder: false,
             drawGridlines:true,
             background: 'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     markerOptions: {                                                                     
                        size: 22
                     },
                     shadow: false                    
                   },
	    highlighter: {
                show: true,
                sizeAdjust: 3,               
                tooltipContentEditor:tooltipContentClient
               }  
     
      });    
      
    document.getElementById("endomorph1").value = Math.round(parseFloat(endomorph_score) * 100) / 100 ;
    document.getElementById("mesomorph1").value = Math.round(parseFloat(mesomorph_score) * 100) / 100 ;  
    document.getElementById("ectomorph1").value = Math.round(parseFloat(ectomorph_score) * 100) / 100 ;  

	mytitle = $('<div class="my-jqplot-title" style="font-weight:bold;position:absolute;text-align:center;padding-top: 8px;width:100%">Mesomorphy</div>').insertAfter('.jqplot-grid-canvas');
    mytitle = $('<div class="my-jqplot-title" style="font-weight:bold;position:absolute;text-align:center;padding-top:71%;width:100%; right:-42%;">Ectomorphy</div>').insertAfter('.jqplot-grid-canvas');
    mytitle = $('<div class="my-jqplot-title" style="font-weight:bold;position:absolute;text-align:center;padding-top:71%;width:100%; right:42%;">Endomorphy</div>').insertAfter('.jqplot-grid-canvas');
    
}); 


function sportPlot()
{
    var chartDiv=document.getElementById('chartdiv');
        
    if(chartDiv.innerHTML !== "")
     {         
        while (chartDiv.hasChildNodes())
        {
         chartDiv.removeChild(chartDiv.firstChild);
        }        
     }
    
    var client_x = document.getElementById("client_x").value ;
    var client_y = document.getElementById("client_y").value ;      
    var sportVal = document.getElementById("sports").value ;
    // Added
	var sport_x=0.0;
    var sport_y=0.0;
	
	var sportBF = '<?php echo json_encode($fieldData);?>';
    var length = sportBF.length;

    
    $.each($.parseJSON(sportBF), function(key,value){
      if(value.sport == sportVal)
       {
           sport_x =( parseFloat(value.ectomorph) - parseFloat(value.endomorph) ) ; 
           sport_y = (2 * parseFloat(value.mesomorph) - ( parseFloat(value.endomorph) + parseFloat(value.ectomorph) ) )  ; 
           
           console.log(sport_x);
           console.log(sport_y);
           
           document.getElementById("endomorph2").value = parseFloat(value.endomorph) ;
           document.getElementById("mesomorph2").value = parseFloat(value.mesomorph) ;  
           document.getElementById("ectomorph2").value = parseFloat(value.ectomorph) ;  
           
       }
    });
	// Added
	
	
	
	
	
    /* var sportBF = sportsList() ;    
    var length = sportBF.length;
    for(var i=0; i < length; i++)
    {
       if(sportBF[i]["sport"] == sportVal)
       {
         var sport_x = ((sportBF[i]["ectomorph"]) - (sportBF[i]["endomorph"])) ; 
         var sport_y = (2 * (sportBF[i]["mesomorph"]) - ((sportBF[i]["endomorph"]) + (sportBF[i]["ectomorph"]))) ;  
         
         document.getElementById("endomorph2").value = parseFloat(sportBF[i]["endomorph"]) ;
         document.getElementById("mesomorph2").value = parseFloat(sportBF[i]["mesomorph"]) ;  
         document.getElementById("ectomorph2").value = parseFloat(sportBF[i]["ectomorph"]) ;  
         
       }    
    } */
	
	
     
    if(document.getElementById("selectedSports_x").value == "")
    {    
     document.getElementById("selectedSports_x").value = sport_x ; 
    }
    else
    {
     document.getElementById("selectedSports_x").value += ","+sport_x ; 
    }    
    var sportList_x =  document.getElementById("selectedSports_x").value ;
    var x_array = sportList_x.split(',');   
    
    
    if(document.getElementById("selectedSports_y").value == "")
    {    
     document.getElementById("selectedSports_y").value = sport_y ; 
    }
    else
    {
     document.getElementById("selectedSports_y").value += ","+sport_y ; 
    }
    var sportList_y =  document.getElementById("selectedSports_y").value ;
    var y_array = sportList_y.split(',');  
    
    if(x_array.length == y_array.length)
    {
       var coordLength = x_array.length ; 
    }    
    
    var coordinates = new Array() ; 
    for(var i = 0; i < coordLength; i++)
    {
     coordinates.push([x_array[i],y_array[i]]);  
    }    
    
    
// plot sports    
    $.jqplot.config.enablePlugins = true;      
    var chartData1 = [[parseFloat(client_x),parseFloat(client_y)]] ;
    //var chartData2 = [coordinates] ;       
    var plot1 = $.jqplot('chartdiv', [chartData1, coordinates], {
        
        axes: {
        xaxis: {            
          ticks : ['-9', '-8', '-7', '-6', '-5', '-4', '-3', '-2', '-1', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'],           
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,           
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['-10',  '-8', '-6', '-4','-2', '0', '2', '4', '6', '8', '10', '12', '14', '16'],        
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{           
             shadow:false,            
             drawBorder: false,
             drawGridlines:true,
             background: 'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                                         
                     shadow: false                    
                   },
        series: [
            {
             showLine:false,                     
                     markerOptions: {                                                                     
                        size: 22
                     },
             shadow: false,
             highlighter: {
                show: true,
                sizeAdjust: 3,               
                tooltipContentEditor:tooltipContentClient
               }       
            },
            {
            showLine:false,                     
                     markerOptions: {                                                                     
                        size: 16
                     },
            shadow: false,
            highlighter: {
                show: true,
                sizeAdjust: 3,               
                tooltipContentEditor:tooltipContentEditor
               }          
            }                      
        ]   
      });  
  }


function tooltipContentClient(str, seriesIndex, pointIndex, plot) {
    var data = document.getElementById("somato").innerHTML ;    
    return data  ;
}   

function tooltipContentEditor(str, seriesIndex, pointIndex, plot) {
	
	
	// Added
	var sportVal = document.getElementById("sports").value ;
    
    var sportBF = '<?php echo json_encode($fieldData);?>';
    var length = sportBF.length;

	var data=0.0;
    $.each($.parseJSON(sportBF), function(key,value){
     
           var sport_x = (parseFloat(value.ectomorph) - parseFloat(value.endomorph)) ; 
           var sport_y = (2 * (parseFloat(value.mesomorph)) - (parseFloat(value.endomorph) + parseFloat(value.ectomorph) ) ) ; 
           
           if(sport_x == plot.data[seriesIndex][pointIndex][0]  && sport_y == plot.data[seriesIndex][pointIndex][1])
            {
              data = value.sport +": "+parseFloat(value.endomorph) +" - "+parseFloat(value.mesomorph) +" - "+parseFloat(value.ectomorph) ;           
            }
     
    });
	// Added
	
	
	
	
	
    /* var sportBF = sportsList() ;    
    var length = sportBF.length;
    for(var i=0; i < length; i++)
    {
       var sport_x = ((sportBF[i]["ectomorph"]) - (sportBF[i]["endomorph"])) ; 
       var sport_y = (2 * (sportBF[i]["mesomorph"]) - ((sportBF[i]["endomorph"]) + (sportBF[i]["ectomorph"]))) ; 
       
       if(sport_x == plot.data[seriesIndex][pointIndex][0]  && sport_y == plot.data[seriesIndex][pointIndex][1])
       {
           var data = sportBF[i]["sport"]+": "+sportBF[i]["endomorph"]+" - "+sportBF[i]["mesomorph"]+" - "+sportBF[i]["ectomorph"] ;           
       }
    } */
	
	
    return data ;
}




function sportsList()
{
   if(document.getElementById("gender").value == "Male")
   {    
      var sportBF= '<?php echo json_encode($fieldData); ?>';       
       //alert(sportBF);       
       console.log(sportBF);

      /* var sportBF = [
                        { sport: "American football [running back]", endomorph: 2.18, mesomorph: 6.27, ectomorph: 1.16},
                        { sport: "American football [linemen]", endomorph: 6.46, mesomorph: 6.07, ectomorph: 1.03},
                        { sport: "American football [quarterback]", endomorph: 4.96, mesomorph: 5.05, ectomorph: 2.79},
                        { sport: "American football [wide receiver]", endomorph: 2.31, mesomorph: 4.77, ectomorph: 2.46},                     
                        { sport: "Australian football [midfield]", endomorph: 2.10, mesomorph: 5.92, ectomorph: 2.32},
                        { sport: "Australian football [general]", endomorph: 2.30, mesomorph: 5.44, ectomorph: 2.59},
                        { sport: "Australian football [ruckmen]", endomorph: 3.65, mesomorph: 3.87, ectomorph: 5.46},                       
                        { sport: "Badminton", endomorph: 2.50, mesomorph:4.60, ectomorph: 3.20},
                        { sport: "Baseball", endomorph: 2.97, mesomorph: 5.77, ectomorph: 1.90},
                        { sport: "Basketball [guards]", endomorph: 2.40, mesomorph: 4.45, ectomorph: 3.65},
                        { sport: "Basketball [forwards]", endomorph: 2.45, mesomorph: 4.55, ectomorph: 3.70},
                        { sport: "Basketball [centres]", endomorph: 2.55, mesomorph:4.11, ectomorph: 3.70},                                                 
                        { sport: "Bodybuilding", endomorph: 1.30, mesomorph: 8.45, ectomorph: 1.08},
                        { sport: "Boxing - general", endomorph: 2.3, mesomorph: 5.41, ectomorph: 2.51},                        
                        { sport: "Canoeing (Canadian)", endomorph: 1.9, mesomorph: 5.6, ectomorph: 2.3},                        
                        { sport: "Cycling [track sprint]", endomorph:2.08, mesomorph: 7.18, ectomorph: 1.09},
                        { sport: "Cycling [off-road]", endomorph: 1.55, mesomorph: 4.77, ectomorph: 2.68},
                        { sport: "Cycling [road]", endomorph: 1.57, mesomorph: 4.53, ectomorph: 3.00},
                        { sport: "Dance", endomorph: 2.5, mesomorph: 3.85, ectomorph: 3.45},
                        { sport: "Decathlon", endomorph: 1.9, mesomorph: 5.3, ectomorph: 2.5},
                        { sport: "Discus", endomorph: 5.03, mesomorph: 6.11, ectomorph: 1.65},                        
                        { sport: "Diving", endomorph: 1.95, mesomorph: 5.35, ectomorph: 2.55},
                        { sport: "Fencing", endomorph: 2.47, mesomorph: 4.89, ectomorph: 2.46},
                        { sport: "Gymnastics", endomorph: 1.78, mesomorph: 6.15, ectomorph: 2.25},
                        { sport: "handball [wing]", endomorph: 2.42, mesomorph: 5.12, ectomorph: 2.76},
                        { sport: "Hockey (field)", endomorph: 2.9, mesomorph: 4.33, ectomorph: 2.7},
                        { sport: "Hockey (ice)", endomorph: 2.71, mesomorph: 5.6, ectomorph: 1.86},                        
                        { sport: "Judo", endomorph: 2.72, mesomorph: 6.29, ectomorph: 1.51},
                        { sport: "Jumping - general", endomorph: 1.75, mesomorph: 5.00, ectomorph: 3.15},
                        { sport: "Karate", endomorph: 2.6, mesomorph: 5.2, ectomorph: 2.6},
                        { sport: "Kayak - sprint", endomorph: 1.95, mesomorph: 5.4, ectomorph: 2.3},
                        { sport: "Kayak - slalom", endomorph: 2.33, mesomorph: 5.17, ectomorph: 2.6},
                        { sport: "Kayak - general", endomorph: 1.88, mesomorph: 5.4, ectomorph: 2.68},
                        { sport: "Lacrosse", endomorph: 2.9, mesomorph: 5.4, ectomorph: 2.5},
                        { sport: "Orienteering", endomorph: 1.95, mesomorph: 4.65, ectomorph: 3.45},
                        { sport: "Parachuting", endomorph: 3.4, mesomorph: 4.9, ectomorph: 1.8},
                        { sport: "Pentathlon", endomorph: 2.00, mesomorph: 5.3, ectomorph: 2.4},
                        { sport: "Powerlifting lightweight", endomorph: 2.10, mesomorph: 7.9, ectomorph: 0.6},   
                        { sport: "Powerlifting middleweight", endomorph: 2.2, mesomorph: 8.2, ectomorph: 0.5},   
                        { sport: "Powerlifting heavyweight", endomorph: 4.2, mesomorph: 9.2, ectomorph: 0.3},                      
                        { sport: "Rockclimbing", endomorph: 1.9, mesomorph: 4.9, ectomorph: 3.5},
                        { sport: "Rollerskating", endomorph: 1.2, mesomorph: 5.6, ectomorph: 3.00},
                        { sport: "Rowing - heavyweight", endomorph: 2.23, mesomorph: 5.31, ectomorph: 2.61},
                        { sport: "Rowing - lightweight", endomorph: 1.4, mesomorph: 4.8, ectomorph: 3.22},
                        { sport: "Rugby Union", endomorph: 2.89, mesomorph: 5.9, ectomorph: 1.8},
                        { sport: "Rugby Union [backs]", endomorph: 2.23, mesomorph: 5.43, ectomorph: 2.02},
                        { sport: "Running - sprint", endomorph: 1.7, mesomorph: 4.5, ectomorph: 3.5},
                        { sport: "Running - middle distance", endomorph: 1.5, mesomorph: 4.03, ectomorph: 3.83},
                        { sport: "Running - distance", endomorph: 1.6, mesomorph: 4.4, ectomorph: 3.55},
                        { sport: "Skating - figure", endomorph: 1.7, mesomorph: 5.00, ectomorph: 2.90},
                        { sport: "Skiing - cross-country", endomorph: 1.97, mesomorph: 5.35, ectomorph: 2.68},
                        { sport: "Skiing - downhill", endomorph: 2.07, mesomorph: 5.3, ectomorph: 2.63},
                        { sport: "Skijumping", endomorph: 2.2, mesomorph: 6.25, ectomorph: 2.08},                        
                        { sport: "Soccer", endomorph: 2.46, mesomorph: 4.62, ectomorph: 2.65},
                        { sport: "Squash", endomorph: 2.5, mesomorph: 5.2, ectomorph: 2.8},
                        { sport: "Surfing", endomorph: 2.6, mesomorph: 5.2, ectomorph: 2.6},
                        { sport: "Swimming", endomorph: 2.69, mesomorph: 5.15, ectomorph: 2.56},
                        { sport: "Table tennis", endomorph: 3.5, mesomorph: 3.9, ectomorph: 2.5},
                        { sport: "Tennis", endomorph: 2.2, mesomorph: 4.45, ectomorph: 3.1},
                        { sport: "Throwing - general", endomorph: 3.13, mesomorph: 6.73, ectomorph: 1.43},
                        { sport: "Triathlon", endomorph: 1.7, mesomorph: 4.3, ectomorph: 3.1},
                        { sport: "Volleyball", endomorph: 2.29, mesomorph: 4.4, ectomorph: 3.29},
                        { sport: "Walking", endomorph: 1.7, mesomorph: 4.7, ectomorph: 3.55},
                        { sport: "Waterpolo", endomorph: 2.73, mesomorph: 5.33, ectomorph: 2.28},
                        { sport: "Weightlifting", endomorph: 2.96, mesomorph: 7.05, ectomorph: 1.06},
                        { sport: "Wrestling", endomorph: 2.85, mesomorph: 6.29, ectomorph: 1.49}
                                           ];  */
                  
    }
    else if(document.getElementById("gender").value == "Female")
    {
		
		var sportBF= '<?php echo json_encode($fieldData); ?>';       
       //alert(sportBF);       
       console.log(sportBF);
		
      /*  var sportBF = [                        
                        { sport: "Badminton", endomorph: 3.6, mesomorph:4.4, ectomorph: 2.5},                        
                        { sport: "Basketball [guards]", endomorph: 2.9, mesomorph: 3.9, ectomorph: 2.9},
                        { sport: "Basketball [forwards]	", endomorph: 2.8, mesomorph: 3.5, ectomorph: 3.2}, 
                        { sport: "Basketball [centres]", endomorph: 3.2, mesomorph: 3.1, ectomorph: 3.4},                                                
                        { sport: "Bodybuilding", endomorph: 2.48, mesomorph: 5.1, ectomorph: 2.5},                                           
                        { sport: "Cricket", endomorph: 4.9, mesomorph: 4.4, ectomorph: 2.00},
                        { sport: "Cycling [track sprint]", endomorph: 1.83, mesomorph: 5.25, ectomorph: 2.4}, 
                        { sport: "Cycling [off-road]", endomorph: 1.8, mesomorph: 4.5, ectomorph:3.4},  
                        { sport: "Cycling [road]", endomorph: 2.8, mesomorph: 3.4, ectomorph: 2.8},                        
                        { sport: "Dance [standard]", endomorph: 3.8, mesomorph: 3.26, ectomorph: 3.33},                        
                        { sport: "Diving", endomorph: 2.85, mesomorph: 3.95, ectomorph: 2.85},
                        { sport: "Fencing", endomorph: 3.6, mesomorph: 3.6, ectomorph: 2.4},
                        { sport: "Golf", endomorph: 4.5, mesomorph: 4.3, ectomorph: 2.4},
                        { sport: "Gymnastics [rhythmic]", endomorph: 3.1, mesomorph: 4.05, ectomorph: 3.00},
                        { sport: "Handball [goalkeeper]", endomorph: 3.75, mesomorph: 4.23, ectomorph: 2.45},
                        { sport: "Hockey (field)", endomorph: 3.45, mesomorph: 4.1, ectomorph: 2.35},                                                
                        { sport: "Jumping - general", endomorph: 2.3, mesomorph: 3.05, ectomorph: 4.00},                        
                        { sport: "Kayak - slalom", endomorph: 3.7, mesomorph: 3.3, ectomorph: 2.5},
                        { sport: "Kayak - general", endomorph: 3.5, mesomorph: 4.5, ectomorph: 2.5},
                        { sport: "Lacrosse", endomorph: 4.1, mesomorph: 4.5, ectomorph: 2.4},
                        { sport: "Netball", endomorph: 3.5, mesomorph: 3.57, ectomorph: 2.93},
                        { sport: "Orienteering", endomorph: 3.4, mesomorph: 4.00, ectomorph: 2.8},                        
                        { sport: "Pentathlon", endomorph: 2.5, mesomorph: 3.7, ectomorph: 3.1},  
                        { sport: "Rockclimbimg ", endomorph: 2.5, mesomorph: 4.15, ectomorph: 3.66}, 
                        { sport: "Rowing heavyweight ", endomorph: 2.90, mesomorph: 3.6, ectomorph: 2.83}, 
                        { sport: "Rowing lightweight ", endomorph: 1.9, mesomorph: 4.0, ectomorph: 3.10},                                     
                        { sport: "Rugby Union [forwards]", endomorph: 4.5, mesomorph: 4.55, ectomorph: 1.8},
                        { sport: "Rugby Union [backs]", endomorph: 3.68, mesomorph: 4.01, ectomorph: 2.33},
                        { sport: "Running - sprint", endomorph: 2.68, mesomorph: 3.95, ectomorph: 3.15},
                        { sport: "Running - middle distance", endomorph: 2.14, mesomorph: 3.42, ectomorph: 3.76},
                        { sport: "Running - distance", endomorph: 3.02, mesomorph: 3.47, ectomorph: 3.95},                        
                        { sport: "Skiing - cross-country", endomorph: 3.5, mesomorph: 4.3, ectomorph: 2.3},                        
                        { sport: "Soccer", endomorph: 4.2, mesomorph: 4.6, ectomorph: 2.2},
                        { sport: "Softball", endomorph: 3.8, mesomorph: 4.3, ectomorph: 2.7},
                        { sport: "Squash", endomorph: 3.4, mesomorph: 4.00, ectomorph: 2.8},
                        { sport: "Surfing", endomorph: 3.9, mesomorph: 4.1, ectomorph: 2.6},
                        { sport: "Swimming", endomorph: 2.9, mesomorph: 3.9, ectomorph: 3.00},
                        { sport: "Synchronised swimming", endomorph: 3.3, mesomorph: 3.5, ectomorph: 3.1},
                        { sport: "Table tennis", endomorph: 4.5, mesomorph: 3.3, ectomorph: 2.7},
                        { sport: "Tennis", endomorph: 3.17, mesomorph: 3.53, ectomorph: 3.1},
                        { sport: "Throwing - general", endomorph: 3.4, mesomorph: 4.00, ectomorph: 2.9},
                        { sport: "Triathlon", endomorph: 3.1, mesomorph: 4.3, ectomorph: 2.6},
                        { sport: "Volleyball", endomorph: 3.23, mesomorph: 3.7, ectomorph: 3.03},                        
                        { sport: "Waterpolo", endomorph: 3.6, mesomorph: 3.9, ectomorph: 2.8}                                          
                  ];*/
    }
    
    return sportBF ;
}



 // Calculation with 3 Somatotype Columns
    function get_3tem()
    {
       var endo = [] ; // Trial1 values
       var meso = [] ; // Trial2 values
       var ecto = [] ; // Trial3 values

      // Sum of Endomorph values  
       var sum1 = 0;      
        $("#ulscroller1 li input").each(function() {
          var items1 = $(this).val();	
          if(items1 !== "")
          { 
            sum1 = sum1 + parseFloat(items1) ;            
            endo.push(items1);  
          }
         }); 


     // Sum of Mesomorph values      
       var sum2 = 0;        
        $("#ulscroller2 li input").each(function() {
          var items2 = $(this).val();	
          if(items2 !== "")
          { 
            sum2 = sum2 + parseFloat(items2) ;           
            meso.push(items2);  
          }
         });
         

     // Sum of Ectomorph values      
       var sum3 = 0;           
        $("#ulscroller3 li input").each(function() {
          var items3 = $(this).val();	
          if(items3 !== "")
          { 
            sum3 = sum3 + parseFloat(items3) ;            
            ecto.push(items3);  
          }
         });

         var N1 = endo.length ;
         var N2 = meso.length ;
         var N3 = ecto.length ;

         var endoMean = sum1 / N1 ;
         var mesoMean = sum2 / N2 ; 
         var ectoMean = sum3 / N3 ;          

         var x1 = (ectoMean - endoMean) ; 
         var y1 = 2 * mesoMean - endoMean - ectoMean ;
                
         var index ; var SDDSquared ; var SDD ; var TotSDD = 0 ; var TotSDDSquared = 0 ; 
         for(index = 0; index < endo.length; ++index) 
         {
           var endo2 = endo[index] ;
           var meso2 = meso[index] ; 
           var ecto2 = ecto[index] ; 
           
           var x2 = (ecto2 - endo2) ; 
           var y2 = 2 * meso2 - endo2 - ecto2 ;
           
           SDDSquared = (3 * ((x1 - x2)*(x1 - x2)) + ((y1 - y2)*(y1 - y2))) ;
           SDD = Math.sqrt(SDDSquared) ;
           //alert(SDD) ;
           TotSDD = TotSDD + SDD ;
           TotSDDSquared = TotSDDSquared + SDDSquared ;          
         }  
        
         
         var SDI = TotSDD / N1 ;
             SDI = Math.round(SDI * 100) / 100 ; //Somatotype Dispersion Index
         var SDV = (TotSDDSquared - (TotSDD * TotSDD) / N1) /(N1 - 1) ;
             SDV = Math.round(SDV * 100) / 100 ; //Somatotype Dispersion Variance
         
         endoMean = Math.round(endoMean * 100) / 100 ; 
         mesoMean = Math.round(mesoMean * 100) / 100 ; 
         ectoMean = Math.round(ectoMean * 100) / 100 ; 
         
         document.getElementById('sdi_mean').innerHTML = parseFloat(endoMean)+"-"+parseFloat(mesoMean)+"-"+parseFloat(ectoMean) ;
         document.getElementById('sdi').innerHTML = Math.round(parseFloat(SDI) * 100) / 100 ;
         document.getElementById('sdv').innerHTML = Math.round(parseFloat(SDV) * 100) / 100 ;
         
         document.getElementById('sdi_result').style.display = "" ;
         document.getElementById('sdiData').style.display = "none" ; 
     }

</script>
</body>
</html>
