<?php 
			 //To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
            $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
            $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
            $bodyfatdiv="block";    
            }
            else
            {
             $bodyfatdiv="none";    
            }
   $is_dynamic=$_SESSION['is_virtual']; 
  ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">

<script type="text/javascript">	
	window.onload = function() {
	    var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
		 $("#weight").val(<?php echo $_SESSION['MASS'];?>);
         $("#age").val(<?php echo $_SESSION['vp_age'];?>);  
         $("#max_hr").val(<?php echo $_SESSION['HRmax'];?>);  
         $("#workload1").val(<?php echo $_SESSION['stage1WATT'];?>);
         $("#heart_rate1").val(<?php echo $_SESSION['stage1HR'];?>);
         $("#rpe1").val(<?php echo $_SESSION['stage1RPE'];?>);
         $("#workload2").val(<?php echo $_SESSION['stage2WATT'];?>);
         $("#heart_rate2").val(<?php echo $_SESSION['stage2HR'];?>);
         $("#rpe2").val(<?php echo $_SESSION['stage2RPE'];?>);
         $("#workload3").val(<?php echo $_SESSION['stage3WATT'];?>);
         $("#heart_rate3").val(<?php echo $_SESSION['stage3HR'];?>);
         $("#rpe3").val(<?php echo $_SESSION['stage3RPE'];?>);
        } 
    
	};
	
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		 var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
		$(".v_person").fadeTo( "slow" , 1, function() {}); 
		$(".v_detail").toggle();
		}
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	$(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	$(document).on('click','.menu_btn', function(){
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	 $(document).on('click','.vitual_btn', function(){
                  $("#vpvo2max").attr("action", "<?php echo base_url(); ?>index.php/Fitness/FitnessVirtualpersonGeneration");     
                  $("#vpvo2max").submit();       
         $(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
               return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	});
</script>
</head>

<body>
  <div class="v_person">
	 <a href="#" class="discart" title="Exit virtual person">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm]</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],2);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg]</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat</label><input type="text" name="body_fat" value="">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk group</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [mL/kg/min]</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
  
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container green_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" title="Back"><img src="<?php echo "$base/assets/images/"?>back_green.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Fitness Testing</h3>
             <p>Aerobic fitness / VO<sub>2max</sub> [mL/kg/min] / submaximal tests</p>
        </div>
        
        
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_green.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_green.jpg"></a>
        </div>
         <div class="orng_box_btn f_right">
        	<form name="vpform" id="vpvo2max" method="post"> 
        	<a href="#" class="vitual_btn" title="Generate new VP profile"><img src="<?php echo "$base/assets/images/"?>virtual_icon.png" style="margin-top: 12px;"></a>
             <input type="hidden" name="vptype" value="vpvo2max">
                </form>
         </div>
		
		<div class="vitual_div">
            </div>    
		<div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
        		<div class="info_block_head">Predicted VO<sub>2max</sub> and PWC</div>
                    <p>These prediction tests typically involve 3 or 4 stages on a bicycle ergometer. You can use this screen to predict VO2max using an automated age-predicted or user-entered maximal heart rate [or RPE], or calculate a PWC by entering the heart rate 
					   to be used [usually 150 or 170 BPM] in the ‘Known HRmax’ data entry field. Collect workload at the end of each stage (Watts) as well as heart rate (BMP) and rating of perceived exertion (use Borg's 6-20 RPE scale). The RPE scale is shown below:
                       At the end of each exercise period the client should also rate their perception of exercise. That is, how heavy and strenuous the exercise feels to them. The perception of exertion depends on a range of input variables that are coming from
                       various sensory / pain receptions and assimilated in the brain. Much of the sensory input is based on the strain and fatigue in the muscles and on feelings of breathlessness, aches in the chest and perception of heaviness. The client should be 
                       shown the RPE rating scale and asked: "we want you to use this scale from 6 to 20, where 6 means 'no exertion at all' and 20 means 'maximal exertion'. Values can be entered as half-units if necessary [such as 11.5].
                       Try to summarise your feeling of exertion as honestly as possible, without thinking about what the actual physical load is. It's your own feeling of effort and exertion that's important. Look at the scale and the expressions and then give a number according to the RPE table below":
                    </p>     
                    <br/>
                    6 No exertion at all<br/>
                    7 Extremely light<br/>8<br/>
                    9 Very light &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Normal, healthy person walking slowly at own pace for some minutes<br/>
                    10<br/>
                    11 Light<br/>
                    12<br/>
                    13 Somewhat hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Somewhat hard but still feels OK to continue<br/>
                    14<br/>
                    15 Hard (heavy)<br/>
                    16<br/>
                    17 Very hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Very strenuous. A healthy person can still go on, but really has to push him/herself. Feels very heavy and the person is very tired<br/>
                    18<br/>
                    19 Extremely hard &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Extremely strenuous exercise level. For most people the most strenuous exercise they have ever experienced<br/>
                    20 Maximal exertion<br/><br/>
                    <p>It is suggested that you use the range 9-15 for most people unless they are reasonably well trained in which case you might consider the range 9-17.</p>
                    <div class="info_block_foot">
                        <a href="#" class="lite_btn grey_btn f_right close">Close</a>
                     </div>  
                </div> 
    
    </div>
</div>

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientSubmaximalInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?> 
	
    <div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
            	<li><a href="#" id="anaerobic_strength"><img src="<?php echo "$base/assets/images/"?>icon_anaerobic.png"> Anaerobic strength and power</a>
                	<ul class="sub_menu">
                    	<li><a href="<?php echo site_url('Fitness/vertical_jump'); ?>">vertical jump</a></li>
                        <li><a href="<?php echo site_url('Fitness/flight_time'); ?>">flight:contact time</a></li>
                        <li><a href="<?php echo site_url('Fitness/peak_power'); ?>">peak power [W]</a></li>
                        <li><a href="<?php echo site_url('Fitness/strength'); ?>">strength</a></li>
                    </ul>
                </li>
                <li><a href="#" id="anaerobic_capacity"><img src="<?php echo "$base/assets/images/"?>icon_capacity.png"> Anaerobic capacity</a>
                	<ul class="sub_menu">
                      <li><a href="<?php echo site_url('Fitness/total_work_done_30s'); ?>">30 s total work [kJ]</a></li>
					  <li><a href="<?php echo site_url('Fitness/maod_test'); ?>">maximal accumulated oxygen deficit [MAOD]</a></li>
                    </ul>
                </li>
                <li><a href="#" id="aerobic_fitness"><img src="<?php echo "$base/assets/images/"?>icon_fitness.png"> Aerobic fitness</a>
                		<ul class="sub_menu">
                    	<li><a href="#" id="VO2max">VO<sub>2max</sub> [mL/kg/min]</a>
                        	<ul class="inner_sub_menu">
                                <li><a href="<?php echo site_url('Fitness/submaximal_test'); ?>">submaximal</a></li>
                                <li><a href="#" id="max-txt">maximal tests</a>
									<ul class="inner_sub_menu2">
										<li><a href="<?php echo site_url('Fitness/shuttle_test'); ?>">20 m shuttle</a></li>
										 <li><a href="<?php echo site_url('Fitness/bike_test'); ?>">Predicting VO2max using maximal-effort treadmill or bike tests</a></li>
									</ul>
								</li>
								
                                <li><a href="<?php echo site_url('Fitness/v02max_test'); ?>">measured VO<sub>2max</sub></a></li>
                            </ul>
                        </li>
                        <li><a href="<?php echo site_url('Fitness/lactate_threshold'); ?>">lactate threshold</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Predicted VO<sub>2max</sub> and PWC</div>
        	 <div class="side_25_per">
             	<div class="field_row" style="border:0;">
                      <div class="field_100 gen" style="visibility:hidden;">
                        <input type="radio" name="gender" id="gender_Male" value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "checked";}?>> <label for="gender_Male" style="float:left; margin-right:20px;"><span style="margin-right:8px;"></span> Male</label>
                    <input type="radio" name="gender" id="gender_Female" value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "checked";}?>><label for="gender_Female" style="float:left;"><span style="margin-right:8px;"></span>Female</label>
                      </div>  
                      
                    <div class="field_100">
                    	<label>Age [yr]</label>
                        <input type="text" name="age" id="age"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->age)?$fieldData[0]->age:$_SESSION['age']; ?>">               
                    </div>    
                    
                    <div class="field_100">
                    	<label>Body mass [kg]</label>
                         <input type="text" name="weight" id="weight" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->weight)?$fieldData[0]->weight:""; ?>">               
                    </div> 
              </div>
              
              	<div class="field_row" style="border:0;">   
                	 <div class="field_100">
                    	 <strong>Predicted values</strong>
                    </div>                  
                    <div class="field_100">
                        <div id="heatRateVal" class="predicted_value">
                        	<label>Max HR [BPM]</label>
                        	<input type="text" name="max_hr" id="max_hr" readonly> 
                        </div>
                        <div id="rpeVal" style="display: none;" class="predicted_value">
                         <label>Max RPE</label>
                         <input type="text" name="max_rpe" id="max_rpe" readonly>
                        </div>  
                    </div>   
                    
                    <div class="field_100">
                    	<div class="predicted_value">
                        <label>Max Power [Watts]</label>
                        <input type="text" name="max_wl" id="max_wl" readonly> 
                        </div>         
                    </div>
                    
                    <div class="field_100">
                    	<div class="predicted_value">
                        <label>VO<sub>2max </sub> [mL/kg/min]</label>
                        <input type="text" name="vo_max" id="vo_max" readonly> 
						
						<input type="hidden" name="vo_max_hr" id="vo_max_hr" readonly> 
						<input type="hidden" name="vo_max_rpe" id="vo_max_rpe" readonly> 
						
                        </div>      
                    </div>
              </div>              
             
              	<div class="field_row" style="border:0;">                    
                    <div class="field_100">
                        <label><strong><span id="maxVal" style="float:none;">Known HRmax</span> <span id="known_max_hr_val" style="float:none;">[BPM]</span></strong></label>
                        <div class="predicted_value">
                            <input class="cus-input" type="text" name="known_max_hr" id="known_max_hr"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" onkeyUp="regHeartRate()"> 
                            <input class="cus-input" type="text" name="known_max_rpe" id="known_max_rpe"  onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" onkeyUp="regRPE()" style="display: none;">
                        </div>
                        
                	</div>
             </div>		
             </div>
             
             <div class="side_75_per">
             	<div class="field_row verticle_field_row">
                	<div class="field_24">&nbsp;</div>
                    <div class="field_24"><strong>Power [W]</strong></div>
                    <div class="field_24"><strong>Heart rate [BPM]</strong></div>
                    <div class="field_24"><strong>RPE</strong></div>
                </div>
                <!-- Stage 1 Values -->  
                <div class="field_row verticle_field_row">
                	<div class="field_24">Stage 1</div>
                    <div class="field_24"><input class="cus-input" type="text" name="workload1" id="workload1" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->workload1)?$fieldData[0]->workload1:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="heart_rate1" id="heart_rate1" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->heart_rate1)?$fieldData[0]->heart_rate1:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="rpe1" id="rpe1" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->rpe1)?$fieldData[0]->rpe1:""; ?>"></div>
                </div>
                <!-- Stage 2 Values --> 
                <div class="field_row verticle_field_row">
                	<div class="field_24">Stage 2</div>
                    <div class="field_24"><input class="cus-input" type="text" name="workload2" id="workload2" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->workload2)?$fieldData[0]->workload2:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="heart_rate2" id="heart_rate2" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->heart_rate2)?$fieldData[0]->heart_rate2:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="rpe2" id="rpe2" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->rpe2)?$fieldData[0]->rpe2:""; ?>"></div>
                </div>	  
                
                <!-- Stage 3 Values --> 
                <div class="field_row verticle_field_row">                    
                    <div class="field_24">Stage 3</div>
                    <div class="field_24"><input class="cus-input" type="text" name="workload3" id="workload3" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->workload3)?$fieldData[0]->workload3:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="heart_rate3" id="heart_rate3" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->heart_rate3)?$fieldData[0]->heart_rate3:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="rpe3" id="rpe3" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->rpe3)?$fieldData[0]->rpe3:""; ?>"></div>
                </div>	 
                
                <!-- Stage 4 Values --> 
                <div class="field_row verticle_field_row" id="stage_4" style="display: none;">                    
                    <div class="field_24">Stage 4</div>
                    <div class="field_24"><input class="cus-input" type="text" name="workload4" id="workload4" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->workload4)?$fieldData[0]->workload4:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="heart_rate4" id="heart_rate4" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->heart_rate4)?$fieldData[0]->heart_rate4:""; ?>"></div>
                    <div class="field_24"><input class="cus-input" type="text" name="rpe4" id="rpe4" onkeypress="return event.charCode >= 0 && event.charCode < 58;" pattern="[0-9]*" value="<?php echo isset($fieldData[0]->rpe4)?$fieldData[0]->rpe4:""; ?>"></div>
                </div>	
                
                <div class="field_row verticle_field_row" style="border:0; padding-bottom:10px;">      
                    <button id="plot" name="plot" onclick="show()" class="lite_btn f_right btn_green">Plot</button>
                    <button id="add_stage" onclick="changeStage()" class="lite_btn btn_virtual">Add Stage</button>                    
                    <input type="hidden" name="stageFlag" id="stageFlag">
                </div>
                
                <div class="graph_submaximal">                           
                    <span id="grf_label" style="display: none;"></span>
                    <div id="chartdiv" style="width: 450px; height: 340px; margin-left: 48px; margin-bottom:20px;"></div>
                </div> 
                
                <button id="y_hr" onclick="regHeartRate()" class="lite_btn f_right btn_green" style="margin-bottom: 20px; margin-top: 280px;">Y<sub>HR</sub></button>
                <button id="y_rpe" onclick="regRPE()" class="lite_btn f_right btn_green">Y<sub>RPE</sub></button>             	
             </div>
        </div>
    </div>
    
</div>

<?php echo form_close(); ?>
<!-- Form ends -->	
	
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>	

<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
	
   $(document).on('click','#plot', function(event){
	event.preventDefault();
   });
   
   $(document).on('click','#add_stage', function(event){
	event.preventDefault();
   });

   $(document).on('click','#y_hr', function(event){
	event.preventDefault();
   });

   $(document).on('click','#y_rpe', function(event){
	event.preventDefault();
   });   

   $(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
   });

        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script>

<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.trendline.min.js"></script>
<link rel="stylesheet" type="text/css" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />

<script>  
   
   function changeStage()
   {  
       if(document.getElementById("stageFlag").value == "" ){
          
         document.getElementById("add_stage").innerHTML = "Remove Stage";
         document.getElementById("stage_4").setAttribute('style', ""); 
         document.getElementById("stageFlag").value = "1" ;
       }
       else{            
            document.getElementById("workload4").value = "" ;  
            document.getElementById("heart_rate4").value = "" ;
            document.getElementById("rpe4").value = "" ;       
            document.getElementById("stage_4").setAttribute('style', "display:none");     
            document.getElementById("add_stage").innerHTML = "Add Stage";
            document.getElementById("stageFlag").value = "" ;
         }
   }
     
   function regHeartRate() 
    {
        var heartRateDiv =  document.getElementById("heatRateVal") ;
        var rpeDiv =  document.getElementById("rpeVal") ;
        
        rpeDiv.style.display = "none" ; 
        heartRateDiv.style.display = "block" ;
        
        document.getElementById("grf_label").style.display = "block" ;
        document.getElementById("grf_label").setAttribute('class', "");
        document.getElementById("grf_label").setAttribute('class', "grf_label2");            
        document.getElementById("grf_label").innerHTML = "Heart rate [BPM]";
        
        
        document.getElementById("known_max_rpe").style.display = "none" ;    
        document.getElementById("known_max_hr").style.display = "block" ;                  
        document.getElementById("maxVal").innerHTML = "Known HRmax";
        document.getElementById("known_max_hr_val").style.display = "inline-block" ; 
        
        
        
        var chartDiv=document.getElementById('chartdiv');
        document.getElementById("max_wl").value = "" ;
        document.getElementById("vo_max").value = "" ;
		
		document.getElementById("max_wl_hr").value = "";
		 document.getElementById("vo_max_hr").value = "" ;
        
        if(chartDiv.innerHTML !== "")
        {
             while (chartDiv.hasChildNodes()) {
            chartDiv.removeChild(chartDiv.firstChild);
            }         
        }
        
        var ageVal = document.getElementById("age").value ;
		console.log("ageVal"+ageVal);
		
		
        var weightVal = document.getElementById("weight").value ;
        console.log("weightVal"+weightVal);
        var x1 = document.getElementById("workload1").value ;console.log("x1"+x1);
        var x2 = document.getElementById("workload2").value ;console.log("x2"+x2);
        var x3 = document.getElementById("workload3").value ;console.log("x3"+x3);
        var x4 = document.getElementById("workload4").value ;console.log("x4"+x4);
              
        var y1 = document.getElementById("heart_rate1").value ;console.log("y1"+y1);
        var y2 = document.getElementById("heart_rate2").value ;console.log("y2"+y2);
        var y3 = document.getElementById("heart_rate3").value ;    console.log("y3"+y3);    
        var y4 = document.getElementById("heart_rate4").value ;    console.log("y4"+y4);    
               
        document.getElementById("max_hr").value = "" ;
        
        if(ageVal !== "")
        {
           var hrMax = (208 - (0.7 * ageVal)) ;           
           document.getElementById("max_hr").value = parseFloat(hrMax) ;
        }
        
        
    if(document.getElementById("workload4").value == "" && document.getElementById("heart_rate4").value == ""){  
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; console.log("x1y1"+x1y1);     //product  console.log("y4"+y4);    
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; console.log("x2y2"+x2y2);    //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; console.log("x3y3"+x3y3);    //product
        
        var sumxy = x1y1 + x2y2 + x3y3 ; console.log("sumxy"+sumxy);// sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ; console.log("sumx"+sumx);// sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) ; console.log("sumy"+sumy);// sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;  console.log("xsquared"+xsquared);
        var sumxsquared = (sumx * sumx)  ;  console.log("sumxsquared"+sumxsquared);
      
        var slope = (3 * sumxy - (sumx * sumy)) / (3 * xsquared - sumxsquared) ;  console.log("slope"+slope);
        var intercept = (sumy - (slope * sumx)) / 3  ;   console.log("intercept"+intercept);
       
        if(document.getElementById("known_max_hr").value == "")
        {
          var y = document.getElementById("max_hr").value ;  
        }
        else
        {
          var y = document.getElementById("known_max_hr").value ;    
        }
        console.log("y"+y);
        var x = (parseFloat(y) - intercept) / slope ;
        console.log("x"+x);
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
		   
		   
		    document.getElementById("max_wl_hr").value = "";
           document.getElementById("vo_max_hr").value = "" ;
            
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
			
			document.getElementById("max_wl_hr").value = Math.floor(parseFloat(x) * 10) / 10 ; 
			
			console.log(document.getElementById("max_wl").value);
			
			
			var code=document.getElementById("max_wl").value;
            fcookie='max_wl';
			document.cookie = fcookie+"=" + code ;  
			
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			document.getElementById("vo_max_hr").value = Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			
        }
      
        var max_x = Math.floor(x);  
        var reg_x = x3 ;
        var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)]] ;
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(0)], [parseFloat(max_x),parseFloat(y)]] ;
        
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {         
          label: 'Power [W]',
          ticks : ['0','50', '100', '150', '200', '250', '300', '350', '400'],
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
        yaxis: {              
                ticks:['60', '80', '100', '120', '140', '160', '180', '200'], 
				//ticks:['0', '20', '40', '60', '80', '100', '120', '140', '160', '180', '200'],               
              // label: 'Heart Rate',
              // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
              // tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                   }               
              }
         },
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }      
        
     
      }); 
    }
   
     else{  
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        var x4y4 = parseFloat(x4) * parseFloat(y4) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 + x4y4 ; // sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ; // sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) + parseFloat(y4) ; // sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) + (parseFloat(x3) * parseFloat(x3)) + (parseFloat(x4) * parseFloat(x4)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (4 * sumxy - (sumx * sumy)) / (4 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 4  ; 
             
        if(document.getElementById("known_max_hr").value == "")
        {
          var y = document.getElementById("max_hr").value ;  
        }
        else
        {
          var y = document.getElementById("known_max_hr").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
       
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
		   
		   document.getElementById("vo_max_hr").value="" ;
		   
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
			
			document.getElementById("max_wl_hr").value = Math.floor(parseFloat(x) * 10) / 10 ; 
			
			var code=document.getElementById("max_wl").value;
            fcookie='max_wl';
			document.cookie = fcookie+"=" + code ;
			
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			
			document.getElementById("vo_max_hr").value= Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			
        }
      
      var max_x = Math.floor(x);      
      var reg_x = x4 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)], [parseFloat(x4), parseFloat(y4)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;     
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(0)], [parseFloat(max_x),parseFloat(y)]] ;
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {         
          label: 'Power [W]',
          ticks : ['0','50', '100', '150', '200', '250', '300', '350', '400'],
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
        yaxis: {              
               ticks:['60', '80', '100', '120', '140', '160', '180', '200'],               
			   // ticks:['0', '20', '40', '60', '80', '100', '120', '140', '160', '180', '200'],               
              // label: 'Heart Rate',
              // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
              // tickRenderer: $.jqplot.CanvasAxisTickRenderer,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                   }               
              }
         }, 
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }           
        
     
      }); 
    }
   
  } 
    
    function regRPE() 
    {
        var heartRateDiv =  document.getElementById("heatRateVal") ;
        var rpeDiv =  document.getElementById("rpeVal") ;
        
        heartRateDiv.style.display = "none" ; 
        rpeDiv.style.display = "block" ;
        
        document.getElementById("grf_label").style.display = "block" ;
        document.getElementById("grf_label").setAttribute('class', "");
        document.getElementById("grf_label").setAttribute('class', "grf_label1");
        document.getElementById("grf_label").innerHTML = "RPE" ;
        
        
        document.getElementById("known_max_hr").style.display = "none" ;    
        document.getElementById("known_max_rpe").style.display = "block" ;                  
        document.getElementById("maxVal").innerHTML = "Known Max RPE";
        document.getElementById("known_max_hr_val").style.display = "none" ; 
        
        
       var chartDiv=document.getElementById('chartdiv');
        document.getElementById("max_wl").value = "" ;
        document.getElementById("vo_max").value = "" ;
		
		
		document.getElementById("max_wl_rpe").value = "" ;
        document.getElementById("vo_max_rpe").value = "" ;
		
        
        if(chartDiv.innerHTML !== "")
        {         
          while (chartDiv.hasChildNodes()) {
            chartDiv.removeChild(chartDiv.firstChild);
            }
            //chartDiv.innerHTML = "" ;
        }
        
        var ageVal = document.getElementById("age").value ;
        var weightVal = document.getElementById("weight").value ;
        
        var x1 = document.getElementById("workload1").value ;
        var x2 = document.getElementById("workload2").value ;
        var x3 = document.getElementById("workload3").value ;
        var x4 = document.getElementById("workload4").value ;
              
        var y1 = document.getElementById("rpe1").value ;
        var y2 = document.getElementById("rpe2").value ;
        var y3 = document.getElementById("rpe3").value ;
        var y4 = document.getElementById("rpe4").value ;
     
        if(document.getElementById("max_rpe").value == "")
        {
            document.getElementById("max_rpe").value = "20" ;
        } 
              
     if(document.getElementById("workload4").value == "" && document.getElementById("rpe4").value == "")
     {    
        var x1y1 = parseFloat(x1) * parseFloat(y1) ;console.log(x1y1); //product  
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; console.log(x2y2);//product 
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; console.log(x3y3);//product
        
        var sumxy = x1y1 + x2y2 + x3y3 ; console.log("sumxy"+sumxy);//sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) ;   console.log("sumx"+sumx);      //sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) ;  console.log("sumy"+sumy);       //sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) +(parseFloat(x3) * parseFloat(x3)) ;   console.log("sumxsquared"+sumxsquared);   
        var sumxsquared = (sumx * sumx)  ;   console.log("intercept"+intercept);   
      
        var slope = (3 * sumxy - (sumx * sumy)) / (3 * xsquared - sumxsquared) ;   console.log("slope"+slope);   
        var intercept = (sumy - (slope * sumx)) / 3  ;      
     
       console.log("intercept"+intercept);   
        
        if(document.getElementById("known_max_rpe").value == "")
        {
          var y = document.getElementById("max_rpe").value ;
        }
        else
        {
          var y = document.getElementById("known_max_rpe").value ;    
        }
        console.log(y);
        var x = (parseFloat(y) - intercept) / slope ;
         console.log("x"+x);
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;       

           document.getElementById("max_wl_rpe").value = "" ;
           document.getElementById("vo_max_rpe").value = "" ;

		   
        }
        else
        {
           document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
		   
		   document.getElementById("max_wl_rpe").value = Math.floor(parseFloat(x) * 10) / 10 ; 
		   
		   console.log(document.getElementById("max_wl").value);
		   
		    /* var code=document.getElementById("max_wl").value;
            fcookie='max_wl';
			document.cookie = fcookie+"=" + code ; */
		   
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			
			 document.getElementById("vo_max_rpe").value = Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			
        }
     
      var max_x = Math.floor(x); 
      var reg_x = x3 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(6)], [parseFloat(max_x),parseFloat(y)]] ;
    
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {            
          ticks : ['0','50', '100', '150', '200', '250', '300', '350', '400'],
          label: 'Power [W]',          
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],               
               // label: 'RPE',               
               // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
                //tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }                  
     
      }); 
     }
  
     else
     {    
        var x1y1 = parseFloat(x1) * parseFloat(y1) ; //product
        var x2y2 = parseFloat(x2) * parseFloat(y2) ; //product
        var x3y3 = parseFloat(x3) * parseFloat(y3) ; //product
        var x4y4 = parseFloat(x4) * parseFloat(y4) ; //product
        
        var sumxy = x1y1 + x2y2 + x3y3 + x4y4 ; //sum of products
        var sumx = parseFloat(x1) + parseFloat(x2) + parseFloat(x3) + parseFloat(x4) ;     //sum of x
        var sumy = parseFloat(y1) + parseFloat(y2) + parseFloat(y3) + parseFloat(y4) ;     //sum of y    
        
        var xsquared = (parseFloat(x1) * parseFloat(x1)) + (parseFloat(x2) * parseFloat(x2)) + (parseFloat(x3) * parseFloat(x3)) + (parseFloat(x4) * parseFloat(x4)) ;
        var sumxsquared = (sumx * sumx)  ;
      
        var slope = (4 * sumxy - (sumx * sumy)) / (4 * xsquared - sumxsquared) ;
        var intercept = (sumy - (slope * sumx)) / 4  ; 
        
       if(document.getElementById("known_max_rpe").value == "")
        {
          var y = document.getElementById("max_rpe").value ;
        }
        else
        {
          var y = document.getElementById("known_max_rpe").value ;    
        }
        
        var x = (parseFloat(y) - intercept) / slope ;
        
        if(isNaN(parseFloat(x)))
        {          
           document.getElementById("max_wl").value = "" ;
           document.getElementById("vo_max").value = "" ;
		   
		    document.getElementById("max_wl_rpe").value = "" ;
        document.getElementById("vo_max_rpe").value = "" ;
		   
            
        }
        else
        {
            document.getElementById("max_wl").value = Math.floor(parseFloat(x) * 10) / 10 ; 
			document.getElementById("max_wl_rpe").value =Math.floor(parseFloat(x) * 10) / 10 ; 
			
			var code=document.getElementById("max_wl").value;
            fcookie='max_wl';
			document.cookie = fcookie+"=" + code ;
            
            
            var vo1  =  ((x / 9.81) * 60) * 2 + (3.5 * weightVal) ;
            var predicted_VO  =  vo1 / weightVal ;
            
            document.getElementById("vo_max").value =  Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
			document.getElementById("vo_max_rpe").value =Math.floor(parseFloat(predicted_VO) * 10) / 10 ;
        }
     
      var max_x = Math.floor(x);       
      var reg_x = x4 ;
      var reg_y = intercept + (slope * parseFloat(reg_x)) ;   
      
    $.jqplot.config.enablePlugins = true;   
    var chartData1 = [[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)], [parseFloat(x4), parseFloat(y4)]]; 
    var chartData2 = [[0,parseFloat(y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData3 = [[parseFloat(reg_x),parseFloat(reg_y)], [parseFloat(max_x),parseFloat(y)]] ;  
    var chartData4 = [[parseFloat(max_x),parseFloat(6)], [parseFloat(max_x),parseFloat(y)]] ;
    
    var plot1 = $.jqplot('chartdiv', [chartData1, chartData2, chartData3, chartData4], {
          
        axes: {
        xaxis: {            
          ticks : ['0','50', '100', '150', '200', '250', '300', '350', '400'],
          label: 'Power [W]',          
          labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
         // tickRenderer: $.jqplot.CanvasAxisTickRenderer,            
          tickOptions: {
             formatString: '%d'
          }           
        },        
       yaxis: {              
               ticks:['6', '7', '8', '9', '10', '11', '12', '13', '14', '15', '16', '17', '18', '19', '20'],               
               // label: 'RPE',               
               // labelRenderer: $.jqplot.CanvasAxisLabelRenderer ,
                //tickRenderer: $.jqplot.CanvasAxisTickRenderer ,
                tickOptions: {
                    formatString: '%.2f' ,
                    angle: 30
                    }                      
            }
         },         
        grid:{
            borderColor:'transparent',
            shadow:false,
            drawBorder:false,
            shadowColor:'transparent'
        },
        seriesDefaults: {   
                     showLine:false,                     
                     shadow: false                    
                   },
        seriesColors: ["#4bb2c5", "#ffc61e", "#A8A8A8", "#A8A8A8"],
        series: [
            {
             trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 2,
                shadow: false
            }            
            },
           {
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'solid',
                lineWidth: 3,
                shadow: false
            },            
            showMarker: false
            },       
           { 
            trendline: {
                renderer: $.jqplot.LineRenderer(),
                linePattern: 'dashed',
                lineWidth: 2,
                shadow: false
            },   
            showMarker: false            
            },
           {
            showLine: true,
            linePattern: 'dashed',
            lineWidth: 2,
            showMarker: false,
            shadow: false
            }            
        ],
        highlighter: {
            sizeAdjust: 3,
            tooltipLocation: 'n',
            tooltipAxes: 'y',
            tooltipFormatString: '%.2f',
            useAxesFormatters: false
        }                  
     
      }); 
     }      
    }
    
    function show()
    {         
       // Heart Values  
        if(document.getElementById("heart_rate1").value !== "" && document.getElementById("heart_rate2").value !== "" && document.getElementById("heart_rate3").value !== "")
        {
          regHeartRate() ;
        }
        
        
       // RPE Values  
        else if(document.getElementById("rpe1").value !== "" && document.getElementById("rpe2").value !== "" && document.getElementById("rpe3").value !== "")
        {
          regRPE() ;
        }
        
        else
        {
            alert ("No values have been entered");
            return false;
        }
        
    }
    
</script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script>
</body>
</html>
