<?php

/**
 * provides a very simple black header at the top of the page
 *
 * @author Matt Palmer
 *
 * @param	$filename	string			Contains url of VP drop down face image
 */

?>

<div class="header_tiny">
	<div class="header header_tiny">
		<div class="wrapper"> 
			<div class="head_left">
				<a href="https://www.unisa.edu.au/" target="_blank">
					<img id="unisa_logo_id" src="<?php echo base_url('/assets/images/') ."/unisa-logo.png"; ?>">
				</a>
			</div>
    		<div class="head_left header_heading"><a href="<?php echo site_url('/welcome/dashboard'); ?>">Exercise Science Toolkit</a></div>
		
		   <!-- <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>-->
		   <div class="head_right vp_dropdown">
				<span class="vpdd_name">
					<?php echo $_SESSION['user_first_name'].' ' ;?>
					<?php echo $_SESSION['user_last_name'] ;?>
				</span>
				<input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" />
				<div class="vp_dropdown_content">
					<?php $this->view('modals/v_person.php'); ?>
				</div>
			</div>

			<script>
				// show VP when clicking on name/icon
			
				$(".vp_dropdown").hover( function() {
					$(".vp_dropdown_content").fadeIn(300);
				});

				// hide when click on name/icon or outside of VP box
				// (note, the below method had to be employed due to some very inconsisent results from jQuery)
			
				var isNotHovered1 = false;
				var isNotHovered2 = false;
				var isNotHovered3 = false;
				$(".vp_dropdown_content").hover(function() {
					isNotHovered1 = false;
					console.log(".vp_dropdown_content hover");
				});
				$(".vp_dropdown").hover(function() {
					isNotHovered2 = false;
					console.log(".vp_dropdown hover");
				});
				$(".vp_gen_wrapper").hover(function() {
					isNotHovered3 = false;
					console.log(".vp_gen_wrapper hover");
				});
				$(".vp_dropdown_content").mouseleave(function() {
					isNotHovered1 = true;
					console.log(".vp_dropdown_content");
				});
				$(".vp_dropdown").mouseleave(function() {
					isNotHovered2 = true;
					console.log(".vp_dropdown");
				});
				$(".vp_gen_wrapper").mouseleave(function() {
					isNotHovered3 = true; 
					console.log(".vp_gen_wrapper");
				});

				$(document).mousemove(function(e) {
				
					if(isNotHovered1 && isNotHovered2 && isNotHovered3) {
						$(".vp_dropdown_content").fadeOut(300);
						console.log("is not over");
					}
				});

			</script>
		</div>
	</div>
</div>