<?php

/**
 * header for body composition
 *
 * @author Matt Palmer
 *
 * @param	string		$title			Title of the page currently used
 * @param	string		$breadcrumbs	Contains string with breadcrumbs
 * @param   boolean		$show_video		Whether to show video icon or now (later remove and replace with vid url - if empty don't display icon)
 * @param	string		$menu_colour	Set a background colour for menu items
 * @param	string		$submenu_color	Set a background colour for the submenu items
 * @param	string		$back_url		Link for the back '<' button to go back to the appropriate view
 * @param	$filename	string			Contains url of VP drop down face image
 *
 * @param	array		$menu_array		Contains a 3 dimensional array containing title and url of menu and submenu items - 
 *										1st dimension - each menu
 *										2nd dimension - [0] is menu item and [1+] is submenu item
 *										3rd dimension - 'title' is title, 'url' is view url
 *
 */

?>
<div class="header header_full">
	<div class="wrapper">
		<div class="head_left">
			<a href="https://www.unisa.edu.au/" target="_blank">
				<img id="unisa_logo_id" src="<?php echo base_url('/assets/images/') ."/unisa-logo.png"; ?>">
			</a>
		</div>

		<div class="head_left header_heading">
			<a href="<?php echo site_url('/welcome/dashboard'); ?>">Exercise Science Toolkit</a>
		</div>

		<div class="head_left headandsubhead">
			<p class="header_mediumtext">
				<?=$title?>
			</p>
			<p class="header_smalltext">
				<?=$breadcrumbs?>
			</p>
		</div>

		<div class="head_right vp_dropdown">
			<span class="vpdd_name">
				<?php echo $_SESSION['user_first_name'].' ' ;?>
				<?php echo $_SESSION['user_last_name'] ;?>
			</span>
			<input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" />
			<div class="vp_dropdown_content">				
				<?php $this->view('modals/v_person.php'); ?>
			</div>
		</div>

		<script>
			// show VP when clicking on name/icon 
			
			$(".vp_dropdown").hover( function() {
				$(".vp_dropdown_content").fadeIn(300);
			});

			// hide when mouse leaves name/icon or outside of VP box
			 
			$(document).mousemove(function (e) { 
				if (!$(".vp_dropdown_content").is(':hover') && 
						!$(".vp_dropdown").is(':hover') && 
						!$(".vp_gen_wrapper").is(':hover') &&
						$(".vp_dropdown_content").is(':visible'))
						{
					$(".vp_dropdown_content").fadeOut(300);
				}
			});

		</script> 		
	</div>
</div>

</div class="subheader">	
	<div class="orng_container" style="background-color: <?=$menu_colour ?>;">
		<div class="wrapper">
			<div class="orng_box_btn f_left">
				<!-- <a href="#" id="exit" onclick="window.location.href = '<?=$back_url ?>';" title="Back"><img src="<?=$back_image_url ?>"></a> -->
				<a href="<?=$back_url ?>" id="exit" title="Back"><img src="<?=$back_image_url ?>"></a>
			</div>

			<div class="subheader_menu f_left">
				<?php
					// create menu structure
					for($outer = 0; $outer < count($menu_array); $outer++) {
						?>
							<div class="dropdown" style="background-color: <?php echo $menu_colour ?>">
							<?php
								// get first menu items
								$title = $menu_array[$outer][0]["title"];
								$url = $menu_array[$outer][0]["url"];

								// place menu items
							?>
							<a class="dropbtn" href="<?php echo($url); ?>">
								<?php echo($title);?>
							</a>

							<div class="dropdown-content" style="background-color: <?php echo $submenu_colour ?>">
								<div class="dropdown-menu-separator" style="background-color: <?php echo $menu_colour ?> ;"></div>
								<?php
									if (count($menu_array[$outer]) > 1) {
										for($inner = 1; $inner < count($menu_array[$outer]); $inner++) {
											// get submenu items
											$title = $menu_array[$outer][$inner]["title"];
											$url = $menu_array[$outer][$inner]["url"];

											// place submenu items
											?>
												<a class="dropbtn topborder" href="<?php echo($url); ?>">
												<?php echo($title);?>
												</a>
											<?php
										}
									}
								?>
							</div>
						</div>
					<?php
					}
				?>
			</div>
			<div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;">
				<svg class="printer_svg" version="1.1" id="Layer_1"
					xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
					<g>
						<g>
							<rect x="166.4" y="435.2" width="179.2" height="25.6"/>
						</g>
					</g>
					<g>
						<g>
							<rect x="166.4" y="384" width="179.2" height="25.6"/>
						</g>
					</g>
					<g>
						<g>
							<rect x="166.4" y="332.8" width="179.2" height="25.6"/>
						</g>
					</g>
					<g>
						<g>
							<path d="M460.8,128h-64V0H115.2v128h-64C22.972,128,0,150.972,0,179.2v179.2c0,28.228,22.972,51.2,51.2,51.2h64V512h281.6V409.6
			h64c28.237,0,51.2-22.972,51.2-51.2V179.2C512,150.972,489.037,128,460.8,128z M140.8,25.6h230.4V128H140.8V25.6z M371.2,486.4
			H140.8V281.6h230.4V486.4z M486.4,358.4c0,14.14-11.46,25.6-25.6,25.6h-64V281.6h25.6c7.074,0,12.8-5.726,12.8-12.8
			c0-7.074-5.726-12.8-12.8-12.8H89.6c-7.074,0-12.8,5.726-12.8,12.8c0,7.074,5.726,12.8,12.8,12.8h25.6V384h-64
			c-14.14,0-25.6-11.46-25.6-25.6V179.2c0-14.14,11.46-25.6,25.6-25.6h409.6c14.14,0,25.6,11.46,25.6,25.6V358.4z"/>
						</g>
					</g>
					<g>
						<g>
							<path d="M422.4,179.2H320c-7.074,0-12.8,5.726-12.8,12.8s5.726,12.8,12.8,12.8h102.4c7.074,0,12.8-5.726,12.8-12.8
			S429.474,179.2,422.4,179.2z"/>
						</g>
					</g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
				</svg>
			</a>
        </div>
			<div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn">
				<svg class="question_svg" version="1.1"
					xmlns="http://www.w3.org/2000/svg"
					xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	  viewBox="0 0 496.304 496.303" style="enable-background:new 0 0 496.304 496.303;"
	 xml:space="preserve">
					<g>
						<path d="M248.152,0C111.32,0,0,111.321,0,248.152c0,136.829,111.32,248.151,248.152,248.151
		c136.829,0,248.152-111.322,248.152-248.151C496.304,111.321,384.992,0,248.152,0z M248.152,472.093
		c-123.479,0-223.941-100.459-223.941-223.941c0-123.479,100.462-223.941,223.941-223.941
		c123.482,0,223.941,100.462,223.941,223.941C472.093,371.634,371.634,472.093,248.152,472.093z M270.985,379.519v37.237
		c0,5.013-4.055,9.079-9.079,9.079h-32.161c-5.018,0-9.079-4.066-9.079-9.079v-37.237c0-5.013,4.061-9.079,9.079-9.079h32.161
		C266.931,370.44,270.985,374.506,270.985,379.519z M310.067,85.671c6.229,6.135,9.386,14.002,9.386,23.403v74.49
		c0,10.229-2.713,19.532-8.056,27.636l-41.678,63.532v67.536c0,5.013-4.055,9.079-9.078,9.079h-29.625
		c-5.019,0-9.079-4.066-9.079-9.079v-63.257c0-11.112,2.586-20.575,7.693-28.146l42.253-63.071v-67.099h-49.559v65.412
		c0,5.012-4.061,9.079-9.079,9.079h-29.415c-5.019,0-9.079-4.067-9.079-9.079v-77.032c0-9.437,3.207-17.321,9.534-23.448
		c6.256-6.052,14.145-9.12,23.454-9.12h78.72C295.94,76.512,303.879,79.592,310.067,85.671z"/>
					</g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
					<g></g>
				</svg>
			</a>
        </div>
		
		<?php
			if ($show_video == true) { ?>
		<div class="head_right video_icon_container">
			<div class="orng_box_btn f_right">
				<svg version="1.1" class="video_icon" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
					 viewBox="0 0 489.8 489.8" style="enable-background:new 0 0 489.8 489.8;" xml:space="preserve">
					<g>
						<g>
							<g>
								<path d="M244.9,0C109.8,0,0,109.8,0,244.9s109.9,244.9,244.9,244.9c135.1,0,244.9-109.9,244.9-244.9C489.8,109.8,380,0,244.9,0z
									 M244.9,455.5c-116.1,0-210.6-94.5-210.6-210.6S128.8,34.3,244.9,34.3s210.6,94.5,210.6,210.6S361,455.5,244.9,455.5z"/>
								<path d="M375.9,230.5L200.1,117.7c-5.3-3.4-12-3.6-17.5-0.6s-8.9,8.8-8.9,15v225.5c0,6.3,3.4,12,8.9,15c2.6,1.4,5.4,2.1,8.2,2.1
									c3.2,0,6.4-0.9,9.3-2.7l175.8-112.7c4.9-3.2,7.9-8.6,7.9-14.4C383.7,239.1,380.8,233.6,375.9,230.5z M207.9,326.3V163.5
									l126.9,81.4L207.9,326.3z"/>
							</g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
						<g>
						</g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
					<g>
					</g>
				</svg>
			</div>
		</div>
		<?php
			}
		?>



        <div class="overlay">&nbsp;</div>
        <div class="info_block">
        	<div class="info_block_head">Generate Virtual Person</div>
            <p>The Virtual Person Generator is a standard Toolkit. In which user can genarated the Virtual person profile with the given number of count with its selected test , User can also select gender and sub-population for generating VP.</p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div> 

		</div>
	</div>
</div>

<script>
	// change background cover of submenu section on hover
	$(".dropdown").hover(function() {
		//$(this).css("background-color", "<?php echo $submenu_colour ?>");
		$(".dropdown-content").css("background-color", "<?php echo $submenu_colour ?>");
	});

</script>


<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  /*$( function() {
    $( ".vp_v_detail" ).draggable();
  } );*/
</script> 