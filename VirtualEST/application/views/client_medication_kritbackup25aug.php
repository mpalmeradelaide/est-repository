<?php 


// kritika Code start
$i=0;
$checkedval="";
$sitearr_values = array();
foreach($MBJdata as $key=>$val)
{
	if($key == "mbjsite")
    {
        foreach($val as $key)
        {
            $sitearr_values[$i]=$key;
            $i++;
        }
    }	
	
	if($key == "MBJoption")
	{
		$checkedval=$MBJdata["MBJoption"];
	}
	
}

//print_r($sitearr_values);

//echo $checkedval;
// kritika Code End







if(count($medical_history)>0)
{
   $gender=$_SESSION['gender'];
    $medical_conditions=array();
    $medication=array();
    $HEART=($medical_history[0]->option_1 === 'Y')?"Y":"N";

       //For Heart Condition
        if($HEART=='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $heartcondition=rand($min,$max);  
            if($heartcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($heartcondition >= 8)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $heartmeds=rand($min,$max);  
        if($heartmeds <= 8)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($heartmeds <= 4)
        {
         $medication[]='cardiovascular';  
        }
        if($heartmeds >= 6)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($heartmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='musculo-skeletal';   
            break;
         case 5:
        $medication[]='psychiatric';   
            break;
         case 6:
        $medication[]='renal medication';   
            break; 
        case 7:
        $medication[]='respiratory';   
            break;
         case 8:
        $medication[]='other';   
            break;
        }
          
        }
        $CHESTPAIN =($medical_history[0]->option_2 === 'Y')?"Y":"N";

       //For CHESTPAIN Condition
        if($CHESTPAIN =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $chestcondition=rand($min,$max);  
            if($chestcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        else if($chestcondition >= 9)
        {
         $medical_conditions[]='cerebrovascular';   
        }
        else 
        {
        $medical_conditions[]='other';
        }
        $min=1;
        $max=10;
        $chestmeds=rand($min,$max);  
        if($chestmeds <= 9)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($chestmeds <= 6)
        {
         $medication[]='cardiovascular';  
        }
        if($chestmeds >= 3)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($chestmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='other';   
            break;
        
        }
          
        }
        $FAINT  =($medical_history[0]->option_3 === 'Y')?"Y":"N";

       //For FAINT Condition
        if($FAINT =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $faintcondition=rand($min,$max);  
            if($faintcondition <= 6)
        {
         $medical_conditions[]='cardiovascular';   
        }
        switch($faintcondition)
        {
        case 7:  
        $medical_conditions[]='thyroid disease'; 
        break;    
        case 8:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 9:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 10:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        }
        $min_other=1;
        $max_other=10;
        $othercondition=rand($min_other,$max_other);  
        if($othercondition>=7)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $faintmeds=rand($min,$max);  
        if($faintmeds <= 3)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($faintmeds <= 5)
        {
         $medication[]='cardiovascular';  
        }
        if($faintmeds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($faintmeds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
       $ASTHMA  =($medical_history[0]->option_4 === 'Y')?"Y":"N";

       //For ASTHMA Condition
        if($ASTHMA =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $asthmacondition=rand($min,$max);  
            if($asthmacondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($asthmacondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='cerebrovascular';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($asthmacondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $asthmameds=rand($min,$max);  
        if($asthmameds >= 2)
        {
         $medication[]='respiratory';  
        }
        if($asthmameds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($asthmameds >= 8)
        {
         $medication[]='LIPID-lowering medication';  
        }
        switch($asthmameds)
        {
        case 1:
        $medication[]='diabetic';   
            break;
         case 2:
        $medication[]='GLUCOSE-lowering medication';   
            break;
        case 3:
        $medication[]='metabolic';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
         case 7:
        $medication[]='other';   
            break;
        }
          
        }
        
        //For DIABETES Condition
         $DIABETES   =($medical_history[0]->option_5 === 'Y')?"Y":"N";
        if($DIABETES =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $diabetescondition=rand($min,$max);  
            if($diabetescondition <= 8)
        {
         $medical_conditions[]='respiratory disease';   
        }
        switch($diabetescondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='arthritis or osteoporosis';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($diabetescondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $diabetesmeds=rand($min,$max);  
        if($diabetesmeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($diabetesmeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($diabetesmeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($diabetesmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         case 6:
        if($gender=='F')
        {
         $medication[]='hormone-replacement therapies';   
        }
         break;
        
        }
        if($diabetesmeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        }
       
         //For BONE Condition
         $BONE =($medical_history[0]->option_6 === 'Y')?"Y":"N";
        if($BONE =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $bonecondition=rand($min,$max);  
            if($bonecondition <= 8)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($bonecondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='diabetes';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($bonecondition >= 9)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $bonemeds=rand($min,$max);  
        if($bonemeds >= 3)
        {
         $medication[]='respiratory';  
        }
        if($bonemeds <= 5)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($bonemeds >= 2)
        {
         $medication[]='GLUCOSE-lowering medication';  
        }
        switch($bonemeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='renal medication';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($bonemeds >= 5 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($bonemeds >= 9) 
        {
         $medication[]='metabolic';     
        }
        
        }

        
       //For OTHERMED  Condition
         $OTHERMED =($medical_history[0]->option_7 === 'Y')?"Y":"N";
        if($OTHERMED =='Y')
        {
        /*Random No*/
        $min=1;
        $max=10;
        $othercondition=rand($min,$max);  
            if($othercondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';   
        }
        switch($othercondition)
        {
        case 3:  
        $medical_conditions[]='cardiovascular'; 
        break;    
        case 4:  
        $medical_conditions[]='cerebrovascular';  
        break;    
         case 5:  
        $medical_conditions[]='kidney disease'; 
        break;
        case 6:  
         $medical_conditions[]='liver or metabolic disorder';  
        break;
        case 7:  
         $medical_conditions[]='psychiatric illness';  
        break;
        case 8:  
         $medical_conditions[]='thyroid disease';  
        break;
        }
       
        if($othercondition >= 6)
        {
         $medical_conditions[]='other';   
        }
        $min=1;
        $max=10;
        $lastmeds=rand($min,$max);  
        if($lastmeds >= 8)
        {
         $medication[]='psychiatric illness';  
        }
        if($lastmeds <= 2)
        {
         $medication[]='BLOOD PRESSURE-lowering medication';  
        }
        if($lastmeds >= 6)
        {
         $medication[]='other';  
        }
        switch($lastmeds)
        {
        case 1:
        $medication[]='respiratory';   
            break;
         case 2:
        $medication[]='LIPID-lowering medication';   
            break;
        case 3:
        $medication[]='other';   
            break;
         case 4:
        $medication[]='metabolic';   
            break;
         case 5:
        $medication[]='musculo-skeletal';   
            break;
         }
        if($lastmeds >= 9 && $gender=='F') 
        {
         $medication[]='hormone-replacement therapies';     
        }
        if($lastmeds >= 7) 
        {
         $medication[]='psychiatric';     
        }
       }
         //##################IF ALL NO ANSWER SELECTED#######################################      
     if($HEART=='N' && $CHESTPAIN=='N' && $FAINT=='N' && $ASTHMA=='N' && $DIABETES=='N' && $BONE=='N' && $OTHERMED=='N')
     {
        
         $min=1;
        $max=100;
        $medcondition=rand($min,$max); 
        echo $medcondition;
        if($medcondition <= 2)
        {
         $medical_conditions[]='arthritis or osteoporosis';  
          $medication[]='other';
        }
        switch($medcondition)
        {
        case 3:
        $medical_conditions[]='cardiovascular';   
        $medication[]='LIPID-lowering medication';  
            break;
         case 4:
        $medical_conditions[]='cerebrovascular';
        $medication[]='other';       
            break;
        case 5:
            $medical_conditions[]='diabetes';
            $medication[]='diabetic';   
            break;
         case 6:
             $medical_conditions[]='kidney disease';
             $medication[]='BLOOD PRESSURE-lowering medication';   
            break;
         case 7:
         $medical_conditions[]='liver or metabolic disorder';
         $medication[]='metabolic';      
            break;
         case 8:
         $medical_conditions[]='psychiatric illness';
         $medication[]='psychiatric';      
            break;
        case 9:
         $medical_conditions[]='respiratory disease';
         $medication[]='respiratory';      
            break;
         case 10:
         $medical_conditions[]='thyroid disease';
         $medication[]='metabolic';      
            break;
         }
     if($medcondition >= 98)
     {
     $medical_conditions[]='other';
         $medication[]='other';         
     }
   }
                
        $medication_selected=implode(',',array_unique($medication));
        $medical_cond_selected=implode(',',array_unique($medical_conditions));
        
       }
$option_4=$fieldData[0]->option_4;
if($option_4=='Y'){
$status =1;}
else{
$status =0;
}




// Kritika 
$_SESSION['medication_arr'] = $medication_selected;
//print_r($_SESSION); die;

// kritika Code
                $med_values=explode(",",$medication_selected);
                //print_r($med_values);  die;
                $object1 = new PHPExcel_Calculation_Statistical();
                $gender=$_SESSION['gender'];  
                if(in_array("BLOOD PRESSURE-lowering medication",$med_values))
                {
                    /*Random Male SBP = NORM.INV(RAND(),124,7)
                    Random Male DBP = NORM.INV(RAND(),80,4)*/
                    // Male SBP Calculation

                    if($gender == "M")
                    {           
                        $SBPmale_BPprob = frand(0,1);
                        $SBPmale_BPprob = round($SBPmale_BPprob, 9);
                        $SBP_BPmale = $object1->NORMINV($SBPmale_BPprob,124,7);
                        $Male_BPSBP= $SBP_BPmale;

                        // Male DBP Calculation
                        $DBPmale_BPprob = frand(0,1);
                        $DBPmale_BPprob = round($DBPmale_BPprob, 9);
                        $DBP_BPmale = $object1->NORMINV($DBPmale_BPprob,80,4);
                        $Male_BPDBP= $DBP_BPmale;
                        
                        
                        $finalarr["SBP"]=$Male_BPSBP;
                        $finalarr["DBP"]=$Male_BPDBP;
                        
                    }

                    else
                    {           
                        /*Random Female SBP = NORM.INV(RAND(),122,7)
                        Random Female DBP = NORM.INV(RAND(),78,4)
                        */
                        // Female SBP
                        $SBPfemale_BPprob = frand(0,1);
                        $SBPfemlae_BPprob = round($SBPfemale_BPprob, 9);
                        $SBP_BPfemale = $object1->NORMINV($SBPfemale_BPprob,122,7);
                        $Female_BPSBP= $SBP_BPfemale;

                        //Female DBP Calculation
                        $DBPfemale_BPprob = frand(0,1);
                        $DBPfemale_BPprob = round($DBPfemale_BPprob, 9);
                        $DBP_BPfemale = $object1->NORMINV($DBPfemale_BPprob,78,4);
                        $Female_BPDBP= $DBP_BPfemale;
                        
                        $finalarr["SBP"]=$Female_BPSBP;
                        $finalarr["DBP"]=$Female_BPDBP;
                        
                        
                    }
                }


                if(in_array("LIPID-lowering medication",$med_values))
                {
                   /* Random Male CHOL = NORM.INV(RAND(),5.0,0.5)
                      Random Female CHOL = NORM.INV(RAND(),5.1,0.55)*/

                    if($gender == "M")
                    {
                        // MALE CHOL 
                        $Male_lipidprob = frand(0,1);
                        $Male_lipidprob = round($Male_lipidprob, 9);
                        $Male_lipid = $object1->NORMINV($Male_lipidprob,5.0,0.5);
                        $Male_lipidCHOL= $Male_lipid;
                        
                        $finalarr["chol"]=$Male_lipidCHOL;
                    }
                    else
                    {
                        //FEMALE CHOL
                        $Female_lipidprob = frand(0,1);
                        $Female_lipidprob = round($Female_lipidprob, 9);
                        $Female_lipid = $object1->NORMINV($Female_lipidprob,5.1,0.55);
                        $Female_lipidCHOL= $Female_lipid;
                        
                        $finalarr["chol"]=$Female_lipidCHOL;
                    }
                }  


                if(in_array("GLUCOSE-lowering medication",$med_values))
                {
                    /*If MEDICATION list includes ‘GLUCOSE-lowering medication’ then 
                    Random Male GLU = NORM.INV(RAND(),5.2,0.4)
                    Random Female GLU = NORM.INV(RAND(),5.1,0.5)*/

                    if($gender == "M")
                    {
                        $male_gluprob = frand(0,1);
                        $male_gluprob = round($male_gluprob, 9);
                        $male_glu = $object1->NORMINV($male_gluprob,5.2,0.4);
                        $Male_glu= $male_lipid;
                        
                        $finalarr["glucose"]=$Male_glu;
                    }
                    else{
                        $female_gluprob = frand(0,1);
                        $female_gluprob = round($female_gluprob, 9);
                        $female_glu = $object1->NORMINV($female_gluprob,5.1,0.5);
                        $Female_glu= $female_glu;
                        
                        $finalarr["glucose"]=$Female_glu;
                    }
                }
             //End Of Medications code
          

              //MBJ Code

              $MBJ_site=array();
              $MBJPain=rand(1,10);
 
              if($MBJPain <= 3){
                  $option_MBJ="Y"; }
              else{
                  $option_MBJ="N"; }
              
			  $finalarr["MBJoption"]=$option_MBJ;
			  
              if($option_MBJ == "Y")
              {
                  $MBJ_number=rand(1,5);
                  if($MBJ_number == 1)
                  {
                      for($i=0;$i<1;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 2)
                  {
                      for($i=0;$i<2;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 3)
                  {
                      for($i=0;$i<3;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  
                  elseif($MBJ_number == 4)
                  {
                      for($i=0;$i<4;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  elseif($MBJ_number == 5)
                  {
                      for($i=0;$i<5;$i++)
                      { $MBJ_site[$i] = rand(1,15); }
                  }
                  else
                  {
                      $MBJ_site[0] = "No value exists";
                  }
              }
              else
              {
                  $MBJ_site = "MBJ Site NO";
              }
              $finalarr["mbjsite"]=$MBJ_site;

              $data['MBJdata'] = $finalarr;  

              //print_r($data['MBJdata']); 
                $checkedval="";
                $i=0;
                $sitearr_values = array();
                foreach($data['MBJdata'] as $key=>$val)
                {               
                    if($key == "mbjsite")
                    {
                        foreach($val as $key)
                        {
                            $sitearr_values[$i]=$key;
                            $i++;
                        }
                    }	

                    if($key == "MBJoption")
                    {
                        $checkedval=$val;
                    }

                }

                //print_r($sitearr_values);
                //print_r($data['MBJdata']);


 function frand($min, $max ,$decimals = 2) {
      $scale = pow(10, $decimals);
      return mt_rand($min * $scale, $max * $scale) / $scale;
  }


/*
    function frand($min, $max) {
      return $min + mt_rand() / mt_getrandmax() * ($max - $min);

    }*/

// kritika code

// kritika







?> 

<!doctype html>
<html>
<head>

<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medication</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
#extra{ display: none; }
</style>
<script type="text/javascript">
  $(function() {
  var test = <?php echo $status;?> ;
   
      //return false;
        //medicalConditions = '<?php  echo $fieldData[0]->medical_conditions !=''?$fieldData[0]->medical_conditions:"blank";?>'; 
       // medicalRegular = '<?php  echo $fieldData[0]->medical_regular !=''?$fieldData[0]->medical_regular:"blank";?>'; 
      medicalConditions = '<?php  echo $medical_cond_selected;?>'; 
      medicalRegular = '<?php  echo $medication_selected;?>';
//alert(medicalConditions);
        //medicalConditions = 'arthritis or osteoporosis,kidney disease,psychiatric illness'; 
  //alert(medicalConditions);
    
    if(medicalConditions !='blank'){
        $("#medicalConditions").val(medicalConditions);
        var res = medicalConditions.split(",");
        res.map( function(item) {
             var newElement = document.createElement("li");
            newElement.innerHTML =item;
            newElement.setAttribute("onclick", "FetchValue(this);");
            newElement.setAttribute("class", "list1");
            document.getElementById('list').appendChild(newElement);
         //alert(item);
        })
      }
      
      if(medicalRegular !='blank'){
       $("#medicalRegular").val(medicalRegular);
        var res = medicalRegular.split(",");
        res.map( function(item) {
             var newElement = document.createElement("li");
            newElement.innerHTML =item;
            newElement.setAttribute("onclick", "FetchValue2(this);");
            newElement.setAttribute("class", "list2");
            document.getElementById('list2').appendChild(newElement);
         //alert(item);
        })
      }
    /*  
     if(medicalConditions)
         {
             var medicalname   = medicalConditions.split(',');
            alert(medicalname);
         }
    else 
        {
            alert('Blank');
        }
  */
  	$(window).bind('scroll', function() {
		  
	   var navHeight = $( window ).height() - 400;
			 if ($(window).scrollTop() > 100) {
				 $('.left').addClass('fixed');
			 }
			 else {
				 $('.left').removeClass('fixed');
			 }
		});  
		


   
   $("#dropdown_id").change(
           
           function(){
       
        
        var lst = document.getElementById('list');
      
         var text = $("#dropdown_id option:selected").text() ;
                 
        text = text.replace(/^\s+|\s+$/g,"");  // strip leading and trailing spaces
       // console.log(text);
        if (text == "") 
        {
            alert ("Please enter a value!");
            return false;
        }
       
          var newElement = document.createElement("li");
            newElement.innerHTML =text;
            newElement.setAttribute("onclick", "FetchValue(this);");
            newElement.setAttribute("class", "list1");
            document.getElementById('list').appendChild(newElement);
           // nCount++;
       
        var list1 ='';
           
                  $( ".list1" ).each(function( index ) {
                      list1 += $(this).text()+',' ;
                     
                  });
               //   console.log(list1);
            $("#medicalConditions").val(list1); 
    //.attr('selected', 'selected');
    $("#dropdown_id").val(0);
    
   } ) ;
        
    
   
        
         
    
    

//////////////////regular basis /////////////////////////////
 $("#regular_basis").change(function(){
        
       // var text = $("#regular_basis option:selected").text() ;
        // AddItem2(text,text)
         
        var lst = document.getElementById('list2');
      
         var text = $("#regular_basis option:selected").text() ;
                 
        text = text.replace(/^\s+|\s+$/g,"");  // strip leading and trailing spaces
       // console.log(text);
        if (text == "") 
        {
            alert ("Please enter a value!");
            return false;
        }
       
          var newElement = document.createElement("li");
            newElement.innerHTML =text;
            newElement.setAttribute("onclick", "FetchValue2(this);");
            newElement.setAttribute("class", "list2");
            document.getElementById('list2').appendChild(newElement);
            //nCount++;
            
              var list1 ='';
           
                  $( ".list2" ).each(function( index ) {
                      list1 += $(this).text()+',' ;
                     
                  });
               //   console.log(list1);
            
            
            
            $("#medicalRegular").val(list1);
             $("#regular_basis").val(0);
}) ;

 if(test ==1){
$("#extra").show();
$("#extra").show("fast");
			$('.mid-medi').addClass("big");
			$('.mid-medi').removeClass("small");

}  



// kritika Code
 $(document).ready(function () {	 
	 var checkedval='<?php echo $checkedval; ?>';
	 //alert(checkedval);
	 if(checkedval == "Y")
	 {
		 $("#extra").show();
	 }
	 else{
		 $("#extra").hide();
	 }
	// alert("aaaaaaaaaa");
	 
  //if (showActivities) $("#show_ACTIVITIES").show();
});

// kritika Code




	
 $(".bone").click(function(){

          if($(this).val() === "Y")
            {
			$("#extra").show();
			$('.mid-medi').addClass("big");
			$('.mid-medi').removeClass("small");
			}
          else
		  {
            $("#extra").hide();
			//$('.mid-medi').css('height', '800px !important');
			$('.mid-medi').addClass("small");
			$('.mid-medi').removeClass("big");
			}
        }); 
	
		
		$("#pt1,#pt2,#pt3,#pt4,#pt5,#pt6,#pt7,#pt8,#pt9,#pt10,#pt11,#pt12,#pt13,#pt14,#pt15 ").click(function() {
		 var  id =$(this).attr('id');

		  
		    var elementId ="#"+id;
		
		    var status;
			if($(elementId).hasClass('dot-green')){
			status=0;
			//alert('green');
			$(this).removeClass("dot-green");
			$(this).addClass("dot-red");
			
					$.ajax({
			            url: "<?php echo base_url(); ?>index.php/welcome/updateMedicationInfo/", 
						type: "get", //send it through get method
						data:{column:id,val:status},
                 
                    success: function (response) {
//alert(response);
//alert("success");   
//$('#order').fadeIn().html(response);
                    },

                    error: function (xhr) {
					alert(response);
					console.log(response);
                       // alert("Something went wrong, please try again");

                    }

                });
			}else{
			status=1;
			//alert('red');
			
			var id =$(this).attr('id');
			
			
			
			//alert("red"+$(this).attr('id'));
				$(this).removeClass("dot-red");
				$(this).addClass("dot-green");
				//$(this).parents(".portlet:first").find(".portlet-content").toggle();
				
				$.ajax({
			        url: "<?php echo base_url(); ?>index.php/welcome/updateMedicationInfo/", 
						type: "get", //send it through get method
						data:{column:id,val:status},
                 
                    success: function (response) {
//alert(response);
//alert("success");   
//$('#order').fadeIn().html(response);
                    },

                    error: function (xhr) {
//alert(response);
                        alert("Something went wrong, please try again");

                    }

                });
			
			}
			
			
			});
		
			
			
			
  
  });  
    
    
  
    

        function FetchValue(listItem) {
            
        
        // listItem.style.background = "#FF00FF";
            document.getElementById('selectedValue').innerHTML = listItem.innerHTML;
          

           
                  $( ".list1" ).each(function( index ) {
                      if($(this).text()===listItem.innerHTML){
                       console.log( index + ": " + $(this).text() );
                       list.removeChild(list.childNodes[index])
                      
                      }
                       setValue(listItem);
                  });
            
            
        }
        
        function setValue(listItem) {
            //alert(listItem.innerHTML);
           // listItem.style.background = "#FF00FF";
            document.getElementById('selectedValue').innerHTML = listItem.innerHTML;
          
                var list1 ='';
           
                  $( ".list1" ).each(function( index ) {
                      list1 += $(this).text()+',' ;
                     
                  });
                  console.log(list1);
            $("#medicalConditions").val(list1);
            
        }
        
         function FetchValue2(listItem) {
            //alert(listItem.innerHTML);
           // listItem.style.background = "#FF00FF";
            document.getElementById('selectedValue2').innerHTML = listItem.innerHTML;
          

           
                  $( ".list2" ).each(function( index ) {
                      if($(this).text()===listItem.innerHTML){
                       console.log( index + ": " + $(this).text() );
                       list2.removeChild(list2.childNodes[index])
                     
                      }
                        setValue2(listItem);
                  });
            
            
        }
        function setValue2(listItem) {
            //alert(listItem.innerHTML);
           // listItem.style.background = "#FF00FF";
            document.getElementById('selectedValue').innerHTML = listItem.innerHTML;
          
                var list1 ='';
           
                  $( ".list2" ).each(function( index ) {
                      list1 += $(this).text()+',' ;
                     
                  });
                // console.log(list1);
            $("#medicalRegular").val(list1);
            
        }
		
	
 
  </script>

<style>
.big{height:1190px !important;}
.small{height:800px !important;}
/*.dot-red{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#00ff00;}*/

.dot-red{position:relative;width:21px;height:21px;border-radius:15px;background:none;border-color:#000;border-width:2px;border-style:solid}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
</style>
</head>
<script type="text/javascript">
        $(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
<body>
 <?php echo form_open('welcome/saveClientMeditationInfo',''); ?> 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedication" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_org.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Pre-exercise Screening</h3>
            <p>Medications & Conditions</p>
        </div>
        
        <div class="orng_box_btn f_right">
        	<a onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_org.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>
        </div>
       <div class="overlay">&nbsp;</div>
        <div class="info_block">
            <div class="info_block_head">Medications & Conditions</div>
            <p>Select any diagnosed medical conditions you have and also any regular medications that you have been prescribed. 
               Only major categories of medications are listed. More than one can be selected.
               The final question is on bone and joint pain that you experience regularly and that is made worse by exercise. 
               If you answer ‘YES’ then a figure appears that allows you to click on to highlight which areas of the body you experience these problems. 
               More than one site can be highlighted.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>       
    
    </div>
</div>

<!--Start Wrapper --> 
<div class="wrapper">

<!--Start login --> 
<div class="login-cont" style="display:none;">
     
<!--	<form action="<?php //echo site_url('welcome/fetchClientInfo'); ?>" method="get" id="inputs"> -->
<!--    <div class="section">
    	<span><b>First name</b><input name="fname" type="text" size="60" required value="<?php //echo $this->session->userdata('user_first_name') ;?>"  disabled="disabled"></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" required  value="<?php //echo $this->session->userdata('user_last_name') ;?>"  disabled="disabled"><input name="submitMedication" type="submit" value="" title="edit client details" /></span>
     </div>  -->
   <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required><input name="submitMedication" type="submit" value="" title="edit client details"/></span>
     </div>
<!--	</form> -->
</div><!--End login --> 

<!--Start contain --> 
<div class="contain medi-contain-new">
   
   <!--Start left --> 
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            <?php //echo form_submit('mysubmit1','',"class='client_submit_form11' , 'id' = 'myform1'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            <?php //echo form_submit('mysubmit2','',"class='client_submit_form2' , 'id' = 'myform2'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            <?php //echo form_submit('mysubmit3','',"class='client_submit_form3' , 'id' = 'myform3'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            <?php //echo form_submit('mysubmit4','',"class='client_submit_form4' , 'id' = 'myform4'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn active" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            <?php //echo form_submit('mysubmit5','',"class='client_submit_form5' , 'id' = 'myform5'");  ?>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            <?php //echo form_submit('mysubmit6','',"class='client_submit_form6' , 'id' = 'myform6'");  ?>
            </div>
     
       </div>
   <!--End Left --> 
 
   <!--Start right --> 
    <?php //print_r($screening); ?>
   <div class="right">
       
   		<div class="right-head" style="margin-bottom:0;">Medications & Conditions</div>
   		<div class="right-section page_medi">
        <div class="field_row checkbox_title_row" style="border:0;">
      		<div class="field_85">&nbsp;</div>
            <div class="field_15">
            	<span>Yes</span>
            	<span>No</span>                
            </div>        
      </div>
      
        <div class="field_row"> 
            <div class="field_85">Have you spent time in hospital (including day admission) for any medical condition/illness/injury during the last 12 months?</div>
            <div class="field_15">
            	<?php $radio_is_checked = ($fieldData[0]->option_1 === 'Y')?"checked":"";
                echo form_radio(array("name"=>"option_1","id"=>"option_1Y","value"=>"Y", "class"=>"gender","checked"=>$radio_is_checked )); ?> <label for="option_1Y"><span></span></label>
                <?php $radio_is_checked = ($fieldData[0]->option_1 === 'N')?"checked":"";
                echo form_radio(array("name"=>"option_1","id"=>"option_1N","value"=>"N", "class"=>"gender","checked"=>$radio_is_checked ,'checked' =>'checked')); ?> <label for="option_1N"><span></span></label>
            </div>
        </div>
        
        <div class="field_row"> 
        <div class="field_50">
        	List any medical conditions you have
            <?php
              $options = array(
                  'selectcountry'  => 'Please choose...',
                            'arthritis or osteoporosis'  => 'arthritis or osteoporosis',
                            'cardiovascular'    => 'cardiovascular',
                            'cerebrovascular'   => 'cerebrovascular',
                            'diabetes' => 'diabetes',
							'kidney disease'  => 'kidney disease',
                            'liver or metabolic disorders'    => 'liver or metabolic disorders',
                            'psychiatric illness'   => 'psychiatric illness',
                            'respiratory disease' => 'respiratory disease',
							'thyroid disease'   => 'thyroid disease',
                            'other' => 'other',
                );
            echo form_dropdown('dropdown_name', $options, 'selectcountry', 'class="dropdown_class"  id="dropdown_id"');
        ?>
        
        </div>
        
        <div class="field_50">
        	Click to remove
            <div class="selected">
        		<div class="selected-head">Selected Item: <span id="selectedValue" style="display:none"></span></div>
        		<div class="selected-value">
        			<ul id="list" style="list-style:none none inside;margin:0px;padding:0px; cursor:pointer;"></ul>
                 </div>
           </div>
       </div>
   
    <!--<div class="selected2"><div>
        Selected Item: <span id="selectedValue2"></span>
    </div>
         </div>
                
        <div class="selected-head">Selected values</div>
       <?php
       $medical_conditions = $fieldData[0]->medical_conditions==''?'':$fieldData[0]->medical_conditions;
       $medical_conditions_arr = explode(',', $medical_conditions);
       
       $options = array(
                
            );
     $options=  array_merge($options,$medical_conditions_arr);
            
            $shirts_on_sale = array();
            $js = 'id="sometext" ; style="height:100px;" ';
            echo form_multiselect('sometext', $options, $shirts_on_sale,$js );
        ?>
      
         <input type="button" class="btnDel" value="Delete" />
                -->
        <input type="hidden" id="medicalConditions" name="medicalConditions" /> 
        <input type="hidden" id="medicalRegular" name="medicalRegular" /> 
        
        

        
        </div>
        
        
        <!--Start row--> 
       <div class="field_row"> 
        
        <div class="field_50">
        List any medications you take on a regular basis
         <?php
                            
              
              $options = array(
                  'selectcountry'  => 'Please choose...',
                            'BLOOD PRESSURE-lowering medication'  => 'BLOOD PRESSURE-lowering medication',
                            'cardiovascular'    => 'cardiovascular',
							'diabetic'  => 'diabetic',
                            'GLUCOSE-lowering medication'   => 'GLUCOSE-lowering medication',
                            'hormone replacement therapies' => 'hormone replacement therapies',
                            'LIPID-lowering medication'    => 'LIPID-lowering medication',
                            'metabolic'   => 'metabolic',
                            'musculo-skeletal' => 'musculo-skeletal',
							'psychiatric'  => 'psychiatric',
                            'renal medication'    => 'renal medication',
                            'respiratory'   => 'respiratory',
                            'other' => 'other',
                );
				
            echo form_dropdown('regular_basis', $options, 'selectcountry', 'class="dropdown_class"  id="regular_basis"');
      
        ?>
        </div>
        
        <div class="field_50">
          <div class="selected">
            <div class="selected-head">Selected Item: <span id="selectedValue2" style="display:none"></span></div>
            <div class="selected-value">
                <ul id="list2" style="list-style:none none inside;margin:0px;padding:0px; cursor:pointer;"></ul>
             </div>
         </div>
        </div>
        
         <?php
		 /*
            $medical_regular = $fieldData[0]->medical_regular==''?'':$fieldData[0]->medical_regular;
            $medical_regular_arr = explode(',', $medical_regular);
       
            $options = array(
                
            );
            $options=  array_merge($options,$medical_regular_arr);
            $shirts_on_sale = array();
            $js = 'id="sometext2" ; style="height:100px;" ';
            echo form_multiselect('sometext2', $options, $shirts_on_sale,$js );
			*/
        ?>

         
        </div><!--End row--> 
        
         <div class="field_row"> 
  		<?php if($screening['gender']=='F'){ ?>      
        <div class="field_85">Are you pregnant or have you given birth within the last 12 months?</div>
         <div class="field_15">
         <?php $radio_is_checked = ($fieldData[0]->option_pregnant === 'Y')?"checked":""; 
           echo form_radio(array("name"=>"option_pregnant","id"=>"option_pregnantY","value"=>"Y", "class"=>"gender","checked"=>$radio_is_checked)); ?> <label for="option_pregnantY">Yes<br><span></span></label>
		 <?php $radio_is_checked = ($fieldData[0]->option_pregnant === 'N')?"checked":""; 
           echo form_radio(array("name"=>"option_pregnant","id"=>"option_pregnantN","value"=>"N", "class"=>"gender","checked"=>$radio_is_checked )); ?> <label for="option_pregnantN">No<br><span></span></label>		   
          </div>
    	<?php  }?> 
        </div>     
     
	 <div class="field_row"> 
		<div class="field_85">Do you have any muscle, bone or joint pain or soreness that is made worse by particular types of activity?</div>
         <div class="field_15">
		 	<?php $radio_is_checked = ($checkedval === 'Y')?"checked":"N";
            echo form_radio(array("name"=>"option_4","id"=>"option_4Y","value"=>"Y", "class"=>"gender  bone",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_4Y">Yes<br><br><span></span></label>
			<?php 
            echo form_radio(array("name"=>"option_4","id"=>"option_4N","value"=>"N", "class"=>"gender bone",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_4N">No<br><br><span></span></label>
			
         </div>
      </div>
        <div id="extra" style="background-image:url(http://115.112.118.252/healthscreen/assets/images/anatomical.png);height:470px;width:55%;background-repeat:no-repeat;background-size:80% 100%;  margin: 0 auto;">
	    <!--back-Left arm -->
	   <div id="pt1" class="<?php echo in_array(1,$sitearr_values)?'dot-green':'dot-red';?>" style="left:72px;top:83px"></div>
	   <div id="pt2" class="<?php echo in_array(2,$sitearr_values)?'dot-green':'dot-red';?>" style="left:45px;top:121px"></div>
	   <div id="pt3" class="<?php echo in_array(3,$sitearr_values)?'dot-green':'dot-red';?>" style="left:30px;top:141px"></div>
	   
	   <!--back-Back -->
	   <div id="pt4" class="<?php echo in_array(4,$sitearr_values)?'dot-green':'dot-red';?>" style="left:149px;top:-25px"></div>
	   <div id="pt5" class="<?php echo in_array(5,$sitearr_values)?'dot-green':'dot-red';?>" style="left:149px;top:9px"></div>
	   <div id="pt6" class="<?php echo in_array(6,$sitearr_values)?'dot-green':'dot-red';?>" style="left:149px;top:40px"></div>
	   
	   <!--back-Right arm -->
	   <div id="pt7" class="<?php echo in_array(7,$sitearr_values)?'dot-green':'dot-red';?>" style="left:222px;top:-69px"></div>
	   <div id="pt8" class="<?php echo in_array(8,$sitearr_values)?'dot-green':'dot-red';?>" style="left:247px;top:-29px"></div>
	   <div id="pt9" class="<?php echo in_array(9,$sitearr_values)?'dot-green':'dot-red';?>" style="left:267px;top:-10px"></div>
	   
	   <!--back-left leg -->
	   <div id="pt10" class="<?php echo in_array(10,$sitearr_values)?'dot-green':'dot-red';?>" style="left:103px;top:-25px"></div>
	   <div id="pt11" class="<?php echo in_array(11,$sitearr_values)?'dot-green':'dot-red';?>" style="left:104px;top:60px"></div>
	   <div id="pt12" class="<?php echo in_array(12,$sitearr_values)?'dot-green':'dot-red';?>" style="left:92px;top:138px"></div>
	   
	   <!--back-Right leg -->
	   <div id="pt13" class="<?php echo in_array(13,$sitearr_values)?'dot-green':'dot-red';?>" style="left:192px;top:-100px"></div>
	   <div id="pt14" class="<?php echo in_array(14,$sitearr_values)?'dot-green':'dot-red';?>" style="left:190px;top:-14px"></div>
	   <div id="pt15" class="<?php echo in_array(15,$sitearr_values)?'dot-green':'dot-red';?>" style="left:201px;top:63px"></div>

	   <!----------------------------------------------------------------------------->
	    
	   </div>
    
      
       <div class="field_row" style="border-bottom:0;"> 
       <label>Notes:</label>
        <?php   $notes = $fieldData[0]->notes==''?'':$fieldData[0]->notes;
				$opts = 'placeholder="Notes", maxlength="200"';
		?>
		 

      <?php echo form_textarea('notes',set_value('description', $notes),$opts); ?>
       </div>
       
      <?php echo form_submit('mysubmit4','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform4'");?>
      <?php echo form_submit('mysubmit6','Next',"class='lite_btn grey_btn f_right btn_orng'","'id' = 'myform6'");?>
       
      <?php echo form_close(); ?>
       
      </div><!--End right section--> 
   </div><!--End right --> 
</div><!--End contain -->

</div><!--End Wrapper --> 

</body>
</html>
