
<?php 

		//To show Hide Additional Info
		    if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
              $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
              $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
              $bodyfatdiv="block";    
            }
            else
            {
              $bodyfatdiv="none";    
            } 
			 
?>


<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/assets/css/"?>rangeslider.css">

<style>
.range_div{ display: inline-block; width:205px; margin: 12px 0;height: 346px; position: relative;} 	
.rangeslider{background:#e6e6e6 !important; box-shadow: none;}
.rangeslider__fill{background:#b3b3b3 !important;box-shadow: none;}
.rangeslider__handle{ background:#4d4d4d !important; width: 20px !important; height: 20px !important; z-index:99;}
.rangeslider--vertical { width:8px !important; height: 100%; left: 50%; margin-left: -4px;}
.rangeslider--vertical .rangeslider__handle { left: -6px !important; border: 0;}
.rangeslider__handle:after{display: none;}
	
.vio_range_div .rangeslider__fill{background:#c7b5e7 !important;}
.vio_range_div .rangeslider__handle{ background:#906bd0 !important;}
ul.range_numbers {list-style: none; margin: 0; padding: 0; position: absolute; left: 0; top:10px; z-index: -1;} 
ul.range_numbers li {color: #808080; font-size: 13px; width:78px; text-align: right; position:relative; margin-top: 11px;}
ul.range_numbers li:before,ul.range_limits li:before{content: ''; width:20px; height: 1px; position: absolute; background:#e9e9e9; right:-35%; top: 8px;} 
.range_div2 ul.range_numbers li{margin-top: 16px;}
	
	 sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}

</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="<?php echo "$base/assets/js/"?>rangeslider.js"></script>


<script type="text/javascript">
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		  $(".v_person").fadeTo( "slow" , 1, function() {});
		$(".v_detail").toggle();
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	$(document).on('click','.menu_btn', function(){
		$(this).toggleClass("active");
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	}); 
</script>
    

    
<script>
$(document).on('click','#restricted_profile', function(){
        $("#restricted_profile").attr("href", "<?php echo site_url('Body/restricted_profile'); ?>");
        document.forms["myform"].submit();
	});  
    
    
$(document).on('click','#full_profile', function(){
         //alert('done doen');
        $("#full_profile").attr("href", "<?php echo site_url('Body/full_profile'); ?>");
		document.forms["myform"].submit();	  
	});  
    
$(document).on('click','#error_analysis', function(){
		document.forms["myform"].submit();	  
        $("#error_analysis").attr("href", "<?php echo site_url('Body/error_analysis'); ?>");
	});  
	

	
</script>  
         
</head>
<body>
   <div class="v_person">
	 <a href="#" class="discart">x</a>
	 <div class="v_image">
	     
             <img src="<?php echo "$base/$filename"?>">
	 </div>
	 <div class="v_btn"><a href="#">Hide Details</a></div>
	 <div class="v_detail">
		<div class="field_row">
		<label>Name *</label>
		<input type="text" name="first_name" value="<?php echo "$_SESSION[user_first_name] $_SESSION[user_last_name]";?>">
	  </div>
		<div class="field_row gen">
		<label>Age *</label>
		<input type="text" name="age" value="<?php echo round($_SESSION['age']);?>">
	  </div>
		<div class="field_row">
			<div class="field_50">
				<label>Height [cm] *</label><input type="text" name="height" value="<?php echo round($_SESSION['HEIGHT'],1);?>">                  
			</div>

			<div class="field_50">
				<label>Body mass [kg] *</label><input type="text" name="body_mass" value="<?php echo round($_SESSION['MASS'],2);?>">                  
			</div>
		</div>
		
	  <div class="field_row">
			<div class="field_50">
				<label>BMI *</label><input type="text" name="BMI" value="<?php echo round($_SESSION['BMI'],2);?>">                  
			</div>

			<div class="field_50" style="display:<?php echo $bodyfatdiv;?>">
				<label>Avg % body fat*</label><input type="text" name="body_fat" value="<?php echo $_SESSION['bodyfat']; ?>">                  
			</div>
		</div>
	  
	  <div class="field_row">
		  <label>Sub-Population</label>
		  <?php 
                $subpop_array=array('selectoccupation'=>'Select','Active'=>'Active','General'=>'General','Sedentary'=>'Sedentary','Athlete'=>'Athlete');
                			
                  echo form_dropdown('subpopulation',$subpop_array,$_SESSION['subpopulation']); 
                ?>  
		</div>
		<div class="field_row" style="display:<?php echo $riskdiv;?>">
			<div class="field_50">
				<label>Risk factor score *</label><input type="text" name="risk_fact_sc" value="<?php echo $_SESSION['risk_factor'];?>">                  
			</div>

			<div class="field_50">
				<label>Risk Group *</label><input type="text" name="risk_group" value="<?php echo $_SESSION['risk_group'];?>">                  
			</div>
		</div>
		<div class="field_row gen" style="display:<?php echo $vo2maxdiv;?>">
		<label>VO2max [in mL/kg/min] *</label>
		<input type="text" name="VO2max" value="<?php echo $_SESSION['VO2max'];?>">
	  </div>
		
	 </div>
 </div>
 
<div class="header">
	<div class="wrapper">
    	<div class="head_left">Exercise Science Toolkit</div>
        <div class="head_right"><?php echo $_SESSION['user_first_name'].' ' ;?><?php echo $_SESSION['user_last_name'] ;?><input name ="submitMedical" type="submit" value="" title="edit client details" class="profile_edit_btn" /></div>
    </div>
</div>
<div class="orng_container violet_container">
	<div class="wrapper">
    	<div class="orng_box_btn f_left">
        	<a href="#" id="exit" onclick="window.location.href = '<?php  echo site_url('/'); ?>';"><img src="<?php echo "$base/assets/images/"?>back_vio.jpg"></a>
        </div>
        <div class="orng_box_head f_left">
        	<h3>Body Composition</h3>
            <p>Restricted Profile</p>
        </div>
        <div class="orng_box_btn f_right">
        	<a href="#" onclick="window.print(); return false;"><img src="<?php echo "$base/assets/images/"?>print_vio.jpg"></a>
        </div>  
        <div class="orng_box_btn f_right">
        	<a href="#" class="info_icon_btn"><img src="<?php echo "$base/assets/images/"?>info_vio.jpg"></a>
        </div>
        <div class="orng_box_btn f_right">
        	<form name="vpform" id="vprestrictedgen" method="post"> 
                <a href="#" class="virtual_btnrestricted"><img src="<?php echo "$base/assets/images/"?>virtual_icon.png" style="margin-top: 12px;"></a>
                <input type="hidden" name="vptype" value="restricted">
            </form> 
        </div>
		
		<div class="vitual_div">
			
		</div>
		<div class="overlay">&nbsp;</div> 
        
        <div class="info_block">
            <div class="info_block_head">Restricted Profile</div>
            <p>The Restricted Profile allows the user to enter up to 9 skinfolds, 5 girths and 2 bone breadths plus height and body mass and calculate a range of variables including:
			phantom z-scores for each anthropometry variable, % body fat using a range of prediction equations, skinfold sums and skinfold maps, somatotype, and plot variables against 
			national age- and sex-based norms. Click the �Virtual Profile� button to  generate a �virtual person� where plausible values are generated in the program to be used in the
			various analyses. These can also be changed as the user explores the various analyses and outputs.
            </p>    
             <div class="info_block_foot">
             	<a href="#" class="lite_btn grey_btn f_right close">Close</a>
             </div>                
        </div>
		
    </div>
</div>

<div class="wrapper">	
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Body/restricted_actions', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	<div class="contain">
    	<div class="side_menu">
        	<a href="#" class="menu_btn"></a>
            <div class="drop_main" style="position: absolute; top:0; display:none;">
        	<ul>
                <li><a href="<?php //echo site_url('Body/restricted_profile'); ?>" id="restricted_profile"><img src="<?php echo "$base/assets/images/"?>icon_restricted.png"> Restricted Profile</a></li>
                <li><a href="<?php //echo site_url('Body/full_profile'); ?>" id="full_profile"><img src="<?php echo "$base/assets/images/"?>icon_full_profile.png"> Full Profile</a></li>
                <li><a href="<?php //echo site_url('Body/error_analysis'); ?>" id="error_analysis"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Error Analysis</a></li>
                <li><a href="<?php //echo site_url('Body/changing_body_mass'); ?>" id="changing_body_mass"><img src="<?php echo "$base/assets/images/"?>icon_error.png"> Changing Body Mass</a></li>
            </ul>
        </div>
        </div>
        
        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">Changing Body Mass</div>
           <p>The current %BF of the client is XX%.</p>
           <p>To estimate the required body mass change, move the sliders on the <br>right side and choose the %BF goal and fat proportion.</p>
            
            
			<div class="half_container f_left">
				<div class="change_head">Current</div>
				
				<div class="field_row" style="border-bottom:0; padding-bottom:0;"> 
					<div class="field_50">
						<label>Body mass (kg)</label><input type="text" id="gender" name="gender" value="" style="width:180px;"> 
					</div>
					<div class="field_50">
						<label>% body fat</label>
						<input type="text" id="age" name="age" value="">
					</div>
				 </div>
				 
				<div class="range_div">
					<input name="slider_1" type="range" value="<?php echo count($fieldData)==0?0:$fieldData[0]->slider_1;?>" min="0" max="5" step="0.1" data-orientation="vertical" data-rangeslider>
					<ul class="range_numbers">
						<li>150</li>
						<li>140</li>
						<li>130</li>
						<li>120</li>
						<li>110</li>
						<li>100</li>
						<li>90</li>
						<li>80</li>
						<li>70</li>
						<li>60</li>
						<li>50</li>
						<li>40</li>
					</ul>
				</div> 
				
				<div class="range_div range_div2">
					<input name="slider_1" type="range" value="<?php echo count($fieldData)==0?0:$fieldData[0]->slider_1;?>" min="0" max="5" step="0.1" data-orientation="vertical" data-rangeslider>
					<ul class="range_numbers">
						<li>50</li>
						<li>45</li>
						<li>40</li>
						<li>35</li>
						<li>30</li>
						<li>25</li>
						<li>20</li>
						<li>15</li>
						<li>10</li>
						<li>5</li>
					</ul>
				</div>
				 
			</div>  
			<div class="half_container f_right">
				<div class="change_head">Goal</div>
				
				<div class="field_row" style="border-bottom:0; padding-bottom:0;"> 
					<div class="field_50">
						<label>Desired % body fat</label>
						<input type="text" id="height" name="height" value="" style="width:180px;">                         
					</div>
					<div class="field_50">
						<label>% mass change as fat</label>
						<input type="text" id="body_mass" name="body_mass" value="">
					</div>
				 </div>
				
				<div class="range_div vio_range_div">
					<input name="slider_1" type="range" value="<?php echo count($fieldData)==0?0:$fieldData[0]->slider_1;?>" min="0" max="5" step="0.1" data-orientation="vertical" data-rangeslider>
					<ul class="range_numbers">
						<li>30</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>3</li>
					</ul>
				</div> 
				
				<div class="range_div vio_range_div">
					<input name="slider_1" type="range" value="<?php echo count($fieldData)==0?0:$fieldData[0]->slider_1;?>" min="0" max="5" step="0.1" data-orientation="vertical" data-rangeslider>
					<ul class="range_numbers">
						<li>100</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>&nbsp;</li>
						<li>0</li>
					</ul>
				</div>
				
				<button class="lite_btn f_right" style="margin-right: 26px;">Use Default value</button>
			</div>  
           
			
			<div class="field_row verticle_field_row" style="border-top: 1px solid #e6e6e6; border-bottom: 0; margin-top: 20px; padding-top: 20px;">
				
			  <p>Using default (typical) value published in the literature:</p>	<br>
			  <div class="field_24">
				<label>Target body mass is</label>
				<input type="text" id="mass" name="mass" value="" style="width:72px;"> kg
			  </div>
			  <div class="field_24" style="width: 26%;">
				<label>The range is between</label>
				<input type="text" id="range" name="range" value="" style="width:72px;"> to  &nbsp;
				<input type="text" id="range2" name="range2" value="" style="width:72px;"> kg
			  </div>
			  <div class="field_24" style="width: 26%;">
				<label>The client needs to lose (or gain)</label>
				<input type="text" id="client" name="client" value="" style="width:72px;"> to   &nbsp;
				<input type="text" id="client2" name="client2" value="" style="width:72px;"> kg
			  </div>
			</div>
                 
        </div>
    </div>

        
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script>
$(function() {
    var $document = $(document);
    var $r = $('input[type=range]');
    $r.rangeslider({
        polyfill: false
    });
});        
</script>
   
</body>
</html>
