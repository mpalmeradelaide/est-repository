<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Pre-exercise screening</title>

<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<style type="text/css">
body {margin-left: 0px;margin-top: 0px;	margin-right: 0px;margin-bottom: 0px;
}
.footer a{color:#333;}
a.tooltip {outline:none;text-decoration:none;}
a.tooltip strong {line-height:30px;}
a.tooltip:hover {text-decoration:none;} 
a.tooltip span {z-index: 10;display: none;padding: 14px 20px;margin-top: -133px;margin-left: -550px;width: 600px;line-height: 16px;
height: 100px;}
a.tooltip:hover span{display:inline; position:absolute; color:#111; border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:20;position:absolute;top:100px;border:0;left:12px;}
a.tooltip span{ border-radius:4px;box-shadow: 5px 5px 8px #CCC;}

a.tooltip2 {outline:none;text-decoration:none;}
a.tooltip2 strong {line-height:30px;}
a.tooltip2:hover {text-decoration:none;} 
a.tooltip2 span {z-index: 10;display: none;padding: 14px 20px;margin-top: -151px;margin-left: -346px;width: 600px;line-height: 16px;
height: 100px;}
a.tooltip2:hover span{display:inline; position:absolute; color:#111; border:1px solid #DCA; background:#fffAF0;}
.callout {z-index:21;position:absolute;top:100px;border:0;left:12px;}
a.tooltip2 span{ border-radius:4px;box-shadow: 5px 5px 8px #CCC;}
.fitness_btn{background: #c60003; height: 45px; border: none; display: block; cursor: pointer; margin: 40px 0 0 0; float: right; margin-left: 0px; font-family: "MYRIADPROREGULAR"; font-size: 24px; color: #FFF; text-align: center; text-decoration: none; line-height: 45px;}
</style>
</head>

<body>
 
<!--Start Wrapper --> 
<div class="wrapper">

  <div class="logo"><img src="<?php echo "$base/$image"?>/logo.png" width="540" height="65" alt="Health Screen Logo"></div>



  
 <!--Start Mid --> 
  <div class="mid">
  <!--Start Access Code box -->
  <form action="<?php echo site_url('welcome/clientDetail'); ?>" method="post" accept-charset="utf-8" id="clientInfo">  
	<div style="margin: 0 auto; width:100%; text-align:center;">
		<!--<a href="#"><div class="quit-btn">Quit</div></a>-->			
				<div><?php echo $msg ;  ?></div>
				<div class="lsection" style="display:inline-block; width:auto; float:none; margin-top:20px;">
					<span style="margin:0;"><p>Access Code</p><input type="text" name="code" value="" style="width:150px;"></span>
				</div>	
	</div> <!--End Access Code box --> 

    <!--Start Buttons --> 
    <div class="buttons" style="padding-top:20px;">
      <ul>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_1.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_2.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_3.PNG" width="140" height="140"></a></li>
      	<li><a href="#" class="last"><img src="<?php echo "$base/$image"?>/img_4.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_5.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_6.PNG" width="140" height="140"></a></li>
      	<li><a href="#"><img src="<?php echo "$base/$image"?>/img_7.PNG" width="140" height="140"></a></li>
      	<li><a href="#" class="last"><img src="<?php echo "$base/$image"?>/img_8.PNG" width="140" height="140"></a></li>
      </ul>
    </div><!--End Buttons --> 
   
  
<!--Start bottom buttons -->
<div style="margin: 0 auto;width:730px;">
	<!--<a href="#"><div class="quit-btn">Quit</div></a>-->
       
			<span style="float:left;">
				<input type="submit" value="Pre-exercise screening" class="start-btn" style="margin-top: 0px; width:280px;"/>
			 </span>
			 <span style="float:right;">
				<a href="<?php echo site_url('Fitness/'); ?>" class="start-btn fitness_btn" style="margin-top: 0px; width:280px;">Fitness testing<a/>
			 </span>
        
    <!--<div class="start-btn"><?php echo anchor('welcome/clientDetail', 'Begins', 'title="Begins"'); ?></div>-->
</div> <!--End botton buttons -->
</form>
<?php //echo anchor('welcome/clientDetail', 'Begins', 'title="Begins"'); ?>
<?php //echo $menu; ?>

<div class="footer">
<div>Developed by Dr Kevin Norton </div>
<div style="display:none;"> &copy; Copyright<a href="#" class="tooltip2"> AFIRM <span title="">AFIRM project team:
Professor Patrick Keyzer, School of Law, La Trobe University, Australia ; 
Professor Kevin Norton, School of Health Sciences, University of South Australia ; 
Professor Joachim Dietrich, Faculty of Law, Bond University, Australia ; 
Veronica Jones, General Manager, The Outsource Place, Australia ; 
Professor Caroline F Finch, Australian Centre for Research into Injury in Sport and it's Prevention, Federation University, Australia ; 
Dr Betul Sekendiz, School of Medical and Applied Sciences, Central Queensland University, Australia ; 
Shannon Gray, Monash Injury Research Institute, Monash University, Australia </span></a>project team </div>
</div>



  </div><!--End Mid --> 
</div><!--End Wrapper --> 

</body>
</html>


