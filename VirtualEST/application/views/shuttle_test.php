<?php 
			  //To show Hide Additional Info
		   if(isset($_SESSION['risk_factor']))
            {
              $riskdiv="block";    
            } 
            else
            {
            $riskdiv="none";    
            }   
            if(isset($_SESSION['VO2max']))
            {
            $vo2maxdiv="block";    
            }
            else
            {
             $vo2maxdiv="none";    
            }
             if(isset($_SESSION['bodyfat']))
            {
            $bodyfatdiv="block";    
            }
            else
            {
             $bodyfatdiv="none";    
            }
			 $is_dynamic=$_SESSION['is_virtual']; 
			 
			
			 
  ?>
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Result Screen</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<style>
sub {
font-size: 75%;
line-height: 0;
position: relative;
vertical-align: baseline;
}
sub {
bottom: -0.25em;
}
</style>
</head>
<script type="text/javascript">
	window.onload = function() {
	  var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
	 $("#vo2Max_value").val(<?php echo $_SESSION['VO2max'];?>); 
	$("#shuttle_level").val("<?php echo $_SESSION['shuttle_level'];?>"); 
	 $("#sixmin_test").val(<?php echo $_SESSION['sixmin_test'];?>); 	
		}	
	  changeMean();
	};	
	$(document).ready(function() {
        var divHeight = $('.contain').height(); 
        $('.side_menu').css('height', divHeight+'px');
		 var is_dynamic='<?php echo $is_dynamic;?>';
		if(is_dynamic == 1)
	    {
		$(".v_person").fadeTo( "slow" , 1, function() {}); 
		$(".v_detail").toggle();
		}
    });
	$(document).on('click','#anaerobic_strength, #anaerobic_capacity, #aerobic_fitness', function(){
		$(".sub_menu").hide();
		$(this).next(".sub_menu").toggle().animate({left: '274px', opacity:'1'});
	});
	$(document).on('click','#VO2max', function(){
		$(".inner_sub_menu").slideUp();
		$(this).next(".inner_sub_menu").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	$(document).on('click','#max-txt', function(){
		$(".inner_sub_menu2").slideUp();
		$(this).next(".inner_sub_menu2").toggle().animate({left: '274px', opacity:'1'});		
	});
	
	
	$(document).on('click','.menu_btn', function(){
		$(".drop_main").toggle().animate({left: '76px', opacity:'1'});
	});
	$(document).on('click','.info_icon_btn', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	$(document).on('click','.close', function(){
		$(".info_block").toggle();
		$(".overlay").toggle();
	});  
	 $(document).on('click','.vitual_btn', function(){
                  $("#vpshuttle_test").attr("action", "<?php echo base_url(); ?>index.php/Fitness/FitnessVirtualpersonGeneration");     
                  $("#vpshuttle_test").submit();       
         $(".v_person").fadeTo( "slow" , 1, function() {});
	});
	$(document).on('click','.discart', function(){
		$(".v_person").fadeTo( "slow" , 0, function() {});
	});
	$(document).on('click','.v_btn a', function(){
		$(this).text(function(i, v){
              return v ==='Hide Details' ? 'Show details' : 'Hide Details'
        });
		$(".v_detail").slideToggle();
	}); 
</script>
</head>
<body>

 <!-- insertion of header -->
 <?=$header_insert; ?>

<div class="overlay">&nbsp;</div> 
<div class="info_block">
	<div class="info_block_head green_container">20m Shuttle Test</div>
	<p>The 20 m shuttle test is a continuous test protocol to maximal effort. It is based on the protocol of Brewer, J., Ramsbottom, R., & Williams, C.(1988). <b>Multistage fitness test: A progressive shuttle-run test for the prediction of maximum oxygen uptake.</b> Belconnen, ACT: Australian Coaching Council.
            The person being tested continues until fatigue and their final shuttle level is entered in the program [using the drop-down menu]. This shuttle score corresponds to a predicted VO2max. The predicted V02max can be plotted against population norms for specific age and gender groups once the user clicks 'Plot'.
            V02max scores for various sporting groups can also be plotted using the drop-down selection menu.</p>         
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>             
</div> 

<div class="wrapper">
<!-- Form begins -->    
<?php
$hidden = array('userid' => $id  );
//$attributes = array('id' => 'myform' , 'name'=>'myform');
echo form_open('Fitness/saveClientShuttleInfo', array('id'=>'myform','name'=>'myform'), $hidden); ?>  
	
    <div class="contain">

        <!--Start right --> 
        <div class="right-section right-section_new">
        	<div class="right-head">20m Shuttle Test</div>
        	<div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label>Test</label>
                	<select name="test" onchange="check_test_legitchoice()">
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic strength and power</option>                   
                    <option value="vertical_jump">Vertical jump</option>                  
                    <option value="flight_time">Flight : Contact time</option> 
                    <option value="peak_power">Peak power [W]</option>
                    <option value="strength">Strength</option>
                    <option value="#"></option>
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Anaerobic capacity</option>                   
                    <option value="30s_total_work_done">30 s total work [kJ]</option>   
                    <option value="maod_test">maximal accumulated oxygen deficit [MAOD]</option>  					
                    <option value="#"></option>
                    <option value="#" style="font-weight:bold; color: #6D6D6D;">Aerobic fitness</option>                   
                    <option value="#" style="font-weight:bold;">VO&#x0032;&#x006D;&#x0061;&#x0078; [mL/kg/min]</option>                                     
                    <option value="3x3min_submaximal_test">Submaximal</option> 
					<option value="#" style="font-weight:bold; color: #6D6D6D;">Maximal</option> 
					<option value="shuttle_test">20 m shuttle</option>
					<option value="bike_test">Predicting VO2max using maximal-effort treadmill or bike tests</option>
					<option value="#"></option>
                    <option value="v02max_test">Measured VO&#x0032;&#x006D;&#x0061;&#x0078;</option> 
                    <option value="lactate_threshold" style="font-weight:bold;">Lactate threshold</option>
                </select>

				<script>
					// refuse submit if 'test' value is '#'
					function check_test_legitchoice() {
						test_choice = $('select[name="test"]').children("option:selected").val();
						if (test_choice != "#") {
							$("#myform").submit();
						} else {
							$('select[name="test"]').val("shuttle_test");
						}
					}
				</script>

                </div>
                <div class="field_24">
                	<label>Gender</label>
                    <select id="gender" name="gender" onchange="changeMean();">
                        <option value="Male" <?php if($fieldData[0]->gender == "Male" || $_SESSION['user_gender'] == "M"){echo "Selected";}?>>male</option>
                        <option value="Female" <?php if($fieldData[0]->gender == "Female" || $_SESSION['user_gender'] == "F"){echo "Selected";}?>>female</option>
                    </select>
                    <script type="text/javascript">               
                       $("#gender").change(function ()
                        {
                            var selectedText = $(this).find("option:selected").text();
                            var selectedValue = $(this).val();
                          
                             if(selectedValue=='Male')
                             {
                                    $.ajax({
                                    type:'POST',
                                    url: "<?php echo base_url('index.php/Fitness/get_view_ajax_male/');?>",                   
                                     success: function (response) {
                                        document.getElementById("sport").innerHTML=response; 
                                      }
                                    }); 
                                 }else{
                                   $.ajax({
                                    type:'POST',
                                    url: "<?php echo base_url('index.php/Fitness/get_view_ajax_female/');?>",                    
                                    success: function (response) {
                                        document.getElementById("sport").innerHTML=response; 
                                      }
                                    }); 
                            }
    
                        });
             
                   </script>
                </div>
                <div class="field_24">
                	<label>Age (yr)</label>
                	<select id="age" name="age" onchange="changeMean();">
                    <?php foreach($age_array as $age){ ?>
                    <option value="<?php echo $age->age ;?>" <?php if($fieldData[0]->age == $age->age || $_SESSION['age_range'] == $age->age){echo "Selected";}?>><?php echo $age->age ;?></option>                   
                   <?php } ?>
                </select>
                </div>
                <div class="field_24">
                	<label>Comparison Sport</label>
                	<select name="sport" id="sport" onchange="changeSportMean()">                  
                    <option>Sports</option>
                     <?php  
                        foreach($sports_array as $sport){ ?>
                        <option value="<?php echo $sport->sports ;?>" <?php if($fieldData[0]->sports == $sport->sports){echo "Selected";}?>><?php echo $sport->sports ;?></option>              
                     <?php } ?>            
                </select>
                </div>
             </div>
             
             <div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label>Shuttle level</label>
                    <select id="shuttle_level" name="shuttle_level" onchange="shuttleLevel()">
						<option>1-1</option>
						<option>1-2</option>
						<option>1-3</option>
						<option>1-4</option>
						<option>1-5</option>
						<option>1-6</option>
						<option>1-7</option>
						<option>2-1</option>
                        <option>2-2</option>
                        <option>2-3</option>
                        <option>2-4</option>
                        <option>2-5</option>
                        <option>2-6</option>
                        <option>2-7</option>
                        <option>2-8</option>
						<option>3-1</option>
                        <option>3-2</option>
                        <option>3-3</option>
                        <option>3-4</option>
                        <option>3-5</option>
                        <option>3-6</option>
                        <option>3-7</option>
                        <option>3-8</option>
						<option>4-1</option>
                        <option>4-2</option>
                        <option>4-3</option>
                        <option>4-4</option>
                        <option>4-5</option>
                        <option>4-6</option>
                        <option>4-7</option>
                        <option>4-8</option>
                        <option>4-9</option>
						<option>5-1</option>
                        <option>5-2</option>
                        <option>5-3</option>
                        <option>5-4</option>
                        <option>5-5</option>
                        <option>5-6</option>
                        <option>5-7</option>
                        <option>5-8</option>
                        <option>5-9</option>
						<option>6-1</option>
                        <option>6-2</option>
                        <option>6-3</option>
                        <option>6-4</option>
                        <option>6-5</option>
                        <option>6-6</option>
                        <option>6-7</option>
                        <option>6-8</option>
                        <option>6-9</option>
                        <option>6-10</option>
						<option>7-1</option>
                        <option>7-2</option>
                        <option>7-3</option>
                        <option>7-4</option>
                        <option>7-5</option>
                        <option>7-6</option>
                        <option>7-7</option>
                        <option>7-8</option>
                        <option>7-9</option>
                        <option>7-10</option>
						<option selected>8-1</option>
                        <option>8-2</option>
                        <option>8-3</option>
                        <option>8-4</option>
                        <option>8-5</option>
                        <option>8-6</option>
                        <option>8-7</option>
                        <option>8-8</option>
                        <option>8-9</option>
                        <option>8-10</option>
                        <option>8-11</option>
						<option>9-1</option>
                        <option>9-2</option>
                        <option>9-3</option>
                        <option>9-4</option>
                        <option>9-5</option>
                        <option>9-6</option>
                        <option>9-7</option>
                        <option>9-8</option>
                        <option>9-9</option>
                        <option>9-10</option>
                        <option>9-11</option>			
                        <option>10-1</option>
                        <option>10-2</option> 
                        <option>10-3</option> 
                        <option>10-4</option> 
                        <option>10-5</option> 
                        <option>10-6</option> 
                        <option>10-7</option> 
                        <option>10-8</option> 
                        <option>10-9</option> 
                        <option>10-10</option> 
                        <option>10-11</option>                        
                        <option>11-1</option> 
                        <option>11-2</option> 
                        <option>11-3</option> 
                        <option>11-4</option> 
                        <option>11-5</option> 
                        <option>11-6</option> 
                        <option>11-7</option> 
                        <option>11-8</option>
                        <option>11-9</option>
                        <option>11-10</option>
                        <option>11-11</option>
                        <option>11-12</option>                        
                        <option>12-1</option>
                        <option>12-2</option>
                        <option>12-3</option>
                        <option>12-4</option>
                        <option>12-5</option>
                        <option>12-6</option>
                        <option>12-7</option>
                        <option>12-8</option>
                        <option>12-9</option>
                        <option>12-10</option>
                        <option>12-11</option>
                        <option>12-12</option>                        
                        <option>13-1</option>
                        <option>13-2</option>
                        <option>13-3</option>
                        <option>13-4</option>
                        <option>13-5</option>
                        <option>13-6</option>
                        <option>13-7</option>
                        <option>13-8</option>
                        <option>13-9</option>
                        <option>13-10</option>
                        <option>13-11</option>
                        <option>13-12</option>
                        <option>13-13</option>                        
                        <option>14-1</option>
                        <option>14-2</option>
                        <option>14-3</option>
                        <option>14-4</option>
                        <option>14-5</option>
                        <option>14-6</option>
                        <option>14-7</option>
                        <option>14-8</option>
                        <option>14-9</option>
                        <option>14-10</option>
                        <option>14-11</option>
                        <option>14-12</option>
                        <option>14-13</option>                        
                        <option>15-1</option>
                        <option>15-2</option>
                        <option>15-3</option>
                        <option>15-4</option>
                        <option>15-5</option>
                        <option>15-6</option>
                        <option>15-7</option>
                        <option>15-8</option>
                        <option>15-9</option>
                        <option>15-10</option>
                        <option>15-11</option>
                        <option>15-12</option>
                        <option>15-13</option>                        
                        <option>16-1</option>
                        <option>16-2</option>
                        <option>16-3</option>
                        <option>16-4</option>
                        <option>16-5</option>
                        <option>16-6</option>
                        <option>16-7</option>
                        <option>16-8</option>
                        <option>16-9</option>
                        <option>16-10</option>
                        <option>16-11</option>
                        <option>16-12</option>
                        <option>16-13</option>
                        <option>16-14</option>                                
                    </select>
                </div>
             <div class="field_24" style="width: 29%;">
                    <label for="value">Meters walked in 6-minute walk test</label>
           
                     <input type="text" name="sixmin_test" id="sixmin_test" readonly style="width: 78%;"> 
             </div>
			 </div>
             
             <div class="field_row verticle_field_row"> 
                <div class="field_24">
                	<label for="value">Predicted VO<sub>2max</sub> (mL/kg/min)</label>
                    <input type="text" name="vo2Max_value" id="vo2Max_value" readonly value="<?php echo isset($fieldData[0]->value)?$fieldData[0]->value:""; ?>"> 
                </div>
                <div class="field_24">
                    <label for="percentile">Performance %</label>
                    <input type="text" name="percentile" id="percentile" readonly value="<?php echo isset($fieldData[0]->percentile)?$fieldData[0]->percentile:""; ?>">
                </div>
                <input id="plot" name="plot" type="button" onclick="show()" class="lite_btn grey_btn f_right btn_green" value="Plot" style="margin-top:32px;"/>
                <input id="scorePlot" name="scorePlot" type="hidden" value="<?php echo isset($fieldData[0]->scorePlot)?$fieldData[0]->scorePlot:""; ?>"/>  
                <input id="sportPlot" name="sportPlot" type="hidden" value="<?php echo isset($fieldData[0]->sportPlot)?$fieldData[0]->sportPlot:""; ?>"/>  
                <input id="sportMeanScore" name="sportMeanScore" type="hidden" value="<?php echo isset($fieldData[0]->sportMeanScore)?$fieldData[0]->sportMeanScore:""; ?>"/> 
                <input id="shuttle_val" name="shuttle_val" type="hidden" value="<?php echo isset($fieldData[0]->shuttle_level)?$fieldData[0]->shuttle_level:""; ?>"/> 
             </div>
             
             <div class="field_row" style="border:0;">
             		<div class="graph">
                	<ul class="verticle_lines">                        
                        <li><span id="class1">~91</span></li> 
                        <li><span id="class2">~87</span></li> 
                        <li><span id="class3">~83</span></li> 
                        <li><span id="class4">~81</span></li> 
                        <li><span id="class5">~78</span></li> 
                        <li><span id="class6">~76</span></li> 
                        <li><span id="class7">~75</span></li> 
                        <li><span id="class8">~72</span></li> 
                        <li><span id="class9">~70</span></li> 
                        <li><span id="class10">~68</span></li> 
                        <li><span id="class11">~65</span></li> 
                        <li><span id="class12">~63</span></li> 
                        <li><span id="class13">~59</span></li> 
                        <li><span id="class14">~56</span></li>
                        <li><span id="class15">~52</span></li>                        
                        <li>
                          <div class="graph_dashed_line"><div class="graph_dashed_line">&nbsp;</div> <span id="meanVal" class="graph_dash_text">45</span> <span>average</span>                            
                            <div class="custom1"><div id="plot16_1" class="blue_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_1" class="red_dot" style="display:none;left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_2" class="blue_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_2" class="red_dot" style="display:none; left:50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom1"><div id="plot16_3" class="blue_dot" style="display:none; left:80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom4"><div id="score16_3" class="red_dot" style="display:none; left:80px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom2"><div id="plot17" class="blue_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom5"><div id="score17" class="red_dot" style="display:none; left:10px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_8" class="blue_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_8" class="red_dot" style="display:none; left:20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_7" class="blue_dot" style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_7" class="red_dot"  style="display:none; left:-8px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_6" class="blue_dot" style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_6" class="red_dot"  style="display:none; left:-20px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_5" class="blue_dot" style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_5" class="red_dot"  style="display:none; left:-50px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_4" class="blue_dot" style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_4" class="red_dot"  style="display:none; left:-80px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_3" class="blue_dot" style="display:none; left:-110px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_3" class="red_dot" style="display:none; left:-110px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_2" class="blue_dot" style="display:none; left:-150px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_2" class="red_dot" style="display:none; left:-150px;" ><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                            <div class="custom3"><div id="plot18_1" class="blue_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div></div><div class="custom6"><div id="score18_1" class="red_dot" style="display:none; left:-195px;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></div>
                          </div>
                        </li>                          
                        </ul>
                    
                        <ul class="bottom_values">
                            <?php $plot=1 ; foreach($prob_array as $val){?>
                            <li><span><?php echo $val->prob_range ;?></span> <div id="<?php echo "plot".$plot ; ?>" class="blue_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/blue_dot.png" alt=""/></div><div id="<?php echo "score".$plot ; ?>" class="red_dot" style="display:none;"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div></li>
                            <?php $plot++ ; }?>
                        </ul>
                            
                           <div class="score_box">
                            <div class="score_text">
                                    your<br/>score<br>
                                    <div style="margin-top: 10px;">Sport<br/>mean<br/><div id="sportMeanVal"></div></div> 
                            </div>                            
                            <!--<div class="red_dot"><img src="<?php echo "$base/$image"?>/red_dot.png" alt=""/></div>-->
                           </div>
                       
                       
                    <div class="graph_pulse_box">
                    	<span>frequency</span>
                    	<img src="<?php echo "$base/$image"?>/pulse.png" alt=""/> 
                    </div>
                </div>
             </div>
             
        </div>
    </div>

	
	<?php echo form_close(); ?>
<!-- Form ends -->
	
</div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
<script type="text/javascript">
$(function() {
var shuttleValue = document.getElementById("shuttle_val").value;  

		if(shuttleValue !== "")
		{						 
			$('#shuttle_level').val(shuttleValue);			
		}	
});
</script>
<script type="text/javascript">

	$(document).on('click','#exit', function(){
	document.forms["myform"].submit();
        //return false;
    });
	    
        $(document).on('click','.print_icon_toggle_btn', function(){
		$(".print_icon_toggle").toggle();
	});
</script> 
<script> 
    
    function changeMean()
    { 
        var ageValue = document.getElementById("age").value;      
      
        if(document.getElementById('gender').value == "Male") {
                
        if(ageValue == "18-29")
        {
            var mean = 44.7 ;
            var sd = 8.9 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 39.4 ;
           var sd = 8.55 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 37.3 ;
           var sd = 8.04 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 32.95 ;
           var sd = 7.5 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 30.6 ;
           var sd = 6.73 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 29.5 ;
           var sd = 6.31 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 36 ;
            var sd = 9.5 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 31.6 ;
           var sd = 9.05 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 30.5 ;
           var sd = 8.2 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 29.6 ;
           var sd = 7.35 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 25.7 ;
           var sd = 7.05 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 23.1 ;
           var sd = 6.5 ;        
      }               
        
    }    
    
    var r1 = (5.2000 * sd) + mean ;
    var r2 = (4.7550 * sd) + mean ;
    var r3 = (4.2650 * sd) + mean ;
    var r4 = (4.1100 * sd) + mean ;
    var r5 = (3.7200 * sd) + mean ;
    var r6 = (3.5410 * sd) + mean ;
    var r7 = (3.3550 * sd) + mean ;
    var r8 = (3.0900 * sd) + mean ;
    var r9 = (2.8785 * sd) + mean ;
    var r10 = (2.5760 * sd) + mean ;
    var r11 = (2.3270 * sd) + mean ;
    var r12 = (2.0550 * sd) + mean ;
    var r13 = (1.6600 * sd) + mean ;
    var r14 = (1.2816 * sd) + mean ;
    var r15 = (0.8420 * sd) + mean ;
     
      document.getElementById('meanVal').innerHTML = Math.round(parseFloat(mean));
      
      
      if(mean == 44.7)
      {
        document.getElementById('class1').innerHTML = "~91" ;
        document.getElementById('class2').innerHTML = "~87" ;
        document.getElementById('class3').innerHTML = "~83" ;
        document.getElementById('class4').innerHTML = "~81" ; 
        document.getElementById('class5').innerHTML = "~78" ;
        document.getElementById('class6').innerHTML = "~76" ;
        document.getElementById('class7').innerHTML = "~75" ;
        document.getElementById('class8').innerHTML = "~72" ;
        document.getElementById('class9').innerHTML = "~70" ;
        document.getElementById('class10').innerHTML = "~68" ;
        document.getElementById('class11').innerHTML = "~65" ;
        document.getElementById('class12').innerHTML = "~63" ;
        document.getElementById('class13').innerHTML = "~59" ;
        document.getElementById('class14').innerHTML = "~56" ;
        document.getElementById('class15').innerHTML = "~52" ;
        
     }
     else{
     
        document.getElementById('class1').innerHTML = "~"+Math.round(parseFloat(r1))   ;
        document.getElementById('class2').innerHTML = "~"+Math.round(parseFloat(r2))   ;
        document.getElementById('class3').innerHTML = "~"+Math.round(parseFloat(r3))   ;
        document.getElementById('class4').innerHTML = "~"+Math.round(parseFloat(r4))   ; 
        document.getElementById('class5').innerHTML = "~"+Math.round(parseFloat(r5))   ;
        document.getElementById('class6').innerHTML = "~"+Math.round(parseFloat(r6))   ;
        document.getElementById('class7').innerHTML = "~"+Math.round(parseFloat(r7))   ;
        document.getElementById('class8').innerHTML = "~"+Math.round(parseFloat(r8))   ;
        document.getElementById('class9').innerHTML = "~"+Math.round(parseFloat(r9))   ;
        document.getElementById('class10').innerHTML = "~"+Math.round(parseFloat(r10)) ;
        document.getElementById('class11').innerHTML = "~"+Math.round(parseFloat(r11)) ;
        document.getElementById('class12').innerHTML = "~"+Math.round(parseFloat(r12)) ;
        document.getElementById('class13').innerHTML = "~"+Math.round(parseFloat(r13)) ;
        document.getElementById('class14').innerHTML = "~"+Math.round(parseFloat(r14)) ;
        document.getElementById('class15').innerHTML = "~"+Math.round(parseFloat(r15)) ; 
   
     } 
     
     getZscore() ;
    // show() ;
      
  }
 
 function shuttleLevel()
 {     
     var shuttle = document.getElementById("shuttle_level").value ;
     //alert(shuttle); die;
     
     if(shuttle == "10-1")
     {
         document.getElementById("vo2Max_value").value = 47.1 ;
     }
     else if(shuttle == "10-2")
     {
         document.getElementById("vo2Max_value").value = 47.5 ;
     }
     else if(shuttle == "10-3")
     {
         document.getElementById("vo2Max_value").value = 47.8 ;
     }
     else if(shuttle == "10-4")
     {
         document.getElementById("vo2Max_value").value = 48.1 ;
     }
     else if(shuttle == "10-5")
     {
         document.getElementById("vo2Max_value").value = 48.4 ;
     }
     else if(shuttle == "10-6")
     {
         document.getElementById("vo2Max_value").value = 48.7 ;
     }
     else if(shuttle == "10-7")
     {
         document.getElementById("vo2Max_value").value = 49.0 ;
     }
     else if(shuttle == "10-8")
     {
         document.getElementById("vo2Max_value").value = 49.3 ;
     }
     else if(shuttle == "10-9")
     {
         document.getElementById("vo2Max_value").value = 49.6 ;
     }
     else if(shuttle == "10-10")
     {
         document.getElementById("vo2Max_value").value = 49.9 ;
     }
     else if(shuttle == "10-11")
     {
         document.getElementById("vo2Max_value").value = 50.2 ;
     }
     else if(shuttle == "1-1")
     {
         document.getElementById("vo2Max_value").value = 17.1 ;
     }
     else if(shuttle == "11-1")
     {
         document.getElementById("vo2Max_value").value = 50.5 ;
     }
     else if(shuttle == "11-2")
     {
         document.getElementById("vo2Max_value").value = 50.8 ;
     }
     else if(shuttle == "11-3")
     {
         document.getElementById("vo2Max_value").value = 51.1 ;
     }
     else if(shuttle == "11-4")
     {
         document.getElementById("vo2Max_value").value = 51.4 ;
     }
     else if(shuttle == "11-5")
     {
         document.getElementById("vo2Max_value").value = 51.6 ;
     }
     else if(shuttle == "11-6")
     {
         document.getElementById("vo2Max_value").value = 51.9 ;
     }
     else if(shuttle == "11-7")
     {
         document.getElementById("vo2Max_value").value = 52.2 ;
     }
     else if(shuttle == "11-8")
     {
         document.getElementById("vo2Max_value").value = 52.5 ;
     }
     else if(shuttle == "11-9")
     {
         document.getElementById("vo2Max_value").value = 52.8 ;
     }
     else if(shuttle == "11-10")
     {
         document.getElementById("vo2Max_value").value = 53.1 ;
     }
     else if(shuttle == "11-11")
     {
         document.getElementById("vo2Max_value").value = 53.4 ;
     }
     else if(shuttle == "11-12")
     {
         document.getElementById("vo2Max_value").value = 53.7 ;
     }
     else if(shuttle == "1-2")
     {
         document.getElementById("vo2Max_value").value = 17.5 ;
     }
     else if(shuttle == "12-1")
     {
         document.getElementById("vo2Max_value").value = 54.0 ;
     }
     else if(shuttle == "12-2")
     {
         document.getElementById("vo2Max_value").value = 54.2 ;
     }
     else if(shuttle == "12-3")
     {
         document.getElementById("vo2Max_value").value = 54.5 ;
     }
     else if(shuttle == "12-4")
     {
         document.getElementById("vo2Max_value").value = 54.8 ;
     }
     else if(shuttle == "12-5")
     {
         document.getElementById("vo2Max_value").value = 55.1 ;
     }
     else if(shuttle == "12-6")
     {
         document.getElementById("vo2Max_value").value = 55.4 ;
     }
     else if(shuttle == "12-7")
     {
         document.getElementById("vo2Max_value").value = 55.7 ;
     }
     else if(shuttle == "12-8")
     {
         document.getElementById("vo2Max_value").value = 55.9 ;
     }
     else if(shuttle == "12-9")
     {
         document.getElementById("vo2Max_value").value = 56.2 ;
     }
     else if(shuttle == "12-10")
     {
         document.getElementById("vo2Max_value").value = 56.5 ;
     }
     else if(shuttle == "12-11")
     {
         document.getElementById("vo2Max_value").value = 56.8 ;
     }
     else if(shuttle == "12-12")
     {
         document.getElementById("vo2Max_value").value = 57.1 ;
     }
     else if(shuttle == "1-3")
     {
         document.getElementById("vo2Max_value").value = 17.9 ;
     }
     else if(shuttle == "13-1")
     {
         document.getElementById("vo2Max_value").value = 57.3 ;
     }
     else if(shuttle == "13-2")
     {
         document.getElementById("vo2Max_value").value = 57.6 ;
     }
     else if(shuttle == "13-3")
     {
         document.getElementById("vo2Max_value").value = 57.9 ;
     }
     else if(shuttle == "13-4")
     {
         document.getElementById("vo2Max_value").value = 58.2 ;
     }
     else if(shuttle == "13-5")
     {
         document.getElementById("vo2Max_value").value = 58.4 ;
     }
     else if(shuttle == "13-6")
     {
         document.getElementById("vo2Max_value").value = 58.7 ;
     }
     else if(shuttle == "13-7")
     {
         document.getElementById("vo2Max_value").value = 59.0 ;
     }
     else if(shuttle == "13-8")
     {
         document.getElementById("vo2Max_value").value = 59.3 ;
     }
     else if(shuttle == "13-9")
     {
         document.getElementById("vo2Max_value").value = 59.5 ;
     }
     else if(shuttle == "13-10")
     {
         document.getElementById("vo2Max_value").value = 59.8 ;
     }
     else if(shuttle == "13-11")
     {
         document.getElementById("vo2Max_value").value = 60.1 ;
     }
     else if(shuttle == "13-12")
     {
         document.getElementById("vo2Max_value").value = 60.3 ;
     }
     else if(shuttle == "13-13")
     {
         document.getElementById("vo2Max_value").value = 60.6 ;
     }
     else if(shuttle == "1-4")
     {
         document.getElementById("vo2Max_value").value = 18.4 ;
     }
     else if(shuttle == "14-1")
     {
         document.getElementById("vo2Max_value").value = 60.9 ;
     }
     else if(shuttle == "14-2")
     {
         document.getElementById("vo2Max_value").value = 61.1 ;
     }
     else if(shuttle == "14-3")
     {
         document.getElementById("vo2Max_value").value = 61.4 ;
     }
     else if(shuttle == "14-4")
     {
         document.getElementById("vo2Max_value").value = 61.7 ;
     }
     else if(shuttle == "14-5")
     {
         document.getElementById("vo2Max_value").value = 61.9 ;
     }
     else if(shuttle == "14-6")
     {
         document.getElementById("vo2Max_value").value = 62.2 ;
     }
     else if(shuttle == "14-7")
     {
         document.getElementById("vo2Max_value").value = 62.5 ;
     }
     else if(shuttle == "14-8")
     {
         document.getElementById("vo2Max_value").value = 62.7 ;
     }
     else if(shuttle == "14-9")
     {
         document.getElementById("vo2Max_value").value = 63.0 ;
     }
     else if(shuttle == "14-10")
     {
         document.getElementById("vo2Max_value").value = 63.3 ;
     }
     else if(shuttle == "14-11")
     {
         document.getElementById("vo2Max_value").value = 63.5 ;
     }
     else if(shuttle == "14-12")
     {
         document.getElementById("vo2Max_value").value = 63.8 ;
     }
     else if(shuttle == "14-13")
     {
         document.getElementById("vo2Max_value").value = 64.1 ;
     }
     else if(shuttle == "1-5")
     {
         document.getElementById("vo2Max_value").value = 18.8 ;
     }
     else if(shuttle == "15-1")
     {
         document.getElementById("vo2Max_value").value = 64.3 ;
     }
     else if(shuttle == "15-2")
     {
         document.getElementById("vo2Max_value").value = 64.6 ;
     }
     else if(shuttle == "15-3")
     {
         document.getElementById("vo2Max_value").value = 64.8 ;
     }
     else if(shuttle == "15-4")
     {
         document.getElementById("vo2Max_value").value = 65.1 ;
     }
     else if(shuttle == "15-5")
     {
         document.getElementById("vo2Max_value").value = 65.4 ;
     }
     else if(shuttle == "15-6")
     {
         document.getElementById("vo2Max_value").value = 65.6 ;
     }
     else if(shuttle == "15-7")
     {
         document.getElementById("vo2Max_value").value = 65.9 ;
     }
     else if(shuttle == "15-8")
     {
         document.getElementById("vo2Max_value").value = 66.1 ;
     }
     else if(shuttle == "15-9")
     {
         document.getElementById("vo2Max_value").value = 66.4 ;
     }
     else if(shuttle == "15-10")
     {
         document.getElementById("vo2Max_value").value = 66.7 ;
     }
     else if(shuttle == "15-11")
     {
         document.getElementById("vo2Max_value").value = 66.9 ;
     }
     else if(shuttle == "15-12")
     {
         document.getElementById("vo2Max_value").value = 67.2 ;
     }
     else if(shuttle == "15-13")
     {
         document.getElementById("vo2Max_value").value = 67.4 ;
     }
     else if(shuttle == "1-6")
     {
         document.getElementById("vo2Max_value").value = 19.2 ;
     }
     else if(shuttle == "16-1")
     {
         document.getElementById("vo2Max_value").value = 67.7 ;
     }
     else if(shuttle == "16-2")
     {
         document.getElementById("vo2Max_value").value = 67.9 ;
     }
     else if(shuttle == "16-3")
     {
         document.getElementById("vo2Max_value").value = 68.2 ;
     }
     else if(shuttle == "16-4")
     {
         document.getElementById("vo2Max_value").value = 68.5 ;
     }
     else if(shuttle == "16-5")
     {
         document.getElementById("vo2Max_value").value = 68.7 ;
     }
     else if(shuttle == "16-6")
     {
         document.getElementById("vo2Max_value").value = 69.0 ;
     }
     else if(shuttle == "16-7")
     {
         document.getElementById("vo2Max_value").value = 69.2 ;
     }
     else if(shuttle == "16-8")
     {
         document.getElementById("vo2Max_value").value = 69.5 ;
     }
     else if(shuttle == "16-9")
     {
         document.getElementById("vo2Max_value").value = 69.7 ;
     }
     else if(shuttle == "16-10")
     {
         document.getElementById("vo2Max_value").value = 70.0 ;
     }
     else if(shuttle == "16-11")
     {
         document.getElementById("vo2Max_value").value = 70.2 ;
     }
     else if(shuttle == "16-12")
     {
         document.getElementById("vo2Max_value").value = 70.5 ;
     }
     else if(shuttle == "16-13")
     {
         document.getElementById("vo2Max_value").value = 70.7 ;
     }
     else if(shuttle == "1-7")
     {
         document.getElementById("vo2Max_value").value = 19.6 ;
     }
     else if(shuttle == "2-1")
     {
         document.getElementById("vo2Max_value").value = 20.0 ;
     }
     else if(shuttle == "2-2")
     {
         document.getElementById("vo2Max_value").value = 20.5 ;
     }
     else if(shuttle == "2-3")
     {
         document.getElementById("vo2Max_value").value = 20.9 ;
     }
     else if(shuttle == "2-4")
     {
         document.getElementById("vo2Max_value").value = 21.3 ;
     }
     else if(shuttle == "2-5")
     {
         document.getElementById("vo2Max_value").value = 21.7 ;
     }
     else if(shuttle == "2-6")
     {
         document.getElementById("vo2Max_value").value = 22.1 ;
     }
     else if(shuttle == "2-7")
     {
         document.getElementById("vo2Max_value").value = 22.5 ;
     }
     else if(shuttle == "2-8")
     {
         document.getElementById("vo2Max_value").value = 22.9 ;
     }
     else if(shuttle == "3-1")
     {
         document.getElementById("vo2Max_value").value = 23.3 ;
     }
     else if(shuttle == "3-2")
     {
         document.getElementById("vo2Max_value").value = 23.7 ;
     }
     else if(shuttle == "3-3")
     {
         document.getElementById("vo2Max_value").value = 24.1 ;
     }
     else if(shuttle == "3-4")
     {
         document.getElementById("vo2Max_value").value = 24.5 ;
     }
     else if(shuttle == "3-5")
     {
         document.getElementById("vo2Max_value").value = 24.9 ;
     }
     else if(shuttle == "3-6")
     {
         document.getElementById("vo2Max_value").value = 25.3 ;
     }
     else if(shuttle == "3-7")
     {
         document.getElementById("vo2Max_value").value = 25.7 ;
     }
     else if(shuttle == "3-8")
     {
         document.getElementById("vo2Max_value").value = 26.1 ;
     }
     else if(shuttle == "4-1")
     {
         document.getElementById("vo2Max_value").value = 26.5 ;
     }
     else if(shuttle == "4-2")
     {
         document.getElementById("vo2Max_value").value = 26.9 ;
     }
     else if(shuttle == "4-3")
     {
         document.getElementById("vo2Max_value").value = 27.2 ;
     }
     else if(shuttle == "4-4")
     {
         document.getElementById("vo2Max_value").value = 27.6 ;
     }
     else if(shuttle == "4-5")
     {
         document.getElementById("vo2Max_value").value = 28.0 ;
     }
     else if(shuttle == "4-6")
     {
         document.getElementById("vo2Max_value").value = 28.4 ;
     }
     else if(shuttle == "4-7")
     {
         document.getElementById("vo2Max_value").value = 28.8 ;
     }
     else if(shuttle == "4-8")
     {
         document.getElementById("vo2Max_value").value = 29.1 ;
     }
     else if(shuttle == "4-9")
     {
         document.getElementById("vo2Max_value").value = 29.5 ;
     }
     else if(shuttle == "5-1")
     {
         document.getElementById("vo2Max_value").value = 29.9 ;
     }
     else if(shuttle == "5-2")
     {
         document.getElementById("vo2Max_value").value = 30.3 ;
     }
     else if(shuttle == "5-3")
     {
         document.getElementById("vo2Max_value").value = 30.6 ;
     }
     else if(shuttle == "5-4")
     {
         document.getElementById("vo2Max_value").value = 31.0 ;
     }
     else if(shuttle == "5-5")
     {
         document.getElementById("vo2Max_value").value = 31.4 ;
     }
     else if(shuttle == "5-6")
     {
         document.getElementById("vo2Max_value").value = 31.7 ;
     }
     else if(shuttle == "5-7")
     {
         document.getElementById("vo2Max_value").value = 32.1 ;
     }
     else if(shuttle == "5-8")
     {
         document.getElementById("vo2Max_value").value = 32.5 ;
     }
     else if(shuttle == "5-9")
     {
         document.getElementById("vo2Max_value").value = 32.8 ;
     }
     else if(shuttle == "6-1")
     {
         document.getElementById("vo2Max_value").value = 33.2 ;
     }
     else if(shuttle == "6-2")
     {
         document.getElementById("vo2Max_value").value = 33.6 ;
     }
     else if(shuttle == "6-3")
     {
         document.getElementById("vo2Max_value").value = 33.9 ;
     }
     else if(shuttle == "6-4")
     {
         document.getElementById("vo2Max_value").value = 34.3 ;
     }
     else if(shuttle == "6-5")
     {
         document.getElementById("vo2Max_value").value = 34.6 ;
     }
     else if(shuttle == "6-6")
     {
         document.getElementById("vo2Max_value").value = 35.0 ;
     }
     else if(shuttle == "6-7")
     {
         document.getElementById("vo2Max_value").value = 35.3 ;
     }
     else if(shuttle == "6-8")
     {
         document.getElementById("vo2Max_value").value = 35.7 ;
     }
     else if(shuttle == "6-9")
     {
         document.getElementById("vo2Max_value").value = 36.0 ;
     }
     else if(shuttle == "6-10")
     {
         document.getElementById("vo2Max_value").value = 36.4 ;
     }
     else if(shuttle == "7-1")
     {
         document.getElementById("vo2Max_value").value = 36.7 ;
     }
     else if(shuttle == "7-2")
     {
         document.getElementById("vo2Max_value").value = 37.1 ;
     }
     else if(shuttle == "7-3")
     {
         document.getElementById("vo2Max_value").value = 37.4 ;
     }
     else if(shuttle == "7-4")
     {
         document.getElementById("vo2Max_value").value = 37.8 ;
     }
     else if(shuttle == "7-5")
     {
         document.getElementById("vo2Max_value").value = 38.1 ;
     }
     else if(shuttle == "7-6")
     {
         document.getElementById("vo2Max_value").value = 38.4 ;
     }
     else if(shuttle == "7-7")
     {
         document.getElementById("vo2Max_value").value = 38.8 ;
     }
     else if(shuttle == "7-8")
     {
         document.getElementById("vo2Max_value").value = 39.1 ;
     }
     else if(shuttle == "7-9")
     {
         document.getElementById("vo2Max_value").value = 39.4 ;
     }
     else if(shuttle == "7-10")
     {
         document.getElementById("vo2Max_value").value = 39.8 ;
     }
     else if(shuttle == "8-1")
     {
         document.getElementById("vo2Max_value").value = 40.1 ;
     }
     else if(shuttle == "8-2")
     {
         document.getElementById("vo2Max_value").value = 40.4 ;
     }
     else if(shuttle == "8-3")
     {
         document.getElementById("vo2Max_value").value = 40.8 ;
     }
     else if(shuttle == "8-4")
     {
         document.getElementById("vo2Max_value").value = 41.1 ;
     }
     else if(shuttle == "8-5")
     {
         document.getElementById("vo2Max_value").value = 41.4 ;
     }
     else if(shuttle == "8-6")
     {
         document.getElementById("vo2Max_value").value = 41.8 ;
     }
     else if(shuttle == "8-7")
     {
         document.getElementById("vo2Max_value").value = 42.1 ;
     }
     else if(shuttle == "8-8")
     {
         document.getElementById("vo2Max_value").value = 42.4 ;
     }
     else if(shuttle == "8-9")
     {
         document.getElementById("vo2Max_value").value = 42.7 ;
     }
     else if(shuttle == "8-10")
     {
         document.getElementById("vo2Max_value").value = 43.1 ;
     }
     else if(shuttle == "8-11")
     {
         document.getElementById("vo2Max_value").value = 43.4 ;
     }
     else if(shuttle == "9-1")
     {
         document.getElementById("vo2Max_value").value = 43.7 ;
     }
     else if(shuttle == "9-2")
     {
         document.getElementById("vo2Max_value").value = 44.0 ;
     }
     else if(shuttle == "9-3")
     {
         document.getElementById("vo2Max_value").value = 44.3 ;
     }
     else if(shuttle == "9-4")
     {
         document.getElementById("vo2Max_value").value = 44.7 ;
     }
     else if(shuttle == "9-5")
     {
         document.getElementById("vo2Max_value").value = 45.0 ;
     }
     else if(shuttle == "9-6")
     {
         document.getElementById("vo2Max_value").value = 45.3 ;
     }
     else if(shuttle == "9-7")
     {
         document.getElementById("vo2Max_value").value = 45.6 ;
     }
     else if(shuttle == "9-8")
     {
         document.getElementById("vo2Max_value").value = 45.9 ;
     }
     else if(shuttle == "9-9")
     {
         document.getElementById("vo2Max_value").value = 46.2 ;
     }
     else if(shuttle == "9-10")
     {
         document.getElementById("vo2Max_value").value = 46.5 ;
     }
     else if(shuttle == "9-11")
     {
         document.getElementById("vo2Max_value").value = 46.8 ;
     }
     
     getZscore();
  
 }
 
 
  //calculate z-score value
    function getZscore()
    { 
        var ageValue = document.getElementById("age").value;
        var vo2MAxVal = document.getElementById("vo2Max_value");
        var vo2Max = vo2MAxVal.value;
      
        
        var div1=document.getElementById('plot1');
        var div2=document.getElementById('plot2');
        var div3=document.getElementById('plot3');
        var div4=document.getElementById('plot4');
        var div5=document.getElementById('plot5');
        var div6=document.getElementById('plot6');
        var div7=document.getElementById('plot7');
        var div8=document.getElementById('plot8');
        var div9=document.getElementById('plot9');
        var div10=document.getElementById('plot10');
        var div11=document.getElementById('plot11');
        var div12=document.getElementById('plot12');
        var div13=document.getElementById('plot13');
        var div14=document.getElementById('plot14');
        var div15=document.getElementById('plot15');
        var div16_1=document.getElementById('plot16_1');
        var div16_2=document.getElementById('plot16_2');
        var div16_3=document.getElementById('plot16_3');
        var div17=document.getElementById('plot17');
        var div18_8=document.getElementById('plot18_8');
        var div18_7=document.getElementById('plot18_7');
        var div18_6=document.getElementById('plot18_6');
        var div18_5=document.getElementById('plot18_5');
        var div18_4=document.getElementById('plot18_4');
        var div18_3=document.getElementById('plot18_3');
        var div18_2=document.getElementById('plot18_2');
        var div18_1=document.getElementById('plot18_1');
        
        if(div1.style.display == "block")
        {
            div1.style.display = "none"
        }
        else if(div2.style.display == "block")
        {
            div2.style.display = "none"
        }
        else if(div3.style.display == "block")
        {
            div3.style.display = "none"
        }
        else if(div4.style.display == "block")
        {
            div4.style.display = "none"
        }
        else if(div5.style.display == "block")
        {
            div5.style.display = "none"
        }
        else if(div6.style.display == "block")
        {
            div6.style.display = "none"
        }
        else if(div7.style.display == "block")
        {
            div7.style.display = "none"
        }
        else if(div8.style.display == "block")
        {
            div8.style.display = "none"
        }
        else if(div9.style.display == "block")
        {
            div9.style.display = "none"
        }
        else if(div10.style.display == "block")
        {
            div10.style.display = "none"
        }
        else if(div11.style.display == "block")
        {
            div11.style.display = "none"
        }
        else if(div12.style.display == "block")
        {
            div12.style.display = "none"
        }
        else if(div13.style.display == "block")
        {
            div13.style.display = "none"
        }
        else if(div14.style.display == "block")
        {
            div14.style.display = "none"
        }
        else if(div15.style.display == "block")
        {
            div15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        } 
        
      
        if(document.getElementById('gender').value == "Male") {
                
         if(ageValue == "18-29")
        {
            var mean = 44.7 ;
            var sd = 8.9 ;                   
            
        }else if(ageValue == "30-39") {
        
           var mean = 39.4 ;
           var sd = 8.55 ;
        
       }else if(ageValue == "40-49") {
        
           var mean = 37.3 ;
           var sd = 8.04 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 32.95 ;
           var sd = 7.5 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 30.6 ;
           var sd = 6.73 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 29.5 ;
           var sd = 6.31 ;        
      }
        
    }else if(document.getElementById('gender').value == "Female") {
              
        if(ageValue == "18-29")
        {
            var mean = 36 ;
            var sd = 9.5 ;
            
        }else if(ageValue == "30-39") {
        
           var mean = 31.6 ;
           var sd = 9.05 ;
        
      }else if(ageValue == "40-49") {
        
           var mean = 30.5 ;
           var sd = 8.2 ;
        
      }else if(ageValue == "50-59") {
        
           var mean = 29.6 ;
           var sd = 7.35 ;
        
      }else if(ageValue == "60-69") {
        
           var mean = 25.7 ;
           var sd = 7.05 ;
        
      }else if(ageValue == "70+") {
        
           var mean = 23.1 ;
           var sd = 6.5 ;        
      }               
        
    }
          
      var z_score = (parseFloat(vo2Max) - parseFloat(mean)) / parseFloat(sd) ;    
      if(isNaN(z_score)){            
            document.getElementById("percentile").value = "" ;  
        }
        else
        {
           z_score = Math.floor(parseFloat(z_score) * 1000) / 1000 ;
            
           // NORMDIST for z-score 
           var perform_percent = normalcdf(z_score);
           
            if(isNaN(parseFloat(perform_percent * 100)))
            {              
               document.getElementById("percentile").value = "" ;
             }
             else
             {                
                 document.getElementById("percentile").value = Math.floor(parseFloat(perform_percent * 100) * 1000000) / 1000000 ;                   
                 
             } 
           
           if(document.getElementById("percentile").value < 48)
           {  
             if(document.getElementById("percentile").value >= 0 && document.getElementById("percentile").value < 7)
                {              
                   var scorePlot = '1 in 1_1' ;
                }
             else if(document.getElementById("percentile").value >= 7 && document.getElementById("percentile").value < 14)
                {              
                   var scorePlot = '1 in 1_2' ;
                }  
             else if(document.getElementById("percentile").value >= 14 && document.getElementById("percentile").value < 20)
                {              
                   var scorePlot = '1 in 1_3' ;
                }  
             else if(document.getElementById("percentile").value >= 20 && document.getElementById("percentile").value < 30)
                {              
                   var scorePlot = '1 in 1_4' ;
                }  
             else if(document.getElementById("percentile").value >= 30 && document.getElementById("percentile").value < 35)
                {              
                   var scorePlot = '1 in 1_5' ;
                }  
             else if(document.getElementById("percentile").value >= 35 && document.getElementById("percentile").value < 40)
                {              
                   var scorePlot = '1 in 1_6' ;
                }  
             else if(document.getElementById("percentile").value >= 40 && document.getElementById("percentile").value < 44)
                {              
                   var scorePlot = '1 in 1_7' ;
                }  
             else if(document.getElementById("percentile").value >= 44 && document.getElementById("percentile").value < 48 )
                {              
                   var scorePlot = '1 in 1_8' ;
                }                
           }
           else if(document.getElementById("percentile").value >= 48 && document.getElementById("percentile").value < 52)
           {              
              var scorePlot = '1 in 2' ;
           }          
           else if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 80)
           {              
               if(document.getElementById("percentile").value >= 52 && document.getElementById("percentile").value <= 60)
               {
                 var scorePlot = '1 in 3_1' ;   
               }
               else if(document.getElementById("percentile").value > 60 && document.getElementById("percentile").value <= 70)
               {
                 var scorePlot = '1 in 3_2' ;   
               }
               else if(document.getElementById("percentile").value > 70 && document.getElementById("percentile").value <= 80)
               {
                 var scorePlot = '1 in 3_3' ;   
               }
                
           }  
           else if(z_score > 0.8100 && z_score <= 0.8420)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 0.8420 && z_score <= 1.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.0100 && z_score <= 1.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.1100 && z_score <= 1.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5' ;
           }
           else if(z_score > 1.2100 && z_score <= 1.2816)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.2816 && z_score <= 1.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.4100 && z_score <= 1.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.5100 && z_score <= 1.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10' ;
           }
           else if(z_score > 1.6100 && z_score <= 1.6600)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.6600 && z_score <= 1.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.8100 && z_score <= 1.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 20' ;
           }
           else if(z_score > 1.9100 && z_score <= 2.0550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.0550 && z_score <= 2.1100)
           {
              var perform_percent = 0.98257082 ;
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.1100 && z_score <= 2.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50' ;
           }
           else if(z_score > 2.2100 && z_score <= 2.3270)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.3270 && z_score <= 2.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.4100 && z_score <= 2.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100' ;
           }
           else if(z_score > 2.5100 && z_score <= 2.5760)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.5760 && z_score <= 2.7100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.7100 && z_score <= 2.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 200' ;
           }
           else if(z_score > 2.8100 && z_score <= 2.8785)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 2.8785 && z_score <= 3.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 500' ;
           }
           else if(z_score > 3.0100 && z_score <= 3.0900)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.0900 && z_score <= 3.2100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1000' ;
           }
           else if(z_score > 3.2100 && z_score <= 3.3550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.3550 && z_score <= 3.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 2,500' ;
           }
           else if(z_score > 3.4100 && z_score <= 3.5410)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.5410 && z_score <= 3.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 5,000' ;
           }
           else if(z_score > 3.6100 && z_score <= 3.7200)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.7200 && z_score <= 3.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.8100 && z_score <= 3.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 3.9100 && z_score <= 4.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000' ;
           }
           else if(z_score > 4.0100 && z_score <= 4.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 50,000' ;
           }
           else if(z_score > 4.1100 && z_score <= 4.2650)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.2650 && z_score <= 4.3100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.3100 && z_score <= 4.4100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.4100 && z_score <= 4.5100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.5100 && z_score <= 4.6100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 100,000' ;
           }
           else if(z_score > 4.6100 && z_score <= 4.7550)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.7550 && z_score <= 4.8100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.8100 && z_score <= 4.9100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 4.9100 && z_score <= 5.0100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.0100 && z_score <= 5.1100)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 1,000,000' ;
           }
           else if(z_score > 5.1100 && z_score <= 5.2000)
           {
              var perform_percent = normalcdf(z_score);	
              var scorePlot = '1 in 10,000,000' ;
           }
           else if(z_score > 5.2000)
           {
              var perform_percent = normalcdf(z_score);	                      
              var scorePlot = '1 in 10,000,000' ;
           }
           
           document.getElementById("scorePlot").value = scorePlot ;            
            
        }     
       
       changeSportMean() ;
        
    }
    
    function normalcdf(x){ 
        //HASTINGS.  MAX ERROR = .000001
	var t = 1/(1 + 0.2316419 * Math.abs(x));
	var d = 0.3989423 * Math.exp(-x * x / 2);
	var Prob = d * t * (0.3193815 + t * (-0.3565638 + t * (1.781478 + t * (-1.821256 + t * 1.330274))));
	if (x > 0) {
		Prob = 1 - Prob ;
	}
        
       // alert(Prob); die;
	return Prob ;
    }   
    
 
     
    function changeSportMean()
    {   
        var sportValue = document.getElementById("sport").value ;           
        
        var testmean1 = document.getElementById('class1').innerHTML.replace('~', '')   ;
        var testmean2 = document.getElementById('class2').innerHTML.replace('~', '')   ;
        var testmean3 = document.getElementById('class3').innerHTML.replace('~', '')   ;
        var testmean4 = document.getElementById('class4').innerHTML.replace('~', '')   ; 
        var testmean5 = document.getElementById('class5').innerHTML.replace('~', '')   ;
        var testmean6 = document.getElementById('class6').innerHTML.replace('~', '')   ;
        var testmean7 = document.getElementById('class7').innerHTML.replace('~', '')   ;
        var testmean8 = document.getElementById('class8').innerHTML.replace('~', '')   ;
        var testmean9 = document.getElementById('class9').innerHTML.replace('~', '')   ;
        var testmean10 = document.getElementById('class10').innerHTML.replace('~', '')  ;
        var testmean11 = document.getElementById('class11').innerHTML.replace('~', '')  ;
        var testmean12 = document.getElementById('class12').innerHTML.replace('~', '')  ;
        var testmean13 = document.getElementById('class13').innerHTML.replace('~', '')  ;
        var testmean14 = document.getElementById('class14').innerHTML.replace('~', '')  ;
        var testmean15 = document.getElementById('class15').innerHTML.replace('~', '')  ;        
        var testmean = document.getElementById('meanVal').innerHTML   ;
        
       
        var divsport1=document.getElementById('score1');
        var divsport2=document.getElementById('score2');
        var divsport3=document.getElementById('score3');
        var divsport4=document.getElementById('score4');
        var divsport5=document.getElementById('score5');
        var divsport6=document.getElementById('score6');
        var divsport7=document.getElementById('score7');
        var divsport8=document.getElementById('score8');
        var divsport9=document.getElementById('score9');
        var divsport10=document.getElementById('score10');
        var divsport11=document.getElementById('score11');
        var divsport12=document.getElementById('score12');
        var divsport13=document.getElementById('score13');
        var divsport14=document.getElementById('score14');
        var divsport15=document.getElementById('score15');
        var div16_1=document.getElementById('score16_1');
        var div16_2=document.getElementById('score16_2');
        var div16_3=document.getElementById('score16_3');
        var div17=document.getElementById('score17');
        var div18_8=document.getElementById('score18_8');
        var div18_7=document.getElementById('score18_7');
        var div18_6=document.getElementById('score18_6');
        var div18_5=document.getElementById('score18_5');
        var div18_4=document.getElementById('score18_4');
        var div18_3=document.getElementById('score18_3');
        var div18_2=document.getElementById('score18_2');
        var div18_1=document.getElementById('score18_1');
       
        
      if(sportValue != "") 
       {
        if(divsport1.style.display == "block")
        {
            divsport1.style.display = "none"
        }
        else if(divsport2.style.display == "block")
        {
            divsport2.style.display = "none"
        }
        else if(divsport3.style.display == "block")
        {
            divsport3.style.display = "none"
        }
        else if(divsport4.style.display == "block")
        {
            divsport4.style.display = "none"
        }
        else if(divsport5.style.display == "block")
        {
            divsport5.style.display = "none"
        }
        else if(divsport6.style.display == "block")
        {
            divsport6.style.display = "none"
        }
        else if(divsport7.style.display == "block")
        {
            divsport7.style.display = "none"
        }
        else if(divsport8.style.display == "block")
        {
            divsport8.style.display = "none"
        }
        else if(divsport9.style.display == "block")
        {
            divsport9.style.display = "none"
        }
        else if(divsport10.style.display == "block")
        {
            divsport10.style.display = "none"
        }
        else if(divsport11.style.display == "block")
        {
            divsport11.style.display = "none"
        }
        else if(divsport12.style.display == "block")
        {
            divsport12.style.display = "none"
        }
        else if(divsport13.style.display == "block")
        {
            divsport13.style.display = "none"
        }
        else if(divsport14.style.display == "block")
        {
            divsport14.style.display = "none"
        }
        else if(divsport15.style.display == "block")
        {
           divsport15.style.display = "none"
        } 
        else if(div16_1.style.display == "block")
        {
            div16_1.style.display = "none"
        } 
         else if(div16_2.style.display == "block")
        {
            div16_2.style.display = "none"
        }
         else if(div16_3.style.display == "block")
        {
            div16_3.style.display = "none"
        }
        else if(div17.style.display == "block")
        {
            div17.style.display = "none"
        } 
        else if(div18_8.style.display == "block")
        {
            div18_8.style.display = "none"
        } 
        else if(div18_7.style.display == "block")
        {
            div18_7.style.display = "none"
        } 
        else if(div18_6.style.display == "block")
        {
            div18_6.style.display = "none"
        } 
        else if(div18_5.style.display == "block")
        {
            div18_5.style.display = "none"
        } 
        else if(div18_4.style.display == "block")
        {
            div18_4.style.display = "none"
        } 
        else if(div18_3.style.display == "block")
        {
            div18_3.style.display = "none"
        } 
        else if(div18_2.style.display == "block")
        {
            div18_2.style.display = "none"
        } 
        else if(div18_1.style.display == "block")
        {
            div18_1.style.display = "none"
        } 
    }
        
        
   // Male     
 /*      if(document.getElementById('gender').value == "Male") {
                
        if(sportValue == "archery")
        {
            var mean = 49 ;
            var sd = 4.1 ;                  
            
        }else if(sportValue == "Australian football [AFL midfield]") {
        
           var mean = 64.1 ;
           var sd = 3.2 ;   
           
      }else if(sportValue == "Australian football [AFL]") {
        
           var mean = 59.6 ;
           var sd = 3.1 ;   
           
      }else if(sportValue == "badminton") {
        
           var mean = 60.8 ;
           var sd = 4.2 ;  
           
      }else if(sportValue == "baseball") {
        
           var mean = 48 ;
           var sd = 4.9 ;        
           
      }else if(sportValue == "basketball") {
        
           var mean = 58 ;
           var sd = 3.8 ;    
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 47 ;
           var sd = 4.2 ;     
           
      }else if(sportValue == "boxing - general") {
        
           var mean = 71 ;
           var sd = 3.1 ;
           
      }else if(sportValue == "boxing - heavyweight") {
        
           var mean = 65 ;
           var sd = 2.1 ;   
           
      }else if(sportValue == "canoe polo") {
        
           var mean = 61 ;
           var sd = 2.8 ; 
           
      }else if(sportValue == "canoeing (Canadian)") {
        
           var mean = 72 ;
           var sd = 1.7 ; 
           
      }else if(sportValue == "cricket") {
        
           var mean = 54.8 ;
           var sd = 4 ;   
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 76.3 ;
           var sd = 2.4 ;     
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 81.6 ;
           var sd = 2.6 ;     
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 56 ;
           var sd = 4.2 ;   
           
      }else if(sportValue == "decathlon") {
        
           var mean = 68 ;
           var sd = 4.5 ;  
           
      }else if(sportValue == "discus") {
        
           var mean = 40.6 ;
           var sd = 4.8 ;    
           
      }else if(sportValue == "diving") {
        
           var mean = 45 ;
           var sd = 5 ;       
           
      }else if(sportValue == "fencing") {
        
           var mean = 52.5 ;
           var sd = 5.4 ;      
           
      }else if(sportValue == "golf") {
        
           var mean = 50 ;
           var sd = 4.2 ;        
      }else if(sportValue == "gymnastics") {
        
           var mean = 52.5 ;
           var sd = 4.5 ; 
            
      }else if(sportValue == "handball") {
        
           var mean = 70.2 ;
           var sd = 3.8 ;     
           
      }else if(sportValue == "high jump") {
        
           var mean = 48 ;
           var sd = 5 ;        
            
      }else if(sportValue == "hockey (field)") {
        
           var mean = 65 ;
           var sd = 4.4 ; 
           
      }else if(sportValue == "hockey (ice)") {
        
           var mean = 65.1 ;
           var sd = 3.2 ; 
           
      }else if(sportValue == "hurdles") {
        
           var mean = 68.9 ;
           var sd = 2.7 ; 
           
      }else if(sportValue == "javelin") {
        
           var mean = 48.5 ;
           var sd = 5.6 ; 
           
      }else if(sportValue == "jockey") {
        
           var mean = 52.6 ;
           var sd = 5 ;    
           
      }else if(sportValue == "judo") {
        
           var mean = 59.5 ;
           var sd = 5.5 ;
           
      }else if(sportValue == "karate") {
        
           var mean = 59.2 ;
           var sd = 5.4 ; 
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 67 ;
           var sd = 4.1 ; 
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 69.8   ;
           var sd = 3.8 ;  
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 67.4 ;
           var sd = 3.6 ;     
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 57 ;
           var sd = 4.2 ;        
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 67.1 ;
           var sd = 3.2 ;       
           
      }else if(sportValue == "long jump") {
        
           var mean = 64.5 ;
           var sd = 3.8 ;    
           
      }else if(sportValue == "orienteering") {
        
           var mean = 76 ;
           var sd = 3.9 ;
           
      }else if(sportValue == "powerlifting") {
        
           var mean = 40.6 ;
           var sd = 5.1 ;       
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 53.7 ;
           var sd = 4.8 ;  
           
      }else if(sportValue == "rollerskating") {
        
           var mean = 62.6 ;
           var sd = 4.7 ;  
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 68.9 ;
           var sd = 2.9 ;  
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 75.9 ;
           var sd = 3.9 ;  
           
      }else if(sportValue == "rugby League - backs") {
        
           var mean = 62 ;
           var sd = 4.9 ;  
           
      }else if(sportValue == "rugby League - forwards") {
        
           var mean = 60 ;
           var sd = 3.8 ;      
           
      }else if(sportValue == "rugby union") {
        
           var mean = 51.6 ;
           var sd = 4.8 ;  
           
      }else if(sportValue == "running - distance") {
        
           var mean = 79.2 ;
           var sd = 4 ;  
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 82.5 ;
           var sd = 2.5 ; 
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 66.4 ;
           var sd = 5 ;    
           
      }else if(sportValue == "sailing") {
        
           var mean = 40.5 ;
           var sd = 5.7 ;   
           
      }else if(sportValue == "shooting") {
        
           var mean = 52.5 ;
           var sd = 3.1 ;  
           
      }else if(sportValue == "shot put") {
        
           var mean = 35.6 ;
           var sd = 5.4 ;     
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 52.6 ;
           var sd = 2.7 ;  
            
      }else if(sportValue == "soccer") {
        
           var mean = 65.5 ;
           var sd = 4.7 ;   
           
      }else if(sportValue == "squash") {
        
           var mean = 69 ;
           var sd = 4.1 ; 
           
      }else if(sportValue == "sumo wrestling") {
        
           var mean = 28.9   ;
           var sd = 4.9 ;   
           
      }else if(sportValue == "surfing") {
        
           var mean = 50.7 ;
           var sd = 5 ;    
           
      }else if(sportValue == "swimming") {
        
           var mean = 70.4 ;
           var sd = 4.9 ;  
           
      }else if(sportValue == "table tennis") {
        
           var mean = 54.5 ;
           var sd = 6.9 ; 
           
      }else if(sportValue == "tennis") {
        
           var mean = 64.1 ;
           var sd = 6 ;    
           
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 59 ;
           var sd = 5.1 ;   
           
      }else if(sportValue == "triathlon") {
        
           var mean = 79.9 ;
           var sd = 3.1 ;        
           
      }else if(sportValue == "volleyball") {
        
           var mean = 54 ;
           var sd = 5 ;     
           
      }else if(sportValue == "walking") {
        
           var mean = 68 ;
           var sd = 4.4 ;   
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 65.7 ;
           var sd = 4.9 ;   
           
      }else if(sportValue == "weightlifting") {
        
           var mean = 35.4 ;
           var sd = 4.8 ;        
      }else if(sportValue == "wrestling") {
        
          var mean = 49.9 ;
          var sd = 4.9 ;          
      }
             
    }
    
  // Female      
    else if(document.getElementById('gender').value == "Female") {
              
        if(sportValue == "archery")
        {
            var mean = 43 ;
            var sd = 2.75 ;                  
            
        }else if(sportValue == "badminton") {
        
           var mean = 55.4 ;
           var sd = 2.9 ;      
           
      }else if(sportValue == "basketball") {
        
           var mean = 50.3 ;
           var sd = 2.5 ;       
           
      }else if(sportValue == "bodybuilding") {
        
           var mean = 43 ;
           var sd = 2.3 ;  
           
      }else if(sportValue == "cricket") {
        
           var mean = 48.7 ;
           var sd = 2 ;   
           
      }else if(sportValue == "cycling - mountain bike") {
        
           var mean = 66.8 ;
           var sd = 2.35 ;     
           
      }else if(sportValue == "cycling - road") {
        
           var mean = 70 ;
           var sd = 2.45 ; 
           
      }else if(sportValue == "cycling - track sprint") {
        
           var mean = 51 ;
           var sd = 3.1 ; 
           
      }else if(sportValue == "discus") {
        
           var mean = 38 ;
           var sd = 2.1 ;   
           
      }else if(sportValue == "diving") {
        
           var mean = 43 ;
           var sd = 1.9 ;        
           
      }else if(sportValue == "fencing") {
        
           var mean = 44.9 ;
           var sd = 2.5 ; 
           
      }else if(sportValue == "golf") {
        
           var mean = 44.5 ;
           var sd = 2 ;    
           
      }else if(sportValue == "gymnastics") {
        
           var mean = 43.8 ;
           var sd = 2.6 ;  
           
      }else if(sportValue == "handball") {
        
           var mean = 55.6 ;
           var sd = 2.35 ;     
           
      }else if(sportValue == "heptathlon") {
        
           var mean = 60 ;
           var sd = 2.8 ;        
           
      }else if(sportValue == "high jump") {
        
           var mean = 46 ;
           var sd = 2.7 ;  
           
      }else if(sportValue == "hockey (field)") {
        
           var mean = 51 ;
           var sd = 2.6 ;        
           
      }else if(sportValue == "hurdles") {
        
           var mean = 61 ;
           var sd = 2 ;     
           
      }else if(sportValue == "javelin") {
        
           var mean = 44 ;
           var sd = 3 ;  
           
      }else if(sportValue == "judo") {
        
           var mean = 49.8 ;
           var sd = 2.4 ;       
           
      }else if(sportValue == "karate") {
        
           var mean = 46.2 ;
           var sd = 2.4 ; 
           
      }else if(sportValue == "kayak - general") {
        
           var mean = 53 ;
           var sd = 3.1 ;  
           
      }else if(sportValue == "kayak - marathon") {
        
           var mean = 53.2 ;
           var sd = 3.4 ;     
           
      }else if(sportValue == "kayak - slalom") {
        
           var mean = 50.1 ;
           var sd = 2.4 ;
           
      }else if(sportValue == "kayak- sprint") {
        
           var mean = 54.2 ;
           var sd = 2.7 ;  
           
      }else if(sportValue == "lacrosse") {
        
           var mean = 52.3 ;
           var sd = 3.4 ;       
           
      }else if(sportValue == "long jump") {
        
           var mean = 44 ;
           var sd = 2.5 ; 
           
      }else if(sportValue == "orienteering") {
        
           var mean = 64 ;
           var sd = 3.4 ;
           
      }else if(sportValue == "netball") {
        
           var mean = 55.1 ;
           var sd = 2.4 ;      
           
      }else if(sportValue == "rockclimbing") {
        
           var mean = 43 ;
           var sd = 5 ;        
      }else if(sportValue == "rollerskating") {
        
           var mean = 47.8 ;
           var sd = 2.2 ;  
           
      }else if(sportValue == "rowing - heavyweight") {
        
           var mean = 52.5 ;
           var sd = 1.55 ; 
           
      }else if(sportValue == "rowing - lightweight") {
        
           var mean = 58 ;
           var sd = 2.9 ;     
           
      }else if(sportValue == "rugby union") {
        
           var mean = 42.6 ;
           var sd = 3.3 ;        
           
      }else if(sportValue == "running - distance") {
        
           var mean = 73.5 ;
           var sd = 2.15 ;   
           
      }else if(sportValue == "running - middle distance") {
        
           var mean = 75.1 ;
           var sd = 2 ;     
           
      }else if(sportValue == "running - sprint") {
        
           var mean = 56 ;
           var sd = 2.3 ;  
           
      }else if(sportValue == "sailing") {
        
           var mean = 39.7 ;
           var sd = 2.4 ;        
      }else if(sportValue == "shooting") {
        
           var mean = 44 ;
           var sd = 2.5 ; 
           
      }else if(sportValue == "shot put") {
        
           var mean = 33.1 ;
           var sd = 3 ;   
           
      }else if(sportValue == "skating - figure") {
        
           var mean = 50.4 ;
           var sd = 1.75 ;
           
      }else if(sportValue == "soccer") {
        
           var mean = 50.4 ;
           var sd = 2.5 ;   
           
      }else if(sportValue == "softball") {
        
           var mean = 45 ;
           var sd = 3 ;    
           
      }else if(sportValue == "squash") {
        
           var mean = 56 ;
           var sd = 2.6 ;  
           
      }else if(sportValue == "surfing") {
        
           var mean = 45.6 ;
           var sd = 1 ;     
           
      }else if(sportValue == "swimming") {
        
           var mean = 60.5 ;
           var sd = 3.5 ;  
           
      }else if(sportValue == "synchronised swimming") {
        
           var mean = 50 ;
           var sd = 3 ;  
           
      }else if(sportValue == "table tennis") {
        
           var mean = 46.1 ;
           var sd = 3.4 ;  
           
      }else if(sportValue == "tennis") {
        
           var mean = 55.2 ;
           var sd = 3.2 ;        
      }else if(sportValue == "ten-pin bowling") {
        
           var mean = 41 ;
           var sd = 2.5 ;        
      }else if(sportValue == "triathlon") {
        
           var mean = 66 ;
           var sd = 3.4 ;  
           
      }else if(sportValue == "volleyball") {
        
           var mean = 49 ;
           var sd = 3 ;   
           
      }else if(sportValue == "walking") {
        
           var mean = 64.7 ;
           var sd = 2.9 ;   
           
      }else if(sportValue == "waterpolo") {
        
           var mean = 53.2 ;
           var sd = 3.4 ;        
      }             
        
    }
      */
	  
	  
	  
	  
	  	 	        //Male
   if(document.getElementById('gender').value == "Male") {
		var gender=	document.getElementById('gender').value;
		var meanget=undefined;
		var sdget=undefined;
		var mean=undefined;
		var sd=undefined;
		 $.ajax({
			type: "POST",
			url: '<?php echo base_url(); ?>index.php/Fitness/get_sports_fitness_norms',
			data:{'sportValue': sportValue,'gender' : gender },
			 dataType: 'json',
			success: function(data){
               console.log(data);
              meanget=data.endurance_mean;
			 sdget=data.endurance_sd;         
				
				 },
				 async: false // <- this turns it into synchronous 
			});	
	 if(meanget)
		{
		var mean=meanget;	
		}
		if(sdget)
		{
		var sd=sdget;  	
		}
	}
    
  // Female      

    else if(document.getElementById('gender').value == "Female") {
              
       var gender=	document.getElementById('gender').value;
		var meanget=undefined;
		var sdget=undefined;
		var mean=undefined;
		var sd=undefined;
		 $.ajax({
			type: "POST",
			url: '<?php echo base_url(); ?>index.php/Fitness/get_sports_fitness_norms',
			data:{'sportValue': sportValue,'gender' : gender },
			 dataType: 'json',
			success: function(data){
               console.log(data);
             meanget=data.endurance_mean;
			 sdget=data.endurance_sd;      
				 },
				 async: false // <- this turns it into synchronous 
			});	
        if(meanget)
		{
		var mean=meanget;	
		}
		if(sdget)
		{
		var sd=sdget;  	
		}
		
    }  
        console.log(mean);
	  
	  
	  
	  
          if(mean < testmean)
           {  
             if(mean < (parseFloat(testmean) / 2))
             {
              if(mean >= 0 && mean < ((parseFloat(testmean) / 2) - 6))
                {              
                   var sportPlot = '1 in 1_1' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) - 6) && mean < ((parseFloat(testmean) / 2) - 4))
                {              
                   var sportPlot = '1 in 1_2' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 4) && mean < ((parseFloat(testmean) / 2) - 2))
                {              
                   var sportPlot = '1 in 1_3' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) - 2) && mean < (parseFloat(testmean) / 2))
                {              
                   var sportPlot = '1 in 1_4' ;
                }
             }
             else{
                 
             if(mean >= (parseFloat(testmean) / 2) && mean < ((parseFloat(testmean) / 2) + 2))
                {              
                   var sportPlot = '1 in 1_5' ;
                }
             else if(mean >= ((parseFloat(testmean) / 2) + 2) && mean < ((parseFloat(testmean) / 2) + 4))
                {              
                  var sportPlot = '1 in 1_6' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 4) && mean < ((parseFloat(testmean) / 2) + 6))
                {              
                  var sportPlot = '1 in 1_7' ;
                }  
             else if(mean >= ((parseFloat(testmean) / 2) + 6) && mean < testmean)
                {              
                   var sportPlot = '1 in 1_8' ;
                }        
             }
           }
           
           else if(mean >= testmean && mean < (parseFloat(testmean) + 1))
           {              
              var sportPlot = '1 in 2' ;
           }  
      
          else if(mean >= (parseFloat(testmean) + 1) && mean < testmean15)
           {           
               if(mean >= (parseFloat(testmean) + 1) && mean < (parseFloat(testmean) + 2))
               {
                 var sportPlot = '1 in 3_1' ;   
               }
               else if(mean >= (parseFloat(testmean) + 2) && mean < (parseFloat(testmean) + 3))
               {
                 var sportPlot = '1 in 3_2' ;   
               }
               else if(mean >= (parseFloat(testmean) + 3) && mean < testmean15)
               { 
                 var sportPlot = '1 in 3_3' ;   
               }
                
           }       
        
          else if(mean >= testmean15 && mean < testmean14)
           {              
              var sportPlot = '1 in 5' ;
           }           
           else if(mean >= testmean14 && mean < testmean13)
           {             
              var sportPlot = '1 in 10' ;
           }           
           else if(mean >= testmean13 && mean < testmean12)
           {             
              var sportPlot = '1 in 20' ;
           }          
           else if(mean >= testmean12 && mean < testmean11)
           {              
              var sportPlot = '1 in 50' ;
           }          
           else if(mean >= testmean11 && mean < testmean10)
           {              
              var sportPlot = '1 in 100' ;
           }          
           else if(mean >= testmean10 && mean < testmean9)
           {             	
              var sportPlot = '1 in 200' ;
           }          
           else if(mean >= testmean9 && mean < testmean8)
           {              
              var sportPlot = '1 in 500' ;
           }           
           else if(mean >= testmean8 && mean < testmean7)
           {              
              var sportPlot = '1 in 1000' ;
           }          
           else if(mean >= testmean7 && mean < testmean6)
           {              
              var sportPlot = '1 in 2,500' ;
           }          
           else if(mean >= testmean6 && mean < testmean5)
           {              
              var sportPlot = '1 in 5,000' ;
           }        
           else if(mean >= testmean5 && mean < testmean4)
           {              	
              var sportPlot = '1 in 10,000' ;
           }           
           else if(mean >= testmean4 && mean < testmean3)
           {             	
              var sportPlot = '1 in 50,000' ;
           }
           else if(mean >= testmean3 && mean < testmean2)
           {              	
              var sportPlot = '1 in 100,000' ;
           }           
           else if(mean >= testmean2 && mean < testmean1)
           {              
              var sportPlot = '1 in 1,000,000' ;
           }           
           else if(mean >= testmean1)
           {              
              var sportPlot = '1 in 10,000,000' ;
           }           
                               
           document.getElementById("sportPlot").value = sportPlot ;    
           
           document.getElementById("sportMeanScore").value = mean ; // store mean value of sport selected
           
           
  }
 
 
 
    function show()
    {    
        var score = document.getElementById("scorePlot").value ;    
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
      else
      { 
        if(score == '1 in 1_1')
        {
           var div=document.getElementById('plot18_1');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_2')
        {
           var div=document.getElementById('plot18_2');
           div.style.display = "block" ;
            
        } 
        else if(score == '1 in 1_3')
        {
           var div=document.getElementById('plot18_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_4')
        {
           var div=document.getElementById('plot18_4');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_5')
        {
           var div=document.getElementById('plot18_5');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_6')
        {
           var div=document.getElementById('plot18_6');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_7')
        {
           var div=document.getElementById('plot18_7');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 1_8')
        {
           var div=document.getElementById('plot18_8');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 2')
        {
           var div=document.getElementById('plot17');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_1')
        {
           var div=document.getElementById('plot16_1');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_2')
        {
           var div=document.getElementById('plot16_2');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 3_3')
        {
           var div=document.getElementById('plot16_3');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 5')
        {
           var div=document.getElementById('plot15');
           div.style.display = "block" ;
            
        }
        else if(score == '1 in 10') {
        
           var div=document.getElementById('plot14');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 20') {
        
          var div=document.getElementById('plot13');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 50') {
        
           var div=document.getElementById('plot12');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 100') {
        
           var div=document.getElementById('plot11');
           div.style.display = "block" ;
        
      }
      else if(score == '1 in 200') {
        
           var div=document.getElementById('plot10');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 500') {
        
           var div=document.getElementById('plot9');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1000') {
        
           var div=document.getElementById('plot8');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 2,500') {
        
           var div=document.getElementById('plot7');
           div.style.display = "block" ; 
            
      }
      else if(score == '1 in 5,000') {
        
           var div=document.getElementById('plot6');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000') {
        
           var div=document.getElementById('plot5');
           div.style.display = "block" ;      
           
      }
      else if(score == '1 in 50,000') {
        
           var div=document.getElementById('plot4');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 100,000') {
        
           var div=document.getElementById('plot3');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 1,000,000') {
        
           var div=document.getElementById('plot2');
           div.style.display = "block" ;
           
      }
      else if(score == '1 in 10,000,000') {
        
          var div=document.getElementById('plot1');
          div.style.display = "block" ;     
      }
      
    }
      
      showSport() ;

  }
  
  function showSport()
    {       
        var sport = document.getElementById("sportPlot").value ; 
        var sportMean = document.getElementById('sportMeanScore').value ;
        
         if (document.getElementById("vo2Max_value").value == "" || document.getElementById("vo2Max_value").value == undefined)
           {
               alert ("No values have been entered");
               return false;
           }
           
      else
      { 
          
        if(sport == '1 in 1_1')
        {
           var div=document.getElementById('score18_1');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_2')
        {
           var div=document.getElementById('score18_2');
           div.style.display = "block" ;
            
        } 
        else if(sport == '1 in 1_3')
        {
           var div=document.getElementById('score18_3');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_4')
        {
           var div=document.getElementById('score18_4');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_5')
        {
           var div=document.getElementById('score18_5');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_6')
        {
           var div=document.getElementById('score18_6');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_7')
        {
           var div=document.getElementById('score18_7');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 1_8')
        {
           var div=document.getElementById('score18_8');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 2')
        {
           var div=document.getElementById('score17');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_1')
        {
           var div=document.getElementById('score16_1');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_2')
        {
           var div=document.getElementById('score16_2');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 3_3')
        {
           var div=document.getElementById('score16_3');
           div.style.display = "block" ;
            
        }   
       
       else if(sport == '1 in 5')
        {
           var div=document.getElementById('score15');
           div.style.display = "block" ;
            
        }
        else if(sport == '1 in 10') {
        
           var div=document.getElementById('score14');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 20') {
        
          var div=document.getElementById('score13');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 50') {
        
           var div=document.getElementById('score12');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 100') {
        
           var div=document.getElementById('score11');
           div.style.display = "block" ;
        
      }
      else if(sport == '1 in 200') {
        
           var div=document.getElementById('score10');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 500') {
        
           var div=document.getElementById('score9');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1000') {
        
           var div=document.getElementById('score8');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 2,500') {
        
           var div=document.getElementById('score7');
           div.style.display = "block" ; 
            
      }
      else if(sport == '1 in 5,000') {
        
           var div=document.getElementById('score6');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000') {
        
           var div=document.getElementById('score5');
           div.style.display = "block" ;      
           
      }
      else if(sport == '1 in 50,000') {
        
           var div=document.getElementById('score4');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 100,000') {
        
           var div=document.getElementById('score3');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 1,000,000') {
        
           var div=document.getElementById('score2');
           div.style.display = "block" ;
           
      }
      else if(sport == '1 in 10,000,000') {
        
          var div=document.getElementById('score1');
          div.style.display = "block" ;     
      }
      
    }
      
       if(sportMean == 'undefined')
      {   
        document.getElementById('sportMeanVal').innerHTML = "" ;
      }
      else
      {
          document.getElementById('sportMeanVal').innerHTML = "["+sportMean+"]" ;
      }
      

  }
  
</script>  
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="https://raw.githubusercontent.com/furf/jquery-ui-touch-punch/master/jquery.ui.touch-punch.min.js"></script>
   <script>
  $( function() {
    $( ".v_person" ).draggable();
  } );
  </script>
</body>
</html>
