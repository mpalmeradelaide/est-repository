<?php
$gender=$_SESSION['user_gender'];
//print_r($fieldData);
 
 ?> 
<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Medication</title>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasTextRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisLabelRenderer.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.canvasAxisTickRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.categoryAxisRenderer.min.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.pointLabels.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.highlighter.js"></script>
<script type="text/javascript" src="<?php echo "$base/assets/dist/"?>plugins/jqplot.trendline.min.js"></script>
<link rel="stylesheet" type="text/css" src="<?php echo "$base/assets/dist/"?>jquery.jqplot.css" />
<link rel="stylesheet" type="text/css"   href="<?php echo "$base/$css"?>">
<link rel="stylesheet" type="text/css" href="<?php echo "$base/assets/css/"?>style.css">
<link rel="stylesheet" type="text/css"   href="../../assets/css/rangeslider.css">

<style>
	body{overflow-x: hidden;}	
.range_div{ display: inline-block; width:180px; margin: 12px 0;height: 240px; position: relative; margin-left: -20px;} 	
.rangeslider{background:#e6e6e6 !important; box-shadow: none;}
.rangeslider__fill{background:#b3b3b3 !important;box-shadow: none;}
.rangeslider__handle{ background:#4d4d4d !important; width: 20px !important; height: 20px !important; z-index:99;}
.rangeslider--vertical { width:8px !important; height: 100%; left: 50%; margin-left: -4px;}
.rangeslider--vertical .rangeslider__handle { left: -6px !important; border: 0;}
.rangeslider__handle:after{display: none;}	
.vio_range_div .rangeslider__fill{background:#c7b5e7 !important;}
.vio_range_div .rangeslider__handle{ background:#906bd0 !important;}
ul.range_numbers {list-style: none; margin: 0; padding: 0; position: absolute; left: 0; top:0; bottom: 0; z-index: -1;} 
ul.range_numbers li {color: #808080; font-size: 13px; width:74px; text-align: right; position:relative; margin-top:3px;}
ul.range_numbers li:before,ul.range_limits li:before{content: ''; width:20px; height: 1px; position: absolute; background:#e9e9e9; right:-35%; top: 8px;}
ul.range_numbers li:first-child{margin-top:2px;}
	
.range_div2 ul.range_numbers li{margin-top:21px;}
.range_div2 ul.range_numbers li:first-child{margin-top:3px;}
	
.range_div ul.range_numbers2 li{margin-top:15px;}
.range_div ul.range_numbers2 li:first-child{margin-top:5px;}
	
.range_div ul.range_numbers3 li{margin-top:-1.5px;}
.range_div ul.range_numbers3 li:first-child{margin-top:3px;}	

	
</style>
<!--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>-->

<script type="text/javascript" src="../../assets/js/rangeslider.js"></script>
<!--style>
#extra{ display: none; }
</style-->
<script type="text/javascript">
    
$(document).ready(function() {
     
        var vp_age='<?php echo $_SESSION['vp_age']?>';
 	var smoking='<?php echo (($fieldData['smoking'] == '1'))?"1":"0";?>';
        var gender='<?php echo $_SESSION['user_gender'];?>';
	$("#LE").html('<?php echo $_SESSION['LE'];?> yr');
	var LE_graph='<?php echo round($_SESSION['LE']);?>';
	  calculate_healthage();
	/* var calculate_width=LE_graph - 40;
			var setwidth = calculate_width * 11.3;
			 $('.graph_curve_in').css('width',setwidth+'px'); */
			
	//calculate_healthage();	
    $('input[type=radio]').change(function() 
        {
            calculate_healthage();
        });
	    if(smoking == '0')
		{
		$('.cvd_row').hide();
        }
		else 
		{
		 $('.cvd_row').show();
		}
	});
  //Calculate Life Expectancy
    function life_expectancy(health_age)
    {
        var smoking=$("input[name='smoker']:checked"). val();   // For yes or no
		
		var no_of_smoke=$("#smoke_no").val();// Here it is Smoke number
		
        var colestrol=$("#chol").val();  
        var hdl=$("#hdl").val();
        
        var SBP=$("#sbp").val();
		var pa=$("#pa").val();  // Physical Activity Value.
		var total_sed=$("#total_sed").val(); // Sedentary value.
		var waist=$("#waist").val();
		var body_weight=$("#body_weight").val();
		var dbp=$("#dbp").val();
		var triglycerides=$("#triglycerides").val();
		var glucose=$("#glucose").val();

        // For gender
        var gender=$("input[name='gend']:checked"). val();		
        var vp_age='<?php echo $_SESSION['vp_age']?>'; 

		// Algorithm Steps		 
				var actual_age='<?php echo $_SESSION['vp_age']; ?>';
				var health_age=health_age;
				var age_gap=actual_age - health_age;
			
				if(smoking == 'Y')
				{   $('.cvd_row').show();
				var smokeval=1;	
				var numbr_of_smoke = no_of_smoke;
				}else{
				$('.cvd_row').hide();
				   var smokeval=0;			
				   var numbr_of_smoke = 0;
				}


				// calculate life expectancy
				//y = -0.000090995563364x3 + 0.000452083896037x2 + 0.508746011970253x + 81.3068468339833
				var life_expectancy = ( -0.000090995563364 * Math.pow(age_gap,3) ) + (0.000452083896037 * Math.pow(age_gap,2)) + (0.508746011970253 * age_gap) + 81.3068468339833;			 
				//$life_expectancy = ( -0.000090995563364 * ($age_gap * $age_gap * $age_gap) ) + (0.000452083896037 * ($age_gap * $age_gap)) + (0.508746011970253 * ($age_gap)) + 81.3068468339833  ;
				
				// Exponent of the equation
				//y = 0.000000000112981x5 - 0.000000005784660x4 - 0.000000403605651x3 + 0.000046117808940x2 - 0.001214174971577x + 6.257928433678860
				var y= (0.000000000112981 * Math.pow(age_gap,5)) - (0.000000005784660 * Math.pow(age_gap,4)) - (0.000000403605651 * Math.pow(age_gap,3)) +  ( 0.000046117808940 * Math.pow(age_gap,2)) - (0.001214174971577 *Math.pow(age_gap,1)) + 6.257928433678860;

				/*
				IF Age difference <-40 years then x = -40
				IF Age difference >50 years then x = 50
				*/
				if(age_gap < -40)
				{
				age_gap = -40;
				}				 
				if(age_gap > 50)
				{
				age_gap = 50;
				}
				//FIRST ADJUSTMENT  = (80^y)/10000000000			 
				var first_adj = (Math.pow(80,y))/10000000000;
				// Step 4
				/* Quality Years
				y = 0.00723x2 - 1.71606x + 97.89004
				*/			 
				var quality_year = 0.00723 * (age_gap * age_gap) - 1.71606 *(age_gap) + 97.89004;
				/* Step 5 
				Life expectancy years [that is, DEATH AGE] = y
				Y = (AGE GAP * Quality years fraction)/100 + FIRST ADJUSTMENT
				Where y = DEATH AGE:
				*/
				var deathage = parseFloat((age_gap * quality_year)/100 + parseFloat(first_adj));
                if(gender == 'F' || gender == 'Female')
				{
				// Therefore FEMALE DEATH AGE = DEATH AGE + (DEATH AGE *0.1 – 4)
				deathage=parseFloat(deathage)+(parseFloat(deathage) * 0.1 - 4);			
				}
                //IF DEATH AGE < Actual Age +2 THEN DEATH AGE = Actual Age + 4		 
				if( deathage < (parseFloat(actual_age) + parseFloat(2)))
				{
				deathage = parseFloat(actual_age)+parseFloat(4);
				
                }
			  var life_exp=parseFloat((deathage).toFixed(1)); 
			$("#LE").html(life_exp+' yr');
			create_graph_array(life_exp);
			
           // Algorithm Ends
			}
   
  //Create Graph  
function create_graph_array(life_exp)
   {
   var x=40.0;
   var y=100.0;
   var add_x=(life_exp-x)/10;
   var x1=parseFloat((add_x + x).toFixed(1)) ;
   var y1='97.9';
   var x2=parseFloat((add_x + x1).toFixed(1)) ;
   var y2='96.3'; 
   var x3=parseFloat((add_x + x2).toFixed(1)) ;
   var y3='93.6'; 
   var x4=parseFloat((add_x + x3).toFixed(1)) ;
   var y4='89.6'; 
   var x5=parseFloat((add_x + x4).toFixed(1)) ;
   var y5='83.5';  
   var x6=parseFloat((add_x + x5).toFixed(1)) ;
   var y6='74.6';
   var x7=parseFloat((add_x + x6).toFixed(1)) ;
   var y7='62.1';
   var x8=parseFloat((add_x + x7).toFixed(1)) ;
   var y8='44.7';
   var x9=parseFloat((add_x + x8).toFixed(1)) ;
   var y9='21.1';
   var x10=life_exp;
   var y10='0.0';
var chartData1 = [[parseFloat(x), parseFloat(y)],[parseFloat(x1), parseFloat(y1)], [parseFloat(x2), parseFloat(y2)], [parseFloat(x3), parseFloat(y3)], [parseFloat(x4), parseFloat(y4)], [parseFloat(x5), parseFloat(y5)], [parseFloat(x6), parseFloat(y6)], [parseFloat(x7), parseFloat(y7)], [parseFloat(x8), parseFloat(y8)], [parseFloat(x9), parseFloat(y9)], [parseFloat(x10), parseFloat(y10)]] ;
 $('#chartdiv').html('');
 //plotting Graph
         var plot2 = $.jqplot ('chartdiv', [chartData1], {
      // Give the plot a title.
       seriesColors: ['#85802b'],
        title: '',
      // You can specify options for all axes on the plot at once with
      // the axesDefaults object.  Here, we're using a canvas renderer
      // to draw the axis label which allows rotated text.
      axesDefaults: {
        labelRenderer: $.jqplot.CanvasAxisLabelRenderer,
		
      },
      // Likewise, seriesDefaults specifies default options for all
      // series in a plot.  Options specified in seriesDefaults or
      // axesDefaults can be overridden by individual series or
      // axes options.
      // Here we turn on smoothing for the line.
      
        
        
       series: [{ showMarker: false, fill: true, color: '#FCB1B8' }],
      // An axes object holds options for all axes.
      // Allowable axes are xaxis, x2axis, yaxis, y2axis, y3axis, ...
      // Up to 9 y axes are supported.
      axes: {
        // options for each axis are specified in seperate option objects.
        xaxis: {
          label: "Years of life expectancy",
          ticks:['40.0', '45.0', '50.0', '55.0', '60.0', '65.0', '70.0', '75.0', '80.0', '85.0', '90.0', '95.0', '100.0'],
// Turn off "padding".  This will allow data point to lie on the
          // edges of the grid.  Default padding is 1.2 and will keep all
          // points inside the bounds of the grid.
          pad: 0
        },
        yaxis: {
          label: "",
          ticks:['0.0', '20.0', '40.0', '60.0', '80.0', '100.0', '120.0'],
		  showTicks: false
        }
      }
    });
        
        
 
   }
    

   function cumnormdist(x)
        {
          var b1 =  0.319381530;
          var b2 = -0.356563782;
          var b3 =  1.781477937;
          var b4 = -1.821255978;
          var b5 =  1.330274429;
          var p  =  0.2316419;
          var c  =  0.39894228;

          if(x >= 0.0) {
              var t = 1.0 / ( 1.0 + p * x);
              return (1.0 - c * Math.exp( -x * x / 2.0 ) * t *
              ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1 ));
          }
          else {
              var t = 1.0 / ( 1.0 - p * x );
              return ( c * Math.exp( -x * x / 2.0 ) * t *
              ( t *( t * ( t * ( t * b5 + b4 ) + b3 ) + b2 ) + b1));
            }
        }

		//calculate Healthage
		//calculate Healthage
function calculate_healthage()
	{		
var mean_values_dict_male = {
                    SBP:128.4,
                    DBP:83.22,
                    chol:5.53,
                    lnHDL:0.11,
                    LDL:19.355,
                    lntrig:0.138,
                    glucose:5.901,
                    lnBMI:3.229,
                    WHR:0.92,
                    lnSumSF:3.861,
                    SRT:207.3,
                    CRT:392.7,
                    fev1:80.1,
                    flex:4.53,
                    grip:46.13,
                    VO2max:36.6
		};
             
	 var sd_values_dict_male = {
                    SBP:19.1,
                    DBP:10.45,
                    chol:1.13,
                    lnHDL:0.233,
                    LDL:0,
                    lntrig:0.501,
                    glucose:1.354,
                    lnBMI:0.129,
                    WHR:0.066,
                    lnSumSF:0.419,
                    SRT:30,
                    CRT:60,
                    fev1:7.2,
                    flex:10.08,
                    grip:8.41,
                    VO2max:9.74
					};
                var mean_values_dict_female = {
                    SBP:123.4,
                    DBP:75.22,
                    chol:5.54,
                    lnHDL:0.109,
                    LDL:19.355,
                    lntrig:0.139,
                    glucose:5.645,
                    lnBMI:3.222,
                    WHR:0.802,
                    lnSumSF:3.981,
                    SRT:218.2,
                    CRT:409.1,
                    fev1:80.3,
                    flex:8.74,
                    grip:28.15,
                    VO2max:31.6
                };
                var sd_values_dict_female ={
                    
                    SBP:19.1,
                    DBP:10.45,
                    chol:1.12,
                    lnHDL:0.235,
                    LDL:0,
                    lntrig:0.503,
                    glucose:1.164,
                    lnBMI:0.167,
                    WHR:0.074,
                    lnSumSF:0.416,
                    SRT:32,
                    CRT:64,
                    fev1:7.3,
                    flex:9.5,
                    grip:5.25,
                    VO2max:9.51
                    
                };
					
			
// Start Calculation For HealthAge		
  var healthAge = 0.0;
  var performanceAge = 0.0;
  var health_z_count = 0.0;
  var performance_z_count = 0.0;
  var performanceYearsAdded =0;
  var healthYearsAdded =0.0;
  var totalTimeExercising=0;
  var hlth_age=0.0;
  var riskFactorNo=0;	
  
  totalTimeExercising=$("#pa").val();
  var rf_systolic=$("#sbp").val(); //SBP
  var rf_diastolic=$("#dbp").val(); //DBP (mmHg)
  var glucose=$("#glucose").val();//$rf_fbg
  var cholestrol=$("#chol").val();//$rf_tbc
  var rf_hdl=$("#hdl").val();
  
  var high_cholestrol='<?php echo (isset($fieldData['high_chol'])?$fieldData['high_chol']:"N");?>';//$rf9
  var high_blood_sugar= '<?php echo (isset($fieldData['high_blood_sugar'])?$fieldData['high_blood_sugar']:"N");?>';  //$rf10
  var clientGender=$("input[name='gend']:checked"). val();
  var triglyceride=$("#triglycerides").val();//$rf_triglycerides
  var rf_ldl=$("#ldl").val(); 
  var totalTimeVigourous='<?php echo (isset($fieldData['vigorous_time'])?$fieldData['vigorous_time']:"0");?>'; 
  var familyHistory ='<?php echo (isset($fieldData['heart_disease'])?$fieldData['heart_disease']:"N");?>'; 
   var high_bp ='<?php echo (isset($fieldData['high_bp'])?$fieldData['high_bp']:"N");?>'; //$rf8
  var clientCurrentAge = '<?php echo $_SESSION['vp_age']?>';
  var smoking=$("input[name='smoker']:checked"). val();   //$rf4
  // Composition
  var height=$("#height").val();

  // TODO for debugging
  
  if (typeof height_question_asked === 'undefined') {
	height_question_asked = false;
  }
  if((height=='' || height=='0') && height_question_asked == false)
			{	
			$(".info_block2").toggle();
			$(".overlay2").toggle();
			$('#height_text').val('');
			height_question_asked = true;
			}
		
  var  weight= $("#body_weight").val();
  
  var measuredReportBMI =(weight/(height*height))*10000;
    var self_height='<?php echo (isset($fieldData['self_height'])?$fieldData['self_height']:"0");?>';
  var self_weight='<?php echo (isset($fieldData['self_weight'])?$fieldData['self_weight']:"0");?>';
  var selfReportBMI = (self_weight/(self_height*self_height))*10000;
  var m_waist=$("#waist").val();
  var m_hip=$("#hip").val();
  var m_biceps='<?php echo (isset($fieldData['option_biceps'])?$fieldData['option_biceps']:"0");?>';
  var m_triceps='<?php echo (isset($fieldData['option_triceps'])?$fieldData['option_triceps']:"0");?>';
  var m_subscapular='<?php echo (isset($fieldData['option_subscapular'])?$fieldData['option_subscapular']:"0");?>';
  //Sum of Skin-folds
    var sumSK=$("#sumSK").val();
    if(smoking=='Y')
             {
             smoking=1;    
             }
             else
             {
             smoking=0;    
             }
  var smokeperday = $("#cig_count").val(); 
 
  var isPhyData = '<?php echo (isset($fieldData['med_status'])?$fieldData['med_status']:"N");?>';
  var sitting_job='<?php echo (isset($fieldData['sitting_job'])?$fieldData['sitting_job']:"N");?>';  //$pa9
   if(sitting_job=='Y')
             {
             sitting_job=1;    
             }
             else
             {
             sitting_job=0;    
             }
  //Calculation Start
 //Calculation Start
  if((isPhyData == "Y") || totalTimeExercising > 0){
      //For Male
	  if(clientGender == 'M')
	  { 
	   healthYearsAdded = healthYearsAdded + (-(0.03 * totalTimeExercising));
	  }
	  else
	  {
	   healthYearsAdded = healthYearsAdded - (-2+(0.025 * totalTimeExercising));	  
	  }  
        //=-(-2+(0.025 * E13))    
       	healthYearsAdded = healthYearsAdded - (0.75 * (0.01 * totalTimeVigourous));
            //Sedentary Activity
	        
        } // END med_status =='Y' && totalTimeExercising >0
		
		var yrs_sendentary = $("#total_sed").val();
            if(yrs_sendentary > 10){
                yrs_sendentary = 10;
            }
			if(yrs_sendentary>0)
			{
            healthYearsAdded = healthYearsAdded + (1.25 * (-2 + (0.5 * yrs_sendentary)));
		    }
         
		   if (sitting_job == 1 ) {
                
                 healthYearsAdded = healthYearsAdded + 1;
                 
            }

	if(familyHistory =='Y'){
	healthYearsAdded = healthYearsAdded + ((-0.1 * clientCurrentAge) + 6.5);
	}
		
		//SMOKING ALGO
		if(smoking == 1){ //Do you smoke?
                
               	if(smokeperday == ""){ //How many do you smoke per day? (/day)
              
                    hlth_age = (cumnormdist(2.0))*100;
                       healthAge = healthAge + (cumnormdist(2.0))*100;
                        health_z_count = health_z_count + 1;
                    }
                else{
              
                var tempZSCore = 0.5 + (1.5108 * Math.log(smokeperday));
                
                healthAge = healthAge + cumnormdist(tempZSCore) *100;
                        health_z_count = health_z_count + 1;
				var smoke_adj= 0.102 * smokeperday - 0.0604;
				healthYearsAdded = healthYearsAdded + smoke_adj;
		   }
		}
		else
		{
			//=IF(E19="NO",-0.0598*E7 + 0.891,0)
			//  Health Age = -0.0598 * AGE + 0.891
		healthYearsAdded = healthYearsAdded +(-0.0598 * clientCurrentAge + 0.891);
		
		}		
		
                //HIGH BP
                    if(high_bp == 'Y'){
                //if 'yes' then add 7.254 - (0.13164*age) +(0.000821*age^2) to Health Age  
                  //healthYearsAdded = healthYearsAdded + 2;  
                healthYearsAdded = healthYearsAdded + 7.254- (0.13164*clientCurrentAge)+(0.000821*Math.pow(clientCurrentAge,2));
                 }
		
                if(high_blood_sugar == 'Y'){ //Have you been told you have high blood sugar?
                   // healthYearsAdded = healthYearsAdded + 2;
                    //=5-(0.04*E7)
               healthYearsAdded = healthYearsAdded + (5-(0.04*clientCurrentAge));
                }
             
             
            if(high_cholestrol == 'Y'){
               // healthYearsAdded = healthYearsAdded + 1;
               // healthAge = healthAge + 2;
               // =5-(0.05*E7)
                healthYearsAdded = healthYearsAdded + (5-(0.05*clientCurrentAge));
                }
		
  
    // SBP AND DBP Calculation
		 if((rf_systolic > 0) || (rf_diastolic > 0))
		 {
         		if(clientGender == 'M'){
		   var mean_SBP = mean_values_dict_male["SBP"];
                    var sd_SBP =   sd_values_dict_male["SBP"];
                    var zScore = (rf_systolic - mean_SBP)/sd_SBP;
					hlth_age = cumnormdist(zScore) *100;
                   healthAge = healthAge + (hlth_age * 1.25);
                    health_z_count = health_z_count + 1;
                   
		 var mean_DBP = mean_values_dict_male["DBP"];
                    var sd_DBP = sd_values_dict_male["DBP"];
		   var zScore = (rf_diastolic - mean_DBP)/sd_DBP;
                   hlth_age = cumnormdist(zScore) *100;
                    healthAge = healthAge + (hlth_age * 1.25);
                    health_z_count = health_z_count + 1;
                   }
                //Female
				else{
                    var mean_SBP = mean_values_dict_female["SBP"];
                    var sd_SBP = sd_values_dict_female["SBP"] ;
		    var zScore = (rf_systolic - mean_SBP)/sd_SBP;
                    hlth_age = cumnormdist(zScore) *100;
                    healthAge = healthAge + (hlth_age * 1.25);
               
                health_z_count = health_z_count + 1;
                   var mean_DBP = mean_values_dict_female["DBP"];
                    var sd_DBP = sd_values_dict_female["DBP"];
		    var zScore = (rf_diastolic - mean_DBP)/sd_DBP;
                    hlth_age = cumnormdist(zScore) *100;
		
                healthAge = healthAge + (hlth_age * 1.25);
                health_z_count = health_z_count + 1;
					}
         
                }
         
    
// Cholestrol and triglycerides
 if(cholestrol != 0 || triglyceride != 0){

              if(clientGender == 'M' && (cholestrol != 0)){          
           
				 var mean_Chol = mean_values_dict_male["chol"];
				 var sd_Chol = sd_values_dict_male["chol"];
				 var zScore =   (cholestrol - mean_Chol)/sd_Chol;
				 hlth_age = cumnormdist(zScore) *100;
		         healthAge = healthAge + (hlth_age * 1.15);
                 health_z_count = health_z_count + 1;
				
				} 
				else if (clientGender == 'F' && (cholestrol != 0))
				{
                 var mean_Chol = mean_values_dict_female["chol"];
                 var sd_Chol = sd_values_dict_female["chol"];
                 var zScore =   (cholestrol-mean_Chol)/sd_Chol;
                 hlth_age = cumnormdist(zScore) *100;
		         healthAge = healthAge + (hlth_age * 1.15);
                 health_z_count = health_z_count + 1;
				
				}
			 if(clientGender == 'M' && (triglyceride != 0))
             {   
			var trigLogV = Math.log(triglyceride);
				    var mean_lntrig = mean_values_dict_male["lntrig"];
                    var sd_lntrig = sd_values_dict_male["lntrig"];
                    var zScore = (trigLogV - mean_lntrig)/sd_lntrig;
					healthAge = healthAge + cumnormdist(zScore)*100;
					health_z_count = health_z_count + 1;
					
            }
             if(clientGender == 'F' && (triglyceride != 0))
                {    
                var trigLogV = Math.log(triglyceride);
	
            var mean_lntrig = mean_values_dict_female["lntrig"];
                var sd_lntrig =  sd_values_dict_female["lntrig"];
                var zScore = (trigLogV - mean_lntrig)/sd_lntrig;
                healthAge = healthAge + cumnormdist(zScore)*100;
                health_z_count = health_z_count + 1;	
	         }
			}
		
  //Fasting blood glucose (mM)
	if(glucose > 0){  
              
		if(clientGender == 'M'){
                   var mean_glucose = mean_values_dict_male["glucose"];
				   var sd_glucose = sd_values_dict_male["glucose"] ;
				   var zScore =   (glucose-mean_glucose)/sd_glucose;
                   healthAge = healthAge + cumnormdist(zScore)*100;
                   health_z_count = health_z_count + 1;
				 }
                else{
                    
                    var mean_glucose = mean_values_dict_female["glucose"];
                    var sd_glucose = sd_values_dict_female["glucose"];
                    var zScore =   (glucose - mean_glucose)/sd_glucose;
		    healthAge = healthAge + cumnormdist(zScore)*100;
                     health_z_count = health_z_count + 1;	
                	
				}
         
        }
  
  
  // LDL CALCULATION
  if((rf_ldl != 0)){
        if(clientGender == 'M'){
            var mean_LDL = mean_values_dict_male["LDL"];
            var ldlPercent_zscore = (-30.129 + (mean_LDL * rf_ldl));
	   healthAge = healthAge + ldlPercent_zscore;
            health_z_count = health_z_count + 1;
			}
        else{
            
            var mean_LDL = mean_values_dict_female["LDL"];
            var ldlPercent_zscore = (-30.129 + (mean_LDL * rf_ldl));
           var rank =ldlPercent_zscore;
       
//            healthAge = healthAge + cumnormdist(ldlPercent_zscore)*100*1.5;
//		  
//		  // rank
//            var ranks = 0.0;
//            if(ldlPercent_zscore > 0){
//                ranks = (100 - ldlPercent_zscore)/100;
//            }
//            else{
//                ranks = ldlPercent_zscore/100;
//            }
//            healthAge = healthAge + ranks*100;
//            health_z_count = health_z_count + 1;
	}
	 }
 
 // HDL CALCULATION
 if((rf_hdl != 0)){
        if(clientGender == 'M'){
            var mean_LDL = mean_values_dict_male["LDL"];             
            var mean_lnHDL = mean_values_dict_male["lnHDL"];
            var sd_lnHDL = sd_values_dict_male["lnHDL"];
            var logV = Math.log(rf_hdl);//also converted mg/dL to mm
            var zScore = (mean_lnHDL-logV)/sd_lnHDL;
            healthAge = healthAge + cumnormdist(zScore)*100;
             health_z_count = health_z_count + 1;
            
			}
        
		else{
           var mean_lnHDL = mean_values_dict_female["lnHDL"];
           var sd_lnHDL  = sd_values_dict_female["lnHDL"];
           var logV = Math.log(rf_hdl);//also converted mg/dL to mm
           var zScore = (mean_lnHDL - logV)/ sd_lnHDL;
            healthAge = healthAge + cumnormdist(zScore)*100;
             health_z_count = health_z_count + 1;
           }
    
    }
       	if(rf_ldl > 0 && rf_hdl > 0){
         var LHratio = (rf_ldl)/(rf_hdl);
           var LHratioAdjFinal = -18.286 + (11.683 * LHratio)-(2.6667 * LHratio* LHratio) + (0.22222 * LHratio * LHratio * LHratio);
	 healthYearsAdded = healthYearsAdded + LHratioAdjFinal;
         
    }

//Body Composition
   if(measuredReportBMI > 0)
   {
   if(clientGender == 'M'){
              var mean_lnBMI = mean_values_dict_male["lnBMI"];
			  var sd_lnBMI = sd_values_dict_male["lnBMI"];
			  var zScore = (Math.log(measuredReportBMI) - mean_lnBMI)/sd_lnBMI;
			   healthAge = healthAge + cumnormdist(zScore)*100;
			   health_z_count = health_z_count + 1;
               performanceAge = performanceAge + cumnormdist(zScore)*100;
			   performance_z_count = performance_z_count + 1;	
			 }
					
		 else{
               var mean_lnBMI = mean_values_dict_female["lnBMI"];
               var  sd_lnBMI=sd_values_dict_female["lnBMI"]; 
               var zScore =  (Math.log(measuredReportBMI) - mean_lnBMI)/sd_lnBMI;
                healthAge = healthAge + cumnormdist(zScore)*100;
                   health_z_count = health_z_count + 1;
                   performanceAge = performanceAge + cumnormdist(zScore)*100;
                   performance_z_count = performance_z_count + 1;    
				  	
              
            }
     }		
	    else{
           if(selfReportBMI >= 30){
               riskFactorNo = riskFactorNo + 1;
            }
            if(clientGender == 'M')
			{
				var mean_lnBMI = mean_values_dict_male["lnBMI"];
				var sd_lnBMI = sd_values_dict_male["lnBMI"];
                var zScore = (Math.log(selfReportBMI) - mean_lnBMI)/sd_lnBMI;
				 healthAge = healthAge + cumnormdist(zScore)*100;
                 health_z_count = health_z_count + 1;
			      performanceAge = performanceAge + cumnormdist(zScore)*100;
				  performance_z_count = performance_z_count + 1;
            }
            else{
                var mean_lnBMI = mean_values_dict_female["lnBMI"];
				var sd_lnBMI = sd_values_dict_male["lnBMI"];
                var zScore = (Math.log(selfReportBMI) - mean_lnBMI)/sd_lnBMI;
                healthAge = healthAge + cumnormdist(zScore)*100;
                health_z_count = health_z_count + 1; 
                performanceAge = performanceAge + cumnormdist(zScore)*100;
                performance_z_count = performance_z_count + 1;
            }
        }
         //Waist & HIP
		 if(m_waist > 0 && m_hip > 0){
        
        if(clientGender == 'M'){
            var measuredWHR = ((m_waist))/((m_hip));
			
           
            var bc_WHR = measuredWHR;
            var bc_waist = m_waist;
            var mean_WHR = mean_values_dict_male["WHR"];
            var sd_WHR = sd_values_dict_male["WHR"];
			var zScore = (measuredWHR-mean_WHR)/sd_WHR;
			 healthAge = healthAge + cumnormdist(zScore)*100;
             health_z_count = health_z_count + 1;
		     performanceAge = performanceAge +cumnormdist(zScore)*100;
             performance_z_count = performance_z_count + 1;
        }
        else{
           
           var measuredWHR = ((m_waist))/((m_hip));
            
            var bc_WHR = measuredWHR;
            var bc_waist = m_waist;
			var mean_WHR = mean_values_dict_female["WHR"];
			var sd_WHR = sd_values_dict_female["WHR"];
			var zScore = (measuredWHR-mean_WHR)/sd_WHR;
			healthAge = healthAge + cumnormdist(zScore)*100;
            health_z_count = health_z_count + 1;
            performanceAge = performanceAge + cumnormdist(zScore)*100;
            performance_z_count = performance_z_count + 1;
		  
		 }
       
    }
	
	//Triceps , Biceps,subscapular
        if(sumSK > 0.0){
              var mean_lnSumSF = mean_values_dict_male["lnSumSF"];
            var sd_lnSumSF = sd_values_dict_male["lnSumSF"];    
            var bc_SOS = sumSK;
            if(clientGender == 'M'){
          		  var mean_lnSumSF = mean_values_dict_male["lnSumSF"];
			  var sd_lnSumSF = sd_values_dict_male["lnSumSF"];
              var zScore = (Math.log(sumSK)-mean_lnSumSF)/sd_lnSumSF ;
                healthAge = healthAge + cumnormdist(zScore)*100;
                health_z_count = health_z_count + 1; 
				performanceAge = performanceAge + cumnormdist(zScore)*100;
                performance_z_count = performance_z_count + 1.5;            
            }
            else{
                var mean_lnSumSF = mean_values_dict_female["lnSumSF"];
              var sd_lnSumSF = sd_values_dict_female["lnSumSF"];
              var zScore = (Math.log(sumSK)-mean_lnSumSF)/sd_lnSumSF;
               hlth_age = cumnormdist(zScore) *100;
				
				healthAge = healthAge + (hlth_age * 1.25);
				 	 
				  health_z_count = health_z_count + 1;
					performanceAge = performanceAge + cumnormdist(zScore)*100*1.5;
                    performance_z_count = performance_z_count + 1.5;
				
				}
            }
   
	var finalHealthAge = 0.0;
    if(health_z_count == 0)
	{
        health_z_count = 1;
    }
	
    finalHealthAge = healthAge / health_z_count;
  var finalAgeValue =0;
  var avg_z_score_age=finalHealthAge;
 	if(finalHealthAge>0)
	{
	 finalAgeValue= (parseFloat(clientCurrentAge)+parseFloat(finalHealthAge))/2 + parseFloat(healthYearsAdded);
	 }
	 else
	 {
	finalAgeValue=clientCurrentAge;
	}
	
	healthAge=finalAgeValue;
   console.log('Health_age :'+healthAge);
   life_expectancy(healthAge);
  }

  </script>    

<style>
.big{height:1190px !important;}
.small{height:800px !important;}
/*.dot-red{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#00ff00;}*/

.dot-red{position:relative;width:21px;height:21px;border-radius:15px;background:none;border-color:#000;border-width:2px;border-style:solid}
.dot-green{position:relative;width:25px;height:25px;border-radius:15px;background:#ff0000;}
.cvd_row .field_25 .range_div output {
    transform: translateX(220%) !important;
}	
</style>
</head>
<script type="text/javascript">
        $(document).on('click','.info_icon_btn', function(){
			$(".info_block").toggle();
			$(".overlay").toggle();
		});  
		$(document).on('click','.close', function(){
			$(".info_block").toggle();
			$(".overlay").toggle();
		}); 
	//RESET
	$(document).on('click','#reset', function(){
		window.location = "https://est-demo.jamesanthony.consulting/VirtualEST/index.php/welcome/life_expectancy";
		}); 
	
		$(document).on('change','#body_weight_range', function(){
			if(height=='' || height=='0')
			{	
			$(".info_block2").toggle();
			$(".overlay2").toggle();
			$('#height_text').val('');
			}
		});  
		$(document).on('click','.close2', function(){
			$(".info_block2").toggle();
			$(".overlay2").toggle();
		}); 
		
	$(document).on('click','#height_submit', function(){
			var chaged_height=$('#height_text').val();
			if(chaged_height!='' && chaged_height>0)
			{
			$('#height').val(chaged_height);
			 calculate_healthage();	
			}
			$(".info_block2").toggle();
			$(".overlay2").toggle();
		}); 
	
$(function() {
  $('input[type="range"]').on('input', function() {
	var control = $(this),
    controlMin = control.attr('min'),
    controlMax = control.attr('max'),
    controlVal = control.val(),
    controlThumbWidth = control.data('thumbwidth');

  var range = controlMax - controlMin;
  
  var position = ((controlVal - controlMin) / range) * 100;
  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
  var output = control.next('output');
  
  output
    .css('bottom', 'calc(' + position + '% - ' + positionOffset + 'px)')
    .text(controlVal);
  var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
    calculate_healthage();
   $text_box.val(this.value);
});
});

$(function() {
  
  if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i)) || (navigator.userAgent.match(/iPad/i))) {
  $('input[type="range"]').bind('touchstart', function() {
	 var control = $(this),
    controlMin = control.attr('min'),
    controlMax = control.attr('max'),
    controlVal = control.val(),
    controlThumbWidth = control.data('thumbwidth');

  var range = controlMax - controlMin;
  
  var position = ((controlVal - controlMin) / range) * 100;
  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
  var output = control.next('output');
  
  output
    .css({'display':'block','transform':'translateX(130%)','-webkit-transform':'translateX(130%)','-webkit-backface-visibility':'hidden','backface-visibility':'hidden','bottom':'calc(' + position + '% - ' + positionOffset + 'px)'})
    .text(controlVal);
  var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
    calculate_healthage();
   $text_box.val(this.value);
});
$('input[type="range"]').bind('touchend', function() {
    $("output").css({'display':'none'});
});
 }
 else
 {
	$('input[type="range"]').on('mouseenter', function() {
		 var control = $(this),
		controlMin = control.attr('min'),
		controlMax = control.attr('max'),
		controlVal = control.val(),
		controlThumbWidth = control.data('thumbwidth');

	  var range = controlMax - controlMin;
	  
	  var position = ((controlVal - controlMin) / range) * 100;
	  var positionOffset = Math.round(controlThumbWidth * position / 100) - (controlThumbWidth / 2);
	  var output = control.next('output');
	  
	  output
		.css({'display':'block','transform':'translateX(130%)','-webkit-transform':'translateX(130%)','-webkit-backface-visibility':'hidden','backface-visibility':'hidden','bottom':'calc(' + position + '% - ' + positionOffset + 'px)'})
		.text(controlVal);
	  var $slider = $(this),
		 $text_box = $('#'+$(this).attr('link-to'));
		$text_box.val(this.value);
	});
	$('input[type="range"]').bind('mouseleave', function() {
		$("output").css({'display':'none'});
	});	 
 }
});


 $(function() {
  $('input').filter( function(){return this.type == 'range' } ).each(function(){  
      var $slider = $(this),
     $text_box = $('#'+$(this).attr('link-to'));
     $slider.change(function(){
             $text_box.val(this.value); 
             calculate_healthage();
			
        //calculate_oz_rating();
	});
   $text_box.on("change", function() {
   $slider.val($text_box.val()).change();
 
    });
      $text_box.val(this.value);   
	});
}); 


</script>
<body>

<!-- insertion of header -->
<?=$header_insert; ?>

 <?php echo form_open('welcome/save_life_expectancy',''); ?> 

<div class="overlay">&nbsp;</div>
<div class="info_block">
	<div class="info_block_head">Life expectancy screen</div>
	<p>Life expectancy is based on the current inputs of risk factors and health-related behaviours. These are each compared to population norms for all adults to give a series of percentile ranks. These ranks are weighted according to WHO burden of disease statistics to account for variables having greater or lessor effects on life expectancy. Finally, based on the relative age (or 'health age'), trajectories of years of life left (added to current chronological age to give a life expectancy) are made based on average patterns for the equivalent (health) age.
	</p>    
	<div class="info_block_foot">
		<a href="#" class="lite_btn grey_btn f_right close">Close</a>
	</div>                
</div>       

<!--Start Wrapper --> 
<div class="wrapper">
<div class="login-cont" style="display:none;">
   <div class="section">
        <span><b>First name</b><input name="fname" type="text" size="60" value="<?php echo $_SESSION['user_first_name'] ;?>" disabled="disabled" required></span>
        <span><b>Last name</b><input name="lname" type="text" size="60" value="<?php echo $_SESSION['user_last_name'] ;?>" disabled="disabled" required><input name="submitMedication" type="submit" value="" title="edit client details"/></span>
     </div>
</div>
<div class="contain medi-contain-new pnt_mt_20">
   <div class="left">
            <div class="btn">
            <button type="submit" name="mysubmit1" class="left_panel_btn" id="myform1"><img src="<?php echo "$base/assets/images/"?>icon_medical.png"> Medical History</button>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit2" class="left_panel_btn" id="myform2"><img src="<?php echo "$base/assets/images/"?>icon_physical.png"> Physical Activity</button>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit3" class="left_panel_btn" id="myform3"><img src="<?php echo "$base/assets/images/"?>icon_risk.png"> Risk Factors</button>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit4" class="left_panel_btn" id="myform4"><img src="<?php echo "$base/assets/images/"?>icon_bodyComposition.png"> Body Composition</button>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit5" class="left_panel_btn" id="myform5"><img src="<?php echo "$base/assets/images/"?>icon_medication.png"> Medications & Conditions</button>
            </div>
            
            <div class="btn">
            <button type="submit" name="mysubmit6" class="left_panel_btn" id="myform6"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Screening Summary</button>
            </div>
			<div class="btn">
            <button type="submit" name="mysubmit7" class="left_panel_btn" id="myform7"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Absolute CVD risk</button>
            </div>
			
			<div class="btn">
            <button type="submit" name="mysubmit8" class="left_panel_btn active" id="myform8"><img src="<?php echo "$base/assets/images/"?>icon_screening.png"> Life Expectancy</button>
            </div>
     
       </div>
   <div class="right">
       
   		<div class="right-head" style="margin-bottom:0;">Life Expectancy Estimator</div>
   		<div class="right-section page_medi">
       
      
        <div class="field_row vd_head"> 
            <div class="field_85">This is an estimated life expectancy based on your current inputs for risk factors and health-related behaviours</div>
        </div>
		
        <div class="field_row" style="padding-bottom:0;">
        	<div class="field_35 f_left any_left">        
			    
			
	   		<p style="font-size:18px; margin-bottom:15px;">'What-if' analysis</p>
                   <div class="field_row checkbox_title_row" style="border:0;">
            <div class="field_50">Age</div>
				<div class="field_50">
                                      <input type="text" id="age" name="age" value="<?php echo $_SESSION['vp_age'];?>" disabled style="width:62px;">      
                                </div>
            </div>
     		 <div class="field_row checkbox_title_row" style="border:0;">
            <div class="field_50">Gender</div>
				<div class="field_50">
                                    <?php $radio_is_checked = ( ($gender === 'M' || $gender === 'Male' ) ) ? "checked" : "N";
                                    echo form_radio(array("name" => "gend", "id" => "option_5Y", "value" => "M", "class" => "gender", 'checked' => ($radio_is_checked === "N") ? "" : "checked"));
                                    ?> <label for="option_5Y"><span style="margin-right: 5px;"></span>Male</label>
                                    <?php echo form_radio(array("name" => "gend", "id" => "option_5N", "value" => "F", "class" => "gender", 'checked' => ($radio_is_checked === "N") ? "checked" : "")); ?> <label for="option_5N"><span style="margin-right: 5px;"></span>Female</label>             
                                </div>
            </div> 
      		<div class="field_row prnt_mar0 prnt_pad0">
				<div class="field_50">Smoker</div>
				<div class="field_50">
					 <?php     $radio_is_checked = ( ($fieldData['smoking'] == '1'))?"checked":"N";
                        echo form_radio(array("name"=>"smoker","id"=>"option_2Y","value"=>"Y", "class"=>"gender",'checked'=>($radio_is_checked === "N")?"":"checked" ));?> <label for="option_2Y"><span style="margin-right: 5px;"></span>Yes</label>
                        <?php 
                        echo form_radio(array("name"=>"smoker","id"=>"option_2N","value"=>"N", "class"=>"gender",'checked' =>($radio_is_checked === "N")?"checked":""));?> <label for="option_2N"><span style="margin-right: 5px;"></span>No</label>             
				</div>
				<div class="clearfix">&nbsp;</div>
			</div> 
                        </div>
			
         	<div class="field_65 f_right any_right">
			  <div class="field_row cvd_row" style="margin:0; padding: 0; border: 0;">
				<div class="field_25">
				  <input type="text" id="cig_count" name="cig_count" value="<?php echo (isset($fieldData['numbr_of_smoke'])?$fieldData['numbr_of_smoke']:"0");?>" style="width:80px;">
				  <label>cigarettes/day</label>
				  <div class="range_div"> 
					<input name="cig_slider" type="range" value="<?php echo (isset($fieldData['numbr_of_smoke'])?$fieldData['numbr_of_smoke']:"0");?>" min="1" max="40" step="1" link-to="cig_count" data-thumbwidth="20" data-rangeslider class="rang_slider vertical voilet">
					<output name="rangeVal" class="voilet"></output>
					<ul class="range_numbers range_numbers2">
					  <li>40</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>1</li>
					</ul>
				  </div>
				  <!--<label>min/wk</label>-->
				</div>
			  </div>
			</div>
                    
        </div>
        
			<button type="button" name="reset" class="lite_btn grey_btn f_right btn_orng print_hide" id="reset" style="margin-bottom: 20px;">Reset</button>
			
		  <div class="field_row cvd_row_7 print_tpmr">
		  
		  	<div class="field_25">
				  <input type="text" id="pa" name="pa" value="<?php echo $fieldData['physical_activity'];?>" style="width:38px;">
				  <label>physical<br>activity</label>
				  <div class="range_div"> 
					
					<input name="pa_slider" type="range" value="<?php echo (isset($fieldData['physical_activity'])?$fieldData['physical_activity']:"0");?>" min="0" max="600" step="1" link-to="pa" data-thumbwidth="20" data-rangeslider class="rang_slider vertical voilet">
					<output name="rangeVal" class="voilet"></output>
					<ul class="range_numbers">
					  <li>600</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>0</li>
					</ul>
				  </div>
				  <label>min/wk</label>
				</div>
				<div class="field_25">
				  <input type="text" id="total_sed" name="total_sed" value="<?php echo (isset($fieldData['total_sed'])?$fieldData['total_sed']:"0");?>" style="width:38px;">
				  <label>sitting <br>time</label>
				  <div class="range_div">
					<input name="sed_slider" type="range" value="<?php echo (isset($fieldData['total_sed'])?$fieldData['total_sed']:"0");?>" min="0" max="10" step="0.1" data-orientation="vertical" link-to="total_sed" data-rangeslider data-thumbwidth="20"  class="rang_slider vertical voilet">
					<output name="rangeVal" class="voilet"></output>
					<ul class="range_numbers">
					  <li>10</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>0</li>
					</ul>
				  </div>
				  <label>hr/day</label>
				</div>
				<div class="field_25">
				  <input type="text" id="waist" name="waist" value="<?php echo (isset($fieldData['waist'])?$fieldData['waist']:"40");?>" style="width:38px;">
				  <label>waist</label>
				  <div class="range_div">
					<input name="waist_slider" type="range" value="<?php echo (isset($fieldData['waist'])?$fieldData['waist']:"40");?>" min="40" max="150" step="0.1" data-orientation="vertical" link-to="waist" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
					<output name="rangeVal" class="voilet"></output>
					<ul class="range_numbers">
					  <li>150</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>40</li>
					</ul>
				  </div>
				  <label>cm</label>
				</div>
				<div class="field_25">
				  <input type="text" id="hip" name="hip" value="<?php echo (isset($fieldData['hip'])?$fieldData['hip']:"40");?>"  style="width:38px;">
				  <label>Hip</label>
				  <div class="range_div">
					<input name="hip_slider" type="range" value="<?php echo (isset($fieldData['hip'])?$fieldData['hip']:"40");?>" min="40" max="150" step="0.1" data-orientation="vertical" link-to="hip" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
					<output name="rangeVal" class="voilet"></output>
					<ul class="range_numbers">
					  <li>150</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>40</li>
					</ul>
				  </div>
				  <label>cm</label>
				</div>
			<div class="field_25">
			     <input type="hidden" name="height" id="height" value="<?php 
				 //echo (isset($fieldData['height'])?$fieldData['height']:"0");
				 echo (isset($_SESSION['HEIGHT'])?$_SESSION['HEIGHT']:"0");				 
				 ?>"/>
			  <input type="text" id="body_weight" name="body_weight" value="<?php echo (isset($fieldData['weight'])?$fieldData['weight']:"40");?>" style="width:38px;">
			  <label>body <br>weight</label>
			  <div class="range_div"> 
				
				<input name="weight_slider" type="range" value="<?php echo (isset($fieldData['weight'])?$fieldData['weight']:"40");?>" min="40" max="200" step="1" link-to="body_weight" data-thumbwidth="20" data-rangeslider class="rang_slider vertical voilet" id="body_weight_range">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>200</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>40</li>
				</ul>
			  </div>
			  <label>kg</label>
			</div>
			 <div class="field_25">
			  <input type="text" id="sumSK" name="sumSK" value="<?php echo (isset($fieldData['sumSK'])?$fieldData['sumSK']:"20");?>"  style="width:38px;">
			  <label>skinfold <br>sum</label>
			  <div class="range_div"> 
				
				<input name="sumSK_slider" type="range" value="<?php echo (isset($fieldData['sumSK'])?$fieldData['sumSK']:"20");?>" min="20" max="200" step="1" link-to="sumSK" data-thumbwidth="20" data-rangeslider class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>200</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>20</li>
				</ul>
			  </div>
			  <label>kg</label>
			</div>
			<div class="field_25">
			  <input type="text" id="sbp" name="sbp" value="<?php echo (isset($fieldData['SBP'])?$fieldData['SBP']:"80");?>" style="width:38px;">
			  <label>SBP</label>
			  <div class="range_div">
				<input name="sbp_slider" type="range" value="<?php echo (isset($fieldData['SBP'])?$fieldData['SBP']:"80");?>" min="80" max="200" step="1" data-orientation="vertical" link-to="sbp" data-rangeslider data-thumbwidth="20"  class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>200</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>80</li>
				</ul>
			  </div>
			  <label>mmHg</label>
			</div>
			<div class="field_25">
			  <input type="text" id="dbp" name="dbp" value="<?php echo (isset($fieldData['DBP'])?$fieldData['DBP']:"40");?>" style="width:38px;">
			  <label>DBP</label>
			  <div class="range_div">
				<input name="dbp_slider" type="range" value="<?php echo (isset($fieldData['DBP'])?$fieldData['DBP']:"40");?>" min="40" max="140" step="1" data-orientation="vertical" link-to="dbp" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>140</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>40</li>
				</ul>
			  </div>
			  <label>mmHg</label>
			</div>
			
			<div class="field_25">
			  <input type="text" id="chol" name="chol" value="<?php echo (isset($fieldData['colestrol'])?$fieldData['colestrol']:"3");?>" style="width:38px;">
			  <label>cholest-<br>erol</label>
			  <div class="range_div">
				<input name="chol_slider" type="range" value="<?php echo (isset($fieldData['colestrol'])?$fieldData['colestrol']:"3");?>" min="3" max="12" step="0.1" data-orientation="vertical" link-to="chol" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>12</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>3</li>
				</ul>
			  </div>
			  <label>mM</label>
			</div>
			
			<div class="field_25">
			  <input type="text" id="hdl" name="hdl" value="<?php echo (isset($fieldData['HDL'])?$fieldData['HDL']:"0.6");?>" style="width:38px;">
			  <label>HDL</label>
			  <div class="range_div">
				<input name="hdl_slider" type="range" value="<?php echo (isset($fieldData['HDL'])?$fieldData['HDL']:"0.6");?>" min="0.6" max="2.8" step="0.1" data-orientation="vertical" link-to="hdl" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>2.8</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>0.6</li>
				</ul>
			  </div>
			  <label>mM</label>
			</div>
			<!--newly added-->
                      <div class="field_25">
			  <input type="text" id="ldl" name="ldl" value="<?php echo (isset($fieldData['ldl'])?$fieldData['ldl']:"1");?>"  style="width:38px;">
			  <label>LDL</label>
			  <div class="range_div">
				<input name="ldl_slider" type="range" value="<?php echo (isset($fieldData['ldl'])?$fieldData['ldl']:"1");?>" min="1" max="10" step="0.1" data-orientation="vertical" link-to="ldl" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>10</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>1</li>
				</ul>
			  </div>
			  <label>mM</label>
			</div>
			<div class="field_25">
			  <input type="text" id="triglycerides" name="triglycerides" value="<?php echo (isset($fieldData['triglycerides'])?$fieldData['triglycerides']:"1");?>" style="width:38px;">
			  <label>triglyce-<br>rides</label>
			  <div class="range_div">
				<input name="tri_slider" type="range" value="<?php echo (isset($fieldData['triglycerides'])?$fieldData['triglycerides']:"1");?>" min="1" max="8" step="0.1" data-orientation="vertical" link-to="triglycerides" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>8</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>1</li>
				</ul>
			  </div>
			  <label>mM</label>
			</div>
			
			<div class="field_25">
			  <input type="text" id="glucose" name="glucose" value="<?php echo (isset($fieldData['glucose'])?$fieldData['glucose']:"1");?>" style="width:38px;">
			  <label>glucose</label>
			  <div class="range_div">
				<input name="glucose_slider" type="range" value="<?php echo (isset($fieldData['glucose'])?$fieldData['glucose']:"1");?>" min="1" max="12" step="0.1" data-orientation="vertical" link-to="glucose" data-rangeslider data-thumbwidth="20" class="rang_slider vertical voilet">
				<output name="rangeVal" class="voilet"></output>
				<ul class="range_numbers">
				  <li>12</li>
				  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
					  <li>&nbsp;</li>
				  <li>1</li>
				</ul>
			  </div>
			  <label>mM</label>
			</div>
		  </div>
        
        <p style="page-break-after: always;">&nbsp;</p>
        	<div class="field_row" style="border: 0;">
        		 <div class="health_tag">Life Expectancy : <span id="LE"></span></div>
        		<p style="font-size:18px; margin-bottom:15px;" class="print_center">Relative health</p>
				<div id="chartdiv" style="width: 700px; height: 380px; margin-bottom:30px; margin: auto;"></div> 
			</div>
         <div class="clearfix">&nbsp;</div>
          
         
         <!--<a href="#" class="info_icon_btn2"><img src="<?php echo "$base/assets/images/"?>info_org.jpg"></a>-->
         <div class="overlay2">&nbsp;</div>
        <div class="info_block2">
            <div class="info_block_head2">Height</div>
            
			<div class="field_row" style="border:0;">
            	<div class="field_50">Enter Height in cm</div>
				<div class="field_50">
					  <input type="text" id="height_text" name="height_text" value="">      
				</div>
            </div>
           
            
             <div class="info_block_foot2">             	
             	<a href="#" class="lite_btn grey_btn f_right close2">Close</a>
             	<a href="#" id="height_submit" class="lite_btn btn_orng f_right" style="margin-right: 12px;">Submit</a>
             </div>                
        </div>
      <?php //echo form_submit('mysubmit4','Previous',"class='lite_btn grey_btn f_left btn_orng'","'id' = 'myform4'");?>
      <div>
		<?php echo form_submit('mysubmit7','Previous',"class='lite_btn grey_btn btn_orng'","'id' = 'myform6'");?>
		<a onclick="window.location.href = 'https://est-demo.jamesanthony.consulting/VirtualEST/index.php/';">
			<div class="lite_btn grey_btn f_right btn_orng">Back to Dashboard</div>
		</a>
	  </div>
	   
      <?php echo form_close(); ?>
       
      </div><!--End right section--> 
   </div><!--End right --> 
</div><!--End contain -->

</div><!--End Wrapper --> 
<div class="footer" style="display: none;"> 
	<div class="wrapper">
    	<p style="float:left; margin-top:8px;">Developed by Professor Kevin Norton and Dr Lynda Norton</p>
    </div> 
</div>
</body>
</html>
