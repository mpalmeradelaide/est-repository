<?php
	include 'ChromePhp.php';

	ChromePhp::log("ajax_submit_body_composition has been loaded and run");
	
	/**
	 * Saving any changes that may be made on the body composition form to the session
	 * unique to this particular form.
	 *
	 * @author: Matt Palmer
	 */

	//_SESSION['']=round($_POST[''],1);

	//ChromePhp::log("");
	ChromePhp::log("Values obtained from body composition form");
	ChromePhp::log("option_height = " .$_POST['option_height']);
	ChromePhp::log("option_weight = " .$_POST['options_weight']);
	ChromePhp::log("option_height_measured = " .$_POST['option_height_measured']);
	ChromePhp::log("option_weight_measured = " .$_POST['option_weight_measured']);
	ChromePhp::log("option_bmi = " .$_POST['option_bmi']);
	ChromePhp::log("option_waist = " .$_POST['option_waist']);
	ChromePhp::log("option_hip = " .$_POST['option_hip']);
	ChromePhp::log("option_whr = " .$_POST['option_whr']);
	ChromePhp::log("triceps = " .$_POST['option_triceps']);
	ChromePhp::log("biceps = " .$_POST['option_biceps']);
	ChromePhp::log("subscapular = " .$_POST['option_subscapular']);
	ChromePhp::log("option_sos = " .$_POST['option_sos']);

	$_SESSION['option_height']=round($_POST['option_height'],1);
	$_SESSION['option_weight']=round($_POST['options_weight'],1);
	$_SESSION['option_height_measured']=round($_POST['option_height_measured'],1);
	$_SESSION['option_weight_measured']=round($_POST['option_weight_measured'],1);
	$_SESSION['option_bmi']=round($_POST['option_bmi'],1);
	$_SESSION['option_waist']=round($_POST['option_waist'],1);
	$_SESSION['option_hip']=round($_POST['option_hip'],1);
	$_SESSION['option_whr']=round($_POST['option_whr'],1);
	$_SESSION['triceps']=round($_POST['option_triceps'],1);
	$_SESSION['biceps']=round($_POST['option_biceps'],1);
	$_SESSION['subscapular']=round($_POST['option_subscapular'],1);
	$_SESSION['option_sos']=round($_POST['option_sos'],1);

?>