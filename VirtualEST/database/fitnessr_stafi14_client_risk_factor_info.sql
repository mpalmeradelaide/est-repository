-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_risk_factor_info`
--

DROP TABLE IF EXISTS `client_risk_factor_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_risk_factor_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `option_1` varchar(10) NOT NULL,
  `option_2` varchar(10) NOT NULL,
  `option_3` varchar(10) NOT NULL,
  `option_4` varchar(10) NOT NULL,
  `option_5` varchar(10) NOT NULL,
  `option_6` varchar(10) NOT NULL,
  `option_7` varchar(10) NOT NULL,
  `option_8` varchar(10) NOT NULL,
  `option_9` varchar(10) NOT NULL,
  `option_10` varchar(10) NOT NULL,
  `option_11` varchar(10) NOT NULL,
  `option_12` varchar(10) NOT NULL,
  `option_13` varchar(100) NOT NULL,
  `option_gender` varchar(10) NOT NULL,
  `option_age` varchar(100) NOT NULL,
  `option_smoke` varchar(100) NOT NULL,
  `option_smoke_6` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_risk_factor_info`
--

LOCK TABLES `client_risk_factor_info` WRITE;
/*!40000 ALTER TABLE `client_risk_factor_info` DISABLE KEYS */;
INSERT INTO `client_risk_factor_info` VALUES (41,76,'N','N','N','N','N','N','120','80','','','','3.6','','0','','',''),(42,77,'N','N','N','N','N','N','130','85','1.7','2.3','4','4.6','1.2','0','','',''),(43,78,'N','N','N','N','N','N','124','76','','','','5.3','','0','','',''),(44,79,'N','N','0','N','N','N','','','','','','','','0','','',''),(45,80,'N','Y','N','Y','N','N','132','84','0.8','4.3','5.1','5.3','1.7','0','','10',''),(46,81,'N','N','N','N','N','N','','','','','','','','0','','',''),(47,82,'N','N','N','N','N','N','','','','','','','','0','','',''),(48,83,'N','N','0','0','N','N','','','','','','','','0','','',''),(49,84,'0','0','0','0','0','0','','','','','','','','0','','',''),(50,86,'0','0','0','0','0','0','','','','','','','','0','','',''),(51,85,'0','0','0','0','0','0','','','','','','','','0','','',''),(52,87,'0','0','0','0','0','0','','','','','','','','0','','','');
/*!40000 ALTER TABLE `client_risk_factor_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:49
