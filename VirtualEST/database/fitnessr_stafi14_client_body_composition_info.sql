-- MySQL dump 10.13  Distrib 5.6.23, for Win32 (x86)
--
-- Host: localhost    Database: fitnessr_stafi14
-- ------------------------------------------------------
-- Server version	5.5.42

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `client_body_composition_info`
--

DROP TABLE IF EXISTS `client_body_composition_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client_body_composition_info` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `c_id` int(11) NOT NULL,
  `option_height` varchar(10) NOT NULL,
  `option_weight` varchar(10) NOT NULL,
  `option_height_measured` varchar(10) NOT NULL,
  `option_weight_measured` varchar(10) NOT NULL,
  `option_bmi` varchar(10) NOT NULL,
  `option_waist` varchar(10) NOT NULL,
  `option_hip` varchar(10) NOT NULL,
  `option_whr` varchar(10) NOT NULL,
  `option_triceps` varchar(10) NOT NULL,
  `option_biceps` varchar(10) NOT NULL,
  `option_subscapular` varchar(10) NOT NULL,
  `option_sos` varchar(10) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client_body_composition_info`
--

LOCK TABLES `client_body_composition_info` WRITE;
/*!40000 ALTER TABLE `client_body_composition_info` DISABLE KEYS */;
INSERT INTO `client_body_composition_info` VALUES (42,76,'165','73','','','26.8','','','0','','','','0'),(43,77,'187','76','187','76','21.7','85','99','0.86','7','5','9','21'),(44,78,'','','199','89','22.5','83','','0','','','','0'),(45,79,'193','85','','','22.8','','','0','','','','0'),(46,80,'185','96','185','99.5','29.1','103','111','0.93','','','','0'),(47,81,'177','88','','','28.1','','','0','','','','0'),(48,82,'173','67','','','22.4','','','0','','','','0'),(49,83,'180','83','','','25.6','83','102','0.81','7','5','9.5','21.5'),(50,84,'','','','','0','','','0','','','','0'),(51,86,'','','','','0','','','0','','','','0');
/*!40000 ALTER TABLE `client_body_composition_info` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-27 17:17:55
