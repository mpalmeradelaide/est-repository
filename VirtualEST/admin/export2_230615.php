<?php
error_reporting(1);
include('db.php');
function csvEscape($escape) {
	$escape = str_replace(","," ",$escape);
	$escape = addslashes($escape);
	return $escape;
}

function generateCsv($data, $delimiter = ',', $enclosure = '"') {
       $handle = fopen('php://temp', 'r+');
       foreach ($data as $line) {
               fputcsv($handle, $line, $delimiter, $enclosure);
       }
       rewind($handle);
       while (!feof($handle)) {
               $contents .= fread($handle, 8192);
       }
       fclose($handle);
       return $contents;
}
 
//$host = '[INSERT DATABASE HOST]';
//$user = '[INSERT USERNAME]';
//$pass = '[INSERT PASSWORD';
//$db = '[INSERT DB NAME]';
//$table = '[INSERT TABLE]';
 
//$link = mysql_connect($host, $user, $pass) or die("Can not connect." . mysql_error());
//mysql_select_db($db) or die("Can not connect.");
/*
$tables =array('client_info','client_medical_info','client_medication_info','client_physical_activity_info','client_risk_factor_info','client_body_composition_info');
*/
     $csv_output.= "client Info\n";
// ci.first_name  as 'First Name', ci.last_name as 'Last Name',
    //$csv_output.= $table."\n";

/*
    $result = mysql_query("SELECT ci.gender, ci.dob as 'Date Of Birth', ci.country ,ci.occupation, 
     mi.option_1 as 'med1',mi.option_2 as 'med2' ,mi.option_3 as 'med3',mi.option_4 as 'med4',mi.option_5 as 'med5',mi.option_6 as 'med6',mi.option_7 as 'med7',mi.notes as 'med8_notes',mi.option_8 as 'med9',
     pai.option_1 as 'pa1',pai.option_2 as 'pa2',pai.option_3 as 'pa3',pai.option_4 as 'pa4',pai.option_5 as 'pa5',pai.option_6 as 'pa6',pai.option_7 as 'pa7',pai.option_8 as 'pa8',pai.option_9 as 'pa9',
     rfi.option_1 as 'rf1',rfi.option_2 as 'rf2',rfi.option_3 as 'rf3',rfi.option_4 as 'rf4',rfi.option_5 as 'rf5',rfi.option_6 as 'rf6',rfi.option_7 as 'rf7',rfi.option_8 as 'rf8',rfi.option_9 as 'rf9',rfi.option_10 as 'rf10',rfi.option_11 as 'rf11',rfi.option_12 as 'rf12',rfi.option_13 as 'rf13',rfi.option_gender as 'rf_gender',rfi.option_age as 'rf_age',rfi.option_smoke as 'rf_smoke',rfi.option_smoke_6 as 'rf_smoke_6',
     bci.option_height as 'self height',bci.option_weight as 'self weight',bci.option_height_measured as 'm_height',bci.option_weight_measured as 'm_weight',bci.option_bmi as 'bmi' ,bci.option_waist as 'waist',bci.option_hip as 'hip' ,bci.option_whr as 'whr',bci.option_triceps as 'triceps',bci.option_biceps as 'biceps',bci.option_subscapular as 'subscapular' ,bci.option_sos as 'sos',
     medi.option_1 as 'ai1',medi.medical_conditions as 'ai2',medi.medical_regular as 'ai3',medi.option_4 as 'ai4',medi.notes as 'notes' ,ci.risk_factor as 'Risk Factor' ,ci.created_date as 'Test Date' FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id");

*/


 $result = mysql_query("SELECT 
 ci.gender,
 ci.dob as 'date of birth',
 ci.country ,
 ci.occupation,
 ci.p_name as 'Project Name',
 ci.email as 'Email',
 ci.p_desc as 'Project Description',
 ci.screening_decision_category as 'Screening Decision Category',
 mi.option_1 as 'APSS stage 1 qu1 CVD',
 mi.option_2 as 'APSS stage 1 qu2 chest pain' ,
 mi.option_3 as 'APSS stage 1 qu3 dizzyness',
 mi.option_4 as 'APSS stage 1 qu4 asthma',
 mi.option_5 as 'APSS stage 1 qu5 diabetes',
 mi.option_6 as 'APSS stage 1 qu6 musculoskeletal',
 mi.option_7 as 'APSS stage 1 qu7 other',
 mi.notes as 'APSS stage 1 notes',
 mi.option_8 as 'APSS stage 1 professional decision',
 pai.option_1 as 'PA walking times/wk',
 pai.option_2 as 'PA walking min/wk',
 pai.option_3 as 'PA vigorous times/wk',
 pai.option_4 as 'PA vigorous min/wk',
 pai.option_5 as 'PA moderate times/wk',
 pai.option_6 as 'PA moderatemin/wk',
 pai.option_7 as 'SED screen time hr/day',
 pai.option_8 as 'SED drive time hr/day',
 pai.option_9 as 'OCCUP sed',
 rfi.option_1 as 'RF 1 family history',
 rfi.option_2 as 'RF2 current smoker',
 rfi.option_3 as 'RF 2 quit smoking',
 rfi.option_4 as 'self report high BP',
 rfi.option_5 as 'self report high chol',
 rfi.option_6 as 'self report high BG',
 rfi.option_7 as 'RF SBP',
 rfi.option_8 as 'RF DBP',
 rfi.option_9 as 'RF HDL',
 rfi.option_10 as 'RF LDL',
 rfi.option_11 as 'RF TC',
 rfi.option_12 as 'RF FBG',
 rfi.option_13 as 'RF LDL',
 rfi.option_gender as 'RF 1 gender',
 rfi.option_age as 'RF 1 age',
 rfi.option_smoke as 'RF 2 current smokes/day',
 rfi.option_smoke_6 as 'RF 2 prior smokes/day',
 bci.option_height as ' self report height',
 bci.option_weight as 'self report weight',
 bci.option_height_measured as 'measured height',
 bci.option_weight_measured as 'measured weight',
 bci.option_bmi as 'BMI' ,
 bci.option_waist as 'waist',
 bci.option_hip as 'hip' ,
 bci.option_whr as 'WHR',
 bci.option_triceps as 'triceps',
 bci.option_biceps as 'biceps',
 bci.option_subscapular as 'subscapular' ,
 bci.option_sos as 'sum of skinfolds',
 medi.option_1 as 'hospitalisation',
 medi.medical_conditions as 'medical conditions',
 medi.medical_regular as 'medication class',
 medi.option_4 as 'musculoskeletal condition',
 medi.notes as 'medication notes' ,
 ci.risk_factor as 'risk factor total [-2 to +9]' ,
ci.screening_decision_category	 as 'screening decision category' ,
 ci.created_date as 'test date' 
 FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id");




    $i = 0;
    if (mysql_num_rows($result) > 0) {
     while ($row = mysql_fetch_field($result)) {
       //print_r($row);
      $csv_output .= $row->name.", ";
      $i++;
     }
    }
    $csv_output .= "\n" ;
   
// $values = mysql_query("SELECT * FROM {$table}");

  //ci.first_name  as 'First Name', ci.last_name as 'Last Name',
 $values = mysql_query("SELECT  ci.gender, ci.dob as 'Date Of Birth', ci.country ,ci.occupation, ci.p_name as 'Project Name', ci.email as 'Email', ci.p_desc as 'Project Description', ci.screening_decision_category as 'Screening Decision Category',
     mi.option_1 as 'med1',mi.option_2 as 'med2' ,mi.option_3 as 'med3',mi.option_4 as 'med4',mi.option_5 as 'med5',mi.option_6 as 'med6',mi.option_7 as 'med7',mi.notes as 'med8_notes',mi.option_8 as 'med9',
     pai.option_1 as 'pa1',pai.option_2 as 'pa2',pai.option_3 as 'pa3',pai.option_4 as 'pa4',pai.option_5 as 'pa5',pai.option_6 as 'pa6',pai.option_7 as 'pa7',pai.option_8 as 'pa8',pai.option_9 as 'pa9',
     rfi.option_1 as 'rf1',rfi.option_2 as 'rf2',rfi.option_3 as 'rf3',rfi.option_4 as 'rf4',rfi.option_5 as 'rf5',rfi.option_6 as 'rf6',rfi.option_7 as 'rf7',rfi.option_8 as 'rf8',rfi.option_9 as 'rf9',rfi.option_10 as 'rf10',rfi.option_11 as 'rf11',rfi.option_12 as 'rf12',rfi.option_13 as 'rf13',rfi.option_gender as 'rf_gender',rfi.option_age as 'rf_age',rfi.option_smoke as 'rf_smoke',rfi.option_smoke_6 as 'rf_smoke_6',
     bci.option_height as 'self height',bci.option_weight as 'self weight',bci.option_height_measured as 'm_height',bci.option_weight_measured as 'm_weight',bci.option_bmi as 'bmi' ,bci.option_waist as 'waist',bci.option_hip as 'hip' ,bci.option_whr as 'whr',bci.option_triceps as 'triceps',bci.option_biceps as 'biceps',bci.option_subscapular as 'subscapular' ,bci.option_sos as 'sos',
     medi.option_1 as 'ai1',medi.medical_conditions as 'ai2',medi.medical_regular as 'ai3',medi.option_4 as 'ai4',medi.notes as 'notes', ci.risk_factor as 'Risk Factor',ci.screening_decision_category	 as 'screening decision category' , ci.created_date as 'Test Date' FROM `client_info` ci
  left JOIN client_medical_info mi ON mi.c_id = ci.id 
  left JOIN client_physical_activity_info pai ON pai.c_id = ci.id 
  left JOIN client_risk_factor_info rfi ON rfi.c_id = ci.id 
  left JOIN client_medication_info medi ON medi.c_id = ci.id 
  left JOIN client_body_composition_info bci ON bci.c_id = ci.id");
    
  //  $values = mysql_query("SELECT * FROM client_info");
   $data =array();
    while ($rowr = mysql_fetch_row($values)) {
     //  print_r($rowr, $rowr);
      array_push($data,$rowr);
       //echo "</br>"; 
    // for ($j=0;$j<$i;$j++) {
     // print($rowr[$j]);   
    //  $csv_output .= csvEscape($rowr).", ";
   //   $csv_output .= generateCsv($rowr).", ";
     //}
    // $csv_output .= "\n";
    }
     // die;
     $csv_output .= generateCsv($data);
   
   //print_r($data);
   
    //$csv_output .= $table;
//} 
  //print $csv_output;
 //die;
$filename = "export_".date("Y-m-d_H-i",time());
header("Content-type: application/vnd.txt");
header( "Content-disposition: attachment; filename=".$filename.".csv");
print $csv_output;
exit;

?>